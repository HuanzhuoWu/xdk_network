/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/
/* module includes ********************************************************** */

/* system header files */
#include "BCDS_Basics.h"
#include <stdio.h>

/* additional interface header files */
#include "BCDS_WlanConnect.h"
#include "BCDS_NetworkConfig.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Assert.h"
#include "PTD_portDriver_ih.h"
#include "PTD_portDriver_ph.h"

/* own header files */
#include "WlanNetworkManagement.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
#if (ONE == NCI_DHCP_MODE)
static uint8_t dhcpFlag_mdu8; /**< callback flag for DHCP; let user know that the DHCP IP was acquired */
#endif
/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/******************************************************************************/
/**
 * @brief        WLI Disconnect callback function. This function is called by
 *               the device when a disconnect event takes place.
 *
 * @param  [in]  returnStatus
 *               Enumeration element containing the connection return status.
 *
 * @returns      none.
 *
 ******************************************************************************/
void myAllDisconnectCallbackFunc(WlanConnect_Status_T returnStatus)
{
    if (WLAN_DISCONNECTED == returnStatus)
    {
        /* Network disconnection successfully from WlanConnect_Disconnect function*/
        printf("[NCA] : Callback Function : Disconnected by event or by function!\n\r");
    }
    else
    {
        /* Disconnection failed, still connected */
    }
}

/******************************************************************************/
/**
 *  @brief Local function for initializing the leds
 *
 *  @param       none
 *
 *  @returns     none
 ******************************************************************************/
static void ledsInit(void)
{
    /* init led*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_ORANGE));
    /* init led*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_YELLOW));
    /* init led*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_RED));
}

/******************************************************************************/
/**
 *  @brief Local function for turning on the leds
 *
 *  @param       none
 *
 *  @returns     none
 ******************************************************************************/
static void ledsOn(void)
{
    /* led on */
    PTD_pinOutSet((GPIO_Port_TypeDef )PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
    /* led on */
    PTD_pinOutSet((GPIO_Port_TypeDef )PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
    /* led on */
    PTD_pinOutSet((GPIO_Port_TypeDef )PTD_PORT_LED_RED, PTD_PIN_LED_RED);
}
#if (ONE == NCI_DHCP_MODE)
/******************************************************************************/
/**
 * @brief        NCI DHCP Callback function. This function is called by
 *               the device when an IP was acquired using DHCP.
 *
 * @param  [in]  returnStatus
 *               Enumeration element containing the IP return status.
 *
 * @returns      none.
 *
 ******************************************************************************/
void myDhcpIpCallbackFunc(NetworkConfig_IpStatus_T returnStatus)
{
    /* set the DHCP flag*/
    if (NETWORKCONFIG_IPV4_ACQUIRED == returnStatus)
    {
        dhcpFlag_mdu8 = NETWORKCONFIG_DHCP_FLG_ACQ;
        printf("[NCA] : Callback Function : IP was acquired using DHCP\n\r");
    }
    else
    {
        /* DHCP failed */
    }
}
#endif
/******************************************************************************/
/**
 *  @brief Local function for turning off the leds
 *
 *  @param       none
 *
 *  @returns     none
 ******************************************************************************/
static void ledsOff(void)
{
    /* led off*/
    PTD_pinOutClear((GPIO_Port_TypeDef )PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
    /* led on */
    PTD_pinOutClear((GPIO_Port_TypeDef )PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
    /* led off*/
    PTD_pinOutClear((GPIO_Port_TypeDef )PTD_PORT_LED_RED, PTD_PIN_LED_RED);
}

/******************************************************************************/
/**
 * @brief        This function does the following :
 *                 - disconnects from a network if connected
 *                 - sets an IP (either static or DHCP),
 *                 - connects to a known network
 *                 - get the IP setting
 *                 - disconnects from WLAN network
 *
 * @param        none.
 *
 * @return       none.
 ******************************************************************************/
static void setAndGetIp(void)
{
    /* local variables */
    NetworkConfig_IpSettings_T myIpGet;
    Retcode_T retStatus[MAX_SIZE_BUFFER];
    WlanConnect_SSID_T connectSSID;
    WlanConnect_PassPhrase_T connectPassPhrase;

    /* WPA-PSK and WPA2-PSK wireless network SSID */
    connectSSID = (WlanConnect_SSID_T) WLAN_CONNECT_WPA_SSID;
    /* WPA-PSK and WPA2-PSK wireless network Password */
    connectPassPhrase = (WlanConnect_PassPhrase_T) WLAN_CONNECT_WPA_PASS;

    /* ************************************************************************/
    /*   Set IPV4 parameters
     **************************************************************************/
#if (ZERO == NCI_DHCP_MODE)
    /* local variable */
    NetworkConfig_IpSettings_T myIpSet;
    /* set static IP parameters*/
    myIpSet.isDHCP = (uint8_t) NETWORKCONFIG_DHCP_DISABLED; /* Disable DHCP*/
    myIpSet.ipV4 = NetworkConfig_Ipv4Value(192, 168, 1, 111);
    myIpSet.ipV4DnsServer = NetworkConfig_Ipv4Value(192, 168, 1, 1);
    myIpSet.ipV4Gateway = NetworkConfig_Ipv4Value(192, 168, 1, 1);
    myIpSet.ipV4Mask = NetworkConfig_Ipv4Value(255, 255, 255, 0);
#endif /* (ZERO == NCI_DHCP_MODE) */

#if (ONE == NCI_DHCP_MODE)
    /* local variable */
    NetworkConfig_IpCallback_T myIpCallback;
    /* Set the IP callback*/
    myIpCallback = myDhcpIpCallbackFunc;
#endif /* (ONE == NCI_DHCP_MODE) */

#if (TWO == NCI_DHCP_MODE)
    /* Default mode*/
#endif /* (TWO == NCI_DHCP_MODE) */

    /* ************************************************************************/
    /*    Disconnect from previous connect if any
     **************************************************************************/
    WlanConnect_DisconnectCallback_T myAllDisconnectCallback;
    myAllDisconnectCallback = myAllDisconnectCallbackFunc;

    if (DISCONNECTED_IP_NOT_ACQUIRED
            != WlanConnect_GetCurrentNwStatus()
            && (DISCONNECTED_AND_IPV4_ACQUIRED
                    != WlanConnect_GetCurrentNwStatus()))
    {
        /* If the there are any valid profiles stored, the board could connect
         * immediately after power up; therefore a disconnect must be called*/
        retStatus[0] = (Retcode_T) WlanConnect_Disconnect(myAllDisconnectCallback);

        if (RETCODE_OK == retStatus[0])
        {
            printf("[NCA] : Disconnect successful.\n\r");
        }
        else
        {
            printf("[NCA] : Disconnect has failed\n\r");
        }
    }

    /* ************************************************************************/
    /*   Call Set IP functions
     **************************************************************************/
#if (ZERO == NCI_DHCP_MODE)
    /* Set the static IP */
    retStatus[1] = NetworkConfig_SetIpStatic(myIpSet);

    if (RETCODE_OK == retStatus[1])
    {
        printf("[NCA] : Static IP is set to : %u.%u.%u.%u\n\r",
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpSet.ipV4, 3)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpSet.ipV4, 2)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpSet.ipV4, 1)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpSet.ipV4, 0)));
    }
    else
    {
        printf("[NCA] : NetworkConfig_SetIpStatic API has failed\n\r");
    }
#endif /* (ZERO == NCI_DHCP_MODE) */
#if (ONE == NCI_DHCP_MODE)
    printf("[NCA] : DHCP is set\n\r");
    /*reset DHCP flag and let callback set it to 0s*/
    dhcpFlag_mdu8 = NETWORKCONFIG_DHCP_FLG_NOT_ACQ;
    /* Set the DHCP with callback*/
    retStatus[2] = NetworkConfig_SetIpDhcp(myIpCallback);
    if (RETCODE_OK == retStatus[2])
    {
        printf("[NCA] : IP will be set using DHCP with callback. Waiting for IP ...\n\r");
    }
    else
    {
        printf("[NCA] : NetworkConfig_SetIpDhcp API has failed\n\r");
    }
#endif /* (NCI_DHCP_MODE == 1) */
#if (TWO == NCI_DHCP_MODE)
    /* Set the DHCP without callback */
    retStatus[3] = NetworkConfig_SetIpDhcp(ZERO);

    if (RETCODE_OK == retStatus[3])
    {
        printf("[NCA] : IP will be set using DHCP. Waiting for IP ...\n\r");
    }
    else
    {
        printf("[NCA] : NetworkConfig_SetIpDhcp API has failed!\n\r");
    }
#endif /* (TWO == NCI_DHCP_MODE) */

    /* ************************************************************************/
    /*   Connect to a WLAN WPA network
     **************************************************************************/
    printf("\n\r[NCA] : XDK will connect to %s network.\n\r",
    WLAN_CONNECT_WPA_SSID);
    retStatus[4] = (Retcode_T) WlanConnect_WPA(connectSSID, connectPassPhrase,
    NULL);
    if (RETCODE_OK == retStatus[4])
    {
        printf("[NCA] : Connected successfully.\n\r");
    }
    else if ((Retcode_T) RETCODE_CONNECTION_ERROR == Retcode_getCode(retStatus[4]))
    {
        printf("[NCA] : WLI_connectWPA API SSID has failed!\n\r");
    }
    else if ((Retcode_T) RETCODE_ERROR_WRONG_PASSWORD == Retcode_getCode(retStatus[4]))
    {
        printf("[NCA] : WLI_connectWPA API PASSWORD has failed!\n\r");
    }
    else
    {
        printf("[NCA] : WLI_connectWPA API  has failed!\n\r");
    }
    /* ************************************************************************/
    /*   Get the IP settings for both Static and DHCP
     **************************************************************************/
#if (ZERO == NCI_DHCP_MODE)
    /* Get Static IP settings and store them locally*/
    retStatus[5] = NetworkConfig_GetIpSettings(&myIpGet);

    if (RETCODE_OK == retStatus[5])
    {
        printf("[NCA] :  - The static IP was retrieved : %u.%u.%u.%u \n\r",
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 3)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 2)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 1)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 0)));
    }
    else
    {
        printf("[NCA] : NetworkConfig_GetIpSettings API has failed\n\r");
    }

#endif /* (ZERO == NCI_DHCP_MODE) */

#if (ONE == NCI_DHCP_MODE)

    /* Give time to acquired IP*/
    static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
    vTaskDelay((portTickType) DELAY_5_SEC / portTICK_RATE_MS);

    if (NETWORKCONFIG_DHCP_FLG_ACQ == dhcpFlag_mdu8)
    {
        retStatus[7] = NetworkConfig_GetIpSettings(&myIpGet);

        if (RETCODE_OK == retStatus[7])
        {
            printf("[NCA] :  - The DHCP IP (with callback) : %u.%u.%u.%u \n\r",
                    (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 3)),
                    (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 2)),
                    (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 1)),
                    (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 0)));
        }
        else
        {
            printf("[NCA] : NetworkConfig_GetIpSettings API has failed\n\r");
        }
    }
#endif /* (ONE == NCI_DHCP_MODE) */

#if (TWO == NCI_DHCP_MODE)
    retStatus[6] = NetworkConfig_GetIpSettings(&myIpGet);

    if (RETCODE_OK == retStatus[6])
    {
        printf("[NCA] :  - The DHCP IP (no callback) : %u.%u.%u.%u \n\r",
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 3)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 2)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 1)),
                (unsigned int) (NetworkConfig_Ipv4Byte(myIpGet.ipV4, 0)));
    }
    else
    {
        printf("[NCA] : NetworkConfig_GetIpSettings API has failed\n\r");
    }
#endif /* (TWO == NCI_DHCP_MODE) */

    /* ************************************************************************/
    /*   Disconnect
     **************************************************************************/

    static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
    vTaskDelay((portTickType) DELAY_5_SEC / portTICK_RATE_MS);

    retStatus[8] = WlanConnect_Disconnect(NULL);

    if (RETCODE_OK == retStatus[8])
    {
        /* Disconnected successfully */
        printf("[NCA] : Disconnection successfully.\n\r");
    }
    else
    {
        printf("[NCA] : Disconnection has failed.\n\r");
    }

    printf("[NCA] : ###################################################\n\r");
    printf("[NCA] : ## User can configure the application by using : ##\n\r");
    printf("[NCA] : ##                  NCI_DHCP_MODE                ##\n\r");
    printf("[NCA] : ###################################################\n\r");
}

/******************************************************************************/
/**
 * @brief        Local function for scanning networks. This function also calls
 *               the setAndGetIp function in order to set/get IP settings
 *               and connect/disconnect from a network.
 * @param        none.
 *
 * @return       none.
 ******************************************************************************/
static void scanNetwork(void)
{
    /* local variables */
    Retcode_T retScanStatus[MAX_SIZE_BUFFER];
    WlanConnect_ScanInterval_T scanInterval = SCAN_INTERVAL;
    WlanConnect_ScanList_T scanList;

    /* Initialize and turn on led before scanning*/
    ledsInit();
    ledsOn();

    retScanStatus[0] = WlanConnect_ScanNetworks(scanInterval, &scanList);

    if (RETCODE_OK == retScanStatus[0])
    {
        printf("\n\r[NCA] : Hey User! XDK found the following networks:\n\r");

        for (int i = ZERO; i < WLANCONNECT_MAX_SCAN_INFO_BUF; i++)
        {
            if (ZERO != scanList.ScanData[i].SsidLength)
            {
                printf("[NCA] :  - found SSID number %d is : %s\n\r", i,
                        scanList.ScanData[i].Ssid);
                static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
                vTaskDelay((portTickType) DELAY_500_MSEC / portTICK_RATE_MS);
            }
        }
        printf("[NCA] : Finished scan successfully \n\r");
    }
    else if ((Retcode_T) RETCODE_NO_NW_AVAILABLE == Retcode_getCode(retScanStatus[0]))
    {
        printf("[NCA] : Scan function did not found any network\n\r");
    }
    else /*(RETCODE_FAILURE == retScanStatus[0])*/
    {
        printf("[NCA] : Scan function failed\n\r");
    }
    /* Turn off the led when the scan has finished and results are printed */
    ledsOff();
}

/******************************************************************************/
/**
 * @brief        Local function for infinite loop. This function calls the
 *               scanNetwork and the setAndGetIp function
 * @param [in]   handle
 *               Generic pointer for handling the function.
 *
 * @return       none.
 ******************************************************************************/
static void infiniteLoopFunction(void* handle)
{
    /* local variables */
    (void) handle;

    INFINITE_LOOP
    {
        /* Scan for networks*/
        scanNetwork();
        /* Set IP, Connect, Get IP and Disconnect*/
        setAndGetIp();
    }
}

/******************************************************************************/
/**
 *  @brief       Function to Initialize the Network Configuration Abstraction
 *               application. Create timer task to start scanNetwork
 *               function after one second.
 *  @param       none
 *
 *  @returns     none
 ******************************************************************************/
static void init(void)
{
    /* Initialize WLAN driver and WLAN stack*/
    if (RETCODE_OK != WlanConnect_Init())
    {
        printf("Error occurred initializing WLAN \r\n ");
        return;
    }

    /* Create task */
    if (pdPASS
            != xTaskCreate(infiniteLoopFunction,
                    (const char * const ) "infiniteLoopFunction",
                    TASK_STACK_DEPTH, NULL, TASK_PRIORITY, ZERO))
    {
        /* Assertion due to "SW timer was not Started, Due to Insufficient heap memory" */
        assert(pdFAIL);
    }
}

/* global functions ********************************************************* */

/******************************************************************************/
/**
 * @brief        System initialization function, This function is called by the
 *               system at the beginning of the startup procedure.
 *               Called only once
 *
 * @param  [in]  xTimer
 *               The name of the OS timer handle.
 *
 * @returns      none.
 *
 ******************************************************************************/
void appInitSystem(xTimerHandle xTimer)
{
    (void) (xTimer);
    /*Call the NCA module init API */
    init();
}
/** ************************************************************************* */
