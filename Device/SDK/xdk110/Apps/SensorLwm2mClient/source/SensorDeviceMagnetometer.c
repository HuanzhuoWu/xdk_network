/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Magnetometer.h"
#include "SensorDeviceMagnetometer.h"

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

static SensorDeviceProcessDataFloat_T SensorDeviceData = { { { 0 } }, 3, 0, CURRENT, false };

void SensorDeviceMagnetometer_Activate(bool enable)
{
    if (SensorDeviceData.enabled == enable)
        return;
    SensorDeviceData.enabled = enable;
    SensorDevice_ResetProcessDataFloat(&SensorDeviceData);
    if (enable)
    {
        float ValueRange = 2500.F;
        Retcode_T ReturnValue = Magnetometer_init(xdkMagnetometer_BMM150_Handle);
        if (RETCODE_OK != ReturnValue)
            return;

        ObjectMagnetometer_Enable(-ValueRange, ValueRange);
    }
    else
    {
        if (RETCODE_OK != Magnetometer_deInit(xdkMagnetometer_BMM150_Handle))
        {
            printf("Disable magnetometer failed!\n");
        }
        ObjectMagnetometer_Disable();
    }
}

void SensorDeviceMagnetometer_Update(enum ProcessingMode mode, bool notify)
{
    if (!SensorDeviceData.enabled)
        return;

    SensorDeviceSampleDataFloat_T Sample;
    Magnetometer_XyzData_T GetMagData = { INT32_C(0), INT32_C(0), INT32_C(0), INT32_C(0) };

    Retcode_T AdvancedApiRetValue = Magnetometer_readXyzTeslaData(xdkMagnetometer_BMM150_Handle, &GetMagData);

    if (RETCODE_OK == AdvancedApiRetValue)
    {
        Sample.values[0] = (float) GetMagData.xAxisData;
        Sample.values[1] = (float) GetMagData.yAxisData;
        Sample.values[2] = (float) GetMagData.zAxisData;
        SensorDevice_ProcessDataFloat(mode, &SensorDeviceData, &Sample);
    }
    if (notify)
    {
        if (SensorDevice_GetDataFloat(&SensorDeviceData, &Sample))
            ObjectMagnetometer_SetValues((float) Sample.values[0], (float) Sample.values[1], (float) Sample.values[2]);
    }
}

