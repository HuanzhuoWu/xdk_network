/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/* module includes ********************************************************** */

/* own header files */
#include "SntpTime.h"
#include "ButtonsMan.h"
#include "Lwm2mInterface.h"
#include "Lwm2mObject_AlertNotification.h"

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* additional interface header files */
#include "led.h"
#include "button.h"
#include "XdkBoardHandle.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/*lint -esym(956, Button?Handle, UseButtonsForLed) const after init */
static BUTTON_handle_tp Button1Handle = (BUTTON_handle_tp) NULL; /**< variable to store button 1 handle */
static BUTTON_handle_tp Button2Handle = (BUTTON_handle_tp) NULL; /**< variable to store button 2 handle */
static bool UseButtonsForLed = false;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief     this callback will get triggered whenever any one of button is pressed
 * @param[in] handle handle of the push button
 * @param[in] userParameter parameter of the corresponding handle
 */
static void Callback(void *handle, uint32_t userParameter)
{
    char buf[100];
    switch (userParameter)
    {
    /*  Button 1 press/release */
    case CALLBACK_PARAMETER_PB1:
        if (BUTTON_isPressed(handle))
        {
            if (UseButtonsForLed)
            {
                /* use button 1 in test mode */
                printf("Button-1 pressed  - Red ON\n\r");
                Lwm2mInterfaceRedLedSetState(true);
                /* button 1 triggers notification */
                char buf[100];
                sprintf(buf, "Button Alert:%lu:Button-1 Push:MEDIUM:BTN1", (unsigned long)GetUtcTime());
                ObjectAlertNotification_SetValue(buf);
            }
            else
            {
                printf("Button-1 pressed\n\r");
            }
        }
        if (BUTTON_isReleased(handle))
        {
            if (UseButtonsForLed)
            {
                /* use button 1 in test mode */
                printf("Button-1 released - Red OFF\n\r");
                Lwm2mInterfaceRedLedSetState(false);
                /* button 1 triggers notification */
                sprintf(buf, "Button Alert:%lu:Button-1 Release:MEDIUM:BTN1", (unsigned long)GetUtcTime());
                ObjectAlertNotification_SetValue(buf);
            }
            else
            {
                printf("Button-1 released\n\r");
            }
        }
        break;
        /* Button 2 press/release */
    case CALLBACK_PARAMETER_PB2:
        if (BUTTON_isPressed(handle))
        {
            if (UseButtonsForLed)
            {
                /* use button 2 in test mode */
                printf("Button-2 pressed  - Orange OFF\n\r");
                Lwm2mInterfaceOrangeLedSetState(false);
                /* button 2 is currently not intended to trigger a notification */
            }
            else
            {
                printf("Button-2 pressed\n\r");
            }
        }
        if (BUTTON_isReleased(handle))
        {
            if (UseButtonsForLed)
            {
                /* use button 2 in test mode */
                printf("Button-2 released - Orange ON\n\r");
                Lwm2mInterfaceOrangeLedSetState(true);
                /* button 2 is currently not intended to trigger a notification */
            }
            else
            {
                printf("Button-2 released\n\r");
            }
        }
        break;
    default:
        printf("Button not available\n\r");
        break;
    }
}

/**
 * @brief This will create the handles for button
 *
 * @retval FAILURE Button Handle is not created
 * @retval SUCCESS Button Handle is created
 */
static Return_t CreateButton(void)
{
    Return_t returnValue = FAILURE;

    Button1Handle = BUTTON_create(gpioButton1_Handle, GPIO_STATE_OFF);
    if (Button1Handle != NULL)
    {
        Button2Handle = BUTTON_create(gpioButton2_Handle, GPIO_STATE_OFF);
    }
    if (Button2Handle != NULL)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/**
 * @brief This will enable the  button
 *
 * @retval FAILURE Button Handle is not enabled
 * @retval SUCCESS Button Handle is enabled
 */
static Return_t EnableButton(void)
{
    Return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;
    buttonReturn = BUTTON_enable(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_enable(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/**
 * @brief This will set the callback for the button
 *
 * @retval FAILURE Callback set failed
 * @retval SUCCESS Callback set successfully
 */
static Return_t SetButtonCallback(void)
{
    Return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_setCallback(Button1Handle, Callback, CALLBACK_PARAMETER_PB1);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_setCallback(Button2Handle, Callback, CALLBACK_PARAMETER_PB2);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/* global functions ********************************************************* */
/**
 * @brief This will create and enable both LED and BUTTON handle
 *
 * @retval FAILURE led and button initialization failed
 * @retval SUCCESS led and button initialization success
 */
Return_t InitButtonMan(bool testMode)
{
    Return_t returnValue = FAILURE;

    UseButtonsForLed = testMode;
    returnValue = CreateButton();
    if (returnValue == SUCCESS)
    {
        returnValue = EnableButton();
    }
    else
    {
        printf("Error in creating button\n\r");

    }
    if (returnValue == SUCCESS)
    {
        returnValue = SetButtonCallback();
    }
    else
    {
        printf("Error in enabling button\n\r");
    }
    return (returnValue);
}

/**
 * @brief This will delete both LED and BUTTON handles
 *
 * @retval FAILURE led and button de-initialization failed
 * @retval SUCCESS led and button de-initialization success
 */
Return_t LedAndButtonDeinit(void)
{
    Return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_delete(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_delete(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/** ************************************************************************* */
