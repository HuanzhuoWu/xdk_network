/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_GYROSCOPE_H_
#define SOURCE_LWM2MOBJECT_GYROSCOPE_H_

#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for Gyroscope.
 * @see https://github.com/IPSO-Alliance/pub/blob/master/reg/xml/3334.xml
 */
#define LWM2M_OBJECTID_GYROSCOPE 3334

/**
 * @brief LWM2M resource data for barometer.
 */
struct GyroscopeResource_S
{
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T units; /**< textual units, "Deg/s" according UCUM */
    Lwm2mResource_T xValue; /**< value for x-axis */
    Lwm2mResource_T yValue; /**< value for y-axis */
    Lwm2mResource_T zValue; /**< value for z-axis */
};

typedef struct GyroscopeResource_S GyroscopeResource_T;

/*lint -esym(956, GyroscopeResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern GyroscopeResource_T GyroscopeResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectGyroscope_Init(void);

/**
 * @brief Enable LWM2M object instance to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRange minimum range of value in [Deg/s].
 * @param[in] maxRange maximum range of value in [Deg/s].
 */
extern void ObjectGyroscope_Enable(float minRange, float maxRange);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectGyroscope_Disable(void);

/**
 * @brief Set values for x-, y-, and z-axis.
 *
 * Set values for axis, units in "Deg/s". Will triggers the sending of
 * a notification, if a values was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] xValue value of x-axis in [Deg/s]
 * @param[in] yValue value of y-axis in [Deg/s]
 * @param[in] zValue value of z-axis in [Deg/s]
 */
extern void ObjectGyroscope_SetValues(float xValue, float yValue, float zValue); // in Deg/s

#endif /* SOURCE_LWM2MOBJECT_GYROSCOPE_H_ */
