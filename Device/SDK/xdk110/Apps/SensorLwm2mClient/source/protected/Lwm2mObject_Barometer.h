/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_BAROMETER_H_
#define SOURCE_LWM2MOBJECT_BAROMETER_H_

/* additional interface header files */
#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for barometer.
 */
#define LWM2M_OBJECTID_IPSO_BAROMETER 3315

/**
 * @brief LWM2M resource data for barometer.
 */
struct BarometerResource_S
{
    Lwm2mResource_T minMeasuredValue; /**< minimum value */
    Lwm2mResource_T maxMeasuredValue; /**< maximum value */
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T resetMinMaxValues; /**< reset minimum and maximum values */
    Lwm2mResource_T sensorValue; /**< current sensor value */
    Lwm2mResource_T units; /**< textual units, "hPa" according UCUM */
};

typedef struct BarometerResource_S BarometerResource_T;
/*lint -esym(956, BarometerResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern BarometerResource_T BarometerResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectBarometer_Init(void);

/**
 * @brief Enable the LWM2M object instance to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRangeValue_hPa minimum range of possible values in [hPa]
 * @param[in] maxRangeValue_hPa maximum range of possible values in [hPa]
 */
extern void ObjectBarometer_Enable(float minRangeValue_hPa, float maxRangeValue_hPa);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectBarometer_Disable(void);

/**
 * @brief  the function set the current measured sensorValue in [hPa]
 *
 * Will triggers the sending of a notification, if the value was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] sensorValue_hPa current sensor value in [hPa]
 */
extern void ObjectBarometer_SetValue(float sensorValue_hPa);

#endif /* SOURCE_LWM2MOBJECT_BAROMETER_H_ */
