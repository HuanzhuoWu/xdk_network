/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef XDK110_BUTTONS_MAN_H_
#define XDK110_BUTTONS_MAN_H_

#include "BCDS_Basics.h"

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define ZERO_VALUE             UINT32_C(0)        /**< Macro to represent zero value */
#define CALLBACK_PARAMETER_PB1 UINT32_C(0x11)     /**< Macro represents callback parameter for Button1 */
#define CALLBACK_PARAMETER_PB2 UINT32_C(0x12)     /**< Macro represents callback parameter for Button2 */

/** enum to represent return status */
enum Return_E
{
    FAILURE,
    SUCCESS,
};

typedef enum Return_E Return_t;
/**
 * @brief Initialize button manager.
 * In test mode, button 1 is bound to the red LED and triggers a alert notification
 * and button 2 is bound to the orange LED.
 * @param[in] testMode true, to enable test mode, false, to disable test mode.
 */
Return_t InitButtonMan(bool testMode);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* XDK110_BUTTONS_MAN_H_ */

/** ************************************************************************* */
