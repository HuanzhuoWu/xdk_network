/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_ILLUMINANCE_H_
#define SOURCE_LWM2MOBJECT_ILLUMINANCE_H_

#include <Serval_Lwm2m.h>
#include <Serval_Exceptions.h>

/**
 * @brief LWM2M object id for illuminance.
 */
#define LWM2M_OBJECTID_IPSO_ILLUMINANCE 3301

/**
 * @brief LWM2M resource data for illuminance.
 */
struct IlluminanceResource_S
{
    Lwm2mResource_T minValue; /**< minimum value */
    Lwm2mResource_T maxValue; /**< maximum value */
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T resetMinMax; /**< reset minimum and maximum values */
    Lwm2mResource_T value; /**< current sensor value */
    Lwm2mResource_T units; /**< textual units, "mlx" according UCUM */
};

typedef struct IlluminanceResource_S IlluminanceResource_T;

/*lint -esym(956, IlluminanceResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern IlluminanceResource_T IlluminanceResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectIlluminance_Init(void);

/**
 * @brief Enables the LWM2M object instance
 * to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRange minimum range of possible values in [mlx]
 * @param[in] maxRange maximum range of possible values in [mlx]
 */
extern void ObjectIlluminance_Enable(float minRange, float maxRange);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectIlluminance_Disable(void);

/**
 * @brief  the function set the current measured sensorValue in [mlx]
 *
 * Will triggers the sending of a notification, if the value was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] currentValue current sensor value in [mlx]
 */
extern void ObjectIlluminance_SetValue(float currentValue);

#endif /* SOURCE_LWM2MOBJECT_ILLUMINANCE_H_ */
