/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */
#ifndef XDK110_APPLWM2MINTERFACE_H_
#define XDK110_APPLWM2MINTERFACE_H_

#include <Serval_Defines.h>

#if SERVAL_ENABLE_LWM2M
#include <Serval_Types.h>
#include <Serval_CoapServer.h>
#include <Serval_Network.h>

/* public type and macro definitions */

/** enum to represent LED return status */
enum led_return_E
{
    RETURN_FAILURE,
    RETURN_SUCCESS,
};

typedef enum led_return_E led_return_t;
/**
 * LWM2M configuration
 */
struct Lwm2mConfiguration_S
{
	bool        sdCardConfig; /**< LWM2M server is read from SD Card */
	uint16_t    localPort; /**< port number of LWM2M Interface between Device and LWM2M Server */
	const char* destServer; /**< LWM2M server for registering and data exchange */
	uint32_t    lifetime; /**< lifetime for registration updates */
	bool        useDtlsPsk; /**< indicates use of DTLS/PSK */
	const char* pskIdentity; /**< DTLS/PSK identity */
	const char* pskSecretKey; /**< DTLS/PSK secret key */
};

typedef struct Lwm2mConfiguration_S Lwm2mConfiguration_T;

/**
 * Callback handler for LED state changes.
 *
 * @param[in] state new state of LED. true for LED on, false, for off
 */
typedef void (*LedStateChangeHandler_T)(bool state);


/**
 * @brief The function initializes the LWM2M Interface between Device and the LWM2M Server.
 * @param[in] endpointName  LWM2M endpoint name. The parameter is used as reference
 *                          therefore the endpoint name must stay valid after the call.
 * @param[in] macAddr MAC Address supporting the application. The parameter is used as reference
 *                          therefore MAC address must stay valid after the call.
 * @param[in] binding device binding. "U" for UDP, "UQ" for UDP with Queuing.
 * @param[in] secureMode  setting the security (currently implicit PSK)
 * @param[in] testMode    true, to enable test mode for LEDs, false, otherwise
 *
 * @return  RC_OK Interface Initialization was successful.
 * @return  RC_SERVAL_ERROR Initialization Failure.
 */
extern retcode_t Lwm2mInterfaceInitialize(const char* endpointName, const char* macAddr, const char* binding, bool secureMode, bool enableConNotifies, uint8_t testMode);

/**
 * @brief The function Starts the LWM2M Interface to the LWM2M Server to the configured port Number.
 * @param[in] configuration configuration of LWM2M
 *
 * @return  RC_OK Interface Started between Device and LWM2M Server.
 * @return  RC_SERVAL_ERROR Interface Not started to the Configured Port Number.
 */
extern retcode_t Lwm2mInterfaceStart(const Lwm2mConfiguration_T *configuration);

/**
 * @brief Reboot the device.
 *
 * When the function is called first, it reboots with a delay of 5s to have time to send a response.
 * Though reboot is sometimes used for undetermined working devices, the delay of 5 s may not work.
 * Therefore, a second call of this function reboots the device immediately (and without sending a response).
 */
extern void Lwm2mInterfaceReboot(void);

/**
 * @brief Set state of the red LED.

 * Set state of the red LED and reports that to the associated handler.
 * @see Lwm2mInterfaceSetRedLedStateChangeHandler
 *
 * @param[in] on state of LED
 */
extern void Lwm2mInterfaceRedLedSetState(bool on);

/**
 * @brief Set state of the orange LED.
 *
 * Set state of the orange LED and reports that to the associated handler.
 * @see Lwm2mInterfaceSetOrangeLedStateChangeHandler
 *
 * @param[in] on state of LED
 */
extern void Lwm2mInterfaceOrangeLedSetState(bool on);

/**
 * @brief Set state of the yellow LED.
 *
 * Set state of the yellow LED and reports that to the associated handler.
 * @see Lwm2mInterfaceSetOrangeLedStateChangeHandler
 *
 * @param[in] on state of LED
 */
extern void Lwm2mInterfaceYellowLedSetState(bool on);

/**
 * @brief Set change handler for red LED.
 *
 * @param[in] handler callback for LED state changes
 */
extern void Lwm2mInterfaceSetRedLedStateChangeHandler(LedStateChangeHandler_T handler);

/**
 * @brief Set change handler for orange LED.
 *
 * @param[in] handler callback for LED state changes
 */
extern void Lwm2mInterfaceSetOrangeLedStateChangeHandler(LedStateChangeHandler_T handler);

/**
 * @brief Set change handler for yellow LED.
 *
 * @param[in] handler callback for LED state changes
 */
extern void Lwm2mInterfaceSetYellowLedStateChangeHandler(LedStateChangeHandler_T handler);

extern void Lwm2mInterfaceRebootNow(const char *msg);

#endif /* SERVAL_ENABLE_LWM2M */

#endif /* XDK110_APPLWM2MINTERFACE_H_ */
