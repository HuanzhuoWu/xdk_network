/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*
* Contributors:
* Bosch Software Innovations GmbH
*/
/*----------------------------------------------------------------------------*/

/* header definition ******************************************************** */
#ifndef LWM2MCLIENT_H_
#define LWM2MCLIENT_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

#warning Please enter WLAN configurations with valid SSID & WPA key in below Macros and remove this line to avoid warning.
#define WLAN_CONNECT_WPA_SSID           "NETWORK_SSID"        			/**< Macros to define WPA/WPA2 network settings */
#define WLAN_CONNECT_WPA_PASS           "NETWORK_WPA_KEY"	      		/**< Macros to define WPA/WPA2 network settings */

#warning Please enter security mode here (0/1) and remove this line to avoid warning
#define SECURITY_ENABLE 				0			/**< switch on/off PSK based dTLS security  */

#if SECURITY_ENABLE == 0
# ifndef LWM2M_SERVER_IP
#  warning Please enter LWM2M server configuration and remove this line to avoid warning.
#  define LWM2M_SERVER_IP			"coap://leshan.eclipse.org:5683" /**< using standard CoAP ports unsecure 5683 secure 5684, giving an appending port will override */
# endif
#else
# ifndef LWM2M_SECURE_SERVER_IP
#  warning Please enter opt. secure LWM2M server configuration and remove this line to avoid warning.
#  define LWM2M_SECURE_SERVER_IP		"coaps://leshan.eclipse.org:5684" /**< using standard CoAP ports unsecure 5683 secure 5684, giving an appending port will override */
# endif
#endif

#warning Please enter lifetime in seconds. For coaps: lifetime relates to SERVAL_SECURE_SERVER_CONNECTION_TIMEOUT=60s setting! Remove this line to avoid warning.
#define LWM2M_DEFAULT_LIFETIME          UINT32_C(60)            /**< Macro to represent the default lifetime in s. In coaps case this value relates to the SERVAL_SECURE_SERVER_CONNECTION_TIMEOUT setting. */

#define CONNECT_TIME_INTERVAL           UINT32_C(3000)          /**< Macro to represent connect time interval */
#define TIMERBLOCKTIME                  UINT32_MAX              /**< Macro used to define blocktime of a timer*/
#define ZERO                            UINT8_C(0x00)           /**< Macro to define value zero*/
#define ONE                             UINT8_C(0x01)           /**< Macro to define value one*/
#define SERIAL_CONNECT_TIME_INTERVAL    UINT32_C(5000)          /**< Macro to represent connect time interval */
#define TASK_STACK_SIZE                 UINT32_C(2048)          /**< Macro to represent the task stack size */
#define TASK_PRIORITY                   UINT32_C(2)             /**< Macro to represent the task priority */
#define SERIAL_CONNECT_TIME_INTERVAL    UINT32_C(5000)          /**< Macro to represent connect time interval */
/* Server Connecting Port is user configurable */
#define CLIENT_CONNECT_PORT             UINT32_C(12345)
#if SERVAL_ENABLE_TLS_CLIENT
#define DEST_PORT_NUMBER        		UINT16_C(443)            /**< Macro to define the secure Server Port number */
#else
#define DEST_PORT_NUMBER        		UINT16_C(80)            /**< Macro to define the unsecure Server Port number */
#endif

/* local module global variable declarations */

/* local inline function definitions */

#endif /* LWM2MCLIENT_H_ */

/** ************************************************************************* */
