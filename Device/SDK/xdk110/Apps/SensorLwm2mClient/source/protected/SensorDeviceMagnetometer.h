/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 * @see Lwm2mObject_Magnetometer.h
 */

#ifndef SOURCE_SENSORDEVICEMAGNETOMETER_H_
#define SOURCE_SENSORDEVICEMAGNETOMETER_H_

#include "SensorDeviceUtil.h"

/**
 * @brief Initialize sensor.
 * Initialize sensor, setting it active or inactive and initialize the lwm2m object accordingly.
 * Intended to be only called by the sensors timer thread.
 *
 * @param[in] enable true, activate sensor, false, disable sensor
 */
extern void SensorDeviceMagnetometer_Activate(bool enable);

/**
 * @brief Read sensor values and process them.
 * Read the values from the sensor, process them according the provided mode and
 * transfer them to lwm2m objects, when notify indicates that.
 * Intended to be only called by the sensors timer thread.
 * @param[in] mode processing mode
 * @param[in] notify true, transfer values to lwm2m objects, false, no transfer, process only
 */
extern void SensorDeviceMagnetometer_Update(enum ProcessingMode mode, bool notify);

#endif /* SOURCE_SENSORDEVICEMAGNETOMETER_H_ */
