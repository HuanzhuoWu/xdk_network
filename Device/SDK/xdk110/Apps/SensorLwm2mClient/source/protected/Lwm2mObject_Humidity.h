/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_HUMIDITY_H_
#define SOURCE_LWM2MOBJECT_HUMIDITY_H_

#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for humidity.
 */
#define LWM2M_OBJECTID_IPSO_HUMIDITY UINT16_C(3304)

/**
 * @brief LWM2M resource data for humidity.
 */
struct HumidityResource_S
{
    Lwm2mResource_T minMeasuredValue; /**< minimum value */
    Lwm2mResource_T maxMeasuredValue; /**< maximum value */
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T resetMinMaxValues; /**< reset minimum and maximum values */
    Lwm2mResource_T sensorValue; /**< current sensor value */
    Lwm2mResource_T units; /**< textual units, "%" according UCUM */
};

typedef struct HumidityResource_S HumidityResource_T;

/*lint -esym(956, HumidityResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern HumidityResource_T HumidityResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectHumidity_Init(void);

/**
 * @brief Enables the LWM2M object instance
 * to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRangeValue minimum range of possible values in [%]
 * @param[in] maxRangeValue maximum range of possible values in [%]
 */
extern void ObjectHumidity_Enable(float minRangeValue, float maxRangeValue);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectHumidity_Disable(void);

/**
 * @brief  the function set the current measured sensorValue in [%]
 *
 * Will triggers the sending of a notification, if the value was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] sensorValue_prH current sensor value in [%]
 */
extern void ObjectHumidity_SetValue(float sensorValue_prH);

#endif /* SOURCE_LWM2MOBJECT_HUMIDITY_H_ */
