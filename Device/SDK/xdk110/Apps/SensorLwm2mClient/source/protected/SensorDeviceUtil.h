/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef SOURCE_SENSORDEVICE_H_
#define SOURCE_SENSORDEVICE_H_

#include <stdbool.h>

/**
 * @brief Definition of maxiumum values processed per sensor.
 */
#define SENSORDEVICE_MAX_VALUES 3

/**
 * @brief Processing modes for sensor values.
 */
enum ProcessingMode
{
    CURRENT, /**< use current value */
    AVG, /**< use avarage value */
    MIN, /**< use minimum value */
    MAX /**< use maximum value */
};

typedef enum ProcessingMode ProcessingMode_T;
/**
 * @brief Sensor values for processing.
 *
 * Used to pass values from sensors to processing functions.
 */
typedef struct SensorDeviceSampleDataFloat
{
    double values[SENSORDEVICE_MAX_VALUES]; /**< array with seonsor values */
} SensorDeviceSampleDataFloat_T;

/*lint -esym(956, *SensorDeviceData) only to be used by sensor timer task */
/**
 * @brief Processed sensor values.
 */
typedef struct SensorDeviceProcessDataFloat
{
    SensorDeviceSampleDataFloat_T data; /**< processed sensor values */
    int value_counter; /**< number of used values. Must not be larger then SENSORDEVICE_MAX_VALUES */
    int sample_counter; /**< number of samples */
    ProcessingMode_T mode; /**< processing mode */
    bool enabled; /**< indicator for sensor activation */
} SensorDeviceProcessDataFloat_T;

/**
 * @brief reset processed sensor values.
 *
 * Reset minimum, maximum and avarage value by reseting the sample_counter to 0.
 *
 * @param data processed sensor values
 */
extern void SensorDevice_ResetProcessDataFloat(SensorDeviceProcessDataFloat_T* data);

/**
 * @brief process current sensor values.
 *
 * Process current sensor values according process mode and store the result in the processed sensor values.
 * Adjust processed value for minimum, maximum or current mode, add values for avarage mode.
 *
 * @param mode process mode
 * @param data processed sensor values
 * @param sample current sensor values
 */
extern void SensorDevice_ProcessDataFloat(ProcessingMode_T mode, SensorDeviceProcessDataFloat_T* data, SensorDeviceSampleDataFloat_T* sample);

/**
 * @brief get processed sensor values.
 *
 * Get the processed sensor values and reset the processing.
 *
 * @param data processed sensor values
 * @param sample read values
 * @return true, if values are valid, false, otherwise
 */
extern bool SensorDevice_GetDataFloat(SensorDeviceProcessDataFloat_T* data, SensorDeviceSampleDataFloat_T* sample);

#endif /* SOURCE_SENSORDEVICE_H_ */

/** ************************************************************************* */
