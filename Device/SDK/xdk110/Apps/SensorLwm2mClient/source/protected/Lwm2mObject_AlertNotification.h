/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef SOURCE_LWM2MOBJECT_ALERTNOTIFICATION_H_
#define SOURCE_LWM2MOBJECT_ALERTNOTIFICATION_H_

/**
 * @brief LWM2M object id for alert notification.
 * BOSCH propr. in range of 10241 – 32768
 */
#define LWM2M_OBJECTID_ALERTNOTIFICATION 15003

#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M resource data for alert notification.
 */
struct AlertNotificationResource_S
{
    Lwm2mResource_T alert; /**< alert, textual information */
    Lwm2mResource_T resetAlert; /**< reset alert */
};

typedef struct AlertNotificationResource_S AlertNotificationResource_T;

/*lint -esym(956, AlertNotificationResources) mutex used */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern AlertNotificationResource_T AlertNotificationResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectAlertNotification_Init(void);

/**
 * @brief Enable LWM2M object instance to start providing alerts.
 */
extern void ObjectAlertNotification_Enable(void);

/**
 * @brief Set alert.
 *
 * Copies provided string into the alert message.
 *
 * @param[in] alert alert information
 */
extern void ObjectAlertNotification_SetValue(const char* alert);

#endif /* SOURCE_LWM2MOBJECT_ALERTNOTIFICATION_H_ */

/** ************************************************************************* */
