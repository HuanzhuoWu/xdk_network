/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 * @see Lwm2mObject_Temperature.h
 * @see Lwm2mObject_Humidity.h
 * @see Lwm2mObject_Barometer.h
 */

#ifndef SOURCE_SENSORDEVICEENVIRONMENT_H_
#define SOURCE_SENSORDEVICEENVIRONMENT_H_

#include "SensorDeviceUtil.h"

/**
 * @brief Initialize sensor.
 * Initialize sensors, setting them active or inactive.
 * Though the sensor provides three seperate values, the XDK sensor is activated, if at least
 * one sensor is active. The related lwm2m objects are initialized accordingly.
 * Intended to be only called by the sensors timer thread.
 *
 * @param[in] enableTemperature true, activate temperature, false, disable temperature
 * @param[in] enableHumidity true, activate humidity, false, disable humidity
 * @param[in] enablePressure true, activate pressure, false, disable pressure
 */
extern void SensorDeviceEnvironment_Activate(bool enableTemperature, bool enableHumidity, bool enablePressure);

/**
 * @brief Read sensor values and process them.
 * Read the values from the sensor, process them according the provided mode and
 * transfer them to lwm2m objects, when notify indicates that.
 * Intended to be only called by the sensors timer thread.
 *
 * @param[in] mode processing mode
 * @param[in] notify true, transfer values to lwm2m objects, false, no transfer, process only
 */
extern void SensorDeviceEnvironment_Update(enum ProcessingMode mode, bool notify);

#endif /* SOURCE_SENSORDEVICEENVIRONMENT_H_ */
