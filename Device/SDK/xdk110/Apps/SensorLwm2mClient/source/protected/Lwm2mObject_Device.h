/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_PROTECTED_LWM2MOBJECT_DEVICE_H_
#define SOURCE_PROTECTED_LWM2MOBJECT_DEVICE_H_

/* -- LWM2M Object Device ------ */

/**
 * @brief LWM2M object id for device.
 */
#define LWM2M_OBJECTID_DEVICE   	3

/**
 * @brief LWM2M resource data for device.
 */
struct DeviceResource_S
{
    Lwm2mResource_T manufacturer; /* IX=0 */
    Lwm2mResource_T modelNumber; /* IX=1 */
    Lwm2mResource_T serialNumber; /* IX=2 */
    Lwm2mResource_T FirmwareVersion; /* IX=3 */
    Lwm2mResource_T deviceReboot; /* IX=4 */
    Lwm2mResource_T factoryReset; /* IX=5 */
    Lwm2mResource_T availablePowerSources; /* IX=6 */
    Lwm2mResource_T powerSourceVoltage; /* IX=7 */
    Lwm2mResource_T powerSourceCurrent; /* IX=8 */
    Lwm2mResource_T batteryLevel; /* IX=9 */
    Lwm2mResource_T memoryFree; /* IX=10 */
    Lwm2mResource_T errorCode; /* IX=11 */
    Lwm2mResource_T resetErrorCode; /* IX=12 */
    Lwm2mResource_T currentTime; /* IX=13 */
    Lwm2mResource_T UTCOffset; /* IX=14 */
    Lwm2mResource_T Timezone; /* IX=15 */
    Lwm2mResource_T SupportedBindingAndModes; /* IX=16 */
};

typedef struct DeviceResource_S DeviceResource_T;

//lint -esym(956, DeviceResources)
/**
 * @brief LWM2M resource data for instance 0.
 */
extern DeviceResource_T DeviceResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectDevice_Init(void);

/**
 * @brief Enable LWM2M object instance. Start reporting values.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectDevice_Enable(void);

/**
 * @brief notify that the device time has changed.
 * Will send a notification to observers.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectDevice_NotifyTimeChanged(void);

#endif /* SOURCE_PROTECTED_LWM2MOBJECT_DEVICE_H_ */
