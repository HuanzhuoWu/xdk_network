/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef SOURCE_LWM2MOBJECT_SENSORDEVICE_H_
#define SOURCE_LWM2MOBJECT_SENSORDEVICE_H_

/* additional interface header files */
#include <Serval_Exceptions.h>
#include <Serval_Clock.h>
#include <Serval_Lwm2m.h>
/**
 * @brief LWM2M object id for sensor device.
 * BOSCH propr. in range of 10241 – 32768
 */
#define LWM2M_OBJECTID_SENSORDEVICE 15000

/**
 * @brief LWM2M resource data for sensor device.
 */
struct SensorDeviceResource_S
{
    Lwm2mResource_T transportInterval; /**< transport interval for sensor values in [s]*/
    Lwm2mResource_T accelerometerEnabled; /**< accelerometer enable/disable */
    Lwm2mResource_T gyroscopeEnabled; /**< gyroscope enable/disable */
    Lwm2mResource_T humiditySensorEnabled; /**< humidity enable/disable */
    Lwm2mResource_T illuminanceSensorEnabled; /**< illuminance enable/disable */
    Lwm2mResource_T magnetometerEnabled; /**< magnetometer enable/disable */
    Lwm2mResource_T barometerEnabled; /**< barometer/disable */
    Lwm2mResource_T temperatureSensorEnabled; /**< temperature enable/disable */
    Lwm2mResource_T preprocessingMode; /**< preprocessing mode, "current", "min", "max" or "avg" */
    Lwm2mResource_T macAddress; /**< WLAN MAC address of device */
};
typedef struct SensorDeviceResource_S SensorDeviceResource_T;

/*lint -esym(956, SensorDeviceResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern SensorDeviceResource_T SensorDeviceResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 *
 * @param[in] mac textual WLAN MAC address of device. The parameter is used as reference
 *                          therefore the MAC address name must stay valid after the call.
 */
extern void ObjectSensorDevice_Init(const char* mac);

/**
 * @brief Enable LWM2M object instance to start providing sensor device controls.
 * Starts sensors timer to frequently read and process values.
 */
extern void ObjectSensorDevice_Enable(void);

#endif /* SOURCE_LWM2MOBJECT_SENSORDEVICE_H_ */

/** ************************************************************************* */
