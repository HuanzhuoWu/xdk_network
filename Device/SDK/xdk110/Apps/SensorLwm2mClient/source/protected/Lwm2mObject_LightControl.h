/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef SOURCE_LWM2MOBJECT_LIGHTCONTROL_H_
#define SOURCE_LWM2MOBJECT_LIGHTCONTROL_H_

#include <stdbool.h>

/* additional interface header files */
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for light control.
 */
#define LWM2M_OBJECTID_IPSO_LIGHTCONTROL 3311

/**
 * @brief LWM2M resource data for light control.
 */
struct LightControlResource_S
{
    Lwm2mResource_T colour; /**< color of light. */
    Lwm2mResource_T onOff; /**< state of light */
};

typedef struct LightControlResource_S LightControlResource_T;

/*lint -esym(956, Light*Resources) resources are not modified */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern LightControlResource_T LightRedResources;
/**
 * @brief LWM2M resource data for instance 1.
 */
extern LightControlResource_T LightOrangeResources; //lint !e956, resources are not modified
/**
 * @brief LWM2M resource data for instance 2.
 */
extern LightControlResource_T LightYellowResources; //lint !e956, resources are not modified

/**
 * @brief Initialize LWM2M object instances. Must be called before any other function call.
 */
extern void ObjectLightControl_Init(void);

/**
 * @brief Enable LWM2M object instances to start providing light controls.
 */
extern void ObjectLightControl_Enable(bool testMode);

#endif /* SOURCE_LWM2MOBJECT_LIGHTCONTROL_H_ */

/** ************************************************************************* */
