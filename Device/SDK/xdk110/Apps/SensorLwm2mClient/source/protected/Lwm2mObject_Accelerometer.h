/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef SOURCE_LWM2MOBJECT_ACCELEROMETER_H_
#define SOURCE_LWM2MOBJECT_ACCELEROMETER_H_

/* additional interface header files */
#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for accelerometer.
 */
#define LWM2M_OBJECTID_IPSO_ACCELEROMETER 3313

/**
 * @brief LWM2M resource data for accelerometer.
 */
struct AccelerometerResource_S
{
    Lwm2mResource_T minRange; /**< minimum range of possible values */
    Lwm2mResource_T maxRange; /**< maximum range of possible values */
    Lwm2mResource_T units; /**< textual units, "g" according UCUM */
    Lwm2mResource_T xAxis; /**< acceleration x-axis */
    Lwm2mResource_T yAxis; /**< acceleration y-axis */
    Lwm2mResource_T zAxis; /**< acceleration z-axis */
};

typedef struct AccelerometerResource_S AccelerometerResource_T;

/*lint -esym(956, AccelerometerResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern AccelerometerResource_T AccelerometerResources;
/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectAccelerometer_Init(void);

/**
 * @brief Enable LWM2M object instance to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRangeValue minimum range of value in [g].
 * @param[in] maxRangeValue maximum range of value in [g].
 */
extern void ObjectAccelerometer_Enable(float minRangeValue, float maxRangeValue);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectAccelerometer_Disable(void);

/**
 * @brief Set values for x-, y-, and z-axis.
 *
 * Set values for axis, units in [g]. Will triggers the sending of
 * a notification, if a value was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] xAxis acceleration of x-axis in [g]
 * @param[in] yAxis acceleration of y-axis in [g]
 * @param[in] zAxis acceleration of z-axis in [g]
 */
extern void ObjectAccelerometer_SetValues(float xAxis, float yAxis, float zAxis);

#endif /* SOURCE_LWM2MOBJECT_ACCELEROMETER_H_ */

/** ************************************************************************* */
