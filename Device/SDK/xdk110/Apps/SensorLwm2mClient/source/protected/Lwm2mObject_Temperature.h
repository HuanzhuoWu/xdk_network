/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_TEMPERATURE_H_
#define SOURCE_LWM2MOBJECT_TEMPERATURE_H_

#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for temperature.
 */
#define LWM2M_OBJECTID_IPSO_TEMPERATURE 3303

/**
 * @brief LWM2M resource data for temperature.
 */
struct TemperatureResource_S
{
    Lwm2mResource_T minMeasuredValue; /**< minimum value */
    Lwm2mResource_T maxMeasuredValue; /**< maximum value */
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T resetMinMaxValues; /**< reset minimum and maximum values */
    Lwm2mResource_T sensorValue; /**< current sensor value */
    Lwm2mResource_T units; /**< textual units, "Cel" according UCUM */
};

typedef struct TemperatureResource_S TemperatureResource_T;

/*lint -esym(956, TemperatureResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern TemperatureResource_T TemperatureResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectTemperature_Init(void);

/**
 * @brief Enable the LWM2M object instance
 * to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRangeValue_C minimum range of possible values in [Cel]
 * @param[in] maxRangeValue_C maximum range of possible values in [Cel]
 */
extern void ObjectTemperature_Enable(float minRangeValue_C, float maxRangeValue_C);

/**
 * @brief Disable the LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectTemperature_Disable(void);

/**
 * @brief  the function set the current measured sensorValue in [Cel]
 *
 * Will triggers the sending of a notification, if the value was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] sensorValue_C current sensor value in [Cel]
 */
extern void ObjectTemperature_SetValue(float sensorValue_C);

#endif /* SOURCE_LWM2MOBJECT_TEMPERATURE_H_ */
