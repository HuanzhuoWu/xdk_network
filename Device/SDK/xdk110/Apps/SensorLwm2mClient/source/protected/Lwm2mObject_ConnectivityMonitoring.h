/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */

#ifndef SOURCE_LWM2MOBJECT_CONNECTIVITY_MONITORING_H_
#define SOURCE_LWM2MOBJECT_CONNECTIVITY_MONITORING_H_

#include <Serval_Lwm2m.h>

/* -- LWM2M Object Connectivity Monitoring ------ */

/* Connectivity Monitoring Object ID */
#define LWM2M_OBJECTID_CONNECTIVITY_MONITORING	4

struct ConnMonResource_S
{
    Lwm2mResource_T networkBearer; /* IX=0 */
    Lwm2mResource_T availableNetWorkBearer; /* IX=1 */
    Lwm2mResource_T radioSignalStrength; /* IX=2 */
    Lwm2mResource_T ipAddresses; /* IX=3 */
};

typedef struct ConnMonResource_S ConnMonResource_T;

/*lint -esym(956, ConnMonResources) mutex used*/
extern ConnMonResource_T ConnMonResources;

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectConnectivityMonitoring_Init(void);

/**
 * @brief Enable LWM2M object instance. Start reporting values.
 */
extern void ObjectConnectivityMonitoring_Enable(void);

/**
 * @brief Set radio signal strength.
 *
 * @param[in] rss radio signal strength in decibel.
 */
extern void ObjectConnectivityMonitoring_SetRadioSignalStrength(int rss); // 0..64 in dBm in GSM case!

/**
 * @brief Set ip address.
 *
 * @param[in] ipAddr ip address in literal format e.g. "192.168.10.106".
 */
extern void ObjectConnectivityMonitoring_SetIpAddress(const char* ipAddr);

#endif /* SOURCE_LWM2MOBJECT_CONNECTIVITY_MONITORING_H_ */
