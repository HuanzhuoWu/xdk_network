/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 */
#ifndef SOURCE_LWM2MOBJECT_MAGNETOMETER_H_
#define SOURCE_LWM2MOBJECT_MAGNETOMETER_H_

#include <Serval_Lwm2m.h>

/**
 * @brief LWM2M object id for magnetometer.
 */
#define LWM2M_OBJECTID_IPSO_MAGNETOMETER 3314

#ifndef MAGNETOMETER_DECLANATIONANGLE
/** magnetic declanation angle for Waiblingen */
#define MAGNETOMETER_DECLANATIONANGLE 0.0349066
#endif

/**
 * @brief LWM2M resource data for magnetometer.
 */
struct MagnetometerResource_S
{
    Lwm2mResource_T minRangeValue; /**< minimum range of possible values */
    Lwm2mResource_T maxRangeValue; /**< maximum range of possible values */
    Lwm2mResource_T units; /**< textual units, "uT" according UCUM */
    Lwm2mResource_T xValue; /**< value for x-axis */
    Lwm2mResource_T yValue; /**< value for y-axis */
    Lwm2mResource_T zValue; /**< value for z-axis */
    Lwm2mResource_T direction; /**< direction according x-axis and y-axis*/
};

typedef struct MagnetometerResource_S MagnetometerResource_T;

/*lint -esym(956, MagnetometerResources) only accessed by serval scheduler */
/**
 * @brief LWM2M resource data for instance 0.
 */
extern MagnetometerResource_T MagnetometerResources; //lint !e956, only accessed by serval scheduler

/**
 * @brief Initialize LWM2M object instance. Must be called before any other function call.
 */
extern void ObjectMagnetometer_Init(void);

/**
 * @brief Enable LWM2M object instance to start providing sensor data.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] minRangeValue minimum range of value in [uT].
 * @param[in] maxRangeValue maximum range of value in [uT].
 */
extern void ObjectMagnetometer_Enable(float minRangeValue, float maxRangeValue);

/**
 * @brief Disable LWM2M object instance to stop providing sensor data.
 * Sets unit to "" and all other values to 0.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 */
extern void ObjectMagnetometer_Disable(void);

/**
 * @brief Set values for x-, y-, and z-axis.
 *
 * Set values for axis, units in [uT]. Will triggers the sending of
 * a notification, if a values was changed.
 * Function is thread safe, it schedules a job for serval.
 * Execution may therefore be deferred.
 *
 * @param[in] x value of x-axis in [uT]
 * @param[in] y value of y-axis in [uT]
 * @param[in] z value of z-axis in [uT]
 */
extern void ObjectMagnetometer_SetValues(float x, float y, float z);

#endif /* SOURCE_LWM2MOBJECT_MAGNETOMETER_H_ */
