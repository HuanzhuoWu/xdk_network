/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
#include "FotaDownload.h"
#include "BCDS_FotaStorageAgent.h"
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "BCDS_PowerMgt.h"
#include "BCDS_NVMConfig.h"
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include "em_device.h"
#include <core_cm3.h>

/* Put the type and macro definitions here */
#define FOTA_CLIENT_ACTIVE_SESSION_DELAY   2 /* Value in seconds */
/* Put constant and variable definitions here */

static uint32_t DelayTimeInitial = 0;
static uint8_t FotaDelayReq = FALSE;

struct FotaStorageAgent_S * FotaStorageAgentHandle = &SDCardStorage;

/* Put private function declarations here */

/* Put function implementations here */

Retcode_T FotaNotificationcallback(FotaStateMachine_State_T state, FotaStateMachine_MsgNotify_T notifyMsg)
{
    Retcode_T returnCode = RETCODE_OK;

    if (FOTA_STATE_NOTFY == notifyMsg)
    {
        switch (state)
        {
            case FOTA_UPDATING:
                /*Do reboot here*/
                NVIC_SystemReset();
            break;
            default:
            break;
        }
    }
    else
    {
        if (notifyMsg == FOTA_VALID_URI_RECEIVED)
        {

        }
        else if (notifyMsg == FOTA_DELAY_REQUEST)
        {
            FotaDelayReq = TRUE;
            DelayTimeInitial = PowerMgt_GetSystemTime();
        }
    }
    return (returnCode);
}

void FotaInit(void)
{
    Retcode_T ReturnValue = RETCODE_FAILURE;
    uint32_t Retcode= RETCODE_FAILURE;
    ReturnValue = FotaStateMachine_Init(FotaStorageAgentHandle, FotaNotificationcallback);
    if(RETCODE_OK != ReturnValue )
    {
        Retcode = Retcode_getCode(ReturnValue);
        if ((Retcode == RETCODE_FAILURE) || (Retcode == FOTA_FAILED_NVM_READ_CREDENTIALS))
        {
            printf("Fota was in inconsistent state so reset to default state on Foat init \n");
        }
        else
        {
            printf("Fota Init failed \n");
            assert(false);
        }
    }
    else
    {
        printf("Fota Init success \n");
    }
}

void FotaTask(void * pvParameters)
{
    BCDS_UNUSED(pvParameters);
    uint32_t TimeToFotaTask = UINT32_C(0); /**< variable to delay Fota task */
    uint32_t current = 0;

    TimeToFotaTask = xTaskGetTickCount();
    for (;;)
    {
        if (TRUE == FotaDelayReq)
        {
            current = PowerMgt_GetSystemTime();
            if ((current - DelayTimeInitial) < FOTA_CLIENT_ACTIVE_SESSION_DELAY)
            {
                vTaskDelayUntil(&TimeToFotaTask, PowerMgt_GetMsDelayTimeInSystemTicks(FOTA_CLIENT_ACTIVE_SESSION_DELAY *1000));
            }
            else
            {
                FotaDelayReq = FALSE;
            }
        }
        else
        {
            FotaStateMachine_Handler();
            vTaskDelayUntil(&TimeToFotaTask, PowerMgt_GetMsDelayTimeInSystemTicks(FOTA_POLLING_TIME_MS));
        }
    }
}

