/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Accelerometer.h"
#include "SensorDeviceAccelerometer.h"

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

#define ACCELEROMETER_TO_FLOAT(I) ((I) / 1000.0F)

static SensorDeviceProcessDataFloat_T SensorDeviceData = { { { 0 } }, 3, 0, CURRENT, false };

void SensorDeviceAccelerometer_Activate(bool enable)
{
    if (SensorDeviceData.enabled == enable)
        return;
    SensorDeviceData.enabled = enable;
    SensorDevice_ResetProcessDataFloat(&SensorDeviceData);
    if (enable)
    {
        float ValueRange = 0.0F;
        Accelerometer_Range_T SensorRange;
        Retcode_T AccelReturnValue = Accelerometer_init(xdkAccelerometers_BMA280_Handle);
        if (RETCODE_OK != AccelReturnValue)
            return;

        AccelReturnValue = Accelerometer_getRange(xdkAccelerometers_BMA280_Handle, &SensorRange);
        if (RETCODE_OK != AccelReturnValue)
            return;

        switch (SensorRange)
        {
        case ACCELEROMETER_BMA280_RANGE_2G:
            case ACCELEROMETER_BMI160_RANGE_2G:
            ValueRange = 2.0F;
            break;
        case ACCELEROMETER_BMA280_RANGE_4G:
            case ACCELEROMETER_BMI160_RANGE_4G:
            ValueRange = 4.0F;
            break;
        case ACCELEROMETER_BMA280_RANGE_8G:
            case ACCELEROMETER_BMI160_RANGE_8G:
            ValueRange = 8.0F;
            break;
        case ACCELEROMETER_BMA280_RANGE_16G:
            case ACCELEROMETER_BMI160_RANGE_16G:
            ValueRange = 16.0F;
            break;
        default:
            break;
        }
        ObjectAccelerometer_Enable(-ValueRange, ValueRange);
    }
    else
    {
        if (RETCODE_OK != Accelerometer_deInit(xdkAccelerometers_BMA280_Handle))
        {
            printf("Failed to disable the accelerometer\r\n");
        }
        ObjectAccelerometer_Disable();
    }
}

void SensorDeviceAccelerometer_Update(enum ProcessingMode mode, bool notify)
{
    if (!SensorDeviceData.enabled)
        return;

    SensorDeviceSampleDataFloat_T Sample;
    Accelerometer_XyzData_T GetAccelData = { INT32_C(0), INT32_C(0), INT32_C(0) };

    Retcode_T accelReturnValue = Accelerometer_readXyzGValue(xdkAccelerometers_BMA280_Handle, &GetAccelData);
    if (RETCODE_OK == accelReturnValue)
    {
        Sample.values[0] = ACCELEROMETER_TO_FLOAT(GetAccelData.xAxisData);
        Sample.values[1] = ACCELEROMETER_TO_FLOAT(GetAccelData.yAxisData);
        Sample.values[2] = ACCELEROMETER_TO_FLOAT(GetAccelData.zAxisData);
        SensorDevice_ProcessDataFloat(mode, &SensorDeviceData, &Sample);
    }
    if (notify)
    {
        if (SensorDevice_GetDataFloat(&SensorDeviceData, &Sample))
            ObjectAccelerometer_SetValues((float) Sample.values[0], (float) Sample.values[1], (float) Sample.values[2]);
    }
}
