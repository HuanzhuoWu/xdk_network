/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObjects.h"
#include "Lwm2mInterface.h"
#include "SntpTime.h"

/* additional interface header files */
#include <Serval_Exceptions.h>
#include <Serval_Log.h>
#include <em_system.h>

#define LOG_MODULE "DVO" /**< serval logging prefix */

/* global variables ********************************************************* */

/* constant definitions ***************************************************** */
#define LWM2MOBJECTS_UDP_LENGTH                 UINT32_C(1)
#define LWM2MOBJECTS_UDP_QUEUED_LENGTH          UINT32_C(2)
#define LWM2MOBJECTS_SMS_LENGTH                 UINT32_C(1)
#define LWM2MOBJECTS_SMS_QUEUED_LENGTH          UINT32_C(2)
#define LWM2MOBJECTS_UDP_AND_SMS_LENGTH         UINT32_C(2)
#define LWM2MOBJECTS_UDP_QUEUED_AND_SMS_LENGTH  UINT32_C(3)

#define DEVICE_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(DeviceResources, res)

#define POWER_SOURCES_COUNT		    			2

/* local variables ********************************************************* */

/*lint -esym(956,tz) */
static char tz[20] = "Europe/Berlin";
/*lint -e(956) */
static int error_codes_count = 1;
/*lint -esym(956,error_codes) */
static int32_t error_codes[5] = { 0, 0, 0, 0, 0 };
/*lint -esym(956,power_sources) */
static int32_t power_sources[POWER_SOURCES_COUNT] = { 1, 5 };
/*lint -esym(956,power_sources_voltage) */
static int32_t power_sources_voltage[POWER_SOURCES_COUNT] = { 3700, 5000 };
/*lint -esym(956,power_sources_current) */
static int32_t power_sources_current[POWER_SOURCES_COUNT] = { 200, 500 };
static volatile bool started = false;

/* local functions ********************************************************** */
/* @brief
 * This function is used to update the errorCode Resource value into the LWM2M Server*
 */
static retcode_t util_serialize_array(Lwm2mSerializer_T *serializer_ptr, int count, int32_t values[])
{
    int index;

    if (serializer_ptr == NULL)
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    retcode_t rc = Lwm2mSerializer_startSerializingResourceArray(serializer_ptr);
    if (rc != RC_OK)
        return rc;

    for (index = 0; index < count; ++index)
    {
        rc = Lwm2mSerializer_setResourceInstanceId(serializer_ptr, index);
        if (rc != RC_OK)
            return rc;

        rc = Lwm2mSerializer_serializeInt(serializer_ptr, values[index]);
        if (rc != RC_OK)
            return rc;
    }

    return Lwm2mSerializer_endSerializingResourceArray(serializer_ptr);
}

/* @brief
 * This function is used to set the device serial number resource with the XDK CoreID in the LWM2M Server*
 */
static retcode_t serialNumber_RO(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }
    char coreID[17]; // max. 16 hex digits (uint64_t)
    sprintf(coreID, "%llX", SYSTEM_GetUnique());
#ifdef USE_LOG_FOR_DEVICE
    printf("serialNumber (coreID): %s\r\n", coreID);
#endif
    StringDescr_T strDescr_sn;
    StringDescr_set(&strDescr_sn, coreID, strlen(coreID));
    return (Lwm2mSerializer_serializeString(serializer_ptr, &strDescr_sn));
}
/* @brief
 * This function is used to update the deviceReboot Resource value into the LWM2M Server*
 */
static retcode_t deviceRebootFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

    printf("deviceReboot\r\n");
    Lwm2mInterfaceReboot();
    return (RC_OK);
}
/* @brief
 * This function is used to update the factoryReset Resource value into the LWM2M Server*
 */
static retcode_t factoryResetFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

    printf("factoryReset\r\n");
    return (RC_OK);
}
/* @brief
 * This function is used to update the errorCode Resource value into the LWM2M Server*
 */
static retcode_t availablePowerSources(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
#ifdef USE_LOG_FOR_DEVICE
    printf("availablePowerSource\r\n");
#endif
    return util_serialize_array(serializer_ptr, POWER_SOURCES_COUNT, power_sources);
}
/* @brief
 * This function is used to update the errorCode Resource value into the LWM2M Server*
 */
static retcode_t powerSourcesVoltage(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
#ifdef USE_LOG_FOR_DEVICE
    printf("powerSourcesVoltage\r\n");
#endif
    return util_serialize_array(serializer_ptr, POWER_SOURCES_COUNT, power_sources_voltage);
}
/* @brief
 * This function is used to update the errorCode Resource value into the LWM2M Server*
 */
static retcode_t powerSourcesCurrent(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
#ifdef USE_LOG_FOR_DEVICE
    printf("powerSourcesCurrent\r\n");
#endif
    return util_serialize_array(serializer_ptr, POWER_SOURCES_COUNT, power_sources_current);
}

/* @brief
 * This function is used to update the batteryLevel Resource value into the LWM2M Server*
 */
static retcode_t batteryLevelFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }
#ifdef USE_LOG_FOR_DEVICE
    printf("batteryLevel\r\n");
#endif
    return (Lwm2mSerializer_serializeInt(serializer_ptr, 100));
}
/* @brief
 * This function is used to update the memoryFree Resource value into the LWM2M Server*
 */
static retcode_t memoryFreeFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }

    size_t heapBytes = xPortGetFreeHeapSize();
#ifdef USE_LOG_FOR_DEVICE
    printf("memoryFree %u\r\n", (unsigned) heapBytes);
#endif
    return (Lwm2mSerializer_serializeInt(serializer_ptr, heapBytes / 1024)); // kB!
}

/* @brief
 * This function is used to update the errorCode Resource value into the LWM2M Server*
 */
static retcode_t errorCodeFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
#ifdef USE_LOG_FOR_DEVICE
    printf("errorCode\r\n");
#endif
    return util_serialize_array(serializer_ptr, error_codes_count, error_codes);
}
/* @brief
 * This function is used to update the resetErrorCode Resource value into the LWM2M Server*
 */
static retcode_t resetErrorCodeFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

#ifdef USE_LOG_FOR_DEVICE
    printf("resetErrorCode\r\n");
#endif
    error_codes_count = 1;
    error_codes[0] = 0;

    if (started)
    {
        Lwm2m_URI_Path_T errorCodeUriPath = { OBJECTS_IX_DEVICE_0, OBJECTS_IX_DEVICE_0, DEVICE_RESOURCES_INDEX(errorCode) };
        Lwm2mReporting_resourceChanged(&errorCodeUriPath);
    }

    return (RC_OK);
}

/* @brief
 * This function is used to update the currentTime Resource value into the LWM2M Server*
 */
static retcode_t getCurrentTime(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    retcode_t rc = RC_OK;

    if (parser_ptr != NULL)
    {
        /* Update current time */
        int32_t newTime;
        rc = Lwm2mParser_getTime(parser_ptr, &newTime);
        if (rc != RC_OK)
        {
            return (rc);
        }

        SetUtcTime(newTime);

        /* URI values from array by index:              /<ObjectId>		    /<ObjectId-Inst>	 /<ResourceId> */
        if (started)
        {
            Lwm2m_URI_Path_T currentTimeUriPath = { OBJECTS_IX_DEVICE_0, OBJECTS_IX_DEVICE_0, DEVICE_RESOURCES_INDEX(currentTime) };
            Lwm2mReporting_resourceChanged(&currentTimeUriPath);
        }
        printf("currentTime adjusted\r\n");
        return (RC_OK);
    }

#ifdef USE_LOG_FOR_DEVICE
    printf("currentTime\r\n");
#endif
    return (Lwm2mSerializer_serializeTime(serializer_ptr, GetUtcTime()));
}

/* @brief
 * This function is used to update the tzone Resource value into the LWM2M Server*
 */
static retcode_t getTimeZone(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    StringDescr_T strDescr_tz;

#ifdef USE_LOG_FOR_DEVICE
    printf("tzone\r\n");
#endif
    if (parser_ptr == NULL)
    {
        StringDescr_set(&strDescr_tz, (char*) tz, strlen(tz));
        return (Lwm2mSerializer_serializeString(serializer_ptr, &strDescr_tz));
    }
    else
    {
        retcode_t rc;

        rc = Lwm2mParser_getString(parser_ptr, &strDescr_tz);
        if (rc != RC_OK)
        {
            return (rc);
        }
        if ((0 > strDescr_tz.length) || ((uint32_t) strDescr_tz.length >= sizeof(tz)))
        {
            return (RC_LWM2M_INTERNAL_ERROR);
        }
        strncpy(tz, strDescr_tz.start, strDescr_tz.length);
        tz[strDescr_tz.length] = '\0';
        if (started)
        {
            Lwm2m_URI_Path_T timeZoneUriPath = { OBJECTS_IX_DEVICE_0, OBJECTS_IX_DEVICE_0, DEVICE_RESOURCES_INDEX(Timezone) };
            Lwm2mReporting_resourceChanged(&timeZoneUriPath);
        }
        return (RC_OK);
    }
}
/* @brief
 * This function is used to update the getBinding Resource value into the LWM2M Server*
 */
static retcode_t getBindingFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }

    char* binding = "";
    uint8_t length = 0;

#ifdef USE_LOG_FOR_DEVICE
    printf("getBinding\r\n");
#endif
    switch (DeviceResourceInfo.binding)
    {
    case UDP:
        binding = "U";
        length = LWM2MOBJECTS_UDP_LENGTH;
        break;

    case UDP_QUEUED:
        binding = "UQ";
        length = LWM2MOBJECTS_UDP_QUEUED_LENGTH;
        break;

    case SMS:
        binding = "S";
        length = LWM2MOBJECTS_SMS_LENGTH;
        break;

    case SMS_QUEUED:
        binding = "SQ";
        length = LWM2MOBJECTS_SMS_QUEUED_LENGTH;
        break;

    case UDP_AND_SMS:
        binding = "US";
        length = LWM2MOBJECTS_UDP_AND_SMS_LENGTH;
        break;

    case UDP_QUEUED_AND_SMS:
        binding = "UQS";
        length = LWM2MOBJECTS_UDP_QUEUED_AND_SMS_LENGTH;
        break;
    }

    StringDescr_T strDescr;

    StringDescr_set(&strDescr, binding, length);

    return (Lwm2mSerializer_serializeString(serializer_ptr, &strDescr));
}
/* TODO need a way to define inactive resources. */
DeviceResource_T DeviceResources =
        { //   R-ID!
        { 0, LWM2M_STRING_RO( "Bosch BCDS" ) },
                { 1, LWM2M_STRING_RO( "XDK110" ) },
                { 2, LWM2M_DYNAMIC( serialNumber_RO ) | LWM2M_READ_ONLY  },
                { 3, LWM2M_STRING_RO( "BCX-1.1.0" ) },
                { 4, LWM2M_FUNCTION( deviceRebootFunc ) },
                { 5, LWM2M_FUNCTION( factoryResetFunc ) },
                { 6, LWM2M_DYNAMIC_ARRAY( availablePowerSources ) | LWM2M_READ_ONLY },
                { 7, LWM2M_DYNAMIC_ARRAY( powerSourcesVoltage ) | LWM2M_READ_ONLY },
                { 8, LWM2M_DYNAMIC_ARRAY( powerSourcesCurrent ) | LWM2M_READ_ONLY },
                { 9, LWM2M_DYNAMIC( batteryLevelFunc ) | LWM2M_READ_ONLY },
                { 10, LWM2M_DYNAMIC( memoryFreeFunc ) | LWM2M_READ_ONLY },
                { 11, LWM2M_DYNAMIC_ARRAY( errorCodeFunc ) | LWM2M_READ_ONLY },
                { 12, LWM2M_FUNCTION( resetErrorCodeFunc ) },
                { 13, LWM2M_DYNAMIC( getCurrentTime ) },
                { 14, LWM2M_STRING_RO( "UTC+2" ) },
                { 15, LWM2M_DYNAMIC( getTimeZone ) },
                { 16, LWM2M_DYNAMIC( getBindingFunc ) | LWM2M_READ_ONLY },
                };

/* Here URI_Path is the path points to the "Current Time" Resource */

void ObjectDevice_Init(void)
{
    started = false;
}

void ObjectDevice_Enable(void)
{
    started = true;
}

void ObjectDevice_NotifyTimeChanged(void)
{
    if (started)
    {
        Lwm2m_URI_Path_T currentTimeUriPath = { OBJECTS_IX_DEVICE_0, OBJECTS_IX_DEVICE_0, DEVICE_RESOURCES_INDEX(currentTime) };
        retcode_t rc = Lwm2mReporting_resourceChanged(&currentTimeUriPath);
        if (RC_OK != rc && RC_COAP_SERVER_SESSION_ALREADY_ACTIVE != rc)
        {
            LOG_DEBUG("Could not send time notification " RC_RESOLVE_FORMAT_STR, RC_RESOLVE(rc));
        }
    }
}
