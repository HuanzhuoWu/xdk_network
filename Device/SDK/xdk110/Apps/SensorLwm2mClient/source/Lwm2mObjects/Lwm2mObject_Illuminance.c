/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObjects.h"
#include "Lwm2mUtil.h"
#include "Lwm2mInterface.h"

/* system header files */
#include "BCDS_Basics.h"

/* additional interface header files */
#include <Serval_Exceptions.h>

#define ILLUMINANCE_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(IlluminanceResources, res)

#define FLUSH_RESOURCES	Lwm2mReporting_multipleResourcesChanged(&IlluminanceUriPath, 6, \
			ILLUMINANCE_RESOURCES_INDEX(minValue),\
			ILLUMINANCE_RESOURCES_INDEX(maxValue),\
			ILLUMINANCE_RESOURCES_INDEX(units),\
			ILLUMINANCE_RESOURCES_INDEX(value),\
			ILLUMINANCE_RESOURCES_INDEX(minRangeValue),\
			ILLUMINANCE_RESOURCES_INDEX(maxRangeValue))

static retcode_t ResetMinMax(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr);
static void ObjectIlluminance_InternalEnable(float minRange, float maxRange);
static void ObjectIlluminance_InternalDisable(void);
static void ObjectIlluminance_InternalSetValue(float currentValue);

IlluminanceResource_T IlluminanceResources =
        {
                { 5601, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // minValue			|R|Single|M|Float |	  |
                { 5602, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // maxValue			|R|Single|O|Float |   |
                { 5603, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // minRangeValue	|R|Single|O|Float |   |
                { 5604, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // minRangeValue	|R|Single|O|Float |   |
                { 5605, LWM2M_FUNCTION( ResetMinMax ) }, // reset			|R|Single|O|Float |   |
                { 5700, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // Value			|R|Single|O|Float |   |
                { 5701, LWM2M_STRING_RO("") }, // unit 			|R|Single|O|String|   |
                };

/*lint -e(956) */
static bool MinMaxInit = false;

/*lint -e(956) const ?*/
static Lwm2m_URI_Path_T IlluminanceUriPath = { OBJECTS_IX_ILLUMINANCE_0, OBJECTS_IX_ILLUMINANCE_0, -1 };

static Lwm2m_Single_Resource_Update_T AsyncCall_Updater = { .set_single = ObjectIlluminance_InternalSetValue, .mutex = LWM2M_MUTEX_INIT_VALUE };
static Lwm2m_Pair_Resource_Update_T AsyncCall_Enabler = { .set_pair = ObjectIlluminance_InternalEnable, .mutex = LWM2M_MUTEX_INIT_VALUE };
static Lwm2m_Call_T AsyncCall_Disabler = { .call = ObjectIlluminance_InternalDisable };
static volatile bool Started = false;

static retcode_t ResetMinMax(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, minValue, IlluminanceResources.value.data.f);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, maxValue, IlluminanceResources.value.data.f);
    if (Started)
        LWM2M_DYNAMIC_CHANGES_REPORT(changes, IlluminanceUriPath);

    return (RC_OK);
}

static void ObjectIlluminance_InternalEnable(float minRange, float maxRange)
{
    Started = true;
    IlluminanceResources.minValue.data.f = minRange;
    IlluminanceResources.maxValue.data.f = minRange;
    IlluminanceResources.minRangeValue.data.f = minRange;
    IlluminanceResources.maxRangeValue.data.f = maxRange;
    IlluminanceResources.value.data.f = minRange;
    IlluminanceResources.units.data.s = "mlx";
    MinMaxInit = false;
    FLUSH_RESOURCES;
}

static void ObjectIlluminance_InternalDisable(void)
{
    Started = true;
    IlluminanceResources.minValue.data.f = 0.0F;
    IlluminanceResources.maxValue.data.f = 0.0F;
    IlluminanceResources.minRangeValue.data.f = 0.0F;
    IlluminanceResources.maxRangeValue.data.f = 0.0F;
    IlluminanceResources.value.data.f = 0.0F;
    IlluminanceResources.units.data.s = "";
    MinMaxInit = false;
    FLUSH_RESOURCES;
}

static void ObjectIlluminance_InternalSetValue(float currentValue)
{
    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    if (MinMaxInit)
    {
        if (LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, value, currentValue))
        {
            LWM2M_DYNAMIC_CHANGES_SET_FLOAT_MIN_VALUE(changes, IlluminanceResources, minValue, currentValue);
            LWM2M_DYNAMIC_CHANGES_SET_FLOAT_MAX_VALUE(changes, IlluminanceResources, maxValue, currentValue);
            if (Started)
                LWM2M_DYNAMIC_CHANGES_REPORT(changes, IlluminanceUriPath);
        }
    }
    else
    {
        MinMaxInit = true;
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, value, currentValue);
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, minValue, currentValue);
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, IlluminanceResources, maxValue, currentValue);
        if (Started)
            LWM2M_DYNAMIC_CHANGES_REPORT(changes, IlluminanceUriPath);
    }
}

/* global functions ********************************************************* */

void ObjectIlluminance_Init(void)
{
    Started = false;
    LWM2M_MUTEX_CREATE(AsyncCall_Updater.mutex);
    AsyncCall_Enabler.mutex = AsyncCall_Updater.mutex;
}

void ObjectIlluminance_Enable(float minRange, float maxRange)
{
    Lwm2mUtil_UpdatePairResources(minRange, maxRange, &AsyncCall_Enabler);
}

void ObjectIlluminance_Disable(void)
{
    Lwm2mUtil_Schedule(&AsyncCall_Disabler);
}

void ObjectIlluminance_SetValue(float currentValue)
{
    Lwm2mUtil_UpdateSingleResource(currentValue, &AsyncCall_Updater);
}
