/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObjects.h"
#include "Lwm2mUtil.h"
#include "Lwm2mInterface.h"

/* additional interface header files */
#include <Serval_Exceptions.h>

#define GYROSCOPE_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(GyroscopeResources, res)

#define FLUSH_RESOURCES	Lwm2mReporting_multipleResourcesChanged(&GyroscopeUriPath, 6, \
			GYROSCOPE_RESOURCES_INDEX(minRangeValue),\
			GYROSCOPE_RESOURCES_INDEX(maxRangeValue),\
			GYROSCOPE_RESOURCES_INDEX(units),\
			GYROSCOPE_RESOURCES_INDEX(xValue),\
			GYROSCOPE_RESOURCES_INDEX(yValue),\
			GYROSCOPE_RESOURCES_INDEX(zValue))

static void ObjectGyroscope_InternalEnable(float minRangeValue, float maxRangeValue);
static void ObjectGyroscope_InternalDisable(void);
static void ObjectGyroscope_InternalSetValues(float xValue, float yValue, float zValue);

/*lint -e(956) const ?*/
static Lwm2m_URI_Path_T GyroscopeUriPath = { OBJECTS_IX_GYROSCOPE_0, OBJECTS_IX_GYROSCOPE_0, -1 };

static Lwm2m_Pair_Resource_Update_T AsyncCall_Enabler = { .set_pair = ObjectGyroscope_InternalEnable, .mutex = LWM2M_MUTEX_INIT_VALUE };
static Lwm2m_Call_T AsyncCall_Disabler = { .call = ObjectGyroscope_InternalDisable };
static Lwm2m_Tripple_Resource_Update_T AsyncCall_Updater = { .set_tripple = ObjectGyroscope_InternalSetValues, .mutex = LWM2M_MUTEX_INIT_VALUE };
static volatile bool Started = false;

/* TODO need a way to define inactive resources. */
GyroscopeResource_T GyroscopeResources =
        {
                { 5603, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // minRangeValue|R|Single|O|Float |   |
                { 5604, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // minRangeValue|R|Single|O|Float |   |
                { 5701, LWM2M_STRING_RO("") }, // units        |R|Single|O|String|   |
                { 5702, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // xValue 		|R|Single|M|Float |   |
                { 5703, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // yValue       |R|Single|O|Float |   | Optional really?
                { 5704, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY }, // zValue       |R|Single|O|Float |   | Optional really?
        };

static void ObjectGyroscope_InternalEnable(float minRangeValue, float maxRangeValue)
{
    Started = true;
    GyroscopeResources.minRangeValue.data.f = minRangeValue;
    GyroscopeResources.maxRangeValue.data.f = maxRangeValue;
    GyroscopeResources.units.data.s = "Deg/s";
    GyroscopeResources.xValue.data.f = minRangeValue;
    GyroscopeResources.yValue.data.f = minRangeValue;
    GyroscopeResources.zValue.data.f = minRangeValue;

    FLUSH_RESOURCES;
}

static void ObjectGyroscope_InternalDisable()
{
    Started = true;
    GyroscopeResources.minRangeValue.data.f = 0.0F;
    GyroscopeResources.maxRangeValue.data.f = 0.0F;
    GyroscopeResources.units.data.s = "";
    GyroscopeResources.xValue.data.f = 0.0F;
    GyroscopeResources.yValue.data.f = 0.0F;
    GyroscopeResources.zValue.data.f = 0.0F;
    FLUSH_RESOURCES;
}

static void ObjectGyroscope_InternalSetValues(float xValue, float yValue, float zValue)
{
    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, GyroscopeResources, xValue, xValue);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, GyroscopeResources, yValue, yValue);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, GyroscopeResources, zValue, zValue);
    if (Started)
        LWM2M_DYNAMIC_CHANGES_REPORT(changes, GyroscopeUriPath);
}

/* global functions ********************************************************* */
void ObjectGyroscope_Init(void)
{
    Started = false;
    LWM2M_MUTEX_CREATE(AsyncCall_Updater.mutex);
    AsyncCall_Enabler.mutex = AsyncCall_Updater.mutex;
}

void ObjectGyroscope_Enable(float minRangeValue, float maxRangeValue)
{
    Lwm2mUtil_UpdatePairResources(minRangeValue, maxRangeValue, &AsyncCall_Enabler);
}

void ObjectGyroscope_Disable()
{
    Lwm2mUtil_Schedule(&AsyncCall_Disabler);
}

void ObjectGyroscope_SetValues(float xValue, float yValue, float zValue)
{
    Lwm2mUtil_UpdateTrippleResources(xValue, yValue, zValue, &AsyncCall_Updater);
}
