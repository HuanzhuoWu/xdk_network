/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObjects.h"
#include "Lwm2mInterface.h"
#include "Lwm2mUtil.h"

/* additional interface header files */
#include <Serval_Exceptions.h>

/* global variables ********************************************************* */

/* constant definitions ***************************************************** */
#define CONN_MON_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(ConnMonResources, res)

/* impl. specific see: Lwm2mObject_Device.h: DeviceResource_S */
#define NETWORK_BEARER_COUNT	1
#define IP_ADDRESSES_COUNT		1

/* local variables ********************************************************* */
/*lint -esym(956, NetworkBearer) const ?*/
static int32_t NetworkBearer[NETWORK_BEARER_COUNT] = { 21 };

/*lint -esym(956, IpAddresses) mutex used*/
static char IpAddresses[IP_ADDRESSES_COUNT][16] = { "" }; /* xxx.xxx.xxx.xxx 4*3+3+1=16 max. */
static LWM2M_MUTEX_INSTANCE(Mutex) = LWM2M_MUTEX_INIT_VALUE;
static volatile bool Started = false;

/* local functions ********************************************************** */
/* @brief
 * This function is used to return the available network Bearer array
 */
static retcode_t GetAvailableNetworkBearer(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }
    else
    {
//        printf("availableNetworkBearer\r\n");
        retcode_t
        rc = Lwm2mSerializer_startSerializingResourceArray(serializer_ptr);
        if (rc != RC_OK)
            return rc;
        rc = Lwm2mSerializer_setResourceInstanceId(serializer_ptr, 0); //index=0
        if (rc != RC_OK)
            return rc;
        rc = Lwm2mSerializer_serializeInt(serializer_ptr, NetworkBearer[0]); //index=0
        if (rc != RC_OK)
            return rc;
        return Lwm2mSerializer_endSerializingResourceArray(serializer_ptr);
    }
}
/* @brief
 * This function is used to update the return the current IP address.
 */
static retcode_t GetIpAddresses(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (parser_ptr != NULL)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }
    else
    {
//        printf("ipAddresses\r\n");
        retcode_t
        rc = Lwm2mSerializer_startSerializingResourceArray(serializer_ptr);
        if (rc != RC_OK)
            return rc;
        rc = Lwm2mSerializer_setResourceInstanceId(serializer_ptr, 0); //index=0
        if (rc != RC_OK)
            return rc;
        StringDescr_T strDes;
        if (LWM2M_MUTEX_LOCK(Mutex))
        {
            StringDescr_set(&strDes, IpAddresses[0], strlen(IpAddresses[0])); //index=0
            rc = Lwm2mSerializer_serializeString(serializer_ptr, &strDes);
            LWM2M_MUTEX_UNLOCK(Mutex);
        }
        if (rc != RC_OK)
            return rc;
        return Lwm2mSerializer_endSerializingResourceArray(serializer_ptr);
    }
}
/* global functions */

void ObjectConnectivityMonitoring_Init(void)
{
    Started = false;
    NetworkBearer[0] = 21;
    IpAddresses[0][0] = 0;
    IpAddresses[0][sizeof(IpAddresses[0]) - 1] = 0;
    ConnMonResources.radioSignalStrength.data.i = 0;
    LWM2M_MUTEX_CREATE(Mutex);
}

void ObjectConnectivityMonitoring_Enable(void)
{
    Started = true;
}

void ObjectConnectivityMonitoring_SetRadioSignalStrength(int rss)
{ // 0..64 in dBm in GSM case!
    if (ConnMonResources.radioSignalStrength.data.i != rss)
    {
        ConnMonResources.radioSignalStrength.data.i = rss;
        if (Started)
        {
            Lwm2m_URI_Path_T rscUriPath = { OBJECTS_IX_CONN_MON_0, OBJECTS_IX_CONN_MON_0,
                    CONN_MON_RESOURCES_INDEX(radioSignalStrength) };
            Lwm2mReporting_resourceChanged(&rscUriPath);
        }
    }
}
void ObjectConnectivityMonitoring_SetIpAddress(const char* ipAddr)
{
    if (strcmp(IpAddresses[0], ipAddr) != 0)
    {
        if (LWM2M_MUTEX_LOCK(Mutex))
        {
            strncpy(IpAddresses[0], ipAddr, (sizeof(IpAddresses[0]) - 1));
            IpAddresses[0][sizeof(IpAddresses[0]) - 1] = 0;
            LWM2M_MUTEX_UNLOCK(Mutex);
            if (Started)
            {
                Lwm2m_URI_Path_T rscUriPath = { OBJECTS_IX_CONN_MON_0, OBJECTS_IX_CONN_MON_0,
                        CONN_MON_RESOURCES_INDEX(ipAddresses) };
                Lwm2mReporting_resourceChanged(&rscUriPath);
            }
        }
    }
}

ConnMonResource_T ConnMonResources =
        {
                { 0, LWM2M_INTEGER(21) | LWM2M_READ_ONLY }, // minRangeValue|R|Single|M|Int   |   |
                { 1, LWM2M_DYNAMIC_ARRAY(GetAvailableNetworkBearer) | LWM2M_READ_ONLY  }, // avaNetwBearer|R|Multi |M|Int   |   |
                { 2, LWM2M_INTEGER(0) | LWM2M_READ_ONLY }, // RadioSigStrn.|R|Single|M|Int   |   | dBm |
                { 4, LWM2M_DYNAMIC_ARRAY(GetIpAddresses)| LWM2M_READ_ONLY  }, // ipAddresses 	|R|Multi |M|String|   |
                };
