/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/* own header files */
#include "SensorDeviceAccelerometer.h"
#include "SensorDeviceEnvironment.h"
#include "SensorDeviceGyroscope.h"
#include "SensorDeviceMagnetometer.h"
#include "SensorDeviceIlluminance.h"
#include "Lwm2mObject_SensorDevice.h"
#include "Lwm2mObjects.h"
#include "Lwm2mUtil.h"

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Clock.h>
#include <Serval_Lwm2m.h>

#define DEFAULT_SENSORS_TRANSPORT_INTERVAL UINT32_C(2000)
#define DEFAULT_SENSORS_MODE CURRENT

#define SENSORS_READ_INTERVAL UINT32_C(100)

#define MAX_MODE_LENGTH UINT8_C(8)
#define MAC_LENGTH      UINT8_C(18)

#define SENSORDEVICE_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(SensorDeviceResources, res)

static retcode_t Interval(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr);
static retcode_t PreprocessingMode(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr);

SensorDeviceResource_T SensorDeviceResources =
        {
                { 0, LWM2M_DYNAMIC(Interval) },
                { 1, LWM2M_BOOL(true) },
                { 2, LWM2M_BOOL(true) },
                { 3, LWM2M_BOOL(true) },
                { 4, LWM2M_BOOL(true) },
                { 5, LWM2M_BOOL(true) },
                { 6, LWM2M_BOOL(true) },
                { 7, LWM2M_BOOL(true) },
                { 8, LWM2M_DYNAMIC(PreprocessingMode) },
                { 9, LWM2M_STRING_RO("") },
                };

/*lint -e(956) only accessed by timer */
static uint64_t LastTransportTime = 0; /** time of last sending information */
/*lint -e(956) only accessed by timer */
static int32_t LastTransportInterval = DEFAULT_SENSORS_TRANSPORT_INTERVAL; /** last time for sending sensor values in ms */
/*lint -e(956) only accessed by timer */
static ProcessingMode_T LastMode = DEFAULT_SENSORS_MODE;

/*lint -e(956) mutex used */
static int32_t TransportInterval = DEFAULT_SENSORS_TRANSPORT_INTERVAL; /** time for sending sensor values in ms */
/*lint -e(956) mutex used */
static ProcessingMode_T Mode = DEFAULT_SENSORS_MODE;

/*lint -e(956) */
static xTimerHandle TimerHandle = NULL;/**< Timer handle for periodically reading sensor values */

static LWM2M_MUTEX_INSTANCE(Mutex) = LWM2M_MUTEX_INIT_VALUE;
static volatile bool Started = false;

static void SensorTimer(xTimerHandle xTimer)
{
    uint64_t Millis;
    bool Notify = false;
    bool Start = false;
    int32_t CurrentTransportInterval = LastTransportInterval;
    ProcessingMode_T CurrentMode = LastMode;

    (void) xTimer;
    Clock_getTimeMillis(&Millis);

    if (LWM2M_MUTEX_LOCK_TIMEOUT(Mutex, 50))
    {
        CurrentTransportInterval = TransportInterval;
        CurrentMode = Mode;
        LWM2M_MUTEX_UNLOCK(Mutex);
    }

    if (LastMode != CurrentMode)
    {
        LastMode = CurrentMode;
        Start = true;
    }
    if (LastTransportInterval != CurrentTransportInterval)
    {
        LastTransportInterval = CurrentTransportInterval;
        Start = true;
    }
    if (Start)
    {
        LastTransportTime = Millis;
    }
    else
    {
        Notify = (LastTransportTime + LastTransportInterval) < Millis;
        if (Notify)
            LastTransportTime = Millis;
    }

    SensorDeviceAccelerometer_Activate(SensorDeviceResources.accelerometerEnabled.data.b);
    if (SensorDeviceResources.accelerometerEnabled.data.b)
    {
        SensorDeviceAccelerometer_Update(CurrentMode, Notify);
    }
    SensorDeviceGyroscope_Activate(SensorDeviceResources.gyroscopeEnabled.data.b);
    if (SensorDeviceResources.gyroscopeEnabled.data.b)
    {
        SensorDeviceGyroscope_Update(CurrentMode, Notify);
    }
    SensorDeviceMagnetometer_Activate(SensorDeviceResources.magnetometerEnabled.data.b);
    if (SensorDeviceResources.magnetometerEnabled.data.b)
    {
        SensorDeviceMagnetometer_Update(CurrentMode, Notify);
    }
    SensorDeviceIlluminance_Activate(SensorDeviceResources.illuminanceSensorEnabled.data.b);
    if (SensorDeviceResources.illuminanceSensorEnabled.data.b)
    {
        SensorDeviceIlluminance_Update(CurrentMode, Notify);
    }
    SensorDeviceEnvironment_Activate(SensorDeviceResources.temperatureSensorEnabled.data.b,
            SensorDeviceResources.humiditySensorEnabled.data.b,
            SensorDeviceResources.barometerEnabled.data.b);
    if (SensorDeviceResources.barometerEnabled.data.b
            || SensorDeviceResources.temperatureSensorEnabled.data.b
            || SensorDeviceResources.humiditySensorEnabled.data.b)
    {
        SensorDeviceEnvironment_Update(CurrentMode, Notify);
    }
}

static retcode_t Interval(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (NULL != parser_ptr)
    {
        /* write value */
        int32_t value;
        retcode_t rc = Lwm2mParser_getInt(parser_ptr, &value);
        if (RC_OK == rc && value != TransportInterval)
        {
            if (LWM2M_MUTEX_LOCK(Mutex))
            {
                TransportInterval = value;
                LWM2M_MUTEX_UNLOCK(Mutex);
                printf("tranport interval changed: %ld\n", (long) value);
                if (Started)
                {
                    Lwm2m_URI_Path_T path = { OBJECTS_IX_SENSORDEVICE_0, OBJECTS_IX_SENSORDEVICE_0, SENSORDEVICE_RESOURCES_INDEX(transportInterval) };
                    Lwm2mReporting_resourceChanged(&path);
                }
            }
        }
        return rc;
    }
    else if ( NULL != serializer_ptr)
    {
        /* read value */
        return (Lwm2mSerializer_serializeInt(serializer_ptr, TransportInterval));
    }
    return (RC_LWM2M_METHOD_NOT_ALLOWED);
}

static retcode_t PreprocessingMode(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if (NULL != parser_ptr)
    {
        /* write value */

        StringDescr_T strDescr;
        retcode_t rc = Lwm2mParser_getString(parser_ptr, &strDescr);
        if (RC_OK == rc)
        {
            enum ProcessingMode newMode = CURRENT;
            if (StringDescr_compare(&strDescr, "current"))
                newMode = CURRENT;
            else if (StringDescr_compare(&strDescr, "max"))
                newMode = MAX;
            else if (StringDescr_compare(&strDescr, "min"))
                newMode = MIN;
            else if (StringDescr_compare(&strDescr, "avg"))
                newMode = AVG;
            else
                return RC_LWM2M_BAD_REQUEST;
            if (Mode != newMode)
            {
                if (LWM2M_MUTEX_LOCK(Mutex))
                {
                    Mode = newMode;
                    LWM2M_MUTEX_UNLOCK(Mutex);
                    if (Started)
                    {
                        Lwm2m_URI_Path_T path = { OBJECTS_IX_SENSORDEVICE_0, OBJECTS_IX_SENSORDEVICE_0, SENSORDEVICE_RESOURCES_INDEX(preprocessingMode) };
                        Lwm2mReporting_resourceChanged(&path);
                    }
                }
            }
        }
        return rc;
    }
    else if ( NULL != serializer_ptr)
    {
        /* read value */
        StringDescr_T strDescr;
        const char * mode = "current";
        switch (Mode)
        {
        case CURRENT:
            mode = "current";
            break;
        case MAX:
            mode = "max";
            break;
        case MIN:
            mode = "min";
            break;
        case AVG:
            mode = "avg";
            break;
        }
        StringDescr_set(&strDescr, mode, strlen(mode));
        return (Lwm2mSerializer_serializeString(serializer_ptr, &strDescr));
    }
    return (RC_LWM2M_METHOD_NOT_ALLOWED);
}

void ObjectSensorDevice_Init(const char* mac)
{
    configASSERT(portTICK_RATE_MS);

    Started = false;
    LWM2M_MUTEX_CREATE(Mutex);
    uint32_t Ticks = SENSORS_READ_INTERVAL / portTICK_RATE_MS;

        /* create timer task for sending data to lwm2m */
    TimerHandle = xTimerCreate((const char * const ) "sensors", Ticks, pdTRUE, NULL, SensorTimer);

    /* timer create fail case */
    if (TimerHandle == NULL)
    {
        assert(false); /* "This software timer was not Created, Due to Insufficient heap memory"); */
    }
    SensorDeviceResources.macAddress.data.s = (char*) mac;
}

void ObjectSensorDevice_Enable(void)
{
    Started = true;
    /*start the timer*/
    if (pdPASS != xTimerStart(TimerHandle, 5000 / portTICK_RATE_MS))
    {
        assert(0);
    }
}
