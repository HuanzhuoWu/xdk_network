/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObjects.h"
#include "Lwm2mUtil.h"
#include "Lwm2mInterface.h"

/* additional interface header files */
#include <Serval_Clock.h>
#include <Serval_Exceptions.h>
#include <BCDS_Retcode.h>
#include "XdkSensorHandle.h"

/* global variables ********************************************************* */

/* local variables ********************************************************** */

/*lint -e(956) const ?*/
static Lwm2m_URI_Path_T HumidityUriPath = { OBJECTS_IX_HUMIDITY_0, OBJECTS_IX_HUMIDITY_0, -1 };
static volatile bool started = false;

/* constant definitions ***************************************************** */
#define HUMIDITY_RESOURCE_INDEX(res) LWM2M_RESOURCES_INDEX(HumidityResources, res)

#define FLUSH_RESOURCES	Lwm2mReporting_multipleResourcesChanged(&HumidityUriPath, 6, \
		HUMIDITY_RESOURCE_INDEX(minRangeValue),\
		HUMIDITY_RESOURCE_INDEX(maxRangeValue),\
		HUMIDITY_RESOURCE_INDEX(units),\
		HUMIDITY_RESOURCE_INDEX(sensorValue),\
		HUMIDITY_RESOURCE_INDEX(minMeasuredValue),\
		HUMIDITY_RESOURCE_INDEX(maxMeasuredValue))

/* local functions ********************************************************** */
static retcode_t ResetMinMaxValues(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr);
static void ObjectHumidity_InternalEnable(float minRangeValue, float maxRangeValue);
static void ObjectHumidity_InternalDisable(void);
static void ObjectHumidity_InternalSetValue(float sensorValue_prh);

/* TODO need a way to define inactive resources. */
HumidityResource_T HumidityResources =
        {
                { 5601, LWM2M_FLOAT( 0.0F) | LWM2M_READ_ONLY }, // minMeassuredValue|R|Single|O|Float |   |
                { 5602, LWM2M_FLOAT( 0.0F) | LWM2M_READ_ONLY }, // maxMeassuredValue|R|Single|O|Float |   |
                { 5603, LWM2M_FLOAT( 0.0F) | LWM2M_READ_ONLY }, // minRangeValue    |R|Single|O|Float |   |
                { 5604, LWM2M_FLOAT( 0.0F) | LWM2M_READ_ONLY }, // maxRangeValue    |R|Single|O|Float |   |
                { 5605, LWM2M_FUNCTION(ResetMinMaxValues) }, // resetMinMaxValue |E|Single|O|      |   |
                { 5700, LWM2M_FLOAT( 0.0F) | LWM2M_READ_ONLY }, // sensorValue      |R|Single|M|Float |   |
                { 5701, LWM2M_STRING_RO("") }, // units            |R|Single|O|String|   |see:http://unitsofmeasure.org/ucum.html
                };

/*lint -e(956) */
static bool MinMaxInit = false;

static Lwm2m_Pair_Resource_Update_T AsyncCall_Enabler = { .set_pair = ObjectHumidity_InternalEnable, .mutex = LWM2M_MUTEX_INIT_VALUE };
static Lwm2m_Call_T AsyncCall_Disabler = { .call = ObjectHumidity_InternalDisable };
static Lwm2m_Single_Resource_Update_T AsyncCall_Updater = { .set_single = ObjectHumidity_InternalSetValue, .mutex = LWM2M_MUTEX_INIT_VALUE };

static retcode_t ResetMinMaxValues(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, minMeasuredValue, HumidityResources.sensorValue.data.f);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, maxMeasuredValue, HumidityResources.sensorValue.data.f);
    if (started)
        LWM2M_DYNAMIC_CHANGES_REPORT(changes, HumidityUriPath);

    printf("Humidity: reset min/max Values to current\r\n");
    return (RC_OK);
}

static void ObjectHumidity_InternalEnable(float minRangeValue, float maxRangeValue)
{
    started = true;
    HumidityResources.units.data.s = "%";
    HumidityResources.minRangeValue.data.f = minRangeValue;
    HumidityResources.maxRangeValue.data.f = maxRangeValue;
    HumidityResources.minMeasuredValue.data.f = minRangeValue;
    HumidityResources.maxMeasuredValue.data.f = minRangeValue;
    HumidityResources.sensorValue.data.f = minRangeValue;
    MinMaxInit = false;
    FLUSH_RESOURCES;
}

static void ObjectHumidity_InternalDisable(void)
{
    started = true;
    HumidityResources.units.data.s = "";
    HumidityResources.minRangeValue.data.f = 0.0F;
    HumidityResources.maxRangeValue.data.f = 0.0F;
    HumidityResources.minMeasuredValue.data.f = 0.0F;
    HumidityResources.maxMeasuredValue.data.f = 0.0F;
    HumidityResources.sensorValue.data.f = 0.0F;
    MinMaxInit = false;
    FLUSH_RESOURCES;
}

static void ObjectHumidity_InternalSetValue(float sensorValue_prh)
{
    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    if (MinMaxInit)
    {
        if (LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, sensorValue, sensorValue_prh))
        {
            LWM2M_DYNAMIC_CHANGES_SET_FLOAT_MIN_VALUE(changes, HumidityResources, minMeasuredValue, sensorValue_prh);
            LWM2M_DYNAMIC_CHANGES_SET_FLOAT_MAX_VALUE(changes, HumidityResources, maxMeasuredValue, sensorValue_prh);
            if (started)
                LWM2M_DYNAMIC_CHANGES_REPORT(changes, HumidityUriPath);
        }
    }
    else
    {
        MinMaxInit = true;
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, sensorValue, sensorValue_prh);
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, minMeasuredValue, sensorValue_prh);
        LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, HumidityResources, maxMeasuredValue, sensorValue_prh);
        if (started)
            LWM2M_DYNAMIC_CHANGES_REPORT(changes, HumidityUriPath);
    }
}

/* global functions ********************************************************* */
void ObjectHumidity_Init(void)
{
    started = false;
    LWM2M_MUTEX_CREATE(AsyncCall_Updater.mutex);
    AsyncCall_Enabler.mutex = AsyncCall_Updater.mutex;
}

void ObjectHumidity_Enable(float minRangeValue, float maxRangeValue)
{
    Lwm2mUtil_UpdatePairResources(minRangeValue, maxRangeValue, &AsyncCall_Enabler);
}

void ObjectHumidity_Disable(void)
{
    Lwm2mUtil_Schedule(&AsyncCall_Disabler);
}

void ObjectHumidity_SetValue(float sensorValue_prh)
{
    Lwm2mUtil_UpdateSingleResource(sensorValue_prh, &AsyncCall_Updater);
}
