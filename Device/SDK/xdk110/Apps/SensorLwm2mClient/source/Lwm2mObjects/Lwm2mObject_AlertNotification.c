/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_AlertNotification.h"
#include "Lwm2mUtil.h"
#include "Lwm2mObjects.h"

/* additional interface header files */
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

#define ALERTNOTIFICATION_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(AlertNotificationResources, res)

static LWM2M_MUTEX_INSTANCE(Mutex) = LWM2M_MUTEX_INIT_VALUE;

/*lint -e(956) const ?*/
static Lwm2m_URI_Path_T AlertUriPath = { OBJECTS_IX_ALERTNOTIFICATION_0, OBJECTS_IX_ALERTNOTIFICATION_0, ALERTNOTIFICATION_RESOURCES_INDEX(alert) };

/*lint -esym(956, AlertMessage) mutex used */
static char AlertMessage[128] = { 0 };
static volatile bool Started = false;

static retcode_t ResetAlertFunc(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    (void) parser_ptr;
    (void) serializer_ptr;

    printf("reset alert\r\n");
    if (LWM2M_MUTEX_LOCK(Mutex))
    {
        AlertMessage[0] = 0;
        LWM2M_MUTEX_UNLOCK(Mutex);
        if (Started)
            Lwm2mReporting_resourceChanged(&AlertUriPath);
    }

    return (RC_OK);
}

static retcode_t ReadAlert(Lwm2mSerializer_T *serializer_ptr, Lwm2mParser_T *parser_ptr)
{
    if ( NULL == serializer_ptr || NULL != parser_ptr)
    {
        return (RC_LWM2M_METHOD_NOT_ALLOWED);
    }
    retcode_t result = RC_COAP_OVERLOADED;
    if (LWM2M_MUTEX_LOCK(Mutex))
    {
        StringDescr_T strDescr_sn;
        StringDescr_set(&strDescr_sn, AlertMessage, strlen(AlertMessage));
        result = Lwm2mSerializer_serializeString(serializer_ptr, &strDescr_sn);
        LWM2M_MUTEX_UNLOCK(Mutex);
    }
    return result;
}

void ObjectAlertNotification_SetValue(const char* alert)
{
    if (strncmp(AlertMessage, alert, sizeof(AlertMessage) - 1) != 0)
    {
        if (LWM2M_MUTEX_LOCK(Mutex))
        {
            strncpy(AlertMessage, alert, sizeof(AlertMessage) - 1);
            printf("alert: %s\r\n", AlertMessage);
            LWM2M_MUTEX_UNLOCK(Mutex);
            if (Started)
                Lwm2mReporting_resourceChanged(&AlertUriPath);
        }
    }
}

void ObjectAlertNotification_Init(void)
{
    Started = false;
    AlertMessage[0] = 0;
    AlertMessage[sizeof(AlertMessage) - 1] = 0;
    LWM2M_MUTEX_CREATE(Mutex);
}

void ObjectAlertNotification_Enable(void)
{
    Started = true;
}

AlertNotificationResource_T AlertNotificationResources =
        {
                { 0, LWM2M_DYNAMIC(ReadAlert) | LWM2M_READ_ONLY },
                { 1, LWM2M_FUNCTION(ResetAlertFunc) },
                };
