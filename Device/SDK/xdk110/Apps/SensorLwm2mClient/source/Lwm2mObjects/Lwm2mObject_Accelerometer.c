/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Accelerometer.h"
#include "Lwm2mObjects.h"
#include "Lwm2mUtil.h"

/* additional interface header files */
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

#define ACCELEROMETER_RESOURCES_INDEX(res) LWM2M_RESOURCES_INDEX(AccelerometerResources, res)

#define FLUSH_RESOURCES	Lwm2mReporting_multipleResourcesChanged(&AccelerometerUriPath, 6, \
			ACCELEROMETER_RESOURCES_INDEX(units),\
			ACCELEROMETER_RESOURCES_INDEX(minRange),\
			ACCELEROMETER_RESOURCES_INDEX(maxRange),\
			ACCELEROMETER_RESOURCES_INDEX(xAxis),\
			ACCELEROMETER_RESOURCES_INDEX(yAxis),\
			ACCELEROMETER_RESOURCES_INDEX(zAxis))

static void ObjectAccelerometer_InternalEnable(float minRange, float maxRange);
static void ObjectAccelerometer_InternalDisable(void);
static void ObjectAccelerometer_InternalSetValues(float xAxis, float yAxis, float zAxis);

/*lint -e(956) const ?*/
static Lwm2m_URI_Path_T AccelerometerUriPath = { OBJECTS_IX_ACCELEROMETER_0, OBJECTS_IX_ACCELEROMETER_0, -1 };

static Lwm2m_Pair_Resource_Update_T AsyncCall_Enabler = { .set_pair = ObjectAccelerometer_InternalEnable, .mutex = LWM2M_MUTEX_INIT_VALUE };
static Lwm2m_Call_T AsyncCall_Disabler = { .call = ObjectAccelerometer_InternalDisable };
static Lwm2m_Tripple_Resource_Update_T AsyncCall_Updater = { .set_tripple = ObjectAccelerometer_InternalSetValues, .mutex = LWM2M_MUTEX_INIT_VALUE };
static volatile bool Started = false;

AccelerometerResource_T AccelerometerResources =
        {
                { 5603, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY },
                { 5604, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY },
                { 5701, LWM2M_STRING_RO("") },
                { 5702, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY },
                { 5703, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY },
                { 5704, LWM2M_FLOAT(0.0F) | LWM2M_READ_ONLY },
        };

static void ObjectAccelerometer_InternalEnable(float minRange, float maxRange)
{
    Started = true;
    AccelerometerResources.units.data.s = "g";
    AccelerometerResources.minRange.data.f = minRange;
    AccelerometerResources.maxRange.data.f = maxRange;
    AccelerometerResources.xAxis.data.f = minRange;
    AccelerometerResources.yAxis.data.f = minRange;
    AccelerometerResources.zAxis.data.f = minRange;
    FLUSH_RESOURCES;
}

static void ObjectAccelerometer_InternalDisable(void)
{
    Started = true;
    AccelerometerResources.units.data.s = "";
    AccelerometerResources.minRange.data.f = 0.0F;
    AccelerometerResources.maxRange.data.f = 0.0F;
    AccelerometerResources.xAxis.data.f = 0.0F;
    AccelerometerResources.yAxis.data.f = 0.0F;
    AccelerometerResources.zAxis.data.f = 0.0F;
    FLUSH_RESOURCES;
}

static void ObjectAccelerometer_InternalSetValues(float xAxis, float yAxis, float zAxis)
{
    INIT_LWM2M_DYNAMIC_CHANGES(changes);

    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, AccelerometerResources, xAxis, xAxis);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, AccelerometerResources, yAxis, yAxis);
    LWM2M_DYNAMIC_CHANGES_SET_FLOAT_VALUE(changes, AccelerometerResources, zAxis, zAxis);
    if (Started)
        LWM2M_DYNAMIC_CHANGES_REPORT(changes, AccelerometerUriPath);
}

void ObjectAccelerometer_Init(void)
{
    Started = false;
    LWM2M_MUTEX_CREATE(AsyncCall_Updater.mutex);
    AsyncCall_Enabler.mutex = AsyncCall_Updater.mutex;
}

void ObjectAccelerometer_Enable(float minRange, float maxRange)
{
    Lwm2mUtil_UpdatePairResources(minRange, maxRange, &AsyncCall_Enabler);
}

void ObjectAccelerometer_Disable(void)
{
    Lwm2mUtil_Schedule(&AsyncCall_Disabler);
}

void ObjectAccelerometer_SetValues(float xAxis, float yAxis, float zAxis)
{
    Lwm2mUtil_UpdateTrippleResources(xAxis, yAxis, zAxis, &AsyncCall_Updater);
}
