/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*
* Contributors:
* Bosch Software Innovations GmbH
*/
/*----------------------------------------------------------------------------*/

/**
 * Module that parses the configuration file present on the micro SD card
 * 
 * Configuration file has following constraints:
 * 1) No TOKEN should be missing, all TOKENs should be present, even if not used.
 * 2) TOKENs should not have any space.
 * 3) Value should not have any trailing spaces.
 * 4) TOKENs are case sensitive.
 * 5) CFG_FILE_READ_BUFFER size is currently 800 bytes.
 * 6) Default value of TOKENs are not considered from '.c' and '.h' files.
 *
 * Example for config.txt file:
 * ----------------------------
 * #give your router configuration ssid name
 * SSID=twist-wifi
 * #give your router configuration password
 * PASSWORD=twist-wifi
 * #If YES please ensure correct MASK,DNSSERVER&GATEWAY are valid
 * STATIC=YES/NO
 * #This field gives static IP address
 * STATICIP=192.168.1.5
 * #This field gives DNS address
 * DNSSERVER=192.168.1.1
 * #This field gives Gateway address
 * GATEWAY=192.168.1.1
 * #This field gives IP address MASK
 * MASK=255.255.255.0
 * #This field gives COAP address
 * LWM2MDEFSRV=coap://192.168.1.141:5685
 * #This field gives LwM2M End point address Name
 * LWM2MENDPOINT=XDK110_DEMO_LWM2M
 * #This field gives configuration file the attribute value for ACTIVATEDTLS is ON
 * ACTIVATEDTLSPSK = YES/NO
 *#This field gives DTLS/PSK identity of the configuration file
 * DTLSPSKIDENTITY = XDK110_0
 *#This field gives DTLS/PSK secret key of the configuration file (ex: Leshan has a key convert into Hex)
 * DTLSPSKSECRETKEY = Leshan
 * # This Field used to Enable the Buttons & LEDS States
 * TESTMODE = YES/NO
 * ****************************************************************************/

/* module includes ********************************************************** */

//lint -esym(956,*) /* Suppressing Non const, non volatile static or external variable */
/* own header files */
#include "CfgParser.h"

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"
#include "BCDS_Assert.h"

/* additional interface header files */
#include "BCDS_SDCardDriver.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "ff.h"
#include "Lwm2mClient.h"

/* constant definitions ***************************************************** */
#define CFG_DEFAULT_WLAN_SSID           ""            /**< WLAN SSID Name by Default*/
#define CFG_DEFAULT_WLAN_PWD            ""            /**< WLAN PWD by Default*/
#define CFG_DEFAULT_STATICIP            ""            /**< Network static IP Address Default*/
#define CFG_DEFAULT_DNSSERVER           ""            /**< Network DNS server Default*/
#define CFG_DEFAULT_GATEWAY             ""            /**< Network gateway Default*/
#define CFG_DEFAULT_MASK                ""            /**< Network mask Default*/
#define CFG_DEFAULT_LWM2M_SERVER        ""            /**< LWM2M default server by Default*/
#define CFG_DEFAULT_LWM2M_ENDPOINT      ""            /**< LWM2M endpoint name by Default*/
#define CFG_DEFAULT_LWM2M_DTLS_SUFFIX   ""            /**< LWM2M dtls suffix by Default*/
#define CFG_DEFAULT_LWM2M_LIFETIME      "240"         /**< LWM2M lifetime by Default*/
#define CFG_DEFAULT_LWM2M_BINDING       "U"           /**< LWM2M binding by Default*/
#define CFG_DEFAULT_LWM2M_NOTIFIES      "NO"          /**< LWM2M binding by Default*/
#define CFG_STATIC_IP_IS_YES            "YES"         /**< STATIC IP Address is activate*/
#define CFG_STATIC_IP_IS_NO             "NO"          /**< STATIC IP Address is not activated */
#warning Please enter proper SecretKey to establish secured connection to server.
#define CFG_DEFAULT_DTLS_PSK_IDENTITY   ""            /**< DTLS/PSK default identity*/
#define CFG_DEFAULT_DTLS_PSK_SECRET_KEY ""            /**< DTLS/PSK default secret key*/
#define CFG_DTLS_IS_ACTIVATED           "YES"         /**< BLE is activate*/
#define CFG_DTLS_IS_NOT_ACTIVATED       "NO"          /**< BLE is not activated */
#define CFG_TEST_MODE_MIX               "MIX"         /**< BLE is not activated */
#define CFG_TEST_MODE_ACTIVATED         "YES"         /**< BLE is not activated */
#define CFG_TEST_MODE_NOT_ACTIVATED     "NO"          /**< BLE is not activated */

#define CFG_DEFAULT_DRIVE               ""              /**< SD Card default Drive location */
#define CFG_FORCE_MOUNT                 UINT8_C(1)      /**< Macro to define force mount */
#define CFG_CONFIG_FILENAME             "0:config.txt"  /**< Configuration file name */
#define CFG_SEEK_FIRST_LOCATION         UINT8_C(0)      /**< File seek to the first location */
#define CFG_CONFIG_TABLE_SIZE           sizeof(ConfigStructure)/sizeof(ConfigLine_T)
#define CFG_MAX_LINE_SIZE               UINT8_C(50)
#define CFG_DETECT_SD_CARD_INSERTED     UINT8_C(1)
#define CFG_WRITEREAD_BLOCK_TIME        UINT32_C(0xffff)    /**< Macro used to define block time of a timer*/
#define CFG_STACK_SIZE_FOR_TASK         UINT16_C(512)       /**< TASK stack size for CONFIG FILE READER application handler */
#define CFG_TASK_PRIORITY               UINT8_C(2)
#define CFG_FILE_READ_BUFFER            UINT16_C(2024)
#define CFG_TRUE                        UINT8_C(1)    /**< One value  */
#define CFG_FALSE                       UINT8_C(0)    /**< Zero value */

#define CFG_NUMBER_UINT8_ZERO           UINT8_C(0)    /**< Zero value */

/* local variables ********************************************************** */
/** Variable containers for configuration values */
static char A1Val[CFG_MAX_LINE_SIZE];
static char A2Val[CFG_MAX_LINE_SIZE];
static char A3Val[CFG_MAX_LINE_SIZE];
static char A4Val[CFG_MAX_LINE_SIZE];
static char A5Val[CFG_MAX_LINE_SIZE];
static char A6Val[CFG_MAX_LINE_SIZE];
static char A7Val[CFG_MAX_LINE_SIZE];
static char A8Val[CFG_MAX_LINE_SIZE];
static char A9Val[CFG_MAX_LINE_SIZE];
static char A10Val[CFG_MAX_LINE_SIZE];
static char A11Val[CFG_MAX_LINE_SIZE];
static char A12Val[CFG_MAX_LINE_SIZE];
static char A13Val[CFG_MAX_LINE_SIZE];
static char A14Val[CFG_MAX_LINE_SIZE];
static char A15Val[CFG_MAX_LINE_SIZE];
static char A16Val[CFG_MAX_LINE_SIZE];
static char A17Val[CFG_MAX_LINE_SIZE];

/** Attribute names on the configuration file */
static const char A1Name[] = "SSID";
static const char A2Name[] = "PASSWORD";
static const char A3Name[] = "STATIC";
static const char A4Name[] = "STATICIP";
static const char A5Name[] = "DNSSERVER";
static const char A6Name[] = "GATEWAY";
static const char A7Name[] = "MASK";
static const char A8Name[] = "LWM2MDEFSRV";
static const char A9Name[] = "LWM2MENDPOINT";
static const char A10Name[] = "LWM2MDTLSSUFFIX";
static const char A11Name[] = "ACTIVATEDTLSPSK";
static const char A12Name[] = "DTLSPSKIDENTITY";
static const char A13Name[] = "DTLSPSKSECRETKEY";
static const char A14Name[] = "TESTMODE";
static const char A15Name[] = "LWM2MLIFETIME";
static const char A16Name[] = "LWM2MBINDING";
static const char A17Name[] = "LWM2MCONNOTIFIES";

static uint8_t FileReadBuffer[CFG_FILE_READ_BUFFER];
static FATFS s_FatFileSystemObject; /** File system specific objects */

/*
 * Configuration holder structure array
 */
static ConfigLine_T ConfigStructure[] =
        {
                { A1Name, A1Val, CFG_FALSE },
                { A2Name, A2Val, CFG_FALSE },
                { A3Name, A3Val, CFG_FALSE },
                { A4Name, A4Val, CFG_FALSE },
                { A5Name, A5Val, CFG_FALSE },
                { A6Name, A6Val, CFG_FALSE },
                { A7Name, A7Val, CFG_FALSE },
                { A8Name, A8Val, CFG_FALSE },
                { A9Name, A9Val, CFG_FALSE },
                { A10Name, A10Val, CFG_FALSE },
                { A11Name, A11Val, CFG_FALSE },
                { A12Name, A12Val, CFG_FALSE },
                { A13Name, A13Val, CFG_FALSE },
                { A14Name, A14Val, CFG_FALSE },
                { A15Name, A15Val, CFG_FALSE },
                { A16Name, A16Val, CFG_FALSE },
                { A17Name, A17Val, CFG_FALSE },
        };

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief extracts tokens from the input buffer , copy into the token buffer and returns its
 * size at tokensize
 *
 * @param[in] buffer
 *            The input buffer
 * @param[in] idxAtBuffer
 *            The index from which we have to look after tokens
 * @param[in] bufSize
 *            The size of the input buffer (param one)
 * @param[out] token
 *            The buffer that will contain the token (must be different to NULL )
 * @param[in] expTokenType
 *            expected token type awaited from attribute parser
 *
 * @param[out] tokenSize
 *            The pointer to the variable that receive the size of the buffer
 *
 * @return the TOKEN types found (TOKEN_TYPE_UNKNOWN is EOF)
 */
static TokensType_T GetToken(const char *buffer, uint16_t *idxAtBuffer,
        uint16_t bufSize, char *token, uint16_t *tokenSize, States_T expTokenType)
{
    TokensType_T Result = TOKEN_TYPE_UNKNOWN;

    /* erase the OUTPUT token buffer*/
    memset(token, 0, *tokenSize);

    uint8_t index = UINT8_C(0);
    /* Bypass all unusefull chars like \r \n and blanks */
    while (
    ((buffer[*idxAtBuffer] == '\r') ||
            (buffer[*idxAtBuffer] == '\n') ||
            (buffer[*idxAtBuffer] == '\t') ||
            (buffer[*idxAtBuffer] == ' '))
            &&
            (*idxAtBuffer < bufSize)
    )
    {
        *idxAtBuffer = *idxAtBuffer + 1;
    }

    if (expTokenType == STAT_EXP_ATT_NAME)
    {
        while (buffer[*idxAtBuffer] == '#') // Handling in-line comments for key value pair in the configuration file
        {
            while ((buffer[*idxAtBuffer] != '\n')

            && (buffer[*idxAtBuffer] != '\r')
                    && (*idxAtBuffer < bufSize)) // skip to EOL if since Start of comment tag '#' was found
            {
                *idxAtBuffer = *idxAtBuffer + 1;
            }
            while (((buffer[*idxAtBuffer] == '\r') ||
                    (buffer[*idxAtBuffer] == '\n') ||
                    (buffer[*idxAtBuffer] == '\t') ||
                    (buffer[*idxAtBuffer] == ' ')) && (*idxAtBuffer < bufSize)
            )
            {
                *idxAtBuffer = *idxAtBuffer + 1;
            }
        }
    }

    /* is the nex char is the EQUAL sign */
    if (buffer[*idxAtBuffer] == '=')
    {
        /* YES */
        token[index] = '=';
        *idxAtBuffer = *idxAtBuffer + 1;
        index = index + 1;
        Result = TOKEN_EQUAL;
    }
    else
    {
        /* No , its a string ...*/
        if (expTokenType == STAT_EXP_ATT_VALUE)
        {
            while (
            (buffer[*idxAtBuffer] != '\r') &&
                    (buffer[*idxAtBuffer] != '\n') &&
                    (buffer[*idxAtBuffer] != '\t') &&
                    (*idxAtBuffer < bufSize)
            )
            {
                if (index < *tokenSize - 1)
                {
                    token[index] = buffer[*idxAtBuffer];
                    *idxAtBuffer = *idxAtBuffer + 1;
                    index = index + 1;
                }
            };
        }
        else
        {
            while (
            (buffer[*idxAtBuffer] != '\r') &&
                    (buffer[*idxAtBuffer] != '\n') &&
                    (buffer[*idxAtBuffer] != '\t') &&
                    (buffer[*idxAtBuffer] != ' ') &&
                    (buffer[*idxAtBuffer] != '=') &&
                    (*idxAtBuffer < bufSize)
            )
            {
                if (index < *tokenSize - 1)
                {
                    token[index] = buffer[*idxAtBuffer];
                    *idxAtBuffer = *idxAtBuffer + 1;
                    index = index + 1;
                }
            };
        }
        /* Is this string is a known string ,i.e. attribute name ?*/
        for (uint8_t ConfigIndex = UINT8_C(0); ConfigIndex < CFG_CONFIG_TABLE_SIZE; ConfigIndex++)
        {
            if (0 == strcmp(token, ConfigStructure[ConfigIndex].attName))
            {
                Result = TOKEN_ATT_NAME;
                break;
            }
        }
        /* The string is not known one, so it's a value */
        if ((index > 0) && (TOKEN_TYPE_UNKNOWN == Result))
        {
            Result = TOKEN_VALUE;
        }
    }
    /* At this point nothing has been found ! */
    if ((TOKEN_TYPE_UNKNOWN == Result))
    {
        Result = TOKEN_EOF;
    }
    return (Result);
}

/**
 * @brief Parse the config file read inside the buffer
 *
 * @param[in] buffer
 *            The buffer containg the configuration file
 *
 * @param[in] bufSize
 *            The size of the buffer (first param)
 * @return CFG_TRUE if configuration file is correct and contains necessary attribute/values
 *
 */
static uint8_t CfgParseConfigFileBuffer(const char *buffer, uint16_t bufSize)
{
    uint16_t IndexAtBuffer = UINT16_C(0);
    uint8_t Result = CFG_TRUE;
    States_T State = STAT_EXP_ATT_NAME;
    int8_t CurrentConfigToSet = INT8_C(0);
    char Token[CFG_MAX_LINE_SIZE] = { 0 };
    uint16_t OutputTokenSize = UINT16_C(0);
    while (CFG_TRUE == Result)
    {
        OutputTokenSize = CFG_MAX_LINE_SIZE;
        TokensType_T GetTokenType = GetToken(buffer, &IndexAtBuffer, bufSize, Token, &OutputTokenSize, State);
        if (GetTokenType == TOKEN_EOF)
            break;

        switch (State)
        {
        case STAT_EXP_ATT_NAME:
            {
            if (GetTokenType != TOKEN_ATT_NAME)
            {
                printf("Expecting attname at %u\r\n", (uint16_t) (IndexAtBuffer - strlen(Token)));
                Result = CFG_FALSE;
                break;
            }
            for (uint8_t i = UINT8_C(0); i < CFG_CONFIG_TABLE_SIZE; i++)
            {
                if (strcmp(Token, ConfigStructure[i].attName) == UINT8_C(0))
                {
                    CurrentConfigToSet = i;
                    State = STAT_EXP_ATT_EQUAL;
                    break;
                }
            }

            break;
        }
        case STAT_EXP_ATT_EQUAL:
            {
            if (GetTokenType != TOKEN_EQUAL)
            {
                printf("Expecting sign '=' at %u\r\n", (uint16_t) (IndexAtBuffer - strlen(Token)));
                Result = CFG_FALSE;
                break;
            }
            State = STAT_EXP_ATT_VALUE;
            break;
        }
        case STAT_EXP_ATT_VALUE:
            {
            if (GetTokenType != TOKEN_VALUE)
            {
                printf("Expecting value string at %u\r\n", (uint16_t) (IndexAtBuffer - strlen(Token)));
                Result = CFG_FALSE;
                break;
            }

            strcpy(ConfigStructure[CurrentConfigToSet].attValue, Token);

            if (ConfigStructure[CurrentConfigToSet].defined == 0)
            {

                ConfigStructure[CurrentConfigToSet].defined = 1;
                State = STAT_EXP_ATT_NAME;

            }
            else
            {
                Result = CFG_FALSE;
                printf("Twice definition of  attribute %s! \r\n", ConfigStructure[CurrentConfigToSet].attName);
            }
            break;
        }
        default:
            printf("Unexpected state %d \r\n", State);
            break;
        }

    }
    /** Verifying that all atributes are defined */
    for (uint8_t i = UINT8_C(0); Result == CFG_TRUE && i < CFG_CONFIG_TABLE_SIZE; i++)
    {
        if (ConfigStructure[i].defined == UINT8_C(0))
        {
            printf("Value for attribute %s not defined !\r\n", ConfigStructure[i].attName);
            Result = CFG_FALSE;
        }
    }
    printf("SD-Card config.txt:\n\r");
    for (uint8_t i = UINT8_C(0); i < CFG_CONFIG_TABLE_SIZE; i++)
    {
        printf("[%16s] = [%s]\r\n", ConfigStructure[i].attName, ConfigStructure[i].attValue);
    }

    return Result;
}

/** @brief For description of the function please refer interface header CfgParserInterface.h  */
Retcode_T CfgParser_ParseConfigFile(void)
{
    Retcode_T RetVal = (Retcode_T) RETCODE_FAILURE;

    FRESULT FileSystemResult;
    FIL FileReadHandle;
    UINT BytesRead;

    /* Initialize the attribute values holders */
    memset(A1Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A2Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A3Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A4Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A5Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A6Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A7Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A8Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A9Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A10Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A11Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A12Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A13Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A14Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A15Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A16Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    memset(A17Val, CFG_NUMBER_UINT8_ZERO, CFG_MAX_LINE_SIZE);
    /*Initialize file system */
    if (f_mount(&s_FatFileSystemObject, CFG_DEFAULT_DRIVE, CFG_FORCE_MOUNT) != FR_OK)
    {
        printf("f_mount failed !!\r\n");
        return (RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE));
    }

    /* Open the file */
    FileSystemResult = f_open(&FileReadHandle, CFG_CONFIG_FILENAME, FA_OPEN_EXISTING | FA_READ);
    if (FileSystemResult != FR_OK)
    {
        printf("f_open failed !!\r\n");
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T )RETCODE_FAILURE));
    }
    printf("f_open done \r\n");

    /*Step5 - Set the file read pointer to first location */
    FileSystemResult = f_lseek(&FileReadHandle, CFG_SEEK_FIRST_LOCATION);
    if (FileSystemResult != FR_OK)
    {
        /* Error. Cannot set the file write pointer */
        printf("f_lseek failed !!\r\n");
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T ) RETCODE_FAILURE));
    }
    printf("f_lseek done \r\n");

    /*Step6 - Read a buffer from file BUGGY memset !!!! */
    memset(FileReadBuffer, CFG_NUMBER_UINT8_ZERO, CFG_FILE_READ_BUFFER);

    /* DO NOT put the buffer here ... 1024 bytes bigger than the stack size of the task calling this CfgParser_ParseConfigFile!!!!*/

    BytesRead = 0;
    /* FREAD stucks !!!!!!! */

    FileSystemResult = f_read(&FileReadHandle, FileReadBuffer, CFG_FILE_READ_BUFFER, &BytesRead);
    printf("f_read done, bytes Read = %d \r\n", BytesRead);

    if (BytesRead >= CFG_FILE_READ_BUFFER)
    {
        printf("f_read done, bytes Read = %d  is greater than or equal to CFG_FILE_READ_BUFFER \r\n", BytesRead);
        assert(0);
    }
    if (FileSystemResult == FR_OK)
    {
        if (CFG_TRUE == CfgParseConfigFileBuffer((const char*) FileReadBuffer, BytesRead))
        {
            RetVal = RETCODE_OK;
        }
        else
        {
            RetVal = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
        }
        FileSystemResult = f_close(&FileReadHandle);
    }

    if (FileSystemResult != FR_OK)
    {
        printf("Error in closing default configuration file !!\r\n");
        RetVal = RETCODE(RETCODE_SEVERITY_ERROR, RETCODE_FAILURE);
    }

    return RetVal;
}

/**
 * @brief returns the WLAN SSID defined at the configuration file
 *
 * @return WLAN SSID
 */
const char *CfgParser_GetWlanSSID(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_SSID].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_SSID].attValue;
    }
    else
        return CFG_DEFAULT_WLAN_SSID;

}
/**
 * @brief returns the PWD defined by the attribute PXD of the configuration file
 *
 * @return WLAN PWD
 */
const char *CfgParser_GetWlanPassword(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_PASSWORD].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_PASSWORD].attValue;
    }
    else
        return CFG_DEFAULT_WLAN_PWD;
}

/**
 * @brief For description please refer CfgParser.h header file
 */
extern const char *CfgParser_GetStaticIpAddress(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_STATICIP].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_STATICIP].attValue;
    }
    else
        return CFG_DEFAULT_STATICIP;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
extern const char *CfgParser_GetDnsServerAddress(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_DNSSERVER].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_DNSSERVER].attValue;
    }
    else
        return CFG_DEFAULT_DNSSERVER;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
extern const char *CfgParser_GetSubnetMask(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_MASK].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_MASK].attValue;
    }
    else
        return CFG_DEFAULT_MASK;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
extern const char *CfgParser_GetGatewayAddress(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_GATEWAY].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_GATEWAY].attValue;
    }
    else
        return CFG_DEFAULT_GATEWAY;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
const char *CfgParser_GetLwm2mServerAddress(void)
{

    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_DEFAULT_SERVER].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_LWM2M_DEFAULT_SERVER].attValue;
    }
    else
        return CFG_DEFAULT_LWM2M_SERVER;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
const char *CfgParser_GetLwm2mEndpointName(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_ENDPOINT].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_LWM2M_ENDPOINT].attValue;
    }
    else
        return CFG_DEFAULT_LWM2M_ENDPOINT;
}

/**
 * @brief For description please refer CfgParserInterface.h header file
 */
CfgParser_ConditionalValues_T CfgParser_IsStaticModeActivated(void)
{
    CfgParser_ConditionalValues_T RetVal = CGF_CONDITIONAL_VALUE_OUT_OF_CHOICE;
    if (CFG_TRUE == ConfigStructure[ATT_IDX_STATIC].defined)
    {
        if (0 == strcmp(CFG_STATIC_IP_IS_YES, ConfigStructure[ATT_IDX_STATIC].attValue))
        {
            RetVal = CGF_CONDITIONAL_VALUE_YES;
        }
        else if (0 == strcmp(CFG_STATIC_IP_IS_NO, ConfigStructure[ATT_IDX_STATIC].attValue))
        {
            RetVal = CGF_CONDITIONAL_VALUE_NO;
        }
        else
        {
            RetVal = CGF_CONDITIONAL_VALUE_OUT_OF_CHOICE;
        }
    }
    else
    {
        RetVal = CGF_CONDITIONAL_VALUE_NOT_DEFINED;
    }
    return (RetVal);
}

bool CfgParser_ActivateDtlsPsk(void)
{
    bool RetVal = false;

    if (CFG_TRUE == ConfigStructure[ATT_IDX_ACTIVATE_DTLS_PSK].defined)
    {
        if (0 == strcmp(CFG_DTLS_IS_ACTIVATED, ConfigStructure[ATT_IDX_ACTIVATE_DTLS_PSK].attValue))
        {
            RetVal = true;
        }
        else
        {
            RetVal = false;
        }
    }
    else
    {
#if SECURITY_ENABLE == 1
        RetVal = true;
#endif
    }
    return (RetVal);
}

const char *CfgParser_GetLwm2mDtlsSuffix(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_DTLS_SUFFIX].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_LWM2M_DTLS_SUFFIX].attValue;
    }
    else
        return CFG_DEFAULT_LWM2M_DTLS_SUFFIX;
}

const char *CfgParser_GetDtlsPskIdentity(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_DTLS_PSK_IDENTITY].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_DTLS_PSK_IDENTITY].attValue;
    }
    else
        return CFG_DEFAULT_DTLS_PSK_IDENTITY;
}

const char *CfgParser_GetDtlsPskSecretKey(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_DTLS_PSK_SECRET_KEY].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_DTLS_PSK_SECRET_KEY].attValue;
    }
    else
        return CFG_DEFAULT_DTLS_PSK_SECRET_KEY;
}

uint8_t CfgParser_TestMode(void)
{
    uint8_t Result = CFG_TESTMODE_MIX;
    if (CFG_TRUE == ConfigStructure[ATT_IDX_TEST_MODE].defined)
    {
        if (0 == strcmp(CFG_TEST_MODE_MIX, ConfigStructure[ATT_IDX_TEST_MODE].attValue))
        {
            Result = CFG_TESTMODE_MIX;
        }
        if (0 == strcmp(CFG_TEST_MODE_ACTIVATED, ConfigStructure[ATT_IDX_TEST_MODE].attValue))
        {
            Result = CFG_TESTMODE_ON;
        }
        if (0 == strcmp(CFG_TEST_MODE_NOT_ACTIVATED, ConfigStructure[ATT_IDX_TEST_MODE].attValue))
        {
            Result = CFG_TESTMODE_OFF;
        }
    }
    return Result;
}

/**
 * @brief For description please refer CfgParser.h header file
 */
uint32_t CfgParser_GetLwm2mLifetime(void)
{
    const char* value = CFG_DEFAULT_LWM2M_LIFETIME;
    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_LIFETIME].defined)
    {
        value = ConfigStructure[ATT_IDX_TEST_MODE].attValue;
    }
    return (uint32_t) atol(value);
}

const char *CfgParser_GetLwm2mDeviceBinding(void)
{
    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_BINDING].defined)
    {
        return (const char*) ConfigStructure[ATT_IDX_LWM2M_BINDING].attValue;
    }
    else
        return CFG_DEFAULT_LWM2M_BINDING;
}

uint8_t CfgParser_UseLwm2mConNotifies(void)
{
    uint8_t Result = CFG_FALSE;
    if (CFG_TRUE == ConfigStructure[ATT_IDX_LWM2M_CON_NOTIFIES].defined)
    {
        if (0 == strcmp(CFG_TEST_MODE_ACTIVATED, ConfigStructure[ATT_IDX_LWM2M_CON_NOTIFIES].attValue))
        {
            Result = CFG_TRUE;
        }
    }
    return Result;
}


/** ************************************************************************* */
