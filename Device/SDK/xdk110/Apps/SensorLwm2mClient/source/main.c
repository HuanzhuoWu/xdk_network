/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */

/**
 * @file
 * \mainpage XDK Sensor LWM2M Client
 *
 * The XDK sensor LWM2M client implements two tasks:
 * - reading sensor values and processing them
 * - maintain a LWM2M connection to the server
 *
 * The first task is controlled by the \link Lwm2mObject_SensorDevice.h \endlink.
 * The sensor device contains a timer, which updates all enabled sensor values every
 * 100msec and processes the value according the \link SensorDeviceResource_S.preprocessingMode \endlink.
 * After the \link SensorDeviceResource_S.transportInterval \endlink, the timer also
 * notifies all enabled LWM2M object instances of the new calculated values.
 * Reads will the get responses with the new values and observes may be notified of
 * changes.
 *
 * The second task requires to maintain a "registration state", which is implemented in
 * \link Lwm2mInterface.h \endlink. Depending on the registration state of the device and
 * the LWM2M lifetime the device sends registration or update registration messages to the
 * server. Therefore the Lwm2mInterface uses a timer, which checks every second the registration
 * state.
 *
 * The application starts here in main with the initialization of  freeRTOS (Real Time Operation System).
 * This starts the client execution in Lwm2mClient.c with the function init.
 */

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* additional interface header files */
#include "XdkSystemStartup.h"

/* own header files */

/* functions */
/** \cond */
int main(void)
{
    systemStartup();
}
/** \endcond */
