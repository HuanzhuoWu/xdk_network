/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Barometer.h"
#include "Lwm2mObject_Temperature.h"
#include "Lwm2mObject_Humidity.h"
#include "SensorDeviceEnvironment.h"

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>
#include "BCDS_Environmental.h"

#define PRESSURE_TO_HPA_FLOAT(I)  ((float)(I) / 100.0F)		/* from integer Pa */
#define TEMPERATURE_TO_C_FLOAT(I) ((float)(I) / 1000.0F)	/* from integer mDeg */
#define HUMIDITY_TO_PERC_FLOAT(I) ((float)(I))				/* from integer %rH */

/* current impl.: one structure for sensor -> one enable! */
static SensorDeviceProcessDataFloat_T TemperatureSensorDeviceData = { { { 0 } }, 1, 0, CURRENT, false };
static SensorDeviceProcessDataFloat_T HumiditySensorDeviceData = { { { 0 } }, 1, 0, CURRENT, false };
static SensorDeviceProcessDataFloat_T PressureSensorDeviceData = { { { 0 } }, 1, 0, CURRENT, false };
/*lint -esym(956, *EnableSensor) only to be used by sensor timer task */
static bool EnableSensor = false;

void SensorDeviceEnvironment_Activate(bool enableTemperature, bool enableHumidity, bool enablePressure)
{
    bool Enable = enableTemperature || enableHumidity || enablePressure;
    if (EnableSensor != Enable)
    {
        EnableSensor = Enable;
        if (Enable)
        {
            Retcode_T environReturnValue = Environmental_init(xdkEnvironmental_BME280_Handle);
            if (RETCODE_OK != environReturnValue)
            {
                enableTemperature = false;
                enableHumidity = false;
                enablePressure = false;
            }
        }
        else
        {
            if (RETCODE_OK != Environmental_deInit(xdkEnvironmental_BME280_Handle))
            {
                printf("Failed to disable the accelerometer\r\n");
            }
        }
    }

    if (TemperatureSensorDeviceData.enabled != enableTemperature)
    {
        TemperatureSensorDeviceData.enabled = enableTemperature;
        SensorDevice_ResetProcessDataFloat(&TemperatureSensorDeviceData);
        if (enableTemperature)
        {
            ObjectTemperature_Enable(-40.0F, 85.0F); // °C	ToDo define correct BME280 values!
        }
        else
        {
            ObjectTemperature_Disable();
        }
    }

    if (HumiditySensorDeviceData.enabled != enableHumidity)
    {
        HumiditySensorDeviceData.enabled = enableHumidity;
        SensorDevice_ResetProcessDataFloat(&HumiditySensorDeviceData);
        if (enableHumidity)
        {
            ObjectHumidity_Enable(0.0F, 100.0F); // %rH	ToDo define correct BME280 values!
        }
        else
        {
            ObjectHumidity_Disable();
        }
    }

    if (PressureSensorDeviceData.enabled != enablePressure)
    {
        PressureSensorDeviceData.enabled = enablePressure;
        SensorDevice_ResetProcessDataFloat(&PressureSensorDeviceData);
        if (enablePressure)
        {
            ObjectBarometer_Enable(300.0F, 1100.0F); // hPa
        }
        else
        {
            ObjectBarometer_Disable();
        }
    }
}

void SensorDeviceEnvironment_Update(enum ProcessingMode mode, bool notify)
{
    Retcode_T ReturnValue = RETCODE_OK;
    SensorDeviceSampleDataFloat_T Sample;
    Environmental_Data_T EnvDataHolder = { INT32_C(0), UINT32_C(0), UINT32_C(0) };

    if (!EnableSensor)
        return;

    /* Read temperature, pressure, humidity current values */
    ReturnValue = Environmental_readData(xdkEnvironmental_BME280_Handle, &EnvDataHolder);

    if ( RETCODE_OK == ReturnValue)
    {
        if (TemperatureSensorDeviceData.enabled)
        {
            Sample.values[0] = TEMPERATURE_TO_C_FLOAT(EnvDataHolder.temperature);
            SensorDevice_ProcessDataFloat(mode, &TemperatureSensorDeviceData, &Sample);
        }
        if (HumiditySensorDeviceData.enabled)
        {
            Sample.values[0] = HUMIDITY_TO_PERC_FLOAT(EnvDataHolder.humidity);
            SensorDevice_ProcessDataFloat(mode, &HumiditySensorDeviceData, &Sample);
        }
        if (PressureSensorDeviceData.enabled)
        {
            Sample.values[0] = PRESSURE_TO_HPA_FLOAT(EnvDataHolder.pressure);
            SensorDevice_ProcessDataFloat(mode, &PressureSensorDeviceData, &Sample);
        }
    }
    else
    {
        printf("Environmental Read actual Data Failed\n\r");
    }
    if (notify)
    {
        if (SensorDevice_GetDataFloat(&TemperatureSensorDeviceData, &Sample))
            ObjectTemperature_SetValue((float) Sample.values[0]);
        if (SensorDevice_GetDataFloat(&HumiditySensorDeviceData, &Sample))
            ObjectHumidity_SetValue((float) Sample.values[0]);
        if (SensorDevice_GetDataFloat(&PressureSensorDeviceData, &Sample))
            ObjectBarometer_SetValue((float) Sample.values[0]);
    }
}
