/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Gyroscope.h"
#include "SensorDeviceGyroscope.h"

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

#define GYROSCOPE_TO_FLOAT(I) ((I) / 1000.0F)

static SensorDeviceProcessDataFloat_T SensorDeviceData = { { { 0 } }, 3, 0, CURRENT, false };

void SensorDeviceGyroscope_InitRand(void)
{
    bool Deinit = false;
    unsigned int Init = 0;
    Retcode_T ReturnValue = RETCODE_OK;
    Gyroscope_XyzData_T GetMdegData = { INT32_C(0), INT32_C(0), INT32_C(0) };

    if (true != xdkGyroscope_BMG160_Handle->sensorInfo.initializationStatus)
    {
        ReturnValue = Gyroscope_init(xdkGyroscope_BMG160_Handle);
        if (RETCODE_OK != ReturnValue)
            return;
        Deinit = true;
        ReturnValue = Gyroscope_readXyzDegreeValue(xdkGyroscope_BMG160_Handle, &GetMdegData);
    }

    if (RETCODE_OK == ReturnValue)
    {
        while (0 == Init)
        {
            ReturnValue = Gyroscope_readXyzDegreeValue(xdkGyroscope_BMG160_Handle, &GetMdegData);
            if (RETCODE_OK == ReturnValue)
            {
                Init = (unsigned int) (GetMdegData.xAxisData + GetMdegData.yAxisData + GetMdegData.zAxisData);
            }
        }
        printf("srand(%u)\n", Init);
        srand(Init);
    }
    if (Deinit)
    {
        ReturnValue = Gyroscope_deInit(xdkGyroscope_BMG160_Handle);
        if (RETCODE_OK != ReturnValue)
        {
            printf("Disable gyroscope failed!\n");
        }
    }
}

void SensorDeviceGyroscope_Activate(bool enable)
{
    if (SensorDeviceData.enabled == enable)
        return;
    SensorDeviceData.enabled = enable;
    SensorDevice_ResetProcessDataFloat(&SensorDeviceData);
    if (enable)
    {
        float ValueRange = 0.0F;
        Gyroscope_Range_T SensorRange;
        Retcode_T ReturnValue = Gyroscope_init(xdkGyroscope_BMG160_Handle);
        if (RETCODE_OK != ReturnValue)
            return;

        ReturnValue = Gyroscope_getRange(xdkGyroscope_BMG160_Handle, &SensorRange);
        if (RETCODE_OK != ReturnValue)
            return;

        switch (SensorRange)
        {
        case GYROSCOPE_BMG160_RANGE_125s: /**< set to 0.0625 deg/s in 125 deg/s range */
            ValueRange = 125.;
            break;
        case GYROSCOPE_BMG160_RANGE_250s: /**< set to 0.125 deg/s in 250 deg/s range */
            ValueRange = 250.;
            break;
        case GYROSCOPE_BMG160_RANGE_500s: /**< set to 0.25 deg/s in 500 deg/s range */
            ValueRange = 500.;
            break;
        case GYROSCOPE_BMG160_RANGE_1000s: /**< set to 0.5 deg/s in 1000 deg/s range */
            ValueRange = 1000.;
            break;
        case GYROSCOPE_BMG160_RANGE_2000s: /**< set to 1 deg/s in 2000 deg/s range */
            ValueRange = 2000.;
            break;
        default:
            break;
        }
        ObjectGyroscope_Enable(-ValueRange, ValueRange);
    }
    else
    {
        if (RETCODE_OK != Gyroscope_deInit(xdkGyroscope_BMG160_Handle))
        {
            printf("Disable gyroscope failed!\n");
        }
        ObjectGyroscope_Disable();
    }
}

void SensorDeviceGyroscope_Update(enum ProcessingMode mode, bool notify)
{
    if (!SensorDeviceData.enabled)
        return;

    SensorDeviceSampleDataFloat_T Sample;
    Gyroscope_XyzData_T GetMdegData = { INT32_C(0), INT32_C(0), INT32_C(0) };

    Retcode_T AdvancedApiRetValue = Gyroscope_readXyzDegreeValue(xdkGyroscope_BMG160_Handle, &GetMdegData);

    if (RETCODE_OK == AdvancedApiRetValue)
    {
        Sample.values[0] = GYROSCOPE_TO_FLOAT(GetMdegData.xAxisData);
        Sample.values[1] = GYROSCOPE_TO_FLOAT(GetMdegData.yAxisData);
        Sample.values[2] = GYROSCOPE_TO_FLOAT(GetMdegData.zAxisData);
        SensorDevice_ProcessDataFloat(mode, &SensorDeviceData, &Sample);
    }
    if (notify)
    {
        if (SensorDevice_GetDataFloat(&SensorDeviceData, &Sample))
            ObjectGyroscope_SetValues((float) Sample.values[0], (float) Sample.values[1], (float) Sample.values[2]);
    }
}

