/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*
* Contributors:
* Bosch Software Innovations GmbH
*/

/* module includes ********************************************************** */

/* own header files */
#include <Serval_Lwm2m.h>
#include "Lwm2mClient.h"
#include "Lwm2mUtil.h"
#include "SensorDeviceGyroscope.h"
#include "SntpTime.h"
#include "CfgParser.h"
#include "Lwm2mInterface.h"
#include "Lwm2mObjects.h"
#include "FotaDownload.h"
#include "ButtonsMan.h"
#include "Lwm2mObject_ConnectivityMonitoring.h"

/* system header files */
#include "BCDS_Basics.h"

/* additional interface header files */
#include <Serval_Network.h>
#include <Serval_HttpClient.h>
#include <Serval_Basics.h>
#include <Serval_Log.h>
#include <Serval_Version.h>
#include "netcfg.h"
#include "FreeRTOS.h"
#include "PAL_initialize_ih.h"
#include "PAL_socketMonitor_ih.h"
#include "BCDS_WlanConnect.h"
#include "BCDS_NetworkConfig.h"
#include "FreeRTOSConfig.h"
#include "BCDS_SDCardDriver.h"
#include "BCDS_PowerMgt.h"

/* constant definitions ***************************************************** */
#define EPN_LENGTH      UINT8_C(32)	/* serval limit! see also LWM2M-TS chap 6.2.1 needs more! */
#define MAC_LENGTH      UINT8_C(18) /* serval limit! see also LWM2M-TS chap 6.2.1 needs more! */
#define WLAN_CONTROL_TASK_STACK_SIZE        UINT8_C(512)         /**< TASK stack size for Wlan Control Task */
#define WLAN_CONTROL_TASK_PRIORITY          UINT8_C(3)           /**< Task Priority for Wlan Control Task*/
#define WLAN_FAILED_REBOOTTIME_IN_S         UINT16_C(60)         /**< reboottime in seconds, if initial wlan failed */
#define SDCARD_DRIVE_NUMBER                UINT8_C(0)           /**< SD Card Drive 0 location */
#define SDCARD_POLLING_TIME_MS    (TickType_t)5000
#define CONFIG_FILE_ERROR_TIME_MS          (TickType_t)500
#define TASK_PRIORITY                   UINT32_C(2)             /**< Macro to represent the task priority */
#define FOTA_TASK_SIZE                  UINT32_C(3048)          /**< Macro to represent the task stack size */

/* local variables ********************************************************** */

/*lint -e956 */
static char GenericEndpointName[EPN_LENGTH];
static char MacAddress[MAC_LENGTH];
static xSemaphoreHandle WlanControlSemaHandle = NULL;
static xTaskHandle WlanControlTaskHandle = NULL;
static xTimerHandle WlanControlTimerHandle = NULL;
static xTaskHandle initializationTask = NULL;

/* global variables ********************************************************* */

/* WLAN connection status */
static WlanConnect_Status_T WlanConnectStatus = WLAN_DISCONNECTED;
static NetworkConfig_IpStatus_T IpAquiredStatus = NETWORKCONFIG_IP_NOT_ACQUIRED;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

static void LogWlanVersion(void)
{
    SlVersionFull Version;
    _u8 ConfigOpt;
    _u8 ConfigLen = (_u8) sizeof(Version);
    _i32 ret;

    memset(&Version, 0, sizeof(Version));
    ConfigOpt = SL_DEVICE_GENERAL_VERSION;
    ret = sl_DevGet(SL_DEVICE_GENERAL_CONFIGURATION, &ConfigOpt, &ConfigLen, (_u8 *) (&Version));
    if (SL_OS_RET_CODE_OK == ret)
    {
        if (Version.ChipFwAndPhyVersion.ChipId & 0x10)
            printf("\nThis is a CC3200");
        else
            printf("\nThis is a CC3100");

        if (Version.ChipFwAndPhyVersion.ChipId & 0x2)
            printf("Z device\n");
        else
            printf("R device\n");

        printf("CHIP 0x%lx\nMAC 31.%lu.%lu.%lu.%lu\nPHY %u.%u.%u.%u\nNWP %lu.%lu.%lu.%lu\nROM 0x%x\nHOST %lu.%lu.%lu.%lu\n",
                Version.ChipFwAndPhyVersion.ChipId,
                Version.ChipFwAndPhyVersion.FwVersion[0], Version.ChipFwAndPhyVersion.FwVersion[1],
                Version.ChipFwAndPhyVersion.FwVersion[2], Version.ChipFwAndPhyVersion.FwVersion[3],
                Version.ChipFwAndPhyVersion.PhyVersion[0], Version.ChipFwAndPhyVersion.PhyVersion[1],
                Version.ChipFwAndPhyVersion.PhyVersion[2], Version.ChipFwAndPhyVersion.PhyVersion[3],
                Version.NwpVersion[0], Version.NwpVersion[1], Version.NwpVersion[2], Version.NwpVersion[3],
                Version.RomVersion,
                SL_MAJOR_VERSION_NUM, SL_MINOR_VERSION_NUM, SL_VERSION_NUM, SL_SUB_VERSION_NUM);
    }
    else
    {
        printf("DevGet %d\n", (int) ret);
    }
}

static bool EmptyCfg(const char *value)
{
    return (NULL == value) || (0 == *value);
}

/*lint -esym(534, AdjustIpAddress) */
static bool AdjustIpAddress(bool log)
{
    NetworkConfig_IpSettings_T MyIpSettings;
    char IpAddress[PAL_IP_ADDRESS_SIZE] = { 0 };
    Ip_Address_T* IpAddressBin = Ip_getMyIpAddr();
    Ip_Address_T NewIpAddressBin;

    if (RETCODE_OK != NetworkConfig_GetIpSettings(&MyIpSettings))
    {
        printf("Failed to get WLAN IP settings\n");
        return false;
    }

    NewIpAddressBin = Basics_ntohl(MyIpSettings.ipV4);
    if ((NewIpAddressBin != *IpAddressBin) || log)
    {
        *IpAddressBin = NewIpAddressBin;
        (void) Ip_convertAddrToString(IpAddressBin, IpAddress);
        printf("IP address of device  %s\r\n", IpAddress);
        ObjectConnectivityMonitoring_SetIpAddress(IpAddress);
        NewIpAddressBin = Basics_ntohl(MyIpSettings.ipV4Mask);
        (void) Ip_convertAddrToString(&NewIpAddressBin, IpAddress);
        printf("              Mask    %s\r\n", IpAddress);
        NewIpAddressBin = Basics_ntohl(MyIpSettings.ipV4Gateway);
        (void) Ip_convertAddrToString(&NewIpAddressBin, IpAddress);
        printf("              Gateway %s\r\n", IpAddress);
        NewIpAddressBin = Basics_ntohl(MyIpSettings.ipV4DnsServer);
        (void) Ip_convertAddrToString(&NewIpAddressBin, IpAddress);
        printf("              DNS     %s\r\n", IpAddress);
        return true;
    }
    return false;
}

static Retcode_T SetStaticIpSettings(void)
{
    retcode_t rc = RC_OK;
    const char* StaticIpSetting = NULL;
    Ip_Address_T Address;
    NetworkConfig_IpSettings_T IpSet;

    memset(&IpSet, 0, sizeof(IpSet));
    IpSet.isDHCP = (uint8_t) NETWORKCONFIG_DHCP_DISABLED;

    /* setting static IP parameters*/
    StaticIpSetting = CfgParser_GetStaticIpAddress();
    rc = Ip_convertStringToAddr(StaticIpSetting, &Address);
    if (RC_OK == rc)
    {
        printf("   STATIC IP      %s\r\n", StaticIpSetting);
        IpSet.ipV4 = Basics_htonl(Address);
    }
    /* setting static IP Dns Server parameters*/
    StaticIpSetting = CfgParser_GetDnsServerAddress();
    rc = Ip_convertStringToAddr(StaticIpSetting, &Address);
    if (RC_OK == rc)
    {
        printf("   STATIC DNS     %s\r\n", StaticIpSetting);
        IpSet.ipV4DnsServer = Basics_htonl(Address);
    }
    /* setting static IP Gateway parameters*/
    StaticIpSetting = CfgParser_GetGatewayAddress();
    rc = Ip_convertStringToAddr(StaticIpSetting, &Address);
    if (RC_OK == rc)
    {
        printf("   STATIC GATEWAY %s\r\n", StaticIpSetting);
        IpSet.ipV4Gateway = Basics_htonl(Address);
    }
    /* setting static IP subnetmask parameters*/
    StaticIpSetting = CfgParser_GetSubnetMask();
    rc = Ip_convertStringToAddr(StaticIpSetting, &Address);
    if (RC_OK == rc)
    {
        printf("   STATIC MASK    %s\r\n", StaticIpSetting);
        IpSet.ipV4Mask = Basics_htonl(Address);
    }
    return NetworkConfig_SetIpStatic(IpSet);
}

/**
 * @brief This API is called when the LWM2M Client
 *      Connecting to a WLAN Access point.
 *       This function connects to the required AP (SSID_NAME).
 *       The function will return once we are connected and have acquired IP address
 *   @warning
 *      If the WLAN connection fails or we don't acquire an IP address, We will be stuck in this function forever.
 *      Check whether the callback "SimpleLinkWlanEventHandler" or "SimpleLinkNetAppEventHandler" hits once the
 *      sl_WlanConnect() API called, if not check for proper GPIO pin interrupt configuration or for any other issue
 *
 * @retval     RC_OK       IP address returned successfully
 *
 * @retval     RC_PLATFORM_ERROR         Error occurred in fetching the ip address
 *
 */

static retcode_t WlanConnect(const char* ssid, const char* pwd)
{
    if (RETCODE_OK != WlanConnect_Init())
    {
        return (RC_PLATFORM_ERROR);
    }

    int tries = 1;
    Retcode_T rc;
    WlanConnect_SSID_T ConnectSSID;
    WlanConnect_PassPhrase_T ConnectPassPhrase;

    ConnectSSID = (WlanConnect_SSID_T) ssid;
    ConnectPassPhrase = (WlanConnect_PassPhrase_T) pwd;

    SlSecParams_t SecParams;

    /* Set network parameters */
    SecParams.Key = (signed char *) ConnectPassPhrase;
    SecParams.KeyLen = strlen((const char*) ConnectPassPhrase);
    SecParams.Type = 0x02; //WLI_SECURITY_TYPE_WPA;

    LogWlanVersion();

    if (CGF_CONDITIONAL_VALUE_YES == CfgParser_IsStaticModeActivated())
    {
        printf("STATIC IP\r\n");
        if (RETCODE_OK != SetStaticIpSettings())
        {
            printf("Error in setting static IP\n\r");
            return (RC_PLATFORM_ERROR);
        }
    }
    else
    {
        printf("DHCP\r\n");
        if (RETCODE_OK != NetworkConfig_SetIpDhcp(NULL))
        {
            printf("Error in setting DHCP\n\r");
            return (RC_PLATFORM_ERROR);
        }
    }

    printf("Start connecting to WPA network\r\n");

    rc = WlanConnect_WPA(ConnectSSID, ConnectPassPhrase, NULL);
    while ((RETCODE_ERROR_IP_NOT_ACQ == Retcode_getCode(rc) || RETCODE_ERROR_WRONG_PASSWORD == Retcode_getCode(rc))
            && 5 >= tries)
    {
        /* retry, sometimes RETCODE_ERROR_IP_NOT_ACQ or RETCODE_ERROR_WRONG_PASSWORD are reported */
        rc = WlanConnect_WPA(ConnectSSID, ConnectPassPhrase, NULL);
        ++tries;
    }
    if (RETCODE_OK == rc)
    {
        int SlRet;
        printf("Connected to WPA network successfully\r\n");
        SlRet = sl_WlanProfileDel(0xFF);
        if (SL_RET_CODE_OK > SlRet)
        {
            printf("  WLAN: failed to delete all profiles\n\r");
        }
        SlRet = sl_WlanProfileAdd((signed char *) ConnectSSID, strlen((const char*) ConnectSSID), 0, &SecParams, 0, 1, 0);
        if (SL_RET_CODE_OK > SlRet)
        {
            printf("  WLAN: failed to add profile\n\r");
        }
        SlRet = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(0, 0, 0, 0, 0), NULL, 0); // reset
        if (SL_RET_CODE_OK > SlRet)
        {
            printf("  WLAN: failed to reset connection policy\n\r");
        }
        SlRet = sl_WlanPolicySet(SL_POLICY_CONNECTION, SL_CONNECTION_POLICY(1, 1, 0, 0, 0), NULL, 0); // autoreconnect, fast
        if (SL_RET_CODE_OK > SlRet)
        {
            printf("  WLAN: failed to set connection policy\n\r");
        }
        SlRet = sl_WlanRxStatStart();
        if (SL_RET_CODE_OK > SlRet)
        {
            printf("  WLAN: failed to start Rx Statistic\n\r");
        }
        AdjustIpAddress(true);
        return (RC_OK);
    }
    else
    {
        if (RETCODE_ERROR_WRONG_PASSWORD == Retcode_getCode(rc))
        {
            printf("  WLAN: connect WPA failed! Wrong password.\n\r");
        }
        else if (RETCODE_ERROR_IP_NOT_ACQ == Retcode_getCode(rc))
        {
            printf("  WLAN: connect WPA failed! IP not acquired. (%d tries)\n\r", tries);
        }
        else
        {
            printf("  WLAN: connect WPA failed %lx %lu\n\r", (long unsigned) rc, (long unsigned) Retcode_getCode(rc));
        }

        return (RC_PLATFORM_ERROR);
    }
}

static const char* GetWlanConnectDescription(WlanConnect_Status_T Status)
{
    switch (Status)
    {
    case WLAN_CONNECTED:
        return "online";
    case WLAN_DISCONNECTED:
        return "offline";
    case WLAN_CONNECTION_ERROR:
        return "error";
    case WLAN_CONNECTION_PWD_ERROR:
        return "pwd error";
    case WLAN_DISCONNECT_ERROR:
        return "disconnect error";
    }
    return NULL;
}

static void WlanControl(void)
{
    bool log = false;
    WlanConnect_Status_T Status = WlanConnect_GetStatus();
    if (WlanConnectStatus != Status)
    {
        const char* Description = GetWlanConnectDescription(Status);
        WlanConnectStatus = Status;
        if (NULL == Description)
        {
            printf("WLAN: connect status: --- unknown %d ---\n\r", Status);
        }
        else
        {
            printf("WLAN: connect status: --- %s ---\n\r", Description);
        }
        if (WlanConnectStatus != WLAN_CONNECTED)
        {
            *Ip_getMyIpAddr() = *Ip_getUnspecifiedAddr();
            IpAquiredStatus = NETWORKCONFIG_IP_NOT_ACQUIRED;
            printf("IP Address of the Device is: --.--.--.--\r\n");
            ObjectConnectivityMonitoring_SetIpAddress("");
        }
    }
    if (WlanConnectStatus == WLAN_CONNECTED)
    {
        NetworkConfig_IpStatus_T IpStatus = NetworkConfig_GetIpStatus();
        if (IpAquiredStatus != IpStatus)
        {
            IpAquiredStatus = IpStatus;
            log = true;
        }
        if (NETWORKCONFIG_IPV4_ACQUIRED == IpStatus)
        {
            AdjustIpAddress(log);
        }
    }
    SlGetRxStatResponse_t RxStatResp;
    if (SL_RET_CODE_OK == sl_WlanRxStatGet(&RxStatResp, 0))
    {
        ObjectConnectivityMonitoring_SetRadioSignalStrength(RxStatResp.AvarageDataCtrlRssi);
    }
}

static void WlanControlTask(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);

    for (;;)
    {
        if (xSemaphoreTake(WlanControlSemaHandle, portMAX_DELAY) == pdTRUE)
        {
            WlanControl();
        }
        else
        {
            assert(0);
        }
    }
}

static void WlanControlTimerCB(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);
    /*lint -e(534) also OK, if sema is already given */
    xSemaphoreGive(WlanControlSemaHandle);
}

static void InitWlanControl(void)
{
    uint32_t Ticks = 10000 / portTICK_RATE_MS;
    /**create a binary semaphore*/
    WlanControlSemaHandle = xSemaphoreCreateBinary();
    if (NULL == WlanControlSemaHandle)
    {
        /* Failed to create semaphore */
        assert(0);
    }
    if (xSemaphoreGive(WlanControlSemaHandle) != pdTRUE)
    {
        /* Failed to Semaphore Give */
        assert(0);
    }
    /** Create a task */
    if (xTaskCreate(WlanControlTask, (const char * const) "WLanControlTask", WLAN_CONTROL_TASK_STACK_SIZE, NULL, WLAN_CONTROL_TASK_PRIORITY, // 224: 24Bytes remaining
            &WlanControlTaskHandle) != pdPASS)
    {
        assert(0);
    }

    /** this is the timer (10sec) task which signals the task using semaphore() */
    WlanControlTimerHandle = xTimerCreate((const char * const ) "wlanControlTimer", Ticks, pdTRUE,
    NULL, WlanControlTimerCB);
    if (NULL == WlanControlTimerHandle) /* Timer was not created */
    {
        assert(0);
    }
    if (xTimerStart(WlanControlTimerHandle, portMAX_DELAY) != pdTRUE)
    {
        assert(0);
    }
}

/** @brief This function does the initialization of required setup to interact with lwm2m Server and the objects that are defined for this project.
 *  It includes providing the necessary data for the objects like object and resource ID, object name,
 *  its instances.
 */
static void Init(void* param)
{
    BCDS_UNUSED(param);
    Lwm2mConfiguration_T Configuration;
    Configuration.localPort = CLIENT_CONNECT_PORT;
    bool EnableConNotifies = FALSE;
    uint8_t TestMode = 0;
    const char* Ssid = NULL;
    const char* Pwd = NULL;
    const char* Endpoint = NULL;
    const char* Binding = "U";
    uint8_t WlanMacBytes[SL_MAC_ADDR_LEN];
    uint8_t WlanMacBytesLength = SL_MAC_ADDR_LEN;
    int32_t ret;
    retcode_t rc = RC_OK;
    Retcode_T ReturnValue = RETCODE_FAILURE;
	Retcode_T DiskInitStatus = SDCARD_NOT_INITIALIZED;

    /* Startup delay to allow to Serial Port */
    vTaskDelay((SERIAL_CONNECT_TIME_INTERVAL / portTICK_RATE_MS));

    if (SDCARD_INSERTED == SDCardDriver_GetDetectStatus())
    {
        DiskInitStatus = SDCardDriver_DiskInitialize(SDCARD_DRIVE_NUMBER); /* Initialize SD card */
        if (DiskInitStatus != RETCODE_OK)
        {
            printf("disk initialization failed");
            SDCardDriver_Disconnect();
            assert(false);
        }
        printf("Success in initializing the SD card disk \r\n");
        ReturnValue = CfgParser_ParseConfigFile();
        if (RETCODE_OK == ReturnValue)
        {
            printf("Config file is not Correct Please give a proper Config file and reboot the device\n\r");
        }
    }
    FotaInit();
    ObjectConnectivityMonitoring_Init();
    SensorDeviceGyroscope_InitRand();
    EnableConNotifies = CfgParser_UseLwm2mConNotifies();
    TestMode = CfgParser_TestMode();
    Ssid = CfgParser_GetWlanSSID();
    Pwd = CfgParser_GetWlanPassword();
    Configuration.lifetime = CfgParser_GetLwm2mLifetime();
    Configuration.destServer = CfgParser_GetLwm2mServerAddress();
    Endpoint = CfgParser_GetLwm2mEndpointName();
    if (EmptyCfg(Ssid))
    {
        Ssid = WLAN_CONNECT_WPA_SSID;
    }
    else
    {
        printf("SSID from SDCARD\r\n");
    }
    if (EmptyCfg(Pwd))
    {
        Pwd = WLAN_CONNECT_WPA_PASS;
    }
    else
    {
        printf("WLAN-PWD from SDCARD\r\n");
    }
    if (EmptyCfg(Configuration.destServer))
    {
#if SECURITY_ENABLE == 0
        Configuration.sdCardConfig = false;
        Configuration.destServer = LWM2M_SERVER_IP;
#else
        Configuration.sdCardConfig = false;
        Configuration.destServer = LWM2M_SECURE_SERVER_IP;
#endif
    }
    else
    {
        Configuration.sdCardConfig = true;
        printf("LWM2M default Server from SDCARD\r\n");
    }

    printf("INIT Starting up\r\n");

    printf("wlanConnect\r\n");
    rc = WlanConnect(Ssid, Pwd);
    if (RC_OK != rc)
    {
        printf("Network init/connection failed " RC_RESOLVE_FORMAT_STR "\n", RC_RESOLVE(rc));
        vTaskDelay((WLAN_FAILED_REBOOTTIME_IN_S * 1000) / portTICK_PERIOD_MS);
        Lwm2mInterfaceRebootNow("NETWORK FAILED!");
        return;
    }
    printf("Network Initialization done\r\n");

    ret = sl_NetCfgGet((_u8) SL_MAC_ADDRESS_GET, NULL, &WlanMacBytesLength, WlanMacBytes);
    if (SL_RET_CODE_OK > ret)
    {
        printf("WLAN MAC status: %d\r\n", (int) ret);
        return;
    }
    snprintf(MacAddress, sizeof(MacAddress), "%02X-%02X-%02X-%02X-%02X-%02X", WlanMacBytes[0], WlanMacBytes[1], WlanMacBytes[2], WlanMacBytes[3], WlanMacBytes[4], WlanMacBytes[5]);
    printf("WLAN got MAC: %s\r\n", MacAddress);

    rc = PAL_initialize();
    if (RC_OK != rc)
    {
        printf("PAL and network initialize %i \r\n", rc);
        return;
    }
    printf("PAL Initialization done\r\n");

    LOG_INFO("Logging enabled\r\n");

    PAL_socketMonitorInit();
    printf("PAL SocketMonitor done\n");

    Configuration.useDtlsPsk = CfgParser_ActivateDtlsPsk();

    if (EmptyCfg(Endpoint))
    {
        memset(GenericEndpointName, 0, sizeof(GenericEndpointName));
        strncpy(GenericEndpointName, MacAddress, sizeof(GenericEndpointName) - 1);
        if (Configuration.useDtlsPsk)
        {
            const char * suffix = CfgParser_GetLwm2mDtlsSuffix();
            strncat(GenericEndpointName, suffix, sizeof(GenericEndpointName) - 1 - strlen(GenericEndpointName));
        }
        Endpoint = GenericEndpointName;
    }
    else
        printf("LWM2M endpoint name from SDCARD\r\n");

    if (Configuration.useDtlsPsk)
    {
        Configuration.pskIdentity = CfgParser_GetDtlsPskIdentity();
        if (EmptyCfg(Configuration.pskIdentity))
        {
            Configuration.pskIdentity = Endpoint;
        }
        printf("DTLS/PSK identity   %s\r\n", Configuration.pskIdentity);
        Configuration.pskSecretKey = CfgParser_GetDtlsPskSecretKey();
        if (EmptyCfg(Configuration.pskSecretKey))
        {
            /* Need to provide the SecretKey whenever trying to registering with the security this
             * conditional check will avoid the user to understand the problem while serval restrict to register without
             * secretkey */
            printf("Please Provide the Secuirtykey\r\n");
            assert(false);
        }
        printf("DTLS/PSK secretKey  %s\r\n", Configuration.pskSecretKey);
    }
    Binding = CfgParser_GetLwm2mDeviceBinding();

    printf("Serval %d.%d.%d %s %s\r\n", SERVAL_VERSION_MAJOR, SERVAL_VERSION_MINOR, SERVAL_VERSION_PATCH, __DATE__, __TIME__);
    printf("Lwm2m Interface endpointName: %s, binding: %s, notifies: %s\r\n", Endpoint, Binding, EnableConNotifies ? "CON" : "NON");
    rc = Lwm2mInterfaceInitialize(Endpoint, MacAddress, Binding, Configuration.useDtlsPsk, EnableConNotifies, TestMode);
    if (RC_OK != rc)
    {
        printf("LWM2M Interface Failed to Initialize %i \r\n", rc);
        return;
    }

    InitWlanControl();
    if (SUCCESS != InitButtonMan(CFG_TESTMODE_OFF != TestMode))
    {
        printf("ButtonManagerFailed to Initialize\r\n");
    }

    InitSntpTime();

    rc = Lwm2mInterfaceStart(&Configuration);
    if (RC_OK == rc)
    {
        printf("Lwm2m Interface started, port: %u\r\n", Configuration.localPort);
    }
    else
    {
        printf("LWM2M Interface Failed to Initialize %i \r\n", rc);
    }
    if (xTaskCreate(FotaTask, (const char * const ) "FotaTask",FOTA_TASK_SIZE,NULL, TASK_PRIORITY, NULL) != pdPASS)
    {
        assert(false);
    }
    vTaskDelete(initializationTask);

}

/* global functions ********************************************************** */

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{
    (void) (xTimer);
    Retcode_T ReturnValue = (Retcode_T) RETCODE_FAILURE;
    /* Initialize SD card */
    ReturnValue = SDCardDriver_Init();

    if (RETCODE_OK != ReturnValue)
    {
        printf("SDCardDriver_Init failed or Led init failed or Leakage sensor init failed or or Accel sensor init failed %x !!\r\n", (unsigned int) ReturnValue);
        assert(false);
    }
    /*initialize LCA module*/
    if (xTaskCreate(Init, (const char * const ) "CheckSDcardInsertedTask",TASK_STACK_SIZE,NULL, TASK_PRIORITY, &initializationTask) != pdPASS)
    {
        assert(false);
    }
}
/** ************************************************************************* */
