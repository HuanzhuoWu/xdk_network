/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "SensorDeviceUtil.h"

void SensorDevice_ResetProcessDataFloat(SensorDeviceProcessDataFloat_T* data)
{
    int Index;
    for (Index = 0; Index < data->value_counter; ++Index)
    {
        data->data.values[Index] = 0.0;
    }
    data->sample_counter = 0;
}

void SensorDevice_ProcessDataFloat(ProcessingMode_T mode, SensorDeviceProcessDataFloat_T* data, SensorDeviceSampleDataFloat_T* sample)
{
    int Index;

    if (!data->enabled)
        return;

    if (data->mode != mode)
    {
        data->mode = mode;
        SensorDevice_ResetProcessDataFloat(data);
    }
    if (0 == data->sample_counter)
    {
        if (MAX == mode || MIN == mode)
            mode = CURRENT;
    }

    ++data->sample_counter;
    switch (mode)
    {
    case MAX:
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            if (data->data.values[Index] < sample->values[Index])
                data->data.values[Index] = sample->values[Index];
        }
        break;
    case MIN:
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            if (data->data.values[Index] > sample->values[Index])
                data->data.values[Index] = sample->values[Index];
        }
        break;
    case CURRENT:
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            data->data.values[Index] = sample->values[Index];
        }
        break;
    case AVG:
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            data->data.values[Index] += sample->values[Index];
        }
        break;
    }
}

bool SensorDevice_GetDataFloat(SensorDeviceProcessDataFloat_T* data, SensorDeviceSampleDataFloat_T* sample)
{
    int Index;
    double Counter;

    if (!data->enabled || 0 == data->sample_counter)
        return false;
    switch (data->mode)
    {
    case MAX:
        case MIN:
        case CURRENT:
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            sample->values[Index] = data->data.values[Index];
        }
        break;
    case AVG:
        Counter = data->sample_counter;
        for (Index = 0; Index < data->value_counter; ++Index)
        {
            sample->values[Index] = data->data.values[Index] / Counter;
        }
        break;
    }
    SensorDevice_ResetProcessDataFloat(data);
    return true;
}
