/**
 * This software is copyrighted by Bosch Connected Devices and Solutions GmbH, 2016.
 * The use of this software is subject to the XDK SDK EULA
 *
 * Contributors:
 * Bosch Software Innovations GmbH
 */
/* own header files */
#include "Lwm2mObject_Illuminance.h"
#include "SensorDeviceIlluminance.h"

/* system header files */
#include <stdio.h>

/* additional interface header files */
#include "XdkSensorHandle.h"
#include <Serval_Exceptions.h>
#include <Serval_Lwm2m.h>

static SensorDeviceProcessDataFloat_T SensorDeviceData = { { { 0 } }, 1, 0, CURRENT, false };

void SensorDeviceIlluminance_Activate(bool enable)
{
    if (SensorDeviceData.enabled == enable)
        return;
    SensorDeviceData.enabled = enable;
    SensorDevice_ResetProcessDataFloat(&SensorDeviceData);
    if (enable)
    {
        float MinRange = 0.0F;
        float MaxRange = 188000000.F;
        Retcode_T ReturnValue = LightSensor_init(xdkLightSensor_MAX44009_Handle);
        vTaskDelay((portTickType) 200 / portTICK_RATE_MS);
        if (RETCODE_OK != ReturnValue)
            return;
        ObjectIlluminance_Enable(MinRange, MaxRange);
    }
    else
    {
        if (RETCODE_OK != LightSensor_deInit(xdkLightSensor_MAX44009_Handle))
        {
            printf("Disable light sensor failed!\n");
        }
        ObjectIlluminance_Disable();
    }
}

void SensorDeviceIlluminance_Update(enum ProcessingMode mode, bool notify)
{
    if (!SensorDeviceData.enabled)
        return;

    SensorDeviceSampleDataFloat_T Sample;
    uint32_t GetLightData = INT32_C(0);

    Retcode_T AdvancedApiRetValue = LightSensor_readLuxData(xdkLightSensor_MAX44009_Handle, &GetLightData);
    if (RETCODE_OK == AdvancedApiRetValue)
    {
        Sample.values[0] = (float) GetLightData;
        SensorDevice_ProcessDataFloat(mode, &SensorDeviceData, &Sample);
    }
    if (notify)
    {
        if (SensorDevice_GetDataFloat(&SensorDeviceData, &Sample))
            ObjectIlluminance_SetValue((float) Sample.values[0]);
    }
}
