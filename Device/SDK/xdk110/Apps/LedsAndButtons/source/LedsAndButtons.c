/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* system header files */
#include "BCDS_Basics.h"
#include <stdio.h>
/* additional interface header files */
#include <FreeRTOS.h>
#include <timers.h>
#include "led.h"
#include "button.h"
#include "XdkBoardHandle.h"

/* own header files */
#include "LedsAndButtons.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static LED_handle_tp redLedHandle = (LED_handle_tp) NULL; /**< variable to store red led handle */
static LED_handle_tp yellowLedHandle = (LED_handle_tp) NULL; /**< variable to store yellow led handle */
static LED_handle_tp orangeLedHandle = (LED_handle_tp) NULL; /**< variable to store orange led handle */
static BUTTON_handle_tp Button1Handle = (BUTTON_handle_tp) NULL; /**< variable to store button 1 handle */
static BUTTON_handle_tp Button2Handle = (BUTTON_handle_tp) NULL; /**< variable to store button 2 handle */
/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief     this callback will get triggered whenever any one of button is pressed
 * @param[in] handle handle of the push button
 * @param[in] userParameter parameter of the corresponding handle
 */
static void callback(void *handle, uint32_t userParameter)
{
    switch (userParameter)
    {
    /*  Button 1 press/release */
    case CALLBACK_PARAMETER_PB1:
        if (BUTTON_isPressed(handle))
        {
            LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;
            returnValue = LED_setState(redLedHandle, LED_SET_ON);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(orangeLedHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(yellowLedHandle, LED_SET_ON);
            }
            if (LED_ERROR_OK == returnValue)
            {
                printf("PB1 pressed\n\r");
            }
            else
            {
                printf("PB1 pressed but setting LED state failed\n\r");
            }
        }
        if (BUTTON_isReleased(handle))
        {
            LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;

            returnValue = LED_setState(redLedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(orangeLedHandle, LED_SET_ON);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(yellowLedHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                printf("PB1 released\n\r");
            }
            else
            {
                printf("PB1 released but setting LED state failed\n\r");
            }
        }
        break;

        /* Button 2 press/release */
    case CALLBACK_PARAMETER_PB2:
        if (BUTTON_isPressed(handle))
        {
            LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;

            returnValue = LED_setState(redLedHandle, LED_SET_ON);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(orangeLedHandle, LED_SET_ON);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(yellowLedHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                printf("PB2 pressed\n\r");
            }
            else
            {
                printf("PB2 pressed but setting LED state failed\n\r");
            }
        }
        if (BUTTON_isReleased(handle))
        {
            LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;

            returnValue = LED_setState(redLedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(orangeLedHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(yellowLedHandle, LED_SET_ON);
            }
            if (LED_ERROR_OK == returnValue)
            {
                printf("PB2 released\n\r");
            }
            else
            {
                printf("PB2 released but setting LED state failed\n\r");
            }
        }
        break;
    default:
        printf("Button not available \n\r");
        break;
    }
}
/**
 * @brief This will create handles for the LEDs
 *
 * @retval FAILURE Led Handle is not created
 * @retval SUCCESS Led Handle is created
 *
 */
static return_t createLed(void)
{
    return_t returnValue = FAILURE;

    redLedHandle = LED_create(gpioRedLed_Handle, GPIO_STATE_OFF);
    if (redLedHandle != NULL)
    {
        yellowLedHandle = LED_create(gpioYellowLed_Handle, GPIO_STATE_OFF);
    }
    if (yellowLedHandle != NULL)
    {
        orangeLedHandle = LED_create(gpioOrangeLed_Handle, GPIO_STATE_OFF);
    }
    if (orangeLedHandle != NULL)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/**
 * @brief This will create the handles for button
 *
 * @retval FAILURE Button Handle is not created
 * @retval SUCCESS Button Handle is created
 */
static return_t createButton(void)
{
    return_t returnValue = FAILURE;

    Button1Handle = BUTTON_create(gpioButton1_Handle, GPIO_STATE_OFF);
    if (Button1Handle != NULL)
    {
        Button2Handle = BUTTON_create(gpioButton2_Handle, GPIO_STATE_OFF);
    }
    if (Button2Handle != NULL)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/**
 * @brief This will enable the  button
 *
 * @retval FAILURE Button Handle is not enabled
 * @retval SUCCESS Button Handle is enabled
 */
static return_t enableButton(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;
    buttonReturn = BUTTON_enable(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_enable(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/**
 * @brief This will set the callback for the button
 *
 * @retval FAILURE Callback set failed
 * @retval SUCCESS Callback set successfully
 */
static return_t setButtonCallback(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_setCallback(Button1Handle, callback, CALLBACK_PARAMETER_PB1);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_setCallback(Button2Handle, callback, CALLBACK_PARAMETER_PB2);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/* global functions ********************************************************* */
/**
 * @brief This will create and enable both LED and BUTTON handle
 *
 * @retval FAILURE led and button initialization failed
 * @retval SUCCESS led and button initialization success
 */
return_t ledAndButtonInit(void)
{
    return_t returnValue = FAILURE;

    returnValue = createLed();
    if (returnValue == SUCCESS)
    {
        returnValue = createButton();
    }
    else
    {
        printf("Error in creating LED\n\r");
    }
    if (returnValue == SUCCESS)
    {
        returnValue = enableButton();
    }
    else
    {
        printf("Error in creating button\n\r");

    }
    if (returnValue == SUCCESS)
    {
        returnValue = setButtonCallback();
    }
    else
    {
        printf("Error in enabling button\n\r");
    }
    return (returnValue);
}

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{
    return_t returnValue = FAILURE;
    BCDS_UNUSED(xTimer);
    returnValue = ledAndButtonInit();
    if (returnValue != SUCCESS)
    {
        printf("Error in led and button initialization\n\r");
    }
}

/**
 * @brief This will delete both LED and BUTTON handles
 *
 * @retval FAILURE led and button de-initialization failed
 * @retval SUCCESS led and button de-initialization success
 */
return_t ledAndButtonDeinit(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;
    LED_errorTypes_t ledReturn = LED_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_delete(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_delete(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        ledReturn = LED_delete(redLedHandle);
    }
    if (ledReturn == LED_ERROR_OK)
    {
        ledReturn = LED_delete(yellowLedHandle);
    }
    if (ledReturn == LED_ERROR_OK)
    {
        ledReturn = LED_delete(orangeLedHandle);
    }
    if (ledReturn == LED_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

/** ************************************************************************* */
