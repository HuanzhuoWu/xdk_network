/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include "BCDS_Basics.h"

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "OrientationData.h"
#include "CompassData.h"
#include "HumidityData.h"
#include "StepCounter.h"
#include "CalibratedSensor.h"
#include "MagneticFingerPrint.h"

/* own header files */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
typedef enum printToolboxData_e
{
    PRINT_ORIENTATION ,/**< Prints Virtual Orientation Sensor data */
    PRINT_COMPASS,/**< Prints Virtual Compass Sensor data */
    PRINT_HUMIDITY,/**< Prints Virtual Humidity Sensor data */
    PRINT_CALIBRATED_SENSORS, /**<Prints Calibrated Sensors data */
    PRINT_STEP_COUNTER,/**< Prints Step counter data */
    MAG_FINGER_PRINT, /**< Magnetic finger print Sensor application */
} printToolboxData_t;

/* global variables ********************************************************* */
static printToolboxData_t sensorToPrint = PRINT_ORIENTATION; /**< variable which indicates which virtual sensor data will be printed */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************** */

/**
 * @brief The function initializes printToolboxData which calls the individual virtual sensor data print functions
 */
void init(void)
{
    switch (sensorToPrint)
    {
    case PRINT_ORIENTATION:

        OrientationSensorInit();
        break;

    case PRINT_COMPASS:

        compassInit();
        break;

    case PRINT_HUMIDITY:

        humiditySensorInit();
        break;

    case PRINT_STEP_COUNTER:

        stepCounterSensorInit();
        break;

    case PRINT_CALIBRATED_SENSORS:

        calibratedSensorInit();
        break;

    case MAG_FINGER_PRINT:

        magneticFingerPrintInit();
        break;

    default:
        break;
    }
}

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{
    BCDS_UNUSED(xTimer);
    init();
}

/**
 *  @brief API to deinitialize the PTB module
 */
void deinit(void)
{

    switch (sensorToPrint)
    {
    case PRINT_ORIENTATION:

        OrientationSensorDeinit();
        break;

    case PRINT_COMPASS:

        compassDeinit();
        break;

    case PRINT_HUMIDITY:

        humiditySensorDeinit();
        break;

    case PRINT_STEP_COUNTER:

        stepCounterSensorDeinit();
        break;

    case PRINT_CALIBRATED_SENSORS:

        calibratedSensorDeinit();
        break;

    case MAG_FINGER_PRINT:

        magneticFingerPrintDeinit();
        break;

    default:
        break;
    }
}

/** ************************************************************************* */

