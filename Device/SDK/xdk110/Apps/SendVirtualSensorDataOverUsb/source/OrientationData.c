/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* own header files */
#include "OrientationData.h"

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Orientation.h"
#include "BCDS_Assert.h"
#include "XdkSensorHandle.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
#define DELAY  UINT32_C(20)      /**< Timer delay is represented by this macro */
#define TIMERBLOCKTIME  UINT32_C(0xffff)    /**< Macro used to define blocktime of a timer*/
#define TIMER_NOT_ENOUGH_MEMORY            (-1L)/**<Macro to define not enough memory error in timer*/
#define TIMER_AUTORELOAD_ON             UINT32_C(1)             /**< Auto reload of timer is enabled*/

/** Enum values represent the type of orientation data to be printed */
typedef enum printOrientationData_e
{
    PRINT_ORIENTATION_EULER_DEG = 0,/**< Prints Orientation Euler Value in Degrees */
    PRINT_ORIENTATION_EULER_RAD,/**< Prints Orientation Euler Value in Radians */
    PRINT_ORIENTATION_QUATERNION,/**< Prints Orientation Quaternion values */
    PRINT_ORIENTATION_ALL, /**< Prints Orientation values ( Quaternion value, Euler value in Radians, Euler value in Degrees ) */
} printOrientationData_t;

static xTimerHandle printTimerHandle = 0;/**< variable to store timer handle*/
static printOrientationData_t dataToPrint = PRINT_ORIENTATION_QUATERNION;/**< variable which indicates what data of orientation sensor is to be printed */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief Gets the Orientation value and prints through the USB printf on serial port
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
static void printOrientationData(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);
    Orientation_EulerData_T eulerValuesInDeg, eulerValuesInRad;
    Orientation_QuaternionData_T quaternionValue;
    Retcode_T orientationReturnStatus =(Retcode_T) RETCODE_FAILURE;

    /* initializing structure elements */
    memset(&eulerValuesInDeg, 0, sizeof(Orientation_EulerData_T));
    memset(&eulerValuesInRad, 0, sizeof(Orientation_EulerData_T));
    memset(&quaternionValue, 0, sizeof(Orientation_QuaternionData_T));

    switch (dataToPrint)
    {

    case PRINT_ORIENTATION_QUATERNION:

        orientationReturnStatus = Orientation_readQuaternionValue(&quaternionValue);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("QUATERNION : %3.2f %3.2f %3.2f %3.2f\n\r",
                    quaternionValue.w, quaternionValue.x, quaternionValue.y, quaternionValue.z);
        }
        else
        {
            printf("Orientation Quaternion Value Read failed \n\r");
        }
        break;

    case PRINT_ORIENTATION_EULER_RAD:

        orientationReturnStatus = Orientation_readEulerRadianVal(&eulerValuesInRad);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("EULER (RADIANS) : %3.2f %3.2f %3.2f\n\r",
                    eulerValuesInRad.pitch, eulerValuesInRad.roll, eulerValuesInRad.heading);
        }
        else
        {
            printf("Orientation Euler Value in Radian Read failed \n\r");
        }
        break;

    case PRINT_ORIENTATION_EULER_DEG:

        orientationReturnStatus = Orientation_readEulerDegreeVal(&eulerValuesInDeg);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("EULER (DEGREES) : %3.2f %3.2f %3.2f\n\r",
                    eulerValuesInDeg.pitch, eulerValuesInDeg.roll, eulerValuesInDeg.heading);
        }
        else
        {
            printf("Orientation Euler Value in Degree Read failed \n\r");
        }
        break;

    case PRINT_ORIENTATION_ALL:
        {

        orientationReturnStatus = Orientation_readQuaternionValue(&quaternionValue);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("QUATERNION : %3.2f %3.2f %3.2f %3.2f\n\r",
                    quaternionValue.w, quaternionValue.x, quaternionValue.y, quaternionValue.z);
        }
        else
        {
            printf("Orientation Quaternion Value Read failed \n\r");
        }

        orientationReturnStatus = Orientation_readEulerRadianVal(&eulerValuesInRad);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("EULER (RADIANS) : %3.2f %3.2f %3.2f\n\r",
                    eulerValuesInRad.pitch, eulerValuesInRad.roll, eulerValuesInRad.heading);
        }
        else
        {
            printf("Orientation Euler Value in Radian Read failed \n\r");
        }

        orientationReturnStatus = Orientation_readEulerDegreeVal(&eulerValuesInDeg);
        if (RETCODE_OK == orientationReturnStatus)
        {
            /* Print orientation information */
            printf("EULER (DEGREES) : %3.2f %3.2f %3.2f\n\r",
                    eulerValuesInDeg.pitch, eulerValuesInDeg.roll, eulerValuesInDeg.heading);
        }
        else
        {
            printf("Orientation Euler Value in Degree Read failed \n\r");
        }
    }
        break;

    default:
        break;
    }
}

/* global functions ********************************************************* */

/* API documentation is in the header */
void OrientationSensorInit(void)
{
    int8_t retValPerSwTimer = TIMER_NOT_ENOUGH_MEMORY;
    Retcode_T orientationInitStatus = (Retcode_T)RETCODE_FAILURE;
    orientationInitStatus = Orientation_init(xdkOrientationSensor_Handle);

    if (RETCODE_OK == orientationInitStatus)
    {
        uint32_t Ticks = DELAY;

        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            Ticks = UINT32_C(1);
        }
        /* create timer task to get and print orientation data periodically */
        printTimerHandle = xTimerCreate((const char * const ) "printOrientationData",
                Ticks, TIMER_AUTORELOAD_ON, NULL, printOrientationData);

        /* POD timer create fail case */
        if (NULL == printTimerHandle)
        {
           /* Assertion Reason: "This software timer was not Created, Due to Insufficient heap memory" */
            assert(false);
        }

        /* Start the created timer*/
        retValPerSwTimer = xTimerStart(printTimerHandle, TIMERBLOCKTIME);

        /* POD timer start fail case */
        if (TIMER_NOT_ENOUGH_MEMORY == retValPerSwTimer)
        {
           /*Assertion reason: "This software timer was not started, Timer could not be set to active state" */
            assert(false);
        }
    }
    else
    {
        printf("Orientation initialisation failed \n\r");
    }
}

/* API documentation is in the header */
void OrientationSensorDeinit(void)
{
    Retcode_T orientationReturnStatus = (Retcode_T)RETCODE_FAILURE;
    /* Checking if the timer handler is valid (not NULL)*/
   /* Assertion Reason: "Orientation Sensor Timer is not valid." */
    assert(printTimerHandle != NULL);

    /* Deinitialise orientation module */
    orientationReturnStatus = Orientation_deInit();

    if (RETCODE_OK == orientationReturnStatus)
    {
        /* Stop the created POD timer */
        if(xTimerStop(printTimerHandle, TIMERBLOCKTIME)==pdFAIL)
        {
            assert(false);
        }
    }
    else
    {
        printf("Orientation deinitialisation failed \n\r");
    }

}

/** ************************************************************************* */

