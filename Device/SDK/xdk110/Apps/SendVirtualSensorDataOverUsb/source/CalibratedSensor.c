/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* own header files */
#include "CalibratedSensor.h"

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_CalibratedAccel.h"
#include "BCDS_CalibratedGyro.h"
#include "BCDS_CalibratedMag.h"
#include "BCDS_Assert.h"
#include "XdkSensorHandle.h"
#include "XdkBoardHandle.h"
#include "led.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
#define DELAY  UINT32_C(50)      /**< Timer delay is represented by this macro */
#define TIMERBLOCKTIME  UINT32_C(0xffff)    /**< Macro used to define blocktime of a timer*/
#define CALIBRATION_DONE UINT8_C(1) /**<Represents calibration done status */
#define CALIBRATION_NOT_DONE UINT8_C(0) /**<Represents calibration not done status */
#define TIMER_NOT_ENOUGH_MEMORY            (-1L)/**<Macro to define not enough memory error in timer*/
#define TIMER_NO_ERROR            (0L)/**<Macro to define not enough memory error in timer*/
#define TIMER_AUTORELOAD_ON             UINT32_C(1)             /**< Auto reload of timer is enabled*/

/** Enum values represent the type of calibrated sensor data to be printed */
typedef enum printData_e
{
    PRINT_CALIBRATED_ACCEL_DATA,/**< Prints calibrated accel data */
    PRINT_CALIBRATED_GYRO_DATA,/**< Prints calibrated gyro data  */
    PRINT_CALIBRATED_MAG_DATA, /**< Prints calibrated mag data */
} printData_t;

/** Enum values represent the calibration Status of sensor */
typedef enum calibrationStatus_e
{
    CALIBRATION_STATUS_UNRELIABLE,/**< unreliable calibration sensor Status of sensor */
    CALIBRATION_STATUS_LOW,/**< low calibration sensor Status of sensor  */
    CALIBRATION_STATUS_MEDIUM,/**< medium calibration sensor Status of sensor */
    CALIBRATION_STATUS_HIGH, /**< low calibration sensor Status of sensor */
} calibrationStatus_t;

static uint8_t isCalibrated = CALIBRATION_NOT_DONE; /**< variable to store calibration status */
static LED_handle_tp ledHandle = (LED_handle_tp) NULL; /**< variable to store led handle */
static xTimerHandle printTimerHandle = NULL;/**< variable to store timer handle*/
static calibrationStatus_t calibSensorAccuracyStatus = CALIBRATION_STATUS_UNRELIABLE; /**<variable to store calibration status of sensor */
/**
 * The variable decides which calibrated sensor data has to be printed. By default CALIBRATED ACCELEROMETER values are printed.
 * user can switch to CALIBRATED GYROSCOPE or CALIBRATED MAGNETOMETER by using corresponding sensor value
 * available in printData_t enumeration
 */
static printData_t dataToPrint = PRINT_CALIBRATED_ACCEL_DATA;/**< variable which indicates what data of calibrated sensor is to be printed */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief   API used to check whether sensor calibration is done or not.
 *          Once the sensor calibration is done, yellow LED starts to glow constantly
 *          if the calibration is not done, then the LED will continue to Blink
 *
 * @retval  CSD_CALIBRATION_NOT_DONE        sensor calibration is not done
 * @retval  CSD_CALIBRATION_DONE            sensor calibration is done
 */
static uint8_t isCalibrationDone(void)
{
    LED_errorTypes_t ledReturnStatus = LED_ERROR_HANDLE_DEALLOCATED;
    Retcode_T calibrationStatus = (Retcode_T) RETCODE_FAILURE;
    /* Initially LED is set to TOGGLE state */
    ledReturnStatus = LED_setState(ledHandle, LED_SET_TOGGLE);
    if (LED_ERROR_OK != ledReturnStatus)
    {
        /*Assertion Reason: "LED set Toggle State failed" */
        assert(false);
    }
    switch (dataToPrint)
    {
    case PRINT_CALIBRATED_ACCEL_DATA:
        /* reading calibration status of calibrated accelerometer */
        calibrationStatus = CalibratedAccel_getStatus((CalibratedAccel_StatusPtr_T) &calibSensorAccuracyStatus);
        break;
    case PRINT_CALIBRATED_GYRO_DATA:
        /* reading calibration status of calibrated gyroscope */
        calibrationStatus = CalibratedGyro_getStatus((CalibratedGyro_CalibStatusPtr_T) &calibSensorAccuracyStatus);
        break;
    case PRINT_CALIBRATED_MAG_DATA:
        /* reading calibration status of calibrated magnetometer */
        calibrationStatus = CalibratedMag_getStatus((CalibratedMag_CalibStatusPtr_T) &calibSensorAccuracyStatus);
        break;
    default:
        calibrationStatus =(Retcode_T) RETCODE_FAILURE;
        break;
    }

    if ((calibrationStatus != RETCODE_OK) || (CALIBRATION_STATUS_HIGH != (calibrationStatus_t) calibSensorAccuracyStatus))
    {
        return (CALIBRATION_NOT_DONE);
    }
    /* Once the calibration is done , LED is set to ON state */
    ledReturnStatus = LED_setState(ledHandle, LED_SET_ON);
    /* Assertion reason:  "LED set On State failed" */
    assert(ledReturnStatus == LED_ERROR_OK);
    return (CALIBRATION_DONE);
}

/**
 * @brief   Gets the data from Corresponding calibrated sensor and prints through the USB printf on serial port.
 *
 * @param[in] pvParameters RTOS task should be defined with the type void *(as argument)
 */
static void printCalibratedSensorsData(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);

    Retcode_T calibratedSensorReturnStatus = (Retcode_T)RETCODE_FAILURE;

    switch (dataToPrint)
    {

    case PRINT_CALIBRATED_ACCEL_DATA:
        {
        CalibratedAccel_XyzLsbData_T calibAccelLsbData;
        CalibratedAccel_XyzMps2Data_T calibAccelMps2Data;
        CalibratedAccel_XyzGData_T calibAccelGData;
        /* initializing structure elements */
        memset(&calibAccelLsbData, 0, sizeof(CalibratedAccel_XyzLsbData_T));
        memset(&calibAccelMps2Data, 0, sizeof(CalibratedAccel_XyzMps2Data_T));
        memset(&calibAccelGData, 0, sizeof(CalibratedAccel_XyzGData_T));

        if ( CALIBRATION_NOT_DONE == isCalibrated)
        {
            /* Checking the calibration status of calibrated accelerometer */
            isCalibrated = isCalibrationDone();
        }
        if ( CALIBRATION_DONE == isCalibrated)
        {
            /* Reading calibrated accelerometer lsb value */
            calibratedSensorReturnStatus = CalibratedAccel_readXyzLsbValue(&calibAccelLsbData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated accelerometer lsb information */
                printf("CALIBRATED ACCEL DATA - LSB\t: %ld lsb\t%ld lsb\t%ld lsb\n\r",
                        (long int) calibAccelLsbData.xAxisData, (long int) calibAccelLsbData.yAxisData, (long int) calibAccelLsbData.zAxisData);
            }
            else
            {
                printf("Calibrated Accelerometer lsb Value Read failed \n\r");
            }
            /* Reading calibrated accelerometer m/s2 value */
            calibratedSensorReturnStatus = CalibratedAccel_readXyzMps2Value(&calibAccelMps2Data);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated accelerometer m/s2 information */
                printf("CALIBRATED ACCEL DATA - M/S2\t: %.4f m/s2\t%.4f m/s2\t%.4f m/s2\n\r",
                        (float) calibAccelMps2Data.xAxisData, (float) calibAccelMps2Data.yAxisData, (float) calibAccelMps2Data.zAxisData);
            }
            else
            {
                printf("Calibrated Accelerometer metre per second squared Value Read failed \n\r");
            }
            /* Reading calibrated accelerometer g value */
            calibratedSensorReturnStatus = CalibratedAccel_readXyzGValue(&calibAccelGData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated accelerometer g information */
                printf("CALIBRATED ACCEL DATA - G\t: %.4f g\t%.4f g\t%.4f g\n\n\r",
                        (float) calibAccelGData.xAxisData, (float) calibAccelGData.yAxisData, (float) calibAccelGData.zAxisData);
            }
            else
            {
                printf("Calibrated Accelerometer G Value Read failed \n\r");
            }
        }
    }
        break;

    case PRINT_CALIBRATED_GYRO_DATA:
        {
        CalibratedGyro_XyzLsbData_T calibGyroLsbData;
        CalibratedGyro_XyzRpsData_T calibGyroRpsData;
        CalibratedGyro_XyzDpsData_T calibGyroDpsData;
        /* initializing structure elements */
        memset(&calibGyroLsbData, 0, sizeof(CalibratedGyro_XyzLsbData_T));
        memset(&calibGyroRpsData, 0, sizeof(CalibratedGyro_XyzRpsData_T));
        memset(&calibGyroDpsData, 0, sizeof(CalibratedGyro_XyzDpsData_T));

        if (CALIBRATION_NOT_DONE == isCalibrated)
        {
            /* Checking the calibration status of calibrated gyroscope */
            isCalibrated = isCalibrationDone();
        }
        if (CALIBRATION_DONE == isCalibrated)
        {
            /* Reading calibrated gyroscope lsb value */
            calibratedSensorReturnStatus = CalibratedGyro_readXyzLsbValue(&calibGyroLsbData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated gyroscope lsb information */
                printf("CALIBRATED GYRO DATA - LSB\t: %ld lsb\t%ld lsb\t%ld lsb\n\r",
                        (long int) calibGyroLsbData.xAxisData, (long int) calibGyroLsbData.yAxisData, (long int) calibGyroLsbData.zAxisData);
            }
            else
            {
                printf("Calibrated Gyroscope lsb Value Read failed \n\r");
            }
            /* Reading calibrated gyroscope rad/sec value */
            calibratedSensorReturnStatus = CalibratedGyro_readXyzRpsValue(&calibGyroRpsData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated gyroscope rad/s information */
                printf("CALIBRATED GYRO DATA - RAD/SEC\t: %.4f rad/s\t%.4f rad/s\t%.4f rad/s\n\r",
                        (float) calibGyroRpsData.xAxisData, (float) calibGyroRpsData.yAxisData, (float) calibGyroRpsData.zAxisData);
            }
            else
            {
                printf("Calibrated Gyroscope radians per second Value Read failed \n\r");
            }
            /* Reading calibrated gyroscope deg/sec value */
            calibratedSensorReturnStatus = CalibratedGyro_readXyzDpsValue(&calibGyroDpsData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated gyroscope deg/sec information */
                printf("CALIBRATED GYRO DATA - DEG/SEC\t: %.4f deg/s\t%.4f deg/s\t%.4f deg/s\n\n\r",
                        (float) calibGyroDpsData.xAxisData, (float) calibGyroDpsData.yAxisData, (float) calibGyroDpsData.zAxisData);
            }
            else
            {
                printf("Calibrated Gyroscope degrees per second Value Read failed \n\r");
            }
        }
    }
        break;

    case PRINT_CALIBRATED_MAG_DATA:
        {
        CalibratedMag_XyzLsbData_T calibMagLsbData;
        CalibratedMag_XyzMicroTesla_T calibMagMicroTeslaData;
        CalibratedMag_XyzGaussData_T calibMagGaussData;
        /* initializing structure elements */
        memset(&calibMagLsbData, 0, sizeof(CalibratedMag_XyzLsbData_T));
        memset(&calibMagMicroTeslaData, 0, sizeof(CalibratedMag_XyzMicroTesla_T));
        memset(&calibMagGaussData, 0, sizeof(CalibratedMag_XyzGaussData_T));

        if (CALIBRATION_NOT_DONE == isCalibrated)
        {
            /* Checking the calibration status of calibrated magnetometer */
            isCalibrated = isCalibrationDone();
        }
        if (CALIBRATION_DONE == isCalibrated)
        {
            /* Reading calibrated magnetometer lsb value */
            calibratedSensorReturnStatus = CalibratedMag_readXyzLsbVal(&calibMagLsbData);

            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated magnetometer lsb information */
                printf("CALIBRATED MAG DATA - LSB\t: %ld lsb\t%ld lsb\t%ld lsb\n\r",
                        (long int) calibMagLsbData.xAxisData, (long int) calibMagLsbData.yAxisData, (long int) calibMagLsbData.zAxisData);
            }
            else
            {
                printf("Calibrated Magnetometer lsb Value Read failed \n\r");
            }
            /* Reading calibrated magnetometer micro tesla value */
            calibratedSensorReturnStatus = CalibratedMag_readMicroTeslaVal(&calibMagMicroTeslaData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated magnetometer micro tesla information */
                printf("CALIBRATED MAG DATA - uT\t: %.4f uT\t%.4f uT\t%.4f uT\n\r",
                        (float) calibMagMicroTeslaData.xAxisData, (float) calibMagMicroTeslaData.yAxisData, (float) calibMagMicroTeslaData.zAxisData);
            }
            else
            {
                printf("Calibrated Magnetometer micro tesla Value Read failed \n\r");
            }
            /* Reading calibrated magnetometer gauss value */
            calibratedSensorReturnStatus = CalibratedMag_readXyzGaussVal(&calibMagGaussData);
            if (RETCODE_OK == calibratedSensorReturnStatus)
            {
                /* Print calibrated magnetometer gauss information */
                printf("CALIBRATED MAG DATA - GAUSS\t: %.4f gauss\t%.4f gauss\t%.4f gauss\n\n\r",
                        (float) calibMagGaussData.xAxisData, (float) calibMagGaussData.yAxisData, (float) calibMagGaussData.zAxisData);
            }
            else
            {
                printf("Calibrated Magnetometer Gauss Value Read failed \n\r");
            }
        }
    }
        break;

    default:
        printf("Invalid data to print");
        break;
    }
}

/* global functions ********************************************************* */

/* API documentation is in the header */
void calibratedSensorInit(void)
{
    int8_t retValPerSwTimer = TIMER_NOT_ENOUGH_MEMORY;
    Retcode_T calibratedSensorInitStatus = (Retcode_T)RETCODE_FAILURE;
    ledHandle = LED_create(gpioYellowLed_Handle, GPIO_STATE_OFF);
    /* assertion Reason : "LED creation failed" */
    assert(ledHandle != NULL);

    switch (dataToPrint)
    {
    case PRINT_CALIBRATED_ACCEL_DATA:
        /* initialize calibrated accelerometer */
        calibratedSensorInitStatus = CalibratedAccel_init(xdkCalibratedAccelerometer_Handle);
        break;
    case PRINT_CALIBRATED_GYRO_DATA:
        /* initialize calibrated gyroscope */
        calibratedSensorInitStatus = CalibratedGyro_init(xdkCalibratedGyroscope_Handle);
        break;
    case PRINT_CALIBRATED_MAG_DATA:
        /* initialize calibrated magnetometer */
        calibratedSensorInitStatus = CalibratedMag_init(xdkCalibratedMagnetometer_Handle);
        break;
    default:
        calibratedSensorInitStatus = (Retcode_T)RETCODE_FAILURE;
        break;
    }

    if (RETCODE_OK == calibratedSensorInitStatus)
    {
        uint32_t Ticks = DELAY;

        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            Ticks = UINT32_C(1);
        }
        /* create timer task to get and print calibrated sensor data periodically */
        printTimerHandle = xTimerCreate((const char * const ) "calibratedSensorsDemo",
                Ticks, TIMER_AUTORELOAD_ON, NULL, printCalibratedSensorsData);

        /* CSD timer create fail case */
        if (NULL == printTimerHandle)
        {
            /* Assertion Reason: "This software timer was not Created, Due to Insufficient heap memory" */
            assert(false);
        }

        /* Start the created timer*/
        retValPerSwTimer = xTimerStart(printTimerHandle, TIMERBLOCKTIME);

        /* CSD timer start fail case */
        if (TIMER_NOT_ENOUGH_MEMORY == retValPerSwTimer)
        {
            /* Assertion Reason: "This software timer was not started, Due to Insufficient heap memory" */
            assert(false);
        }
    }
    else
    {
        printf("calibrated sensor initialisation failed \n\r");
    }
}

/* API documentation is in the header */
void calibratedSensorDeinit(void)
{
    Retcode_T calibratedSensorDeinitStatus = (Retcode_T)RETCODE_FAILURE;
    LED_errorTypes_t ledReturnStatus = LED_ERROR_HANDLE_DEALLOCATED;
    /* Checking if the timer handler is valid (not NULL)*/
    if (NULL == printTimerHandle)
    {
        /* Assertion Reason: "Calibrated Sensor Timer is not valid" */
        assert(false);
    }
    ledReturnStatus = LED_delete(ledHandle);
    if (LED_ERROR_OK != ledReturnStatus)
    {
        /* Assertion Reason : "LED deletion failed" */
        assert(false);
    }
    switch (dataToPrint)
    {
    case PRINT_CALIBRATED_ACCEL_DATA:
        /* deinitialize calibrated accelerometer */
        calibratedSensorDeinitStatus = CalibratedAccel_deInit();
        break;
    case PRINT_CALIBRATED_GYRO_DATA:
        /* deinitialize calibrated gyroscope */
        calibratedSensorDeinitStatus = CalibratedGyro_deInit();
        break;
    case PRINT_CALIBRATED_MAG_DATA:
        /* deinitialize calibrated magnetometer */
        calibratedSensorDeinitStatus = CalibratedMag_deInit();
        break;
    default:
        calibratedSensorDeinitStatus = (Retcode_T)RETCODE_FAILURE;
        break;

    }

    if (RETCODE_OK == calibratedSensorDeinitStatus)
    {
        /* clear the calibration status flag */
        isCalibrated = CALIBRATION_NOT_DONE;
        /* Stop the created CSD timer */
        if (xTimerStop(printTimerHandle, TIMERBLOCKTIME) == pdFAIL)
        {
            assert(false);
        }
    }
    else
    {
        printf("calibrated sensor deinitialisation failed \n\r");
    }

}

/** ************************************************************************* */

