/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include <stdint.h>

/* own header files */
#include "HumidityData.h"

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_Assert.h"
#include "XdkSensorHandle.h"
#include "BCDS_AbsoluteHumidity.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static xTimerHandle printTimerHandle = 0;/**< variable to store timer handle*/
#define DELAY   UINT32_C(20)       /**< Timer delay is represented by this macro */
#define TIMERBLOCKTIME    UINT32_C(0xffff)  /**< Macro used to define blocktime of a timer*/
#define TEMPERATURE2CELSIUS (0.001f)        /**< Macro used to convert temperature to degrees celsius r*/
#define TIMER_NOT_ENOUGH_MEMORY            (-1L)/**<Macro to define not enough memory error in timer*/
#define TIMER_AUTORELOAD_ON             UINT32_C(1)             /**< Auto reload of timer is enabled*/

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief Gets the Humidity value and prints through the USB printf on serial port
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
static void printHumidityData(void *pvParameters)
{
    Retcode_T returnValue = (Retcode_T)RETCODE_FAILURE;
    float actualHumidity = 0.0f;

    BCDS_UNUSED(pvParameters);

    returnValue = AbsoluteHumidity_readValue(xdkHumiditySensor_Handle, &actualHumidity);

    if (returnValue != RETCODE_OK)
    {
        printf("Actual Humidity Read Data Failed\n\r");
    }
    else
    {
        /* Print Absolute Humidity */
        printf("Absolute Humidity= %3.1f g/m3\n\r", (float) actualHumidity);
    }
}

/* global functions ********************************************************* */

/* API documentation is in the header */
void humiditySensorInit(void)
{
    /* Return value for Timer start */
    int8_t retValPerSwTimer = TIMER_NOT_ENOUGH_MEMORY;
    Retcode_T returnValue = (Retcode_T)RETCODE_FAILURE;

    /*initialize Humidity sensor*/
    returnValue = AbsoluteHumidity_init(xdkHumiditySensor_Handle);

    if (RETCODE_OK == returnValue)
    {
        uint32_t Ticks = DELAY;

        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            Ticks = UINT32_C(1);
        }
        /* create timer task to get and print Humidity data every one second automatically*/
        printTimerHandle = xTimerCreate((const char * const ) "printHumidityData",
                Ticks, TIMER_AUTORELOAD_ON, NULL, printHumidityData);

        /* timer create fail case */
        if (NULL == printTimerHandle)
        {
            /* assertion Reason: This software timer was not Created, Due to Insufficient heap memory */
            assert(false);
        }

        /*start the created timer*/
        retValPerSwTimer = xTimerStart(printTimerHandle,
                TIMERBLOCKTIME);

        /* PHD timer start fail case */
        if (TIMER_NOT_ENOUGH_MEMORY == retValPerSwTimer)
        {
            /* Assertion Reason: This software timer was not started, Due to Insufficient heap memory */
            assert(false);
        }
        printf("Humidity Sensor initialization Success\n\r");
    }
    else
    {
        printf("Humidity Sensor initialization Failed\n\r");
    }
}

/* API documentation is in the header */
void humiditySensorDeinit(void)
{
    Retcode_T returnStatus = (Retcode_T)RETCODE_FAILURE;
    /* Checking if the timer handler is valid (not NULL)*/
    /* Assertion Reason : "Humidity Sensor Timer is not valid." */
    assert(printTimerHandle != NULL);

    /* Deinitialise humidity module */
    returnStatus = AbsoluteHumidity_deInit();

    if (RETCODE_OK == returnStatus)
    {
        /* Stop the created PHD timer */
        if(xTimerStop(printTimerHandle, TIMERBLOCKTIME)==pdFAIL)
        {
            assert(false);
        }
    }
    else
    {
        printf("Humidity sensor deinitialisation failed \n\r");
    }
}

/** ************************************************************************* */

