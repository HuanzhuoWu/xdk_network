/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* own header files */
#include "MagneticFingerPrint.h"

/* additional interface header files */
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "XdkSensorHandle.h"
#include "XdkBoardHandle.h"
#include "led.h"
#include "button.h"
#include "BCDS_FingerPrint.h"
#include "BCDS_Assert.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
#define TIMERDELAY                  UINT32_C(50)       /**< data sampling delay is represented by this macro */
#define TIMERBLOCKTIME              UINT32_C(0xffff)   /**< Macro used to define blocktime of a timer*/
#define ZEROVALUE                   UINT32_C(0x00)     /**< Macro used to define default value*/
#define ZERO_VALUE                  UINT32_C(0)        /**< Macro to represent zero value */
#define CALLBACK_FOR_BUTTON_LEFT    UINT32_C(0x11)     /**< Macro represents callback parameter for Button1 */
#define CALLBACK_FOR_BUTTON_RIGHT   UINT32_C(0x12)     /**< Macro represents callback parameter for Button2 */
#define TIMER_NOT_ENOUGH_MEMORY            (-1L)/**<Macro to define not enough memory error in timer*/
#define TIMER_AUTORELOAD_ON             UINT32_C(1)             /**< Auto reload of timer is enabled*/

#define MATCHING_FACTOR  0.9f   /**< Macro represents fingerprint matching factor */

/** enum to represent user interface menu level */
typedef enum menuLevel_e
{
    UI_MENU_LEVEL_0,
    UI_MENU_LEVEL_1,
    UI_MENU_LEVEL_2,
    UI_MENU_LEVEL_3,
} menuLevel_t;

/** enum to represent magnetic fingerprint operational status */
typedef enum fingPrintMonitoringState_e
{
    PROCESS_OFF,
    PROCESS_ON,
} fingPrintMonitoringState_t;

typedef enum fingPrintSetValue_e
{
    NO_REQUEST,
    VALUE_RESET_REQUEST,
    VALUE_SET_REQUEST,
} fingPrintSetValue_t;

/** enum to represent leds */
typedef enum ledNumber_e
{
    LED_RED,
    LED_ORANGE,
    LED_YELLOW,
} ledNumber_t;

static xTimerHandle printTimerHandle = ZEROVALUE; /**< variable to store timer handle*/
static LED_handle_tp LedRedHandle = (LED_handle_tp) NULL; /**< variable to store red led handle */
static LED_handle_tp LedYellowHandle = (LED_handle_tp) NULL; /**< variable to store yellow led handle */
static LED_handle_tp LedOrangeHandle = (LED_handle_tp) NULL; /**< variable to store orange led handle */
static BUTTON_handle_tp ButtonRightHandle = (BUTTON_handle_tp) NULL; /**< variable to store button 1 handle */
static BUTTON_handle_tp ButtonLeftHandle = (BUTTON_handle_tp) NULL; /**< variable to store button 2 handle */
static uint8_t buttonLeftPressedCnt = UINT8_C(0); /**< counter to monitor menu level: 0-> mag fing print monitoring; 1->set/reset/check MF1; 2->set/reset/check MF2; 3->set/reset/check MF3 */

static fingPrintMonitoringState_t fingPrintStatus = PROCESS_OFF; /**< variable to store magnetic fingerprint oparation status */
static fingPrintSetValue_t fingPrintRequest[FINGERPRINT_REF_MAX] = { NO_REQUEST, NO_REQUEST, NO_REQUEST }; /**< variable to store whether a fingerprint has been requested  */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**@brief main process for magnetic fingerprint application
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
static void process(xTimerHandle pxTimer)
{
    Retcode_T fingPrintReturnValue = (Retcode_T) RETCODE_FAILURE;
    LED_errorTypes_t returnValue = LED_ERROR_OUT_OF_RANGE;

    FingerPrint_StorageState_T fingPrintPrintingStatus;
    FingerPrint_Monitor_Data_T fingPrintMatchingLevel[FINGERPRINT_REF_MAX];
    LED_operations_t fingPrintLedStatus[FINGERPRINT_REF_MAX] = { LED_SET_OFF, LED_SET_OFF, LED_SET_OFF };

    BCDS_UNUSED(pxTimer);

    /* check UI menu level */
    switch (buttonLeftPressedCnt)
    {
    /* Magnetic finger print monitoring process */
    case UI_MENU_LEVEL_0:

        returnValue = LED_setState(LedRedHandle, LED_SET_OFF);
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(LedOrangeHandle, LED_SET_OFF);
        }
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(LedYellowHandle, LED_SET_OFF);
        }
        if (returnValue != LED_ERROR_OK)
        {
            printf("Setting LED state failed\r\n");
        }
        if (PROCESS_ON == fingPrintStatus)
        {
            /* Magnetic fingeprint evaluation */
            fingPrintReturnValue = FingerPrint_monitoring(&fingPrintMatchingLevel[FINGERPRINT_REF_VAL_1]);

            /* Visualize when sensor measurement and fingerprint matching estimation */
            for (uint32_t fpIndex = (uint32_t)FINGERPRINT_REF_VAL_1; fpIndex < (uint32_t)FINGERPRINT_REF_MAX; fpIndex++)
            {
                if (fingPrintMatchingLevel[fpIndex].fpDistanceMatching < MATCHING_FACTOR)
                {
                    fingPrintLedStatus[fpIndex] = LED_SET_OFF;
                }
                else
                {
                    fingPrintLedStatus[fpIndex] = LED_SET_ON;
                }
            }
            /* Update led status */
            returnValue = LED_setState(LedRedHandle, fingPrintLedStatus[FINGERPRINT_REF_VAL_1]);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, fingPrintLedStatus[FINGERPRINT_REF_VAL_2]);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, fingPrintLedStatus[FINGERPRINT_REF_VAL_3]);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }
        break;

        /* Check status magnetic finger print 1*/
    case UI_MENU_LEVEL_1:

        fingPrintReturnValue = FingerPrint_checkStoredValue(FINGERPRINT_REF_VAL_1, (FingerPrint_StorageStatusPtr_T) &fingPrintPrintingStatus);

        if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_ON);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                  returnValue = LED_setState(LedYellowHandle, LED_SET_OFF);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }
        else
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_TOGGLE);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, LED_SET_OFF);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }

        if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_1])
        {
            /* Set magnetic fingerprint 1 */
            if (FINGERPRINT_EMPTY == fingPrintPrintingStatus)
            {
                fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_setValue(FINGERPRINT_REF_VAL_1));
            }
            else if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
            {
                printf("Fingerprint 1 recorded\r\n");
            }
        }
        if (VALUE_RESET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_1])
        {
            /* Reset magnetic fingerprint 1 */
            fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_resetValue(FINGERPRINT_REF_VAL_1));
            fingPrintRequest[FINGERPRINT_REF_VAL_1] = NO_REQUEST;
            printf("Fingerprint 1 reset 1\r\n");
        }
        break;

        /* Check status magnetic finger print 2 */
    case UI_MENU_LEVEL_2:

        fingPrintReturnValue = FingerPrint_checkStoredValue(FINGERPRINT_REF_VAL_2, (FingerPrint_StorageStatusPtr_T) &fingPrintPrintingStatus);

        if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_ON);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, LED_SET_OFF);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }
        else
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_TOGGLE);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, LED_SET_OFF);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }

        if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_2])
        {
            /* Set magnetic fingerprint 2 */
            if (FINGERPRINT_EMPTY == fingPrintPrintingStatus)
            {
                fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_setValue(FINGERPRINT_REF_VAL_2));
            }
            else if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
            {
                printf("Fingerprint 2 set\r\n");
            }
        }
        if (VALUE_RESET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_2])
        {
            /* Reset magnetic fingerprint 2 */
            fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_resetValue(FINGERPRINT_REF_VAL_2));
            fingPrintRequest[FINGERPRINT_REF_VAL_2] = NO_REQUEST;
            printf("Fingerprint 2 reset\r\n");
        }
        break;

        /* Check status magnetic finger print 3 */
    case UI_MENU_LEVEL_3:

        fingPrintReturnValue = FingerPrint_checkStoredValue(FINGERPRINT_REF_VAL_3, (FingerPrint_StorageStatusPtr_T) &fingPrintPrintingStatus);

        if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, LED_SET_ON);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }
        else
        {
            returnValue = LED_setState(LedRedHandle, LED_SET_OFF);
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedOrangeHandle, LED_SET_OFF);
            }
            if (LED_ERROR_OK == returnValue)
            {
                returnValue = LED_setState(LedYellowHandle, LED_SET_TOGGLE);
            }
            if (returnValue != LED_ERROR_OK)
            {
                printf("Setting LED state failed\r\n");
            }
        }

        if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_3])
        {
            /* Set magnetic fingerprint 3 */
            if (FINGERPRINT_EMPTY == fingPrintPrintingStatus)
            {
                fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_setValue(FINGERPRINT_REF_VAL_3));
            }
            else if (FINGERPRINT_RECORDED == fingPrintPrintingStatus)
            {
                printf("Fingerprint 3 set\r\n");
            }
        }
        if (VALUE_RESET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_3])
        {
            /* Reset magnetic fingerprint 3 */
            fingPrintReturnValue = (fingPrintReturnValue + FingerPrint_resetValue(FINGERPRINT_REF_VAL_3));
            fingPrintRequest[FINGERPRINT_REF_VAL_3] = NO_REQUEST;
            printf("Fingerprint 3 reset\r\n");
        }
        break;
    default:
        printf("Menu not available \n\r");
        break;
    }
    if (RETCODE_OK == fingPrintReturnValue)
    {
        printf("Fingerprint set and reset happened successfully\r\n");
    }
}

/**
 * @brief     this callback will get triggered whenever any one of button is pressed
 * @param[in] handle handle of the push button
 * @param[in] userParameter parameter of the corresponding handle
 */
static void button_callback(void *handle, uint32_t userParameter)
{
    switch (userParameter)
    {
    /*  Button Left pressed/released */
    case CALLBACK_FOR_BUTTON_LEFT:

        if (BUTTON_isPressed(handle))
        {
            if (buttonLeftPressedCnt <(uint8_t) UI_MENU_LEVEL_3)
            {
                buttonLeftPressedCnt++;
                printf("MFP button left pressed. Menu level: %d\r\n", buttonLeftPressedCnt);
            }
            else
            {
                buttonLeftPressedCnt =(uint8_t) UI_MENU_LEVEL_0;
                printf("MFP button left pressed. Menu level: %d\r\n", buttonLeftPressedCnt);
            }
        }
        break;

        /* Button Right pressed/released */
    case CALLBACK_FOR_BUTTON_RIGHT:

        if (BUTTON_isPressed(handle))
        {
            switch (buttonLeftPressedCnt)
            {
            /* pause/resume magnetic finger print monitoring process */
            case UI_MENU_LEVEL_0:

                if (PROCESS_ON == fingPrintStatus)
                {
                    fingPrintStatus = PROCESS_OFF;
                    printf("pause MFP monitoring\r\n");
                }
                else if (PROCESS_OFF == fingPrintStatus)
                {
                    fingPrintStatus = PROCESS_ON;
                    printf("resume MFP monitoring\r\n");
                }

                break;
                /* Set/Reset magnetic finger print 1 */
            case UI_MENU_LEVEL_1:

                if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_1])
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_1] = VALUE_RESET_REQUEST;
                }
                else
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_1] = VALUE_SET_REQUEST;
                }
                break;

                /* Set/Reset magnetic finger print 2 */
            case UI_MENU_LEVEL_2:

                if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_2])
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_2] = VALUE_RESET_REQUEST;
                }
                else
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_2] = VALUE_SET_REQUEST;
                }
                break;

                /* Set/Reset magnetic finger print 3 */
            case UI_MENU_LEVEL_3:

                if (VALUE_SET_REQUEST == fingPrintRequest[FINGERPRINT_REF_VAL_3])
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_3] = VALUE_RESET_REQUEST;
                }
                else
                {
                    fingPrintRequest[FINGERPRINT_REF_VAL_3] = VALUE_SET_REQUEST;
                }
                break;
            default:
                printf("Menu not available \n\r");
                break;
            }
        }
        break;
    default:
        printf("Button not available \n\r");
        break;
    }
}

/**
 * @brief The function initializes required XDK Leds for UI
 */

static return_t led_init(void)
{
    return_t ledReturnValue = RETURN_NOT_OK; /* Used for error handling from led API */

    /* Create handlers red, yellow and orange xdk leds */
    LedRedHandle = LED_create(gpioRedLed_Handle, GPIO_STATE_OFF);
    if (LedRedHandle != NULL)
    {
        LedYellowHandle = LED_create(gpioYellowLed_Handle, GPIO_STATE_OFF);
    }
    if (LedYellowHandle != NULL)
    {
        LedOrangeHandle = LED_create(gpioOrangeLed_Handle, GPIO_STATE_OFF);
    }
    if (LedRedHandle != NULL && LedOrangeHandle != NULL && LedYellowHandle != NULL)
    {
        ledReturnValue = RETURN_OK;
    }

    return (ledReturnValue);
}

/**
 * @brief The function initializes XDK right and left buttons for UI
 */

static return_t button_init(void)
{
    return_t returnValue = RETURN_NOT_OK; /* Used for error handling from button API */
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    /* Create handlers right and left xdk buttons */
    ButtonRightHandle = BUTTON_create(gpioButton2_Handle, GPIO_STATE_OFF);
    if (ButtonRightHandle != NULL)
    {
        ButtonLeftHandle = BUTTON_create(gpioButton1_Handle, GPIO_STATE_OFF);
    }

    /* Enable buttons */
    if (ButtonRightHandle != NULL && ButtonLeftHandle != NULL)
    {
        buttonReturn = BUTTON_enable(ButtonRightHandle);
        if (BUTTON_ERROR_OK == buttonReturn)
        {
            buttonReturn = BUTTON_enable(ButtonLeftHandle);
        }

        /* Enable callback function */
        if (BUTTON_ERROR_OK == buttonReturn)
        {
            buttonReturn = BUTTON_setCallback(ButtonLeftHandle, button_callback, CALLBACK_FOR_BUTTON_LEFT);
        }
        if (BUTTON_ERROR_OK == buttonReturn)
        {
            buttonReturn = BUTTON_setCallback(ButtonRightHandle, button_callback, CALLBACK_FOR_BUTTON_RIGHT);
        }
        if (BUTTON_ERROR_OK == buttonReturn)
        {
            returnValue = RETURN_OK;
        }
    }

    return (returnValue);
}

/* global functions ********************************************************* */

/* API documentation is in the header */
void magneticFingerPrintInit(void)
{
    /* Error handling from sensor API */
    Retcode_T fingPrintReturnValue = (Retcode_T) RETCODE_FAILURE;
    return_t initReturn = RETURN_NOT_OK;

    /* Return value for Timer start */
    int8_t timerReturnValue = TIMER_NOT_ENOUGH_MEMORY;

    /* Fingerprint initialization */
    fingPrintReturnValue = FingerPrint_init(xdkFingerprintSensor_Handle);

    /* Enable UI - LED */
    initReturn = (return_t) (led_init());

    if (RETURN_OK == initReturn)
    {
        /* Enable UI - BUTTON */
        initReturn = (return_t) (button_init());

        if ((RETCODE_OK == fingPrintReturnValue) && (RETURN_OK == initReturn))
        {
            uint32_t Ticks = TIMERDELAY;

            if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
            {
                Ticks /= portTICK_RATE_MS;
            }
            if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
            {
                Ticks = UINT32_C(1);
            }
            /* Create timer task */
            printTimerHandle = xTimerCreate((const char * const ) "magFingerprintApp",
                    Ticks, TIMER_AUTORELOAD_ON, NULL, process);

            /* Timer create fail case */
            if (NULL == printTimerHandle)
            {
                /* Assertion Reason: This software timer was not created, Due to Insufficient heap memory"*/
                assert(false);
            }

            /* Start the created timer*/
            timerReturnValue = xTimerStart(printTimerHandle, TIMERBLOCKTIME);

            /* Timer start fail case */
            if (TIMER_NOT_ENOUGH_MEMORY == timerReturnValue)
            {
                /* Assertion Reason: This software timer was not started,because the timer command queue is full.*/
                assert(false);
            }
        }
    }
    else
    {
        printf("Magnetometer fingerprint initialization FAILED\n\r");
    }
}

/* API documentation is in the header */
void magneticFingerPrintDeinit(void)
{
    Retcode_T fingPrintReturnValue = (Retcode_T) RETCODE_FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;
    LED_errorTypes_t ledReturn = LED_ERROR_INVALID_PARAMETER;

    /* Checking if the timer handler is valid (not NULL)*/
    /* Assertion Reason: Fingerprint Sensor Timer is not valid. */
    assert(printTimerHandle != NULL);

    fingPrintReturnValue = FingerPrint_deInit();

    if (fingPrintReturnValue != RETCODE_OK)
    {
        printf("Fingerprint deinitialisation failed \n\r");
    }

    buttonReturn = BUTTON_delete(ButtonRightHandle);

    if (buttonReturn != BUTTON_ERROR_OK)
    {
        printf("Right BUTTON deinitialisation failed \n\r");
    }

    if (BUTTON_ERROR_OK == buttonReturn)
    {
        buttonReturn = BUTTON_delete(ButtonLeftHandle);

        if (buttonReturn != BUTTON_ERROR_OK)
        {
            printf("Left BUTTON deinitialisation failed \n\r");
        }
    }

    ledReturn = LED_delete(LedRedHandle);

    if (ledReturn != LED_ERROR_OK)
    {
        printf("Red LED deinitialisation failed \n\r");
    }

    if (LED_ERROR_OK == ledReturn)
    {
        ledReturn = LED_delete(LedYellowHandle);

        if (ledReturn != LED_ERROR_OK)
        {
            printf("Yellow LED deinitialisation failed \n\r");
        }

        if (LED_ERROR_OK == ledReturn)
        {
            ledReturn = LED_delete(LedOrangeHandle);

            if (ledReturn != LED_ERROR_OK)
            {
                printf("Orange LED deinitialisation failed \n\r");
            }
        }
    }

    if ((RETCODE_OK == fingPrintReturnValue) && (BUTTON_ERROR_OK == buttonReturn) && (LED_ERROR_OK == ledReturn))
    {
        /* Stop the created MFP timer */
        if (xTimerStop(printTimerHandle, TIMERBLOCKTIME) == pdFAIL)
        {
            assert(false);
        }
    }
}

/*************************************************************************** */
