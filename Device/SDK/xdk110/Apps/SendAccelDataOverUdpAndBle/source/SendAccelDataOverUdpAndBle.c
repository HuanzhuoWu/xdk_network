/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"
#include "BCDS_Assert.h"
/* additional interface header files */
#include "simplelink.h"
#include <Serval_Types.h>
#include <Serval_Basics.h>
#include <Serval_Ip.h>
#include <FreeRTOS.h>
#include <timers.h>
#include "BCDS_Retcode.h"
#include "BCDS_WlanConnect.h"
#include "BCDS_NetworkConfig.h"
#include "XdkSensorHandle.h"
#include "BLE_stateHandler_ih.h"
#include "BLE_serialDriver_ih.h"
#include "BleAlpwDataExchange_Server.h"
#include "XdkUsbResetUtility.h"

/* own header files */
#include "SendAccelDataOverUdpAndBle.h"
/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static uint16_t BleConnectionHandle = UINT16_C(0); /**< BLE connection handler for Send or Receive to host */

static volatile uint8_t isInterruptHandled = ENABLE_FLAG; /**< BLE InterruptHandler */

static uint8_t bleRecievedData[MAX_DEVICE_LENGTH] = { 0 };/**< Store the bluetooth device name */

static xTimerHandle wifiConnectTimerHandle = NULL;/**< Timer handle for connecting to wifi and obtaining the IP address */

static xTimerHandle btleConnectTimerHandle = NULL;/**< Timer handle for discovering and connecting to device */

static xTimerHandle sendTimerHandle = NULL;/**< Timer handle for periodically sending data over wifi and btle */
/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief       Bluetooth connect/ disconnect callback function
 *
 * @param[in]    connectionDetails  the connection status and the remote device address
 *
 */
static void notificationCallback(BLE_connectionDetails_t connectionDetails)
{

    switch (connectionDetails.connectionStatus)
    {
    case BLE_CONNECTED_TO_DEVICE:
        printf("Device connected  : \r\n");
        break;
    case BLE_DISCONNECTED_FROM_DEVICE:
        printf("Device Disconnected   : \r\n");
        break;
    default:
        /* assertion reason : invalid status of Bluetooth Device */
        assert(false);
        break;
    }
}

/**
 *  @brief
 *      -Function to initialize the Accel Sensor,wifi network and BTLE.
 *      -Create timer task to start WiFi Connect and get IP function after one second. After that another timer
 *      -Create timer task to start BTLE state machine and discovering/connecting BTLE device
 *      -Create timer task to Start after getting IP and then send the Accel Data to WiFi and BTLE
 *
 */
void init(void)
{
    /* Return value for Accel Sensor */
    Retcode_T accelReturnValue = (Retcode_T) RETCODE_FAILURE;
    /* return value for BLE stack configuration */
    BleStatus appInitReturn = BLESTATUS_FAILED;
    BLE_notification_t configParams;
    BLE_returnStatus_t returnValue;
    Retcode_T UsbReturnValue = (Retcode_T)RETCODE_FAILURE;
    BLE_status BleReturnValue = BLESTATUS_FAILED;

    /*registers the Application USB ISR */
    UsbReturnValue = UsbResetUtility_RegAppISR((UsbResetUtility_UsbAppCallback_T) CallbackIsr);

    if (UsbReturnValue != RETCODE_OK)
    {
        /* assertion reason : Registering USB application ISR failed*/
        assert(false);
    }

    /*Initialize BTLE Stack*/
    returnValue = BLE_setDeviceName((uint8_t *) "XDK_DEMO", 8);

    if (returnValue != BLE_SUCCESS)
    {
        /* assertion reason : invalid device name */
        assert(false);
    }

    /* enable and register notification callback for bluetooth device connect and disconnect*/
    configParams.callback = notificationCallback;
    configParams.enableNotification = BLE_ENABLE_NOTIFICATION;

    returnValue = BLE_enablenotificationForConnect(configParams);
    if (returnValue != BLE_SUCCESS)
    {
        /* assertion reason : Enable Notification Failed*/
        assert(false);
    }
    /* Registering the BLE Services  */
    BleReturnValue = BLE_customServiceRegistry(bleAppServiceRegister);
    if (BleReturnValue != BLESTATUS_SUCCESS)
    {
        /* assertion reason : invalid device name */
        assert(false);
    }
    /* Initialize the whole BLE stack */
    appInitReturn = BLE_coreStackInit();

    /*Initialize accel Sensor */
    accelReturnValue = Accelerometer_init(xdkAccelerometers_BMA280_Handle);

    /*RTOS Timer Task List */
    if ((RETCODE_OK == accelReturnValue) && ( BLESTATUS_SUCCESS == appInitReturn))
    {
        uint32_t bleTicks = ZEROSECONDDELAY_BTLE;

        if (bleTicks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            bleTicks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == bleTicks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            bleTicks = UINT32_C(1);
        }
        /* create timer task for BLE state machine */
        btleConnectTimerHandle = xTimerCreate((const char * const ) "BLE", bleTicks, TIMER_AUTORELOAD_ON, NULL, bleAppHandler);

        /* timer create fail case */
        if (btleConnectTimerHandle == NULL)
        {
            /* assertion reason :"This software timer was not Created, Due to Insufficient heap memory" */
            assert(false);
        }
        uint32_t wifiTicks = ONESECONDDELAY_WIFI;

        if (wifiTicks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            wifiTicks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == wifiTicks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            wifiTicks = UINT32_C(1);
        }
        /* create timer task wifi connection*/
        wifiConnectTimerHandle = xTimerCreate((const char * const ) "wifiConnect", wifiTicks, TIMER_AUTORELOAD_OFF, NULL, wifiConnectGetIP);

        /* timer create fail case */
        if (wifiConnectTimerHandle == NULL)
        {
            /* assertion reason :"This software timer was not Created, Due to Insufficient heap memory" */
            assert(false);
        }
        uint32_t Ticks = SENDING_DELAY;

        if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
            Ticks /= portTICK_RATE_MS;
        }
        if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
        {
            Ticks = UINT32_C(1);
        }
        /* create timer task for sending accel data to wifi/btle*/
        sendTimerHandle = xTimerCreate((const char * const ) "wifiBtleSend", Ticks, TIMER_AUTORELOAD_ON, NULL, wifiBtleSend);

        /* timer create fail case */
        if (sendTimerHandle == NULL)
        {
            /* assertion reason :"This software timer was not Created, Due to Insufficient heap memory" */
            assert(false);
        }

        /*start the BTLE connect timer*/
        if (xTimerStart(btleConnectTimerHandle, TIMERBLOCKTIME) != pdTRUE)
        {
            assert(false);
        }

        /*start the wifi connect timer*/
        if (xTimerStart(wifiConnectTimerHandle, TIMERBLOCKTIME) != pdTRUE)
        {
            assert(false);
        }
    }
    else
    {
        printf("accel and BTLE Init Failed...\n\r");
    }

}

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{
    BCDS_UNUSED(xTimer);
    /*Call the Wifi and Btle module API */
    init();
}

/** The function used to initialize the BLE Device and handle, various of event in the state machine
 *
 * @brief BLE device Initialization and handling the BLE events in state machine i.e Device connect/discover/Sleep etc
 *
 * @param[in] xTimer
 */
static void bleAppHandler(xTimerHandle xTimer)
{
    BCDS_UNUSED(xTimer); /* to quiet warnings */

    /* return variable for stack receive status from base band */
    BLE_return_t bleTrsprtRetVal;

    /* return variable for BLE state handler */
    uint8_t bleStateHandleRetVal;

    /* Notify the BLE Stack that some data have been received from the Base band(Chip) or Host */
    bleTrsprtRetVal = BLE_hciReceiveData();

    /* This function is used to run the BLE stack and register a BLE device with the specified role */
    bleStateHandleRetVal = BLE_coreStateMachine();

    UNUSED_PARAMETER(bleTrsprtRetVal);
    UNUSED_PARAMETER(bleStateHandleRetVal);

}

/**
 * @brief API called by xtimerPendFunctionCallFromISR function, which is registered during the USB ISR
 *
 * @param [in]callBackParam1 data buffer
 *
 * @param [in]callBackParam2 length
 */
static void interruptHandling(void *callBackParam1, uint32_t callBackParam2)
{

    BLE_deviceState_t modeConfigured;
    BLE_returnStatus_t retValue = BLE_FAILURE;
    switch (bleRecievedData[NUMBER_ZERO])
    {
    case SET_IDLE_MODE:
        retValue = BLE_setMode(BLE_IDLE);
        if (BLE_SUCCESS == retValue)
        {
            printf("enabling idle mode\r\n");
        }
        else
        {
            printf("enabling idle mode failed\r\n");
        }
        break;
    case SET_DISCOVERABLE_MODE:
        retValue = BLE_setMode(BLE_DISCOVERABLE);
        if (BLE_SUCCESS == retValue)
        {
            printf("enabling discoverable mode\r\n");
        }
        else
        {
            printf("enabling discoverable mode failed\r\n");
        }
        break;
    case SET_SLEEP_MODE:
        retValue = BLE_setMode(BLE_NORMAL_SLEEP);
        if (BLE_SUCCESS == retValue)
        {
            printf("enabling sleep mode\r\n");
        }
        else
        {
            printf("enabling sleep mode failed\r\n");
        }
        break;
    case GET_DEVICE_MODE:
        retValue = BLE_getMode(&modeConfigured);
        if (BLE_SUCCESS == retValue)
        {
            switch (modeConfigured)
            {
            case BLE_DEVICE_IN_IDLE:
                printf("Device is in Idle Sate\r\n");
                break;
            case BLE_DEVICE_IN_DISCOVERABLE:
                printf("Device is in Discoverable Sate\r\n");
                break;
            case BLE_DEVICE_IN_SLEEP:
                printf("Device is in sleep Sate\r\n");
                break;
            case BLE_DEVICE_IN_CONNECTED:
                printf("Device is Connected to a Host\r\n");
                break;
            default:
                break;
            }
        }
        else
        {
            printf("Reading mode failed\r\n");
        }
        break;
    default:
        break;

    }

    /* re-enable  the usb interrupt flag*/
    isInterruptHandled = ENABLE_FLAG;
    UNUSED_PARAMETER(callBackParam1);
    UNUSED_PARAMETER(callBackParam2);
}

/**
 * @brief Sending Accel data to wifi and btle
 *  -accelerometerReadXyzValue() Api reading Accel data(XYZ)
 *  -Opening a UDP client socket and sending Accel data to WIFI using sl_SendTo() api
 *  -sending Accel data to BTLE using BLEALPWDATAEXCHANGE_SERVER_SendData() api
 *
 * @param[in]      port number on which the server will be listening on
 *
 * @return         returnTypes_t:
 *                                  SOCKET_ERROR: when socket has not opened properly
 *                                  SEND_ERROR: when 0 transmitted bytes or send error
 *                                  SUCCESS: when UDP/BTLE sending was successful
 */
static returnTypes_t sendAccelData(uint16_t port)
{
    /* return value for BLE SendData */
    BleStatus bleSendReturn = BLESTATUS_FAILED;

    Retcode_T advancedApiRetValue = (Retcode_T) RETCODE_FAILURE;

    /* buffer for accel data receive function */
    uint8_t accelDataRec[ACCEL_RECEIVELENGTH] = { 0 };
    SlSockAddrIn_t Addr;
    uint16_t AddrSize = (uint16_t) ZERO;
    int16_t SockID = (int16_t) ZERO;
    int16_t Status = (int16_t) ZERO;
    /**
     * This buffer holds the data to be sent to server via UDP
     * */
    static int32_t bsdBuffer_mau[3] = { INT32_C(0), INT32_C(0), INT32_C(0) };

    Accelerometer_XyzData_T getaccelData = { INT32_C(0), INT32_C(0), INT32_C(0) };

    /*Reading accel Data (X,Y,Z) */
    advancedApiRetValue = Accelerometer_readXyzGValue(xdkAccelerometers_BMA280_Handle, &getaccelData);
    if (RETCODE_OK == advancedApiRetValue)
    {
        bsdBuffer_mau[0] = getaccelData.xAxisData;
        bsdBuffer_mau[1] = getaccelData.yAxisData;
        bsdBuffer_mau[2] = getaccelData.zAxisData;

    }

    /* Copying the Accel value into BLE-Buffer */
    sprintf((char*) accelDataRec, "%ld %ld %ld", (long int) bsdBuffer_mau[0],
            (long int) bsdBuffer_mau[1], (long int) bsdBuffer_mau[2]);

    /*Accel Data(X,Y,Z) Sending to WIFI Api*/
    Addr.sin_family = SL_AF_INET;
    Addr.sin_port = sl_Htons((uint16_t) port);
    Addr.sin_addr.s_addr = sl_Htonl(SERVER_IP);
    AddrSize = sizeof(SlSockAddrIn_t);

    /* The return value is a positive number if successful; other wise negative */
    SockID = sl_Socket(SL_AF_INET, SL_SOCK_DGRAM, (uint32_t) ZERO);
    if (SockID < (int16_t) ZERO)
    {
        /* error case*/
        return (SOCKET_ERROR);
    }

    /* The return value is a number of characters sent;negative if not successful */
    Status = sl_SendTo(SockID, bsdBuffer_mau, BUFFER_SIZE * sizeof(int32_t), (uint32_t) ZERO, (SlSockAddr_t *) &Addr, AddrSize);

    /*Check if 0 transmitted bytes sent or error condition*/
    if (Status <= (int16_t) ZERO)
    {
        Status = sl_Close(SockID);
        if (Status < 0)
        {
            return (SEND_ERROR);
        }
        return (SEND_ERROR);
    }

    Status = sl_Close(SockID);
    if (Status < 0)
    {
        return (SEND_ERROR);
    }
    /*Transmitting the Accel value into target device via Alphwise DataExchange service */
    bleSendReturn = BLEALPWDATAEXCHANGE_SERVER_SendData(BleConnectionHandle,
            (uint8_t*) accelDataRec, BLETRANSMITLENGTH);

    if (bleSendReturn == BLESTATUS_FAILED)
    {
        return (SEND_ERROR);
    }

    return (STATUS_SUCCESS);
}

/**
 *  @brief
 *      Function to periodically send data over WiFi and BTLE. This is run as an Auto-reloading timer.
 *
 *  @param [in ] xTimer - necessary parameter for timer prototype
 */
static void wifiBtleSend(xTimerHandle xTimer)
{
    BCDS_UNUSED(xTimer);
    if (sendAccelData(SERVER_PORT) != STATUS_SUCCESS)
    {
        /*Failure case - not handled*/
    }
}

/**
 *  @brief
 *      Function to connect to wifi network and obtain IP address
 *      -Start the wifi/btle sending timer task
 *
 *  @param [in ] xTimer
 */
static void wifiConnectGetIP(xTimerHandle xTimer)
{
    BCDS_UNUSED(xTimer);
    NetworkConfig_IpSettings_T myIpSettings;
    char ipAddress[PAL_IP_ADDRESS_SIZE] = { 0 };
    Ip_Address_T* IpaddressHex = Ip_getMyIpAddr();
    WlanConnect_SSID_T connectSSID;
    WlanConnect_PassPhrase_T connectPassPhrase;
    Retcode_T ReturnValue = (Retcode_T)RETCODE_FAILURE;
    int32_t Result = INT32_C(-1);

    memset(&myIpSettings, (uint32_t) ZERO, sizeof(myIpSettings));
    if (RETCODE_OK != WlanConnect_Init())
    {
        printf("Error occurred initializing WLAN \r\n ");
        return;
    }

    printf("Connecting to %s \r\n ", WLAN_CONNECT_WPA_SSID);
    connectSSID = (WlanConnect_SSID_T) WLAN_CONNECT_WPA_SSID;
    connectPassPhrase = (WlanConnect_PassPhrase_T) WLAN_CONNECT_WPA_PASS;
    ReturnValue = NetworkConfig_SetIpDhcp(NULL);
    if (RETCODE_OK != ReturnValue)
    {
        printf("Error in setting IP to DHCP\n\r");
        return;
    }

    if (ReturnValue == WlanConnect_WPA(connectSSID, connectPassPhrase, NULL))
    {
        ReturnValue = NetworkConfig_GetIpSettings(&myIpSettings);
        if (ReturnValue == ReturnValue)
        {
            *IpaddressHex = Basics_htonl(myIpSettings.ipV4);
            Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
            if (Result < INT32_C(0))
            {
                printf("Couldn't convert the IP address to string format \r\n ");
                return;
            }
            printf("Connected to WPA network successfully \r\n ");
            printf(" Ip address of the device %s \r\n ", ipAddress);
        }
        else
        {
            printf("Error in getting IP settings\n\r");
            return;
        }
    }
    else
    {
        printf("Error occurred connecting %s \r\n ", WLAN_CONNECT_WPA_SSID);
        return;
    }

    /* After connection start the wifi/btle sending timer*/
    if (xTimerStart(sendTimerHandle, TIMERBLOCKTIME) != pdTRUE)
    {
        assert(0);
    }
}

/**
 * @brief   This function callback used in ALPWISE Data Exchange Profile to transfer/receive BLE data
 *
 * @param[in] event: current device state or status
 *
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 *
 * @param[in] parms : This void data pointer has more information of event i.e connection host data/event,
 *                    status,command etc
 */
static void bleAlpwDataExchangeService(BleAlpwDataExchangeEvent event, BleStatus status, void *parameters)
{

    /* receive buffer for Alphwise exchange*/
    uint8_t bleRecData[BLETRANSMITLENGTH], bleSendRet = BLESTATUS_FAILED;

    if (BLEALPWDATAEXCHANGE_EVENT_RXDATA == event)
    {
        /* Checking data NULL pointer condition */
        /* assertion reason :"NULL pointer had been passed to BLE_bleSendData" */
        assert(parameters != NULL);
    }
    else
    {
        /* NULL check has not been done in transmit event due to parms empty
         Transmit event only update the status i.e success or fail*/
    }

    /* Check the Host side receive event */
    if ((BLEALPWDATAEXCHANGE_EVENT_RXDATA == event) && (BLESTATUS_SUCCESS == status) && (parameters))
    {
        /* Alphwise event information copied into local variable*/
        BleAlpwDataExchangeServerRxData *rxData = (BleAlpwDataExchangeServerRxData *) parameters;

        /* Instance for BLE connection handle */
        BleConnectionHandle = rxData->connHandle;

        /* Copying receive, from dataExchange buffer to local buffer*/
        memcpy(bleRecData, rxData->rxData, (int) rxData->rxDataLen);

        /* end of the string should be Null */
        bleRecData[rxData->rxDataLen] = '\0';

        if ((strcmp((const char *) bleRecData, "start") == NUMBER_ZERO))
        {
            /*This function is used to send data to the Host device by Alphwise DataExchange service*/
            bleSendRet = BLEALPWDATAEXCHANGE_SERVER_SendData(
                    BleConnectionHandle, (uint8_t*) "X      Y      Z",
                    BLETRANSMITLENGTH);
            if (bleSendRet == BLESTATUS_FAILED)
            {
                /* assertion reason :"BTLE sending data failed" */
                assert(false);
            }

        }
    }
}

/**
 * @brief  The BEA_bleServiceRegister is used to register the BLE Alpwise DataExchange
 *         service's into attribute database.
 */
static void bleAppServiceRegister(void)
{
    /* flag for service registry return */
    BleStatus serviceRegistryStatus;

    /* Alpwise data Exchange Service Register*/
    serviceRegistryStatus = BLEALPWDATAEXCHANGE_SERVER_Register(bleAlpwDataExchangeService);

    /* Checking data NULL pointer condition */
    if (serviceRegistryStatus == BLESTATUS_FAILED)
    {
        /* assertion reason :"BLE Service registry was failure" */
        assert(false);
    }

}

/**
 * @brief USB recieve call back function
 *
 * @param[in] usbRcvBuffer recieved data
 *
 * @param[in] count length of the data received
 */
static void CallbackIsr(uint8_t *usbRcvBuffer, uint16_t count)
{
    if (ENABLE_FLAG == isInterruptHandled)
    {
        isInterruptHandled = DISABLE_FLAG;

        /* add to timer queue*/
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        if (xTimerPendFunctionCallFromISR(interruptHandling, NULL, UINT8_C(0), &xHigherPriorityTaskWoken) == pdPASS)
        {
            memcpy(bleRecievedData, usbRcvBuffer, count);
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
        else
        {
            assert(false);
        }
    }
}

/** ************************************************************************* */
