/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/
/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <timers.h>
#include "BCDS_SDCardDriver.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "BCDS_Assert.h"
#include "ff.h"

/* own header files */
#include "SdCardExample.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static xTimerHandle sdCardWriteReadHandle; /**< variable to store timer handle*/
static Retcode_T diskInitStatus = (Retcode_T) SDCARD_NOT_INITIALIZED;
static FATFS FatFileSystemObject; /* File system specific objects */

/* global variables ********************************************************* */

/* local module global variable declarations */

uint8_t writeBuffer[SINGLE_SECTOR_LEN] = "XDK ASAMPLE TEST FOR MMC"; /**< Write Buffer for SD-Card */
uint8_t readBuffer[SINGLE_SECTOR_LEN]; /**< read buffer for SD-Card */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/** @brief
 * 		The function used to write and read depending on the SD card Detect status .
 * 		The status can be SD card inserted or not inserted.
 *
 *  @param[in] void *pvParameters
 * 		Rtos task should be defined with the type void *(as argument).
 */
static void sdCardWriteRead(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);

    sdCardAppReturn_t sdCardWriteReadReturn = APP_ERR_ERROR; /* variable for SD-Card ReadWrite status check */

    /* Get the SD Card status as inserted in XDK or not */
    if (SDCARD_INSERTED == SDCardDriver_GetDetectStatus())
    {
        printf("SD card is inserted in XDK\n\r");
        SDCARD_LED1_ON; /* Switch on the LED1 */

        /* SDC Disk FAT file system Write/Read functionality */
        sdCardWriteReadReturn = sdCardFatFileSystemWriteRead();
        if (sdCardWriteReadReturn == APP_ERR_NO_ERROR)
        {
            printf("Write and read using FAT file system success \n\r");
        }
        else
        {
            printf("Write and read using FAT file system failed\n\r");
        }
        /* SDC Disk  single block write and read without FAT file system  */
        sdCardWriteReadReturn = sdCardSingleBlockWriteRead(SECTOR_VALUE);
        if (sdCardWriteReadReturn == APP_ERR_NO_ERROR)
        {
            printf("single block write and read without FAT file system success \n\r");
        }
        else
        {
            printf("single block write and read without FAT file system failed \n\r");
        }
    }
    else
    {
        printf("\r\nSD card is not inserted in XDK\n\r");
        SDCARD_LED1_OFF; /* Switch off the LED1 */
        diskInitStatus = (Retcode_T) SDCARD_NOT_INITIALIZED;
    }
}

/**
 * @brief
 *      The SDC_sdCardWriteReadInit API creates a Write/read task and starts the task.
 *
 * @retval
 * 		SDC_ERR_NO_ERROR  Task was created successfully.
 *
 * @retval
 * 		SDC_ERR_ERROR  Task not started due to some error in OS.
 */
sdCardAppReturn_t sdCardWriteReadInit(void)
{
    uint32_t Ticks = WRITEREAD_DELAY;

    if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
    {
        Ticks /= portTICK_RATE_MS;
    }
    if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
    {
        Ticks = UINT32_C(1);
    }
    sdCardAppReturn_t sdcWriteReadInitStatus = APP_ERR_ERROR; /* Default return value is in Error state */
    /* create timer task to write and Read from SD card */
    sdCardWriteReadHandle = xTimerCreate(
            (const char * const ) "sdCardWriteReadProcess",
            Ticks, TIMER_AUTORELOAD_ON, NULL, sdCardWriteRead);

    if (NULL == sdCardWriteReadHandle) /* Timer was not created */
    {
        assert(false);
    }
    /* start the created timer */
    if (pdTRUE == xTimerStart(sdCardWriteReadHandle,
            WRITEREAD_BLOCK_TIME))
    {
        sdcWriteReadInitStatus = APP_ERR_NO_ERROR;
    }
    else
    {
        sdcWriteReadInitStatus = APP_ERR_INIT_FAILED;
    }

    return (sdcWriteReadInitStatus);
}

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{

    Retcode_T sdInitreturnValue;
    sdCardAppReturn_t csuReturnValue;
    BCDS_UNUSED(xTimer);
    FRESULT FileSystemResult = FR_OK;
    FILINFO FileInfo;

    /* Initialize SD card */
    sdInitreturnValue = SDCardDriver_Init();
    if (RETCODE_OK != sdInitreturnValue)
    { /* Debug fail case test SDC Init */

        /* assert reason : "SD-Card Driver Initialize Failed" */
        assert(false);
    }
    else
    {
        ; /* Do nothing */
    }
    diskInitStatus = SDCardDriver_DiskInitialize(DRIVE_ZERO);
    if (f_mount(&FatFileSystemObject, DEFAULT_LOGICAL_DRIVE, FORCE_MOUNT))
    {
        assert(false);
    }
    if (FR_OK == f_stat(TEST_FILENAME, &FileInfo))
    {
        FileSystemResult = f_unlink(TEST_FILENAME);
    }
    if (FR_OK != FileSystemResult)
    {
        printf("f_unlink failed \n\r");
        return;
    }
    /*Call the Application module API */
    csuReturnValue = sdCardWriteReadInit();

    if (APP_ERR_ERROR == csuReturnValue)
    { /* Debug fail case test for SDC Read/Write */

        /* assert reason : "SD-Card Initialize Timer was not created, Due to Insufficient heap memory" */
        assert(false);
    }
    else
    {
        ; /* Do nothing */
    }
}

/**
 * @brief
 *      The SDC_sdCardWriteReadDeInit API Delete the Write/read task.
 *
 * @retval
 * 		SDC_ERR_NO_ERROR  Task was created successfully.
 *
 * @retval
 * 		SDC_ERR_ERROR  Task not started due to some error in OS.
 */
sdCardAppReturn_t sdCardWriteReadDeInit(void)
{

    sdCardAppReturn_t sdcWriteReadDeInitStatus = APP_ERR_DEINIT_FAILED; /* Default return value is in Error Deinit state */

    if (pdFAIL != xTimerStop(sdCardWriteReadHandle,
            WRITEREAD_BLOCK_TIME))
    {
        sdcWriteReadDeInitStatus = APP_ERR_NO_ERROR;
    }

    return (sdcWriteReadDeInitStatus);
}

/* API documentation is in the interface header */
sdCardAppReturn_t sdCardSingleBlockWriteRead(uint32_t sector)
{

    sdCardAppReturn_t sdcWriteReadStatus; /* Default return value is in Error state */

    if (diskInitStatus != (Retcode_T) SDCARD_STATUS_INITIALIZED)
    { /*SD-Card Disk Not Initialized */
        diskInitStatus = SDCardDriver_DiskInitialize(DRIVE_ZERO);/* Initialize SD card */
    }
    else
    {
        ;/* Do nothing */
    }

    if (diskInitStatus == (Retcode_T) SDCARD_STATUS_INITIALIZED)
    {
        if (SDCardDriver_DiskWrite(DRIVE_ZERO, writeBuffer, sector, SINGLE_BLOCK) == RETCODE_OK)
        {/* Write Data into SD-Card */
            if (SDCardDriver_DiskRead(DRIVE_ZERO, readBuffer, sector, SINGLE_BLOCK) == RETCODE_OK)
            {/* Read Data's from the SD-CARD */
                int8_t sdCardWriteReadCheck; /* Validate the SD_card Write/read Buffer */
                sdCardWriteReadCheck = strncmp((const char*) writeBuffer, (const char*) readBuffer, sizeof(writeBuffer));
                if (INT8_C(0) == sdCardWriteReadCheck )
                {
                    sdcWriteReadStatus = APP_ERR_NO_ERROR;
                }
                else
                {
                    sdcWriteReadStatus = APP_ERR_ERROR;
                }
            }
            else
            { /* SDC Read failure */
                printf("\r\nSD_CARD Read failure\n\r");
                sdcWriteReadStatus = APP_ERR_READ_FAILURE;
            }
        }
        else
        { /* SDC Write failure */
            printf("\r\nSD_CARD Write failure\n\r");
            sdcWriteReadStatus = APP_ERR_WRITE_FAILURE;
        }
    }
    else
    {
        printf("\r\nSD_CARD Initialize failure\n\r");
        sdcWriteReadStatus = APP_ERR_INIT_FAILED;
    }

    return (sdcWriteReadStatus);
}

/* API documentation is in the interface header */
sdCardAppReturn_t sdCardFatFileSystemWriteRead(void)
{
    FIL fileObject; /* File objects */
    int8_t ramBufferWrite[BUFFER_SIZE]; /* Temporary buffer for write file */
    int8_t ramBufferRead[BUFFER_SIZE]; /* Temporary buffer for read file */
    const int8_t stringTestBuffer[] =
            "This content is created by SD card Read/Write functionality using FAT32 file system\n";
    uint16_t fileSize;
    UINT bytesWritten;
    UINT bytesRead;
    uint8_t fileStatus = FILE_EQUAL;

    sdCardAppReturn_t result;
    FRESULT fileSystemResult;

    /*Step1 - Initialization of file buffer write */
    fileSize = sizeof(stringTestBuffer);
    for (uint32_t index = 0; index < fileSize; index++)
    {
        ramBufferWrite[index] = stringTestBuffer[index];
    }

    /* step2 - Initialization of SD card */
    if (diskInitStatus != (Retcode_T) SDCARD_STATUS_INITIALIZED) /*SD-Card Disk Not Initialized */
    {
        diskInitStatus = SDCardDriver_DiskInitialize(DRIVE_ZERO); /* Initialize SD card */
    }
    else
    {
        /* SD card already initialized. So do nothing */
    }

    if ((Retcode_T) SDCARD_STATUS_INITIALIZED == diskInitStatus)
    {
        /*Step1 - open the file to write */
        /*  If file does not exist create it*/
        fileSystemResult = f_open(&fileObject, TEST_FILENAME,
        FA_OPEN_ALWAYS | FA_WRITE);
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot create the file */
            return (APP_ERR_ERROR);
        }

        /*Step2 - Set the file write pointer to first location */
        fileSystemResult = f_lseek(&fileObject, f_size(&fileObject));
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot set the file write pointer */
            return (APP_ERR_ERROR);
        }

        /*Step3 - Write a buffer to file*/
        fileSystemResult = f_write(&fileObject, ramBufferWrite, fileSize,
                &bytesWritten);
        if ((fileSystemResult != FR_OK) || (fileSize != bytesWritten))
        {
            /* Error. Cannot write the file */
            return (APP_ERR_ERROR);
        }

        /*Step4 - Close the file */
        fileSystemResult = f_close(&fileObject);
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot close the file */
            return (APP_ERR_ERROR);
        }

        /*Step5 - Open the file for read */
        fileSystemResult = f_open(&fileObject, TEST_FILENAME, FA_READ);
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot create the file */
            return (APP_ERR_ERROR);
        }

        /*Step6 - Set the file read pointer to first location */
        fileSystemResult = f_lseek(&fileObject, (f_size(&fileObject) - fileSize));
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot set the file pointer */
            return (APP_ERR_ERROR);
        }

        /*Step7 - Read some data from file */
        fileSystemResult = f_read(&fileObject, ramBufferRead, fileSize,
                &bytesRead);
        if ((fileSystemResult == FR_OK) && (fileSize == bytesRead))
        {
            result = APP_ERR_NO_ERROR;
        }
        else
        {
            /* Error. Cannot read the file */
            return (APP_ERR_ERROR);
        }

        /*Step8 - Close the file */
        fileSystemResult = f_close(&fileObject);
        if (fileSystemResult != FR_OK)
        {
            /* Error. Cannot close the file */
            return (APP_ERR_ERROR);
        }

        /*Step9 - Compare ramBufferWrite and ramBufferRead */
        for (uint8_t index = 0; index < fileSize; index++)
        {
            if ((ramBufferWrite[index]) != (ramBufferRead[index]))
            {
                /* Error compare buffers*/
                fileStatus = FILE_NOT_EQUAL;
            }
        }

        if (FILE_EQUAL == fileStatus)
        {
            return (APP_ERR_NO_ERROR);
        }
        else
        {
            return (APP_ERR_ERROR);
        }
    }
    else
    {
        result = (APP_ERR_INIT_FAILED);
    }
    return (result);
}

/** ************************************************************************* */

