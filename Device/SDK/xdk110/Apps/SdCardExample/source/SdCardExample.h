/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef XDK110_SDCARDEXAMPLE_H_
#define XDK110_SDCARDEXAMPLE_H_
/* local interface declaration ********************************************** */

/* local type and macro definitions */
/**
 *  @brief Error code returned by APIs of SD card application
 */
typedef enum sdCardAppReturn_e
{
    APP_ERR_DEINIT_FAILED = INT8_C(-1), /**< SD-Card DeInitialize failure */
    APP_ERR_INIT_FAILED = INT8_C(-2), /**< SD-Card Initialize failure */
    APP_ERR_ERROR = INT8_C(-3), /**< SD-Card Non-Error return */
    APP_ERR_WRITE_FAILURE = INT8_C(-4), /**< SD Card return write failure */
    APP_ERR_READ_FAILURE = INT8_C(-5), /**< SD Card return read failure */
    APP_ERR_NOTREADY_FAILURE = INT8_C(-6), /**< SD Card return Not ready failure */
    APP_ERR_NO_ERROR = INT8_C(0) /**< SD Card Error Return */
} sdCardAppReturn_t;
#define DEFAULT_LOGICAL_DRIVE        ""             /**< Macro to define default logical drive */
#define FORCE_MOUNT                 UINT8_C(1)      /**< Macro to define force mount */
#define TIMER_AUTORELOAD_ON         UINT32_C(1)      /**< Auto reload of timer is enabled*/
#define TIMER_AUTORELOAD_OFF        UINT32_C(0)      /**< Auto reload of timer is enabled*/
#define WRITEREAD_DELAY 			UINT32_C(5000) 	 /**< Millisecond delay for WriteRead timer task */
#define WRITEREAD_BLOCK_TIME 		UINT32_C(0xffff) /**< Macro used to define block time of a timer*/
#define SINGLE_BLOCK				UINT8_C(1)      /**< SD- Card Single block write or read */
#define DRIVE_ZERO				    UINT8_C(0)      /**< SD Card Drive 0 location */
#define PARTITION_RULE_FDISK	    UINT8_C(0)      /**< SD Card Drive partition rule 0: FDISK, 1: SFD */
#define AUTO_CLUSTER_SIZE		    UINT8_C(0)      /**< zero is given, the cluster size is determined depends on the volume size. */
#define SEEK_FIRST_LOCATION		    UINT8_C(0)      /**< File seek to the first location */
#define SECTOR_VALUE			    UINT8_C(1)      /**< SDC Disk sector value */
#define SINGLE_SECTOR_LEN           UINT32_C(512)   /**< Single sector size in SDcard */

#define SDCARD_LED1_ON \
	PTD_pinOutSet((GPIO_Port_TypeDef)PTD_PORT_LED_RED,(unsigned int)PTD_PIN_LED_RED) 	/**< Switch ON LED1 */
#define SDCARD_LED1_OFF  \
	PTD_pinOutClear((GPIO_Port_TypeDef)PTD_PORT_LED_RED,(unsigned int)PTD_PIN_LED_RED)	/**< Switch OFF LED1 */

#define TEST_FILENAME    "test_xdk.txt"	/**< Filename to open/write/read from SD-card */

/* Ram buffers
 * BUFFERSIZE should be between 512 and 1024, depending on available ram on efm32
 */
#define BUFFER_SIZE       UINT16_C(512)
#define FILE_EQUAL 		     UINT8_C(0)
#define FILE_NOT_EQUAL 	     UINT8_C(1)

/* local function prototype declarations */

/**
 * @brief
 *      The sdCardSingleBlockWriteRead API is used to write some string and read the string.
 *      then validate the written and read string.
 *
 * @param[in] sector
 *        SDC Disk sector value for read/Write process.
 *
 * @retval
 *      ERR_NO_ERROR  Task was created successfully.
 *
 * @retval
 *      ERR_ERROR  Task not started due to some error in OS.
 */
sdCardAppReturn_t sdCardSingleBlockWriteRead(uint32_t sector);

/**
 * @brief
 *      The sdCardFatFileSystemWriteRead API uses the FAT file system library calls.
 *      This API will mount and create the file system, then it will open, seek write,
 *      read and close the files. This API will compare the contents which has been
 *      written and read.
 *
 * @retval
 *      ERR_NO_ERROR - All the file operations are success
 *
 * @retval
 *      ERR_ERROR - one of the file operation failed
 *
 * @retval
 *      APP_ERR_INIT_FAILED - disk has not been initialized
 */
sdCardAppReturn_t sdCardFatFileSystemWriteRead(void);

/* local inline function definitions */

#endif /* XDK110_SDCARDEXAMPLE_H_ */

/** ************************************************************************* */
