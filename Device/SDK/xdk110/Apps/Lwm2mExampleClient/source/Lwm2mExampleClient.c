/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* system header files */
#include <stdio.h>
#include "BCDS_Basics.h"

/* additional interface header files */
#include <Serval_Network.h>
#include <Serval_Lwm2m.h>
#include <Serval_HttpClient.h>
#include <Serval_Basics.h>
#include <Serval_Log.h>
#include <FreeRTOS.h>
#include <task.h>

#include "PAL_initialize_ih.h"
#include "PAL_socketMonitor_ih.h"
#include "BCDS_WlanConnect.h"
#include "BCDS_NetworkConfig.h"
#include "AppLwm2mInterface.h"
#include "BCDS_Assert.h"
/* own header files */
#include "Lwm2mExampleClient.h"

/* constant definitions ***************************************************** */
#define LOG_MODULE "CCA"

/* local variables ********************************************************** */

/* global variables ********************************************************* */
xTaskHandle initializationTask;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the header file. */
static retcode_t wlanConnect(void)
{
    NetworkConfig_IpSettings_T myIpSettings;
    char ipAddress[PAL_IP_ADDRESS_SIZE] = { 0 };
    Ip_Address_T* IpaddressHex = Ip_getMyIpAddr();
    WlanConnect_SSID_T connectSSID;
    WlanConnect_PassPhrase_T connectPassPhrase;
    Retcode_T ReturnValue = (Retcode_T)RETCODE_FAILURE;
    int32_t Result = INT32_C(-1);

    if (RETCODE_OK != WlanConnect_Init())
    {
        return (RC_PLATFORM_ERROR);
    }

    connectSSID = (WlanConnect_SSID_T) WLAN_CONNECT_WPA_SSID;
    connectPassPhrase = (WlanConnect_PassPhrase_T) WLAN_CONNECT_WPA_PASS;

    ReturnValue = NetworkConfig_SetIpDhcp(NULL);
    if (ReturnValue != RETCODE_OK)
    {
        printf("Error in setting IP to DHCP\n\r");
        return (RC_PLATFORM_ERROR);
    }

    if (RETCODE_OK == WlanConnect_WPA(connectSSID, connectPassPhrase, NULL))
    {
        ReturnValue = NetworkConfig_GetIpSettings(&myIpSettings);
        if (RETCODE_OK == ReturnValue)
        {
            *IpaddressHex = Basics_htonl(myIpSettings.ipV4);
            Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
            if (Result < INT32_C(0))
            {
                printf("Couldn't convert the IP address to string format \r\n ");
                return (RC_PLATFORM_ERROR);
            }
            printf("Connected to WPA network successfully \r\n ");
            printf(" Ip address of the device %s \r\n ", ipAddress);
            return (RC_OK);
        }
        else
        {
            printf("Error in getting IP settings\n\r");
            return (RC_PLATFORM_ERROR);
        }
    }
    else
    {
        return (RC_PLATFORM_ERROR);
    }

}

/* global functions ********************************************************** */

/** @brief This function does the initialization of required setup to interact with lwm2m Server and the objects that are defined for this project.
 *  It includes providing the necessary data for the objects like object and resource ID, object name,
 *  its instances.
 */
void init(void)
{
    char buffer[128];
    retcode_t rc = RC_OK;
    int32_t Result = INT32_C(-1);

    printf("INIT Starting up\r\n");

    printf("wlanConnect\r\n");
    rc = wlanConnect();
    if (RC_OK != rc)
    {
        printf("Network init/connection failed %i \r\n", rc);
        return;
    }
    printf("Network Initialization done\r\n");

    printf("PAL_initialize\r\n");
    rc = PAL_initialize();
    if (RC_OK != rc)
    {
        printf("PAL and network initialize %i \r\n", rc);
        return;
    }
    printf("PAL Initialization done\r\n");

    LOG_INFO("Logging enabled\r\n");

    printf("PAL_socketMonitorInit\r\n");
    PAL_socketMonitorInit();
    printf("PAL SocketMonitor done\n");

    /* IP address */
    Ip_Address_T * my_ip = Ip_getMyIpAddr();
    Result = Ip_convertAddrToString(my_ip, buffer);
    if (Result < 0)
    {
        printf("Couldn't convert the IP address to string format \r\n ");
        return;
    }
    printf("IP Address of the Device is %s\r\n", buffer);

    printf("Lwm2mInterface Initialization start\r\n");
    rc = Lwm2mInterfaceInitialize();
    if (RC_OK != rc)
    {
        assert(false);
    }
    printf("Lwm2m Interface done\r\n");

    printf("Lwm2m Interface start with given port\r\n");
    rc = Lwm2mInterfaceStart(Ip_convertIntToPort(CLIENT_CONNECT_PORT));
    if (RC_OK != rc)
    {
        assert(false);
    }
    printf("Lwm2m Interface started\r\n");

    printf("LCA Initialization Done\r\n");
}

void application(void * pvParameters)
{
    BCDS_UNUSED(pvParameters);
    init();
    /* Task Suspended once the init done */
    vTaskSuspend(initializationTask);
}

/**
 * @brief This is a template function where the user can write his custom application.
 *
 */
void appInitSystem(xTimerHandle xTimer)
{
    (void) (xTimer);
    /*initialize LCA module*/
    if (xTaskCreate(application, (const char * const ) "application",TASK_STACK_SIZE,NULL, TASK_PRIORITY, &initializationTask) != pdPASS)
    {
        assert(false);
    }
}
/** ************************************************************************* */
