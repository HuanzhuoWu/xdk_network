/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

#include "AppLwm2mInterface.h"

#warning Enable and disable security here, by default security has disabled by defining SECURITY_ENABLE as 0.
#define SECURITY_ENABLE 0

#if SERVAL_ENABLE_COAP_SERVER

#include <Serval_Network.h>
#include <Serval_Log.h>
#include <Serval_Timer.h>
#include <Serval_Msg.h>
#include <Serval_Coap.h>
#include <Serval_CoapServer.h>
#include <Serval_CoapClient.h>
#include <Serval_Clock.h>
#include <Serval_Ip.h>
#include "led.h"
#include "XdkBoardHandle.h"
#include <Serval_Lwm2m.h>
#include "AppLwm2mObjects.h"
#include <FreeRTOS.h>
#include <timers.h>
#include "BCDS_PowerMgt.h"

#define LOG_MODULE "CCA"

/* global variables ********************************************************* */
static LED_handle_tp redLedHandle = (LED_handle_tp) NULL; /**< variable to store red led handle */
static LED_handle_tp yellowLedHandle = (LED_handle_tp) NULL; /**< variable to store yellow led handle */
static LED_handle_tp orangeLedHandle = (LED_handle_tp) NULL; /**< variable to store orange led handle */
static uint8_t RegistrationUpdateRetry = UINT8_C(0);

/** variable to store timer handle*/
xTimerHandle registrationUpdateTimer_ptr;
xTimerHandle timeChangeTimer_ptr;

extern Lwm2mDevice_T deviceResourceInfo;

/* local functions ********************************************************** */

/* @brief
 * This function is used to update the time when the device has registered first attempt into the Leshan Server.
 *
 * */
void timeChanged(xTimerHandle pxTimer)
{
    BCDS_UNUSED(pxTimer);

    retcode_t rc = RC_OK;

    /* Here URI_Path is the path points to the "Current Time" Resource */
    Lwm2m_URI_Path_T currentTimeUriPath = { OBJECT_INDEX_VALUE, OBJECT_INSTANCE_INDEX_VALUE, OBJECT_RESOURCE_NUMBER };
    rc = Lwm2mReporting_resourceChanged(&currentTimeUriPath);
    if (rc != RC_OK)
    {
        printf("Could not send notification\r\n");
    }
}

/* @brief
 * This function is used to update the registration.
 *
 * */
void RegistrationUpdate(xTimerHandle pxTimer)
{
    BCDS_UNUSED(pxTimer);
    retcode_t rc = RC_OK;

    rc = Lwm2mRegistration_update(SERVER_INDEX);
    if (RC_OK == rc)
    {
        RegistrationUpdateRetry = UINT8_C(0);
    }
    else
    {
        printf("registration Update failed\r\n");
        RegistrationUpdateRetry++;
    }
    if (RegistrationUpdateRetry >= MAX_REGISTRATION_UPDATE_RETRY)
    {
        printf("Max no. of retries has been done for registration update\n\r");
        if (xTimerStop(registrationUpdateTimer_ptr, TIMERBLOCKTIME) != pdTRUE)
        {
            assert(false);
        }
    }
}

/**
 * @brief The function is to get the registration status of the Device while connection to LWM2M Server 
 *	and It will toggle the Orange LED to indicate the Registration Success State and Red LED will indicate the Registration Failure state.
 */
static void registrationCallback(retcode_t status)
{
    if (status == RC_OK)
    {
        printf("Registration succeeded\r\n");
        LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;
        returnValue = LED_setState(redLedHandle, LED_SET_OFF);
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(orangeLedHandle, LED_SET_ON);
        }
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(yellowLedHandle, LED_SET_OFF);
        }
        if (returnValue != LED_ERROR_OK)
        {
            printf("Setting LED state failed\r\n");
        }
        if (xTimerStart(registrationUpdateTimer_ptr, TIMERBLOCKTIME) != pdTRUE)
        {
            assert(false);
        }
    }
    else
    {
        LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;
        returnValue = LED_setState(redLedHandle, LED_SET_ON);
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(orangeLedHandle, LED_SET_OFF);
        }
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(yellowLedHandle, LED_SET_OFF);
        }
        if (returnValue != LED_ERROR_OK)
        {
            printf("Setting LED state failed\r\n");
        }
        printf("registration failed " RC_RESOLVE_FORMAT_STR "\n", RC_RESOLVE((unsigned int )status));
        /* TODO maybe resue the registrationUpdateTimer_ptr for retrying...*/
    }
}
/**
 * @brief The function is to get the Updated Registration status of the Device while re-connection to LWM2M Server.
 */
static void registrationUpdateCallback(retcode_t status)
{
    if (status == RC_OK)
    {
        printf("Registration update succeeded\r\n");
    }
    else
    {
        printf("registration update failed " RC_RESOLVE_FORMAT_STR "\n", RC_RESOLVE((unsigned int )status));
        /* TODO maybe resue the registrationUpdateTimer_ptr for retrying...*/
    }
}
/**
 * @brief The function is to deregister the device in LWM2M Server.
 */
static void deregistrationCallback(retcode_t status)
{
    if (status == RC_OK)
    {
        printf("Deregistration succeeded\r\n");
    }
    else
    {
        printf("deregistration failed " RC_RESOLVE_FORMAT_STR "\n", RC_RESOLVE((unsigned int )status));
        /* TODO maybe resue the registrationUpdateTimer_ptr for retrying...*/
    }
}
/**
 * @brief The callback will get triggerd whenever there is a Notification event.
 */
static void NotificationCallback(retcode_t status)
{
    if (status == RC_OK)
    {
        printf("Notification succeeded\r\n");
        RegistrationUpdateRetry = UINT8_C(0);
        if ( xTimerReset(registrationUpdateTimer_ptr, TIMERBLOCKTIME) != pdTRUE)
        {
            assert(false);
        }
    }
    else
    {
        printf("Notification failed " RC_RESOLVE_FORMAT_STR "\n", RC_RESOLVE((unsigned int )status));
    }
}
/**
 * @brief The function will get called when we trying to register to the LWM2M Server with configured port Number and also It will provide the RED & Orange LED indications to represent the Registration process.
 */
static void applicationCallback(Lwm2m_Event_Type_T eventType, Lwm2m_URI_Path_T *path, retcode_t status)
{
    BCDS_UNUSED(path);

    if (eventType == LWM2M_EVENT_TYPE_REGISTRATION)
    {
        printf("Registration process has started\r\n");
        LED_errorTypes_t returnValue = LED_ERROR_INVALID_PARAMETER;
        returnValue = LED_setState(redLedHandle, LED_SET_ON);
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(orangeLedHandle, LED_SET_ON);
        }
        if (LED_ERROR_OK == returnValue)
        {
            returnValue = LED_setState(yellowLedHandle, LED_SET_OFF);
        }
        if (returnValue != LED_ERROR_OK)
        {
            printf("Setting LED state failed\r\n");
        }
        registrationCallback(status);
    }
    else if (eventType == LWM2M_EVENT_TYPE_REGISTRATION_UPDATE)
    {
        registrationUpdateCallback(status);
    }
    else if (eventType == LWM2M_EVENT_TYPE_DEREGISTRATION)
    {
        deregistrationCallback(status);
    }
    else if (eventType == LWM2M_EVENT_TYPE_NOTIFICATION)
    {
        NotificationCallback(status);
    }
}

/**
 * @brief This will create handles for the LEDs
 *
 * @retval RETURN_FAILURE Led Handle is not created
 * @retval RETURN_SUCCESS Led Handle is created
 *
 */
static led_return_t createLed(void)
{
    led_return_t returnValue = RETURN_FAILURE;

    redLedHandle = LED_create(gpioRedLed_Handle, GPIO_STATE_OFF);
    if (redLedHandle != NULL)
    {
        yellowLedHandle = LED_create(gpioYellowLed_Handle, GPIO_STATE_OFF);
    }
    if (yellowLedHandle != NULL)
    {
        orangeLedHandle = LED_create(gpioOrangeLed_Handle, GPIO_STATE_OFF);
    }
    if (orangeLedHandle != NULL)
    {
        returnValue = RETURN_SUCCESS;
    }
    return (returnValue);
}
/* global functions ********************************************************* */

/* temporary, function will be exported by lwm2m_security in future version */
static inline retcode_t Lwm2mInterface_copyToBuffer(OutputBuffer_T* buffer, const char* data, uint16_t len)
{
    if ( buffer->writableLength == 0 )
    {
        buffer->length  = len;
        buffer->content = (char*) data;
        return RC_OK;
    }
    else if ( buffer->writableLength >= len )
    {
        buffer->length = len;
        memcpy(buffer->content, data, len);
        return RC_OK;
    }
    else
    {
        return RC_DTLS_INSUFFICIENT_MEMORY;
    }
}

static retcode_t Lwm2mInterface_securityCallback(SecurityToken_T token, SecurityData_T* tokenData)
{
    if ( token.type != PSK_PEER_KEY_AND_ID ) return RC_DTLS_UNSUPPORTED_TOKEN;

    retcode_t rc = Lwm2mSecurity_defaultCallback(token, tokenData);
    if (RC_DTLS_PEER_REJECTED == rc)
    {
        PeerKeyAndIdData_T* data = (PeerKeyAndIdData_T*) tokenData;
        Lwm2mServer_T* Server = Lwm2m_getServer(0);
        rc = Lwm2mInterface_copyToBuffer(&data->ourIdentity, Server->securityInfo.my_identity, strlen(Server->securityInfo.my_identity));
        if ( RC_OK == rc )
        {
            rc = Lwm2mInterface_copyToBuffer(&data->key, Server->securityInfo.secret_key, strlen(Server->securityInfo.secret_key));
        }
    }
    return rc;
}
/* API documentation is in the header file lwm2mInterface.h*/
retcode_t Lwm2mInterfaceStart(Ip_Port_T port)
{
    retcode_t rc = RC_OK;
    led_return_t LedReturnValue = RETURN_FAILURE;
    Security_setCallback(Lwm2mInterface_securityCallback);

    Lwm2mServer_T* server = Lwm2m_getServer(SERVER_INDEX);

#if SECURITY_ENABLE==0
    printf("coap without encryption\r\n");
    strncpy(server->serverAddress, "coap://192.168.1.133:5685", strlen("coap://192.168.1.133:5683"));
#elif SECURITY_ENABLE==1
    printf("coap with encryption\r\n");
    strncpy(server->serverAddress, "coaps://217.92.33.109:5894", strlen("coaps://217.92.33.109:5894"));
    strncpy(server->securityInfo.peer_identity, "Leshan", strlen("Leshan"));
    strncpy(server->securityInfo.my_identity, "XDK110_0", strlen("XDK110_0"));
    strncpy(server->securityInfo.secret_key, "Leshan", strlen("Leshan"));
#endif
    server->permissions[0] = LWM2M_READ_ALLOWED;
    Lwm2m_setNumberOfServers(NUMBER_OF_SERVERS);
    /* LED created to represent the registration status */
    LedReturnValue = createLed();
    if (LedReturnValue != RETURN_SUCCESS)
    {
        printf("Creating LED failed\r\n");
        assert(false);
    }
    rc = Lwm2m_start(port, applicationCallback);

    if (rc != RC_OK)
    {
        printf("Starting the Lightweight M2M module failed\r\n");
        assert(false);
    }
    uint32_t Ticks = PowerMgt_GetMsDelayTimeInSystemTicks(TIME_CHANGE_NOTIFICATION_DELAY);

    timeChangeTimer_ptr = xTimerCreate((const char * const ) "timeChangeTimer_ptr", // text name, used for debugging.
            Ticks, // The timer period in ticks.
            pdTRUE, // The timers will auto-reload themselves when they expire.
            (void *) NULL, // Assign each timer a unique id equal to its array index.
            timeChanged // On expiry of the timer this function will be called.
            );
    if (timeChangeTimer_ptr == NULL) /* Timer was not created */
    {
        assert(false);
    }
    if (xTimerStart(timeChangeTimer_ptr, TIMERBLOCKTIME) != pdTRUE)
    {
        assert(false);
    }

    uint32_t RegUpdateTicks = PowerMgt_GetMsDelayTimeInSystemTicks(REGISTRATION_UPDATE_DELAY);

    registrationUpdateTimer_ptr = xTimerCreate((const char * const ) "RegistrationUpdate_ptr", // text name, used for debugging.
            RegUpdateTicks, // The timer period in ticks.
            pdTRUE, // The timers will auto-reload themselves when they expire.
            (void *) NULL, // Assign each timer a unique id equal to its array index.
            RegistrationUpdate // On expiry of the timer this function will be called.
            );
    if (registrationUpdateTimer_ptr == NULL) /* Timer was not created */
    {
        assert(false);
    }

    rc = Lwm2mRegistration_register(SERVER_INDEX);
    if (RC_OK != rc)
    {
        printf("Registration Failed\r\n");
    }
    return rc;
}

/* API documentation is in the header file lwm2mInterface.h*/
retcode_t Lwm2mInterfaceInitialize(void)
{
    retcode_t rc = RC_OK;
#if SECURITY_ENABLE==0
    deviceResourceInfo.name = "XDK110_UNSECURE";
    deviceResourceInfo.secure = false;
#elif SECURITY_ENABLE==1
    deviceResourceInfo.name = "XDK110_SECURE_DTLS";
    deviceResourceInfo.secure = true;
#endif

    printf("Lwm2m_initialize\r\n");
    rc = Lwm2m_initialize(&deviceResourceInfo);
    printf("done\r\n");
    return rc;
}

#endif /* SERVAL_ENABLE_COAP_SERVER */
