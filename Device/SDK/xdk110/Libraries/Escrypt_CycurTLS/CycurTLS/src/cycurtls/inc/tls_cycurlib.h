/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Inclusion of necessary header files from CycurLIB
              and CycurLIB configuration checks.

$Rev: 481 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CYCURLIB_H_
#define ESC_TLS_CYCURLIB_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "cycurlib_config.h"

/* Header files that are always required */
#include "random.h"

/* Certificate parsing */
#if (EscTls_KEY_EXCHANGE_CERT_BASED)
#   include "x509.h"
#endif

/* Include asymmetric crypto depending on key exchange method */
#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
#   include "pkcs1_rsaes_v15.h"
#   include "pkcs1_rsassa_v15.h"
#elif (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)
#   include "ecc.h"
#elif (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
#   include "dh.h"
#endif

/* Include the HMAC for the PRF */
#if (EscTls_PRF_DIGEST == EscTls_PRF_SHA_256)
#   include "hmac_sha_256.h"
#endif

/* Include the HMAC for the MAC (in case of block cipher in CBC mode) */
#if (EscTls_HMAC == EscTls_SHA)
#   include "hmac_sha_1.h"
#elif (EscTls_HMAC == EscTls_SHA_256)
#   include "hmac_sha_256.h"
#endif

/* Include AES for either CBC or CCM mode */
#if (EscTls_CIPHER == EscTls_AES_128_CBC) || (EscTls_CIPHER == EscTls_AES_256_CBC)
#   include "aes_cbc.h"
#elif (EscTls_CIPHER == EscTls_AES_128_CCM_8) || (EscTls_CIPHER == EscTls_AES_256_CCM_8)
#   include "aes_ccm.h"
#endif


/***************************************************************************
 * 3. CONFIGURATION CHECKS                                                 *
 ***************************************************************************/

/* The TLS implementation requires to get all of the fields of the certificate */
#if (EscTls_KEY_EXCHANGE_CERT_BASED) && (EscX509_PARSE_INFO == 0)
#   error "EscX509_PARSE_INFO must be enabled in x509.h for certificate based key exchange"
#endif

/* Check AES max key size configuration */
#if (EscTls_CIPHER == EscTls_AES_256_CCM_8) || (EscTls_CIPHER == EscTls_AES_256_CBC)
#if (EscAes_MAX_KEY_BITS < 256U)
#   error "EscAes_MAX_KEY_BITS must be set to 256 in order to use AES-256 cipher suites"
#endif
#endif

/* Check ECC configuration */
#if (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)

/* At least one supported ECC curve must be enabled when using ECDHE_ECDSA */
#if ( (EscEcc_SECP_192_ENABLED == 0) && \
      (EscEcc_SECP_224_ENABLED == 0) && \
      (EscEcc_SECP_256_ENABLED == 0) && \
      (EscEcc_SECP_384_ENABLED == 0) && \
      (EscEcc_SECP_521_ENABLED == 0) && \
      (EscEcc_BRAINPOOL_P256_ENABLED == 0) && \
      (EscEcc_BRAINPOOL_P384_ENABLED == 0) && \
      (EscEcc_BRAINPOOL_P512_ENABLED == 0) )
#   error "At least one curve supported by TLS (sepc192/224/256/384/521, brainpoolp256/384/512)" \
          "must be activated in ecc_cfg.h"
#endif

/* ECDH must be enabled */
#if (EscEcc_ECDH_ENABLED == 0)
#   error "ECDH must be enabled in the ecc_cfg.h"
#endif

/* ECDSA must be enabled */
#if (EscEcc_ECDSA_ENABLED == 0)
#   error "ECDSA must be enabled in the ecc_cfg.h"
#endif

/* The X509 parser must support ECC certificates */
#if (EscX509_ALGORITHM_ECDSA_ENABLED == 0)
#   error "The support for X509 certificates with ECDSA must be enabled in x509.h"
#endif

#endif

/* Check RSA configuration */
#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
#if (EscX509_ALGORITHM_RSA_PKCS1V15_ENABLED == 0)
/* The X509 parser must support RSA certificates */
#   error "The support for X509 certificates with RSA PKCS#1 v1.5 must be enabled in x509.h"
#endif
#endif

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. CONSTANTS                                                            *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CYCURLIB_H_ */
