/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Implementation of TLS message processing - server specific code

$Rev: 572 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CORE_SERVER_H_
#define ESC_TLS_CORE_SERVER_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_types.h"
#include "tls_core_common.h"

#if EscTls_HAS_SERVER

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
 * Initialize server specific parts of the context.
 *
 * \param[in, out]  handshakeCtx    The core handshake context.
 */
void
EscTlsCoreSrv_Init(
    EscTlsCore_HandshakeContext *handshakeCtx);

#if EscTls_KEY_EXCHANGE_IS_PSK_BASED
/**
 * Sets the server identity hint which is sent to the client during the handshake.
 * Identity hint buffer must remain allocated during the complete handshake.
 * If not set or set to zero length, the ServerKeyExchange message during the handshake is omitted.
 *
 * \param[in,out]   ctx                     The TLS core handshake context.
 * \param[in]       serverIdentityHint      Pointer to the server identity hint. must remain allocated during the handshake.
 * \param[in]       serverIdentityHintLen   Length of the server identity.
 *
 */
void
EscTlsCoreSrv_SetServerIdentityHint(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_CHAR *serverIdentityHint,
    Esc_UINT16 serverIdentityHintLen );

/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in, out]  ctx             The TLS core handshake context.
 * \param[in]       callbackParam   User defined pointer which is passed to the callback (may be NULL).
 * \param[in]       pskCallback     Callback function for generating a pre-shared key given a client identity
 *                                  (see tls_types.h for the definition of the callback type).
 */
void
EscTlsCoreSrv_SetPskCallback(
    EscTlsCore_HandshakeContext *ctx,
    void *callbackParam,
    EscTls_PskServerCallback pskCallback);
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
/**
 * Sets the Diffie-Hellman parameters for the key exchange.
 * Parameters must fit to the configured options inside Diffie-Hellman module.
 * Parameter buffers must remain allocated during the complete handshake.
 *
 * \param[in,out]   ctx                 The TLS core handshake context.
 * \param[in]       dhModulus           Pointer to the DH modulus. must remain allocated during the handshake.
 * \param[in]       dhModulusLength     Length of the DH modulus.
 * \param[in]       dhGenerator         Pointer to the DH generator. must remain allocated during the handshake.
 * \param[in]       dhGeneratorLength   Length of the DH generator.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCoreSrv_SetDhParams(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_UINT8 *dhModulus,
    Esc_UINT16 dhModulusLength,
    const Esc_UINT8 *dhGenerator,
    Esc_UINT16 dhGeneratorLength );
#endif

#if (EscTls_KEY_EXCHANGE_CERT_BASED)
/**
 * Enable or disable client authentication. This function is only useful on the
 * server side. By default, no client authentication is used.
 *
 * \param[in,out]  ctx                  The TLS handshake context.
 * \param[in]      clientAuthEnabled    Flag to enable (TRUE) or disable (FALSE) client authentication.
 */
void
EscTlsCoreSrv_SetClientAuth(
    EscTlsCore_HandshakeContext *ctx,
    Esc_BOOL clientAuthEnabled);
#endif

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#endif /* EscTls_HAS_SERVER */

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CORE_SERVER_H_ */
