/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief     Low-level Interface to DTLS implementation.

 \see       RFC 6347

 */
/***************************************************************************/

#ifndef ESC_DTLS_INTERNAL_H_
#define ESC_DTLS_INTERNAL_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_cycurlib.h"

#include "tls_core.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** The maximum number of messages which can be generated in one flight */
#define EscDtls_MAX_MESSAGES_PER_FLIGHT         5U

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** DTLS 48-bit sequence number */
typedef struct
{
    /** Most significant two bytes of sequence number */
    Esc_UINT16 highPart16;

    /** Least significant four bytes of sequence number */
    Esc_UINT32 lowPart32;
} EscDtls_SequenceNumber;

/** State of the DTLS session */
typedef struct
{
    /** The actual TLS context. Must be the first member. */
    EscTlsCore_SessionContext coreCtx;

    /** Current epoch */
    Esc_UINT16 epoch;

    /** Sequence number for each epoch.
     *  Note that renegotiation is not supported and therefore, the epoch cannot exceed 2.
     */
    EscDtls_SequenceNumber sequenceNumbers[2];

    /** Highest sequence number received per epoch (for replay detection) */
    EscDtls_SequenceNumber highestSeqNum[2];

    /** Bit mask for replay detection. Indicates reception of the last 32 sequences per epoch. */
    Esc_UINT32 replayWindow[2];
} EscDtlsInt_SessionContext;

/** State of the DTLS handshake */
typedef struct
{
    /** The TLS handshake state */
    EscTlsCore_HandshakeContext coreCtx;

    /** The current timeout [ms] for retransmission. */
    Esc_UINT32 timeout;

    /** The current sequence number of sent handshake messages. This is the sequence number
     * as it appears in the handshake header of DTLS handshake messages */
    Esc_UINT16 writeSeqNumber;

    /** Identifiers of previously sent messages. This is used to keep track of which
     * messages were sent in a previous flight. The IDs are used to regenerate the same
     * messages again during a retransmission.
     */
    EscTls_HskId sentHskId[EscDtls_MAX_MESSAGES_PER_FLIGHT];

    /** Number of message IDs present in sentHskId. */
    Esc_UINT8 numSentHskMessages;

    /** Index of message inside a flight that is currently retransmitted */
    Esc_UINT8 retransmitHskIndex;

    /** A flag to indicate that the transmission of a flight is complete. This is used
     * to check whether a the transmission of a new flight is started.
     */
    Esc_BOOL transmissionComplete;

    /** A flag to indicate whether we are currently executing a retransmission. */
    Esc_BOOL retransmission;

    /** The expected handshake sequence number for received handshake messages */
    Esc_UINT16 readSeqNumber;

    /** The total size of the current handshake message.
     *  This is set when the first fragment of a handshake message is received.
     */
    Esc_UINT32 readMessageSize;

    /** Flag whether we have already stored a fragment of the current handshake message or not */
    Esc_BOOL fragmentStored;

    /** Start offset of the handshake message part that was completely received */
    Esc_UINT32 completeStart;

    /** End offset of the handshake message part that was completely received */
    Esc_UINT32 completeBehindEnd;

    /** Type of the current handshake message (e.g. ClientHello). Must be consistent for all fragments
     *  of a handshake message.
     */
    EscTls_HandshakeType handshakeType;

    /** Internal buffer that is used to defragment received messages. Used only during the handshake. */
    Esc_UINT8 *internalBuffer;
    /** Size of the internal buffer */
    Esc_UINT32 internalBufferSize;

    /** Backup of member writeSeqNumber. This is saved during normal transmission and
     * restored for retransmission.
     */
    Esc_UINT16 writeSeqNumberBackup;

    /** The sequence number of the last handshake message received in a flight.
     * If the same sequence number is received again, a retransmission is started.
     */
    Esc_UINT16 seqNumOfLastMessageInReceivedFlight;

    /** Next action to be executed in the handshake: send message, receive message or handshake complete. */
    EscTls_HandshakeAction nextAction;
} EscDtlsInt_HandshakeContext;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/* ------------------------------- Functions for initialization -------------------------- */

/**
 * Initializes a DTLS context.
 *
 * \param[in,out] sessionCtx          The DTLS session context.
 * \param[in,out] handshakeCtx        The DTLS handshake context.
 * \param[in]     internalBuffer      This buffer is used to defragment received handshake messages.
 *                                    It can be freed after the handshake is completed.
 * \param[in]     internalBufferSize  Size of the read buffer. Must be at least the
 *                                    size of the expected certificate chain.
 *                                    For PSK cipher suites, roughly 200 bytes should be sufficient.
 * \param[in]     prng                The pseudo-random number generator (PRNG) to be used
 *                                    to provide random numbers for TLS. See random.h.
 * \param[in]     prngState           The state of the PRNG. May be NULL if the PRNG implementation
 *                                    is stateless.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_Init(
    EscDtlsInt_SessionContext *sessionCtx,
    EscDtlsInt_HandshakeContext *handshakeCtx,
    Esc_UINT8 *internalBuffer,
    Esc_UINT32 internalBufferSize,
    EscRandom_GetRandom prng,
    void *prngState);

#if (EscTls_KEY_EXCHANGE_CERT_BASED)
/**
 * Sets the current Unix time for an upcoming session. The timestamp is used to check
 * whether the peer's certificate is expired.
 *
 * \param[in,out]   ctx             The DTLS handshake context.
 * \param[in]       gmtUnixTime     The current Unix timestamp
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetTime(
    EscDtlsInt_HandshakeContext *ctx,
    Esc_UINT32 gmtUnixTime );

/**
 * Sets a callback for additional verification of the peer's certificate.
 * A typical use-case on client side is to check the domain name of the server certificate.
 * This callback is invoked after the validation of the peer's certificate chain.
 * Setting the callback to Esc_NULL_PTR effectively disables the callback.
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       callback    The callback to be executed. For details, see tls_types.h
 * \param[in,out]   param       An arbitrary, additional parameter to be passed to the callback.
 *                              It can be NULL if it is not used.
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetVerifyCallback(
    EscDtlsInt_HandshakeContext *ctx,
    EscTls_VerifyCertCallback callback,
    void *param);

/**
 * Sets the trusted certificates for a DTLS context.
 * Note: This function does not create a copy of the given arrays. Therefore,
 * they must remain allocated throughout the entire handshake!
 *
 * \param[in,out]   ctx                 The DTLS handshake context.
 * \param[in]       trustedRootCert     List of trusted root certificates
 * \param[in]       numTrustedRootCert  Number of root certificates in the list
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetTrustedCertificates(
    EscDtlsInt_HandshakeContext *ctx,
    EscX509_CertificateT trustedRootCert[],
    Esc_UINT16 numTrustedRootCert);

/**
 * Sets the certificate chain for authentication.
 * Note: This function does not create a copy of the given arrays. Therefore,
 * they must remain allocated throughout the entire handshake!
 *
 * \param[in,out]   ctx                 The DTLS handshake context.
 * \param[in]       certificateChain    List of certificates in the certificate chain,
 *                                      lowest certificate first.
 * \param[in]       numCertificates     Number of certificates in the chain
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetCertificateChain(
    EscDtlsInt_HandshakeContext *ctx,
    const EscTls_CertificateT certificateChain[],
    Esc_UINT16 numCertificates);

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
/**
 * Sets the private key for authentication.
 * Note: This function does not create a copy of the given private key. Therefore,
 * it must remain allocated throughout the entire handshake!
 *
 * \param[in,out]   ctx         The DTLS handshake context.
 * \param[in]       privateKey  Pointer to the RSA private key
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetRsaPrivateKey(
    EscDtlsInt_HandshakeContext *ctx,
    const EscRsa_PrivKeyT *privateKey);
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)
/**
 * Sets the ECC private key for authentication. This key MUST be the private key
 * corresponding to the public key contained in the certificate set via
 * EscTls_SetCertificateChain(). The key expected here is a raw key and not encoded
 * in any way (no PEM, DER, ASN.1, PKCS#8).
 * Note that this function does not create a deep copy of the key, but only
 * stores the pointer. Therefore, the key data must remain allocated for
 * the duration of the handshake!
 *
 * \param[in,out]   handshakeCtx    The TLS handshake context.
 * \param[in]       keyData         The ECC private key.
 * \param[in]       keyDataLen      The length of the ECC key in bytes.
 * \param[in]       curveType       The elliptic curve on which the key is defined. The following values are permitted:
 *                                  - EscEcc_CURVE_SECP_192  (for NIST P-192)
 *                                  - EscEcc_CURVE_SECP_224  (for NIST P-224)
 *                                  - EscEcc_CURVE_SECP_256  (for NIST P-256)
 *                                  - EscEcc_CURVE_SECP_384  (for NIST P-384)
 *                                  - EscEcc_CURVE_SECP_521  (for NIST P-521)
 *                                  - EscEcc_CURVE_BRAINPOOL_P256  (for BrainpoolP256r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P384  (for BrainpoolP384r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P512  (for BrainpoolP512r1)
 *
 * \retval EscTlsRet_UNEXPECTED_DATA if the curve type is unknown or disabled in the configuration, or if
 *                                   the private key's length does not match the curve type.
 * \retval EscTlsRet_OK on success.
 */
EscTls_RETURN
EscDtlsInt_SetEccPrivateKey(
    EscDtlsInt_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *keyData,
    Esc_UINT16 keyDataLen,
    EscEcc_CurveId curveType);
#endif


#elif EscTls_KEY_EXCHANGE_IS_PSK_BASED

/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in,out]   ctx             The DTLS handshake context.
 * \param[in]       callbackParam   User defined pointer which is passed to the callback (may be NULL).
 * \param[in]       pskCallback     Callback function for generating a pre-shared key
 *                                  and a client identity given a server hint.
 *                                  (see tls_types.h for the definition of the callback type)
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_SetClientPskCallback(
    EscDtlsInt_HandshakeContext *ctx,
    void *callbackParam,
    EscTls_PskClientCallback pskCallback);
#endif

/* ------------------------------- Functions for executing the handshake -------------------------- */

/**
 * Process a previously received network packet containing exactly one or
 * more records with one or more handshake fragments. The function decodes the
 * records contained inside and stores them into the internal buffer (whose
 * address and size is stored in the context).
 *
 * This function must be called initially by the server and whenever
 * the implementation expects more data from the peer (as indicated
 * by next handshake action EscTls_HSACTION_RECV_DATA).
 *
 * \param[in]   sessionCtx          The DTLS session context.
 * \param[in]   handshakeCtx        The DTLS handshake context.
 * \param[in]   inputBuffer         The buffer containing the network packet.
 * \param[in]   packetSize          Size of the network packet in bytes.
 * \param[out]  nextAction          Next handshake action (send/receive/completed).
 *                                  This parameter is only valid if the function returns
 *                                  with EscTlsRet_OK or EscTlsRet_WARNING_DETECTED!
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_CLOSED_BY_PEER    The peer has shut down the connection. In this case, a close_notify
 *                                     alert must be sent (\see EscTlsInt_Shutdown()) and the connection
 *                                     must be closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED  The peer has sent a fatal alert. The connection must be closed
 *                                         immediately in this case. A detailed error code can be obtained
 *                                         by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be generated
 *                                     and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_IGNORED           One or more invalid records were received. According to the DTLS standard,
 *                                     such errors must be ignored. However, diagnostic logging can be done
 *                                     using this return code. If an invalid record is received,
 *                                     all further records in the buffer are ignored.
 */
EscTls_RETURN
EscDtlsInt_HandshakeReceived(
    EscDtlsInt_SessionContext *sessionCtx,
    EscDtlsInt_HandshakeContext *handshakeCtx,
    Esc_UINT8 *inputBuffer,
    Esc_UINT16 packetSize,
    EscTls_HandshakeAction *nextAction);

/**
 * Generates a handshake packet to be sent to the peer. The packet may contain
 * one or more handshake messages/fragments.
 *
 * This function must be called initially by the client and whenever
 * the implementation needs to send more data to the peer as indicated
 * by next handshake action EscTls_HSACTION_SEND_DATA.
 *
 * \param[in,out]   sessionCtx          The DTLS session context.
 * \param[in,out]   handshakeCtx        The DTLS handshake context.
 * \param[in]       outputBuffer        The buffer to store the network packet.
 * \param[in]       outputBufferSize    Size of the buffer in bytes.
 * \param[out]      packetSize          Number of bytes stored in the buffer (size of the packet).
 * \param[out]      nextAction          Next handshake action (send/receive/completed).
 *                                      This parameter is only valid if the function
 *                                      returns with EscTlsRet_OK or EscTlsRet_WARNING_DETECTED!
 *
 * \retval EscTlsRet_OK                 On success.
 * \retval EscTlsRet_ERROR_DETECTED     A fatal error occurred. An alert with fatal severity must
 *                                      be generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                      The session is closed afterwards. A detailed error code can
 *                                      be obtained by calling EscDtlsInt_GetLastError().
 */
EscTls_RETURN
EscDtlsInt_HandshakeToBeSent(
    EscDtlsInt_SessionContext *sessionCtx,
    EscDtlsInt_HandshakeContext *handshakeCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *packetSize,
    EscTls_HandshakeAction *nextAction);

/**
 * If the caller expects to receive data (i.e. current handshake action is EscTls_HSACTION_RECV_DATA),
 * but detects a timeout then he should call this function to trigger a re-transmission of the latest
 * flight. Next action after this function is always EscDtlsInt_HandshakeToBeSent().
 *
 * \param[in,out]   sessionCtx      The DTLS session context.
 * \param[in,out]   handshakeCtx    The DTLS handshake context.
 * \param[out]      nextAction      Next handshake action (send/receive/completed).
 *                                  This parameter is only valid if the function returns with EscTlsRet_OK!
 *
 * \return EscTlsRet_OK on success.
 */
EscTls_RETURN
EscDtlsInt_Retransmit(
    EscDtlsInt_SessionContext *sessionCtx,
    EscDtlsInt_HandshakeContext *handshakeCtx,
    EscTls_HandshakeAction *nextAction);

/**
 * Get the current timeout for receiving data that is recommended by the DTLS RFC.
 *
 * \param[in]   ctx     The DTLS handshake context.
 *
 * \return The timeout in milliseconds.
 */
Esc_UINT32
EscDtlsInt_GetRetransmissionTimeout(
    const EscDtlsInt_HandshakeContext *ctx);

/* ------------------------ Functions for processing application data ------------------------ */

/**
 * Get the maximum number of extra bytes that will be added to the application data
 * due to the protection (i.e. encryption and MAC). This function can be used
 * to calculate how large the output buffer for EscDtlsInt_AppDataToBeSent() must be.
 * The handshake must have been finished before calling this function!
 *
 * \param[in]   sessionCtx      The DTLS session context.
 * \param[out]  overheadSize    Number of extra bytes to be added for protection of application data.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscDtlsInt_GetMaxProtectionOverhead(
    EscDtlsInt_SessionContext *sessionCtx,
    Esc_UINT16 *overheadSize );

/**
 * Decodes application data received via the network. The provided buffer must contain one or
 * more complete records! The function processes only the first record in the buffer
 * and returns information on how many bytes were processed.
 * The data is decrypted and headers are removed. The handshake must have been
 * finished before calling this function!
 *
 * Input buffer and output buffer may be the same (in-place processing).
 *
 * \param[in,out] sessionCtx        The DTLS session context.
 * \param[in]   inputBuffer         The received input data to be decoded.
 *                                  Can be modified even if bytesWritten is zero!
 * \param[in]   packetSize          Size of the input data in bytes.
 * \param[out]  outputBuffer        Buffer where the decoded data is stored.
 * \param[in]   outputBufferSize    Size of the output buffer in bytes.
 * \param[out]  bytesWritten        Number of bytes stored in the output buffer.
 * \param[out]  bytesProcessed      Number of bytes which were decoded by this function.
 *                                  If bytesProcessed is less than packetSize then
 *                                  additional records are stored inside the buffer and
 *                                  bytesProcessed can be used as an offset to the buffer
 *                                  for decoding the next record.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_WARNING_DETECTED  A non fatal error occurred. An alert with warning severity must
 *                                     be generated and sent to the peer. The handshake can be proceed.
 *                                     (\see EscTlsInt_BuildAlert()) and sent to the peer. A detailed
 *                                     error code can be obtain by calling EscTlsInt_GetLastError().
 *                                     The remaining records are properly decoded!
 * \retval EscTlsRet_CLOSED_BY_PEER    The peer has shut down the connection. In this case, a close_notify
 *                                     alert must be sent (\see EscTlsInt_Shutdown()) and the connection
 *                                     must be closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED  The peer has sent a fatal alert. The connection must be closed
 *                                         immediately in this case. A detailed error code can be obtained
 *                                         by calling EscTlsInt_GetLastError().
 *                                         All records behind the alert are dropped and not decoded.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be
 *                                     generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_IGNORED           An invalid record was received. According to the DTLS standard,
 *                                     such errors must be ignored. However, diagnostic logging can be done
 *                                     using this return code. The state is not changed when this code is
 *                                     returned.
 */
EscTls_RETURN
EscDtlsInt_AppDataReceived(
    EscDtlsInt_SessionContext *sessionCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 packetSize,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten,
    Esc_UINT16 *bytesProcessed);

/**
 * Encode application data for sending. The data is encrypted and encapsulated in
 * a TLS record.
 * The handshake must have been finished before calling this function!
 *
 * Note: Input buffer and output buffer may be the same (in-place processing).
 *
 * \param[in,out] sessionCtx        The DTLS session context.
 * \param[in]   inputBuffer         The input data to be encoded for sending.
 * \param[in]   appDataSize         Size of the input data in bytes.
 * \param[out]  outputBuffer        Buffer where the encoded packet is stored.
 * \param[in]   outputBufferSize    Size of the output buffer in bytes. Its size must be at least
 *                                  (appDataSize + EscDtlsInt_GetMaxProtectionOverhead()) bytes.
 * \param[out]  bytesWritten        Number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be
 *                                     generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can
 *                                     be obtained by calling EscDtlsInt_GetLastError().
 */
EscTls_RETURN
EscDtlsInt_AppDataToBeSent(
    EscDtlsInt_SessionContext *sessionCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 appDataSize,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten );

/* ------------------------ Function to shut down the connection ------------------------ */

/**
 * Shuts down the session. This function generates a close_notify alert to be sent to the peer
 * in order to inform it about the shutdown.
 *
 * \param[in,out] sessionCtx       The DTLS session context.
 * \param[out]  outputBuffer       Buffer where the "close notify" message is stored.
 * \param[in]   outputBufferSize   Size of the output buffer in bytes.
 * \param[out]  bytesWritten       Number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must
 *                                     be generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscDtlsInt_GetLastError().
 */
EscTls_RETURN
EscDtlsInt_Shutdown(
    EscDtlsInt_SessionContext *sessionCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten);

/* ------------------------ Functions for error handling --------------------------------- */

/**
 * This function should be called whenever any other DTLS function returns
 * Esc_ALERT_SEND_WARNING or Esc_ALERT_SEND_ERROR. It generates an alert message which must
 * be sent to the peer.
 *
 * Depending on whether this function is called in response to Esc_ALERT_SEND_WARNING or
 * Esc_ALERT_SEND_ERROR, the connection may continue or must be closed.
 * In case of Esc_ALERT_SEND_ERROR, the connection must be closed on the transport.
 * In case of Esc_ALERT_SEND_WARNING, the connection can continue.
 *
 * \param[in,out] sessionCtx          The DTLS session context.
 * \param[out]  outputBuffer          The buffer to store the alert message to.
 * \param[in]   outputBufferSize      The size of the output buffer in bytes.
 * \param[out]  bytesWritten          The number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred and the alert could not be created.
 *                                     The session should be closed on network level.
 */
EscTls_RETURN
EscDtlsInt_BuildAlert(
    EscDtlsInt_SessionContext *sessionCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten);

/**
 * Get the last error which occurred during the protocol.
 * For definitions of error values and types refer to tls_error.h.
 *
 * \param[in] sessionCtx   The DTLS session context.
 *
 * \return previously occurred error
 */
EscTls_RETURN
EscDtlsInt_GetLastError(
    const EscDtlsInt_SessionContext *sessionCtx );

/**
 * Get the connection's current top level state.
 *
 * \param[in]   sessionCtx     The DTLS session context.
 *
 * \retval  EscTls_STATE_HANDSHAKE              The handshake is in progress.
 * \retval  EscTls_STATE_CONNECTED              The connection is established. Application data can be sent/received.
 * \retval  EscTls_STATE_CLOSED                 The connection was closed locally or by the host.
 *                                              Note that this only refers to the DTLS layer, but has
 *                                              no meaning for the underlying transport layer.
 * \retval  EscTls_STATE_ERROR                  An error occurred during the session and the session cannot be used
 *                                              any further.
 */
EscTls_ConnectionState
EscDtlsInt_GetConnectionTopLevelState(
    const EscDtlsInt_SessionContext *sessionCtx );

/**
 * Get the connection's current state.
 *
 * \param[in]   sessionCtx     The DTLS session context.
 *
 * \return  The current connection state
 */
EscTls_ConnectionState
EscDtlsInt_GetConnectionState(
    const EscDtlsInt_SessionContext *sessionCtx );

/**
 * Set the connection's current state (top and sub level state).
 * This function must be used with care and should only be used for setting error code and not for restoring a closed connection.
 *
 * \param[in]   sessionCtx     The TLS session context.
 * \param[in]   state   The new connection state.
 *
 */
void
EscDtlsInt_SetConnectionState(
    EscDtlsInt_SessionContext *sessionCtx,
    EscTls_ConnectionState state );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
    }
#endif

#endif /* ESC_DTLS_INTERNAL_H_ */
