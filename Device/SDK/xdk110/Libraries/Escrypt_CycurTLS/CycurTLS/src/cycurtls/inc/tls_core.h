/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Implementation of TLS message processing

$Rev: 588 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CORE_H_
#define ESC_TLS_CORE_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_types.h"

#include "tls_core_client.h"
#include "tls_core_server.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/* ------------------------------- Initialization functions ------------------------------------ */

/**
 * Initializes a TLS core context
 *
 * \param[in]      isServer     If TRUE, the session is a server side session, otherwise
 *                              it is the client side.
 * \param[in, out] sessionCtx   The TLS core session context.
 * \param[in, out] handshakeCtx The TLS core handshake context.
 * \param[in]      prng         The pseudo-random number generator (PRNG) to be used
 *                              to provide random numbers for TLS. See random.h.
 * \param[in]      prngState    The state of the PRNG. May be NULL if the PRNG implementation
 *                              is stateless.
 * \param[in]      isDtls       TRUE for DTLS initialization, else FALSE
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_Init(
    Esc_BOOL isServer,
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    EscRandom_GetRandom prng,
    void *prngState,
    Esc_BOOL isDtls);

#if (EscTls_KEY_EXCHANGE_CERT_BASED)

/**
 * Sets list of trusted certificates
 *
 * \param[in, out] ctx                  The TLS core handshake context.
 * \param[in]      trustedRootCert      List of trusted X.509 certificates
 * \param[in]      numTrustedRootCert   Number of trusted certificates in the list
 */
void
EscTlsCore_SetTrustedCertificates(
    EscTlsCore_HandshakeContext *ctx,
    EscX509_CertificateT trustedRootCert[],
    Esc_UINT16 numTrustedRootCert);

/**
 * Sets certificate chain for authentication
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[in]       certificateChain    List of certificates in the certificate chain (subject certificate first)
 * \param[in]       numCertificates     Number of certificates in the chain
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_SetCertificateChain(
    EscTlsCore_HandshakeContext *ctx,
    const EscTls_CertificateT certificateChain[],
    Esc_UINT16 numCertificates);

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
/**
 * Sets private key for client authentication. The key must be the private key corresponding
 * to the public key contained in the lowest certificate set with
 * EscTlsCore_SetCertificateChain().
 *
 * \param[in, out]  ctx         The TLS core handshake context.
 * \param[in]      privateKey   Pointer to private RSA key
 */
void
EscTlsCore_SetRsaPrivateKey(
    EscTlsCore_HandshakeContext *ctx,
    const EscRsa_PrivKeyT *privateKey);
#endif

#if EscTls_KEY_EXCHANGE_USES_ECC
/**
 * Sets the ECC private key for authentication. This key MUST be the private key
 * corresponding to the public key contained in the certificate set via
 * EscTls_SetCertificateChain(). The key expected here is a raw key and not encoded
 * in any way (no PEM, DER, ASN.1, PKCS#8).
 * Note that this function does not create a deep copy of the key, but only
 * stores the pointer. Therefore, the key data must remain allocated for
 * the duration of the handshake!
 *
 * \param[in,out]   handshakeCtx    The TLS handshake context.
 * \param[in]       keyData         The ECC private key.
 * \param[in]       keyDataLen      The length of the ECC key in bytes.
 * \param[in]       curveType       The elliptic curve on which the key is defined. The following values are permitted:
 *                                  - EscEcc_CURVE_SECP_192  (for NIST P-192)
 *                                  - EscEcc_CURVE_SECP_224  (for NIST P-224)
 *                                  - EscEcc_CURVE_SECP_256  (for NIST P-256)
 *                                  - EscEcc_CURVE_SECP_384  (for NIST P-384)
 *                                  - EscEcc_CURVE_SECP_521  (for NIST P-521)
 *                                  - EscEcc_CURVE_BRAINPOOL_P256  (for BrainpoolP256r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P384  (for BrainpoolP384r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P512  (for BrainpoolP512r1)
 *
 * \retval EscTlsRet_UNEXPECTED_DATA if the curve type is unknown or disabled in the configuration, or if
 *                                   the private key's length does not match the curve type.
 * \retval EscTlsRet_OK on success.
 */
EscTls_RETURN
EscTlsCore_SetEccPrivateKey(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_UINT8 *keyData,
    Esc_UINT16 keyDataLen,
    EscEcc_CurveId curveType);
#endif

/**
 * Sets the current Unix time for an upcoming session. The timestamp is used to check
 * whether any certificate is expired.
 *
 * \param[in,out]   ctx             The TLS handshake context.
 * \param[in]       gmtUnixTime     The current Unix timestamp
 */
void
EscTlsCore_SetTime(
    EscTlsCore_HandshakeContext *ctx,
    Esc_UINT32 gmtUnixTime );

/**
 * Sets a callback for additional verification of the peer's certificate.
 * A typical use-case on client side is to check the domain name of the server certificate.
 * This callback is invoked after the validation of the peer's certificate chain.
 * Setting the callback to Esc_NULL_PTR effectively disables the callback.
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       callback    The callback to be executed. For details, see tls_types.h
 * \param[in,out]   param       An arbitrary, additional parameter to be passed to the callback.
 *                              It can be NULL if it is not used.
 */
void
EscTlsCore_SetVerifyCallback(
    EscTlsCore_HandshakeContext *ctx,
    EscTls_VerifyCertCallback callback,
    void *param);

#endif

/* ------------------------------- Functions to realize the handshake -------------------------- */

/**
 * Checks if all parameters required for the given cipher suite were set by the user.
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_CheckHandshakeParameters(
    EscTlsCore_HandshakeContext *ctx);

/**
 * Processes an incoming handshake message. The handshake message must be complete for processing!
 * The internal state is modified by calling this function.
 *
 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 * \param[in]       messageContent      Content of the handshake message.
 * \param[in]       messageSize         Size of the handshake message.
 * \param[in]       messageType         Type of the handshake message (taken from the handshake header).
 * \param[out]      nextAction          Determine next handshake action (send / received / completed).
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ProcessHandshakeMessage(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    const Esc_UINT8 messageContent[],
    Esc_UINT32 messageSize,
    Esc_UINT8 messageType,
    EscTls_HandshakeAction *nextAction);

/**
 * Process the ChangeCipherSpec message. Calling this function changes the internal state and
 * changes the currently used encryption parameters for the record layer.
 *
 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 * \param[in]       inputBuffer         Pointer to the change cipher spec message
 * \param[in]       inputBufferSize     Size of the change cipher spec message
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ProcessChangeCipherSpec(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize);

/**
 * Returns flag signalizing, if the outgoing records are encrypted
 *
 * \param[in]   ctx      The TLS core session context.
 *
 * \return TRUE if records are encrypted else FALSE
 */
Esc_BOOL
EscTlsCore_OutMessageIsProtected(
    EscTlsCore_SessionContext *ctx);

/**
 * Returns flag signalizing, if the ingoing records are encrypted
 *
 * \param[in]   ctx      The TLS core session context.
 *
 * \return TRUE if records are encrypted else FALSE
 */
Esc_BOOL
EscTlsCore_InMessageIsProtected(
    EscTlsCore_SessionContext *ctx);

/**
 * Generate a handshake message for sending and change the internal state correspondingly.
 * The generated message does neither include the handshake header nor the record header!
 *
 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 * \param[in]       outputBuffer        The output buffer into which the message is to be generated.
 * \param[in]       outputBufferSize    Size of the output buffer in bytes.
 * \param[out]      messageSize         Size of the generated handshake message or handshake message fragment.
 * \param[out]      contentType         Type of the generated message (eg. Handshake or Change Cipher Spec).
 * \param[out]      handshakeType       Type of the generated handshake message (as to be put into the handshake header).
 * \param[out]      nextAction          Determine next handshake action (send / received / completed).
 * \param[out]      messageId           Pointer to the handshake message ID which will be set to the current handshake message.
 *                                      Can be NULL if not used.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_GenerateHandshakeMessage(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize,
    EscTls_ContentType *contentType,
    Esc_UINT8 *handshakeType,
    EscTls_HandshakeAction *nextAction,
    EscTls_HskId *messageId);

/**
 * DTLS specific: Generate a specific handshake message from the previous flight again for
 * retransmission. The internal state is NOT affected by calling this function.
 *
 * The idea behind this function is that the DTLS layer keeps track of which functions were
 * generated during the last flight which was sent. The DTLS layer can then re-generate the
 * corresponding messages for a re-transmission by calling this function.
 *
 * Analogously to EscTlsCore_GenerateHandshakeMessage(), the message may be incomplete.

 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 * \param[in]       messageId           ID of the message to be generated.
 * \param[in]       outputBuffer        The output buffer into which the message is to be generated.
 * \param[in]       outputBufferSize    Size of the output buffer in bytes.
 * \param[out]      messageSize         Size of the generated handshake message or handshake message fragment.
 * \param[out]      contentType         Type of the generated message (eg. Handshake or Change Cipher Spec).
 * \param[out]      handshakeType       Type of the generated handshake message (as to be put into the handshake header).
 * \param[out]      nextAction          Determine next handshake action (send / received / completed).
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_RegenerateHandshakeMessage(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    EscTls_HskId messageId,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize,
    EscTls_ContentType *contentType,
    Esc_UINT8 *handshakeType,
    EscTls_HandshakeAction *nextAction);

/**
 * The hash over the handshake messages (to compute verify_data, cf. RFC 5246) is
 * maintained by the core. However, updating the hash is triggered externally by
 * (d)tls_internal. The hash state is initialized at different times for TLS and DTLS
 * (for TLS all messages are hashed, but for DTLS the first ClientHello and HelloVerifyRequest
 * are not hashed). This function returns whether the hash state is initialized and
 * therefore, handshake messages must be hashed (by calling EscTlsCore_FinishedDigestUpdate()).
 *
 * \param[in]   ctx      The TLS core handshake context.
 *
 * \return TRUE if handshake messages must be hashed, FALSE otherwise.
 */
Esc_BOOL
EscTlsCore_HandshakeMessageMustBeHashed(
    const EscTlsCore_HandshakeContext *ctx);

/**
 * Update the hash digest over all handshake messages (used in the Finished message).
 * Note that this step is NOT done by other message-processing functions,
 * but must be called explicitly.
 *
 * The handshake messages must be passed to this function as they appear in the
 * protocol. This included both sent and received handshake messages. Note that
 * ChangeCipherSpec is NOT a handshake message. The handshake header must be included.
 *
 * \param[in]   ctx                 The TLS core handshake context.
 * \param[in]   content             Buffer with data to be hashed.
 * \param[in]   contentSize         Size of the buffer in bytes.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_FinishedDigestUpdate(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_UINT8 *content,
    Esc_UINT32 contentSize);

/* ------------------------------- Functions to realize protection in the record layer---------- */

/**
 * Get the maximum number of extra bytes that will be added to the protected
 * record (e.g. the computed MAC tag and the padding for the block cipher).
 * The function can be used to check if the output buffer for encoding a record
 * is large enough.
 * The record header is NOT included in this computation!
 *
 * Note: This function MUST not be called before ChangeCipherSpec was generated!
 *
 * \param[in]   ctx      The TLS core session context.
 *
 * \return  Number of extra bytes.
 */
Esc_UINT16
EscTlsCore_GetMaxProtectionOverhead(
    EscTlsCore_SessionContext *ctx);

/**
 * MAC and encrypt a record.
 * This function MUST not be called before ChangeCipherSpec was generated!
 * The size of the buffers is NOT checked inside this function.
 *
 * Note: Input buffer and output buffer may be the same (in-place processing)!
 *
 * \param[in]   ctx                 The TLS core session context.
 * \param[in]   content             Data to be encoded.
 * \param[in]   contentLength       Length of the data in bytes (as used in the record header).
 * \param[in]   contentType         Type of the message (as used in the record header).
 * \param[in]   sequenceNumber      The 64-bit sequence number as byte array.
 * \param[out]  outputBuffer        Output buffer to protected fragment.
 * \param[out]  recordSize          Size of the encoded record (excluding the record header).
 *                                  This value determines how many bytes were written into the
 *                                  output buffer.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_MacEncryptRecord(
    EscTlsCore_SessionContext *ctx,
    const Esc_UINT8 *content,
    Esc_UINT16 contentLength,
    EscTls_ContentType contentType,
    const Esc_UINT8 sequenceNumber[],
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 *recordSize);

/**
 * Decrypt record and verify its MAC.
 * This function MUST not be called before ChangeCipherSpec was generated!
 * The size of the buffers is NOT checked inside this function.
 *
 * Note: Input buffer and output buffer may be the same (in-place processing)!
 *
 * \param[in]   ctx                 The TLS core session context.
 * \param[in]   recordData          Data to be decoded.
 * \param[in]   recordDataLength    Length of the data in bytes (as used in the record header).
 * \param[in]   contentType         Type of the message (as used in the record header).
 * \param[in]   sequenceNumber      The 64-bit sequence number as byte array.
 * \param[out]  outputBuffer        Buffer to store the decoded message.
 * \param[in]   outputBufferSize    Size of output buffer in bytes.
 * \param[out]  dataSize            Size of the decoded record (excluding the record header).
 *                                  This value determines how many bytes were written into the
 *                                  output buffer.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_DecryptVerifyRecord(
    EscTlsCore_SessionContext *ctx,
    const Esc_UINT8 *recordData,
    Esc_UINT16 recordDataLength,
    EscTls_ContentType contentType,
    const Esc_UINT8 sequenceNumber[],
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *dataSize);

/**
 * Store the error code in the context such that it can be requested by Esc(D)TlsInt_GetLastError().
 * Furthermore, the function maps the error code to one of the following error codes:
 *
 * EscTlsRet_OK
 * EscTlsRet_WARNING_DETECTED
 * EscTlsRet_CLOSED_BY_PEER
 * EscTlsRet_ERROR_DETECTED
 * EscTlsRet_FATAL_ALERT_RECEIVED
 * EscTlsRet_IGNORED
 *
 * \param[in]   ctx             The TLS core session context.
 * \param[in]   returnCode      The return code to be stored/translated.
 *
 * \return One of the above return codes.
 */
EscTls_RETURN
EscTlsCore_SetError(
    EscTlsCore_SessionContext *ctx,
    EscTls_RETURN returnCode);

/**
 * Process one or more alert messages. The function will stop processing on the first
 * fatal alert found. Alerts of level warning are currently ignored.
 *
 * \param[in, out]  ctx                 The TLS core session context.
 * \param[in]       inputBuffer         Buffer containing the alert messages
 *                                      (i.e. the data in the record of content type alert)
 * \param[in]       inputBufferSize     Size of the buffer
 *
 * \return An error code that corresponds to the first fatal error encountered
 *         or EscTlsRet_OK in case the buffer contained only warnings.
 */
EscTls_RETURN
EscTlsCore_ProcessAlert(
    EscTlsCore_SessionContext *ctx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize );

/**
 * Parses a TLS return code to an alert
 *
 * \param[in, out]  sessionCtx  The TLS core session context.
 *
 * \return Alert with appropriate alert level and description
 */
EscTls_AlertT
EscTlsCore_ParseErrorToAlert(
    EscTlsCore_SessionContext *sessionCtx);

/**
 * Get the connection's current top level state.
 *
 * \param[in]   ctx     The TLS core session context.
 *
 * \retval  EscTls_STATE_HANDSHAKE              The handshake is in progress.
 * \retval  EscTls_STATE_CONNECTED              The connection is established. Application data can be sent/received.
 * \retval  EscTls_STATE_CLOSED                 The connection was closed locally or by the host.
 *                                              Note that this only refers to the TLS layer, but has
 *                                              no meaning for the underlying transport layer.
 * \retval  EscTls_STATE_ERROR                  An error occurred during the session and the session cannot be used
 *                                              any further.
 * \retval  EscTls_STATE_LOCAL_ERROR            A local error occurred. Fatal alert must be sent.
 */
EscTls_ConnectionState
EscTlsCore_GetConnectionTopLevelState(
    const EscTlsCore_SessionContext *ctx );


/**
 * Get the connection's current state.
 *
 * \param[in]   ctx     The TLS core session context.
 *
 * \return  The current conenction state
 */
EscTls_ConnectionState
EscTlsCore_GetConnectionState(
    const EscTlsCore_SessionContext *ctx );


/**
 * Set the connection's current state (top and sub level state).
 * This function must be used with care and should only be used for setting error code and not for restoring a closed connection.
 *
 * \param[in]   ctx     The TLS session context.
 * \param[in]   state   The new connection state.
 *
 */
void
EscTlsCore_SetConnectionState(
    EscTlsCore_SessionContext *ctx,
    EscTls_ConnectionState state );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CORE_H_ */
