/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Constants of the TLS protocol (e.g. algorithm IDs)

$Rev: 481 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CONSTANTS_H_
#define ESC_TLS_CONSTANTS_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_cycurlib.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/* Possible content of a TLS record */

/** Change cipher spec content identifier */
#define EscTls_CONTENT_CHANGE_CIPHER_SPEC   20U
/** Alert content identifier*/
#define EscTls_CONTENT_ALERT                21U
/** Handshake content identifier */
#define EscTls_CONTENT_HANDSH               22U
/** Application content identifier  */
#define EscTls_CONTENT_APPLICATION          23U

/* Possible content of a handshake message */

/** Hello Request identifier */
#define EscTls_HANDSH_TYPE_HELLO_REQUEST        0U
/** Client Hello identifier */
#define EscTls_HANDSH_TYPE_CLIENT_HELLO         1U
/** Server Hello identifier */
#define EscTls_HANDSH_TYPE_SRVR_HELLO           2U
/** HelloVerifyRequest identifier (DTLS only) */
#define EscTls_HANDSH_TYPE_HELLO_VERIFY_REQUEST 3U
/** Certificate identifier */
#define EscTls_HANDSH_TYPE_CERT                 11U
/** Server Key Exchange identifier */
#define EscTls_HANDSH_TYPE_SRVR_KEY_EXCHANGE    12U
/** Certificate Request identifier */
#define EscTls_HANDSH_TYPE_CERT_REQUEST         13U
/** Server Hello Done identifier */
#define EscTls_HANDSH_TYPE_SRVR_HELLO_DONE      14U
/** Certificate Verify identifier */
#define EscTls_HANDSH_TYPE_CERT_VERIFY          15U
/** Client Key Exchange identifier */
#define EscTls_HANDSH_TYPE_CLIENT_KEY_EXCHANGE  16U
/** Finished identifier */
#define EscTls_HANDSH_TYPE_FINISHED             20U

/* TLS protocol versions */

/** SSL 3.0 */
#define EscTls_VERSION_SSL_3_0              0x0300U
/** TLS 1.0 */
#define EscTls_VERSION_TLS_1_0              0x0301U
/** TLS 1.1 */
#define EscTls_VERSION_TLS_1_1              0x0302U
/** TLS 1.2 */
#define EscTls_VERSION_TLS_1_2              0x0303U

/* DTLS protocol versions */

/** DTLS 1.0 */
#define EscTls_VERSION_DTLS_1_0             0xFEFFU
/** DTLS 1.2 */
#define EscTls_VERSION_DTLS_1_2             0xFEFDU


/* Fixed Sizes */
/** Size of the Record Header */
#define EscTls_RECORD_HEADER_SIZE           5U
/** Size of the Handshake Header */
#define EscTls_HANDSH_HEADER_SIZE           4U
/** Size of the protocol version */
#define EscTls_VERSION_SIZE                 2U
/** Length of the Random values */
#define EscTls_RANDOM_LENGTH                32U
/** Length of the Master Secret */
#define EscTls_MASTER_SECRET_LENGTH         48U
/** Length of the Pre Master Secret */
#define EscTls_PRE_MASTER_SECRET_LENGTH_RSA 48U
/** Size of an Alert message in Byte */
#define EscTls_ALERT_SIZE  2U
/** Size of an ChangeCipherSpec message in Byte */
#define EscTls_CHANGECIPHERSPEC_SIZE        1U
/** Standard max fragment size */
#define EscTls_FRAGMENT_SIZE_MAX            16384U /* 2^14 */
/** Fragment size plus allowed encryption Overhead */
#define EscTls_RECORD_LENGTH_MAX            (EscTls_FRAGMENT_SIZE_MAX + 2048U)
/** Maximum Session ID length */
#define EscTls_SESSION_ID_MAX_LENGTH        32U

/* Fixed Values */
/** Value for the ChangeCipherSpec packet */
#define EscTls_CHANGECIPHERSPEC_VALUE       1U

/** Compression method identifier for no compression */
#define EscTls_COMPRESSION_METHOD_IDENTIFIER_NULL   0x00U

/* Extension types */
/** signature_algorithms extension */
#define EscTls_EXT_SIGNATURE_ALGORITHMS     0x000DU
/** heartbeat extension */
#define EscTls_EXT_HEARTBEAT                0x000FU
/** elliptic_curves extension */
#define EscTls_EXT_ELLIPTIC_CURVES          0x000AU
/** ec_point_formats extension */
#define EscTls_EXT_EC_POINT_FORMATS         0x000BU

/* Heartbeat modes */
/** Peer allowed to send heartbeats (not supported) */
#define EscTls_HEARTBEAT_MODE_PEER_ALLOWED_TO_SEND      0x01U
/** Peer not allowed to send heartbeats */
#define EscTls_HEARTBEAT_MODE_PEER_NOT_ALLOWED_TO_SEND  0x02U

/* Elliptic curve IDs */
/** Elliptic curve ID for secp192r1 */
#define EscTls_NAMED_CURVE_SECP192R1        19U
/** Elliptic curve ID for secp224r1 */
#define EscTls_NAMED_CURVE_SECP224R1        21U
/** Elliptic curve ID for secp256r1 */
#define EscTls_NAMED_CURVE_SECP256R1        23U
/** Elliptic curve ID for secp384r1 */
#define EscTls_NAMED_CURVE_SECP384R1        24U
/** Elliptic curve ID for secp521r1 */
#define EscTls_NAMED_CURVE_SECP521R1        25U
/** Elliptic curve ID for brainpoolP256r1 */
#define EscTls_NAMED_CURVE_BRAINPOOLP256R1  26U
/** Elliptic curve ID for brainpoolP384r1 */
#define EscTls_NAMED_CURVE_BRAINPOOLP384R1  27U
/** Elliptic curve ID for brainpoolP512r1 */
#define EscTls_NAMED_CURVE_BRAINPOOLP512R1  28U


/* ECC point formats */
/** Uncompressed ECC points */
#define EscTls_EC_POINT_FORMAT_UNCOMPRESSED     0x00U

/* ECC curve types */
#define EscTls_EC_CURVE_TYPE_NAMED_CURVE        0x03U

/* Alert Level */

/** Warning occurred */
#define EscTls_ALRT_LVL_WARNING         1U
/** Fatal error occurred */
#define EscTls_ALRT_LVL_FATAL           2U

/* Alert Descriptions */
#define EscTls_ALRT_DESCRP_CLOSE_NOTIFY               0U /*!< Close notify alert */
#define EscTls_ALRT_DESCRP_UNEXPECTED_MESSAGE        10U /*!< Unexpected message alert */
#define EscTls_ALRT_DESCRP_BAD_RECORD_MAC            20U /*!< Bad record MAC alert */
#define EscTls_ALRT_DESCRP_RECORD_OVERFLOW           22U /*!< Record overflow alert */
#define EscTls_ALRT_DESCRP_DECOMPRESSION_FAILURE     30U /*!< Decompression alert */
#define EscTls_ALRT_DESCRP_HANDSHAKE_FAILURE         40U /*!< Handshake failure alert */
#define EscTls_ALRT_DESCRP_BAD_CERTIFICATE           42U /*!< Bad certificate alert */
#define EscTls_ALRT_DESCRP_UNSUP_CERTIFICATE         43U /*!< Unsupported certificate alert */
#define EscTls_ALRT_DESCRP_CERT_REVOKED              44U /*!< Certificate revoked alert */
#define EscTls_ALRT_DESCRP_CERT_EXPIRED              45U /*!< Certificate expired alert */
#define EscTls_ALRT_DESCRP_CERT_UNKNOWN              46U /*!< Certificate unknown alert */
#define EscTls_ALRT_DESCRP_ILLEGAL_PARAMETER         47U /*!< Illegal parameter alert */
#define EscTls_ALRT_DESCRP_UNKNOWN_CA                48U /*!< Unknown CA alert */
#define EscTls_ALRT_DESCRP_ACCESS_DENIED             49U /*!< Access denied alert */
#define EscTls_ALRT_DESCRP_DECODE_ERROR              50U /*!< Decode error alert */
#define EscTls_ALRT_DESCRP_DECRYPT_ERROR             51U /*!< Decrypt error alert */
#define EscTls_ALRT_DESCRP_PROTOCOL_VERSION          70U /*!< Protocol version alert */
#define EscTls_ALRT_DESCRP_INSUFFICIENT_SECURITY     71U /*!< Insufficient security alert */
#define EscTls_ALRT_DESCRP_INTERNAL_ERROR            80U /*!< Internal error alert */
#define EscTls_ALRT_DESCRP_USER_CANCELED             90U /*!< User canceled alert */
#define EscTls_ALRT_DESCRP_NO_RENEGOTIATION         100U /*!< No renegotiation alert */
#define EscTls_ALRT_DESCRP_UNSUP_EXTENSION          110U /*!< Unsupported extension alert */
#define EscTls_ALRT_DESCRP_UNKNOWN_PSK_IDENTITY     115U /*!< Unknown identity in PSK key exchange alert */

#define EscTls_CERT_TYPE_RSA_SIGN           1U /*!< Certificate containing an RSA key */
#define EscTls_CERT_TYPE_DSS_SIGN           2U /*!< Certificate containing a DSA key */
#define EscTls_CERT_TYPE_RSA_FIXED_DH       3U /*!< Certificate containing a static DH key */
#define EscTls_CERT_TYPE_DSS_FIXED_DH       4U /*!< Certificate containing a static DH key */
#define EscTls_CERT_TYPE_ECDSA_SIGN        64U /*!< Certificate containing an ECDSA key */

#define EscTls_SIGN_ALG_ANON    0U /*!< No signing algorithm identifier for certificates */
#define EscTls_SIGN_ALG_RSA     1U /*!< RSA signing algorithm identifier for certificates */
#define EscTls_SIGN_ALG_DSA     2U /*!< DSA signing algorithm identifier for certificates */
#define EscTls_SIGN_ALG_ECDSA   3U /*!< ECDSA signing algorithm identifier for certificates */

#define EscTls_HASH_ALG_NONE        0U /*!< No hash algorithm for certificates */
#define EscTls_HASH_ALG_MD5         1U /*!< MD5 hash for certificates */
#define EscTls_HASH_ALG_SHA         2U /*!< SHA hash for certificates */
#define EscTls_HASH_ALG_SHA_224     3U /*!< SHA 224 hash for certificates */
#define EscTls_HASH_ALG_SHA_256     4U /*!< SHA 256 hash for certificates */
#define EscTls_HASH_ALG_SHA_384     5U /*!< SHA 384 hash for certificates */
#define EscTls_HASH_ALG_SHA_512     6U /*!< SHA 512 hash for certificates */

/** String length of "key expansion" */
#define EscTls_KEY_EXPANSION_LABEL_LENGTH   13U
/** String length of "client finished" */
#define EscTls_CLIENT_FINISHED_LABEL_LENGTH 15U
/** String length "server finished" */
#define EscTls_SRVR_FINISHED_LABEL_LENGTH   15U
/** String length of "master secret" */
#define EscTls_MASTER_SECRET_LABEL_LENGTH   13U

/***************************************************************************
 * 5. CONSTANTS                                                            *
 ***************************************************************************/

/** Expansion label for the key computation. */
extern const Esc_CHAR EscTls_keyExpansionLabel[];
/** Finished label for the client finish. */
extern const Esc_CHAR EscTls_clientFinishedLabel[];
/** Finished label for the server finish. */
extern const Esc_CHAR EscTls_serverFinishedLabel[];
/** Label for the key derivation of the pre-master secret */
extern const Esc_CHAR EscTls_masterSecretLabel[];

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CONSTANTS_H_ */
