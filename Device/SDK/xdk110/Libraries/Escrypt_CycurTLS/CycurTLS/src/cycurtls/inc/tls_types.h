/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Types to be used with the TLS implementation.

 $Rev: 572 $
 */
/***************************************************************************/

#ifndef ESC_TLS_TYPES_H_
#define ESC_TLS_TYPES_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_cycurlib.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

#ifndef Esc_NULL_PTR
/** The null Pointer */
#   define Esc_NULL_PTR ( void * ) 0
#endif

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** Certificate structure */
typedef struct
{
    /** Pointer to the certificate */
    const Esc_UINT8 *certificate;
    /** Length of the certificate in byte */
    Esc_UINT32 certificateLength;
} EscTls_CertificateT;

/**
 * Callback function type for selecting a client identity and a
 * pre-shared key given an (optional) server identity hint.
 *
 * \param[in]       pskCtx                  The same pointer that was passed to EscTls_SetClientPskCallback()
 * \param[in]       serverIdHint            The identity hint provided by the server. The hint is optional
 *                                          and therefore, this pointer may be NULL.
 * \param[in]       serverIdHintLen         The length of the identity hint.
 * \param[out]      clientIdentity          Buffer to store the identity to be used for this connection.
 * \param[in, out]  clientIdentityLen       IN: The buffer size of clientIdentity.
 *                                          OUT: The actual length of the client identity written to the buffer.
 * \param[out]      psk                     Buffer to store the pre-shared key.
 * \param[in]       pskLenMax               The maximum number of bytes to be written to psk.
 *
 * \return On success, the number of bytes written to "psk" is returned. On failure, zero is returned.
 */
typedef Esc_UINT16
(*EscTls_PskClientCallback)(
    void *pskCtx,
    const Esc_CHAR *serverIdHint,
    Esc_UINT16 serverIdHintLen,
    Esc_CHAR *clientIdentity,
    Esc_UINT16 *clientIdentityLen,
    Esc_UINT8 *psk,
    Esc_UINT16 pskLenMax);

/**
 * Callback function type for selecting getting the pre-shared key, given an (optional) client identity.
 *
 * \param[in]   pskCtx                  The same pointer that was passed to EscTls_SetClientPskCallback()
 * \param[in]   clientIdentity          The identity provided by the client. The identity is optional
 *                                      and therefore, this pointer may be NULL.
 * \param[in]   clientIdentityLen       The length of the client identity.
 * \param[out]  psk                     Buffer to store the pre-shared key.
 * \param[in]   pskLenMax               The maximum number of bytes to be written to psk.
 *
 * \return On success, the number of bytes written to "psk" is returned.
 *         On failure, zero is returned which is assumed to be an "unknown_psk_identity" error.
 */
typedef Esc_UINT16
(*EscTls_PskServerCallback)(
    void *pskCtx,
    const Esc_CHAR *clientIdentity,
    Esc_UINT16 clientIdentityLen,
    Esc_UINT8 *psk,
    Esc_UINT16 pskLenMax);

#if (EscTls_KEY_EXCHANGE_CERT_BASED)
/**
 * Callback function type for additional peer certificate checks.
 * A typical use-case on client side is to check the domain name of the server certificate.
 * This callback is invoked after the validation of the peer's certificate chain.
 *
 * \param[in]       cert        The peer's certificate.
 * \param[in, out]  param       An additional parameter that can be used by
 *                              the certification callback.
 *
 * \retval TRUE if the verification was successful. The handshake will continue.
 * \retval FALSE if the verification failed. The handshake is aborted with a bad_certificate alert.
 */
typedef Esc_BOOL
(*EscTls_VerifyCertCallback)(
    const EscX509_CertificateT *cert,
    void *param);

#endif

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_TYPES_H_ */
