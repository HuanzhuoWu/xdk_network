/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       TLS low-level API and record fragmentation module.

 $Rev: 580 $
 */
/***************************************************************************/

#ifndef ESC_TLS_INTERNAL_H_
#define ESC_TLS_INTERNAL_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_core.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** The 64-bit sequence number as two UINT32 values */
typedef struct
{
    /** Most significant bytes */
    Esc_UINT32 highNibble;

    /** Least significant bytes */
    Esc_UINT32 lowNibble;
} EscTls_SequenceNumber;

/** State of the TLS session */
typedef struct
{
    /** The actual TLS context. Must be the first member. */
    EscTlsCore_SessionContext coreCtx;

    /** Sequence number for sending records */
    EscTls_SequenceNumber sendSeqNumber;

    /** Sequence number for receiving records */
    EscTls_SequenceNumber receiveSeqNumber;
} EscTlsInt_SessionContext;

/** State of the TLS handshake */
typedef struct
{
    /** The actual TLS state */
    EscTlsCore_HandshakeContext coreCtx;

    /** Internal buffer for intermediate results */
    Esc_UINT8 *internalBuffer;
    /** Size of the internal buffer */
    Esc_UINT16 internalBufferSize;
    /** Used buffer */
    Esc_UINT16 usedInternalBuffer;

    /** Next action to be executed in the handshake: send message, receive message or handshake complete. */
    EscTls_HandshakeAction nextAction;
} EscTlsInt_HandshakeContext;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/
/**
 * Initializes a TLS context and resets the session. Must be called before a new
 * handshake will be performed.
 *
 * \param[in]       isServer            If TRUE, the session is a server side session, otherwise
 *                                      it is the client side.
 * \param[in,out]   sessionCtx          The TLS session context.
 * \param[in,out]   handshakeCtx        The TLS handshake context.
 * \param[in]       internalBuffer      Buffer for internal storage. Must be at least the size of
 *                                      the expected certificate chain.
 * \param[in]       internalBufferSize  Size of the internal buffer.
 * \param[in]       prng                The pseudo-random number generator (PRNG) to be used
 *                                      to provide random numbers for TLS. See random.h.
 * \param[in]       prngState           The state of the PRNG. May be NULL if the PRNG implementation
 *                                      is stateless.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_Init(
    Esc_BOOL isServer,
    EscTlsInt_SessionContext *sessionCtx,
    EscTlsInt_HandshakeContext *handshakeCtx,
    Esc_UINT8 *internalBuffer,
    Esc_UINT16 internalBufferSize,
    EscRandom_GetRandom prng,
    void *prngState);

#if (EscTls_KEY_EXCHANGE_CERT_BASED)

/**
 * Sets the current Unix time for an upcoming session. The timestamp is used to check
 * whether the peer's certificate is expired.
 *
 * \param[in,out]   ctx             The TLS handshake context.
 * \param[in]       gmtUnixTime     The current Unix timestamp
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetTime(
    EscTlsInt_HandshakeContext *ctx,
    Esc_UINT32 gmtUnixTime );

/**
 * Sets a callback for additional verification of the peer's certificate.
 * A typical use-case on client side is to check the domain name of the server certificate.
 * This callback is invoked after the validation of the peer's certificate chain.
 * Setting the callback to Esc_NULL_PTR effectively disables the callback.
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       callback    The callback to be executed. For details, see tls_types.h
 * \param[in,out]   param       An arbitrary, additional parameter to be passed to the callback.
 *                              It can be NULL if it is not used.
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetVerifyCallback(
    EscTlsInt_HandshakeContext *ctx,
    EscTls_VerifyCertCallback callback,
    void *param);


/**
 * Sets trusted certificates.
 * X509 Certificate array must remain allocated during the complete handshake.
 *
 * \param[in,out]   ctx                 The TLS handshake context.
 * \param[in]       trustedRootCert     List of trusted root certificates.
 *                                      Must remain allocated during the handshake.
 * \param[in]       numTrustedRootCert  Number of trusted root certificates.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetTrustedCertificates(
    EscTlsInt_HandshakeContext *ctx,
    EscX509_CertificateT trustedRootCert[],
    Esc_UINT16 numTrustedRootCert);

/**
 * Sets certificate chain for authentication.
 * Certificate array must remain allocated during the complete handshake.
 *
 * \param[in,out]   ctx                 The TLS handshake context.
 * \param[in]       certificateChain    List of certificates in the certificate chain,
 *                                      lowest certificate first. Must remain allocated during the handshake.
 * \param[in]       numCertificates     Number of certificates in the chain
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetCertificateChain(
    EscTlsInt_HandshakeContext *ctx,
    const EscTls_CertificateT certificateChain[],
    Esc_UINT16 numCertificates);

#if (EscTls_HAS_SERVER)
/**
 * Enable or disable client authentication. This function is only useful on the
 * server side. By default, no client authentication is used.
 *
 * \param[in,out]  ctx                  The TLS handshake context.
 * \param[in]      clientAuthEnabled    Flag to enable (TRUE) or disable (FALSE) client authentication.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetClientAuth(
    EscTlsInt_HandshakeContext *ctx,
    Esc_BOOL clientAuthEnabled);
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
/**
 * Sets the private key for authentication.
 * Private key must remain allocated during the complete handshake.
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       privateKey  Pointer to the RSA private key. Must remain allocated during the handshake.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetRsaPrivateKey(
    EscTlsInt_HandshakeContext *ctx,
    const EscRsa_PrivKeyT *privateKey);
#endif

#if EscTls_KEY_EXCHANGE_USES_ECC
/**
 * Sets the ECC private key for authentication. This key MUST be the private key
 * corresponding to the public key contained in the certificate set via
 * EscTlsInt_SetCertificateChain(). The key expected here is a raw key and not encoded
 * in any way (no PEM, DER, ASN.1, PKCS#8).
 * Note that this function does not create a deep copy of the key, but only
 * stores the pointer. Therefore, the key data must remain allocated for
 * the duration of the handshake!
 *
 * \param[in,out]   ctx             The TLS handshake context.
 * \param[in]       keyData         The ECC private key.
 * \param[in]       keyDataLen      The length of the ECC key in bytes.
 * \param[in]       curveType       The elliptic curve on which the key is defined. The following values are permitted:
 *                                  - EscEcc_CURVE_SECP_192  (for NIST P-192)
 *                                  - EscEcc_CURVE_SECP_224  (for NIST P-224)
 *                                  - EscEcc_CURVE_SECP_256  (for NIST P-256)
 *                                  - EscEcc_CURVE_SECP_384  (for NIST P-384)
 *                                  - EscEcc_CURVE_SECP_521  (for NIST P-521)
 *                                  - EscEcc_CURVE_BRAINPOOL_P256  (for BrainpoolP256r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P384  (for BrainpoolP384r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P512  (for BrainpoolP512r1)
 *
 * \retval EscTlsRet_UNEXPECTED_DATA if the curve type is unknown or disabled in the configuration, or if
 *                                   the private key's length does not match the curve type.
 * \retval EscTlsRet_OK on success.
 */
EscTls_RETURN
EscTlsInt_SetEccPrivateKey(
    EscTlsInt_HandshakeContext *ctx,
    const Esc_UINT8 *keyData,
    Esc_UINT16 keyDataLen,
    EscEcc_CurveId curveType);
#endif

#endif /* EscTls_KEY_EXCHANGE_CERT_BASED */

#if EscTls_KEY_EXCHANGE_IS_PSK_BASED

#   if EscTls_HAS_CLIENT
/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       pskCtx      Pointer to user defined context for key agreement (may be NULL)
 * \param[in]       pskCallback Callback function for generating a pre-shared
 *                              key and a client identity given a server hint
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetClientPskCallback(
    EscTlsInt_HandshakeContext *ctx,
    void *pskCtx,
    EscTls_PskClientCallback pskCallback);
#   endif

#   if EscTls_HAS_SERVER
/**
 * Sets the server identity hint which is sent to the client during the handshake.
 * Identity hint buffer must remain allocated during the complete handshake.
 * If not set or set to zero length, the ServerKeyExchange message during the handshake is omitted.
 *
 * \param[in,out]   ctx                     The TLS handshake context.
 * \param[in]       serverIdentityHint      Pointer to the server identity hint.
 *                                          Must remain allocated during the handshake.
 * \param[in]       serverIdentityHintLen   Length of the server identity.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetServerIdentityHint(
    EscTlsInt_HandshakeContext *ctx,
    const Esc_CHAR *serverIdentityHint,
    Esc_UINT16 serverIdentityHintLen );

/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in,out]   ctx         The TLS handshake context.
 * \param[in]       pskCtx      Pointer to user defined context for key agreement (may be NULL)
 * \param[in]       pskCallback Callback function to select (or derive) a pre-shared key given a client identity
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetServerPskCallback(
    EscTlsInt_HandshakeContext *ctx,
    void *pskCtx,
    EscTls_PskServerCallback pskCallback);
#   endif
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
#   if EscTls_HAS_SERVER
/**
 * Sets the Diffie-Hellman parameters for the key exchange.
 * Parameters must fit to the configured options inside Diffie-Hellman module.
 * Parameter buffers must remain allocated during the complete handshake.
 *
 * \param[in,out]   ctx                 The TLS handshake context.
 * \param[in]       dhModulus           Pointer to the DH modulus. Must remain allocated during the handshake.
 * \param[in]       dhModulusLength     Length of the DH modulus.
 * \param[in]       dhGenerator         Pointer to the DH generator. Must remain allocated during the handshake.
 * \param[in]       dhGeneratorLength   Length of the DH generator.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsInt_SetDhParams(
    EscTlsInt_HandshakeContext *ctx,
    const Esc_UINT8 *dhModulus,
    Esc_UINT16 dhModulusLength,
    const Esc_UINT8 *dhGenerator,
    Esc_UINT16 dhGeneratorLength );
#   endif
#endif

/* ------------------------------- Functions for executing the handshake -------------------------- */

/**
 * This function should be called before starting the handshake. It verifies that all
 * required parameters were set for the configured cipher suite.
 *
 * \param[in] handshakeCtx  The fully initialized handshake context.
 *
 * \return EscTlsRet_OK                         All parameters were set correctly. The handshake can be started.
 * \return EscTlsRet_UNINITIALIZED_PARAMETER    One or more required parameters were not set.
 */
EscTls_RETURN
EscTlsInt_CheckHandshakeParameters(
    EscTlsInt_HandshakeContext *handshakeCtx);

/**
 * Process a previously received network packet containing exactly one record with one or
 * more handshake fragments. The function decodes the record contained
 * inside and stores them into the internal buffer (whose address and size
 * is stored in the context).
 *
 * This function must be called initially by the server and whenever
 * the implementation expects more data from the peer (as indicated
 * by next handshake action EscTls_HSACTION_RECV_DATA).
 *
 * \param[in,out]   sessionCtx      The TLS session context.
 * \param[in,out]   handshakeCtx    The TLS handshake context.
 * \param[in]       inputBuffer     Input buffer containing the data to be decoded.
 *                                  Must be exactly one complete record.
 * \param[in]       packetSize      Size of the network packet in bytes.
 * \param[out]      nextAction      Next handshake action (send/receive/completed).
 *                                  This parameter is only valid if the function returns
 *                                  with EscTlsRet_OK or EscTlsRet_WARNING_DETECTED!
 *
 * \retval EscTlsRet_OK                     On success.
 * \retval EscTlsRet_CLOSED_BY_PEER         The peer has shut down the connection. In this case,
 *                                          a close_notify alert must be sent (\see EscTlsInt_Shutdown())
 *                                          and the connection must be closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED   The peer has sent a fatal alert. The connection must be closed
 *                                          immediately in this case. A detailed error code can be obtained
 *                                          by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_ERROR_DETECTED         A fatal error occurred. An alert with fatal severity must be
 *                                          generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                          The session is closed afterwards. A detailed error code can be
 *                                          obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_HandshakeReceived(
    EscTlsInt_SessionContext *sessionCtx,
    EscTlsInt_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 packetSize,
    EscTls_HandshakeAction *nextAction);

/**
 * Generates a handshake packet to be sent to the peer. The packet may contain
 * one or more handshake messages/fragments.
 *
 * This function must be called initially by the client and whenever
 * the implementation needs to send more data to the peer as indicated
 * by next handshake action EscTls_HSACTION_SEND_DATA.
 *
 * \param[in,out]   sessionCtx          The TLS session context.
 * \param[in,out]   handshakeCtx        The TLS handshake context.
 * \param[in]       outputBuffer        The buffer to store the network packet.
 * \param[in]       outputBufferSize    Size of the buffer in bytes.
 * \param[out]      packetSize          Number of bytes stored in the buffer (size of the packet).
 * \param[out]      nextAction          Next handshake action (send/receive/completed).
 *                                      This parameter is only valid if the function returns with
 *                                      EscTlsRet_OK or EscTlsRet_WARNING_DETECTED!
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be generated
 *                                     and sent to the peer (\see EscTlsInt_BuildAlert()). The session is closed afterwards.
 *                                     A detailed error code can be obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_HandshakeToBeSent(
    EscTlsInt_SessionContext *sessionCtx,
    EscTlsInt_HandshakeContext *handshakeCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *packetSize,
    EscTls_HandshakeAction *nextAction);

/**
 * Get the record size of a incoming message and do some basic integrity checks of the header.
 *
 * \param[in]   sessionCtx  The TLS session context.
 * \param[in]   input       The first 5 Bytes (Record Header) of a message.
 * \param[out]  recordSize  Size of the record. Only valid if EscTlsRet_OK is returned!
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity
 *                                     must be generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_GetRecordSize(
    EscTlsInt_SessionContext *sessionCtx,
    const Esc_UINT8 input[5U],
    Esc_UINT16 *recordSize );

/* ------------------------ Functions for processing application data ------------------------ */

/**
 * Get the maximum number of extra bytes that will be added to the application data
 * due to the protection (i.e. encryption and MAC). This function can be used
 * to calculate how large the output buffer for EscTlsInt_AppDataToBeSent() must be.
 * The handshake must have been finished before calling this function!
 *
 * \param[in]   sessionCtx      The TLS session context.
 * \param[out]  overheadSize    Number of extra bytes to be added for protection of application data.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity
 *                                     must be generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be obtained
 *                                     by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_GetMaxProtectionOverhead(
    EscTlsInt_SessionContext *sessionCtx,
    Esc_UINT16 *overheadSize );

/**
 * Decodes application data received via the network. Must be one complete record!
 * The data is decrypted and headers are removed. The handshake must have been
 * finished before calling this function!
 *
 * Input buffer and output buffer may be the same (in-place processing).
 *
 * \param[in]   sessionCtx          The TLS session context.
 * \param[in]   inputBuffer         The received input data to be decoded.
 * \param[in]   packetSize          Size of the input packet in bytes.
 * \param[out]  outputBuffer        Buffer where the decoded data is stored.
 * \param[in]   outputBufferSize    Size of the output buffer in bytes.
 * \param[out]  bytesWritten        Number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                     On success.
 * \retval EscTlsRet_WARNING_DETECTED       A non fatal error occurred. An alert with warning severity must
 *                                          be generated (\see EscTlsInt_BuildAlert()) and sent to the peer.
 *                                          A detailed error code can be obtain by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_CLOSED_BY_PEER         The peer has shut down the connection. In this case, a close_notify
 *                                          alert must be sent (\see EscTlsInt_Shutdown()) and the connection
 *                                          must be closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED   The peer has sent a fatal alert. The connection must be closed
 *                                          immediately in this case. A detailed error code can be obtained
 *                                          by calling EscTlsInt_GetLastError().
 * \retval EscTlsRet_ERROR_DETECTED         A fatal error occurred. An alert with fatal severity must be
 *                                          generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                          The session is closed afterwards. A detailed error code can be obtained
 *                                          by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_AppDataReceived(
    EscTlsInt_SessionContext *sessionCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 packetSize,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten );

/**
 * Encode application data for sending. The data is encrypted and encapsulated in
 * a TLS record. Function will generate exactly one record. Input data must fit
 * into one record (EscTls_FRAGMENT_SIZE_MAX), if not an error is returned!
 * The handshake must have been finished before calling this function!
 *
 * Note: Input buffer and output buffer may be the same (in-place processing).
 *
 * \param[in]   sessionCtx          The TLS session context.
 * \param[in]   inputBuffer         Pointer to input buffer containing the data to be encoded
 * \param[in]   appDataSize         Length of input data in bytes. Must not exceed EscTls_FRAGMENT_SIZE_MAX.
 * \param[out]  outputBuffer        Output buffer to which the encoded result is stored
 * \param[in]   outputBufferSize    Size of output buffer. Its size must be at least
 *                                  (appDataSize + EscTlsInt_GetMaxProtectionOverhead()) bytes.
 * \param[out]  bytesWritten        Size of encoded data actually stored in the output buffer
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be
 *                                     generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_AppDataToBeSent(
    EscTlsInt_SessionContext *sessionCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 appDataSize,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten );

/* ------------------------ Function to shut down the connection ------------------------ */

/**
 * Shuts down the session. This function generates a close_notify alert
 * to be sent to the peer in order to inform it about the shutdown.
 * Note that according to the standard the peer has to send a close_notify
 * alert as an acknowledge indicator. But for the initiator it is not
 * required to wait for the alert. CycurTLS does not support waiting for
 * the response alert sent by the peer.
 *
 * \param[in, out]  sessionCtx          The TLS session context.
 * \param[out]      outputBuffer        Buffer where the "close notify" message is stored.
 * \param[out]      outputBufferSize    Size of output buffer in bytes.
 * \param[out]      bytesWritten        Number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be
 *                                     generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_Shutdown(
    EscTlsInt_SessionContext *sessionCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten );

/* ------------------------ Functions for error handling --------------------------------- */

/**
 * Builds an alert message.
 *
 * \param[in, out]  sessionCtx          The TLS session context.
 * \param[out]      outputBuffer        Pointer to the encoded message
 * \param[out]      outputBufferSize    Available size of the output buffer
 * \param[out]      bytesWritten        Number of bytes stored in the output buffer.
 *
 * \retval EscTlsRet_OK                On success.
 * \retval EscTlsRet_ERROR_DETECTED    A fatal error occurred. An alert with fatal severity must be
 *                                     generated and sent to the peer (\see EscTlsInt_BuildAlert()).
 *                                     The session is closed afterwards. A detailed error code can be
 *                                     obtained by calling EscTlsInt_GetLastError().
 */
EscTls_RETURN
EscTlsInt_BuildAlert(
    EscTlsInt_SessionContext *sessionCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *bytesWritten );

/**
 * Get the last alert which occurred during the protocol.
 * For definitions of alert values and types refer to tls_types.h.
 *
 * \param[in, out]  sessionCtx         The TLS session context.
 *
 * \return previously occurred error
 */
EscTls_RETURN
EscTlsInt_GetLastError(
    const EscTlsInt_SessionContext *sessionCtx);

/**
 * Get the connection's current top level state.
 *
 * \param[in]   sessionCtx     The TLS session context.
 *
 * \retval  EscTls_STATE_HANDSHAKE  The handshake is in progress.
 * \retval  EscTls_STATE_CONNECTED  The connection is established. Application data can be sent/received.
 * \retval  EscTls_STATE_CLOSED     The connection was closed locally or by the host.
 *                                  Note that this only refers to the TLS layer, but has
 *                                  no meaning for the underlying transport layer.
 * \retval  EscTls_STATE_ERROR      An error occurred during the session and the session cannot be used
 *                                  any further.
 */
EscTls_ConnectionState
EscTlsInt_GetConnectionTopLevelState(
    const EscTlsInt_SessionContext *sessionCtx );

/* ------------------------ Internal Functions - DO NOT CALL DIRECTLY! ------------------- */

/**
 * Get the connection's current state.
 *
 * \param[in]   sessionCtx     The TLS session context.
 *
 * \return  The current connection state
 */
EscTls_ConnectionState
EscTlsInt_GetConnectionState(
    const EscTlsInt_SessionContext *sessionCtx );

/**
 * Set the connection's current state (top and sub level state).
 * This function must be used with care and should only be used
 * for setting error code and not for restoring a closed connection.
 *
 * \param[in]   sessionCtx  The TLS session context.
 * \param[in]   state       The new connection state.
 *
 */
void
EscTlsInt_SetConnectionState(
    EscTlsInt_SessionContext *sessionCtx,
    EscTls_ConnectionState state );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_INTERNAL_H_ */
