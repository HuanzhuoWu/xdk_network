/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Crypto abstraction for TLS

 $Rev: 572 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CRYPTO_H_
#define ESC_TLS_CRYPTO_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_cycurlib.h"
#include "tls_error.h"
#include "tls_types.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/* Standard verifyDataLen used by common cipher suites */
#ifndef EscTls_VERIFY_DATA_LENGTH
/** Length of the verify data */
#   define EscTls_VERIFY_DATA_LENGTH 12U
#endif

/** Ciphersuite identifier for TLS NULL cipher suite */
#define TLS_NULL_WITH_NULL_NULL                   0x0000U

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/* HMAC parameters*/
#if (EscTls_HMAC == EscTls_SHA)
/** Length of the MAC secret */
#   define EscTls_MAC_SECRET_LENGTH                 20U
/** Length of the MAC */
#   define EscTls_MAC_LENGTH                        20U
/** HMAC context */
typedef EscHmacSha1_ContextT EscTls_HMACDigestContextT;
#elif (EscTls_HMAC == EscTls_SHA_256)
/** Length of the MAC secret */
#   define EscTls_MAC_SECRET_LENGTH                 32U
/** Length of the MAC */
#   define EscTls_MAC_LENGTH                        32U
/** HMAC context */
typedef EscHmacSha256_ContextT EscTls_HMACDigestContextT;
#else
#   define EscTls_MAC_SECRET_LENGTH                  0U
#   define EscTls_MAC_LENGTH                         0U
#endif


/* Cipher parameters*/
#if (EscTls_CIPHER == EscTls_AES_128_CBC)

/** Key length */
#   define EscTls_KEY_LENGTH                        16U
/** IV length */
#   define EscTls_IV_LENGTH                         16U
/** Fixed IV length */
#   define EscTls_FIXED_IV_LENGTH                   0U
/** Block length */
#   define EscTls_BLOCK_LENGTH                      16U

/** Cipher context */
typedef EscAesCbc_ContextT EscTls_CipherContext;

#elif (EscTls_CIPHER == EscTls_AES_256_CBC)

/** Key length */
#   define EscTls_KEY_LENGTH                        32U
/** IV length */
#   define EscTls_IV_LENGTH                         16U
/** Fixed IV length */
#   define EscTls_FIXED_IV_LENGTH                   0U
/** Block length */
#   define EscTls_BLOCK_LENGTH                      16U

/** Cipher context */
typedef EscAesCbc_ContextT EscTls_CipherContext;

#elif (EscTls_CIPHER == EscTls_AES_128_CCM_8)

/** Key length */
#   define EscTls_KEY_LENGTH                        16U
/** IV length */
#   define EscTls_IV_LENGTH                         8U
/** Fixed IV length */
#   define EscTls_FIXED_IV_LENGTH                   4U
/** Tag length */
#   define EscTls_TAG_LENGTH                        8U

#elif (EscTls_CIPHER == EscTls_AES_256_CCM_8)

/** Key length */
#   define EscTls_KEY_LENGTH                        32U
/** IV length */
#   define EscTls_IV_LENGTH                         8U
/** Fixed IV length */
#   define EscTls_FIXED_IV_LENGTH                   4U
/** Tag length */
#   define EscTls_TAG_LENGTH                        8U

#endif


#if (EscTls_PRF_DIGEST == EscTls_PRF_SHA_256)
/** Length of the PRF digest */
#   define EscTls_PRF_DIGEST_LEN (EscSha256_DIGEST_LEN)
/** Digest context for the handshake */
typedef EscSha256_ContextT EscTls_HandshakeDigestContextT;
#endif

/** Specific Identifier for cipher suites*/
typedef Esc_UINT16 EscTls_CiphersuiteIdentifier;

#if(EscTls_CIPHER_TYPE == EscTls_BLOCK_CIPHER)

/** Initialization functions for HMAC */
typedef EscTls_RETURN
(*Esc_hmacInitFunction)(
    EscTls_HMACDigestContextT *hashCtx, /*!< Context for the HMAC */
    const Esc_UINT8 key[],              /*!< key array */
    Esc_UINT32 keyLength );             /*!< key length */

/** Update functions for HMAC */
typedef EscTls_RETURN
(*Esc_hmacUpdateFunction)(
    EscTls_HMACDigestContextT *hashCtx, /*!< Context for the HMAC */
    const Esc_UINT8 payload[],          /*!< payload array */
    Esc_UINT32 payloadLen );           /*!< length of payload in bytes */


/** Finish functions for HMAC */
typedef EscTls_RETURN
(*Esc_hmacFinishFunction)(
    EscTls_HMACDigestContextT *hmacCtx, /*!< Context for the HMAC */
    Esc_UINT8 hmac[]);                  /*!< HMAC array */

/** Initialization functions for block ciphers */
typedef EscTls_RETURN
(*Esc_cipherInitFunction)(
    EscTls_CipherContext *cipherCtx,    /*!< Context of the Cipher */
    Esc_UINT16 keyBits,                 /*!< Key size in bits */
    const Esc_UINT8 *key );             /*!< Pointer to Key */

/** Symmetric function for block ciphers */
typedef EscTls_RETURN
(*Esc_symmetricCipherFunction)(
    EscTls_CipherContext *cipherCtx,    /*!< Context of the Cipher */
    const Esc_UINT8 *inputData,         /*!< Pointer of Data to parse */
    const Esc_UINT32 inputDataLen,      /*!< Data length */
    Esc_UINT8 outputData[] ,            /*!< Pointer to Output Data */
    const Esc_UINT8 *iv );              /*!< Pointer to IV */

#elif(EscTls_CIPHER_TYPE == EscTls_AEAD_CIPHER)

/** Symmetric function for AEAD ciphers */
typedef EscTls_RETURN
(*Esc_aeadCipherFunction)(
        Esc_UINT16 keyBits,             /*!< Key size in bits */
        const Esc_UINT8 key[],          /*!< Pointer to Key */
        const Esc_UINT8 aad[],          /*!< Pointer to additional authenticated data */
        Esc_UINT32 aadLen,              /*!< Length of AAD */
        const Esc_UINT8 nonce[],        /*!< Pointer to nonce */
        Esc_UINT8 nonceLen,             /*!< Length of nonce */
        const Esc_UINT8 inputData[],    /*!< Pointer to input data */
        Esc_UINT32 inputDataLen,        /*!< Length of input data */
        Esc_UINT8 tagLen,               /*!< Tag length */
        Esc_UINT8 outputData[] );       /*!< Pointer to output data */

#endif

/** Cipher suite parameters */
typedef struct
{
    /** Cipher Suite ID*/
    EscTls_CiphersuiteIdentifier id;

    /** Digest length of the hash function used in the PRF */
    Esc_UINT8 prfHashDigestLen;
    /** ID of the hash function used in the PRF */
    Esc_UINT8 prfHashId;
    /** Certificate type (only used if not PSK) */
    Esc_UINT8 certificateType;
    /** Signature algorithm (only used if not PSK) */
    Esc_UINT8 signAlgorithm;

#if (EscTls_CIPHER_TYPE == EscTls_BLOCK_CIPHER)
    /** Block length of the Cipher*/
    Esc_UINT8 blockSize;
    /** IV Length*/
    Esc_UINT8 ivSize;
    /** Cipher key length*/
    Esc_UINT8 keySize;
    /** HMAC digest length*/
    Esc_UINT8 macSize;
    /** HMAC key length*/
    Esc_UINT8 macKeyLength;

    /** Function pointer to Cipher Init function */
    Esc_cipherInitFunction Esc_cipherInit;
    /** Function pointer to encryption function */
    Esc_symmetricCipherFunction Esc_encrypt;
    /** Function pointer to decryption function */
    Esc_symmetricCipherFunction Esc_decrypt;

    /** Function pointer to HMAC init function */
    Esc_hmacInitFunction Esc_hmacInit;
    /** Function pointer to HMAC update function */
    Esc_hmacUpdateFunction Esc_hmacUpdate;
    /** Function pointer to HMAC finish function */
    Esc_hmacFinishFunction Esc_hmacFinish;
#elif (EscTls_CIPHER_TYPE == EscTls_AEAD_CIPHER)
    /** Length of the nonce salt */
    Esc_UINT8 nonceSaltLength;
    /** Length of the explicit nonce */
    Esc_UINT8 nonceLength;
    /** Cipher key length*/
    Esc_UINT8 keySize;
    /** Tag length*/
    Esc_UINT8 tagLength;

    /** Function pointer to the encryption function */
    Esc_aeadCipherFunction Esc_aeadEncrypt;
    /** Function pointer to decryption function */
    Esc_aeadCipherFunction Esc_aeadDecrypt;
#endif
} EscTls_CipherSuiteT;

/* Declaration of supported Cipher Suite*/
#if (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CBC_SHA)
/** Parameters for TLS_RSA_WITH_AES_128_CBC_SHA */
extern const EscTls_CipherSuiteT tlsRsaWithAes128CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CBC_SHA)
/** Parameters for TLS_RSA_WITH_AES_256_CBC_SHA */
extern const EscTls_CipherSuiteT tlsRsaWithAes256CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CBC_SHA256)
/** Parameters for TLS_RSA_WITH_AES_128_CBC_SHA256 */
extern const EscTls_CipherSuiteT tlsRsaWithAes128CbcSha256;

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CBC_SHA256)
/** Parameters for TLS_RSA_WITH_AES_256_CBC_SHA256 */
extern const EscTls_CipherSuiteT tlsRsaWithAes256CbcSha256;

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CBC_SHA)
/** Parameters for TLS_PSK_WITH_AES_128_CBC_SHA */
extern const EscTls_CipherSuiteT tlsPskWithAes128CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_256_CBC_SHA)
/** Parameters for TLS_PSK_WITH_AES_256_CBC_SHA */
extern const EscTls_CipherSuiteT tlsPskWithAes256CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CBC_SHA256)
/** Parameters for TLS_PSK_WITH_AES_128_CBC_SHA256 */
extern const EscTls_CipherSuiteT tlsPskWithAes128CbcSha256;

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CCM_8)
/** Parameters for TLS_RSA_WITH_AES_128_CCM_8 */
extern const EscTls_CipherSuiteT tlsRsaWithAes128Ccm8;

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CCM_8)
/** Parameters for TLS_RSA_WITH_AES_256_CCM_8 */
extern const EscTls_CipherSuiteT tlsRsaWithAes256Ccm8;

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CCM_8)
/** Parameters for TLS_PSK_WITH_AES_128_CCM_8 */
extern const EscTls_CipherSuiteT tlsPskWithAes128Ccm8;

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_256_CCM_8)
/** Parameters for TLS_PSK_WITH_AES_256_CCM_8 */
extern const EscTls_CipherSuiteT tlsPskWithAes256Ccm8;

#elif (EscTls_CIPHER_SUITE == TLS_DHE_PSK_WITH_AES_128_CBC_SHA)
/** Parameters for TLS_PSK_WITH_AES_256_CCM_8 */
extern const EscTls_CipherSuiteT tlsDhePskWithAes128CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA)
/** Parameters for TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA */
extern const EscTls_CipherSuiteT tlsEcdheEcdsaWithAes128CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
/** Parameters for TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA */
extern const EscTls_CipherSuiteT tlsEcdheEcdsaWithAes256CbcSha;

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256)
/** Parameters for TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 */
extern const EscTls_CipherSuiteT tlsEcdheEcdsaWithAes128CbcSha256;

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8)
/** Parameters for TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8 */
extern const EscTls_CipherSuiteT tlsEcdheEcdsaWithAes128Ccm8;

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8)
/** Parameters for TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8 */
extern const EscTls_CipherSuiteT tlsEcdheEcdsaWithAes256Ccm8;

#else
#   error "Unsupported cipher suite selected!"
#endif

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
 * Get cipher suite to corresponding cipher suite identifier
 *
 * \param[in]   suite   Cipher suite identifier
 *
 * \return Pointer to corresponding cipher suite
 */
const EscTls_CipherSuiteT *
EscTls_getCipherSuite(
    EscTls_CiphersuiteIdentifier suite );

/**
 * Initialization function for handshake digest
 *
 * \param[in, out] ctx  Pointer to digest context
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_handshakeDigestInit(
    EscTls_HandshakeDigestContextT *ctx );

/**
 * Handshake digest update function
 *
 * \param[in, out] ctx      Pointer to digest context
 * \param[in]   payload     Pointer to payload
 * \param[in]   payloadLen  Length of payload in Byte
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_handshakeDigestUpdate(
    EscTls_HandshakeDigestContextT *ctx,
    const Esc_UINT8 payload[],
    Esc_UINT32 payloadLen );

/**
 * Handshake digest finish function
 *
 * \param[in]   ctx         Pointer to digest context
 * \param[out]  digest      Pointer to digest
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_handshakeDigestFinish(
    EscTls_HandshakeDigestContextT* ctx,
    Esc_UINT8 digest[] );

/**
 * Returns maximum encoding overhead for a given cipher suite identifier
 *
 * \param[in] suite     16 bit cipher suite identifier
 *
 * \return maximum encoding overhead
 */
Esc_UINT16
EscTls_getMaxSuiteOverhead(
    EscTls_CiphersuiteIdentifier suite);

/**
 * Returns minimum encoding overhead for a given cipher suite identifier
 *
 * \param[in] suite     16 bit cipher suite identifier
 *
 * \return minimum encoding overhead
 */
Esc_UINT16
EscTls_getMinSuiteOverhead(
    EscTls_CiphersuiteIdentifier suite);

/**
 * Implementation of the TLS pseudo-random function (PRF)
 * using SHA-256 as hash function.
 *
 * \param[in] secret            The secret key.
 * \param[in] secretLength      Length of the secret key in bytes.
 * \param[in] label             The label.
 * \param[in] labelLength       Length of the label in bytes.
 * \param[in] seed              The initial seed.
 * \param[in] seedLength        Length of the seed in bytes.
 * \param[in] buffer            Buffer to store the generated random bytes.
 *                              The complete buffer is filled with pseudo-random data.
 * \param[in] bufferSize        Length of the buffer in bytes.
 *
 * \return EscTlsRet_OK on success or a corresponding error otherwise.
 */
EscTls_RETURN
EscTls_PrfSha256(
    const Esc_UINT8 secret[],
    Esc_UINT32 secretLength,
    const Esc_CHAR label[],
    Esc_UINT32 labelLength,
    const Esc_UINT8 seed[],
    Esc_UINT32 seedLength,
    Esc_UINT8 buffer[],
    Esc_UINT32 bufferSize );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CRYPTO_H_ */

