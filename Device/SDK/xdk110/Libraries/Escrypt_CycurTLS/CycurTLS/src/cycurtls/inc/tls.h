/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       High level API and network specific code for TLS

$Rev: 575 $
 */
/***************************************************************************/

#ifndef ESC_TLS_H_
#define ESC_TLS_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_internal.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** The size of the internal buffer inside the session context */
#define EscTls_INTERNAL_BUFFER_SIZE ( EscTls_MAXIMUM_FRAGMENT_LENGTH + EscTls_RECORD_HEADER_SIZE )

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/**
 * Send function pointer. Your send implementation has to follow this outline.
 *
 * Send data to the transport layer (e.g. TCP network socket).
 * Currently, only blocking mode is supported.
 *
 * \param[in] sendState     Send state as your send implementation needs it.
 *                          Can be used for providing socket numbers or flags.
 * \param[out] buffer       Pointer to the array where the data for sending is stored.
 * \param[in, out] len      Length of the data which has to be sent. Returns the bytes which were sent.
 *
 * \retval EscTlsRet_OK                 If everything works fine
 * \retval EscTlsRet_NETWORK_SEND_ERROR If an error occurred.
*/
typedef EscTls_RETURN (*EscTls_Send)(
    void* sendState,
    Esc_UINT8 buffer[],
    Esc_UINT16 *len );

/**
 * Receive function pointer. Your receive implementation has to follow this outline.
 *
 * Receive data from the transport layer (e.g. TCP network socket).
 * Currently, only blocking mode is supported.
 *
 * \param[in] recvState     Receive state as your send implementation needs it. Can be used for providing socket numbers or flags.
 * \param[out] buffer       Pointer to the array where the data from receiving is stored.
 * \param[in, out] len      Length of buffer. Returns the bytes received.
 *
 * \retval EscTlsRet_OK                 If everything works fine
 * \retval EscTlsRet_NETWORK_RECV_ERROR If an error occurred.
*/
typedef EscTls_RETURN (*EscTls_Recv)(
    void* recvState,
    Esc_UINT8 buffer[],
    Esc_UINT16 *len );


/** State of the TLS handshake */
typedef struct
{
    /** The TLS handshake context */
    EscTlsInt_HandshakeContext handshakeCtx;

    /** The flag which indicates if the context is used for the server or client side */
    Esc_BOOL isServer;
} EscTls_HandshakeContext;

/** State of the TLS session */
typedef struct
{
    /** The TLS session context */
    EscTlsInt_SessionContext sessionCtx;

#if (EscTls_SINGLE_SEND_BUFFER == 1)
    /** Pointer to the send buffer. Used for sending application data via Tls_Write().
        Must store fragment plus record header. */
    Esc_UINT8 *internalSendBuffer;
#else
    /** Internal send buffer. Used for sending application data via Tls_Write().
        Must store fragment plus record header. */
    Esc_UINT8 internalSendBuffer[EscTls_INTERNAL_BUFFER_SIZE];
#endif

    /** Internal buffer.
        Used for receiving application data via Tls_Read(), for sending and
        receiving handshake data by Tls_Connect(), and for sending alerts
        by all functions except Tls_Write().
        Must store fragment plus record header. */
    Esc_UINT8 internalBuffer[EscTls_INTERNAL_BUFFER_SIZE];
    /** Used bytes of the internal buffer */
    Esc_UINT16 internalBufferUsed;

    /** send() callback */
    EscTls_Send send;

    /** send() state */
    void *sendState;

    /** recv() callback */
    EscTls_Recv recv;

    /** recv() state */
    void *recvState;
} EscTls_SessionContext;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

#if (EscTls_SINGLE_SEND_BUFFER == 1)
/**
 * Sets the send buffer for the specified context. This buffer will be used for the Tls_Write() function.
 * Any other function use the internal buffer for sending Handshake data or alerts.
 * The TLS implementation does not check for any simultaneous accesses to this buffer.
 * The caller must protect this buffer on its own (e.g. via a mutex).
 *
 * \param[in,out]  sessionCtx               The TLS session context.
 * \param[in]      internalSendBuffer       The pointer to the buffer. Must have a size of at least EscTls_INTERNAL_BUFFER_SIZE.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetSendBuffer(
    EscTls_SessionContext *sessionCtx,
    Esc_UINT8 *internalSendBuffer );
#endif


/**
 * Initializes a TLS context.
 *
 * \param[in,out]  handshakeCtx  The TLS handshake context.
 * \param[in,out]  sessionCtx    The TLS session context.
 * \param[in]      isServer      If TRUE, the session is a server side session, otherwise
 *                               it is the client side.
 * \param[in]     send           The send function to be used to send computed data to the network.
 * \param[in]     sendState      The state of the send function. Can be used for sockets number or additional flags. May be NULL.
 * \param[in]     recv           The send function to be used to send computed data to the network.
 * \param[in]     recvState      The state of the recv function. Can be used for sockets number or additional flags. May be NULL.
 * \param[in]     prng           The pseudo-random number generator (PRNG) to be used
 *                               to provide random numbers for TLS. See random.h.
 * \param[in]     prngState      The state of the PRNG. May be NULL if the PRNG implementation
 *                               is stateless.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_Init(
    EscTls_HandshakeContext *handshakeCtx,
    EscTls_SessionContext *sessionCtx,
    Esc_BOOL isServer,
    EscTls_Send send,
    void *sendState,
    EscTls_Recv recv,
    void *recvState,
    EscRandom_GetRandom prng,
    void *prngState );


/**
 * Connect to a TLS server or wait for a connection attempt of a TLS client.
 * Depends on the client/server configuration by EscTls_Init().
 * This function does the complete TLS handshake.
 *
 * If a fatal error occurred, implementation will try to send a fatal alert to the peer.
 * The return code will be the error for the handshake message generation, independently of the success of the alert generation or sending.
 *
 * \param[in,out]  handshakeCtx  The TLS handshake context.
 * \param[in,out]  sessionCtx    The TLS session context.
 *
 * \retval EscTlsRet_OK                     On success.
 * \retval EscTlsRet_CLOSED_BY_PEER         The peer has shut down the connection. A close_notify
 *                                          alert has be sent and the connection is closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED   The peer has sent a fatal alert. Connection is closed.
 * \retval EscTlsRet_NETWORK_RECV_ERROR     A network error during calling the recv() callback occurred. Session is closed.
 * \retval EscTlsRet_NETWORK_SEND_ERROR     A network error during calling the send() callback occurred. Session is closed.
 * \retval EscTlsRet_UNEXPECTED_FUNCTION_CALL   The TLS connection is in a wrong state (e.g. handshake or closed). Session remains valid.
 */
EscTls_RETURN
EscTls_Connect(
    EscTls_HandshakeContext *handshakeCtx,
    EscTls_SessionContext *sessionCtx );


/**
 * Read and decode TLS application data.
 * If using a blocking recv() this function will return when a complete record is received and decoded.
 *
 * \param[in,out]  sessionCtx       The TLS session context.
 * \param[in]      outputBuffer     The buffer for the decoded data.
 * \param[in,out]  outputBufferSize The pointer to the size of the outputBuffer.
                                    After call it stores the number of bytes in the outputBuffer.
 *
 * \retval EscTlsRet_OK                 A complete record was retrieved and decoded. The outputBuffer was filled
                                        either with the complete decoded data or to the maximum size of the buffer.
                                        EscTls_ReadPending() can be used to determine if decoded data is available
                                        in the internal buffer.
 * \retval EscTlsRet_INSUFFICIENT_BUFFER  The retrieved TLS record is larger than the maximum allowed fragment size,
                                          defined in EscTls_MAXIMUM_FRAGMENT_LENGTH.
 * \retval EscTlsRet_CLOSED_BY_PEER         A close_notify Alert was received. Connection is closed.
 * \retval EscTlsRet_FATAL_ALERT_RECEIVED   The peer has sent a fatal alert. Connection is closed.
 * \retval EscTlsRet_NETWORK_RECV_ERROR     A network error during calling the recv() callback occurred.
 * \retval EscTlsRet_UNEXPECTED_FUNCTION_CALL   The TLS connection is in a wrong state (e.g. handshake or closed). Session remains valid.
 */
EscTls_RETURN
EscTls_Read(
    EscTls_SessionContext *sessionCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT32 *outputBufferSize );

/**
 * Test whether unread data is still available in the TLS read buffer.
 * This data can be read using EscTls_Read() without accessing the underlying
 * transport layer (e.g. using recv() on a socket).
 *
 * \param[in,out]   sessionCtx          The TLS session context.
 * \param[out]      numBytesPending     The number of bytes available in the TLS read buffer.
 *
 * \return EscTlsRet_OK if successful or a corresponding error otherwise.
 */
EscTls_RETURN
EscTls_ReadPending(
    EscTls_SessionContext *sessionCtx,
    Esc_UINT32 *numBytesPending);

/**
 * Encode and send application data.
 * If using a blocking send() this function will return when the complete application data is written to the TLS connection.
 * If EscTls_SINGLE_SEND_BUFFER == 1 the buffer "internalSendBuffer" provided via EscTls_SetSendBuffer() is temporarily used.
 * The caller must ensure that this buffer is empty and is not used simultaneously by another function, e.g. in a second thread.
 *
 * \param[in,out]  sessionCtx       The TLS session context.
 * \param[in]      inputBuffer      The buffer with the application data.
 * \param[in,out]  inputBufferSize  The pointer to the size of the application data.
                                    After call it stores the number of bytes in the outputbuffer.
 *
 * \return EscTlsRet_OK                         All application data was encoded and sent to the network.
 * \retval EscTlsRet_UNEXPECTED_FUNCTION_CALL   The TLS connection is in a wrong state (e.g. handshake or closed). Session remains valid.
 * \retval EscTlsRet_NETWORK_SEND_ERROR         A network error during calling the send() callback occurred. Session is closed.
 */
EscTls_RETURN
EscTls_Write(
    EscTls_SessionContext *sessionCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 *inputBufferSize );


/**
 * Shutdown an active TLS connection by sending a close_notify alert.
 * The connection is immediately closed, implementation will not wait for the close_notify alert from the peer.
 * If using a blocking send() and recv() this function will return when the close_notify alert is written to the TLS connection.
 *
 * \param[in,out]  sessionCtx    The TLS session context.
 *
 * \return EscTlsRet_OK                         Shutdown successful. Connection is closed.
 * \retval EscTlsRet_UNEXPECTED_FUNCTION_CALL   The TLS connection is in a wrong state (e.g. handshake or closed). Session remains valid.
 * \retval EscTlsRet_NETWORK_SEND_ERROR         A network error during calling the send() callback occurred. Session is closed.
 */
EscTls_RETURN
EscTls_Shutdown(
    EscTls_SessionContext *sessionCtx );


/* ------------------------------- Setter functions ------------------------- */

#if (EscTls_KEY_EXCHANGE_CERT_BASED)

/**
 * Sets the current Unix time for an upcoming session. The timestamp is used to check
 * whether the peer's certificate is expired.
 *
 * \param[in,out]  handshakeCtx   The TLS handshake context.
 * \param[in]      gmtUnixTime    The current Unix timestamp
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetTime(
    EscTls_HandshakeContext *handshakeCtx,
    Esc_UINT32 gmtUnixTime );

/**
 * Sets a callback for additional verification of the peer's certificate.
 * A typical use-case on client side is to check the domain name of the server certificate.
 * This callback is invoked after the validation of the peer's certificate chain.
 * Setting the callback to Esc_NULL_PTR effectively disables the callback.
 *
 * \param[in,out]   handshakeCtx    The TLS handshake context.
 * \param[in]       callback        The callback to be executed. For details, see tls_types.h
 * \param[in,out]   param           An arbitrary, additional parameter to be passed to the callback.
 *                                  It can be NULL if it is not used.
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetVerifyCallback(
    EscTls_HandshakeContext *handshakeCtx,
    EscTls_VerifyCertCallback callback,
    void *param);

/**
 * Test the domain name of the peer's certificate against a given name. This function
 * may be called within the callback set with EscTls_SetVerifyCallback() to verify
 * that the domain in the server's certificate is as expected.
 *
 * The function is limited to checking the Common Name (CN) of the certificate.
 * The subjectAltName extension is currently not supported. Only traditional
 * host names are supported (see RFC 6125). Wildcards in the CN are supported
 * only for the leftmost label and only if they comprise the entire label
 *
 *      *.example.net       =>  supported, will match foo.example.net, but not bar.foo.example.net
 *      *foo.example.net    =>  NOT supported
 *      *                   =>  NOT supported
 *      foo.*.net           =>  NOT supported
 *
 * \param[in]      cert         The parsed peer's certificate.
 * \param[in]      hostName     The expected host name as NULL terminated character string.
 *
 * \retval  TRUE if the hostName matches the CN of the certificate.
 * \retval  FALSE if the hostName does not match the certificate's CN or if the certificate
 *                does not have a CN set.
 */
Esc_BOOL
EscTls_CheckDomainName(
    const EscX509_CertificateT *cert,
    const char *hostName);

/**
 * Sets trusted certificates.
 *
 * \param[in,out]  handshakeCtx         The TLS handshake context.
 * \param[in]      trustedRootCert      List of trusted root certificates. Must remain allocated during the handshake.
 * \param[in]      numTrustedRootCert   Number of trusted root certificates.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetTrustedCertificates(
    EscTls_HandshakeContext *handshakeCtx,
    EscX509_CertificateT trustedRootCert[],
    Esc_UINT16 numTrustedRootCert);

/**
 * Sets certificate chain for authentication.
 *
 * \param[in,out]  handshakeCtx      The TLS handshake context.
 * \param[in]      certificateChain  List of certificates in the certificate chain, lowest certificate first.
 *                                   Must remain allocated during the handshake.
 * \param[in]      numCertificates   Number of certificates in the chain
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetCertificateChain(
    EscTls_HandshakeContext *handshakeCtx,
    const EscTls_CertificateT certificateChain[],
    Esc_UINT16 numCertificates);

#if (EscTls_HAS_SERVER)
/**
 * Enable or disable client authentication. This function is only useful on the
 * server side. By default, no client authentication is used.
 *
 * \param[in,out]  handshakeCtx         The TLS handshake context.
 * \param[in]      clientAuthEnabled    Flag to enable (TRUE) or disable (FALSE) client authentication.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetClientAuth(
    EscTls_HandshakeContext *handshakeCtx,
    Esc_BOOL clientAuthEnabled);
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
/**
 * Sets the RSA private key for authentication.
 * Note that this function does not create a deep copy of the key, but only
 * stores the pointer. Therefore, the key data must remain allocated for
 * the duration of the handshake!
 *
 * \param[in,out]  handshakeCtx  The TLS handshake context.
 * \param[in]      privateKey    Pointer to the RSA private key. Must remain allocated during the handshake.
 *                               See also rsa.h in the CycurLIB for conversion functions from byte arrays.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetRsaPrivateKey(
    EscTls_HandshakeContext *handshakeCtx,
    const EscRsa_PrivKeyT *privateKey);
#endif

#if EscTls_KEY_EXCHANGE_USES_ECC
/**
 * Sets the ECC private key for authentication. This key MUST be the private key
 * corresponding to the public key contained in the certificate set via
 * EscTls_SetCertificateChain(). The key expected here is a raw key and not encoded
 * in any way (no PEM, DER, ASN.1, PKCS#8).
 * Note that this function does not create a deep copy of the key, but only
 * stores the pointer. Therefore, the key data must remain allocated for
 * the duration of the handshake!
 *
 * \param[in,out]   handshakeCtx    The TLS handshake context.
 * \param[in]       keyData         The ECC private key.
 * \param[in]       keyDataLen      The length of the ECC key in bytes.
 * \param[in]       curveType       The elliptic curve on which the key is defined. The following values are permitted:
 *                                  - EscEcc_CURVE_SECP_192  (for NIST P-192)
 *                                  - EscEcc_CURVE_SECP_224  (for NIST P-224)
 *                                  - EscEcc_CURVE_SECP_256  (for NIST P-256)
 *                                  - EscEcc_CURVE_SECP_384  (for NIST P-384)
 *                                  - EscEcc_CURVE_SECP_521  (for NIST P-521)
 *                                  - EscEcc_CURVE_BRAINPOOL_P256  (for BrainpoolP256r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P384  (for BrainpoolP384r1)
 *                                  - EscEcc_CURVE_BRAINPOOL_P512  (for BrainpoolP512r1)
 *
 * \retval EscTlsRet_UNEXPECTED_DATA if the curve type is unknown or disabled in the configuration, or if
 *                                   the private key's length does not match the curve type.
 * \retval EscTlsRet_OK on success.
 */
EscTls_RETURN
EscTls_SetEccPrivateKey(
    EscTls_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *keyData,
    Esc_UINT16 keyDataLen,
    EscEcc_CurveId curveType);
#endif

#endif

#if EscTls_KEY_EXCHANGE_IS_PSK_BASED
#   if EscTls_HAS_CLIENT
/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in,out]  handshakeCtx  The TLS handshake context.
 * \param[in]      pskCtx        Pointer to user defined context for key agreement (may be NULL).
 * \param[in]      pskCallback   Callback function for generating a pre-shared
 *                               key and a client identity given a server hint.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetClientPskCallback(
    EscTls_HandshakeContext *handshakeCtx,
    void *pskCtx,
    EscTls_PskClientCallback pskCallback);
#   endif

#   if EscTls_HAS_SERVER
/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in,out]  handshakeCtx  The TLS handshake context.
 * \param[in]      pskCtx        Pointer to user defined context for key agreement (may be NULL)
 * \param[in]      pskCallback   Callback function for generating a pre-shared key given a client identity
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetServerPskCallback(
    EscTls_HandshakeContext *handshakeCtx,
    void *pskCtx,
    EscTls_PskServerCallback pskCallback);

/**
 * Sets the server identity hint which is sent to the client during the handshake.
 * If not set or set to zero length, the ServerKeyExchange message during the handshake is omitted.
 *
 * \param[in,out]   handshakeCtx            The TLS handshake context.
 * \param[in]       serverIdentityHint      Pointer to the server identity hint. Must remain allocated during the handshake.
 * \param[in]       serverIdentityHintLen   Length of the server identity.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetServerIdentityHint(
    EscTls_HandshakeContext *handshakeCtx,
    const Esc_CHAR *serverIdentityHint,
    Esc_UINT16 serverIdentityHintLen );

#   endif

#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
#   if EscTls_HAS_SERVER
/**
 * Sets the Diffie-Hellman parameters for the key exchange.
 * Parameters must fit to the configured options inside Diffie-Hellman module.
 * Parameter buffers must remain allocated during the complete handshake.
 *
 * \param[in,out]   handshakeCtx        The TLS handshake context.
 * \param[in]       dhModulus           Pointer to the DH modulus. Must remain allocated during the handshake.
 * \param[in]       dhModulusLength     Length of the DH modulus.
 * \param[in]       dhGenerator         Pointer to the DH generator. Must remain allocated during the handshake.
 * \param[in]       dhGeneratorLength   Length of the DH generator.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTls_SetDhParams(
    EscTls_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *dhModulus,
    Esc_UINT16 dhModulusLength,
    const Esc_UINT8 *dhGenerator,
    Esc_UINT16 dhGeneratorLength );
#   endif
#endif

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_H_ */
