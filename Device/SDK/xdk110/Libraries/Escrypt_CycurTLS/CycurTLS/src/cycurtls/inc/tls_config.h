/***************************************************************************
 * This file is part of CycurTLS                                           *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       TLS Configuration

 $Rev: 581 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CONFIG_H_
#define ESC_TLS_CONFIG_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/* Supported cipher suites - do not change! */

/** TLS_RSA_WITH_AES_128_CBC_SHA */
#define TLS_RSA_WITH_AES_128_CBC_SHA            0x002FU
/** TLS_RSA_WITH_AES_256_CBC_SHA */
#define TLS_RSA_WITH_AES_256_CBC_SHA            0x0035U
/** TLS_RSA_WITH_AES_128_CBC_SHA256 */
#define TLS_RSA_WITH_AES_128_CBC_SHA256         0x003CU
/** TLS_RSA_WITH_AES_256_CBC_SHA256 */
#define TLS_RSA_WITH_AES_256_CBC_SHA256         0x003DU
/** TLS_PSK_WITH_AES_128_CBC_SHA */
#define TLS_PSK_WITH_AES_128_CBC_SHA            0x008CU
/** TLS_PSK_WITH_AES_256_CBC_SHA */
#define TLS_PSK_WITH_AES_256_CBC_SHA            0x008DU
/** TLS_PSK_WITH_AES_128_CBC_SHA256 */
#define TLS_PSK_WITH_AES_128_CBC_SHA256         0x00AEU
/** TLS_RSA_WITH_AES_128_CCM_8 */
#define TLS_RSA_WITH_AES_128_CCM_8              0xC0A0U
/** TLS_RSA_WITH_AES_256_CCM_8 */
#define TLS_RSA_WITH_AES_256_CCM_8              0xC0A1U
/** TLS_PSK_WITH_AES_128_CCM_8 */
#define TLS_PSK_WITH_AES_128_CCM_8              0xC0A8U
/** TLS_PSK_WITH_AES_256_CCM_8 */
#define TLS_PSK_WITH_AES_256_CCM_8              0xC0A9U
/** TLS_DHE_PSK_WITH_AES_128_CBC_SHA */
#define TLS_DHE_PSK_WITH_AES_128_CBC_SHA        0x0090U
/** TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA */
#define TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA    0xC009U
/** TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA */
#define TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA    0xC00AU
/** TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 */
#define TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256 0xC023U
/** TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8 */
#define TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8      0xC0AEU
/** TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8 */
#define TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8      0xC0AFU

/** Cipher Suite is not assigned - we take a value from the "for private use" range */
#define TLS_UNASSIGNED_SUITE                    0xFFFFU


/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

#ifndef EscTls_HAS_SERVER
    /** Enable (1) or disable (0) server side support */
#   define  EscTls_HAS_SERVER   1
#endif

#ifndef EscTls_HAS_CLIENT
    /** Enable (1) or disable (0) client side support */
#   define  EscTls_HAS_CLIENT   1
#endif

#ifndef EscTls_HAS_DTLS
    /** Enabled (1) or diable (0) support for the DTLS protocol */
#   define EscTls_HAS_DTLS      1
#endif

#ifndef EscTls_SINGLE_SEND_BUFFER
    /** Enable (1) or disable (0) a single send buffer for several connections
        using the high-level API (tls.h).
        If enabled, the send buffer must be provided externally for every connection
        (e.g. context) via the function EscTls_SetSendBuffer() prior to the
        connection establishment which is done via EscTls_Connect().
        If disabled, every context has its own send buffer. */
#   define EscTls_SINGLE_SEND_BUFFER            0
#endif

#ifndef EscTls_MAXIMUM_FRAGMENT_LENGTH
    /** Set the maximum supported fragment length
        for the TLS application data. TLS default is 2^14.
        This size is also used for the maximum size of the handshake fragments.
        Furthermore, this length limits the size of one handshake message in
        general, i.e. the certificate chain must not exceed this length. */
#   define EscTls_MAXIMUM_FRAGMENT_LENGTH       16384U
#endif


#ifndef EscTls_CIPHER_SUITE
    /** Select cipher suite. */
#   define EscTls_CIPHER_SUITE  (TLS_PSK_WITH_AES_128_CBC_SHA)
#endif

#ifndef EscTls_CHECK_CERT_EXPIRATION
    /** Enable checking the notBefore and notAfter dates for each certificate against the current time.
     *  This requires that a correct time stamp is set via EscTls_SetTime / EscDtls_SetTime
     *  before starting the handshake.
     *  If it is NOT set then the validity period of the certificates will be ignored!
     */
#   define EscTls_CHECK_CERT_EXPIRATION         1
#endif

#ifndef EscTls_STRICT_CA_CERT_CHECK
    /** If enabled, the certificates of intermediate CAs are checked more strictly
     * as described in the path validation algorithm specified in RFC 5280 (6.1.4 (k)):
     * - The certificate must be X509 v3 (i.e. v1 or v2 are not allowed)
     * - The basicConstraints extension must be present with caFlag being set to TRUE
     * You may need to disable these checks if your PKI does not follow RFC 5280.
     */
#   define EscTls_STRICT_CA_CERT_CHECK          1
#endif

#ifndef EscTls_ANSWER_HEARTBEAT_EXTENSION
    /** Enabling this configuration option lets the CycurTLS server accept the heartbeat
     *  hello extension. Note that this does NOT enable support for heartbeat messages!
     *  OpenSSL always sends this extension by default, regardless of whether heartbeats
     *  will be sent or not. If disabled, a connection attempt containing the heartbeat
     *  extension will fail with an unsupported_extension alert.
     */
#   define EscTls_ANSWER_HEARTBEAT_EXTENSION    1
#endif

#ifndef EscDtls_INITIAL_TIMEOUT
    /** DTLS timeout duration in milliseconds.
     *  Default timeout after one second */
#   define EscDtls_INITIAL_TIMEOUT              (1U * 1000U)
#endif

#ifndef EscDtls_MAXIMUM_TIMEOUT
    /** DTLS timeout duration in milliseconds.
     *  Default timeout after one minute */
#   define EscDtls_MAXIMUM_TIMEOUT              (60U * 1000U)
#endif

#ifndef EscTls_PSK_IDENTITY_LENGTH_MAX
/** Maximum supported identity and server hint length, see RFC 4279, 5.3 */
#   define EscTls_PSK_IDENTITY_LENGTH_MAX       128U
#endif

#ifndef EscTls_PSK_KEY_LENGTH_MAX
/** Maximum supported PSK length, see RFC 4279, 5.3 */
#   define EscTls_PSK_KEY_LENGTH_MAX            64U
#endif

#ifndef EscDtls_MAX_COOKIE_SIZE
/** The maximum cookie size.
 *  The DTLS 1.2 standard requires 255 bytes. It must not exceed 255 bytes,
 *  but can be reduced to save memory (if it is assured that the server side sticks
 *  to this length when sending the cookie). Typically, server implementations
 *  use much less than 255 bytes.
 */
#   define EscDtls_MAX_COOKIE_SIZE              255U
#endif

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/* Definition of the Algorithm Identifiers */
/** AES 128 CBC */
#define EscTls_AES_128_CBC      0x0001
/** AES 256 CBC */
#define EscTls_AES_256_CBC      0x0002
/** AES 128 CCM 8*/
#define EscTls_AES_128_CCM_8    0x0003
/** AES 256 CCM 8*/
#define EscTls_AES_256_CCM_8    0x0004

/** PRF with SHA 256*/
#define EscTls_PRF_SHA_256      0x0101

/** RSA */
#define EscTls_RSA              0x0201
/** Pre-shared key */
#define EscTls_PSK              0x0202
/** Pre-shared key with DHE */
#define EscTls_DHE_PSK          0x0203
/** ECDHE signed with ECDSA */
#define EscTls_ECDHE_ECDSA      0x0204

/** HMAC hash function SHA1 */
#define EscTls_SHA              0x0301
/** HMAC hash function SHA256 */
#define EscTls_SHA_256          0x0302
/** No HMAC (AEAD mode) */
#define EscTls_NO_HMAC          0x0303

/* Cipher Types */

/** Block ciphers*/
#define EscTls_BLOCK_CIPHER     0x0402
/** Authenticated encryption with associated data ciphers*/
#define EscTls_AEAD_CIPHER      0x0403

#ifndef EscTls_PRF_DIGEST
    /** PRF Hash function*/
#   define EscTls_PRF_DIGEST (EscTls_PRF_SHA_256)
#endif

/* CipherSuite dependent configurations */
#if (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CBC_SHA)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA 1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CBC_SHA)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 256 CBC */
#   define EscTls_CIPHER       (EscTls_AES_256_CBC)
/** HMAC with SHA 1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CBC_SHA256)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA-256 */
#   define EscTls_HMAC         (EscTls_SHA_256)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CBC_SHA256)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 256 CBC */
#   define EscTls_CIPHER       (EscTls_AES_256_CBC)
/** HMAC with SHA-256 */
#   define EscTls_HMAC         (EscTls_SHA_256)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif(EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CBC_SHA)
/** Key exchange via PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_PSK)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA 1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif(EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_256_CBC_SHA)
/** Key exchange via PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_PSK)
/** Encryption via AES 256 CBC */
#   define EscTls_CIPHER       (EscTls_AES_256_CBC)
/** HMAC with SHA 1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif(EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CBC_SHA256)
/** Key exchange via PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_PSK)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA 256 */
#   define EscTls_HMAC         (EscTls_SHA_256)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_128_CCM_8)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 128 CCM */
#   define EscTls_CIPHER       (EscTls_AES_128_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_RSA_WITH_AES_256_CCM_8)
/** Key exchange via RSA */
#   define EscTls_KEY_EXCHANGE (EscTls_RSA)
/** Encryption via AES 256 CCM */
#   define EscTls_CIPHER       (EscTls_AES_256_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_128_CCM_8)
/** Key exchange via PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_PSK)
/** Encryption via AES 128 CCM */
#   define EscTls_CIPHER       (EscTls_AES_128_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_PSK_WITH_AES_256_CCM_8)
/** Key exchange via PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_PSK)
/** Encryption via AES 256 CCM */
#   define EscTls_CIPHER       (EscTls_AES_256_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_DHE_PSK_WITH_AES_128_CBC_SHA)
/** Key exchange via DHE_PSK */
#   define EscTls_KEY_EXCHANGE (EscTls_DHE_PSK)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA)
/** Key exchange via ECDHE_ECDSA */
#   define EscTls_KEY_EXCHANGE (EscTls_ECDHE_ECDSA)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA)
/** Key exchange via ECDHE_ECDSA */
#   define EscTls_KEY_EXCHANGE (EscTls_ECDHE_ECDSA)
/** Encryption via AES 256 CBC */
#   define EscTls_CIPHER       (EscTls_AES_256_CBC)
/** HMAC with SHA1 */
#   define EscTls_HMAC         (EscTls_SHA)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256)
/** Key exchange via ECDHE_ECDSA */
#   define EscTls_KEY_EXCHANGE (EscTls_ECDHE_ECDSA)
/** Encryption via AES 128 CBC */
#   define EscTls_CIPHER       (EscTls_AES_128_CBC)
/** HMAC with SHA-256 */
#   define EscTls_HMAC         (EscTls_SHA_256)
/** Block cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_BLOCK_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_128_CCM_8)
/** Key exchange via ECDHE_ECDSA */
#   define EscTls_KEY_EXCHANGE (EscTls_ECDHE_ECDSA)
/** Encryption via AES 128 CCM */
#   define EscTls_CIPHER       (EscTls_AES_128_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#elif (EscTls_CIPHER_SUITE == TLS_ECDHE_ECDSA_WITH_AES_256_CCM_8)
/** Key exchange via ECDHE_ECDSA */
#   define EscTls_KEY_EXCHANGE (EscTls_ECDHE_ECDSA)
/** Encryption via AES 128 CCM */
#   define EscTls_CIPHER       (EscTls_AES_256_CCM_8)
/** No HMAC (AEAD mode) */
#   define EscTls_HMAC         (EscTls_NO_HMAC)
/** AEAD cipher */
#   define EscTls_CIPHER_TYPE  (EscTls_AEAD_CIPHER)

#else
#   error "Unsupported cipher suite selected!"
#endif

/** Shortcut for testing whether the key exchange uses PSK */
#define EscTls_KEY_EXCHANGE_IS_PSK_BASED ( (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK) || (EscTls_KEY_EXCHANGE == EscTls_PSK) )

/** Shortcut for testing whether the key exchange involves ECC */
#define EscTls_KEY_EXCHANGE_USES_ECC (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)

/** Shortcut for testing whether the key exchange involves ephemeral (EC)DH */
#define EscTls_KEY_EXCHANGE_USES_DHE ( (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK) || (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA) )

/** Short for cipher suites for which certificates are exchanged */
#define EscTls_KEY_EXCHANGE_CERT_BASED ( (EscTls_KEY_EXCHANGE == EscTls_RSA) || (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA) )

/* Configuration checks */

#if (EscTls_HAS_CLIENT == 0) && (EscTls_HAS_SERVER == 0)
#error "Either or both client and server must be enabled!"
#endif

#if EscDtls_MAX_COOKIE_SIZE > 255
#error "EscDtls_MAX_COOKIE_SIZE must not be larger than 255!"
#endif

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CONFIG_H_ */
