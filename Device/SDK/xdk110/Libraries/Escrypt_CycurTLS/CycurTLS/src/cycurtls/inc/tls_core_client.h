/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Implementation of TLS message processing - client specific code

$Rev: 527 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CORE_CLIENT_H_
#define ESC_TLS_CORE_CLIENT_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_cycurlib.h"

#include "tls_types.h"
#include "tls_core_common.h"

#if EscTls_HAS_CLIENT

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
 * Initialize client specific parts of the context.
 *
 * \param[in, out]  handshakeCtx    The core handshake context.
 */
void
EscTlsCoreCli_Init(
    EscTlsCore_HandshakeContext *handshakeCtx);

#if EscTls_KEY_EXCHANGE_IS_PSK_BASED
/**
 * Sets the PSK callback function for cipher suites using PSK key exchange
 *
 * \param[in, out]  ctx             The TLS core handshake context.
 * \param[in]       callbackParam   User defined pointer which is passed to the callback (may be NULL).
 * \param[in]       pskCallback     Callback function for generating a pre-shared key
 *                                  and a client identity given a server hint.
 *                                  (see tls_types.h for the definition of the callback type)
 */
void
EscTlsCoreCli_SetPskCallback(
    EscTlsCore_HandshakeContext *ctx,
    void *callbackParam,
    EscTls_PskClientCallback pskCallback);

#endif

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#endif /* EscTls_HAS_CLIENT */

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CORE_CLIENT_H_ */
