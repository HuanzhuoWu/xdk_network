/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Utility functions.

 $Rev: 588 $
 */
/***************************************************************************/

#ifndef ESC_TLS_UTIL_H_
#define ESC_TLS_UTIL_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/


#include "tls_config.h"
#include "tls_cycurlib.h"
#include "tls_error.h"
#include "tls_types.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

#ifndef EscTls_DEBUG
/** Enable debug output to stdout using printf(). */
#   define EscTls_DEBUG                         0
#endif

#if EscTls_DEBUG == 1
#   include <stdio.h>
#endif

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** Return the minimum of two operands */
#define Esc_MIN(a,b)    ((a) < (b) ? (a) : (b))
/** Return the maximum of two operands */
#define Esc_MAX(a,b)    ((a) > (b) ? (a) : (b))

/* Debugging macros */

#if EscTls_DEBUG == 1
#   define EscTls_LOG0(x)          printf("[CYCURTLS LOG]  " x "\n")
#   define EscTls_LOG1(x, y)       printf("[CYCURTLS LOG]  " x "\n", y)
#   define EscTls_LOG2(x, y, z)    printf("[CYCURTLS LOG]  " x "\n", y, z)
#   define EscTls_LOG_ARRAY(str, array, len)   EscTlsUtil_LogArray(str, array, len)
#else
#   define EscTls_LOG0(x)
#   define EscTls_LOG1(x, y)
#   define EscTls_LOG2(x, y, z)
#   define EscTls_LOG_ARRAY(str, array, len)
#endif

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
 * Sets n bytes at dest to data
 *
 * \param[out]  dest    Pointer to the buffer to set
 * \param[in]   value   Value to set bytes at dest to
 * \param[in]   n       Number of bytes to set
 *
 */
void
EscTlsUtil_MemSet(
    void *dest,
    Esc_UINT8 value,
    Esc_UINT32 n );

/**
 * Copies n bytes from src to dest
 * src and dest should not overlap (use EscTlsUtil_MemMove instead)
 *
 * \param[in]   src     Pointer to data to copy
 * \param[in]   n       Number of bytes to copy
 * \param[out]  dest    Pointer to the buffer to paste
 *
 */
void
EscTlsUtil_MemCpy(
    void *dest,
    const void *src,
    Esc_UINT32 n );

/**
 * Copies n bytes from src to dest
 *
 * \param[in]   src     Pointer to data to copy
 * \param[in]   n       Number of bytes to copy
 * \param[out]  dest    Pointer to the buffer to paste
 *
 */
void
EscTlsUtil_MemMove(
    void *dest,
    const void *src,
    Esc_UINT32 n );

/**
 * Compare if the content of the given buffers is equal.
 *
 * \param[in]   buffer1     First buffer to be compared
 * \param[in]   buffer2     Second buffer to be compared
 * \param[in]   n           Length of buffers in bytes.
 *
 * \return TRUE if buffer1 and buffer2 have an equal content, FALSE otherwise.
 */
Esc_BOOL
EscTlsUtil_MemCmp(
    const void *buffer1,
    const void *buffer2,
    Esc_UINT32 n);

/**
 * Encode a 16-bit value as big endian integer into the given buffer.
 *
 * \param[out]  buffer  The buffer to store the encoded value.
 * \param[in]   value   The value to be encoded.
 */
void
EscTlsUtil_EncodeUint16(
    Esc_UINT8 buffer[],
    Esc_UINT16 value);

/**
 * Encode a 24-bit value as big endian integer into the given buffer.
 *
 * \param[out]  buffer  The buffer to store the encoded value.
 * \param[in]   value   The value to be encoded.
 */
void
EscTlsUtil_EncodeUint24(
    Esc_UINT8 buffer[],
    Esc_UINT32 value);

/**
 * Encode a 32-bit value as big endian integer into the given buffer.
 *
 * \param[out]  buffer  The buffer to store the encoded value.
 * \param[in]   value   The value to be encoded.
 */
void
EscTlsUtil_EncodeUint32(
    Esc_UINT8 buffer[],
    Esc_UINT32 value);

/**
 * Decode a 16-bit big endian integer from the given buffer.
 *
 * \param[in]   buffer  The buffer to decode the value from.
 *
 * \return The decoded value.
 */
Esc_UINT16
EscTlsUtil_DecodeUint16(
    const Esc_UINT8 buffer[]);

/**
 * Decode a 24-bit big endian integer from the given buffer.
 *
 * \param[in]   buffer  The buffer to decode the value from.
 *
 * \return The decoded value.
 */
Esc_UINT32
EscTlsUtil_DecodeUint24(
    const Esc_UINT8 buffer[]);

/**
 * Decode a 32-bit big endian integer from the given buffer.
 *
 * \param[in]   buffer  The buffer to decode the value from.
 *
 * \return The decoded value.
 */
Esc_UINT32
EscTlsUtil_DecodeUint32(
    const Esc_UINT8 buffer[]);

/**
 * Determine how many length bytes are required to encode the
 * length in DER (ASN.1).
 *
 * Len in [0..127]      => 1 byte
 * Len in [128..255]    => 2 bytes
 * Len in [256..65535]  => 3 bytes
 *
 * \param[in]   len     Length in bytes
 *
 * \return  Number of length bytes required for DER encoding
 */
Esc_UINT8
EscTlsUtil_DerGetNumLengthBytes(
    Esc_UINT16 len);

/**
 * Encode the given length using DER (ASN.1).
 *
 * Len in [0..127]      =>  <len0>
 * Len in [128..255]    =>  0x81 <len0>
 * Len in [256..65535]  =>  0x82 <len1> <len0>
 *
 * \param[in]   len     Byte length to be encoded.
 * \param[out]  buffer  Buffer to write the encoding to.
 */
void
EscTlsUtil_DerEncodeLength(
    Esc_UINT16 len,
    Esc_UINT8 buffer[]);

/**
 * Overwrite secret values such as key material.
 *
 * \param[in]   buffer  Data to be overwritten.
 * \param[in]   len     Length of the data in bytes.
 */
void
EscTlsUtil_Overwrite(
    volatile void *buffer,
    Esc_UINT32 len);

#if EscTls_DEBUG == 1
/**
 * Print the content of an array on stdout for debugging purposes.
 * The content is printed in hex.
 *
 * \param[in]   str     Prefix to be printed before the array.
 * \param[in]   array   The array to be printed.
 * \param[in]   len     Length of the array in bytes.
 */
void
EscTlsUtil_LogArray(
    const Esc_CHAR *str,
    const Esc_UINT8 array[],
    Esc_UINT32 len);
#endif

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_UTIL_H_ */
