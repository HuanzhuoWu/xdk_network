/***************************************************************************
 * This file is part of CycurTLS                                      *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/
 
/***************************************************************************/
/*!
 \file

 \brief       Implementation of TLS message processing -
              Common code for client and server side

$Rev: 576 $
 */
/***************************************************************************/

#ifndef ESC_TLS_CORE_COMMON_H_
#define ESC_TLS_CORE_COMMON_H_

#ifdef  __cplusplus
extern "C"
{
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "tls_config.h"
#include "tls_cycurlib.h"

#include "tls_constants.h"
#include "tls_types.h"
#include "tls_error.h"
#include "tls_util.h"
#include "tls_crypto.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/* Actions for handshake state machine. */

/** EscDtlsInt_HandshakeToBeSent function must be called next */
#define EscTls_HSACTION_SEND_DATA                  0x11U

/** EscDtlsInt_HandshakeReceived function must be called next */
#define EscTls_HSACTION_RECV_DATA                  0x22U

/** Handshake is completed */
#define EscTls_HSACTION_COMPLETED                  0x33U

/** Pseudo-action for initialization */
#define EscTls_HSACTION_NONE                       0x44U


/** Number of supported cipher suites at runtime */
#define EscTls_NUMBER_SUPPORTED_CIPHER_SUITES           1U
/** Number of supported compression methods at runtime */
#define EscTls_NUMBER_SUPPORTED_COMPRESSION_METHODS     1U


/* Bit flags indicating whether an extension was received */
/** Heartbeat extension received flag */
#define EscTls_EXT_RECEIVED_HEARTBEAT               0x0001U
/** ec_point_formats extension received flag */
#define EscTls_EXT_RECEIVED_EC_POINT_FORMATS        0x0002U
/** elliptic_curves extension received flag */
#define EscTls_EXT_RECEIVED_ELLIPTIC_CURVES         0x0004U
/** signature_algorithms extension received flag */
#define EscTls_EXT_RECEIVED_SIGNATURE_ALGORITHMS    0x0008U


/**
 * Connection state defines.
 * First 3 bits are for top level state. Remaining 5 bits for detailed state, e.g. handshake states.
*/

/** Top level state mask */
#define EscTls_TOP_STATE_MASK           0xE0U
/** Sub level state mask */
#define EscTls_SUB_STATE_MASK           0x1FU

/* Top states */
/** Handshake in progress */
#define EscTls_STATE_HANDSHAKE          0x20U
/** TLS connection is connected. */
#define EscTls_STATE_CONNECTED          0x40U
/** TLS connection was gracefully closed. */
#define EscTls_STATE_CLOSED             0x60U
/** TLS connection is closed by an error (e.g local error occurred or fatal alert received). */
#define EscTls_STATE_ERROR              0x80U
/** Local error occurred. Fatal alert must be sent. */
#define EscTls_STATE_LOCAL_ERROR        0xA0U

/* Global sub states */
/** Close-Notify was received. Close-Notify must be sent for acknowledge. */
#define EscTls_STATE_SUB_CLOSENNOTIFY_RECV          0x1EU
/** Renegotiation was requested. */
#define EscTls_STATE_SUB_RENEGOTIATION_REQU         0x1FU

/* Connection states */
/** Close-Notify was received in connection state. Close-Notify must be sent for acknowledge. */
#define EscTls_STATE_CON_CLOSENNOTIFY_RECV          (EscTls_STATE_CONNECTED | EscTls_STATE_SUB_CLOSENNOTIFY_RECV)
/** Renegotiation was requested. TLS is connected. */
#define EscTls_STATE_CON_RENEGOTIATION_REQU         (EscTls_STATE_CONNECTED | EscTls_STATE_SUB_RENEGOTIATION_REQU)


/* HSK states */
/** Initial state for a TLS client (i.e. before ClientHello) */
#define EscTls_STATE_HSK_START_TLS                     0x20U

/** Initial state for a DTLS client (i.e. before ClientHello without cookie) */
#define EscTls_STATE_HSK_START_DTLS                    0x21U

/** ClientHello without cookie was processed successfully (DTLS only) */
#define EscTls_STATE_HSK_AFTER_CLIENT_HELLO_WITHOUT_COOKIE    0x22U
/** Hello Verify Request was processed successfully (DTLS only) */
#define EscTls_STATE_HSK_AFTER_SERVER_HELLO_VERIFY_REQUEST    0x23U

/** ClientHello was processed successfully */
#define EscTls_STATE_HSK_AFTER_CLIENT_HELLO                   0x24U
/** ServerHello was processed successfully */
#define EscTls_STATE_HSK_AFTER_SERVER_HELLO                   0x25U
/** Server Certificate was processed successfully */
#define EscTls_STATE_HSK_AFTER_SERVER_CERTIFICATE             0x26U
/** Server Key Exchange was processed successfully */
#define EscTls_STATE_HSK_AFTER_SERVER_KEYEXCHANGE             0x27U
/** Certificate Request was processed successfully */
#define EscTls_STATE_HSK_AFTER_CERTIFICATE_REQUEST            0x28U
/** Server Hello Done was processed successfully */
#define EscTls_STATE_HSK_AFTER_SERVER_HELLO_DONE              0x29U
/** Client Certificate was processed successfully */
#define EscTls_STATE_HSK_AFTER_CLIENT_CERTIFICATE             0x2AU
/** Client Key Exchange was processed successfully */
#define EscTls_STATE_HSK_AFTER_CLIENT_KEY_EXCHANGE            0x2BU
/** Certificate Verify was processed successfully */
#define EscTls_STATE_HSK_AFTER_CERTIFICATE_VERIFY             0x2CU
/** ChangeCipherSpec was processed successfully */
#define EscTls_STATE_HSK_AFTER_CLIENT_CHANGECIPHERSPEC        0x2DU
/** Client Finished was processed successfully */
#define EscTls_STATE_HSK_AFTER_CLIENT_FINISHED                0x2EU
/** Change Cipher Spec was processed successfully */
#define EscTls_STATE_HSK_AFTER_SERVER_CHANGECIPHERSPEC        0x2FU

/* Handshake-Message (not type!) Identifier */
#define EscTls_HSK_ID_SERVER_MASK 0x80U
#define EscTls_HSK_ID_CLIENT_MASK 0x40U

#define EscTls_HSK_ID_CLIENT_HELLO              ( 1U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_HELLO_VERIFY_REQUEST      ( 2U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_SERVER_HELLO              ( 3U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_SERVER_CERTIFICATE        ( 4U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_SERVER_KEY_EXCHANGE       ( 5U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_CERTIFICATE_REQUEST       ( 6U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_SERVER_HELLO_DONE         ( 7U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_CLIENT_CERTIFICATE        ( 8U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_CLIENT_KEY_EXCHANGE       ( 9U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_CERTIFICATE_VERIFY        (10U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_CLIENT_CHANGE_CIPHER_SPEC (11U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_CLIENT_FINISHED           (12U | EscTls_HSK_ID_CLIENT_MASK)
#define EscTls_HSK_ID_SERVER_CHANGE_CIPHER_SPEC (13U | EscTls_HSK_ID_SERVER_MASK)
#define EscTls_HSK_ID_SERVER_FINISHED           (14U | EscTls_HSK_ID_SERVER_MASK)

/* Maximum size of the CertificateVerify message:
 * 2 bytes for signature/hash algorithm
 * 2 bytes for signature length
 * max. signature length
 */
#if (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)
/* DER encoded ECC signature:
 * - 3x 4 bytes for the headers (seq, int, int)
 * - 2x field element length with prepended zero to make it unsigned */
#define EscTls_MAX_CERT_VERIFY_LENGTH   (2U + 2U + (3U * 4U) + (2U * (1U + EscEcc_KEY_BYTES_MAX)))
#elif (EscTls_KEY_EXCHANGE == EscTls_RSA)
/* RSA signature:
 * - max RSA modulus size
 */
#define EscTls_MAX_CERT_VERIFY_LENGTH   (2U + 2U + EscRsa_KEY_BYTES_MAX)
#endif

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** Accumulated information about an elliptic curve */
typedef struct
{
    /** Curve ID in TLS */
    Esc_UINT16 tlsCurveId;

    /** Curve ID in CycurLIB */
    Esc_UINT8 libCurveId;

    /** Size of the curve in bytes. This assumes that both the prime p and the order n
     * have the same bit size. The byte size is rounded up (e.g. 521 bits => 66 bytes)
     */
    Esc_UINT8 curveSize;
} EscTls_CurveMap;

#if EscTls_KEY_EXCHANGE_CERT_BASED
/** Private key */
typedef struct
{
#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
    /** Pointer to RSA private key as defined in CycurLIB */
    const EscRsa_PrivKeyT *rsaKey;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)
    /** Pointer to ECC private key */
    const Esc_UINT8 *keyData;
    /** Length of private key */
    Esc_UINT16 keyDataLen;
    /** The elliptic curve on which the key is to be used */
    const EscTls_CurveMap *curve;
#endif
} EscTls_PrivateKey;
#endif

/** Type for next action to be executed within the handshake (send / receive / finished) */
typedef Esc_UINT8 EscTls_HandshakeAction;

/** type for handshake-message IDs */
typedef Esc_UINT8 EscTls_HskId;

/** Content type of handshake messages */
typedef Esc_UINT8 EscTls_HandshakeType;

/** Connection State type */
typedef Esc_UINT8 EscTls_ConnectionState;

/** Content type on record layer */
typedef Esc_UINT8 EscTls_ContentType;

/** Alert structure */
typedef struct
{
    Esc_UINT8 level;          /*!< Alert level (No Alert, Warning or Fatal) */
    Esc_UINT8 description;    /*!< Alert description */
} EscTls_AlertT;

/* Required forward declaration for below function pointers */

struct EscTlsCore_SessionContext_st;
struct EscTlsCore_HandshakeContext_st;

/* Function pointers for functions that must be implemented differently on
 * client side and on server side.
 */

typedef EscTls_RETURN
(*ProcessChangeCipherSpec_fp)(
    struct EscTlsCore_SessionContext_st *sessionCtx,
    struct EscTlsCore_HandshakeContext_st *handshakeCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize);

typedef EscTls_RETURN
(*ProcessHandshakeMessage_fp)(
    struct EscTlsCore_SessionContext_st *sessionCtx,
    struct EscTlsCore_HandshakeContext_st *handshakeCtx,
    const Esc_UINT8 messageContent[],
    Esc_UINT32 messageSize,
    Esc_UINT8 messageType,
    EscTls_HandshakeAction *nextAction);

typedef EscTls_RETURN
(*GenerateHandshakeMessage_fp)(
    struct EscTlsCore_SessionContext_st *sessionCtx,
    struct EscTlsCore_HandshakeContext_st *handshakeCtx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize,
    EscTls_ContentType *contentType,
    Esc_UINT8 *handshakeType,
    EscTls_HandshakeAction *nextAction,
    EscTls_HskId *messageId);

typedef EscTls_RETURN
(*RegenerateHandshakeMessage_fp)(
    struct EscTlsCore_SessionContext_st *sessionCtx,
    struct EscTlsCore_HandshakeContext_st *handshakeCtx,
    EscTls_HskId messageId,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize,
    EscTls_ContentType *contentType,
    Esc_UINT8 *handshakeType,
    EscTls_HandshakeAction *nextAction);

typedef EscTls_RETURN
(*CheckHandshakeParameters_fp)(
    struct EscTlsCore_HandshakeContext_st *handshakeCtx);


typedef EscTls_RETURN
(*ParseHelloExtension_fp)(
    struct EscTlsCore_SessionContext_st *sessionCtx,
    struct EscTlsCore_HandshakeContext_st *handshakeCtx,
    const Esc_UINT8 *extension,
    Esc_UINT16 extensionSize,
    Esc_UINT16 extensionType);


/* A structure to encapsulate the set of above functions */

typedef struct
{
    ProcessChangeCipherSpec_fp      processChangeCipherSpec;
    ProcessHandshakeMessage_fp      processHandshakeMessage;
    GenerateHandshakeMessage_fp     generateHandshakeMessage;
    RegenerateHandshakeMessage_fp   regenerateHandshakeMessage;
    CheckHandshakeParameters_fp     checkHandshakeParameters;
} HandshakeFunctions;

/** Parameters needed by the record layer */
typedef struct
{
#if (EscTls_MAC_SECRET_LENGTH != 0U)
    /** The secret for the MAC */
    Esc_UINT8 macSecret[EscTls_MAC_SECRET_LENGTH];
#endif

    /** The key for the cipher */
    Esc_UINT8 key[EscTls_KEY_LENGTH];

#if (EscTls_FIXED_IV_LENGTH != 0U)
    /** The initial vector for block ciphers or the nonce for AEAD ciphers */
    Esc_UINT8 iv[EscTls_FIXED_IV_LENGTH];
#endif

#if (EscTls_CIPHER_TYPE == EscTls_BLOCK_CIPHER)
    /** Context of the cipher */
    EscTls_CipherContext cipherCtx;
#endif

    /** Flag to control whether the record layer is protected (i.e. MACed and encrypted) or not */
    Esc_BOOL protectionActive;

} EscTls_ProtectionParametersT;

/**
 * State/key information of the current session.
 */
typedef struct EscTlsCore_SessionContext_st
{
    /** The protocol version being used */
    Esc_UINT16 protocolVersion;

    /** Parameters used to encode outgoing records */
    EscTls_ProtectionParametersT sendParams;

    /** Parameters used to decode ingoing records */
    EscTls_ProtectionParametersT receiveParams;

    /** PRNG */
    EscRandom_GetRandom prng;

    /** PRNG state */
    void *prngState;

    /** Last occurred error */
    EscTls_RETURN error;

    /** Cipher suite being used */
    const EscTls_CipherSuiteT *suite;

    /** State of the TLS connection */
    EscTls_ConnectionState connectionState;
} EscTlsCore_SessionContext;


/**
 * Handshake state information - Client specific.
 */
typedef struct
{
    /* DTLS specific */

#if EscTls_HAS_DTLS
#   if EscTls_KEY_EXCHANGE_CERT_BASED
    /** For DTLS, we store the entire CertificateVerify message for re-transmission.
     * This is necessary, because:
     * - The hash over all handshake messages will continue running to cover also
     *   Finished (client). We need to preserve at least the digest or verify_data.
     * - RSA: signature generation is deterministic, but computationally intensive.
     *   We don't want to repeat signature generation in the case of re-transmission.
     * - ECDSA: signature generation is not deterministic such that the entire
     *   signature must be preserved.
     */
    Esc_UINT8 certVerifyMessage[EscTls_MAX_CERT_VERIFY_LENGTH];

    /** Length of the stored CertificateVerifyMessage. Zero if no message was stored, yet. */
    Esc_UINT16 certVerifyMessageLength;
#   endif

#   if (EscTls_KEY_EXCHANGE == EscTls_RSA)

    /** A copy of the encrypted premaster secret for retransmission */
    Esc_UINT8 encryptedPreMasterSecret[EscRsa_KEY_BYTES_MAX];

    /** Flag indicating whether or not the premaster secret was already computed */
    Esc_BOOL premasterSecretComputed;
#   endif
#endif

#if EscTls_KEY_EXCHANGE_CERT_BASED
    /** Flag is true, when certificate request transmitted */
    Esc_BOOL clientCertificateRequestFlag;

    /** Flag is true, when the certificate request contained supported certificate types */
    Esc_BOOL clientCertificateTypeMatchFlag;
#endif

#if EscTls_KEY_EXCHANGE_IS_PSK_BASED
    /** Pointer to a user defined context to use in the PSK callback function */
    void *callbackParam;
    /** Callback function for generating the pre-shared key */
    EscTls_PskClientCallback pskCallback;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_PSK)
    /** Buffer containing the server identity hint for PSK key exchange. */
    Esc_CHAR serverIdentityHint[EscTls_PSK_IDENTITY_LENGTH_MAX];
    /** Length of the server identity hint */
    Esc_UINT16 serverIdentityHintLen;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
    /** The client identity. */
    Esc_CHAR clientIdentity[EscTls_PSK_IDENTITY_LENGTH_MAX];
    /** Actual length of the client identity */
    Esc_UINT16 clientIdentityLength;
#endif

} EscTlsCoreCli_HandshakeContext;


/**
 * Handshake state information - Server specific.
 */
typedef struct
{
#if EscTls_KEY_EXCHANGE_IS_PSK_BASED
    /** Pointer to a user defined context to use in the PSK callback function */
    void *callbackParam;
    /** Callback function for generating the pre-shared key */
    EscTls_PskServerCallback pskCallback;

    /** Pointer to the server identity hint for PSK key exchange. */
    const Esc_CHAR *serverIdentityHint;
    /** Length of the server identity hint */
    Esc_UINT16 serverIdentityHintLen;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
    /** The DH modulus */
    const Esc_UINT8 *dhModulus;
    /** Length of the DH modulus */
    Esc_UINT16 dhModulusLength;
    /** The DH generator */
    const Esc_UINT8 *dhGenerator;
    /** Length of the DH generator */
    Esc_UINT16 dhGeneratorLength;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
    /** The ClientHello.version */
    Esc_UINT16 clientHelloVersion;
#endif

    /** Flag to enable/disable client authentication. This has only an impact on the
     * server side for certificate based key exchange methods. Note that this flag
     * is also present in the PSK case to spare a lot of #ifdefs in other places.
     */
    Esc_BOOL clientAuthEnabled;
} EscTlsCoreSrv_HandshakeContext;


/**
 * Handshake state information.
 */
typedef struct EscTlsCore_HandshakeContext_st
{
    /* Members that are common for client and server */

    /* DTLS specific */

#if EscTls_HAS_DTLS
    /** The cookie sent by the server in the HelloVerifyRequest message. */
    Esc_UINT8 cookie[EscDtls_MAX_COOKIE_SIZE];

    /** The actual length of the DTLS cookie */
    Esc_UINT8 cookieLength;
#endif

    /** The verify_data of the Finished message. This is a backup for DTLS re-transmission. */
    Esc_UINT8 finishedVerifyData[EscTls_VERIFY_DATA_LENGTH];

    /** Flag to indicate that the verify_data was stored at finishedVerifyData */
    Esc_BOOL finishedVerifyDataStored;

    /** set to TRUE if we run DTLS */
    Esc_BOOL isDtls;

    /** The master secret used in TLS */
     Esc_UINT8 masterSecret[EscTls_MASTER_SECRET_LENGTH];

    /** Client random value */
    Esc_UINT8 clientRandom[EscTls_RANDOM_LENGTH];
    /** Server random value */
    Esc_UINT8 serverRandom[EscTls_RANDOM_LENGTH];

    /** Digest context over all handshake messages */
    EscTls_HandshakeDigestContextT handshakeDigest;

    /** A flag to indicate whether the recently received or sent handshake messages must be
     * hashed to compute verify_data or not */
    Esc_BOOL hashHandshakeMessages;

    /** Bit flags to indicate which hello extensions have been received from the peer */
    Esc_UINT16 extensionsReceived;

#if EscTls_KEY_EXCHANGE_CERT_BASED
    /** Current UNIX time */
    Esc_UINT32 gmtUnixTime;

    /** Private key for authentication */
    EscTls_PrivateKey privKey;

    /** Flag to indicate whether the own private key was set */
    Esc_BOOL privKeyWasSet;

    /** List of trusted CAs */
    EscX509_CertificateT *trustedRootCert;
    /** Number of trusted CAs */
    Esc_UINT16 numTrustedRootCert;

    /** List of certificates in certificate chain */
    const EscTls_CertificateT *certificateChain;
    /** Number of certificates in certificate chain */
    Esc_UINT16 numCertificates;
    /** Length of certificate chain including a prepended 3 byte length field in bytes*/
    Esc_UINT32 certificateChainLength;

    /** Verify callback for the peer's certificate */
    EscTls_VerifyCertCallback certVerifyCallback;
    /** Optional parameter for the verify callback */
    void *certVerifyParam;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
    /** The public key of the peer */
    EscRsa_PubKeyT peerPublicKey;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_ECDHE_ECDSA)
    /** The peer's static public key from its certificate */
    EscEcc_PublicKeyT peerPubKey;
    /** The curve on which the peer's public key is defined */
    const EscTls_CurveMap *peerPubKeyCurve;

    /** The local ephemeral ECDH public key */
    EscEcc_PublicKeyT localEcdhPublicKey;
    /** The local ephemeral ECDH private key */
    Esc_UINT8 localEcdhPrivateKey[EscEcc_KEY_BYTES_MAX];
    /** Length of the local ephemeral ECDH private key in bytes */
    Esc_UINT32 localEcdhPrivateKeySize;

    /** The elliptic curve to be used for ECDH */
    const EscTls_CurveMap *ecdhCurve;
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_DHE_PSK)
    /** The local ephemeral DH public key */
    EscDh_PublicKeyT localDhPublicKey;
    /** The local ephemeral DH private key */
    EscDh_PrivateKeyT localDhPrivateKey;
    /** Diffie-Hellman domain parameters (prime p and generator g) */
    EscDh_DomainParametersT dhDomainParams;
#endif

    /** Members that are specific to client or server */
    union
    {
#if EscTls_HAS_CLIENT
        EscTlsCoreCli_HandshakeContext client;
#endif

#if EscTls_HAS_SERVER
        EscTlsCoreSrv_HandshakeContext server;
#endif
    } side;

    const HandshakeFunctions *hsFunctions;

} EscTlsCore_HandshakeContext;

#if EscTls_KEY_EXCHANGE_USES_ECC
extern const EscTls_CurveMap EscTls_enabledCurves[];
extern const Esc_UINT8 EscTls_numEnabledCurves;
#endif

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

 /**
 * Parse a TLS variable size vector of type opaque whose upper limit
 * is 16 bit, i.e.
 *
 * opaque vec<min..2^16-1>;
 *
 * \param[in]   inputBuffer     The buffer containing the vector.
 * \param[in]   inputLength     Length of the buffer.
 * \param[in]   minLength       Minimum length of the vector. The function fails if the size
 *                              encoded at the beginning is smaller.
 * \param[out]  vectorLength    The encoded length of the vector.
 * \param[out]  vectorPtr       Pointer to the vector bytes.
 *
 * \retval  EscTlsRet_OK            The vector was parsed successfully.
 * \retval  EscTlsRet_DECODE_ERROR  The buffer is too small to contain the vector or the vector
 *                                  is smaller than the minimum length.
 */
EscTls_RETURN
EscTlsCore_ParseOpaqueVector16(
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputLength,
    Esc_UINT16 minLength,
    Esc_UINT16 *vectorLength,
    const Esc_UINT8 **vectorPtr);

#if EscTls_KEY_EXCHANGE_CERT_BASED
/**
 * Parses a Certificate message
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[in]       inputBuffer         Pointer to the message
 * \param[in]       inputBufferSize     Length of the message
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ParseCertificate(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize);

#endif

/**
 * Parses a Finished message
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[in]       finishedLabel       Pointer to the finished label
 * \param[in]       finishedLabelLen    Size of the finished label
 * \param[in]       inputBuffer         Pointer to the finished message
 * \param[in]       inputBufferSize     Size of the finished message
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ParseFinished(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_CHAR *finishedLabel,
    Esc_UINT32 finishedLabelLen,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize);

#if EscTls_KEY_EXCHANGE_CERT_BASED
/**
 * Builds a Certificate message
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[out]      outputBuffer        Pointer to the encoded message
 * \param[out]      outputBufferSize    Available size of the output buffer
 * \param[out]      messageSize         Pointer to the resulting Length of the message
 *
 * \return EscTlsRet_OK if everything works fine
 */

EscTls_RETURN
EscTlsCore_BuildCertificate(
    EscTlsCore_HandshakeContext *ctx,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize );
#endif

/**
 * Builds a Finished message
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[in]       finishedLabel       Pointer to the finished label
 * \param[in]       finishedLabelLen    Size of the finished label
 * \param[out]      outputBuffer        Pointer to the encoded message
 * \param[out]      outputBufferSize    Available size of the output buffer
 * \param[out]      messageSize         Pointer to the resulting Length of the message
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_BuildFinished(
    EscTlsCore_HandshakeContext *ctx,
    const Esc_CHAR *finishedLabel,
    Esc_UINT32 finishedLabelLen,
    Esc_UINT8 *outputBuffer,
    Esc_UINT16 outputBufferSize,
    Esc_UINT16 *messageSize );

#if (EscTls_KEY_EXCHANGE_CERT_BASED) && (EscTls_CHECK_CERT_EXPIRATION == 1)
/**
 * Converts a X509 time string to a GMT UNIX timestamp
 *
 * \param[out]  dest        Pointer to resulting GMT UNIX time stamp
 * \param[in]   timeStr     Time string to convert. It is NOT null-terminated.
 * \param[in]   timeStrLen  Length of time string in bytes.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_TimeStringToGmtUnix(
    Esc_UINT32 *dest,
    const Esc_UINT8 *timeStr,
    Esc_UINT32 timeStrLen);

#endif /* EscTls_CHECK_CERT_EXPIRATION == 1 */

#if (EscTls_CIPHER_TYPE == EscTls_BLOCK_CIPHER)

/**
 * Verifies the padding and the HMAC of a message
 *
 * \param[in, out]  ctx                 The TLS core session context.
 * \param[in]       inputBuffer         Pointer to the message to verify
 * \param[in]       inputBufferSize     Length of the message to verify
 * \param[in, out]  parameters          Pointer to the protection parameters
 * \param[in]       protocolVersion     The expected (D)TLS protocol version.
 * \param[in]       type                Content type of the message
 * \param[in]       seqNum              Current value of the record sequence number as byte array.
 * \param[out]      plaintextLength     Length of the message without padding and HMAC
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_VerifyPaddingMac(
    EscTlsCore_SessionContext *ctx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT16 inputBufferSize,
    const EscTls_ProtectionParametersT *parameters,
    Esc_UINT16 protocolVersion,
    EscTls_ContentType type,
    const Esc_UINT8 sequenceNumber[],
    Esc_UINT16 *plaintextLength );

#endif

/**
 * Initializes protection parameters
 *
 * \param[in, out]  parameters   Pointer to the protection parameters
 *
 */
void
EscTlsCore_InitProtectionParameters(
    EscTls_ProtectionParametersT *parameters );

/**
 * Computes the Master Secret and the session's key material.
 * The pre-master secret is overwritten afterwards!
 *
 * \param[in, out]  ctx                 The TLS core handshake context.
 * \param[in, out]  premasterSecret     Pointer to the Premaster Secret
 * \param[in]       premasterSecretLen  Length of the Premaster Secret
 * \param[out]      clientWriteParams   The protection parameters that the client uses to send.
 * \param[out]      serverWriteParams   The protection parameters that the server uses to send.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ComputeMasterSecretAndKeys(
    EscTlsCore_HandshakeContext *ctx,
    Esc_UINT8 *premasterSecret,
    Esc_UINT32 premasterSecretLen,
    EscTls_ProtectionParametersT *clientWriteParams,
    EscTls_ProtectionParametersT *serverWriteParams);

/**
 * Initialize symmetric crypto for sending protected packets.
 *
 * \param[in, out]  ctx                 The TLS core session context.
 * \param[in, out]  params              The protection parameters to be initialized.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_InitSymmetricCrypto(
    EscTlsCore_SessionContext *ctx,
    EscTls_ProtectionParametersT *params);


#if (EscTls_CIPHER_TYPE == EscTls_BLOCK_CIPHER)

/**
 * Generates a TLS HMAC
 *
 * \param[in]   suite           Pointer to the cipher suite to use
 * \param[in]   key             HMAC key array
 * \param[in]   sequenceNumber  Sequence number as byte array
 * \param[in]   type            Content type of the message
 * \param[in]   version         The TLS protocol version
 * \param[in]   payload         Pointer to the payload
 * \param[in]   data_length     Length of the payload in byte
 * \param[out]  hmac_hash       the resulting HMAC
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_Hmac(
    const EscTls_CipherSuiteT *suite,
    const Esc_UINT8 key[],
    const Esc_UINT8 sequenceNumber[],
    EscTls_ContentType type,
    Esc_UINT16 version,
    const Esc_UINT8 payload[],
    Esc_UINT16 dataLength,
    Esc_UINT8 hmacHash[] );

#endif

/**
 * Parses the extensions contained in a ClientHello or ServerHello message.
 * For each extension found, the provided callback function is called.
 *
 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 * \param[in]       inputBuffer         The buffer containing the list of hello extensions.
 * \param[in]       inputBufferSize     Length of the buffer.
 * \param[in]       extCallback         Function to call for each extension.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ParseHelloExtensions(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx,
    const Esc_UINT8 *inputBuffer,
    Esc_UINT32 inputBufferSize,
    ParseHelloExtension_fp extCallback);

#if EscTls_KEY_EXCHANGE_USES_ECC

/**
 * Parse the ec_point_formats extension. The function will only check that the
 * extension is syntactically correct and that the uncompressed format
 * is contained in the list.
 *
 * \param[in]       extension           The buffer containing the extension.
 * \param[in]       extensionSize       Size of the ec_point_format extension.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_ParsePointFormatExt(
    const Esc_UINT8 *extension,
    Esc_UINT16 extensionSize);


/**
 * Search the list of supported curves for the given TLS curve ID.
 *
 * \param[in]   id      The TLS curve ID to be searched for.
 *
 * \return A pointer to the list entry or Esc_NULL_PTR if none was found.
 */
const EscTls_CurveMap *
EscTlsCore_FindCurveByTlsId(
    Esc_UINT16 id);

/**
 * Search the list of supported curves for the given CycurLIB curve ID.
 *
 * \param[in]   id      The CycurLIB curve ID to be searched for.
 *
 * \return A pointer to the list entry or Esc_NULL_PTR if none was found.
 */
const EscTls_CurveMap *
EscTlsCore_FindCurveByCycurLibId(
    Esc_UINT8 id);

/**
 * Estimate required size of DER encoding of an ECDSA signature.
 * This function is pessimistic and might slightly overestimate.
 *
 * \param[in]   curveSize   The size of the elliptic curve in bytes.
 *
 * \return number of bytes required to encode the signature in DER.
 */
Esc_UINT16
EscTlsCore_DerEncodedEcdsaSigSize(
    Esc_UINT8 curveSize);

/**
 * Encode an ECDSA signature with DER encoding (ASN.1). The signature
 * is encoded as sequence of two integers.
 * The required buffer size should be checked before using EscTlsCore_DerEncodedEcdsaSigSize().
 *
 * \param[in]   signature   The ECDSA signature.
 * \param[in]   curveSize   The size of the elliptic curve in bytes.
 * \param[out]  buffer      Output buffer to write the encoded signature to.
 * \param[out]  sigSize     The actual size of the encoded signature written to the buffer.
 */
void
EscTlsCore_DerEncodeEcdsaSig(
    const EscEcc_SignatureT *signature,
    Esc_UINT8 curveSize,
    Esc_UINT8 *buffer,
    Esc_UINT16 *sigSize);

/**
 * Decode a DER encoded ECDSA signature. The expected signature is encoded as a
 * sequence of two integers.
 *
 * \param[in]   buffer      Buffer containing the encoded signature.
 * \param[in]   sigSize     Size of the encoded signature in bytes.
 * \param[in]   curveSize   The size of the elliptic curve in bytes.
 * \param[out]  signature   The decoded ECDSA signature.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_DerDecodeEcdsaSig(
    const Esc_UINT8 *buffer,
    Esc_UINT16 sigSize,
    Esc_UINT8 curveSize,
    EscEcc_SignatureT *signature);

#endif

#if (EscTls_KEY_EXCHANGE_USES_DHE)
/**
 * Generate a new Diffie-Hellman (DH) key pair. The key pair is stored in
 * the handshake context.
 *
 * \param[in, out]  sessionCtx          The TLS core session context.
 * \param[in, out]  handshakeCtx        The TLS core handshake context.
 *
 * \return EscTlsRet_OK if everything works fine
 */
EscTls_RETURN
EscTlsCore_GenerateDhKeyPair(
    EscTlsCore_SessionContext *sessionCtx,
    EscTlsCore_HandshakeContext *handshakeCtx);
#endif

#if (EscTls_KEY_EXCHANGE == EscTls_RSA)
/**
 * Convert a TLS hash algorithm identifier to the corresponding hash algorithm identifier
 * used by CycurLIB in the PKCS#1 modules.
 *
 * \param[in]   tlsHashId   The TLS hash algorithm identifier.
 *
 * \return The corresponding hash algorithm identifier used in the PKCS#1 modules of the CycurLIB.
 */
Esc_UINT8
EscTlsCore_TlsHashIdToLibPkcs1HashId(
    Esc_UINT8 tlsHashId);
#endif


/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/

#ifdef  __cplusplus
}
#endif

#endif /* ESC_TLS_CORE_COMMON_H_ */
