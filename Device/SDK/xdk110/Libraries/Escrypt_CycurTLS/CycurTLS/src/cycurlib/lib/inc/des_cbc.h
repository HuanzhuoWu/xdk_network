/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       DES implementation CBC mode
   \see FIPS PUB 46-3

   $Rev: 2788 $
 */
/***************************************************************************/

#ifndef ESC_DES_CBC_H_
#define ESC_DES_CBC_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "des_ecb.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** DES context CBC. */
typedef struct
{
    /** Context in ECB mode. */
    EscDesEcb_ContextT ecbCtx;

    /** 64-bit initialization vector   */
    Esc_UINT8 iv[EscDes_BLOCK_BYTES];
} EscDesCbc_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
Initializes the CBC Context with the corresponding DES key and IV.

This function does not check the key for weaknesses nor does it
check the parity bits.

To check if a key is a known weak key, use the function EscDesKeytest_IsKeyWeak().
\see des_keytest.h

\param[out] cbcCtx The DES CBC context.
\param[in] key Key bytes array. Array of size EscDesCbc_KEY_BYTES.
\param[in] iv 64-bit initialization vector. Array of EscDes_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesCbc_Init(
    EscDesCbc_ContextT* cbcCtx,
    const Esc_UINT8 key[],
    const Esc_UINT8 iv[] );

/**
Decrypts one or more data blocks and updates the DES context.

The context must be initialized with EscDesCbc_Init.
Plain pointer can be the same as cipher pointer.

\param[in] cbcCtx The previous DES CBC context.
\param[out] cbcCtx The updated DES CBC context.
\param[out] plain Will contain the plaintext after the function call.
\param[in] cipher Cipher data bytes array.
\param[in] length Length of plain and cipher data in bytes. Has to be multiple of EscDesCbc_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesCbc_Decrypt(
    EscDesCbc_ContextT* cbcCtx,
    Esc_UINT8 plain[],
    const Esc_UINT8 cipher[],
    Esc_UINT32 length );

/**
Encrypts one or more data blocks and updates the DES context.

The context must be initialized with EscDesCbc_Init.

plain pointer can be the same as cipher pointer.

\param[in] cbcCtx The previous DES CBC context.
\param[out] cbcCtx The updated DES CBC context.
\param[in] plain Plaintext to encrypt.
\param[out] cipher Array to write ciphertext to.
\param[in] length Length of plain and cipher data in bytes. Has to be multiple of EscDesCbc_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesCbc_Encrypt(
    EscDesCbc_ContextT* cbcCtx,
    Esc_UINT8 cipher[],
    const Esc_UINT8 plain[],
    Esc_UINT32 length );

/**
Sets a new IV for the DES CBC context.

\param[out] cbcCtx The DES CBC context.
\param[in] iv 64-bit initialization vector. Array of EscDes_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesCbc_SetIV(
    EscDesCbc_ContextT* cbcCtx,
    const Esc_UINT8 iv[] );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif

#endif
