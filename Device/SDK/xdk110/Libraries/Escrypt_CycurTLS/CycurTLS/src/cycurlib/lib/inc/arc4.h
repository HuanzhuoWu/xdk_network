/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       ARC4 implementation also known as ARCFOUR

   $Rev: 2788 $
 */
/***************************************************************************/

#ifndef ESC_ARC4_H_
#define ESC_ARC4_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "cycurlib_config.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** Number of bytes in the state. */
#define EscArc4_STATE_BYTES 256U

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** Arc4 context. */
typedef struct
{
    /** State. */
    Esc_UINT8 state[EscArc4_STATE_BYTES];
    /** Index i. */
    Esc_UINT8 i;
    /** Index j. */
    Esc_UINT8 j;
} EscArc4_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
Initializes the ARC4 context with the key.

\param[out] ctx The ARC4 context.
\param[in] key Key bytes array of keyLen bytes.
\param[in] keyLen Number of bytes in key. 1 <= keyLen <= 256

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscArc4_Init(
    EscArc4_ContextT* ctx,
    const Esc_UINT8 key[],
    Esc_UINT16 keyLen );

/**
Exors the generated pseudo random numbers with the input text.

The same function can be used for encryption and decryption.

The inData pointer can be the same as outData pointer.

\param[in] ctx The previous ARC4 context.
\param[out] ctx The updated ARC4 context.
\param[out] outData The inputData exored with the next random numbers.
\param[in] inData The input data.
\param[in] len The number of bytes in inData and outData.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscArc4_Apply(
    EscArc4_ContextT* ctx,
    Esc_UINT8 outData[],
    const Esc_UINT8 inData[],
    Esc_UINT32 len );

/**
Generate n bytes of the key stream and discard them.

Use this function for RC4-Drop[n]. It exist several suggestions for
discarding the first n bytes of the keystream. For example RFC4345 favoured
1536 as a good value for n.

\param[in] ctx The previous ARC4 context.
\param[out] ctx The updated ARC4 context.
\param[in] n The number of bytes which will be generated and discarded.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscArc4_Drop(
    EscArc4_ContextT* ctx,
    Esc_UINT32 n );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif

#endif
