/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       ECIES implementation according to the IEEE Std 1363a

   \see         IEEE Std 1363-2000 and IEEE Std 1363a
                All quoted references refer to the IEEE standard

   KDF (KDF2) + XOR Encryption is used.
   DHAES and NON-DHAES Mode is supported
   SHA-1 and SHA-256 is supported for KDF and HMAC
   EscEcc_ECDH_ENABLED in ecc.h must be set to 1 for using ECIES!

  $Rev: 593 $
 */
/***************************************************************************/

#ifndef ESC_ECIES_H_
#define ESC_ECIES_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "ecc.h"
#include "_feArith.h"
#include "_ptarithws.h"

#if EscEcc_ECDH_ENABLED == 1

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/** Enables the SHA-1 */
#define EscEcies_USE_SHA1   1

/** Enables the SHA-256 */
#define EscEcies_USE_SHA256 2

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/* User definable configuration */
 #ifndef EscEcies_DHAES_ENABLED
/**
 * If disabled, NON-DHAES Mode is used for compatibility with ANSI X9.63.
 * In this mode the scheme is malleable and is not secure against an
 * adaptively chosen ciphertext attack (CCA2) (see D.5.3.3 - Note 1)
 **/
#   define EscEcies_DHAES_ENABLED 1
#endif

#ifndef EscEcies_PLAIN_LEN
/** Set the plaintext length */
#   define EscEcies_PLAIN_LEN 16U
#endif

#ifndef EscEcies_HASH_TYPE
/** Determines the hash algorithm for the key derivation function and the HMAC.
    Supported values:
    - EscEcies_USE_SHA1
    - EscEcies_USE_SHA256
*/
#    define EscEcies_HASH_TYPE (EscEcies_USE_SHA1)
#endif

#ifndef EscEcies_MAC_KEY_BYTES
/** Set the used key length for the MAC tag in bytes */
#   define EscEcies_MAC_KEY_BYTES 16U
#endif

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

#if (EscEcies_HASH_TYPE == EscEcies_USE_SHA1)
#    include "sha_1.h"
#    include "hmac_sha_1.h"
/** Length of the output of one hash operation (depends on the used hash algorithm) */
#    define EscEcies_HASH_LEN EscSha1_DIGEST_LEN
/** Length of the MAC Tag */
#    define EscEcies_MAC_TAG_LEN EscHmacSha1_MAX_MAC_LENGTH
#elif (EscEcies_HASH_TYPE == EscEcies_USE_SHA256)
#   include "sha_256.h"
#   include "hmac_sha_256.h"

/** Length of the output of one hash operation (depends on the used hash algorithm) */
#   define EscEcies_HASH_LEN (EscSha256_DIGEST_LEN)
/** Length of the MAC Tag */
#   define EscEcies_MAC_TAG_LEN (EscHmacSha256_MAX_MAC_LENGTH)
#else
#   error "EscEcies_HASH_TYPE is not supported!"
#endif

/** Maximum length of the encoded ECC public key */
#define EscEcies_PUBLIC_KEY_LEN_MAX ( (2U * EscFeArith_KEY_BYTES_MAX) + 1U )

/** Maximum length of the ciphertext */
#define EscEcies_CIPHERTEXT_LEN_MAX ( EscEcies_PLAIN_LEN + EscEcies_PUBLIC_KEY_LEN_MAX + EscEcies_MAC_TAG_LEN )

/***************************************************************************
* 5. TYPE DEFINITIONS                                                     *
***************************************************************************/

/** ECIES context. */
typedef struct
{
    /** Encryption/Decryption Counter for the KDF Parameter */
    Esc_UINT32 counter;
    /** senders public key */
    EscEcc_PublicKeyT pubKey;
    /** The shared secret which will be computed with the senders private
        and the receivers public key */
#if EscEcies_DHAES_ENABLED == 1
    Esc_UINT8 sharedSecret[EscEcies_PUBLIC_KEY_LEN_MAX + EscFeArith_KEY_BYTES_MAX];
#else
    Esc_UINT8 sharedSecret[EscFeArith_KEY_BYTES_MAX];
#endif
    /** Selected Ecc Curve */
    const EscPtArithWs_CurveT *curve;
} EscEcies_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/
/**
Initialize the encryption counter and generate the sender's key pair.

The public key is not validated here since it can be omitted if the key is already authenticated.
If the public key must be validated beforehand, the function EscEcc_PublicKeyValidation from the ECC module can be used.
For the public key requirements cf. D.5.3.5 and D.5.1.6.

\param[out] ctx The ECIES context.
\param[in] recPublicKey The Receiver's public key.
\param[out] privKey The generated private Key of the sender.
\param[in,out] privKeyLen In: Size of the privKey buffer in bytes. EscFeArith_KEY_BYTES_MAX can
                              be used as the maximum size allowed by configuration.
                          Out: Size of the private key in bytes
\param[in] randomState Random state which may be required by your random implementation.
\param[in] getRandomFunc Random function as specified in random.h
\param[in] curveId The ID of the selected elliptic curve.

\return Esc_NO_ERROR if everything works fine.
 */
Esc_ERROR
EscEcies_Init_Encrypt(
    EscEcies_ContextT* ctx,
    const EscEcc_PublicKeyT* recPublicKey,
    Esc_UINT8 privKey[],
    Esc_UINT32* privKeyLen,
    void* randomState,
    EscRandom_GetRandom getRandomFunc,
    const EscEcc_CurveId curveId );

/**
Initialize decryption counter and compute the shared secret

\param[out] ctx The ECIES context.
\param[in] ciphertext The ciphertext with the encoded public key of the sender
\param[in] ciphertextLen Length of the ciphertext in bytes. This must be at least
           enough bytes to store the encoded public key (2 * curve.keySizeBytes + 1).
\param[in] privKey The private Key of the receiver.
\param[in] privKeyLen Length of the private key in bytes. This must match the selected curve (curve.keySizeBytes).
\param[in] curveId The ID of the selected elliptic curve.

\return Esc_NO_ERROR if everything works fine.
 */
Esc_ERROR
EscEcies_Init_Decrypt(
    EscEcies_ContextT* ctx,
    const Esc_UINT8 ciphertext[],
    Esc_UINT32 ciphertextLen,
    const Esc_UINT8 privKey[],
    Esc_UINT32 privKeyLen,
    const EscEcc_CurveId curveId );

/**
Encrypts one data block.
This implementation uses the KDF parameters as an encryption counter as proposed in 11.3.2 - Note 1 and D.5.3.3

\param[in] ctx The ECIES context.
\param[in] plaintext The plaintext. Buffer must have a length of EscEcies_PLAIN_LEN.
\param[out] ciphertext Array to store ciphertext.
\param[in,out] ciphertextLen In: Size of the ciphertext buffer in Bytes. EscEcies_CIPHERTEXT_LEN_MAX can
                                  be used as the maximum allowed by configuration.
                             Out: Size of the computed ciphertext in bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscEcies_Encrypt(
    EscEcies_ContextT* ctx,
    const Esc_UINT8 plaintext[],
    Esc_UINT8 ciphertext[],
    Esc_UINT32* ciphertextLen );

/**
Decrypts one data block and check the appended MAC for validity. Multiple ciphertext blocks must be
decrypted in the same order as the encryption took place.
This implementation uses the KDF parameters as an encryption counter as proposed in 11.3.2 - Note 1 and D.5.3.3

\param[in] ctx The ECIES context.
\param[in] ciphertext The ciphertext. Has to be the complete ciphertext with the encoded public key and MAC tag.
\param[in] ciphertextLen Length of the ciphertext in bytes.
\param[out] plaintext Array to store the plaintext of size EscEcies_PLAIN_LEN, must have a length of EscEcies_PLAIN_LEN.

\return Esc_NO_ERROR if everything works fine or invalid MAC.
*/
Esc_ERROR
EscEcies_Decrypt(
    EscEcies_ContextT* ctx,
    const Esc_UINT8 ciphertext[],
    Esc_UINT32 ciphertextLen,
    Esc_UINT8 plaintext[] );


/* SINGLE BLOCK ENCRYPTION/DECRYPTION */

/**
Encrypts one data block. Since this function does not use the KDF parameters, the resulting ciphertext
is not compatible with the EscEcies_Decrypt function. The output has to be decrypted with EscEcies_Decrypt_Single.

\param[in] plaintext The plaintext. Buffer must have a length of EscEcies_PLAIN_LEN.
\param[out] ciphertext Array to store ciphertext.
\param[in,out] ciphertextLen In: Size of the ciphertext buffer in Bytes. EscEcies_CIPHERTEXT_LEN_MAX can
                                  be used as the maximum allowed by configuration.
                             Out: Size of the computed ciphertext in bytes.
\param[in] recPublicKey The Receiver's public key. The key is validated by this function.
\param[out] privKey The private Key of the sender.
\param[in,out] privKeyLen In: Size of the privKey buffer in bytes. EscFeArith_KEY_BYTES_MAX can
                              be used as the maximum size allowed by configuration.
                          Out: Size of the private key in bytes
\param[in] randomState Random state which may be required by your random implementation.
\param[in] getRandomFunc Random function as specified in random.h
\param[in] curveId The ID of the selected elliptic curve.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscEcies_Encrypt_Single(
    const Esc_UINT8 plaintext[],
    Esc_UINT8 ciphertext[],
    Esc_UINT32* ciphertextLen,
    const EscEcc_PublicKeyT* recPublicKey,
    Esc_UINT8 privKey[],
    Esc_UINT32* privKeyLen,
    void* randomState,
    EscRandom_GetRandom getRandomFunc,
    const EscEcc_CurveId curveId );

/**
Decrypts one data block and check the appended MAC for validity. Since this function does not use the KDF
parameters, the ciphertext has to be encrypted with the the EscEcies_Encrypt_Single function.

\param[in] ciphertext The ciphertext. Has to be the complete ciphertext with the encoded public key and MAC tag.
\param[in] ciphertextLen Length of the ciphertext in bytes.
\param[out] plaintext Array to store the plaintext of size EscEcies_PLAIN_LEN, must have a length of EscEcies_PLAIN_LEN.
\param[in] privKey The private Key of the receiver.
\param[in] privKeyLen Length of the private key in bytes. This must match the selected curve (curve.keySizeBytes).
\param[in] curveId The ID of the selected elliptic curve.

\return Esc_NO_ERROR if everything works fine or invalid MAC.
*/
Esc_ERROR
EscEcies_Decrypt_Single(
    const Esc_UINT8 ciphertext[],
    Esc_UINT32 ciphertextLen,
    Esc_UINT8 plaintext[],
    const Esc_UINT8 privKey[],
    Esc_UINT32 privKeyLen,
    const EscEcc_CurveId curveId );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#endif /* EscEcc_ECDH_ENABLED */

#ifdef  __cplusplus
}
#endif

#endif /* ESC_ECIES_H_ */
