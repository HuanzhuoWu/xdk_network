/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       MD2 implementation.

                A maximum of 2^32-2 byte can be hashed.

   \see         RFC-1319

   $Rev: 2788 $
 */
/***************************************************************************/

#ifndef ESC_MD2_H_
#define ESC_MD2_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "cycurlib_config.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** Length of one hash block in bytes */
#define EscMd2_BLOCK_BYTES 16U

/** The MD2 digest length in bytes. */
#define EscMd2_DIGEST_LEN  (EscMd2_BLOCK_BYTES)

/***************************************************************************
* 5. TYPE DEFINITIONS                                                     *
***************************************************************************/

/** Hash context for MD2. */
typedef struct
{
    /** The first 16 byte point to state. The next 32 byte are temporary variables. */
    Esc_UINT8 x[3U * EscMd2_BLOCK_BYTES];
    /** The current checksum. */
    Esc_UINT8 checksum[EscMd2_BLOCK_BYTES];
    /** Current block. */
    Esc_UINT8 block[EscMd2_BLOCK_BYTES];
    /** Number of bytes in block used. */
    Esc_UINT8 blockLen;
    /** Total length of the message in byte. */
    Esc_UINT32 totalLen;
} EscMd2_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
This function initializes the MD2 context.

\param[out] ctx pointer to MD2 context.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscMd2_Init(
    EscMd2_ContextT* ctx );

/**
This function updates MD2 context for a block of data.

\param[in] ctx Pointer to input MD2 context.
\param[out] ctx Pointer to updated MD2 context.
\param[in] message Pointer to input data chunk.
                   May be a NULL pointer if messageLength is 0.
\param[in] messageLength Number of bytes of input data chunk.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscMd2_Update(
    EscMd2_ContextT* ctx,
    const Esc_UINT8 message[],
    Esc_UINT32 messageLength );

/**
This function does the final wrap-up and returns the hash.

\param[in] ctx Pointer to last MD2 context.
\param[out] ctx Pointer to updated MD2 context.
\param[out] digest Buffer to store digest value of digestLength bytes.
\param[in]  digestLength The desired length of the digest to be stored in the buffer "digest".
                         This parameter can be used to truncate the digest such that only
                         the first bytes are copied to the output buffer. The value must
                         not be zero and must not exceed EscMd2_DIGEST_LEN.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscMd2_Finish(
    EscMd2_ContextT* ctx,
    Esc_UINT8 digest[],
    Esc_UINT8 digestLength );

/**
This function computes the MD2 digest by internal use
of EscMd2_Init, EscMd2_Update, and EscMd2_Finish.

\param[in] message Message the digest is computed for.
                   May be a NULL pointer if messageLength is 0.
\param[in] messageLength Length of message data in bytes.
\param[out] digest Buffer to store digest value of digestLength bytes. Can be the same as the message buffer.
\param[in]  digestLength The desired length of the digest to be stored in the buffer "digest".
                         This parameter can be used to truncate the digest such that only
                         the first bytes are copied to the output buffer. The value must
                         not be zero and must not exceed EscMd2_DIGEST_LEN.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscMd2_Calc(
    const Esc_UINT8 message[],
    Esc_UINT32 messageLength,
    Esc_UINT8 digest[],
    Esc_UINT8 digestLength );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif
#endif
