/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       MD-5 implementation.
                A maximum of 2^32-2 bytes can be hashed.

   \see         RFC-1321

   $Rev: 2788 $
 */
/***************************************************************************/

#ifndef ESC_MD5_H_
#define ESC_MD5_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "cycurlib_config.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/** The MD-5 digest length in bytes. */
#define EscMd5_DIGEST_LEN   16U

/** Length of one hash block in bytes */
#define EscMd5_BLOCK_BYTES  64U

/***************************************************************************
* 5. TYPE DEFINITIONS                                                     *
***************************************************************************/

/** Hash context for MD-5. */
typedef struct
{
    /** Hash values h0 .. h4. */
    Esc_UINT32 h[4];
    /** Current block. */
    Esc_UINT8 block[EscMd5_BLOCK_BYTES];
    /** Number of bytes in block used. */
    Esc_UINT8 blockLen;
    /** Total length of the message in byte. */
    Esc_UINT32 totalLen;
} EscMd5_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
This function initializes the MD5 values according.

\param[out] ctx pointer to MD5 context.

\return Esc_NO_ERROR if everything works fine.
*/
/***************************************************************************/
Esc_ERROR
EscMd5_Init(
    EscMd5_ContextT* ctx );

/**
This function updates MD5 context for a block of data.

\param[in] ctx Pointer to input MD5 context.
\param[out] ctx Pointer to updated MD5 context.
\param[in] message Pointer to input data chunk.
                   May be a NULL pointer if messageLength is 0.
\param[in] messageLength Number of bytes of input data chunk.

\return Esc_NO_ERROR if everything works fine.
*/
/***************************************************************************/
Esc_ERROR
EscMd5_Update(
    EscMd5_ContextT* ctx,
    const Esc_UINT8 message[],
    Esc_UINT32 messageLength );

/**
This function does the final wrap-up and returns the hash.

\param[in] ctx Pointer to last MD5 context.
\param[out] ctx Pointer to updated MD5 context.
\param[out] digest Buffer to store digest value of digestLength bytes.
\param[in]  digestLength The desired length of the digest to be stored in the buffer "digest".
                         This parameter can be used to truncate the digest such that only
                         the first bytes are copied to the output buffer. The value must
                         not be zero and must not exceed EscMd5_DIGEST_LEN.

\return Esc_NO_ERROR if everything works fine.
*/
/***************************************************************************/
Esc_ERROR
EscMd5_Finish(
    EscMd5_ContextT* ctx,
    Esc_UINT8 digest[],
    Esc_UINT8 digestLength );

/**
This function computes the MD5 digest by internal use
of EscMd5_Init, EscMd5_Update, and EscMd5_Finish.

\param[in] message Message the digest is computed for.
\param[in] messageLength Length of message data in bytes
                   May be a NULL pointer if messageLength is 0.
\param[out] digest Buffer to store digest value of digestLength bytes. Can be the same as the message buffer.
\param[in]  digestLength The desired length of the digest to be stored in the buffer "digest".
                         This parameter can be used to truncate the digest such that only
                         the first bytes are copied to the output buffer. The value must
                         not be zero and must not exceed EscMd5_DIGEST_LEN.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscMd5_Calc(
    const Esc_UINT8 message[],
    Esc_UINT32 messageLength,
    Esc_UINT8 digest[],
    Esc_UINT8 digestLength );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif
#endif
