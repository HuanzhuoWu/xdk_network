/***************************************************************************
 * This file is part of the cryptographic library CycurLIB                 *
 *                                                                         *
 * Copyright (C) 2016 ESCRYPT GmbH - All Rights Reserved                   *
 *                                                                         *
 *     ESCRYPT GmbH - Embedded Security                                    *
 *     Zentrum fuer IT-Sicherheit                                          *
 *     Lise-Meitner-Allee 4                                                *
 *     44801 Bochum                                                        *
 *     Germany                                                             *
 *                                                                         *
 *     www.escrypt.com                                                     *
 *                                                                         *
 ***************************************************************************/

/***************************************************************************/
/*!
   \file

   \brief       DES implementation ECB mode
   \see FIPS PUB 46-3

   $Rev: 2788 $
 */
/***************************************************************************/

#ifndef ESC_DES_ECB_H_
#define ESC_DES_ECB_H_

#ifdef  __cplusplus
extern "C" {
#endif

/***************************************************************************
 * 1. INCLUDES                                                             *
 ***************************************************************************/

#include "_des.h"

/***************************************************************************
 * 2. DEFINES REQUIRED BY CONFIGURATION (MUST NOT BE CHANGED!)             *
 ***************************************************************************/

/***************************************************************************
 * 3. CONFIGURATION                                                        *
 ***************************************************************************/

/***************************************************************************
 * 4. DEFINES                                                              *
 ***************************************************************************/

/***************************************************************************
 * 5. TYPE DEFINITIONS                                                     *
 ***************************************************************************/

/** DES context ECB. */
typedef struct
{
    /** The key schedule. */
    EscDes_KeySchedT sched;
} EscDesEcb_ContextT;

/***************************************************************************
 * 6. FUNCTION PROTOTYPES                                                  *
 ***************************************************************************/

/**
Initializes the ECB Context with the corresponding DES key.

This function does not check the key for weaknesses nor does it
check the parity bits.

To check if a key is a known weak key, use the function EscDesKeytest_IsKeyWeak().
\see des_keytest.h

\param[out] ecbCtx The DES ECB context.
\param[in] key Key bytes array. Array of size EscDes_KEY_BYTES.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesEcb_Init(
    EscDesEcb_ContextT* ecbCtx,
    const Esc_UINT8 key[] );

/**
Encrypts one DES block in ECB mode.

Block size is EscDes_BLOCK_BYTES.
The context must be initialized with EscDesEcb_Init.

The parameters plain and cipher can point to the same location.

\param[in] ecbCtx Context initialized with EscDesEcb_Init.
\param[in] plain Plaintext to encrypt. Must be exactly EscDes_BLOCK_BYTES bytes.
\param[out] cipher Encrypted plaintext. Must be exactly EscDes_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesEcb_Encrypt(
    const EscDesEcb_ContextT* ecbCtx,
    const Esc_UINT8 plain[],
    Esc_UINT8 cipher[] );

/**
Decrypts one DES block in ECB mode.

Block size is EscDes_BLOCK_BYTES.
The context must be initialized with EscDesEcb_Init.

The parameters plain and cipher can point to the same location.

\param[in] ecbCtx Context initialized with EscDesEcb_Init.
\param[in] cipher Ciphertext to decrypt. Must be exactly EscDes_BLOCK_BYTES bytes.
\param[out] plain Decrypted ciphertext. Must be exactly EscDes_BLOCK_BYTES bytes.

\return Esc_NO_ERROR if everything works fine.
*/
Esc_ERROR
EscDesEcb_Decrypt(
    const EscDesEcb_ContextT* ecbCtx,
    const Esc_UINT8 cipher[],
    Esc_UINT8 plain[] );

/**
Encrypts one DES block in ECB mode.

Block size is EscDes_BLOCK_BYTES.
The context must be initialized with EscDesEcb_Init.

The parameters plain and cipher can point to the same location.

This function performs no parameter check and is intended for internal use.

\param[in] ecbCtx Context initialized with EscDesEcb_Init.
\param[in] plain Plaintext to encrypt. Must be exactly EscDes_BLOCK_BYTES bytes.
\param[out] cipher Encrypted plaintext. Must be exactly EscDes_BLOCK_BYTES bytes.
*/
void
EscDesEcb_EncryptFast(
    const EscDesEcb_ContextT* ecbCtx,
    const Esc_UINT8 plain[],
    Esc_UINT8 cipher[] );

/**
Decrypts one DES block in ECB mode.

Block size is EscDes_BLOCK_BYTES.
The context must be initialized with EscDesEcb_Init.

The parameters plain and cipher can point to the same location.

This function performs no parameter check and is intended for internal use.

\param[in] ecbCtx Context initialized with EscDesEcb_Init.
\param[in] cipher Ciphertext to decrypt. Must be exactly EscDes_BLOCK_BYTES bytes.
\param[out] plain Decrypted ciphertext. Must be exactly EscDes_BLOCK_BYTES bytes.
*/
void
EscDesEcb_DecryptFast(
    const EscDesEcb_ContextT* ecbCtx,
    const Esc_UINT8 cipher[],
    Esc_UINT8 plain[] );

/***************************************************************************
 * 7. END                                                                  *
 ***************************************************************************/
#ifdef  __cplusplus
}
#endif

#endif
