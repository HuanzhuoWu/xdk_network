##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

#Build chain settings	
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra -Wstrict-prototypes\
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections -D$(BCDS_DEVICE_ID) 
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -Wno-unused-parameter -Wno-missing-braces -Wno-missing-field-initializers -Wno-strict-aliasing -Wno-sign-compare
CFLAGS_DEBUG =  $(BCDS_CFLAGS_DEBUG_COMMON) $(CFLAGS_COMMON)
CFLAGS_RELEASE = $(BCDS_CFLAGS_RELEASE_COMMON) $(CFLAGS_COMMON)
ARFLAGS = -cr

# A variable which evaluates to a list of all source code files (*.c)
BCDS_WIFILIB_SOURCE_FILES = \
	$(BCDS_WIFILIB_SOURCE_PATH)/device.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/driver.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/flowcont.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/fs.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/netapp.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/netcfg.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/nonos.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/socket.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/spawn.c \
	$(BCDS_WIFILIB_SOURCE_PATH)/wlan.c 	 	
	
# Objects
BCDS_WIFILIB_SOURCE_OBJECT_FILES = \
	$(patsubst $(BCDS_WIFILIB_SOURCE_PATH)/%.c, %.o, $(BCDS_WIFILIB_SOURCE_FILES))

BCDS_WIFILIB_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_WIFILIB_DEBUG_OBJECT_PATH)/, $(BCDS_WIFILIB_SOURCE_OBJECT_FILES))

BCDS_WIFILIB_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_WIFILIB_DEBUG_OBJECT_PATH)/, $(BCDS_WIFILIB_SOURCE_OBJECT_FILES:.o=.d))	
	
BCDS_WIFILIB_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_WIFILIB_RELEASE_OBJECT_PATH)/, $(BCDS_WIFILIB_SOURCE_OBJECT_FILES))
	
BCDS_WIFILIB_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_WIFILIB_RELEASE_OBJECT_PATH)/, $(BCDS_WIFILIB_SOURCE_OBJECT_FILES:.o=.d))		 
	
##################### Build Targets #########################
.PHONY: debug
debug: $(BCDS_WIFILIB_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_WIFILIB_RELEASE_LIB)

all: debug release
	
$(BCDS_WIFILIB_DEBUG_LIB): $(BCDS_WIFILIB_DEBUG_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build WIFILIB for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_WIFILIB_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_WIFILIB_RELEASE_LIB): $(BCDS_WIFILIB_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build WIFILIB for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_WIFILIB_RELEASE_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_WIFILIB_DEBUG_OBJECT_PATH)/%.o: $(BCDS_WIFILIB_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
        -I . $(BCDS_WIFILIB_INCLUDES) $< -o $@

$(BCDS_WIFILIB_RELEASE_OBJECT_PATH)/%.o: $(BCDS_WIFILIB_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
		-I . $(BCDS_WIFILIB_INCLUDES) $< -o $@

-include $(BCDS_WIFILIB_DEBUG_DEPENDENCY_FILES) $(BCDS_WIFILIB_RELEASE_DEPENDENCY_FILES)

.PHONY: clean		
clean:	
	rm -rf $(BCDS_WIFILIB_DEBUG_PATH)
	rm -rf $(BCDS_WIFILIB_RELEASE_PATH)
	
cdt:
	@echo "cdt"
	$(CC) $(BCDS_CFLAGS_COMMON) $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON) $(BCDS_WIFILIB_INCLUDES) $(BCDS_WIFILIB_DEFAULT_INCLUDE_PATH) -E -P -v -dD -c ${CDT_INPUT_FILE}
.PHONY: cdt

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_WIFILIB_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_WIFILIB_DEBUG_OBJECT_PATH)
	@echo $(BCDS_WIFILIB_DEBUG_OBJECT_FILES)
