/*----------------------------------------------------------------------------*/
/*
 * Copyright (C) Bosch Connected Devices and Solutions GmbH. 
 * All Rights Reserved. Confidential.
 *
 * Distribution only to people who need to know this information in
 * order to do their job.(Need-to-know principle).
 * Distribution to persons outside the company, only if these persons
 * signed a non-disclosure agreement.
 * Electronic transmission, e.g. via electronic mail, must be made in
 * encrypted form.
 */
/*----------------------------------------------------------------------------*/

/* Own public header*/
/* Own protected header*/
/* Own private header*/
/* System header */

/* Other headers*/
#include "FreeRTOS.h"
#include "semphr.h"
#include "integer.h"
#include "BCDS_Basics.h"
#include "ffconf.h"


/* Put the type and macro definitions here */
#define SUCCESS INT32_C(1)
#define FAILURE INT32_C(0)

/* Put constant and variable definitions here */

/* Put private function declarations here */

/* Put function implementations here */

/* @brief This function is called in f_mount function to create a new
 *  synchronization object, such as semaphore and mutex. When a zero is
 * returned, the f_mount function fails with FR_INT_ERR.
 */
int ff_cre_syncobj(BYTE vol, _SYNC_t *sobj)
{
    BCDS_UNUSED(vol);
    int ReturnValue = FAILURE;

    *sobj = xSemaphoreCreateMutex();
    if ((SemaphoreHandle_t)(*sobj) != NULL)
    {
        ReturnValue = SUCCESS;
    }
    else
    {
        ReturnValue = FAILURE;
    }
    return (ReturnValue);
}

/* @brief This function is called in f_mount function to delete a synchronization
 *  object that created with ff_cre_syncobj function. When a zero is
 *  returned, the f_mount function fails with FR_INT_ERR.
 */
int ff_del_syncobj(_SYNC_t sobj)
{
    int ReturnValue = FAILURE;

    if (sobj != NULL)
    {
        vSemaphoreDelete(sobj);
        ReturnValue = SUCCESS;
    }
    else
    {
        ReturnValue = FAILURE;
    }
    return (ReturnValue);
}

/* @brief This function is called on entering file functions to lock the volume.
 *  When a zero is returned, the file function fails with FR_TIMEOUT.
 */

int ff_req_grant(_SYNC_t sobj)
{
    int ReturnValue = FAILURE;

    if (NULL == sobj)
    {
        return (FAILURE);
    }
    if ((xSemaphoreTake(sobj, _FS_TIMEOUT)) == pdTRUE)
    {
        ReturnValue = SUCCESS;
    }
    else
    {
        ReturnValue = FAILURE;
    }
    return (ReturnValue);
}

/* @brief This function is called on leaving file functions to unlock the volume */
void ff_rel_grant(_SYNC_t sobj)
{
    xSemaphoreGive(sobj);
}
