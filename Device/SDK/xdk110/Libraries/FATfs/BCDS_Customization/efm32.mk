##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

#Build chain settings	
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = \
	$(DEPEDENCY_FLAGS) -std=c99 -Wall -Wextra -mcpu=cortex-m3 -mthumb \
	-ffunction-sections -fdata-sections \
	-Wa,-ahlms=$(@:.o=.lst)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON	
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g -DDEBUG
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON  = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -D BCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -Wno-unused-value -Wno-strict-aliasing
CFLAGS_DEBUG = $(BCDS_CFLAGS_DEBUG_COMMON) $(CFLAGS_COMMON)
CFLAGS_RELEASE = $(BCDS_CFLAGS_RELEASE_COMMON) $(CFLAGS_COMMON)

ARFLAGS = -cr

BCDS_FATFS_SOURCE_FILES = \
	$(BCDS_FATFS_SOURCE_PATH)/ff.c \
	
BCDS_PORTING_SOURCE_FILES = \
	$(BCDS_PORTING_SDCARD_SRC_PATH)/diskio.c \

# Objects
BCDS_FATFS_SOURCE_OBJECT_FILES1 = $(BCDS_PORTING_SOURCE_FILES:.c=.o)
BCDS_FATFS_SOURCE_OBJECT_FILES2 = $(patsubst $(BCDS_FATFS_SOURCE_PATH)/%.c, %.o, $(BCDS_FATFS_SOURCE_FILES))
BCDS_FATFS_SOURCE_OBJECT_FILES = $(BCDS_FATFS_SOURCE_OBJECT_FILES1) $(BCDS_FATFS_SOURCE_OBJECT_FILES2)

BCDS_FATFS_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_FATFS_DEBUG_OBJECT_PATH)/, $(BCDS_FATFS_SOURCE_OBJECT_FILES)) 

BCDS_FATFS_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_FATFS_DEBUG_OBJECT_PATH)/, $(BCDS_FATFS_SOURCE_OBJECT_FILES:.o=.d))
	
BCDS_FATFS_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_FATFS_RELEASE_OBJECT_PATH)/, $(BCDS_FATFS_SOURCE_OBJECT_FILES)) 

BCDS_FATFS_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_FATFS_RELEASE_OBJECT_PATH)/, $(BCDS_FATFS_SOURCE_OBJECT_FILES:.o=.d))
	
##################### Build Targets #########################

.PHONY: debug
debug: $(BCDS_FATFS_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_FATFS_RELEASE_LIB)
	
$(BCDS_FATFS_DEBUG_LIB): $(BCDS_FATFS_DEBUG_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build FATFS for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_FATFS_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_FATFS_RELEASE_LIB): $(BCDS_FATFS_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build FATFS for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_FATFS_RELEASE_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_FATFS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_FATFS_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
        -I . $(BCDS_FATFS_INCLUDES) $< -o $@

$(BCDS_FATFS_DEBUG_OBJECT_PATH)/%.o: %.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
        -I . $(BCDS_FATFS_INCLUDES) $< -o $@

$(BCDS_FATFS_RELEASE_OBJECT_PATH)/%.o: %.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
		-I . $(BCDS_FATFS_INCLUDES) $< -o $@
		
$(BCDS_FATFS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_FATFS_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
		-I . $(BCDS_FATFS_INCLUDES) $< -o $@

-include $(BCDS_FATFS_DEBUG_DEPENDENCY_FILES) $(BCDS_FATFS_RELEASE_DEPENDENCY_FILES)

.PHONY: clean		
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_FATFS_DEBUG_PATH) $(BCDS_FATFS_RELEASE_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_FATFS_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_FATFS_DEBUG_OBJECT_PATH)
	@echo $(BCDS_FATFS_DEBUG_OBJECT_FILES)
