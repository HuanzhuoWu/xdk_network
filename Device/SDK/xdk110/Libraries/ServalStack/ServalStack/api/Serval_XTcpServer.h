/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file       Serval_XTcpServer.h
 *
 * @brief      The interface description of a proprietary TCP Server based
 *             application protocol.
 *
 * @since 1.4
 */

#ifndef SERVAL_XTCPSERVER_H_
#define SERVAL_XTCPSERVER_H_

#include <Serval_Defines.h>

#if SERVAL_ENABLE_XTCP && SERVAL_ENABLE_XTCP_SERVER  /* if XTCP server is disabled -> no compilation */

#include <Serval_Types.h>
#include <Serval_XTcp.h>
#include <Serval_Callable.h>
#include <Serval_Tcp.h>

/**
 * @brief      Initializes the XTCP server including all needed used modules.
 *
 * @retval     RC_OK on successful
 * @retval     RC_XTCP_SERVER_INIT_ERROR if an error occurs while initializing
 *             the XTcp Server
 *
 * @par        Example
 *             Initialize an XTcp Server
 *
 * @code
 *             // Initialize a TCP Server
 *             if (RC_OK != XTcpServer_initialize())
 *             {
 *                 printf("Util and xTCP initialization failure!\n\r");
 *             }
 * @endcode
 */
retcode_t XTcpServer_initialize(void);

/**
 * @brief      This function opens a port and registers a handling function
 *             for this port.
 *
 * @param[in]  serverPort
 *             TCP Server secure port to listen on
 * @param[in]  appSndCallback
 *             Callback function for Send data
 * @param[in]  appRcvCallback
 *             Callback function for Received data
 *
 * @retval     RC_OK on success
 * @retval     RC_XTCP_LISTEN_ERROR if the TCP Server was not started correctly.
 *
 * @par        Example
 *             Start a TCP Server listener
 *
 * @code
 *             Ip_Port_T serverPort = Ip_convertIntToPort(9999);
 *
 *             // Start a TCP Server
 *             if (RC_OK == XTcpServer_listen(serverPort, appSendCallback, appReceiveCallback))
 *             {
 *                 printf("TCP Server Started...\n\r");
 *             }
 *             else
 *             {
 *                 printf("TCP Server not started!\n\r");
 *             }
 * @endcode
 *
 * @see        XTcpClient_connect()
 */
retcode_t XTcpServer_listen(Ip_Port_T serverPort, AppXTcpCallback_T appSndCallback, AppXTcpCallback_T appRcvCallback);

#if SERVAL_ENABLE_TLS_SERVER
/**
 * @brief      This function opens a secure port and registers a handling function
 *             for this port.
 *
 * @param[in]  serverPort
 *             TCP Server secure port to listen on
 * @param[in]  appSndCallback
 *             Callback function for Send data
 * @param[in]  appRcvCallback
 *             Callback function for Received data
 *
 * @retval     RC_OK on success
 * @retval     RC_XTCP_LISTEN_ERROR if the TCP Server was not started correctly.
 *
 * @par        Example
 *             Start a TCP Secure Server
 *
 * @code
 *             Ip_Port_T serverPort = Ip_convertIntToPort(9999);
 *
 *             // Start a TCP Server
 *             if (RC_OK == XTcpServer_listenSecure(serverPort, appSendCallback, appReceiveCallback))
 *             {
 *                 printf("TCP Secure Server Started...\r\n");
 *             }
 *             else
 *             {
 *                 printf("TCP Secure Server not started!\r\n");
 *             }
 * @endcode
 *
 * @see        XTcpClient_connect()
 */
retcode_t XTcpServer_listenSecure(Ip_Port_T serverPort, AppXTcpCallback_T appSndCallback, AppXTcpCallback_T appRcvCallback);
#endif

/**
 * @brief      This function stops an active TCP Server.
 *             When TCP Server is enabled, this function stops the listening by
 *             deleting the socket. After invoking this function the socket used
 *             by this module is invalid, and must not be used for any further
 *             communication unless reinitialized.
 *
 * @param[in]  port
 *             Server Port to be unlisten
 *
 * @retval     RC_OK on success
 * @retval     RC_XTCP_LISTEN_ERROR if the listener to be closed is invalid or close function fails
 *
 * @par        Example
 *             Stop a TCP Server (unlisten)
 * @code
 *             Ip_Port_T serverPort1 = Ip_convertIntToPort(9999);
 *
 *             // Close a TCP Server
 *             if (RC_OK == XTcpServer_unlisten(serverPort1))
 *             {
 *                 printf("TCP Server was stopped ...\n\r");
 *             }
 *             else
 *             {
 *                printf("TCP Server was not stopped!\n\r");
 *             }
 * @endcode
 */
retcode_t XTcpServer_unlisten(Ip_Port_T port);


#endif /* SERVAL_ENABLE_XTCP && SERVAL_ENABLE_XTCP_SERVER */

#endif /* SERVAL_XTCPSERVER_H_ */
