/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_StringDescr.h
 * @brief This module defines a string descriptor type.
 * @ingroup utilgroup
 *
 * This interface provides type definitions, data types and functions to handle
 * string descriptors.
 */

#ifndef SERVAL_STRINGDESCRIPTOR_H_
#define SERVAL_STRINGDESCRIPTOR_H_

#include <Serval_Types.h>

/**
 * This structure describes a string by a pointer to the start location
 * and its length.
 *
 * @see struct StringDescr_S
 */
typedef struct StringDescr_S StringDescr_T;

/**
 * This structure describes a string by a pointer to the start location
 * and its length.
 */
struct StringDescr_S
{
	/* Starting point of the string */
	const char *start;
	/* Length of the string */
	int length;
};


/**
 * This function is called to initialize and prepare a instance of string
 * descriptor. This function must be called before a string descriptor is used.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be used.
 * It has to be a valid pointer.
 *
 * @param[in] start_ptr
 * This gives starting point of the string.
 *
 * @param[in] len
 * This gives length of the string.
 */
inline static void StringDescr_set(
    StringDescr_T *str_ptr, char const * start_ptr, int len)
{
	str_ptr->start = start_ptr;
	str_ptr->length = len;
}


/**
 * This function checks the state of the string descriptor, and sees if the
 * string descriptor is valid or not.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * string descriptor to be checked.
 * It has to be a valid pointer.
 *
 * @return
 * true if string descriptor is valid false if it is not.
 */
inline static bool StringDescr_isValid(const StringDescr_T *str_ptr)
{
	return (str_ptr != NULL && str_ptr->start != NULL);
}


/**
 * This function is called to fetch start and length associated with the
 * given instance of string descriptor. The function StringDescr_set() should
 * have been called before calling this function.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * string descriptor been used.
 * It has to be a valid pointer.
 *
 * @param[in] start_pptr
 * This will contain starting point of the string when the function returns.
 *
 * @param[in] len_ptr
 * This will contain length of the string when the function returns.
 */
inline static void StringDescr_get(const StringDescr_T *str_ptr,
                                   const char **start_pptr, int *len_ptr)
{
	assert(str_ptr != NULL);
	assert(str_ptr->start != NULL);
	assert(start_pptr != NULL);
	assert(len_ptr != NULL);

	*start_pptr = str_ptr->start;
	*len_ptr = str_ptr->length;
}

/**
 * This function returns the character at a specified position in the string
 * associated with the given instance of string descriptor.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * string descriptor been used.
 * It has to be a valid pointer.
 *
 * @param[in] pos
 * This will contain starting point of the string when the function returns.
 *
 * @return
 * Character in the given string at the desired position.
 */
inline static char StringDescr_charAt(const StringDescr_T *str_ptr, int pos)
{
	assert(str_ptr != NULL);
	assert(str_ptr->start != NULL);
	assert(pos >= 0);
	assert(pos < str_ptr->length);

	return str_ptr->start[pos];
}


/**
 * This function returns the total length of the entire string described by the
 * given String Descriptor.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * string descriptor been used.
 * It has to be a valid pointer.
 *
 * @return
 * Total length of the string.
 */
inline static int StringDescr_getLength(const StringDescr_T *str_ptr)
{
	assert(str_ptr != NULL);
	assert(str_ptr->start != NULL);

	return str_ptr->length;
}


/**
 * This function is called to record the description information (i.e. start
 * address and length) of the given string in the given String Descriptor. The
 * content characters will not be copied.
 *
 * @param[in] dest_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @param[in] cstr
 * The string which should be described by the String Descriptor.
 */
inline static void StringDescr_wrap(StringDescr_T *dest_ptr, const char *cstr)
{
	assert(dest_ptr != NULL);
	assert(cstr != NULL);

	dest_ptr->start = cstr;
	dest_ptr->length = strlen(cstr);
}


/**
 * This function is called to clone a String Descriptor which means that the
 * description information of src will be cloned to dest. The clone operation
 * considers only a particular segment of the source String Descriptor. The
 * content characters will not be copied.
 *
 * @param[out] dest_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @param[in] pos
 * The start position of the segment of src which the clone operation should
 * consider.
 *
 * @param[in] n
 * The length of the segment of src  which the clone operation should
 * consider.
 */
inline static void StringDescr_cloneSegment(StringDescr_T *dest_ptr,
        const StringDescr_T *src_ptr, int pos, int n)
{
	assert(dest_ptr != NULL);
	assert(src_ptr != NULL);
	assert(src_ptr->length == 0 || src_ptr->start != NULL);
	assert(pos >= 0 && n >= 0);
	assert(pos + n <= src_ptr->length);

	dest_ptr->start = src_ptr->start + pos;
	int len = n < (src_ptr->length - pos) ? n : src_ptr->length - pos;
	dest_ptr->length = len;
}


/**
 * This function is called to clone a String Descriptor which means that the
 * description information of src will be cloned to dest. The content characters
 * will not be copied.
 *
 * @param[out] dest_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @see
 * StringDescr_cloneSegment()
 */
inline static void StringDescr_clone(StringDescr_T *dest_ptr,
                                     const StringDescr_T *src_ptr)
{
	StringDescr_cloneSegment(dest_ptr, src_ptr, 0, src_ptr->length);
}


/**
 * This function is called to clone a String Descriptor which means that the
 * description information of src will be cloned to dest. The clone operation
 * considers only the segment of the source String Descriptor starting at the
 * specified position. The content characters will not be copied.
 *
 * @param[out] dest_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @param[in] pos
 * The start position of the segment of src which the clone operation should
 * consider.
 */
inline static void StringDescr_cloneFrom(StringDescr_T *dest_ptr,
        const StringDescr_T *src_ptr, int pos)
{
	StringDescr_cloneSegment(dest_ptr, src_ptr, pos,
	                         src_ptr->length - pos);
}


/**
 * This function is called to copy a segment of the string described by a
 * string descriptor into a char buffer.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @param[out] buf_ptr
 * Destination character buffer, This must be valid.
 *
 * @param[in] pos
 * The position at which the copy operation shout start
 *
 * @param[in] n
 * The number of characters which should be maximally copied.
 */
inline static void StringDescr_copySegment(
    const StringDescr_T *src_ptr, char *buf_ptr, int pos, int n)
{
	assert(src_ptr != NULL);
	assert(buf_ptr != NULL);
	assert(pos >= 0);
	//below allow pos == 0 in case we have an empty StringDescr
	assert(pos == 0 || pos < src_ptr->length);
	if ( pos + n > src_ptr->length )
	{
		n = src_ptr->length - pos;
	}

	memcpy(buf_ptr, src_ptr->start + pos, n);
}


/**
 * This function is called to copy the string which is described by a string
 * descriptor into a char buffer.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @param[out] buf_ptr
 * Destination character buffer, This must be valid.
 */
inline static void StringDescr_copy(
    const StringDescr_T *src_ptr, char *buf_ptr)
{
	StringDescr_copySegment(src_ptr, buf_ptr, 0, src_ptr->length);
}


/**
 * This function is called to copy the string which is described by a string
 * descriptor into a char buffer from a given offset.
 *
 * @param[in] src_ptr
 * Reference to a StringDescr_T object which identifies the instance of source
 * string descriptor.
 *
 * @param[out] buf_ptr
 * Destination character buffer. This must be valid.
 *
 * @param[in] pos
 * The position at which the copy operation shout start
 *
 * @see
 * StringDescr_copySegment()
 */
inline static void StringDescr_copyFrom(
    const StringDescr_T *src_ptr, char *buf_ptr, int pos)
{
	StringDescr_copySegment(src_ptr, buf_ptr, pos, src_ptr->length);
}


/**
 * This function finds and returns last occurrence of a character inside a
 * String Descriptor.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be searched.
 *
 * @param[in] c
 * Character to look for.
 *
 * @return
 * Position of the character or -1 if character is not found.
 */
int StringDescr_findLastChar(const StringDescr_T *str_ptr, char c);


/**
 * This function finds and returns first occurrence of a character inside a
 * String Descriptor.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be searched.
 *
 * @param[in] offset
 * The position at which the search is starting.
 *
 * @param[in] c
 * Character to look for.
 *
 * @return
 * Position of the character or -1 if character is not found.
 */
int StringDescr_findFirstChar(const StringDescr_T *str_ptr, int offset, char c);


/**
 * This function compares a segment of string description with a string
 * starting at a certain position, and returns the result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 * @param[in] pos
 * Starting position inside the string description
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @param[in] n
 * The number of characters which should be considered in the comparison
 *
 * @return
 * true if found equal, otherwise false.
 */
bool StringDescr_compareSegment(const StringDescr_T *str_ptr, int pos,
                                const char *cstr, int n);


/**
 * This function compares a string description with a char string at a certain
 * position, and returns the result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 * @param[in] pos
 * Starting position inside the string description
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @return
 * true if found equal, otherwise false.
 */
inline static bool StringDescr_compareFrom(
    const StringDescr_T *str_ptr, int pos, const char *cstr)
{
	assert(str_ptr != NULL);
	assert(cstr != NULL);

	int len = strlen(cstr);

	if(len != str_ptr->length - pos)
	{
		return FALSE;
	}

	return StringDescr_compareSegment(str_ptr, pos, cstr, len);
}

/**
 * This function compares a string description with a char string, and returns
 * the result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @return
 * true if found equal, otherwise false.
 */
inline static bool StringDescr_compare(
    const StringDescr_T *str_ptr, const char *cstr)
{
	return StringDescr_compareFrom(str_ptr, 0, cstr);
}


/**
 * This function compares a segment of string description with @c n characters
 * of a string starting at a certain position, ignoring case, and returns the
 * result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 * @param[in] pos
 * Starting position inside the string description
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @param[in] n
 * the number of character of cstr to be used for the compare
 *
 * @return
 * true if found equal, otherwise false.
 *
 * @see StringDescr_compareSegmentNoCase()
 */
bool StringDescr_compareSegmentNoCase(
    StringDescr_T const *str_ptr, int pos, char const *cstr, int n);


/**
 * This function compares a segment of string description with a char string
 * starting at a certain position, ignoring case, and returns the result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 * @param[in] pos
 * Starting position inside the string description
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @return
 * true if found equal, otherwise false.
 *
 * @see StringDescr_compareSegmentNoCase()
 */
static inline bool StringDescr_compareFromNoCase(
    StringDescr_T const *str_ptr, int pos, char const *cstr)
{
	assert(str_ptr != NULL);
	assert(str_ptr->start != NULL);
	assert(cstr != NULL);

	int len = strlen(cstr);

	if(len != str_ptr->length - pos)
	{
		return FALSE;
	}

	return StringDescr_compareSegmentNoCase(str_ptr, pos, cstr, len);
}

/**
 * This function compares a segment of string description with a char string
 * starting at a certain position, ignoring case, and returns the result.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of string
 * descriptor to be compared.
 *
 *
 * @param[in] cstr
 * C string to be compared.
 *
 * @return
 * true if found equal, otherwise false.
 */
static inline bool StringDescr_compareNoCase(
    const StringDescr_T *str_ptr, const char *cstr)
{
	return StringDescr_compareFromNoCase(str_ptr, 0, cstr);
}

/**
 * This function parses the given string descriptor for an unsigned integer.
 * It does not handle leading or trailing characters and will produce an error
 * if those are found.
 *
 * @param[in]  str_ptr
 *             Reference to a StringDescr_T object which identifies the instance of string
 *             descriptor to be compared.
 *
 * @param[out] value
 *             the parsed value
 *
 * @return     true if parsing went ok, false otherwise
 *
 * @since 1.4
 */
static inline bool StringDescr_parseUInt(const StringDescr_T* str_ptr, uint32_t* value )
{
	int i;
	*value = 0;
	for (i = 0 ; i < str_ptr->length ; ++i)
	{
		if ( str_ptr->start[i] < '0' || str_ptr->start[i] > '9' )
		{
			return false;
		}

		*value = 10 * (*value) + (str_ptr->start[i] - '0');
	}
	return true;
}

#endif /* SERVAL_STRINGDESCRIPTOR_H_ */
