/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Resources.h
 * @brief Lists the network sockets required by the stack
 *
 * This file advertise the network sockets required by the stack. These figures
 * are automatically consistent with the stack configuration. They do not need
 * to be adjusted manually if the stack configuration is changed.
 *
 * Exception: if new protocols are added which require additional sockets
 * the defines below must be adapted.
 */

#ifndef SERVAL_RESOURCES_H_
#define SERVAL_RESOURCES_H_

#include <Serval_Defines.h>
#include <Serval_Http.h>
#include <Serval_Log.h>
#include <Serval_Http.h>
#include <Serval_Rest.h>
#include <Serval_XTcp.h>

/**
 * Number of needed TCP listen sockets.
 */
#define SERVAL_NUM_TCP_LISTENER \
    (  SERVAL_ENABLE_DPWS + \
       SERVAL_ENABLE_WEBSERVER + \
       SERVAL_ENABLE_REST_SERVER * SERVAL_ENABLE_REST_HTTP_BINDING + \
       SERVAL_ENABLE_XTCP_CLIENT \
      )

/**
 * Number of TCP data sockets needed in parallel.
 */
#define SERVAL_NUM_TCP_SOCKETS \
    (SERVAL_HTTP_MAX_NUM_SESSIONS * SERVAL_ENABLE_HTTP) + \
    SERVAL_XTCP_MAX_NUM_CONNECTIONS

/**
 * Number of needed UDP sockets.
 */
#define SERVAL_NUM_UDP_SOCKETS  \
    (  SERVAL_ENABLE_DPWS + \
       SERVAL_ENABLE_COAP_SERVER + \
       SERVAL_ENABLE_COAP_SERVER * SERVAL_ENABLE_DTLS_SERVER + \
       SERVAL_ENABLE_COAP_CLIENT + \
       SERVAL_ENABLE_COAP_CLIENT * SERVAL_ENABLE_DTLS_CLIENT + \
       SERVAL_ENABLE_XUDP + \
       SERVAL_ENABLE_NETWORK_LOG)


#endif /* SERVAL_RESOURCES_H_ */
