/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file       Serval_XTcp.h
 * @ingroup    xtcpgroup
 *
 * @brief      The interface description of a proprietary TCP based application
 *             protocol both for client and server.
 *
 * @since 1.4
 */

#ifndef SERVAL_XTCP_H_
#define SERVAL_XTCP_H_

#include <Serval_Defines.h>

#if ! SERVAL_ENABLE_XTCP
#ifndef SERVAL_XTCP_MAX_NUM_CONNECTIONS
#define SERVAL_XTCP_MAX_NUM_CONNECTIONS                 0
#endif
#ifndef SERVAL_XTCP_MAX_NUM_CLIENT_INSTANCE
#define SERVAL_XTCP_MAX_NUM_CLIENT_INSTANCE             0
#endif
#ifndef SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT
#define SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT     0
#endif
#endif

#if SERVAL_ENABLE_XTCP

#include <Serval_Msg.h>

/**
 * This is the maximal number of parallel XTCP connections either initiated by
 * the application or opened by the server. This should be set to the maximum
 * number of communication peers that are expected by the application.
 *
 * Note: This should not exceed the number of TCP sockets provided by the platform
 */
#ifndef SERVAL_XTCP_MAX_NUM_CONNECTIONS
#define SERVAL_XTCP_MAX_NUM_CONNECTIONS                 10
#endif

/**
 * This is the maximal number of parallel XTCP client instance initiated by
 * the application.
 *
 * Note: This summed with SERVAL_XTCP_MAX_NUM_SERVER_INSTANCE should not be set
 *       greater than the SERVAL_XTCP_MAX_NUM_CONNECTIONS.
 */
#ifndef SERVAL_XTCP_MAX_NUM_CLIENT_INSTANCE
#define SERVAL_XTCP_MAX_NUM_CLIENT_INSTANCE             3
#endif

/**
 * This is the maximal number of parallel XTCP server instance initiated by
 * the application.
 *
 * Note: This summed with SERVAL_XTCP_MAX_NUM_CLIENT_INSTANCE should not be set
 *       greater than the SERVAL_XTCP_MAX_NUM_CONNECTIONS.
 */
#ifndef SERVAL_XTCP_MAX_NUM_SERVER_INSTANCE
#define SERVAL_XTCP_MAX_NUM_SERVER_INSTANCE             3
#endif

/**
 * Number of connections reserved to be used as client connections.
 * Obviously, this must be smaller than SERVAL_XTCP_MAX_NUM_CONNECTIONS
 */
#ifndef SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT
#define SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT     3
#endif

#if SERVAL_ENABLE_XTCP_CLIENT && ( SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT > SERVAL_XTCP_MAX_NUM_CONNECTIONS )
#error "More connections are reserved for client use than are available in the system"
#endif

#if SERVAL_ENABLE_XTCP_SERVER && SERVAL_ENABLE_XTCP_CLIENT && ( SERVAL_XTCP_RESERVED_CONNECTIONS_FOR_CLIENT >= SERVAL_XTCP_MAX_NUM_CONNECTIONS )
#error "All connections are reserved for client used. Server would not be able to function properly"
#endif

/**
 * Define the timeout in ms after which the XTCP server will close an idle connection.
 * It should be a multiple of SERVAL_RESOURCE_MONITOR_PERIODE.
 * Suggested is a value 2-5 times as large as SERVAL_RESOURCE_MONITOR_PERIODE.
 */
#ifndef SERVAL_XTCP_SERVER_CONNECTION_IDLE_TIMEOUT
#define SERVAL_XTCP_SERVER_CONNECTION_IDLE_TIMEOUT      60000
#endif

/**
 * This is the length of the payload buffer of a XTCP message.
 */
#ifndef SERVAL_XTCP_MAX_LEN_APP_PAYLOAD
#define SERVAL_XTCP_MAX_LEN_APP_PAYLOAD                 256
#endif

/**
 * A data type representing a callback function pointer for application. The
 * application uses such callback functions in order to be notified about the
 * sending and receiving result of outgoing/incoming messages.
 */
typedef void (*AppXTcpCallback_T)(uint8_t sessionId, Msg_T *msg_ptr, retcode_t status);

/**
 * @brief      Pushes the application data over an open TCP connection
 *
 * @param[in]  sessionId
 *             Identification number provided by the callback function
 *
 * @param[in]  payload_ptr
 *             A pointer to the buffer which contains the payload of a XTCP
 *             message.
 *
 * @param[in]  payloadLen
 *             The length of the payload. The payload must not be longer than
 *             SERVAL_XTCP_MAX_LEN_APP_PAYLOAD.
 *
 * @retval     RC_OK on success
 * @retval     RC_XTCP_PAYLOAD_TOO_LARGE if the length of the payload is larger
 *             than SERVAL_XTCP_MAX_LEN_APP_PAYLOAD
 * @retval     RC_XTCP_SESSION_NOT_ACTIVE when trying to push data to a session
 *             that does not exist
 * @retval     RC_XTCP_OVERLOADED if no free message struct is available to
 *             accept the new sending job
 * @retval     RC_XTCP_MSG_FACTORY_OVERFLOW if there is a problem to construct
 *             the message payload to be sent
 * @retval     RC_XTCP_SENDING_ERROR if an error occurs while trying to send
 *             the message
 *
 * @par        Example
 *             Push data from a TCP Server to a TCP Client or from TCP Client to
 *             a TCP Server.
 * @code
 *             retcode_t rc = RC_OK;
 *             uint8_t * sendPayload = (uint8_t *)"Message";
 *             unsigned int sendPayloadLen = strlen((char *)sendPayload);
 *             uint8_t sessionId = 1;
 *
 *             // Push payload
 *             rc = XTcp_push(sessionId, sendPayload, sendPayloadLen);
 * @endcode
 *
 * @warning    The session ID must exist in order to push data to a client or a
 *             server.
 *
 * @see        XTcpClient_connect(), XTcpServer_listen(), XTcp_getPayload()
 */
retcode_t XTcp_push(uint8_t sessionId, uint8_t const *payload_ptr,
                    unsigned int payloadLen);

/**
 * @brief      This function is called to get the payload of the received
 *             message. It provides the reference to the payload and its length
 *             as output parameters.
 *
 * @param[in]  msg_ptr
 *             Reference to an object which identifies the instance of parsing
 *             context which should be used for parsing.
 *             It has to be a valid pointer.
 * @param[out] payload_pptr
 *             An output parameter which references to the payload of the TCP
 *             message.
 *
 * @param[out] payloadLen_ptr
 *             An output parameter for the length of the payload of the parsed
 *             TCP message.
 *
 * @retval     none.
 *
 * @par        Example
 *             Get payload from a TCP Server or from a TCP Client.
 *             This must be used in the receive callback function.
 * @code
 *             // The Application Receive Callback must be set when calling
 *             // function to start TCP Server or when calling function to
 *             // connect a TCP Client
 *
 *             void appReceiveCallback(uint8_t sessionId, Msg_T *msg_ptr, retcode_t status)
 *             {
 *                 uint8_t *rcvPayload;
 *                 unsigned int rcvPayloadLen;
 *
 *                 XTcp_getPayload(msg_ptr, &rcvPayload, &rcvPayloadLen);
 *
 *                 printf("Payload (length = %d): ", rcvPayloadLen);
 *             }
 * @endcode
 *
 * @see        XTcpClient_connect(), XTcpServer_listen(), XTcp_push()
 */
void XTcp_getPayload(Msg_T *msg_ptr, uint8_t **payload_pptr,
                     unsigned int *payloadLen_ptr);

/**
 * @brief      This function identifies the connection partner
 *             (IP address, port) for a given socket handle.
 *
 * @param[in]  msg_ptr
 *             Reference to a object which identifies the instance of parsing
 *             context which should be used for parsing. It has to be a valid
 *             pointer.
 *
 * @param[out] ipAddress_ptr
 *             IP address of the connection partner
 *
 * @param[out] port_ptr
 *             TCP port of the connection partner
 *
 * @retval     none.
 *
 * @par        Example
 *             Get peer information.
 *             This must be used in the receive callback function.
 * @code
 *             void appReceiveCallback(uint8_t sessionId, Msg_T *msg_ptr, retcode_t status)
 *             {
 *                 // Get peer information from the message pointer
 *                 Ip_Address_T peerIpAddress;
 *                 uint16_t peerPort;
 *                 char peerIpAddressString[15];
 *
 *                 XTcp_getPeer(msg_ptr, &peerIpAddress, &peerPort);
 *                 // Convert address to string
 *                 rc = Ip_convertAddrToString(&peerIpAddress, peerIpAddressString);
 *
 *                 if(0 > rc)
 *                 {
 *                     printf("IP conversion to string has failed!\r\n");
 *                 }
 *
 *                 printf("[TCE] : Peer IP is : %s:%d\r\n", peerIpAddressString, peerPort);
 *             }
 * @endcode
 */
void XTcp_getPeer(Msg_T *msg_ptr, Ip_Address_T *ipAddress_ptr,
                  uint16_t *port_ptr);

/**
 * @brief      This function closes an active session. Can be called by either a
 *             TCP Client or Server
 *
 * @param[in]  sessionId
 *             Identification number provided by the callback function
 *
 * @retval     RC_OK on success
 * @retval     RC_XTCP_SESSION_NOT_ACTIVE when trying to close a session that
 *             does not exist
 * @retval     RC_XTCP_SOCKET_ACTIVE when trying to close a session that
 *             has not finished receiving/sending data
 * @retval     RC_XTCP_SOCKET_ERROR if the socket to be closed is invalid or
 *             close function fails
 *
 * @par        Example
 *             Close an active session
 * @code
 *             uint8_t sessionId = 0;
 *
 *             if (RC_OK == XTcp_close(sessionId))
 *             {
 *                 printf("TCP Server and Client(s) stopped ...\n\r");
 *             }
 *             else
 *             {
 *                printf("TCP Server not stopped!\n\r");
 *             }
 * @endcode
 *
 * @warning    The session ID must exist in order to close it.
 *
 */
retcode_t XTcp_close(uint8_t sessionId);

/**
 * @brief      This function verifies if a session is secured or not
 *
 * @param[in]  sessionId
 *             Identification number provided by the callback function
 * @param[in]  isSecured
 *             Pointer to secured/non-secured variable passed by the application
 *
 * @retval     RC_OK when success
 * @retval     RC_XTCP_SESSION_NOT_ACTIVE when no session was found
 *
 * @par        Example
 *             Check whether a connection is secured or not inside a callback
 *             function
 * @code
 *             void appReceiveCallback(uint8_t sessionId, Msg_T *msg_ptr, retcode_t status)
 *             {
 *                 retcode_t rc;
 *
 *                 rc = XTcp_isSecured(sessionId, &isSecure);
 *             }
 * @endcode
 *
 */
retcode_t XTcp_isSecured(uint8_t sessionId, bool * isSecured);

#endif /* SERVAL_ENABLE_XTCP */

#endif /* SERVAL_XTCP_H_ */
