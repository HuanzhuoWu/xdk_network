/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file    Serval_XUdp.h
 * @brief   The interface description of an proprietary UDP based application
 *          protocol.
 * @ingroup xudpgroup
 *
 * This UDP based application protocol enabled the application to push
 * application specific data over UDP to some target receiver. The application
 * data is assigned as raw message as well as the payload which is sent to the
 * application by a communication partner. The length of the payload has to be
 * provided by the application.
 */

#ifndef SERVAL_XUDP_H_
#define SERVAL_XUDP_H_

#include <Serval_Defines.h>

#if SERVAL_ENABLE_XUDP

#include <Serval_Types.h>
#include <PIp.h>
#include <Serval_Msg.h>


/*
 *******************************************************************************
 * Parameters for XUDP application layer protocol
 *******************************************************************************
 */

/**
 * This is the length of the payload buffer of a XUDP message.
 */
#ifndef SERVAL_XUDP_MAX_LEN_APP_PAYLOAD
#define SERVAL_XUDP_MAX_LEN_APP_PAYLOAD    256
#endif


/**
 * It initializes the XUDP protocol including all needed modules.
 *
 * @return
 * RC_OK on success or <br>
 * RC_XUDP_INIT_ERROR on failure.
 */
retcode_t XUdp_initialize(void);

/**
 * It starts listening on the specified port number for UDP packets.
 *
 * @param[in] serverPort
 * The port which is used to send and receive messages. It has to be a valid
 * port number (!=0).
 *
 * @param[in] appRecCallback
 * The application callback function which should be called when a message
 * is received. It has to be a valid pointer.
 *
 * @return
 * RC_OK on success or <br>
 * RC_XUDP_SOCKET_ACTIVE if listening was already started before.
 * RC_XUDP_SOCKET_ERROR if opening the UDP socket fails.
 */
retcode_t XUdp_start(Ip_Port_T serverPort, AppCallback_T appRecCallback);


/**
 * It pushes the application data via UDP to the assigned destination IP
 * address and port.
 *
 * @param[in] destIp_ptr
 * The reference of the destination IP address.
 *
 * @param[in] destPort
 * The port of the destination.
 *
 * @param[in] payload_ptr
 * A pointer to the buffer which contains the payload of a XUDP message.
 *
 * @param[in] len
 * The length of the payload. The payload must not be longer than
 * SERVAL_XUDP_MAX_LEN_APP_PAYLOAD.
 *
 * @param[in] appCallback
 * The application callback which will be invoked after finishing the sending
 * in order to notify the application about success or failure of the sending.
 *
 * @param[out] msg_pptr
 * The reference to the message which was sent. The reference must not be used
 * after the application callback (which is invoked after finishing sending)
 * has finished its task.
 *
 * @return
 * RC_OK on success <br>
 * RC_XUDP_PAYLOAD_TOO_LARGE if the length of the payload is larger than
 * SERVAL_XUDP_MAX_LEN_APP_PAYLOAD
 * RC_XUDP_ASLEEP if the stack, and consequently XUDP, is not active.
 * RC_XUDP_OVERLOADED if no free message struct is available to accept the new
 * sending job
 * RC_XUDP_MSG_FACTORY_OVERFLOW if there is a problem to construct the message
 * payload to be sent
 * RC_XUDP_SENDING_ERROR if an error occurs while trying to send the message
 *
 * @see OperationControl_isActive()
 */
retcode_t XUdp_push(Ip_Address_T *destIp_ptr, uint16_t destPort,
                    uint8_t const *payload_ptr, unsigned int len, AppCallback_T appCallback,
                    Msg_T **msg_pptr);


/**
 * It returns the reference to the payload and the payload length of a XUDP
 * message within a message.
 *
 * @param[in] msg_ptr
 * A pointer to a message which contains a XUDP message. It has to be a valid
 * pointer.
 *
 * @param[out] payload_pptr
 * The reference to the payload of the XUDP message.
 *
 * @param[out] payloadLen_ptr
 * The value of the payload length of the XUDP message.
 */
void XUdp_getXUdpPayload(Msg_T *msg_ptr, uint8_t **payload_pptr,
                         unsigned int *payloadLen_ptr);


/**
 * It returns the IP address and the port of a XUDP message within a message.
 *
 * @param[in] msg_ptr
 * A pointer to a message which contains a XUDP message. It has to be a valid
 * pointer.
 *
 * @param[out] ipAddress_ptr
 * The reference to the IP address of a XUDP message within a message.
 *
 * @param[out] port_ptr
 * The port of the XUDP message within a message.
 */
void XUdp_getXUdpPeer(Msg_T *msg_ptr, Ip_Address_T *ipAddress_ptr,
                      uint16_t *port_ptr);


#endif

#endif /* SERVAL_XUDP_H_ */
