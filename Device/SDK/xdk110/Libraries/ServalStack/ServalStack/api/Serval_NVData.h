/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
* @file  Serval_NVData.h
* @brief It describes the struct for the non-volatile data data of the stack.
* @ingroup utilgroup
*
* In some applications, the device needs to periodically switch off and go to
* a deep-sleep state, where RAM and CPU registers are not retained.
* The stack supports such a duty-cycling mechanism by providing API functions
* to put the stack to sleep and to wake it up. In this case the stack must
* save its status into the non-volatile memory before going to sleep and to
* recover it when it wakes up again. This header specifies the non-volatile
* status of the stack in the struct NVData_S.
*
* @see NVData_T, NVData_Func_T, OpControl_trySleep, OpControl_wakeUp
*/

#ifndef SERVAL_NVDATA_H_
#define SERVAL_NVDATA_H_

#include <Serval_Defines.h>
#include <Serval_Types.h>
#include <Serval_Dpws.h>

/**
 * This struct represents the non-volatile data of the stack.
 *
 * This struct represents the status of the stack which should be retained over
 * a deep-sleep state in order to support correct duty-cycling. A non-volatile
 * memory block must be available which is at least as large as this struct.
 *
 * @see NVData_T, NVData_Func_T
 */
struct NVData_S
{
	uint8_t isRead;
#if SERVAL_ENABLE_DPWS
	uint8_t dpwsSequenceMsgNumber;
	Uuid_T dpwsMsgUuidCounter;
	DpwsSubscriber_T dpwsSubscriberList[SERVAL_DPWS_MAX_NUMBER_SUBSCRIBERS];
#endif /* SERVAL_ENABLE_DPWS */

#if SERVAL_ENABLE_COAP
	/**
	 * This represents Message ID for the last CoAP message
	 * The Message ID is 16 bit long.
	 */
	uint16_t coapMsgID;
	/**
	 * This represents Token for the last CoAP request. This for implementation
	 * is fixed to two bytes for space optimization.
	 */
	uint16_t coapRequestToken;
#endif /* SERVAL_ENABLE_COAP */
};

#endif /* SERVAL_NVDATA_H_ */
