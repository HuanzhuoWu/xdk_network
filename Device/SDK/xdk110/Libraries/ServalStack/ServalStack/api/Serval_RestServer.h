/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_RestServer.h
 * @brief REST Server Module
 * @ingroup restgroup
 *
 * The interface description of the REST Server.
 */
#ifndef SERVAL_RESTSERVER_H_
#define SERVAL_RESTSERVER_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_REST_SERVER

#include <Serval_Rest.h>
#if SERVAL_ENABLE_REST_HTTP_BINDING && SERVAL_ENABLE_HTTP_AUTH
#include <Serval_Http.h>
#endif

/** Defines if the secure REST server instance should be enabled. */
#if (SERVAL_ENABLE_REST_COAP_BINDING && SERVAL_ENABLE_DTLS_SERVER) || \
    (SERVAL_ENABLE_REST_HTTP_BINDING && SERVAL_ENABLE_TLS_SERVER)
#define SERVAL_ENABLE_REST_SECURE_SERVER   1
#else
#define SERVAL_ENABLE_REST_SECURE_SERVER   0
#endif
#if SERVAL_ENABLE_REST_HTTP_BINDING && SERVAL_ENABLE_TLS_SERVER
//#error HTTP Server with TLS is currently not supported
#endif

#if SERVAL_ENABLE_REST_SECURE_SERVER
#include <Serval_Security.h>
#endif

#if SERVAL_ENABLE_REST_SERVER_OBSERVATION
#if (!SERVAL_ENABLE_REST_COAP_BINDING)
#warning SERVAL_ENABLE_REST_SERVER_OBSERVATION defined without CoAP binding support
#endif
#include <Serval_Coap.h>
#if (!SERVAL_ENABLE_COAP_OBSERVE)
#error SERVAL_ENABLE_COAP_OBSERVE must be enabled for REST Observations
#endif
#endif

/**
 * This data type represents a REST service.
 *
 * @see struct RestSettings_S
 */
typedef struct RestSettings_S RestSettings_T;

/**
 * This data structure holds the minimum data a REST resource must have.
 */
struct RestResource_S
{
	/** The url of the resource */
	char const *url;

	/** A description of the types of this resource. A space has to be used to
	 * separate multiple types */
	char const *type;

	/** A description of the interfaces this resource supports. A space has to
	 * be used to separate multiple interfaces */
	char const *interface;

	/** A function pointer to the handler function of this resource. This handler
	 * will be invoked by the REST Server on an incoming request to the url or
	 * on sub url's of this resource. */
	retcode_t (*handle)(Msg_T *);

#if SERVAL_ENABLE_REST_SERVER_OBSERVATION
	/** An internal pointer to handle observations.
	 * if required, initialize to NULL.
	 * A value different from NULL indicates that an observation for this resource exists.
	 * This field is mutable.
	 * @since 1.4
	 */
	uint8_t observer;

	/*
	 * A callback pointer to CoAP notification function.
	 * @since 1.4
	 */
	Callable_T notificationCallback;
#endif
};

/**
 * A data type for a resource meta data.
 * @see struct RestResource_S
 */
typedef struct RestResource_S RestResource_T;


/**
 * This is function pointer data type for visitor functions, which are used
 * along with the traversal function of the RESTful resources.
 * @see RestServer_TraversalFunc_T
 */
typedef bool (*RestServer_VisitorFunc_T)(
    RestResource_T const *, void *);

/**
 * This is a function pointer data type for traversal function of the RESTful
 * resources. It traverses the resources and applies the given visitor function
 * to each resource. The traversal terminates when the visitor function returns
 * true. In this case, the corresponding resource is returned. Otherwise, NULL
 * is returned. Notice that a void pointer might be given as a parameter to be
 * passed to the visitor function. This pointer maybe used to reference to any
 * structure needed by the visitor function.
 */
typedef  RestResource_T const *(*RestServer_TraversalFunc_T)(
    RestSettings_T const *, RestServer_VisitorFunc_T, void *);


/**
 * This data structure holds the data related to an application service
 */
struct RestSettings_S
{
	/** This flag defines if the service should be accessible via CoAP */
	bool coapServiceEnabled:1;
	/** This flag defines if the service should be accessible via HTTP */
	bool httpServiceEnabled:1;
	/* pad to align on 8 bit */
	uint8_t:6;

#if SERVAL_ENABLE_REST_COAP_BINDING
	/** The port number the service will listen on for CoAP requests */
	Ip_Port_T coapPort;
#endif

#if SERVAL_ENABLE_REST_HTTP_BINDING
	/** The port number the service will listen on for HTTP requests */
	Ip_Port_T httpPort;
#if SERVAL_ENABLE_HTTP_AUTH
	/** The authentication table which will be used by the service to
	 *  authenticate HTTP requests */
	HttpRealm_T const ** authTable;
#endif
#endif
	/** This is a function pointer to a traversal function of the RESTful
	 * resources.
	 * @see RestServer_TraversalFunc_T */
	RestServer_TraversalFunc_T traverse;
};


/**
 * This data structure holds the data of a REST response.
 */
typedef struct RestServer_RespInfo_S
{
	/** A pointer to the payload to be set in the response */
	uint8_t*             payload_ptr;
	/** The length of the payload the payload_ptr points to */
#if SERVAL_POLICY_LARGE_PAYLOAD == 1
	uint16_t             payloadLen;
#else
	uint8_t              payloadLen;
#endif
	/** The content format of the payload */
	Rest_ContentFormat_T contentFormat;
	/** The Location-Path */
	char*                locationPath;
	/** The Location-Query */
	char*                locationQuery;
	/** The response code of the response */
	Rest_Code_T          responseCode;
} RestServer_RespInfo_T;


/**
 * Initializes the REST server including all needed used modules.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_SERVER_INIT_ERROR if the initialization of the REST server module
 * fails.
 */
retcode_t RestServer_initialize(void);

/**
 * This function is called to start the default REST server
 *
 * @param[in] restService_ptr
 * pointer to the connection and port to be handled
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_SERVER_LISTEN_ERROR if the listening for the server fails.
 */
retcode_t RestServer_startInstance(RestSettings_T *restService_ptr);


/**
 * Writes the response into the given message using the given response info.
 *
 * @param[in] msg_ptr
 * A reference to the message to be used for the response.
 *
 * @param[in] responseInfo_ptr
 * A reference to the response infos to be used for the response
 *
 * @return
 * RC_OK on success <br>
 * RC_REST_CREATE_RESPONSE_ERROR if an error occurs creating the response.
 *
 * @see RestServer_RespInfo_T
 */
retcode_t RestServer_fillRespMsg(Msg_T* msg_ptr,
                                 RestServer_RespInfo_T const *responseInfo_ptr);

/**
 * Sends the response
 *
 * @param[in] msg_ptr
 * A reference to the response message to be sent.
 *
 * @return
 * RC_OK on success <br>
 * RC_REST_SERVER_SEND_ERROR if an error occurs.
 */
retcode_t RestServer_respond(Msg_T* msg_ptr);

#if SERVAL_ENABLE_REST_SERVER_OBSERVATION
/**
 * Notify the stack that a resource has changed.
 *
 * @param[in] resource_ptr
 * Non-null pointer to the resource that has changed
 *
 * @return
 * RC_OK on success (currently no known error state)
 *
 * @since 1.4
 */
retcode_t RestServer_resourceChanged(RestResource_T* resource_ptr);

#endif

#if SERVAL_ENABLE_REST_SECURE_SERVER
/**
 * This function is called to start the secure REST server
 *
 * @param[in] restService_ptr
 * pointer to the connection and port to be handled
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_SERVER_LISTEN_ERROR if the listening for the server fails.
 */
retcode_t RestServer_startSecureInstance(RestSettings_T *restService_ptr);

/**
 * This function close and disconnect an existing secure COAP connection.
 *
 * @param[in] peerAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] peerPort
 * port number of the remote server socket
 *
 * @return when error code available
 * On success, RC_OK is returned. <br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestServer_closeSecureConn(Ip_Address_T *peerAddr_ptr,
                                     Ip_Port_T peerPort);

/**
 * This function reports the secure connections.
 *
 * @param[in] iterator_ptr
 * Iterator for this function. Initialized to -1 it reports the first
 * connection. The next returned values will report the next connections.
 * if -1 returns, no more connections are assigned to this socket.
 * Example:
 *    int16_t iter = -1;
 *    while(1) {
 *         rc = _getSecureConnection(&iter,...);
 *         if(iter < 0) break;
 *         do_some_thing(...);
 *    }
 *
 *
 * @param[in] onlyWithError
 * true -> report only connections with failures.
 * false -> report all connections
 *
 * @param[out] ipAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[out] port_ptr
 * returned port number of the connection endpoint
 */
void RestServer_iterateSecureConnections(int16_t *iterator_ptr,
        bool onlyWithError,
        Ip_Address_T *ipAddr_ptr, Ip_Port_T *port_ptr);

/**
 * This function reports the errors of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] rc_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestServer_getSecureConnError(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    retcode_t *rc_ptr);

/**
 * This function reports the status of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] state_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestServer_getSecureConnState(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    SecureConnectionState_T *state_ptr);

#endif /* SERVAL_ENABLE_REST_SECURE_SERVER */

#endif /* SERVAL_ENABLE_REST_SERVER */
#endif /* SERVAL_RESTSERVER_H_ */
