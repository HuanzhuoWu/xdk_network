/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Profiling.h
 * @brief Profiling Module
 * @ingroup utilgroup
 *
 * This module provides definitions and functions to support the task of
 * runtime and energy profiling. Beside the functions for runtime measurement
 * it sets the status of hardware pins in order to make certain activities
 * visible and traceable using an oscilloscope or a logic analyzer.
 */

#ifndef PROFLING_H_
#define PROFLING_H_

#include <Serval_Types.h>
#include <Serval_Defines.h>
#include <Serval_Exceptions.h>


#if SERVAL_ENABLE_PROFILING

/**
 * The number of hardware pin profiling can process in parallel
 */
#define SERVAL_PROFILING_MAX_NUM_PINS   2

/**
 * This is the maximal length supported for category length for profiling
 * context.
 */
#define SERVAL_PROFILING_MAX_LENGTH_CATEGORY_NAME   30


/**
 * This data structure is used for holding data for profiling (time and energy
 * measurement).
 */
struct Profiling_S
{
	/** This is used as identifier and is added to logging message */
	char categoryName[SERVAL_PROFILING_MAX_LENGTH_CATEGORY_NAME+1];

	/**
	 * This holds the timer value during profiling and also final timing result
	 */
	int32_t time;
#if 0
	int64_t time;
#endif

	/** This identifies the hardware pin used for profiling */
	int pin;
};

/**
 * This data structure is used for holding data for profiling.
 *
 * @see struct Profiling_S
 */
typedef struct Profiling_S Profiling_T;


/**
 * This function is called to initialize the profiling structure.
 *
 * This function initializes and prepare the profiling data structure. This
 * function must be called before start of profiling for each category
 * separately.
 *
 * @param[in] profile_ptr
 * Reference to a Profiling_T object to be initialized.
 * It has to be a valid pointer.
 *
 * @param[in] profileName
 * Identifier for current profiling category, e.g. "Sending" or
 * "Message serialization".
 *
 * @param[in] pin
 * Identifier for hardware pin for current profiling session. This pin will be
 * set to high when the measurement starts, and back to low when the
 * measurement stops.
 *
 * @return RC_OK on success
 * RC_PROFILING_PIN_ERROR Pin is not supported
 */
retcode_t Profiling_setupProfilingCtx(Profiling_T *profile_ptr,
                                      const char * const profileName, int pin);


/**
 * This function called to start the measurement.
 *
 * Before calling this function, Profiling_setupProfilingCtx() must have been called.
 * This function start the profiling and also trigger rising edge on hardware
 * pin selected for profiling during initialization.
 *
 * @param[in] profiling_ptr
 * Reference to a Profiling_T object which is been used for profiling.
 * It has to be a valid pointer.
 */
void Profiling_start(Profiling_T *profiling_ptr);


/**
 * This function called to end of measurement.
 *
 * Before calling this function, Profiling_setupProfilingCtx() must have been called.
 * This function ends a previously started profiling and also trigger a falling
 * edge on hardware pin selected for profiling during initialization.
 *
 * @param[in] profiling_ptr
 * Reference to a Profiling_T object which is been be used for profiling.
 * It has to be a valid pointer.
 */
void Profiling_stop(Profiling_T *profiling_ptr);


/**
 * This function called to log the result of measurement. The result will be
 * logged regardless the log level to all enabled log appenders.
 *
 * Before calling this function, Profiling_setupProfilingCtx() must have been called.
 * This function should be preceded by calls to Profiling_start() and
 * Profiling_stop().
 *
 * @param[in] profiling_ptr
 * Reference to a Profiling_T object which is been be used for profiling.
 * It has to be a valid pointer.
 */
void Profiling_report(Profiling_T *profiling_ptr);


/**
 * This function is called to fetch the time value for given profiling.
 *
 * Before calling this function, Profiling_setupProfilingCtx() should have been
 * called.
 *
 * @param[in] profile_ptr
 * Reference to a Profiling_T object which is been be used for profiling.
 * It has to be a valid pointer.
 *
 * @param[out] t_ptr
 * This will contain time value when the function returns.
 */
void Profiling_getTime(Profiling_T *profile_ptr, uint64_t *t_ptr);


/**
 * This function is called to fetch the hardware pin identifier for given
 * profiling.
 *
 * Before calling this function, Profiling_setupProfilingCtx() should have been
 * called.
 *
 * @param[in] profile_ptr
 * Reference to a Profiling_T object which is been be used for profiling.
 * It has to be a valid pointer.
 *
 * @param[out] pin_ptr
 * This will contain value of hardware pin identifier when the function returns.
 */
void Profiling_getPin(Profiling_T *profile_ptr, int *pin_ptr);


/**
 * This function is called to fetch the identifier for given profiling.
 *
 * Before calling this function, Profiling_setupProfilingCtx() should have been
 * called.
 *
 * @param[in] profile_ptr
 * Reference to a Profiling_T object which is been be used for profiling.
 * It has to be a valid pointer.
 *
 * @param[out] categoryName
 * This will contain value of identifier for given profiling when the function
 * returns.
 */
void Profiling_getCategoryName(Profiling_T *profile_ptr,
                               const char **categoryName);


/**
 * This function returns the value of current time in millisecond as given by
 * clock module.
 *
 * @return
 * Value of current time in millisecond.
 */
int32_t Profiling_getCurrentTimeMillis(void);

#endif /* SERVAL_ENABLE_PROFILING */

#endif /* PROFLING_H_ */
