/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Webserver.h
 * @brief Web Server Module
 * @ingroup webservergroup
 *
 * The application interface of the Webserver. In normal use cases the
 * application does not need to directly access this interface. The "Web"
 * component which added by the code generator to the application contains
 * the glue code between the application stack and provides the application with
 * a simplified interface.
 */

#ifndef SERVAL_WEBSERVER_H_
#define SERVAL_WEBSERVER_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_WEBSERVER  /* if Webserver is disabled -> no compilation */

#include "Serval_Types.h"
#include "Serval_Http.h"
#include "Serval_Msg.h"


/**
 * The data structure to represent (real) website resources.
 *
 * Notice that virtual URLs are supported using a different mechanism.
 */
struct Webserver_Resource_S
{
	/**
	 * The URL of the resource represented as a ContentHandle which is an
	 * abstract reference to a string or file entry.
	 */
	ContentHandle_T url;

	/**
	 * The content type of the resource, e.g. "text/html" or "image/gif"
	 */
	char const * const contentType;

	/**
	 * The recipe for serializing the resource.
	 */
	OutMsgRecipe_T const *page;

	/**
	 * An indicator whether this resource is cacheable or not.
	 */
	bool toCache;
};

/**
 * A data type definition for website resources
 *
 * @see Webserver_Resource_S
 */
typedef struct Webserver_Resource_S Webserver_Resource_T;

/**
 * A data type defining a function pointer for the application specific
 * request handling. The application may provide such a request handling
 * routine for POST requests as well as GET and POST requests to virtual URLs.
 *
 * @see Webserver_Website_S
 */
typedef retcode_t (*Webserver_AppRequestHandler_T)(Msg_T*);


/**
 * A data structure for the entire website hosted by the device.
 */
struct Webserver_Website_S
{
	Ip_Port_T port;

	/**
	 * A list of the hosted (real) resources.
	 */
	Webserver_Resource_T const * const *resources;

	/**
	 * A reference to an application specific request handling routines for
	 * virtual URLs and POST requests.
	 */
	Webserver_AppRequestHandler_T appRequestHandler;
#if SERVAL_ENABLE_HTTP_AUTH
	HttpRealm_T const ** authTable;
#endif /* SERVAL_ENABLE_HTTP_AUTH */
#if SERVAL_ENABLE_TLS_SERVER
	/* if set to true, the webserver used secure TLS protocol */
	bool isSecure;
#endif /* SERVAL_ENABLE_TLS_SERVER */
};

/**
 * A data type representing the website hosted by the device.
 *
 * @see Webserver_Website_S
 */
typedef struct Webserver_Website_S Webserver_Website_T;


/**
 * Initializes the webserver and all dependent components.
 *
 * @return
 * RC_OK or RC_WEBSERVER_INIT_ERROR
 */
retcode_t Webserver_initialize(void);

/**
 * Starts the webserver with the given context.
 *
 * @param[in] websites_ptr
 * A reference to the webserver context including the resources, and handling
 * functions.
 *
 * @return
 * RC_OK or RC_WEBSERVER_START_ERROR
 */
retcode_t Webserver_start(Webserver_Website_T *websites_ptr);

/**
 * It looks up the resource identified by the URL given in the HTTP message.
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @return
 * A reference to the resource element. If the URL does not exist in the website
 * resources, then NULL pointer is returned.
 */
Webserver_Resource_T const *Webserver_lookupUrl(Msg_T *msg_ptr);

/**
 * It returns a reference to the webserver resource of the given web message.
 *
 * @param[in] webMsg_ptr
 * Reference to the WebMsg_t object. It has to be a valid pointer.
 *
 * @return
 * A reference to the webserver resource.
 */
Webserver_Resource_T const* WebMsg_getResource(WebMsg_T *webMsg_ptr);

/**
 * This function is called to set the webserver resource of the given web
 * message. This resource will be used in the response for the given web
 * message.
 *
 * @param[in] webMsg_ptr
 * Reference to the WebMsg_t object to modify. It has to be a valid pointer.
 *
 * @param[in] wResource_ptr
 * The webserver resource to set in the web message.
 */
void WebMsg_setResource(WebMsg_T *webMsg_ptr,
                        Webserver_Resource_T const *wResource_ptr);

/**
 * This function is called to set the content type to be used in the response
 * for the given web message.
 *
 * @param[in] webMsg_ptr
 * Reference to the WebMsg_t object to modify. It has to be a valid pointer.
 *
 * @param[in] contentType_ptr
 * The content type to set.
 *
 * @see Http_ContentType_Text_Plain, Http_ContentType_Text_Html,
 * Http_ContentType_Image_Gif, Http_ContentType_Image_Png,
 * Http_ContentType_Image_Jpeg, Http_ContentType_Image_Bmp,
 * Http_ContentType_Image_Ico, Http_ContentType_App_Pdf,
 * Http_ContentType_App_Soap
 */
void WebMsg_setContentType(WebMsg_T *webMsg_ptr, char const *contentType_ptr);

#endif /* SERVAL_ENABLE_WEBSERVER */
#endif /* SERVAL_WEBSERVER_H_ */
