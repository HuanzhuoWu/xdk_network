/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Dpws.h
 * @brief An interface to the DPWS component.
 * @ingroup dpwsgroup
 *
 * The DPWS component implements the OASIS standard "Device Profile for Web
 * Services" v1.1. (1. July 2009), see
 * http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01
 */
#ifndef SERVAL_DPWS_H_
#define SERVAL_DPWS_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_DPWS  /* if DPWS is disabled -> no compilation here */

#include <Serval_Types.h>
#include <Serval_Http.h>
#include <Serval_Callable.h>
#include <Serval_Msg.h>
#include <Serval_Network.h>
#include <Serval_Uuid.h>

/**
 * The WS-Discovery port. This port is defined in the WS-Discovery standard
 * and should not be modified.
 */
#define SERVAL_DPWS_WS_DISCOVERY_PORT                3702

/**
 * This is the maximal number of subscriptions which the device can handle.
 * Notice that the subscriber list is saved into the NVM on platforms which
 * do not support RAM retention like the G2 platform. Make sure to reserve
 * enough space for this.
 */
#ifndef SERVAL_DPWS_MAX_NUMBER_SUBSCRIBERS
#define  SERVAL_DPWS_MAX_NUMBER_SUBSCRIBERS             2
#endif

/**
 * If this is greater than zero, then DPWS will remember the
 * message IDs of the lastly received messages. This list will be used
 * to detected duplicated messages. If this set to zero, then
 * the duplicate detection will be disabled.
 */
#ifndef SERVAL_DPWS_REQUEST_UUID_HISTORY_LENGTH
#define SERVAL_DPWS_REQUEST_UUID_HISTORY_LENGTH         0
#endif

/**
 * This is the maximal length supported for a string parameter value
 * for the dpws webservice operations. This threshold applies for both
 * input and output parameters.
 */
#ifndef SERVAL_DPWS_MAX_LEN_STRING_PARAM
#define SERVAL_DPWS_MAX_LEN_STRING_PARAM              127
#endif

/**
 * This is the maximal length supported for serial number.
 */
#ifndef SERVAL_DEVICE_MAX_LEN_SERIAL_NUM
#define SERVAL_DEVICE_MAX_LEN_SERIAL_NUM               11
#endif

/**
 * This is the maximal length supported for firmware version.
 */
#ifndef SERVAL_DEVICE_MAX_LEN_FIRMWARE_VERSION
#define SERVAL_DEVICE_MAX_LEN_FIRMWARE_VERSION          7
#endif

/**
 * This is the maximal length supported for dpws friendly name.
 */
#ifndef SERVAL_DEVICE_MAX_LEN_FRIENDLY_NAME
#define SERVAL_DEVICE_MAX_LEN_FRIENDLY_NAME            31
#endif

/**
 * This is the maximal length supported for incoming message ID.
 */
#ifndef SERVAL_DPWS_INCOMING_MSGID_MAX_LEN
#define SERVAL_DPWS_INCOMING_MSGID_MAX_LEN             47
#endif

/**
 * This is a flag which makes the DPWS stack to inline the WSDL definition
 * in the metadata exchange. If it is disabled, then just an URL will be
 * provided as a location reference to the WSDL definition.
 */
#ifndef SERVAL_DPWS_INLINE_WSDL
#define SERVAL_DPWS_INLINE_WSDL                         1
#endif


/**
 * This data structure represents a subscriber to an event
 * @see struct DpwsSubscriber_S
 */
typedef struct DpwsSubscriber_S DpwsSubscriber_T;

/**
 * This data structure represents a DPWS device model
 * @see struct DpwsModel_S
 */
typedef struct DpwsModel_S DpwsModel_T;

/**
 * This data structure represents a DPWS device type
 * @see struct DpwsDeviceType_S
 */
typedef struct DpwsDeviceType_S DpwsDeviceType_T;

/**
 * This data structure represents a DPWS device
 * @see struct DpwsDevice_S
 */
typedef struct DpwsDevice_S DpwsDevice_T;

/**
 * Description of a webservice function.
 * @see struct WSRequest_S
 */
typedef struct WSRequest_S WSRequest_T;

/**
 * This data structure represents an ws event
 * @see struct WSEvent_S
 */
typedef struct WSEvent_S WSEvent_T;

/**
 * This data structure represents a webservice.
 * @see struct Webservice_S
 */
typedef struct Webservice_S Webservice_T;

/**
 * This data structure represents list of web services of a DPWS device.
 * @see struct WebservicesCatalog_S
 */
typedef struct WebservicesCatalog_S WebservicesCatalog_T;

/**
 * This data structure represents a eventing session.
 * @see struct EventingSession_S
 */
typedef struct EventingSession_S EventingSession_T;

/**
 * This data type represents a function pointer for the handle function carried
 * by the WSRequest_T element.
 */
typedef retcode_t (*WSRequestFunc_T)(Msg_T*);

/** This holds the ascii value of string "http://" */
extern char const Http_Url_Prefix[];
/** This holds the ascii value of string ":" */
extern char const Colon[];
/** This holds the ascii value of string "/" */
extern char const Slash[];
/** This holds the ascii value of string "\"" */
extern char const Quote[];
/** This holds the ascii value of string ">" */
extern char const Closing_Angle_Bracket[];
/** This holds the ascii value of string "urn:uuid:" */
extern char const Urn_Uuid[];
/** This holds the ascii value of string "VersionMismatch" */
extern char const Soap_Fault_VerMismatch[];
/** This holds the ascii value of string "MustUnderstand" */
extern char const Soap_Fault_MustUnderstand[];
/** This holds the ascii value of string "DataEncodingUnknown" */
extern char const Soap_Fault_DataEncodingUnknown[];
/** This holds the ascii value of string "Client" */
extern char const Soap_Fault_Receiver[];
/** This holds the ascii value of string "Server" */
extern char const Soap_Fault_Sender[];

/**
 * An enumeration data-type for the DPWS fault
 */
typedef enum
{
	DPWS_FAULT_NONE = 0,
	DPWS_FAULT_SENDER = 1,
	DPWS_FAULT_RECEIVER,
} DpwsFault_T;

/**
 * This data structure gives description of a webservice function. Maps an
 * action name to a handle function.
 */
struct WSRequest_S
{
	/** Action name. */
	ContentHandle_T const reqAction;

	/** Action name. */
	ContentHandle_T const respAction;

	/**
	 * Pointer to the handling function which is executed if action name matches.
	 */
	WSRequestFunc_T const trigger;

	/** The recipe which is used to construct the response body */
	OutMsgRecipe_T const * const respBodyRecipe_ptr;
};

/**
 * This data structure represents an ws event. It maps an action name to a
 * recipe.
 */
struct WSEvent_S
{
	/** Action name. */
	ContentHandle_T const actionName;

	/** The recipe which is used to construct the event body */
	OutMsgRecipe_T const * const eventBodyRecipe_ptr;
};

/**
 * This data structure gives description of a webservice. Maps an
 * action name to a list of handle functions.
 */
struct Webservice_S
{
	/** The endpoint used for addressing the webservice, also refered to as
	 * Webservice name.
	 */
	ContentHandle_T const serviceEndpoint;

	/** Pointer to the webservice actions which can be requested.*/
	WSRequest_T const * const *requests;

	/** Pointer to the webservice events.*/
	WSEvent_T const * const *events;

	OutMsgRecipe_T const * const wsdlRecipe_ptr;

	OutMsgRecipe_T const * const wsdlStandaloneRecipe_ptr;
};

/**
 * This data structure represents list of web services of a DPWS device.
 */
struct WebservicesCatalog_S
{
	/** Number of services */
	uint8_t webservicesNum;
	/** pointer to web services of the DPWS device */
	Webservice_T const * const *webservices;
};

/**
 *  This data structure represents a DPWS device type, this indicate that the
 *  device supports the device profile
 */
struct DpwsDeviceType_S
{
	/** Holds xmlNamespace */
	ContentHandle_T const xmlNamespace;
	/** Holds type */
	ContentHandle_T const type;
	/** Holds next device type of the device */
	struct DpwsDeviceType_S const *next;
};

/**
 *  This data structure represents a device model which identifies a DPWS
 *  device.
 */
struct DpwsModel_S
{
	/** This gives the name of the manufacturer of the DPWS device */
	ContentHandle_T const manufacturer;
	/** This gives the URL of the manufacturer of the DPWS device */
	ContentHandle_T const manufacturerUrl;
	/** This gives the language of the DPWS device */
	ContentHandle_T const language;
	/** This gives the model name from the manufacturer of the DPWS device */
	ContentHandle_T const modelName;
	/** This gives the model number from the manufacturer of the DPWS device */
	ContentHandle_T const modelNumber;
	/** This gives the model URL from the manufacturer of the DPWS device */
	ContentHandle_T const modelUrl;
	/** This gives the presentation URL of the DPWS device */
	ContentHandle_T const presentationUrl;
	/**
	 * This gives DPWS device type.
	 * @see DpwsDeviceType_T
	 */
	DpwsDeviceType_T const * const types;
	/**
	 * This gives relationship recipe of the DPWS device.
	 * @see OutMsgRecipe_T
	 */
	OutMsgRecipe_T const * const relationshipRecipe_ptr;
};

/**
 * This data structure represents a DPWS device. This data structure holds model
 * of the device, also web services of the device and expose functions to
 * manage input and output objects for the device.
 *
 * @see DpwsModel_T WebservicesCatalog_T AppInOutput_Manager_T
 */
struct DpwsDevice_S
{
	/**
	 * The port which the DPWS server will listen to.
	 */
	Ip_Port_T port;

	/**
	 * This represents a DPWS device model.
	 * @see DpwsModel_T
	 */
	DpwsModel_T const *model_ptr;

	/**
	 * This gives web services of the DPWS device.
	 * @see WebservicesCatalog_T
	 */

	WebservicesCatalog_T const *webservicesCatalog_ptr;

	/**
	 * This holds all functions to manage application's input and output objects
	 * @see AppInOutput_Manager_T
	 */
	AppInOutput_Manager_T const *appInOutputManager_ptr;

#if SERVAL_ENABLE_HTTP_AUTH
	HttpRealm_T const * const *authTable;
#endif /* SERVAL_ENABLE_HTTP_AUTH */
};

struct deviceProfileTableEntry_S
{
	const char *namespace;
	const char *type;
};


/**
 *  This data structure represents a subscriber to an event and is used to
 *  notify an event to the subscribed party.
 */
struct DpwsSubscriber_S
{
	/** UUID of the subscription */
	Uuid_T subscriptionUuid;
	/** The client ReferenceParameters/wse:Identifer*/
	char clientRefIdentifier[SERVAL_DPWS_INCOMING_MSGID_MAX_LEN + 1];
	/** IP address of the subscriber to be notified */
	Ip_Address_T notifyToIpAddr;
	/** IP port of the subscriber to be notified */
	Ip_Port_T notifyToPort;
	/** Address of the subscriber to be notified */
	char notifyToAddress[SERVAL_HTTP_MAX_LENGTH_URL + 1];
	/** Address of the subscriber */
	char *url;
	/** This hold the action which subscriber is interested in */
	WSEvent_T const *filterAction_ptr;
	/** This indicate whether subscriber is active or not */
	bool isActive;
	/** This data structure gives description of webservice */
	Webservice_T const *ws_ptr;
	/**
	 * This indicate whether subscriber need Reference Identifier in notifying
	 * event or not
	 * */
	bool withReferenceIdentifier;
};


/**
 * Initializes the DPWS server of a DPWS device, including all needed used
 * modules like DPWS registry, discovery, eventing, xml parser etc. The server
 * is however not started by this call. To start the server, the function
 * Dpws_start() must be called.
 *
 * @return
 * RC_OK on success or RC_DPWS_INIT_ERROR otherwise.
 */
retcode_t Dpws_initialize(void);


/**
 * Starts the DPWS server with the give device context.
 *
 * @param[in] dpwsDevice_ptr
 * The handle of the DPWS device context which the server should be started
 * with. It must be a valid pointer to a valid DPWS device context.
 *
 * @return
 * RC_OK on success or RC_DPWS_START_ERROR otherwise.
 */
retcode_t Dpws_start(DpwsDevice_T const *dpwsDevice_ptr);

/**
 * This function is called by the application to inform DPWS server of a change
 * in network binding, e.g. a change in IP address
 *
 * @param[in] status
 * This holds the network event which causes the invocation of the function.
 *
 * @return RC_OK if event is handled,
 * RC_DPWS_HANDLE_NETWORK_BINDING_CHANGE otherwise.
 */
retcode_t Dpws_handleNetworkBindingChange(Network_Event_T status);

/**
 * This function triggers sending a DPWS Hello message.
 *
 * @return RC_OK, if successful
 * RC_DPWS_SEND_HELLO_FAILED, otherwise.
 */
retcode_t Dpws_sendHello(void);

/**
 * This function triggers sending a DPWS Bye message.
 *
 * Note: AT THE MOMENT THIS FEATURE IS NOT IMPLEMENTED.
 *
 * @return RC_OK, if successful
 * RC_DPWS_SEND_BYE_FAILED, otherwise.
 */
retcode_t Dpws_sendBye(void);

/**
 * This function pushes a DPWS event to the interested subscribers. Sending of
 * the event is performed asynchronously. The application is notified about the
 * success or failure of sending the event using the given callback function.
 *
 * The application callback is called once for each subscriber to which the
 * event message has been pushed. The status passed to the callback is RC_OK
 * if the event message has been pushed successfully to the subscriber.
 * RC_DPWS_PUSH_EVENT_FAILED will be passed otherwise.
 *
 * @param[in]  ws_ptr
 * pointer to webservice to be used.
 *
 * @param[in]  wsEvent_ptr
 * pointer to event to be pushed.
 *
 * @param[in]  appOutput_ptr
 * pointer to application data which belongs to the event.
 *
 * @param[in]  appCallback
 * Callback to invoke when the event has been sent to subscribers.
 *
 * @param[out] eventingSession_pptr
 * Pointer to where to store the session ID.
 *
 * @return RC_OK, if successful
 * RC_DPWS_PUSH_EVENT_FAILED, otherwise.
 */
retcode_t Dpws_pushEvent(Webservice_T const *ws_ptr,
                         WSEvent_T const *wsEvent_ptr, AppOutput_T *appOutput_ptr,
                         AppCallback_T appCallback, EventingSession_T **eventingSession_pptr);

/**
 * This function is called by the application to respond to a DPWS request. The
 * application is notified about the success or failure of sending the response
 * using the given callback function.
 *
 * The application callback is called with the status RC_OK
 * if the response message has been sent successfully to the client.
 * Otherwise, the callback is invoked with the status RC_DPWS_RESPOND_FAILED.
 *
 * @param[in] msg_ptr
 * Pointer to the Msg_T element representing the request/response message.
 *
 * @param[in] appOutput_ptr
 * Reference to a AppOutput_T object, this holds the output pointer for response
 *
 * @param[in] appCallback
 * Reference to a AppCallback_T object, this holds the application callback.
 *
 * @return RC_OK, if successful
 * RC_DPWS_RESPOND_FAILED, otherwise.
 */
retcode_t Dpws_respond(Msg_T *msg_ptr, AppOutput_T *appOutput_ptr,
                       AppCallback_T appCallback);


/**
 * Returns the number of active subscriptions.
 *
 * @return
 * the number of active subscriptions.
 */
int Dpws_getNumActiveSubscriptions(void);


/**
 * Provides the list of subscriptions. Notice that also inactive subscription
 * entries are included.
 *
 * @param[out] subscr_pptr
 * The head of the subscription list. It must be a valid pointer.
 *
 * @param[out] num_ptr
 * The number of subscriptions, i.e. the length of the subscription list
 */
void Dpws_getAllSubscriptions(DpwsSubscriber_T **subscr_pptr,
                              uint32_t *num_ptr);


/**
 * This function is called to delete all subscriptions to DPWS events. It just
 * deletes the structures and does not include sending a subscription end
 * message to the subscriber.
 */
void Dpws_dropAllSubscriptions(void);


/**
 * This function returns the reference to the XML parser object which is
 * associated with the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @return
 * the reference to the XML parser object which is associated with the given
 * DPWS message.
 */
XmlParser_T *DpwsMsg_getParser(DpwsMsg_T *dpwsMsg_ptr);

/**
 * This function returns the reference to the application input object which is
 * associated with the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @return
 * the reference to the application input object which is associated with the
 * given DPWS message.
 */
AppInput_T *DpwsMsg_getAppInput(DpwsMsg_T *dpwsMsg_ptr);

/**
 * This function returns the reference to the application output object which is
 * associated with the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @return
 * the reference to the application output object which is associated with the
 * given DPWS message.
 */
AppOutput_T *DpwsMsg_getAppOutput(DpwsMsg_T *dpwsMsg_ptr);

/**
 * This function is called to set association between given the reference to the
 * application input object and the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @param[in] appInput_ptr
 * A pointer to the application input object. It has to be a valid pointer.
 *
 */
void DpwsMsg_setAppInput(DpwsMsg_T *dpwsMsg_ptr, AppInput_T *appInput_ptr);

/**
 * This function is called to set association between given the reference to the
 * application output object and the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @param[in] appOutput_ptr
 * A pointer to the application output object. It has to be a valid pointer.
 *
 */
void DpwsMsg_setAppOutput(DpwsMsg_T *dpwsMsg_ptr, AppOutput_T *appOutput_ptr);

/**
 * This function returns the reference to the eventing session which is
 * associated with the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * A pointer to the DPWS message. It has to be a valid pointer.
 *
 * @return
 * the reference to the eventing session which is associated with the given DPWS
 *  message.
 */
EventingSession_T *DpwsMsg_getEventingSession(DpwsMsg_T *dpwsMsg_ptr);

/**
 * This function is called to close DPWS message. This function also releases
 * application input or output object associated with the given DPWS message.
 *
 * @param[in] dpwsMsg_ptr
 * Reference to a DpwsMsg_T object which holds the DPWS message to be closed.
 *  It has to be a valid pointer.
 */
void DpwsMsg_close(DpwsMsg_T *dpwsMsg_ptr);

/**
 * This function reads the event namespace from the DPWS device content base
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out]   msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeEventNamespace(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the device type from the DPWS device and write it to the
 * buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeDeviceTypes(OutMsgSerializationHandover_T *msgItems);

/**
 * This function based on the direction of DPWS message reads the end point or
 * the address of an event subscriber and write it to the buffer specified in
 * the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeTo(OutMsgSerializationHandover_T *msgItems);

/**
 * This function based on the direction of DPWS message reads the message ID of
 * request or response and write it to the buffer specified in the object
 * msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeOutMsgId(OutMsgSerializationHandover_T *msgItems);

/**
 * This function based on the direction of DPWS message reads the message action
 * of request or response and write it to the buffer specified in the object
 * msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeAction(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the request message ID and write it to the buffer
 * specified in the object msgItems for the response.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeRelatesToMsgId(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the application instance ID and write it to the buffer
 * specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeInstanceId(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the application sequence message number and write it to
 * the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeMsgNumber(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device UUID and write it to the buffer specified
 *  in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeDeviceUuid(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device meta data version from the registry and
 * write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeMetadataVersion(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device IP address and the DPWS service port used
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_DPWS_INVALID_OWN_IP if IP address is incorrect
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeHostnPort(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device IP address and the web service port used
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_DPWS_INVALID_OWN_IP if IP address is incorrect
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeHostnWebPort(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device firmware version from application data
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeFirmwareVersion(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device serial number from application data
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSerialNumber(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device language from the registry and write it
 * to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeLanguage(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device friendly name and write it to the buffer
 * specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeDeviceFriendlyName(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device manufacturer name from the DPWS device
 * model and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeManufacturer(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device manufacturer URL from the DPWS device
 * model and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeManufacturerUrl(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device model name from the DPWS device model and
 * write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeModelName(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device model number from the DPWS device model
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeModelNumber(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device model URL from the DPWS device model
 * and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeModelUrl(OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device Presentation URL from the DPWS device
 * model and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializePresentationUrl(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the DPWS device expiry information and write it to the
 * buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeExpiry(OutMsgSerializationHandover_T *msgItems);

/**
 * This function based on the direction of DPWS message reads the UUID of
 * response and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_FACTORY_INCOMPLETE, otherwise.
 */
retcode_t DpwsMsg_serializeSubscriptionUuid(
    OutMsgSerializationHandover_T *msgItems);


retcode_t DpwsMsg_serializeSubscriberRefId(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function based on the direction of DPWS message reads the endpoint of a
 * service and write it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_INVALID_DIRECTION, if direction of message provided is incorrect
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSubscriptionEndpoint(
    OutMsgSerializationHandover_T *msgItems);


/**
 * This function reads the Soap fault code from DPWS message and write it to the
 * buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultCodeValue(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function checks and reads the Soap fault sub code from DPWS message and
 * write it to the buffer specified in the object msgItems if there is one.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubcode(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function checks and reads the Soap fault sub sub code from DPWS message
 * and write it to the buffer specified in the object msgItems if there is one.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubSubcode(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the Soap fault sub code from DPWS message and write it
 * to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubcodeValue(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the Soap fault sub sub code from DPWS message and write
 * it to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubSubcodeValue(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the Soap fault reason from DPWS message and write it
 * to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultReason(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function reads the Soap fault detail from DPWS message and write it
 * to the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_writeSoapFaultDetail(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write default soap namespace to the buffer specified in the
 * object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapDefaultNamespace(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write subcode namespace to the buffer specified in the
 * object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubcodeNamespace(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write sub-subcode namespace to the buffer specified in the
 * object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultSubSubcodeNamespace(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write relationship as reply in RelateTo tag the buffer
 * specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapRelateToReplyNamespace(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function checks if message has a detail to be serialized, if detail
 * function is empty it serializes the return code in soap fault structure into
 * the buffer specified in the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSoapFaultDetail(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write code for subscription end into the buffer specified in
 * the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSubscriptionEndCode(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function write reason for subscription end into the buffer specified in
 * the object msgItems.
 *
 * @param[in,out] msgItems
 * The object that holds the buffer and information needed by the serializer to
 * append the data at the right place.
 *
 * @return RC_OK, if successful
 * RC_MSG_SERIALIZATION_FAILED, otherwise.
 */
retcode_t DpwsMsg_serializeSubscriptionEndReason(
    OutMsgSerializationHandover_T *msgItems);

/**
 * This function is called by application before calling MSg_respond() to fill
 * the soap fault in out going message.
 *
 * @param[in] msg_ptr
 * The handle of the message to be sent. It must be a valid pointer.
 *
 * @param fault
 * The type of fault to be generated.
 *
 * @param[in] reasonFunc
 * The handle of the function which will be invoked to serialize the reason for
 * generated fault. If the handle is not valid than a default message will be
 * serialized.
 *
 * @param[in] detailFunc
 * The handle of the function which will be invoked to serialize the detail for
 * generated fault. If the handle is not valid than a default message will be
 * serialized.
 *
 * @return
 * RC_OK on success.
 */
retcode_t Dpws_fillFaultMsg(Msg_T *msg_ptr, DpwsFault_T fault,
                            DynamicContent_T reasonFunc, DynamicContent_T detailFunc);

/**
 * This function when called sends an end subscription to the subscriber
 * provided.
 *
 * @param[out] subscriber_ptr
 * The handle to the subscriber to whom end subscription needed to be sent.
 * It must be a valid pointer.
 *
 * @return
 * ::RC_OK, if successful,
 * ::RC_MSG_ALLOCATION_FAULT,
 * ::RC_HTTP_CLIENT_CONNECT_ERROR,
 * ::RC_HTTP_SEND_ERROR,
 * ::RC_HTTP_TOO_LONG_URL
 * ::RC_HTTP_NO_FREE_SESSION,
 * ::RC_MSG_FACTORY_SIZE_ERROR,
 * ::RC_MSG_FACTORY_ALREADY_COMMITTED
 */
retcode_t Dpws_endSubscription(DpwsSubscriber_T *subscriber_ptr);
#endif /* DPWS_H_ */

#endif /* SERVAL_DPWS_H_ */
