/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Serializer.h
 * @brief Serializer Module
 * @ingroup utilgroup
 *
 * This interface provides functions for the serialization and de-serialization
 * of various data type.
 */

#ifndef SERVAL_SERIALIZER_H_
#define SERVAL_SERIALIZER_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_SERIALIZER

#include <Serval_Types.h>
#include <Serval_StringDescr.h>

/**
 * This function writes data to a buffer in a printf-like fashion.
 *
 * @param[in] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @param[in] format
 * The format of the data (specified like for printf()) and followed by the data
 *
 * @param[in] args
 * The list of data parameters for the given format.
 *
 * @return
 * RC_OK on success<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
retcode_t Serializer_vserialize(char *buf_ptr, int bufSize,
                                int *len_ptr, const char* format, va_list args);


/**
 * This function writes data to a buffer in a printf-like fashion.
 *
 * @param[in] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @param[in] format
 * The format of the data (specified like for printf()) and followed by the data
 *
 * @return
 * RC_OK on success<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
retcode_t Serializer_serialize(char *buf_ptr, int bufSize,
                               int *len_ptr, const char* format, ...);

/**
 * This function writes a bool, a primitive data type, into a given buffer.
 *
 * @param[in] value
 * The value to write to buffer
 *
 * @param[out] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @return
 * RC_OK on success<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
inline static retcode_t Serializer_serializeBool(bool value, char *buf_ptr, int bufSize,
        int *len_ptr)
{
	return Serializer_serialize(buf_ptr, bufSize, len_ptr, value ? "1" : "0");
}

/**
 * This function writes a integer, a primitive data type, into a given buffer.
 *
 * @param[in] value
 * The value to write to buffer
 *
 * @param[out] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @return
 * RC_OK, if successful<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
inline static  retcode_t Serializer_serializeInt(int value, char *buf_ptr, int bufSize,
        int *len_ptr)
{
	return Serializer_serialize(buf_ptr, bufSize, len_ptr, "%i", value);
}


/**
 * This function writes a long integer, a primitive data type, into a given
 * buffer.
 *
 * @param[in] value
 * The value to write to buffer
 *
 * @param[out] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @return
 * RC_OK, if successful<br>
 * RC_MSG_FACTORY_INCOMPLETE buffer was too small<br>
 */
inline static retcode_t Serializer_serializeLong(int64_t value, char *buf_ptr, int bufSize,
        int *len_ptr)
{
	return Serializer_serialize(buf_ptr, bufSize, len_ptr, "%lli", value);
}


/**
 * This function writes a float, a primitive data type, into a given buffer.
 *
 * @param[in] value
 * The value to write to buffer
 *
 * @param[out] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @return
 * RC_OK, if successful<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
inline static retcode_t Serializer_serializeFloat(float value, char *buf_ptr, int bufSize,
        int *len_ptr)
{
	return Serializer_serialize(buf_ptr, bufSize, len_ptr, "%g", (double) value);
}


/**
 * This function writes a string into a given buffer.
 *
 * @param[in] value
 * The value to write to buffer
 *
 * @param[out] buf_ptr
 * The buffer to which given value should be written
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @param[out] len_ptr
 * The number of bytes written
 *
 * @return
 * RC_OK, if successful<br>
 * RC_MSG_FACTORY_INCOMPLETE given buffer is too small<br>
 */
inline static retcode_t Serializer_serializeString(char const *value, char *buf_ptr,
        int bufSize, int *len_ptr)
{
	return Serializer_serialize(buf_ptr, bufSize, len_ptr, "%s", value);
}


/**
 * This function converts a string into integer data type.
 *
 * @param[in] str_ptr
 * The String Descriptor object that has the value desired
 *
 * @param[out] value_ptr
 * Pointer to variable where the resulting value will be stored
 *
 * @return
 * RC_OK, if successful<br>
 * RC_DESERIALIZE_ERROR given string too long<br>
 */
retcode_t Serializer_deserializeInt64(StringDescr_T const *str_ptr,
                                      int64_t *value_ptr);


retcode_t Serializer_deserializeUint64(StringDescr_T const *str_ptr,
                                       uint64_t *value_ptr);


retcode_t Serializer_deserializeInt32(
    StringDescr_T const *str_ptr, int32_t *value_ptr);

retcode_t Serializer_deserializeUint32(
    StringDescr_T const *str_ptr, uint32_t *value_ptr);


retcode_t Serializer_deserializeInt16(
    StringDescr_T const *str_ptr, int16_t *value_ptr);

retcode_t Serializer_deserializeUint16(
    StringDescr_T const *str_ptr, uint16_t *value_ptr);


retcode_t Serializer_deserializeInt8(
    StringDescr_T const *str_ptr, int8_t *value_ptr);

retcode_t Serializer_deserializeUint8(
    StringDescr_T const *str_ptr, uint8_t *value_ptr);


/**
 * This function converts a string into the bool data type.
 *
 * @param[in] str_ptr
 * The String Descriptor object that has the value desired
 *
 * @param[out] value_ptr
 * Pointer to variable where the resulting value will be stored
 *
 * @return
 * RC_OK, if successful<br>
 * RC_DESERIALIZE_ERROR given string does not match "1", "0", "true", "false"
 */
retcode_t Serializer_deserializeBool(StringDescr_T const *str_ptr,
                                     bool *value_ptr);


/**
 * This function converts a string into the float data type.
 *
 * @param[in] str_ptr
 * The String Descriptor object that has the value desired
 *
 * @param[out] value_ptr
 * Pointer to variable where the resulting value will be stored
 *
 * @return
 * RC_OK, if successful<br>
 * RC_DESERIALIZE_ERROR given string too long<br>
 */
retcode_t Serializer_deserializeFloat(StringDescr_T const *str_ptr,
                                      float *value_ptr);


/**
 * This function converts a string into the double data type.
 *
 * @param[in] str_ptr
 * The String Descriptor object that has the value desired
 *
 * @param[out] value_ptr
 * Pointer to variable where the resulting value will be stored
 *
 * @return
 * RC_OK, if successful<br>
 * RC_DESERIALIZE_ERROR given string too long<br>
 */
retcode_t Serializer_deserializeDouble(StringDescr_T const *str_ptr,
                                       double *value_ptr);

/**
 * This function copies a string into the given buffer.
 *
 * @param[in] str_ptr
 * The String Descriptor object that has the value desired
 *
 * @param[out] buf
 * Pointer to buffer where the resulting value will be stored
 *
 * @param[in] bufSize
 * Size of the buffer.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_DESERIALIZE_ERROR given string too long<br>
 */
retcode_t Serializer_deserializeString(StringDescr_T const *str_ptr, char buf[],
                                       uint32_t bufSize);

#endif /* SERVAL_ENABLE_SERIALIZER */

#endif /* SERVAL_SERIALIZER_H_ */
