/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file Serval_CoapClient.h
 * @brief CoAP Client Module
 * @ingroup coapgroup
 *
 * The interface description of the CoAP Client.
 */

#ifndef SERVAL_COAPCLIENT_H_
#define SERVAL_COAPCLIENT_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_COAP_CLIENT  /* if CoAP client is disabled -> no compilation here */

#include <Serval_Exceptions.h>
#include <Serval_Msg.h>
#include <Serval_Coap.h>
#if SERVAL_ENABLE_DTLS
#include <Serval_Security.h>
#endif


/**
 * A data type representing a callback function pointer for CoAP application.
 * The application uses such callback functions in order to be notified about
 * the received messages.
 */
typedef retcode_t (*CoapAppRespCallback_T)(CoapSession_T *coapSession,
        Msg_T *msg_ptr, retcode_t status);


#if ! SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT
/**
 * This function is called to initialize CoAP client and related modules
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_CLIENT_INIT_ERROR, otherwise.
 */
retcode_t CoapClient_initialize(void);
#endif


/**
 * This function is called to initiate a request from CoAP client. This function
 * checks for any previous active transaction with the given server and if not
 * provides the message to be filled for sending the request.
 *
 * @param[in] addr_ptr
 * This is the reference to the IP address of the receiver.
 *
 * @param[in] port
 * This is the server port of the receiver.
 *
 * @param[out] msg_pptr
 * This holds message to be filled by application for request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_CLIENT_INIT_REQ_ERROR, otherwise.
 */
retcode_t CoapClient_initReqMsg(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                Msg_T **msg_pptr);


#if ! SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT
/**
 * This function is called to start the default CoAP client
 *
 * @return, RC_OK, if successful
 * RC_COAP_LISTEN_ERROR, otherwise.
 */
retcode_t CoapClient_startInstance(void);
#endif


/**
 * This function is called to push a request from a CoAP client. This function
 * prepares the message for further processing before passing it to the lower
 * layers.
 *
 * @param[in] msg_ptr
 * This is the reference to the message to be pushed.
 *
 * @param[in] sentCallback_ptr
 * This is the application callback to be called for informing application about
 * status of sending.
 *
 * @param[in] respCallback
 * This is the application callback to be called for a received response
 * message for this request. If the application does not expect a response for
 * this request it should be set to NULL.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_SENDING_ERROR, otherwise.
 */
retcode_t CoapClient_request(Msg_T *msg_ptr, Callable_T *sentCallback_ptr,
                             CoapAppRespCallback_T respCallback);


#if SERVAL_ENABLE_DTLS_CLIENT

/**
 * This function is called to start the secure CoAP client
 *
 * @return, RC_OK, if successful
 * RC_COAP_SECURE_CONNECTION_ERROR, otherwise.
 */
retcode_t CoapClient_startSecureInstance(void);


/**
 * This function is called to initiate a secure request from CoAP client.
 * This function checks for any previous active transaction with the given
 * server and if not provides the message to be filled for sending the request.
 *
 * @param[in] addr_ptr
 * This is the reference to the IP address of the receiver.
 *
 * @param[in] port
 * This is the server port of the receiver.
 *
 * @param[out] msg_pptr
 * This holds message to be filled by application for request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_CLIENT_INIT_REQ_ERROR, otherwise.
 */
retcode_t CoapClient_initSecureReqMsg(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                      Msg_T **msg_pptr);

/**
 * This function connects and opens a secure COAP client connection.
 *
 * @param[in] peerAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] peerPort
 * port number of the remote server socket
 *
 * @return when errorcode available
 * On success, RC_OK is returned. <br>
 * RC_COAP_SECURE_CONNECTION_ERROR
 */
retcode_t CoapClient_connectSecure(Ip_Address_T *peerAddr_ptr,
                                   Ip_Port_T peerPort);

/**
 * This function close and disconnect an existing secure COAP connection.
 *
 * @param[in] peerAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] peerPort
 * port number of the remote server socket
 *
 * @return when error code available
 * On success, RC_OK is returned. <br>
 * RC_COAP_SECURE_CONNECTION_ERROR
 */
retcode_t CoapClient_closeSecureConn(Ip_Address_T *peerAddr_ptr,
                                     Ip_Port_T peerPort);

/**
 * This function reports the secure connections.
 *
 * @param[in] iterator_ptr
 * Iterator for this function. Initialized to -1 it reports the first
 * connection. The next returned values will report the next connections.
 * if -1 returns, no more connections are assigned to this socket.
 * Example:
 *    int16_t iter = -1;
 *    while(1) {
 *         rc = _getSecureConnection(&iter,...);
 *         if(iter < 0) break;
 *         do_some_thing(...);
 *    }
 *
 *
 * @param[in] onlyWithError
 * true -> report only connections with failures.
 * false -> report all connections
 *
 * @param[out] ipAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[out] port_ptr
 * returned port number of the connection endpoint
 */
void CoapClient_iterateSecureConnections(int16_t *iterator_ptr,
        bool onlyWithError,
        Ip_Address_T *ipAddr_ptr, Ip_Port_T *port_ptr);

/**
 * This function reports the errors of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] rc_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_COAP_SECURE_CONNECTION_ERROR
 */
retcode_t CoapClient_getSecureConnError(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    retcode_t *rc_ptr);

/**
 * This function reports the status of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] state_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_COAP_SECURE_CONNECTION_ERROR
 */
retcode_t CoapClient_getSecureConnState(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    SecureConnectionState_T *state_ptr);

#endif /* SERVAL_ENABLE_DTLS */

#endif /* SERVAL_ENABLE_COAP_CLIENT */
#endif /* SERVAL_COAPCLIENT_H_ */
