/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file       Serval_XTcpClient.h
 *
 * @brief      The interface description of a proprietary TCP Client based
 *             application protocol.
 *
 * @since 1.4
 */

#ifndef SERVAL_XTCP_CLIENT_H_
#define SERVAL_XTCP_CLIENT_H_

#include <Serval_Defines.h>

#if SERVAL_ENABLE_XTCP && SERVAL_ENABLE_XTCP_CLIENT

#include <Serval_Types.h>
#include <PIp.h>
#include <Serval_Msg.h>
#include <Serval_XTcp.h>

/**
 * @brief      Initialize the XTCP client including all needed modules.
 *
 * @retval     RC_OK on successful
 * @retval     RC_XTCP_CLIENT_INIT_ERROR if an error occurs while initializing
 *                                       the XTcp Client
 *
 * @par        Example
 *             Initialize a XTcp Client
 *
 * @code
 *             // Initialize a TCP Client
 *             if (RC_OK != XTcpClient_initialize())
 *             {
 *                 printf("Util and xTCP initialization failure!\n\r");
 *             }
 * @endcode
 */
retcode_t XTcpClient_initialize(void);

/**
 * @brief      Initiate a client side, generic TCP connection with a server.
 *             Note that the connection is not opened immediately after this
 *             function has returned, since the three-way TCP handshake needs
 *             to complete first. It is still possible to use the connection
 *             for transmitting data, but there might be an additional, initial
 *             delay.
 *
 * @param[in]  serverIp
 *             Ip address of the server to connect to.
 *
 * @param[in]  serverPort
 *             The port which is used to send and receive messages.
 *             It has to be a valid port number (!=0).
 *
 * @param[in]  appSndCallback
 *             Callback function invoked when
 *             a) data is received in which case the status passed is RC_OK
 *             b) the connection has been closed in which case
 *             RC_XTCP_CONNECTION_CLOSED is passed
 *             c) the payload sent by XTcp_push has been sent in which case
 *             RC_XTCP_SENT is passed
 *             d) an error has occurred in which case the error code is passed
 *             to the callback. Most notably, a RC_XTCP_CONNECTION_FAILED error
 *             code is passed to the callback if the connection attempt has
 *             timed out.
 *
 * @param[in]  appRcvCallback
 *             Callback function invoked when
 *             a) data is sent in which case the status passed is RC_OK
 *             b) the connection has been closed in which case
 *             RC_XTCP_CONNECTION_CLOSED is passed
 *             c) the payload sent by XTcp_push has been sent in which case
 *             RC_XTCP_SENT is passed
 *             d) an error has occurred in which case the error code is passed
 *             to the callback. Most notably, a RC_XTCP_CONNECTION_FAILED error
 *             code is passed to the callback if the connection attempt has
 *             timed out.
 *
 * @retval     RC_OK on success or <br>
 * @retval     RC_XTCP_SOCKET_ERROR if opening the TCP socket fails.
 *
 * @par        Example
 *             Start a Tcp Client
 *
 * @code
 *             retcode_t rc;
 *             uint16_t serverPort = 9999;
               const char * serverIp = "192.168.0.101";
 *             Ip_Address_T destAddress;
 *
 *             // Change Port format from host to network. Needed for xTCP module
 *             Ip_Port_T destPort = Ip_convertIntToPort(serverPort);
 *
 *             // Convert IP address format
 *             Ip_convertStringToAddr(serverIp, &destAddress);
 *             destAddress = Basics_htonl(destAddress);
 *
 *             rc = XTcpClient_connect(destAddress, destPort, appSendCallback, appReceiveCallback);
 *             if (RC_OK == rc)
 *             {
 *                 printf("TCP Client Starting. Trying connecting to %s:%d\r\n", serverIp, serverPort);
 *             }
 *             else
 *             {
 *                 printf("TCP Client not started; RC = %i\r\n", rc);
 *             }
 * @endcode
 *
 * @see        XTcpServer_listen()
 */
retcode_t XTcpClient_connect(const Ip_Address_T serverIp, Ip_Port_T serverPort, AppXTcpCallback_T appSndCallback, AppXTcpCallback_T appRcvCallback);

#if SERVAL_ENABLE_TLS_CLIENT
/**
 * @brief      Initiate a secure client side, generic TCP connection with a server.
 *             Note that the connection is not opened immediately after this
 *             function has returned, since the three-way TCP handshake needs
 *             to complete first. It is still possible to use the connection
 *             for transmitting data, but there might be an additional, initial
 *             delay.
 *
 * @param[in]  serverIp
 *             Ip address of the server to connect to.
 *
 * @param[in]  serverPort
 *             The secure port which is used to send and receive messages.
 *             It has to be a valid port number (!=0).
 *
 * @param[in]  appSndCallback
 *             Callback function invoked when
 *             a) data is received in which case the status passed is RC_OK
 *             b) the connection has been closed in which case
 *             RC_XTCP_CONNECTION_CLOSED is passed
 *             c) the payload sent by XTcp_push has been sent in which case
 *             RC_XTCP_SENT is passed
 *             d) an error has occurred in which case the error code is passed
 *             to the callback. Most notably, a RC_XTCP_CONNECTION_FAILED error
 *             code is passed to the callback if the connection attempt has
 *             timed out.
 *
 * @param[in]  appRcvCallback
 *             Callback function invoked when
 *             a) data is sent in which case the status passed is RC_OK
 *             b) the connection has been closed in which case
 *             RC_XTCP_CONNECTION_CLOSED is passed
 *             c) the payload sent by XTcp_push has been sent in which case
 *             RC_XTCP_SENT is passed
 *             d) an error has occurred in which case the error code is passed
 *             to the callback. Most notably, a RC_XTCP_CONNECTION_FAILED error
 *             code is passed to the callback if the connection attempt has
 *             timed out.
 *
 * @retval     RC_OK on success or <br>
 * @retval     RC_XTCP_SOCKET_ERROR if opening the TCP socket fails.
 *
 * @par        Example
 *             Start a Secure Tcp Client
 *
 * @code
 *             retcode_t rc;
 *             uint16_t serverSecurePort = 9999;
               const char * serverIp = "192.168.0.101";
 *             Ip_Address_T destAddress;
 *
 *             // Change Port format from host to network. Needed for xTCP module
 *             Ip_Port_T destPort = Ip_convertIntToPort(serverSecurePort);
 *
 *             // Convert IP address format
 *             Ip_convertStringToAddr(serverIp, &destAddress);
 *             destAddress = Basics_htonl(destAddress);
 *
 *             rc = XTcpClient_connectSecure(destAddress, destPort, appSendCallback, appReceiveCallback);
 *             if (RC_OK == rc)
 *             {
 *                 printf("TCP Secure Client Starting. Trying connecting to %s:%d\r\n", serverIp, serverSecurePort);
 *             }
 *             else
 *             {
 *                 printf("TCP Secure Client not started; RC = %i\r\n", rc);
 *             }
 * @endcode
 *
 * @see        XTcpServer_listenSecure()
 */
retcode_t XTcpClient_connectSecure(const Ip_Address_T serverIp, Ip_Port_T serverPort, AppXTcpCallback_T appSndCallback, AppXTcpCallback_T appRcvCallback);
#endif /* SERVAL_ENABLE_TLS_CLIENT */

#endif /* SERVAL_ENABLE_XTCP && SERVAL_ENABLE_XTCP_CLIENT */

#endif /* SERVAL_XTCP_CLIENT_H_ */
