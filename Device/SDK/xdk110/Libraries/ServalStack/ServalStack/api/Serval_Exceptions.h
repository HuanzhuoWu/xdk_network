/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Exceptions.h
 * @brief Serval ExceptionHandling
 * @ingroup utilgroup
 *
 * This interface provides type definitions, data types and functions to handle
 * exceptions.
 *
 * The error code is designed to fit into a single, 16 bit integer
 * The higher bits representing the module, the lower bits represent
 * the code within the module. The two high bits of the module
 * are interpreted as the severity of the error code.
 *
 * The stack itself uses modules in the range 1..127.
 * Thus an application may use values 128..255 for own purposes.
 *
 * M is the module, S the severity and C the actual error code
 *  1
 *  5      8        0
 * +--------+--------+
 * |MMMMMMMM|SSCCCCCC|
 * +--------+--------+
 *
 * This yields 255 modules and 255 codes per module.
 * In the naive construction like it is done below,
 * the severity is treated separately and thus only 63 codes per
 * module are actually used. This has not lead to any problems so far.
 *
 */

#ifndef SERVAL_EXCEPTIONS_H_
#define SERVAL_EXCEPTIONS_H_

/* error severity */
#define RC_SEVERITY_MASK    0x00C0
#define RC_SHIFT_SEVERITY        6

/* Severity Fatal Error	-> Application may reset the device */
#define RC_SEVERITY_FATAL     (0x3 << RC_SHIFT_SEVERITY)
/* Major-Error  -> Application may reset the device or try to reinit */
#define RC_SEVERITY_MAJOR     (0x2 << RC_SHIFT_SEVERITY)
/* Local Error but without longer consequences (example: data loose) */
#define RC_SEVERITY_MODERATE  (0x1 << RC_SHIFT_SEVERITY)
/* There was a local error without consequences or external error */
#define RC_SEVERITY_MINOR     (0x0 << RC_SHIFT_SEVERITY)

/* error location (module/component) */
#define RC_MODULE_MASK      0xFF00
#define RC_SHIFT_MODULE          8

#define RC_MODULE_MISC           (0x01 << RC_SHIFT_MODULE)
#define RC_MODULE_IP             (0x02 << RC_SHIFT_MODULE)
#define RC_MODULE_UDP            (0x03 << RC_SHIFT_MODULE)
#define RC_MODULE_TCP            (0x04 << RC_SHIFT_MODULE)
#define RC_MODULE_HTTP           (0x05 << RC_SHIFT_MODULE)
#define RC_MODULE_WEBSERVER      (0x06 << RC_SHIFT_MODULE)
#define RC_MODULE_DPWS           (0x07 << RC_SHIFT_MODULE)
#define RC_MODULE_TEST           (0x08 << RC_SHIFT_MODULE)
#define RC_MODULE_MSG            (0x09 << RC_SHIFT_MODULE)
#define RC_MODULE_XUDP           (0x0A << RC_SHIFT_MODULE)
#define RC_MODULE_COAP           (0x0B << RC_SHIFT_MODULE)
#define RC_MODULE_REST           (0x0C << RC_SHIFT_MODULE)
#define RC_MODULE_DTLS           (0x0D << RC_SHIFT_MODULE)
#define RC_MODULE_FILESYSTEM     (0x0E << RC_SHIFT_MODULE)
#define RC_MODULE_SCHED          (0x0F << RC_SHIFT_MODULE)
#define RC_MODULE_THREADING      (0x10 << RC_SHIFT_MODULE)
#define RC_MODULE_TIMER          (0x11 << RC_SHIFT_MODULE)
#define RC_MODULE_NVDATA         (0x12 << RC_SHIFT_MODULE)
#define RC_MODULE_PROFILING      (0x13 << RC_SHIFT_MODULE)
#define RC_MODULE_CLOCK          (0x14 << RC_SHIFT_MODULE)
#define RC_MODULE_MONITOR        (0x15 << RC_SHIFT_MODULE)
#define RC_MODULE_DUTYCYCLING    (0x16 << RC_SHIFT_MODULE)
#define RC_MODULE_LOG            (0x17 << RC_SHIFT_MODULE)
#define RC_MODULE_APPDATA        (0x18 << RC_SHIFT_MODULE)
#define RC_MODULE_XTCP           (0x19 << RC_SHIFT_MODULE)
#define RC_MODULE_LWM2M          (0x1A << RC_SHIFT_MODULE)
#define RC_MODULE_STACKSPY       (0x29 << RC_SHIFT_MODULE)

/* error code module specific */
#define RC_ERRORCODE_MASK     0x003F

/* Macros for Severity of error code */
#define RC_GET_SEVERITY(x)    ((x)&RC_SEVERITY_MASK)

/* Macros for module of error code */
#define RC_GET_MODULE(x)      ((x)&RC_MODULE_MASK)

/* Macros for Error code module specific */
#define RC_GET_ERRORCODE(x)   ((x)&RC_ERRORCODE_MASK)

#define RC_RESOLVE_FORMAT_STR "M=0x%02X, E=0x%02X, S=0x%X (0x%04X)"
/**
 * @deprecated
 * Use RC_RESOLVE_FORMAT_STR instead of this
 */
#define RC_FORMAT_STR "EXCEPTION M=0x%02X, E=0x%02X, S=0x%X (0x%04X)"
#define RC_RESOLVE(rc) \
		(RC_GET_MODULE(rc)>>RC_SHIFT_MODULE), \
		(RC_GET_ERRORCODE(rc)), \
		(RC_GET_SEVERITY(rc)>>RC_SHIFT_SEVERITY), \
		(rc)

/** Return Codes */
enum retcode_e
{
    /** Success (equal to zero) */
    RC_OK = 0,


    /* Return codes which cannot be assigned to a module (RC_MODULE_MISC)
     * -----------------------------------------------------------------------------
     */
    /** A stack error which cannot be specified any further */
    RC_SERVAL_ERROR =
        0x01 | RC_MODULE_MISC | RC_SEVERITY_MAJOR,

    /** A platform error which cannot be specified any further */
    RC_PLATFORM_ERROR =
        0x02 | RC_MODULE_MISC | RC_SEVERITY_MAJOR,

    /** An application error which cannot be specified any further */
    RC_APP_ERROR =
        0x02 | RC_MODULE_MISC | RC_SEVERITY_MAJOR,

    RC_FATAL_NULLPTR =
        0x03 | RC_MODULE_MISC | RC_SEVERITY_FATAL,

    RC_DESERIALIZE_ERROR =
        0x03 | RC_MODULE_MISC | RC_SEVERITY_MINOR,

    RC_STRING_DESCRIPTOR_ERROR =
        0x04 | RC_MODULE_MISC | RC_SEVERITY_MINOR,

    RC_PREINITIALIZE_ERROR =
        0x05 | RC_MODULE_MISC | RC_SEVERITY_FATAL,

    RC_UTIL_INIT_ERROR =
        0x06 | RC_MODULE_MISC | RC_SEVERITY_MAJOR,

    RC_INVALID_STATUS =
        0x07 | RC_MODULE_MISC | RC_SEVERITY_FATAL,

    RC_NOT_IMPLEMENTED =
        0x08 | RC_MODULE_MISC | RC_SEVERITY_FATAL,

    RC_CONTENTBASE_ERROR =
        0x09 | RC_MODULE_MISC | RC_SEVERITY_MAJOR,


    /* Return codes for IP (RC_MODULE_IP)
    * -----------------------------------------------------------------------------
    */
    RC_IP_OUT_OF_MEMORY =
        0x00 | RC_MODULE_IP | RC_SEVERITY_FATAL,

    RC_IP_BUFFER_ERR =
        0x01 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_ROUTING_ERR =
        0x02 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_ERROR =					 /* for example not a INET-Socket type */
        0x03 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_SOCKET_ERROR =
        0x04 | RC_MODULE_IP | RC_SEVERITY_MODERATE,


    RC_IP_ADDRESS_IN_USE =
        0x05 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_NETIF_ERROR =
        0x06 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_OPERATION_IN_PROGRESS =
        0x07 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_SEND_ERROR =	/* error in lower layer send function */
        0x08 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    RC_IP_RCV_ERROR =   /* error in lower layer send function */
        0x09 | RC_MODULE_IP | RC_SEVERITY_MODERATE,

    // ToDo: validate severity and name
    RC_IP_INVALID_ADDRESS =
        0x10 | RC_MODULE_IP | RC_SEVERITY_MAJOR,


    /* Return codes for UDP (RC_MODULE_UDP)
    * -----------------------------------------------------------------------------
    */
    RC_UDP_SOCKET_BUSY =
        0x00 | RC_MODULE_UDP | RC_SEVERITY_MODERATE,

    RC_UDP_OUT_OF_MEMORY =
        0x01 | RC_MODULE_UDP | RC_SEVERITY_MODERATE,

    RC_UDP_OVERLOADED =
        0x02 | RC_MODULE_UDP | RC_SEVERITY_MODERATE,

    RC_UDP_INVALID_SOCKET =
        0x03 | RC_MODULE_UDP | RC_SEVERITY_MODERATE,

    RC_UDP_NO_FREE_PORT =
        0x04 | RC_MODULE_UDP | RC_SEVERITY_MAJOR,

    RC_UDP_PORT_ALREADY_USED =
        0x05 | RC_MODULE_UDP | RC_SEVERITY_MINOR,

    RC_UDP_MULTICAST_ERROR =
        0x06 | RC_MODULE_UDP | RC_SEVERITY_MAJOR,

    RC_UDP_SEND_ERROR =
        0x07 | RC_MODULE_UDP | RC_SEVERITY_MINOR,

    RC_UDP_RCV_ERROR =
        0x08 | RC_MODULE_UDP | RC_SEVERITY_MINOR,

    /** Any UDP socket error which don't match any of the specific error codes */
    RC_UDP_SOCKET_ERROR =
        0x09 | RC_MODULE_UDP | RC_SEVERITY_MAJOR,


    /* Return codes for TCP (RC_MODULE_TCP)
    * -----------------------------------------------------------------------------
    */
    RC_TCP_SOCKET_BUSY =
        0x00 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_OUT_OF_MEMORY =
        0x01 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_RETX =
        0x02 | RC_MODULE_TCP | RC_SEVERITY_MINOR,

    RC_TCP_OVERLOADED =
        0x03 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_PORT_ALREADY_USED =
        0x04 | RC_MODULE_TCP | RC_SEVERITY_MINOR,

    RC_TCP_NO_FREE_PORT =
        0x05 | RC_MODULE_TCP | RC_SEVERITY_MAJOR,

    RC_TCP_PORT_NOT_USED =
        0x06 | RC_MODULE_TCP | RC_SEVERITY_MINOR,

    RC_TCP_NO_REQUEST_TO_ACCEPT =
        0x07 | RC_MODULE_TCP | RC_SEVERITY_MINOR,

    RC_TCP_INVALID_LISTENER =
        0x08 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_CONNECTION_ABORT =
        0x09 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_CONNECTION_RESET =
        0x0A | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_CONNECTION_CLOSED =
        0x0B | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_NOT_CONNECTED =
        0x0C | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_CONNECTED =
        0x0D | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_TIMEOUT =
        0x0F | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_INVALID_SOCKET =
        0x10 | RC_MODULE_TCP | RC_SEVERITY_FATAL,

    RC_TCP_INVALID_PEER =
        0x11 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_SEND_ERROR =
        0x12 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    RC_TCP_RCV_ERROR =
        0x13 | RC_MODULE_TCP | RC_SEVERITY_MODERATE,

    /** Any TCP socket error which don't match any of the specific error codes */
    RC_TCP_SOCKET_ERROR =
        0x14 | RC_MODULE_TCP | RC_SEVERITY_MAJOR,


    /* Return codes for HTTP (RC_MODULE_HTTP)
    * -----------------------------------------------------------------------------
    */
    RC_HTTP_RESP_STATUS_ERROR =
        0x01 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_SESSION_NOT_ACTIVE =
        0x02 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_UNEXPECTED_ACTIVE_MSG =
        0x03 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_UNEXPECTED_ACTIVE_SESSION =
        0x04 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_INCONSISTENT_SESSION_STATUS =
        0x05 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_NO_FREE_SESSION =
        0x06 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_TOO_LONG_URL =
        0x07 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_CLIENT_CONNECT_ERROR =
        0x08 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_UNEXPECTED_INCOMING_DATA =
        0x09 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_INCOMING_DATA_INVAILD_SESSION =
        0x0A | RC_MODULE_HTTP | RC_SEVERITY_MAJOR,

    RC_HTTP_LISTEN_ERROR =
        0x0B | RC_MODULE_HTTP | RC_SEVERITY_MAJOR,

    RC_HTTP_CONNECTION_ERROR =
        0x0C | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_SEND_ERROR =
        0x0D | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_PIPELINING_NOT_SUPPORTED =
        0x0E | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_CHUNK_RECEIVED =
        0x0F | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_REALLOC_ERROR =
        0x10 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_PARSING_ERROR =
        0x11 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_MALFORMED_HEADER =
        0x12 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_HEADER_INCOMPLETE =
        0x13 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    /* UNUSED, left to document old code
    RC_HTTP_PARSER_URI_MISSING =
    0x14 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,
    */

    RC_HTTP_PARSER_URI_TOO_LONG =
        0x15 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    /* UNUSED, left to document old code
    RC_HTTP_PARSER_PROTOCOL_MISSING =
    0x16 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,
    */

    RC_HTTP_PARSER_INVALID_CONTENT_LENGTH =
        0x17 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_INVALID_CONTENT_TYPE =
        0x18 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_INVALID_TRANSFER_ENCODING =
        0x19 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_INVALID_CONNECTION_LINE =
        0x1A | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_UNSUPPORTED_METHOD =
        0x1B | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_UNSUPPORTED_PROTOCOL_VERSION =
        0x1C | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_INVALID_RESPONSE =
        0x1D | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_DEFRAGMENTATION_ERROR =
        0x1E | RC_MODULE_HTTP | RC_SEVERITY_MAJOR,

    RC_HTTP_INCOMING_DATA_CLOSING_SESSION =
        0x1F | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_EXPECT_HEADER =
        0x20 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_UNMODIFIED_HEADER =
        0x21 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_SERVER_INIT_ERROR =
        0x22 | RC_MODULE_HTTP | RC_SEVERITY_MAJOR,

    RC_HTTP_CLIENT_INIT_ERROR =
        0x23 | RC_MODULE_HTTP | RC_SEVERITY_MAJOR,

    RC_HTTP_PARSER_INVALID_AUTHORIZATION_TYPE =
        0x24 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_AUTH_INVALID_CREDENTIALS =
        0x25 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_INVALID_CHUNKED_ENCODING =
        0x26 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_PARSER_CHUNKED_ENCODING_ACTIVE =
        0x27 | RC_MODULE_HTTP | RC_SEVERITY_MINOR,

    RC_HTTP_PARSER_CHUNKED_WRONG_CONTENT_SIZE =
        0x28 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_CLIENT_PENDING_CONNECTION =
        0x29 | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_CLIENT_INIT_REQ_FAILED =
        0x2A | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,

    RC_HTTP_CLIENT_NO_RESPONSE =
        0x2B | RC_MODULE_HTTP | RC_SEVERITY_MODERATE,


    /* Return codes for Webserver (RC_MODULE_WEBSERVER)
     * -----------------------------------------------------------------------------
     */
    RC_WEBSERVER_UNSUPPORTED_OP =
        0x00 | RC_MODULE_WEBSERVER | RC_SEVERITY_MINOR,

    RC_WEBSERVER_URL_NOT_FOUND =
        0x01 | RC_MODULE_WEBSERVER | RC_SEVERITY_MINOR,

    RC_WEBSERVER_INTERNAL_ERROR =
        0x02 | RC_MODULE_WEBSERVER | RC_SEVERITY_MODERATE,

    RC_WEBSERVER_INIT_ERROR =
        0x03 | RC_MODULE_WEBSERVER | RC_SEVERITY_MAJOR,

    RC_WEBSERVER_START_ERROR =
        0x04 | RC_MODULE_WEBSERVER | RC_SEVERITY_MAJOR,


    /* Return codes for DPWS (RC_MODULE_DPWS)
     * -----------------------------------------------------------------------------
     */
    RC_DPWS_PARSING_ERROR =
        0x00 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_INVALID_ENDPOINT =
        0x01 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_INVALID_SERVICE =
        0x02 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_INVALID_ACTION =
        0x03 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_DUPLICATED_REQUEST_UUID =
        0x04 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_URL_NOT_FOUND =
        0x05 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_UNSUPPORTED_REFERENCE_PARAM =
        0x06 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_LISTEN_AT_DISCOVER_PORT_FAILED =
        0x07 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_JOIN_MCAST_FAILED =
        0x08 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_INIT_ERROR =
        0x09 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    RC_DPWS_PUSH_EVENT_FAILED =
        0x0A | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    RC_DPWS_NETWORK_BINDING_UNDEFINED_STATUS =
        0x0B | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    //ToDo: validate severity and name
    RC_DPWS_UUID_BUFF_TOO_SMALL =
        0x0C | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    //ToDo: validate severity and name
    RC_DPWS_SERVER_SEND_RESPONSE =
        0x0D | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_NO_FREE_EVENTING_SESSION =
        0x0E | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_HANDLE_EVENT_UNSUBSCRIPTION =
        0x0F | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_HANDLE_EVENT_RENEW =
        0x10 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_CREATE_NEW_SUBSCRIBER =
        0x11 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_NOT_IMPLEMENTED =
        0x12 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_HANDLE_NETWORK_BINDING_CHANGE =
        0x13 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_SEND_HELLO_FAILED =
        0x14 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_SEND_BYE_FAILED =
        0x15 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,
    //ToDo: validate severity and name
    RC_DPWS_RESPOND_FAILED =
        0x16 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    RC_DPWS_AUTH_TABLE_MISSING =
        0x17 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    //ToDo: validate severity and name
    RC_UUID_ERROR =
        0x18 | RC_MODULE_DPWS | RC_SEVERITY_FATAL,

    RC_DPWS_INVALID_OWN_IP =
        0x19 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    RC_DPWS_MATCH_BY_INVALID =
        0x1A | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_ACTION_NOT_SUPPORTED =
        0x1B | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_REQUEST_HEADER_INCOMPLETE =
        0x1C | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_SUBSCRIBE_ADDRESS_INVALID =
        0x1D | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_FILTERING_REQ_UNAVAILABLE =
        0x1E | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_DELIVERY_MODE_NOT_SUPPORTED =
        0x1F | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_START_ERROR =
        0x20 | RC_MODULE_DPWS | RC_SEVERITY_MAJOR,

    RC_DPWS_DISCOVERY_SEND_RESPONSE =
        0x21 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_OUT_RESOURCE_ERROR =
        0x22 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_IN_RESOURCE_ERROR =
        0x23 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_RPC_INCONSISTENT_ERROR =
        0x24 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_PROBE_UNSUPPORTED_SCOPE_RULE =
        0x25 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_UNICAST_DISCOVERY_NOT_SUPPORTED =
        0x26 | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,

    RC_DPWS_PROBE_NON_MATCHING_TYPE =
        0x27 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_PROBE_NON_MATCHING_SCOPE =
        0x28 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_RESOLVE_NON_MATCHING =
        0x29 | RC_MODULE_DPWS | RC_SEVERITY_MINOR,

    RC_DPWS_INVALID_MSG_ID =
        0x2A | RC_MODULE_DPWS | RC_SEVERITY_MODERATE,


    /* Return codes for Test (RC_MODULE_TEST)
    * -----------------------------------------------------------------------------
    */
    RC_TEST_ERROR =	 /* Only use in integration or module tests !!! */
        0x01 | RC_MODULE_TEST | RC_SEVERITY_MINOR,

    RC_TEST_ERROR_MAJOR =	 /* Only use in integration or module tests !!! */
        0x02 | RC_MODULE_TEST | RC_SEVERITY_MAJOR,


    /* Return codes for Msg Management (RC_MODULE_MSG)
    * -----------------------------------------------------------------------------
    */
    RC_MSG_FACTORY_ERROR =
        0x00 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_FACTORY_SIZE_ERROR =
        0x01 | RC_MODULE_MSG | RC_SEVERITY_FATAL,

    RC_MSG_FACTORY_ALREADY_COMMITTED =
        0x02 | RC_MODULE_MSG | RC_SEVERITY_FATAL,

    RC_MSG_NO_FREE_MSG =
        0x03 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_POOL_INVALID_BUFFER =
        0x04 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_INVALID_ALP_TYPE =
        0x05 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_TCP_SEND_ERROR =
        0x06 | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_TCP_RECEIVE_ERROR =
        0x07 | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_TCP_OVERLOAD =
        0x08 | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_UDP_OVERLOAD =
        0x09 | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_UDP_TOO_LONG_MSG =
        0x0A | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_UDP_SEND_ERROR =
        0x0B | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_FACTORY_INCOMPLETE =
        0x0C | RC_MODULE_MSG | RC_SEVERITY_MINOR,

    RC_MSG_SOCKET_ERROR =
        0x0D | RC_MODULE_MSG | RC_SEVERITY_MODERATE,

    RC_MSG_INIT_ERROR =
        0x0E | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_ALLOCATION_FAULT =
        0x0F | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_SERIALIZATION_FAILED =
        0x10 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_INVALID_DIRECTION =
        0x11 | RC_MODULE_MSG | RC_SEVERITY_MAJOR,

    RC_MSG_SENDING_DONE =
        0x12 | RC_MODULE_MSG | RC_SEVERITY_MINOR,


    /* Return codes for XUDP (RC_MODULE_XUDP)
     * -----------------------------------------------------------------------------
     */
    RC_XUDP_OVERLOADED =
        0x01 | RC_MODULE_XUDP | RC_SEVERITY_MINOR,

    RC_XUDP_SENDING_ERROR =
        0x02 | RC_MODULE_XUDP | RC_SEVERITY_MODERATE,

    RC_XUDP_SOCKET_ERROR =
        0x03 | RC_MODULE_XUDP | RC_SEVERITY_MODERATE,

    RC_XUDP_ASLEEP =
        0x04 | RC_MODULE_XUDP | RC_SEVERITY_MAJOR,

    RC_XUDP_PAYLOAD_TOO_LARGE =
        0x05 | RC_MODULE_XUDP | RC_SEVERITY_MAJOR,

    RC_XUDP_MSG_FACTORY_OVERFLOW =
        0x06 | RC_MODULE_XUDP | RC_SEVERITY_FATAL,

    RC_XUDP_INIT_ERROR =
        0x07 | RC_MODULE_XUDP | RC_SEVERITY_MAJOR,

    RC_XUDP_SOCKET_ACTIVE =
        0x08 | RC_MODULE_XUDP | RC_SEVERITY_MAJOR,


    /* Return codes for COAP (RC_MODULE_COAP)
    * -----------------------------------------------------------------------------
    */
    /** CoAP module */
    RC_COAP_INIT_ERROR =
        0x00 | RC_MODULE_COAP | RC_SEVERITY_FATAL,

    RC_COAP_LISTEN_ERROR =
        0x01 | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    RC_COAP_UNEXPECTED_ACTIVE_SESSION =
        0x02 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_NO_FREE_ENDPOINT =
        0x03 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_ENDPOINT_MONITOR_INIT_ERROR =
        0x04 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_INIT_ERROR =
        0x05 | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    RC_COAP_SERIALIZE_ERROR =
        0x06 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_PAYLOAD_TOO_LARGE =
        0x07 | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_END_OF_OPTIONS =
        0x08 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_ENCODING_ERROR =
        0x09 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_RESP_CODE_ERROR =
        0x0A | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_REQ_ERROR =
        0x0B | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_RESP_ERROR =
        0x0C | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_RECEIVE_ERROR =
        0x0D | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SENDING_ERROR =
        0x0E | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    // used to be 0x0F, now merged with RC_COAP_SENDING_ERROR
    RC_COAP_CLIENT_SEND_ERROR = RC_COAP_SENDING_ERROR,

    RC_COAP_INVALID_MSG_RECEIVED =
        0x10 | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_RESET_MSG_RECEIVED =
        0x11 | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_REQUEST_TO_CLIENT =
        0x12 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_REQUEST_TIMEOUT =
        0x13 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_DUPLICATE_MSG_RECEIVED =
        0x14 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_INIT_REQ_ERROR =
        0x15 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_SEND_RESET_ERROR =
        0x16 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CONFIRMABLE_NOT_ALLOWED =
        0x17 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SEND_RESET_MESSAGE =
        0x18 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_CLIENT_SESSION_ALREADY_ACTIVE =
        0x19 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_INIT_ERROR =
        0x1A | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    RC_COAP_SERVER_REQ_CODE_ERROR =
        0x1B | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_REQ_ERROR =
        0x1C | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_RESP_ERROR =
        0x1D | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_RECEIVE_ERROR =
        0x1E | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    // used to be 0x1F, now merged with RC_COAP_SENDING_ERROR
    RC_COAP_SERVER_SEND_ERROR = RC_COAP_SENDING_ERROR,

    RC_COAP_SERVER_INIT_REQ_ERROR =
        0x20 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_SEND_RESET_ERROR =
        0x21 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_SESSION_ALREADY_ACTIVE =
        0x22 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_OVERLOADED =
        0x23 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_SERVER_DELAYED_RESP =
        0x24 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_METHOD_NOT_ALLOWED =
        0x25 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_OVERLOAD_QUEUE_FULL =
        0x26 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_TOO_OLD_MSG_FOUND =
        0x27 | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    RC_COAP_MSG_RECEIVE_ERROR =
        0x28 | RC_MODULE_COAP | RC_SEVERITY_MAJOR,

    RC_COAP_CLIENT_RESP_CODE_FOR_FUTURE_USE =
        0x29 | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_ACK_MSG_RECEIVED =
        0x2A | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_SECURE_CONNECTION_ERROR =
        0x2B | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_NO_FREE_OBSERVER =
        0x2C | RC_MODULE_COAP | RC_SEVERITY_MODERATE,

    RC_COAP_DIFFERENT_OBSERVER =
        0x2D | RC_MODULE_COAP | RC_SEVERITY_MINOR,

    RC_COAP_INVALID_OBSERVER =
        0x2E | RC_MODULE_COAP | RC_SEVERITY_MODERATE,


    /* Return codes for REST (RC_MODULE_REST)
    * -----------------------------------------------------------------------------
    */
    RC_REST_SERVER_INIT_ERROR =
        0x00 | RC_MODULE_REST | RC_SEVERITY_MAJOR,

    RC_REST_SERVER_LISTEN_ERROR =
        0x01 | RC_MODULE_REST | RC_SEVERITY_MAJOR,

    RC_REST_BINDING_PARSING_ERROR =
        0x02 | RC_MODULE_REST | RC_SEVERITY_MAJOR,

    RC_REST_BINDING_NO_MORE_OPTIONS =
        0x03 | RC_MODULE_REST | RC_SEVERITY_MINOR,

    RC_REST_SERIALIZE_ERROR =
        0x04 | RC_MODULE_REST | RC_SEVERITY_MAJOR,

    RC_REST_BINDING_SEND_ERROR =
        0x05 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_BUFFER_TOO_SMALL =
        0x06 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_PARSER_UNSUPPORTED_METHOD =
        0x07 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_PARSER_NO_CONTENT_FORMAT =
        0x08 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_PARSER_UNSUPPORTED_CONTENT_FORMAT =
        0x09 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_HANDLE_REQUEST_ERROR =
        0x0A | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_SERVER_APP_NOT_ACCEPTABLE =
        0x0B | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_SERVER_SEND_ERROR =
        0x0C | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CREATE_RESPONSE_ERROR =
        0x0D | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_UNKNOWN_STATUS_CODE =
        0x0E | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CLIENT_INIT_ERROR =
        0x0F | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CLIENT_INIT_REQ_ERROR =
        0x10 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CLIENT_LISTEN_ERROR =
        0x11 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CREATE_REQUEST_ERROR =
        0x12 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_CLIENT_SEND_ERROR =
        0x13 | RC_MODULE_REST | RC_SEVERITY_MODERATE,

    RC_REST_SECURE_CONNECTION_ERROR =
        0x14 | RC_MODULE_REST | RC_SEVERITY_MODERATE,


    /* Return codes for DTLS (RC_MODULE_DTLS)
    * -----------------------------------------------------------------------------
    */
    RC_DTLS_ERROR_CONNECT =
        0x00 | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_ALREADY_CONNECTED =
        0x01 | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_NO_MORE_CONNECTIONS_FREE =
        0x02 | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_INIT_ERROR =
        0x03 | RC_MODULE_DTLS | RC_SEVERITY_FATAL,

    RC_DTLS_PLAIN_SOCKET  =
        0x06 | RC_MODULE_DTLS | RC_SEVERITY_MAJOR,

    RC_DTLS_NO_CONNECTION  =
        0x07 | RC_MODULE_DTLS | RC_SEVERITY_MODERATE,

    RC_DTLS_OVERLOADED =
        0x08 | RC_MODULE_DTLS | RC_SEVERITY_MODERATE,

    RC_DTLS_UNEXPECTED_PACKET =
        0x09 | RC_MODULE_DTLS | RC_SEVERITY_MODERATE,

    RC_DTLS_UNSUPPORTED_TOKEN =
        0x0A | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_TOKEN_NOT_PROVIDED =
        0x0B | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_PEER_REJECTED =
        0x0C | RC_MODULE_DTLS | RC_SEVERITY_MODERATE,

    RC_DTLS_INSUFFICIENT_MEMORY =
        0x0D | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_TOKEN_ERROR =
        0x0E | RC_MODULE_DTLS | RC_SEVERITY_MINOR,

    RC_DTLS_SECURITY_CALLBACK_REQUIRED =
        0x0F | RC_MODULE_DTLS | RC_SEVERITY_FATAL,


    /* Return codes for Filesystem (RC_MODULE_FILESYSTEM)
     * -----------------------------------------------------------------------------
     */
    RC_FILES_NO_SYSTEM =  /* POSIX: ESTALE */
        0x01 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,

    RC_FILES_BAD_NAME =   /* POSIX: ENAME */
        0x02 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,

    RC_FILES_ACCESS_ERROR = /* POSIX: EACCES */
        0x03 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,

    RC_FILES_TOO_MANY_OPEN = /* POSIX: ENFILE */
        0x04 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,

    RC_FILES_NOT_EXIST =  /* POSIX: ENOENT */
        0x05 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,

    RC_FILES_BAD_DSCR =   /* POSIX: EBADF */
        0x06 | RC_MODULE_FILESYSTEM | RC_SEVERITY_MAJOR,


    /* Return codes for Scheduler (RC_MODULE_SCHED)
    * -----------------------------------------------------------------------------
    */
    RC_SCHED_FULL =
        0x01 | RC_MODULE_SCHED | RC_SEVERITY_MAJOR,

    RC_SCHED_INVALID_JOB =
        0x02 | RC_MODULE_SCHED | RC_SEVERITY_MAJOR,

    RC_SCHED_EMPTY =
        0x03 | RC_MODULE_SCHED | RC_SEVERITY_MAJOR,


    /* Return codes for Thread management (RC_MODULE_THREADING)
    * -----------------------------------------------------------------------------
    */
    RC_MUTEX_UNLOCK_ERROR =
        0x01 | RC_MODULE_THREADING | RC_SEVERITY_FATAL,


    /* Return codes for Timer (RC_MODULE_TIMER)
    * -----------------------------------------------------------------------------
    */
    RC_TIMER_INVALID_OPERATION =
        0x01 | RC_MODULE_TIMER | RC_SEVERITY_MODERATE,

    RC_TIMER_INIT =							 /* cannot initialize Timer */
        0x02 | RC_MODULE_TIMER | RC_SEVERITY_MAJOR,

    RC_TIMER_ERROR_FATAL =
        0x03 | RC_MODULE_TIMER | RC_SEVERITY_FATAL,


    /* Return codes for NVData (RC_MODULE_NVDATA)
    * -----------------------------------------------------------------------------
    */
    RC_NV_MEMORY_ACCESS_ERROR =
        0x01 | RC_MODULE_NVDATA | RC_SEVERITY_FATAL,


    /* Return codes for Profiling (RC_MODULE_PROFILING)
    * -----------------------------------------------------------------------------
    */
    RC_PROFILING_PIN_ERROR =
        0x01 | RC_MODULE_PROFILING | RC_SEVERITY_MODERATE,


    /* Return codes for Clock (RC_MODULE_CLOCK)
    * -----------------------------------------------------------------------------
    */
    RC_CLOCK_ERROR_FATAL =
        0x01 | RC_MODULE_CLOCK | RC_SEVERITY_MAJOR,


    /* Return codes for Resources Monitor (RC_MODULE_MONITOR)
    * -----------------------------------------------------------------------------
    */
    RC_MONITOR_ERROR =
        0x01 | RC_MODULE_MONITOR | RC_SEVERITY_FATAL,

    RC_MONITOR_INIT_ERROR =
        0x02 | RC_MODULE_MONITOR | RC_SEVERITY_MAJOR,


    /* Return codes for Duty Cycling (RC_MODULE_DUTYCYCLING)
    * -----------------------------------------------------------------------------
    */
    RC_POWER_DOWN_VETO =
        0x10 | RC_MODULE_DUTYCYCLING | RC_SEVERITY_MINOR,

    RC_RETRY_SLEEP_LATER =
        0x18 | RC_MODULE_DUTYCYCLING | RC_SEVERITY_MINOR,

    RC_ALREADY_SLEEP =
        0x19 | RC_MODULE_DUTYCYCLING | RC_SEVERITY_MODERATE,

    RC_DOESNT_SLEEP =
        0x1A | RC_MODULE_DUTYCYCLING | RC_SEVERITY_MODERATE,

    RC_SLEEP_MODE_ERROR =
        0x1B | RC_MODULE_DUTYCYCLING | RC_SEVERITY_MAJOR,


    /* Return codes for Logging (RC_MODULE_LOG)
    * -----------------------------------------------------------------------------
    */
    RC_LOG_INIT_ERROR =
        0x01 | RC_MODULE_LOG | RC_SEVERITY_MAJOR,

    RC_LOG_LEVEL_NOT_SUPPORTED =
        0x02 | RC_MODULE_LOG | RC_SEVERITY_MINOR,


    /* Return codes for Application Data (RC_MODULE_APPDATA)
    * -----------------------------------------------------------------------------
    */
    RC_APPDATA_PARAM_NOT_SUPPORTED =
        0x01 | RC_MODULE_APPDATA | RC_SEVERITY_FATAL,

    RC_APPDATA_SIZE_NOT_FIT =
        0x02 | RC_MODULE_APPDATA | RC_SEVERITY_FATAL,


    /* Return codes for XTCP (RC_MODULE_XTCP)
     * -----------------------------------------------------------------------------
     */
    RC_XTCP_OVERLOADED =
        0x01 | RC_MODULE_XTCP | RC_SEVERITY_MINOR,

    RC_XTCP_SENDING_ERROR =
        0x02 | RC_MODULE_XTCP | RC_SEVERITY_MODERATE,

    RC_XTCP_RECEIVING_ERROR =
        0x03 | RC_MODULE_XTCP | RC_SEVERITY_MODERATE,

    RC_XTCP_SOCKET_ERROR =
        0x04 | RC_MODULE_XTCP | RC_SEVERITY_MODERATE,

    RC_XTCP_ASLEEP = /* Currently unused, placeholder for later use */
        0x05 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_PAYLOAD_TOO_LARGE =
        0x06 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_MSG_FACTORY_OVERFLOW =
        0x07 | RC_MODULE_XTCP | RC_SEVERITY_FATAL,

    RC_XTCP_INIT_ERROR =
        0x08 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_SOCKET_ACTIVE =
        0x09 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_START_ERROR =
        0x0A | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_CLIENT_INIT_ERROR =
        0x0B | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_SERVER_INIT_ERROR =
        0x0C | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_LISTEN_ERROR =
        0x0D | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_CONNECTION_ERROR =
        0x0E | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_CONNECTION_ESTABLISHED  =
        0x0F | RC_MODULE_XTCP | RC_SEVERITY_MINOR,

    RC_XTCP_CONNECTION_CLOSED =
        0x10 | RC_MODULE_XTCP | RC_SEVERITY_MINOR,

    RC_XTCP_CONNECTION_REFUSED =
        0x11 | RC_MODULE_XTCP | RC_SEVERITY_MINOR,

    RC_XTCP_INCOMING_DATA_INVAILD_SESSION=
        0x12 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_INCOMING_DATA_CLOSING_SESSION=
        0x13 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_NO_FREE_SESSION =
        0x14 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_INVALID_SESSION =
        0x15 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_UNEXPECTED_ACTIVE_MSG =
        0x16 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_SESSION_NOT_ACTIVE =
        0x17 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,

    RC_XTCP_UNEXPECTED_ACTIVE_SESSION =
        0x18 | RC_MODULE_XTCP | RC_SEVERITY_MAJOR,


    /* Return codes for LWM2M (RC_MODULE_LWM2M)
     * -----------------------------------------------------------------------------
     */
    RC_LWM2M_ENTITY_NOT_FOUND =
        0x01 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_BAD_REQUEST =
        0x02 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_UNRECOGNIZED_CRITICAL_OPTION =
        0x03 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_INTERNAL_ERROR =
        0x04 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_UNSUPPORTED_ACCEPT =
        0x05 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_ENTITY_TOO_LARGE =
        0x06 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_SERIALIZATION_ERROR =
        0x07 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_METHOD_NOT_ALLOWED =
        0x08 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_METHOD_UNAUTHORIZED =
        0x09 | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_PARSING_ERROR =
        0x0A | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_NO_FREE_OBSERVER =
        0x0B | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_NO_FREE_ATTRIBUTES =
        0x0C | RC_MODULE_LWM2M | RC_SEVERITY_MODERATE,

    RC_LWM2M_PARSER_END =
        0x0D | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,

    RC_LWM2M_BOOTSTRAP_INCOMPLETE =
        0x0E | RC_MODULE_LWM2M | RC_SEVERITY_MAJOR,

    RC_LWM2M_REGISTRATION_TIMEOUT =
        0x0F | RC_MODULE_LWM2M | RC_SEVERITY_MAJOR,

    RC_LWM2M_REGISTRATION_FAILED =
        0x10 | RC_MODULE_LWM2M | RC_SEVERITY_MAJOR,

    RC_LWM2M_DEREGISTRATION_FAILED =
        0x11 | RC_MODULE_LWM2M | RC_SEVERITY_MAJOR,

    RC_LWM2M_NOT_REGISTERED =
        0x12 | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,

    RC_LWM2M_OBJECTS_INTEGRITY_ERROR =
        0x13 | RC_MODULE_LWM2M | RC_SEVERITY_MAJOR,

    RC_LWM2M_PACKET_DURING_SLEEP =
        0x14 | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,

    RC_LWM2M_BOOTSTRAP_COMPLETE =
        0x15 | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,

    RC_LWM2M_UNAUTHORIZED =
        0x16 | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,

    RC_LWM2M_ENDPOINT_MONITOR_INIT_ERROR =
        0x17 | RC_MODULE_LWM2M | RC_SEVERITY_MINOR,


    /* Return codes for function call stack checks (RC_MODULE_STACKSPY)
    * -----------------------------------------------------------------------------
    */
    RC_STACKSPY_NO_FREE_ENTRY =
        0x29 | RC_MODULE_STACKSPY | RC_SEVERITY_MINOR,

    RC_STACKSPY_RESERVE =
        0x2A | RC_MODULE_STACKSPY | RC_SEVERITY_MAJOR,

    RC_STACK_OVERFLOW =
        0x2B | RC_MODULE_STACKSPY | RC_SEVERITY_FATAL,

    RC_STACK_WRONG_THREAD =
        0x2C | RC_MODULE_STACKSPY | RC_SEVERITY_MAJOR,

    RC_STACK_SUSPICIOUS_CHUNCKS =
        0x2D | RC_MODULE_STACKSPY | RC_SEVERITY_MODERATE,

    RC_STACK_UNVALID_THREADID =
        0x2E | RC_MODULE_STACKSPY | RC_SEVERITY_MODERATE,


    RC_MAX_SERVAL_ERROR = 0x7FFF,
    RC_FIRST_APP_ERROR  = 0x8000,
    RC_MAX_APP_ERROR    = 0xFFFF
};


#endif /* SERVAL_EXCEPTIONS_H_ */
