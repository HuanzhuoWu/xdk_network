/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Http.h
 * @brief Interface to HTTP constants
 * @ingroup httpgroup
 *
 * This module provides declarations which are commonly used by HTTP.
 */
#ifndef SERVAL_HTTP_H_
#define SERVAL_HTTP_H_

#include <Serval_Defines.h>
#include <Serval_Policy.h>
#if SERVAL_ENABLE_HTTP  /* if HTTP is disabled -> no compilation */

#include <Serval_Types.h>
#include <Serval_StringDescr.h>
#include <Serval_Msg.h>

/*
 * Enable SERVAL_ENABLE_HTTP_CHUNKED_RECEIVING in order to enable
 * the HTTP chunked Receiving Code/Feature
 */
#ifndef SERVAL_ENABLE_HTTP_CHUNKED_RECEIVING
#define SERVAL_ENABLE_HTTP_CHUNKED_RECEIVING  1
#endif

/*
 * Enable SERVAL_ENABLE_HTTP_AUTH in order to enable
 * the HTTP Authentication Code
 */
#ifndef SERVAL_ENABLE_HTTP_AUTH
#define SERVAL_ENABLE_HTTP_AUTH 1
#endif

/*
 * Enable SERVAL_ENABLE_HTTP_AUTH_DIGEST in order to enable
 * the HTTP Digest Authentication Code
 */
#ifndef SERVAL_ENABLE_HTTP_AUTH_DIGEST
#define SERVAL_ENABLE_HTTP_AUTH_DIGEST 1
#endif /* SERVAL_ENABLE_HTTP_AUTH_DIGEST */

/**
 * The maximal length of a URL. It is also used for Webservice action names and
 * endpoints.
 */
#ifndef SERVAL_HTTP_MAX_LENGTH_URL
#define SERVAL_HTTP_MAX_LENGTH_URL   128
#endif

/**
 * This is the maximal number of parallel HTTP sessions. By default,
 * SERVAL_HTTP_MAX_NUM_SESSIONS should be equal to SERVAL_MAX_NUM_MESSAGES, which
 * means that all supported requests may be HTTP requests. However, it is
 * possible to reduce this number if UDP messages are also expected.
 *
 * Note: Should be equal to actual number of TCP sockets provided on target
 * platform.
 */
#ifndef SERVAL_HTTP_MAX_NUM_SESSIONS
#define SERVAL_HTTP_MAX_NUM_SESSIONS       1
#endif

/**
 * Define the timeout in ms for garbage collection of HTTP sessions.
 * It should be a multiple of SERVAL_RESOURCE_MONITOR_PERIODE.
 * Suggested is a value 2-5 times as large as SERVAL_RESOURCE_MONITOR_PERIODE.
 */
#ifndef SERVAL_HTTP_SESSION_MONITOR_TIMEOUT
#define SERVAL_HTTP_SESSION_MONITOR_TIMEOUT 5000
#endif

/**
 * define wether HTTP keepalive session will be denied
 * this should be set to 1 if http session garbage is disabled
 */
/* TODO: implement this in the http code!!! */
#ifndef SERVAL_HTTP_SESSION_DENY_KEEP_ALIVE
#define SERVAL_HTTP_SESSION_DENY_KEEP_ALIVE 0
#endif

/**
 *  if SERVAL_ENABLE_HTTP_AUTH => automatic define user/pwd values
 */
#if SERVAL_ENABLE_HTTP_AUTH

#define SERVAL_HTTP_MAX_LEN_USER_NAME 10
#define SERVAL_HTTP_MAX_LEN_PASSWORD 10
#define SERVAL_HTTP_MAX_REALM_NAME 20

/**
 * Maximum length of the client-provided nonce.
 * Setting this value to low will cause digest authorization to fail.
 * However, it is stored in each message, so enlarging it requires additional
 * memory per HTTP message.
 * Firefox 40 uses 16 bytes
 * IE 9 uses 32 bytes
 */
#if SERVAL_ENABLE_HTTP_AUTH_DIGEST
#define SERVAL_HTTP_MAX_LENGTH_CNONCE 32
#endif

#if ! SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK
struct HttpAuthCredential_S
{
#if SERVAL_ENABLE_HTTP_AUTH_DIGEST
	char user[SERVAL_HTTP_MAX_LEN_USER_NAME + 1];
	char HA1[32];
#else
	char userpassb64[SERVAL_HTTP_MAX_LEN_USER_NAME
	                 + SERVAL_HTTP_MAX_LEN_PASSWORD + 6];
#endif /* SERVAL_ENABLE_HTTP_AUTH_DIGEST */
};

typedef struct HttpAuthCredential_S HttpAuthCredential_T;
#endif /* ! SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK */

#if SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK
/**
 * Callback function to authenticate the given user.
 * Called every time a HTTP request needs to be authenticated
 *
 * @param msg_ptr The message containing the request to be authenticated
 *
 * @param username The user who claims to have sent the request (and thus
 *                 requires validation)
 *
 * @return RC_HTTP_AUTH_INVALID_CREDENTIALS if the authentication failed
 *         RC_OK if the authentication succeeded
 */
typedef retcode_t (*HttpAuthenticationCallback_T)(Msg_T* msg_ptr, StringDescr_T* username);

/**
 * Function to validate a message against the provided HA1 digest.
 * This can be used in the authentication callback in order to check the credentials
 * provided in the message against the stored digest.
 * If the authentication scheme is basic, the stack computes the hash for comparison.
 * This means that the application does not need to store the plain text password.
 * Note that the API intentionally does not expose the possibility to compare plain
 * text passwords to discourage storing plain text passwords within the application.
 *
 * @param httpMsg_ptr the http message which contains credentials to be validated
 *
 * @param hashed_a1 This is the H(A1) value as defined per RFC2617.
 *                  For the supported security methods it is MD5(<username>:<realm>:password)
 *
 * @return RC_HTTP_AUTH_INVALID_CREDENTIALS if the authentication failed
 *         RC_OK if the authentication succeeded
 */
retcode_t HttpAuth_validateAuthentication(Msg_T* msg_ptr, char hashed_a1[32]);
#endif /* SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK */

struct HttpRealm_S
{
	/* Defines the realmname, used as an identifier */
	const char * realmname;
	/* Defines the realmurl / parh */
	char * realmurl;
#if ! SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK
	/* A pointer to an array of user credentials */
	HttpAuthCredential_T *Authcredentials;
	/* The length of the user credentials array */
	int32_t credentialsLen;
#else /* ! SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK */
	/* Callback for authenticating the user */
	HttpAuthenticationCallback_T authenticationCallback;
#endif /* ! SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK */
};

typedef struct HttpRealm_S HttpRealm_T;


#if SERVAL_ENABLE_HTTP_AUTH_DIGEST || SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK
/*
 * This defines the number of hex values used for the nonce.
 * E.g. 8 Hex values = 8 * 4bit = 32bit security.
 */
#ifndef SERVAL_HTTP_AUTH_NONCE_LENGTH
#define SERVAL_HTTP_AUTH_NONCE_LENGTH 8
#endif /* SERVAL_HTTP_AUTH_NONCE_LENGTH */

/* Typedef for MD5 */
typedef char unsigned Hash_T;
/* Typedef for MD5 in HEX */
typedef char unsigned HashHex_T;

#endif /* SERVAL_ENABLE_HTTP_AUTH_DIGEST || SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK */
#endif /* SERVAL_ENABLE_HTTP_AUTH */


/**
 * This data type represents a HTTP service.
 *
 * @see struct HttpServerInstance_S
 */
typedef struct HttpServerInstance_S HttpServerInstance_T;

/**
 * This data type represents a HTTP session and holds session related data.
 *
 * @see struct HttpSession_S
 */
typedef struct HttpSession_S HttpSession_T;

/** String value of HTTP protocol HTTP 1.0 */
extern const char Http_Protocol_V10[];

/** String value of HTTP protocol HTTP 1.1 */
extern const char Http_Protocol_V11[];


/** String value of HTTP GET request method */
extern const char Http_Method_Get[];

/** String value of HTTP POST request method */
extern const char Http_Method_Post[];

/** String value of HTTP PUT request method */
extern const char Http_Method_Put[];

/** String value of HTTP DELETE request method */
extern const char Http_Method_Delete[];

/** String value of HTTP HEAD request method */
extern const char Http_Method_Head[];

/** String value of an unknown HTTP request method */
extern const char Http_Method_Unknown[];


/** String value of HTTP header field, Content-Length */
extern const char Http_Header_ContentLength[];

/** String value of HTTP header field, Content-Type */
extern const char Http_Header_ContentType[];

/** String value of HTTP header field, Connection */
extern const char Http_Header_Connection[];

/** String value of HTTP header field, Expect */
extern const char Http_Header_Expect[];

/** String value of HTTP header field, name of the server */
extern const char Http_Header_Server[];

/** String value of HTTP header field, Cache-Control */
extern const char Http_Header_CacheControl[];

/** String value of HTTP header field, Transfer Encoding */
extern const char Http_Header_TransferEncoding[];

/** String value of HTTP header field, Authorization */
extern const char Http_Header_Authorization[];
/** String value of HTTP header field, AuthorizationInfo */
extern const char Http_Header_AuthenticationInfo[];


/** String value of Authorization for indicating Basic Authorization */
extern const char Http_AuthorizationType_Basic[];

/** String value of Authorization for indicating Digest Authorization */
extern const char Http_AuthorizationType_Digest[];

/** String value of Authorization for indicating Unknown Authorization */
extern const char Http_AuthorizationType_Unknown[];

/** String value of HTTP Header Option cnonce */
extern const char Http_AuthDigestOption_cnonce[];

/** String value of HTTP Header Option username */
extern const char Http_AuthDigestOption_username[];

/** String value of HTTP Header Option nonce */
extern const char Http_AuthDigestOption_nonce[];

/** String value of HTTP Header Option digesturi */
extern const char Http_AuthDigestOption_uri[];

/** String value of HTTP Header Option response */
extern const char Http_AuthDigestOption_response[];

/** String value of HTTP Header Option qop */
extern const char Http_AuthDigestOption_qop[];

/** String value of HTTP Header Option nonce count */
extern const char Http_AuthDigestOption_nc[];

/** Standard String value of HTTP Header Option field value nonce count */
extern const char Http_AuthDigestNcValue[];

/** String value of HTTP Header Option qop-option */
extern const char Http_AuthDigestQop_auth[];

/** String value of HTTP Header Option qop-option */
extern const char Http_AuthDigestQop_authint[];

/** String value of HTTP Header option realm */
extern const char Http_Authentication_realm[];

/** String value of HTTP Header option stale */
extern const char Http_AuthDigestOption_stale[];


/** String value of Cache-Control for indicating no cache-control */
extern const char Http_CacheControl_No[];

/** String value of Cache-Control for indicating cache-control */
extern const char Http_CacheControl_Yes[];


/** String value of Content Type for indicating Textual data */
extern const char Http_ContentType_Text_Plain[];

/** String value of Content Type for indicating HTML data */
extern const char Http_ContentType_Text_Html[];

/** String value of Content Type for indicating CSS data */
extern const char Http_ContentType_Text_Css[];

/** String value of Content Type for indicating XML data */
extern const char Http_ContentType_Text_Xml[];

/** String value of Content Type for indicating XML data */
extern const char Http_ContentType_App_Json[];

/** String value of Content Type for indicating XML data */
extern const char Http_ContentType_App_Xml[];

/** String value of Content Type for indicating XHTML data */
extern const char Http_ContentType_App_Xhtml[];

/** String value of Content Type for indicating EXI data */
extern const char Http_ContentType_App_Exi[];

/** String value of Content Type for indicating Portable Document Format */
extern const char Http_ContentType_App_Pdf[];

/** String value of Content Type for indicating SOAP */
extern const char Http_ContentType_App_Soap[];

/** String value of Content Type for indicating arbitrary binary data */
extern const char Http_ContentType_App_OctetStream[];

/** String value of Content Type for indicating GIF image */
extern const char Http_ContentType_Image_Gif[];

/** String value of Content Type for indicating Portable Network Graphics */
extern const char Http_ContentType_Image_Png[];

/** String value of Content Type for indicating JPEG JFIF image */
extern const char Http_ContentType_Image_Jpeg[];

/** String value of Content Type for indicating Microsoft Windows bitmap */
extern const char Http_ContentType_Image_Bmp[];

/** String value of Content Type for indicating icon */
extern const char Http_ContentType_Image_Ico[];

/** String value of an unknown Content Type */
extern const char Http_ContentType_Unknown[];


/** String value of Connection for indicating keep-alive */
extern const char Http_Connection_KeepAlive[];

/** String value of Connection for indicating non-persistent connection*/
extern const char Http_Connection_Close[];

/** String value of TE for indicating chunked encoding*/
extern const char Http_Encoding_Chunked[];


/** String value of space(SP) */
extern const char Http_Space[];

/** String value of CR-LF pairs */
extern const char Http_NewLine[];


/**
 * A selection of HTTP status codes, which are relevant
 * (i.e. generated or interpreted) by the our HTTP
 * implementation.
 */
typedef enum
{
	Http_StatusCode_Invalid = 0,
	Http_StatusCode_Continue = 100,
	Http_StatusCode_SwitchingProtocols = 101,
	Http_StatusCode_OK = 200,
	Http_StatusCode_Created = 201,
	Http_StatusCode_Accepted = 202,
	Http_StatusCode_NonAuthoritativeInformation = 203,
	Http_StatusCode_NoContent = 204,
	Http_StatusCode_ResetContent = 205,
	Http_StatusCode_PartialContent = 206,

	Http_StatusCode_MultipleChoices = 300,
	Http_StatusCode_MovedPermanently = 301,
	Http_StatusCode_Found = 302,
	Http_StatusCode_SeeOther = 303,
	Http_StatusCode_NotModified = 304,
	Http_StatusCode_UseProxy = 305,
	Http_StatusCode_TemporaryRedirect = 307,

	Http_StatusCode_BadRequest = 400,
	Http_StatusCode_Unauthorized = 401,
	Http_StatusCode_PaymentRequired = 402,
	Http_StatusCode_Forbidden = 403,
	Http_StatusCode_NotFound = 404,
	Http_StatusCode_MethodNotAllowed = 405,
	Http_StatusCode_NotAcceptable = 406,
	Http_StatusCode_ProxyAuthenticationRequired = 407,
	Http_StatusCode_RequestTimeout = 408,
	Http_StatusCode_Conflict = 409,
	Http_StatusCode_Gone = 410,
	Http_StatusCode_LengthRequired = 411,
	Http_StatusCode_PreconditionFailed = 412,
	Http_StatusCode_RequestEntityTooLarge = 413,
	Http_StatusCode_RequestURITooLarge = 414,
	Http_StatusCode_UnsupportedMediaType = 415,
	Http_StatusCode_RequestedRangeNotSatisfiable = 416,
	Http_StatusCode_ExpectationFailed = 417,

	Http_StatusCode_InternalServerError = 500,
	Http_StatusCode_NotImplemented = 501,
	Http_StatusCode_BadGateway = 502,
	Http_StatusCode_ServiceUnavailable = 503,
	Http_StatusCode_GatewayTimeout = 504,
	Http_StatusCode_HttpVersionNotSupported = 505,

} Http_StatusCode_T;


/**
 * This function is called to set the content type to be used in the response
 * for the given message.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_T object to modify. It has to be a valid pointer.
 *
 * @param[in] contentType_ptr
 * The content type to set.
 *
 * @see Http_ContentType_Text_Plain, Http_ContentType_Text_Html,
 * Http_ContentType_Image_Gif, Http_ContentType_Image_Png,
 * Http_ContentType_Image_Jpeg, Http_ContentType_Image_Bmp,
 * Http_ContentType_Image_Ico, Http_ContentType_App_Pdf,
 * Http_ContentType_App_Soap
 */
void HttpMsg_setContentType(Msg_T *msg_ptr, char const *contentType_ptr);

/**
 * This function is called to get the content type of the given message.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_T object to modify. It has to be a valid pointer.
 *
 * @see Http_ContentType_Text_Plain, Http_ContentType_Text_Html,
 * Http_ContentType_Image_Gif, Http_ContentType_Image_Png,
 * Http_ContentType_Image_Jpeg, Http_ContentType_Image_Bmp,
 * Http_ContentType_Image_Ico, Http_ContentType_App_Pdf,
 * Http_ContentType_App_Soap
 */
char const *HttpMsg_getContentType(Msg_T *msg_ptr);

/**
 * This function is called to set the HTTP status code to be used in the
 * response for the given message.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_T object to modify. It has to be a valid pointer.
 *
 * @param[in] respStatus
 * The HTTP status code to set in the web message.
 */
void HttpMsg_setStatusCode(Msg_T *msg_ptr, Http_StatusCode_T const respStatus);

/**
 * This function is called to get the status code of the given message.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_T object to read out the status code. It has to be a
 * valid pointer.
 *
 * @see Http_StatusCode_T
 */
Http_StatusCode_T HttpMsg_getStatusCode(Msg_T *msg_ptr);

/**
 * This function is called to set the HTTP Request Method (GET, POST, PUT, etc).
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @param[in] method
 * A reference to the string representing the method to be set.
 *
 * @see Http_Method_Unknown, Http_Method_Head, Http_Method_Get, Http_Method_Post
 * Http_Method_Put, Http_Method_Delete
 */
void HttpMsg_setReqMethod(Msg_T *msg_ptr, char const *method);

/**
 * It returns the HTTP method, e.g. GET, POST, PUT, etc., which is associated
 * with the given HTTP message.
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @return
 * A reference to the string representing the method. It can be also
 * Http_Method_Unknown.
 *
 * @see Http_Method_Unknown, Http_Method_Head, Http_Method_Get, Http_Method_Post
 * Http_Method_Put, Http_Method_Delete
 */
char const *HttpMsg_getReqMethod(Msg_T *msg_ptr);

/**
 * It returns a reference to the URL given in the HTTP message.
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @return
 * A reference to the URL.
 */
char const *HttpMsg_getReqUrl(Msg_T *msg_ptr);

/**
 * This function is called to set the URL in the HTTP Request given by the
 * message.
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @param[in] url_ptr
 * Pointer to URL to be stored in given HTTP Request message.
 *
 * @return
 * RC_OK on success<br>
 * RC_HTTP_TOO_LONG_URL if url is too long
 */
retcode_t HttpMsg_setReqUrl(Msg_T *msg_ptr, char const *url_ptr);

/**
 * It returns a reference to the beginning of the content of the HTTP message.
 *
 * @param[in] msg_ptr
 * A reference to the message including the HTTP message. It must be a valid
 * pointer to a valid message.
 *
 * @param[out] content_pptr
 * It will reference to the start of the content. It must be a valid pointer.
 *
 * @param[out] len_ptr
 * It will indicate the length of the content. It must be a valid pointer.
 */
void HttpMsg_getContent(Msg_T *msg_ptr, char const **content_pptr,
                        unsigned int *len_ptr);


/**
 *  Check HTTP Session Monitor period
 */
#if (SERVAL_HTTP_SESSION_MONITOR_TIMEOUT % SERVAL_RESOURCE_MONITOR_PERIODE)
#error SERVAL_HTTP_SESSION_MONITOR_TIMEOUT is not multiple of RESOURCE_MONITOR_PERIODE
#endif

#endif /* SERVAL_ENABLE_HTTP */
#endif /* SERVAL_HTTP_H_ */

