/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Defines.h
 * @brief Essential configurations of the library
 *
 * This header contains the essential configurations which the user can use to
 * customize the settings and resources of the library.
 */
#ifndef SERVAL_DEFINES_H
#define SERVAL_DEFINES_H


#define SERVAL_MACRO_VALUE_AUX(x) #x
#define SERVAL_MACRO_VALUE(x) SERVAL_MACRO_VALUE_AUX(x)


#ifndef SERVAL_ENABLE_REST_CLIENT
/**
 * Enable ''SERVAL_ENABLE_REST_CLIENT'' in order to enable the
 * RESTful client code.
 */
#define SERVAL_ENABLE_REST_CLIENT 1
#endif


#ifndef SERVAL_ENABLE_REST_SERVER
/**
 * Enable ''SERVAL_ENABLE_REST_SERVER'' in order to enable the
 * RESTful server code.
 */
#define SERVAL_ENABLE_REST_SERVER 1
#endif

#ifndef SERVAL_ENABLE_LWM2M
/**
 * Enable ''SERVAL_ENABLE_LWM2M'' in order to enable support for
 * the lightweight M2M protocol (device side).
 * @since 1.4
 */
#define SERVAL_ENABLE_LWM2M 0
#endif

/* Automatic enable/disable general REST code if REST Server or REST Client
 * is enabled
 */
#if SERVAL_ENABLE_REST_SERVER || SERVAL_ENABLE_REST_CLIENT
#define SERVAL_ENABLE_REST 1
#else
#define SERVAL_ENABLE_REST 0
#endif


#ifndef SERVAL_ENABLE_DPWS
/**
 * Enable ''SERVAL_ENABLE_DPWS'' in order to enable the DPWS code.
 */
#define SERVAL_ENABLE_DPWS 1
#endif


#ifndef SERVAL_ENABLE_WEBSERVER
/**
 * Enable ''SERVAL_ENABLE_WEBSERVER'' in order to enable
 * the Webserver code.
 */
#define SERVAL_ENABLE_WEBSERVER 1
#endif


#ifndef SERVAL_ENABLE_HTTP_CLIENT
/**
 * Enable ''SERVAL_ENABLE_HTTP_CLIENT'' in order to enable the
 * HTTP client code.
 */
#define SERVAL_ENABLE_HTTP_CLIENT 1
#endif


#ifndef SERVAL_ENABLE_HTTP_SERVER
/**
 * Enable ''SERVAL_ENABLE_HTTP_SERVER'' in order to enable the
 * HTTP server code.
 */
#define SERVAL_ENABLE_HTTP_SERVER 1
#endif

/* Automatic enable/disable general HTTP code if HTTP Server or HTTP Client
 * is enabled
 */
#if SERVAL_ENABLE_HTTP_SERVER || SERVAL_ENABLE_HTTP_CLIENT
#define SERVAL_ENABLE_HTTP 1
#else
#define SERVAL_ENABLE_HTTP 0
#endif


#ifndef SERVAL_ENABLE_COAP_SERVER
/**
 * Enable ''SERVAL_ENABLE_COAP_SERVER'' in order to enable
 * the COAP Server code.
 */
#define SERVAL_ENABLE_COAP_SERVER 1
#endif

#ifndef SERVAL_ENABLE_COAP_CLIENT
/**
 * Enable ''SERVAL_ENABLE_COAP_CLIENT'' in order to enable
 * the COAP Client code.
 */
#define SERVAL_ENABLE_COAP_CLIENT 1
#endif

#ifndef SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT
/**
 * Enable ''SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT'' in order
 * to use a single UDP socket for both server and client.
 * This requires both server and client
 * @since 1.4
 */
#define SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT 0
#endif

#if SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT
#if !SERVAL_ENABLE_COAP_SERVER
#warning enabling SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT enables SERVAL_ENABLE_COAP_SERVER
#undef SERVAL_ENABLE_COAP_SERVER
#define SERVAL_ENABLE_COAP_SERVER 1
#endif

#if !SERVAL_ENABLE_COAP_CLIENT
#warning enabling SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT enables SERVAL_ENABLE_COAP_CLIENT
#undef SERVAL_ENABLE_COAP_CLIENT
#define SERVAL_ENABLE_COAP_CLIENT 1
#endif
#endif

/* Automatic enable/disable general COAP code if COAP Server or
 * Client is enabled
 */
#if SERVAL_ENABLE_COAP_SERVER || SERVAL_ENABLE_COAP_CLIENT
#define SERVAL_ENABLE_COAP 1
#else
#define SERVAL_ENABLE_COAP 0
#endif


#ifndef SERVAL_ENABLE_XUDP
/**
 * Enable ''SERVAL_ENABLE_XUDP'' in order to enable XUDP component which
 * implements a basic proprietary UDP based application protocol.
 */
#define SERVAL_ENABLE_XUDP 1
#endif


#ifndef SERVAL_ENABLE_XTCP
/**
 * Enable ''SERVAL_ENABLE_XTCP'' in order to enable XTCP component which
 * implements a basic proprietary TCP based application protocol.
 * @since 1.4
 */
#define SERVAL_ENABLE_XTCP 0
#endif

#ifndef SERVAL_ENABLE_XTCP_SERVER
/**
 * Enable ''SERVAL_ENABLE_XTCP_SERVER'' in order to enable the
 * XTCP server code.
 * @since 1.4
 */
#define SERVAL_ENABLE_XTCP_SERVER 0
#endif

#ifndef SERVAL_ENABLE_XTCP_CLIENT
/**
 * Enable ''SERVAL_ENABLE_XTCP'' in order to enable XTCP component which
 * implements a basic proprietary TCP based application protocol.
 * @since 1.4
 */
#define SERVAL_ENABLE_XTCP_CLIENT 0
#endif


#ifndef SERVAL_ENABLE_TLS_SERVER
/**
 * Enable SERVAL_ENABLE_TLS_SERVER in order to enable
 * the Transport Layer Security (TLS) Code/Feature
 */
#define SERVAL_ENABLE_TLS_SERVER 0
#endif

#ifndef SERVAL_ENABLE_TLS_CLIENT
/**
 * Enable SERVAL_ENABLE_TLS_SERVER in order to enable
 * the Transport Layer Security (TLS) Code/Feature
 */
#define SERVAL_ENABLE_TLS_CLIENT 0
#endif

/* Automatic enable/disable general TLS code if TLS Server or
 * Client is enabled
 */
#if SERVAL_ENABLE_TLS_SERVER || SERVAL_ENABLE_TLS_CLIENT
#define SERVAL_ENABLE_TLS 1
#else
#define SERVAL_ENABLE_TLS 0
#endif


#ifndef SERVAL_ENABLE_DTLS_SERVER
/**
 * Enable SERVAL_ENABLE_DTLS_SERVER in order to enable
 * the Datagram Transport Layer Security (dTLS) Code/Feature
 */
#define SERVAL_ENABLE_DTLS_SERVER 0
#endif

#ifndef SERVAL_ENABLE_DTLS_CLIENT
/**
 * Enable SERVAL_ENABLE_DTLS_SERVER in order to enable
 * the Datagram Transport Layer Security (dTLS) Code/Feature
 */
#define SERVAL_ENABLE_DTLS_CLIENT 0
#endif /* SERVAL_ENABLE_DTLS_CLIENT */

/*
 * Automatic enable/disable general DTLS code if DTLS Server or
 * Client is enabled
 */
#if SERVAL_ENABLE_DTLS_SERVER || SERVAL_ENABLE_DTLS_CLIENT
#define SERVAL_ENABLE_DTLS 1
#else
#define SERVAL_ENABLE_DTLS 0
#endif


/*
 *******************************************************************************
 *  Parameters of Duty Cycling
 *******************************************************************************
 */

#ifndef SERVAL_ENABLE_APP_DATA_ACCESS
/**
 * Switch on or off the mechanism to read application data items by the stack
 * such as serial number, model name, etc.
 * This feature is currently only needed by DPWS.
 */
#define SERVAL_ENABLE_APP_DATA_ACCESS 1
#endif



#ifndef SERVAL_ENABLE_DUTY_CYCLING
/**
 * Switch on/off duty cycling of stack.
 */
#define SERVAL_ENABLE_DUTY_CYCLING 1
#endif


/*
 *******************************************************************************
 *  Generic Authorization Parameters
 *******************************************************************************
 */
#ifndef SERVAL_ENABLE_GENERIC_AUTHORIZATION
/**
 * Generic authorization framework.
 * Useful for cross-layer authentication and authorization.
 *
 * Slightly increases the size of a message by MAX(sizeof(uint16_t), sizeof(void*))
 */
#define SERVAL_ENABLE_GENERIC_AUTHORIZATION 0
#endif

/*
 *******************************************************************************
 *  Parameters of performance and scalability
 *******************************************************************************
 */

#ifndef SERVAL_MAX_NUM_MESSAGES
/**
 * The number of web requests which the stack can process in parallel
 */
#define SERVAL_MAX_NUM_MESSAGES			   1
#endif


#ifndef SERVAL_MAX_SIZE_APP_PACKET
/**
 * This constant defines the maximal size of the application payload of
 * an IP packet. Changing the value of this constant influences the number
 * of packets which are required to send a message. It also influence the
 * size of the communication buffers. Hence, it is a compromise between
 * memory consumption and communication performance.
 * A TCP-Packet (payload) should fit or be an fitting multiple of this number
 */
#define SERVAL_MAX_SIZE_APP_PACKET		   1460
#endif


/**
 * define the time period for the callbacks in of Resource monitor
 * in milliseconds.
 * It must be a value between 200 and 60.000
 */
#ifndef SERVAL_RESOURCE_MONITOR_PERIODE
#define SERVAL_RESOURCE_MONITOR_PERIODE 1000
#endif


/*
 *******************************************************************************
 *  Development Parameters
 *******************************************************************************
 */

#ifndef SERVAL_ENABLE_STACK_SPY
/**
 * Enable ''SERVAL_ENABLE_STACK_SPY'' in order to enable
 * the stack spy code.
 */
#define SERVAL_ENABLE_STACK_SPY 0
#endif

#ifndef SERVAL_ENABLE_PROFILING
/**
 * Enable ''SERVAL_ENABLE_PROFILING'' in order to enable the code related to the
 * time and energy measurement.
 */
#define SERVAL_ENABLE_PROFILING 0
#endif

/**
 * Switch on/off resource monitoring at all. If set to zero all subsequent
 * settings for monitoring are disabled and monitor is even not compiled
 * into the stack.
 */
#define SERVAL_ENABLE_RESOURCE_MONITOR 1

/*
 *******************************************************************************
 *  Internal Parameters
 *******************************************************************************
 */
#ifndef SERVAL_ENABLE_SERIALIZER
#define SERVAL_ENABLE_SERIALIZER ( SERVAL_ENABLE_DPWS || SERVAL_ENABLE_WEBSERVER )
#endif


/*
 *******************************************************************************
 *  Check Settings for consistency
 *******************************************************************************
 */

/* if SERVAL_ENABLE_DPWS => HTTP Server and Client must not be disabled
 */
#if SERVAL_ENABLE_DPWS
#if !SERVAL_ENABLE_HTTP_SERVER || !SERVAL_ENABLE_HTTP_CLIENT
#error SERVAL_ENABLE_HTTP_SERVER and SERVAL_ENABLE_HTTP_CLIENT must be enabled \
when DPWS is enabled
#endif
#if ! SERVAL_ENABLE_SERIALIZER
#error Serializer must not be explicitly disabled when using DPWS
#endif
#endif /* SERVAL_ENABLE_DPWS */

/* if SERVAL_ENABLE_DPWS => APP Data must not be disabled
 */
#if SERVAL_ENABLE_DPWS
#if !SERVAL_ENABLE_APP_DATA_ACCESS
#error SERVAL_ENABLE_APP_DATA_ACCESS must be enabled when DPWS is enabled
#endif
#endif /* SERVAL_ENABLE_DPWS */


/* if SERVAL_ENABLE_WEBSERVER => HTTP Server must not be disabled
 */
#if SERVAL_ENABLE_WEBSERVER
#if !SERVAL_ENABLE_HTTP_SERVER
#error SERVAL_ENABLE_HTTP_SERVER must be enabled when WEBERSER is enabled
#endif
#if ! SERVAL_ENABLE_SERIALIZER
#error Serializer must not be explicitly disabled when using Webserver
#endif
#endif /* SERVAL_ENABLE_WEBSERVER */


#if SERVAL_ENABLE_APP_DATA_ACCESS
#if !SERVAL_ENABLE_DPWS
#error SERVAL_ENABLE_APP_DATA_ACCESS is currently needed only together with \
DPWS. Disable SERVAL_ENABLE_APP_DATA_ACCESS or remove this dependency if it \
does not apply anymore.
#endif
#endif /* SERVAL_ENABLE_DPWS */

/* Check Resource Monitor period
 */
#if (SERVAL_RESOURCE_MONITOR_PERIODE < 200)
#error SERVAL_RESOURCE_MONITOR_PERIODE must be bigger or equal to 200 ms
#endif
#if (SERVAL_RESOURCE_MONITOR_PERIODE > 60000)
#error SERVAL_RESOURCE_MONITOR_PERIODE must be less or equal to 60.000 ms
#endif

#endif /* SERVAL_DEFINES_H */
