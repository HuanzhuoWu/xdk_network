/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Rest.h
 * @brief Interface to REST constants and methods
 * @ingroup restgroup
 *
 * This module provides declarations which are commonly used by REST.
 * This also provides the common interfaces provided by REST module e.g.
 * interfaces to parse a REST message.
 */
#ifndef SERVAL_REST_H_
#define SERVAL_REST_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_REST

/**
 * Enable ''SERVAL_ENABLE_REST_HTTP_BINDING'' in order to enable
 * the RESTful HTTP binding code.
 */
#ifndef SERVAL_ENABLE_REST_HTTP_BINDING
#define SERVAL_ENABLE_REST_HTTP_BINDING (SERVAL_ENABLE_REST && SERVAL_ENABLE_HTTP)
#endif

/**
 * Enable ''SERVAL_ENABLE_REST_COAP_BINDING'' in order to enable
 * the RESTful COAP binding code.
 */
#ifndef SERVAL_ENABLE_REST_COAP_BINDING
#define SERVAL_ENABLE_REST_COAP_BINDING (SERVAL_ENABLE_REST && SERVAL_ENABLE_COAP)
#endif

/**
 *  if SERVAL_ENABLE_REST => HTTP or COAP binding (or both) must be enabled
 */
#if SERVAL_ENABLE_REST && (!SERVAL_ENABLE_REST_HTTP_BINDING) && \
                         (!SERVAL_ENABLE_REST_COAP_BINDING)
#error COAP or HTTP binding must be enabled when REST is enabled
#endif

/**
 *  if SERVAL_ENABLE_REST_HTTP_BINDING => HTTP must be enabled
 */
#if SERVAL_ENABLE_REST_HTTP_BINDING && !SERVAL_ENABLE_HTTP
#error HTTP must be enabled when HTTP REST binding is enabled
#endif

/**
 *  if REST Client and HTTP binding => HTTP Client must be enabled
 */
#if SERVAL_ENABLE_REST_CLIENT && SERVAL_ENABLE_REST_HTTP_BINDING && \
   !SERVAL_ENABLE_HTTP_CLIENT
#error HTTP client must be enabled when REST client with HTTP binding is enabled
#endif

/**
 *  if REST Server and HTTP binding => HTTP Server must be enabled
 */
#if SERVAL_ENABLE_REST_SERVER && SERVAL_ENABLE_REST_HTTP_BINDING && \
   !SERVAL_ENABLE_HTTP_SERVER
#error HTTP server must be enabled when REST server with HTTP binding is enabled
#endif

/**
 *  if SERVAL_ENABLE_REST_COAP_BINDING => COAP must be enabled
 */
#if SERVAL_ENABLE_REST_COAP_BINDING && !SERVAL_ENABLE_COAP
#error COAP must be enabled when COAP REST binding is enabled
#endif

/**
 *  if REST Client and COAP binding => COAP Client must be enabled
 */
#if SERVAL_ENABLE_REST_CLIENT && SERVAL_ENABLE_REST_COAP_BINDING && \
   !SERVAL_ENABLE_COAP_CLIENT
#error COAP client must be enabled when REST client with COAP binding is enabled
#endif

/**
 *  if REST Server and COAP binding => COAP Server must be enabled
 */
#if SERVAL_ENABLE_REST_SERVER && SERVAL_ENABLE_REST_COAP_BINDING && \
   !SERVAL_ENABLE_COAP_SERVER
#error COAP server must be enabled when REST server with COAP binding is enabled
#endif


#include <Serval_Types.h>
#if SERVAL_ENABLE_REST_HTTP_BINDING
#include <Serval_Http.h>
#endif
#if SERVAL_ENABLE_REST_COAP_BINDING
#include <Serval_Coap.h>
#endif
#include <Serval_Msg.h>

/**
 * Enable ''SERVAL_ENABLE_REST_OBSERVATION'' in order to enable the possiblity
 * to observe resources of the REST server using CoAP.
 *
 * @since 1.4
 */
#ifndef SERVAL_ENABLE_REST_SERVER_OBSERVATION
#define SERVAL_ENABLE_REST_SERVER_OBSERVATION 0
#endif

/**
 * This is the length of the payload buffer of a HTTP REST message.
 */
#ifndef REST_HTTP_PAYLOAD_MAX_LEN
#define REST_HTTP_PAYLOAD_MAX_LEN    48
#endif

/**
 * This is the length of the uri buffer used in the REST server module.
 */
#define REST_MAX_URI_LEN 128


/** This enum defines the request methods */
typedef enum Rest_Method_E
{
	REST_GET = 1,
	REST_POST,
	REST_PUT,
	REST_DELETE,
} Rest_Method_T;


/** This enum defines the response codes */
typedef enum Rest_Code_E
{
	REST_CREATED                    =  65, /* 2.01 */
	REST_DELETED                    =  66, /* 2.02 */
	REST_VALID                      =  67, /* 2.03 */
	REST_CHANGED                    =  68, /* 2.04 */
	REST_CONTENT                    =  69, /* 2.05 */
	REST_BAD_REQUEST                = 128, /* 4.00 */
	REST_UNAUTHORIZED               = 129, /* 4.01 */
	REST_BAD_OPTION                 = 130, /* 4.02 */
	REST_FORBIDDEN                  = 131, /* 4.03 */
	REST_NOT_FOUND                  = 132, /* 4.04 */
	REST_METHOD_NOT_ALLOWED         = 133, /* 4.05 */
	REST_NOT_ACCEPTABLE             = 134, /* 4.06 */
	REST_PRECONDITION_FAILED        = 140, /* 4.12 */
	REST_REQUEST_ENTITY_TOO_LARGE   = 141, /* 4.13 */
	REST_REQUEST_URI_TOO_LARGE      = 142, /* 4.14 */
	REST_UNSUPPORTED_CONTENT_FORMAT = 143, /* 4.15 */
	REST_INTERNAL_SERVER_ERROR      = 160, /* 5.00 */
	REST_NOT_IMPLEMENTED            = 161, /* 5.01 */
	REST_BAD_GATEWAY                = 162, /* 5.02 */
	REST_SERVICE_UNAVAILABLE        = 163, /* 5.03 */
	REST_GATEWAY_TIMEOUT            = 164, /* 5.04 */
	REST_PROXYING_NOT_SUPPORTED     = 165, /* 5.05 */
} Rest_Code_T;


/** This enum defines the content formats */
typedef enum Rest_ContentFormat_E
{
	REST_CONTENT_FMT_TEXT_UTF8    =     0,
	REST_CONTENT_FMT_LINK_FORMAT  =    40,
	REST_CONTENT_FMT_XML          =    41,
	REST_CONTENT_FMT_OCTET_STREAM =    42,
	REST_CONTENT_FMT_EXI          =    47,
	REST_CONTENT_FMT_JSON         =    50,
	REST_CONTENT_FMT_MAX          = 65535
} Rest_ContentFormat_T;


/**
 *  This data structure represents a parsing context used during
 *  parsing of an incoming REST message.
 */
typedef struct RestParser_S
{
	/** A reference to the message to parse */
	Msg_T* msg_ptr;
#if SERVAL_ENABLE_REST_COAP_BINDING
	/** A CoAP parser for parsing CoAP messages */
	CoapParser_T coapParser;
#endif
} RestParser_T;

/** This enum gives the type of REST message being serialized */
typedef enum
{
	REST_RESPONSE = 0,
	REST_REQUEST
} RestSerializeType_T;

/**
 *  This data structure represents a serialization context used during
 *  serializing of an outgoing REST message.
 */
typedef struct RestSerializer_S
{
	/** A reference to the message to be used for serialization */
	Msg_T* msg_ptr;
#if SERVAL_ENABLE_REST_COAP_BINDING
	/** A CoAP serializer for serializing CoAP messages */
	CoapSerializer_T coapSerializer;
#endif
} RestSerializer_T;

/**
 * This data type identifies an ongoing rest communication initiated by rest
 * client and can be used to identify and relate a fault in communication e.g.
 * with the request sent.
 * */
typedef void RestSession_T;

/**
 * This function is called to initialize the context of the REST parser for
 * parsing a given message.
 *
 * @param[out] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[in] msg_ptr
 * Pointer to message which should be parsed. The content will not be changed.
 * It has to be a valid pointer.
 */
void RestParser_setup(RestParser_T *parser_ptr, Msg_T *msg_ptr);

/**
 * This function retrieves the payload of an incoming message. The
 * implementation of this function will parse through all the options in case of
 * CoAP is used.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] payload_pptr
 * Output parameter for the payload reference. It must be a valid pointer.
 *
 * @param[out] payloadLen_ptr
 * Output paramter for the payload length. It must be a valid pointer.
 */
#if SERVAL_POLICY_LARGE_PAYLOAD == 1
void RestParser_getPayload(RestParser_T *parser_ptr,
                           uint8_t const **payload_pptr, uint16_t *payloadLen_ptr);
#else
void RestParser_getPayload(RestParser_T *parser_ptr,
                           uint8_t const **payload_pptr, uint8_t *payloadLen_ptr);
#endif

/**
 * This function retrieves the Method of the given message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context. It has to be a valid pointer.

 * @param[out] method_ptr
 * Reference to a Rest_Method_T object were the Method of the message should be
 * copied to.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_PARSER_UNSUPPORTED_METHOD if the Method is not known.
 */
retcode_t RestParser_getMethod(RestParser_T *parser_ptr,
                               Rest_Method_T *method_ptr);

/**
 * This function retrieves the Code of the given message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context. It has to be a valid pointer.

 * @param[out] code_ptr
 * Reference to a Rest_Code_T object were the Code of the message should be
 * copied to.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_UNKNOWN_STATUS_CODE if the Code is not known.
 */
retcode_t RestParser_getCode(RestParser_T *parser_ptr, Rest_Code_T *code_ptr);

/**
 * This function retrieves the URI Path as a string of an incoming message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] buffer_ptr
 * Reference to the buffer which is provided for storing the URI Path. The
 * returned string is null terminated.
 *
 * @param[in] bufferLen
 * Length of the allocated buffer which is provided to store the URI Path.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_BUFFER_TOO_SMALL if the string is larger than the provided buffer.
 */
retcode_t RestParser_getUriPath(RestParser_T *parser_ptr, char *buffer_ptr,
                                uint8_t bufferLen);


/**
 * This function retrieves the Location Path as a string of an incoming message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] buffer_ptr
 * Reference to the buffer which is provided for storing the Location Path. The
 * returned string is null terminated.
 *
 * @param[in] bufferLen
 * Length of the allocated buffer which is provided to store the Location Path.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_BUFFER_TOO_SMALL if the string is larger than the provided buffer.
 */
retcode_t RestParser_getLocationPath(RestParser_T *parser_ptr, char *buffer_ptr,
                                     uint8_t bufferLen);

/**
 * This function retrieves the Location Query as a string of an incoming
 * message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] buffer_ptr
 * Reference to the buffer which is provided for storing the Location Query. The
 * returned string is null terminated.
 *
 * @param[in] bufferLen
 * Length of the allocated buffer which is provided to store the Location Query.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_BUFFER_TOO_SMALL if the string is larger than the provided buffer.
 */
retcode_t RestParser_getLocationQuery(RestParser_T *parser_ptr,
                                      char *buffer_ptr, uint8_t bufferLen);

/**
 * This function retrieves the URI Query as a string of an incoming message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] buffer_ptr
 * Reference to the buffer which is provided for storing the URI Query. The
 * returned string is null terminated.
 *
 * @param[in] bufferLen
 * Length of the allocated buffer which is provided to store the URI Query.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_BUFFER_TOO_SMALL if the string is larger than the provided buffer.
 */
retcode_t RestParser_getUriQuery(RestParser_T *parser_ptr, char *buffer_ptr,
                                 uint8_t bufferLen);

/**
 * This function retrieves the accepted formats which are stored in the message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] formatBuffer_ptr
 * Reference to the buffer which is provided for storing the different
 * acceptable Content Formats.

 * @param[in] formatBufferLen
 * Length of the buffer which is provided for storing the acceptable Content
 * Formats.
 *
 * @param[out] numFormats_ptr
 * Reference to the uint8_t object where the extracted number of acceptable
 * Content Formats will be stored to.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_BUFFER_TOO_SMALL if the number of acceptable content formats defined
 * in the message is greater than the capacity of the given buffer
 * RC_REST_BINDING_PARSING_ERROR if an error has occurred during the parsing
 */
retcode_t RestParser_getAccept(RestParser_T *parser_ptr,
                               Rest_ContentFormat_T *formatBuffer_ptr, uint8_t formatBufferLen,
                               uint8_t *numFormats_ptr);

/**
 * This function retrieves the Content Format which is stored in the message.
 *
 * @param[in] parser_ptr
 * Reference to a RestParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[out] format_ptr
 * Reference to the Rest_ContentFormat_T which is provided for storing the
 * value of the Content Format option.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_PARSER_NO_CONTENT_FORMAT if the content format is not specified
 * RC_REST_BINDING_PARSING_ERROR if an error has occurred during the parsing
 * RC_REST_PARSER_UNSUPPORTED_CONTENT_FORMAT in case of an HTTP message if the
 * HTTP content type can not be mapped to a REST content format
 */
retcode_t RestParser_getContentFormat(RestParser_T *parser_ptr,
                                      Rest_ContentFormat_T *format_ptr);


#endif /* SERVAL_ENABLE_REST */
#endif /* SERVAL_REST_H_ */
