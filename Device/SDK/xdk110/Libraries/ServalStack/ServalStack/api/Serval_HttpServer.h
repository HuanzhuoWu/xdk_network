/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_HttpServer.h
 * @brief HTTP Server Module
 * @ingroup httpgroup
 *
 * The interface description of the HTTP server.
 */

#ifndef SERVAL_HTTPSERVER_H_
#define SERVAL_HTTPSERVER_H_
#include <Serval_Defines.h>
#if SERVAL_ENABLE_HTTP_SERVER  /* if HTTP server is disabled -> no compilation */

#include <Serval_Types.h>
#include <Serval_Http.h>
#include <Serval_Callable.h>
#include <Serval_Tcp.h>


/**
 * Initializes the HTTP server including all needed used modules.
 *
 * @return RC_OK if successful
 * TODO: Error handling!
 */
retcode_t HttpServer_initialize(void);


/**
 * This function opens a port and registers a handling function
 * for this port.
 * @param[in]	HttpServerInstance_ptr
 * pointer to the connection and port to be handled
 *
 * @return
 * RC_OK on success or RC_HTTP_LISTEN_ERROR if listening at the port fails.
 */
retcode_t HttpServer_start(HttpServerInstance_T *HttpServerInstance_ptr);


/**
 * This function is responsible for responding to an HTTP request. It is used by
 * upper protocols such the Webserver or DPWS to respond to incoming requests.
 * It builds the HTTP message header and hands the message to the MsgSender
 * to send it via TCP.
 *
 * @param[in]	msg_ptr
 * Pointer to the Msg_T element representing the request/response message.
 *
 * @param[in]	respStatus
 * The HTTP status code
 *
 * @param[in]	contentType
 * The content type of HTTP response (e.g. "application/soap+xml")
 *
 * @param[in]	alpCallback_ptr
 * Pointer to function that would act as a callback function when sending is
 * completed. This callback function will be invoked to notify the upper-layer
 * about the result of the sending.
 *
 * @return
 * RC_OK or RC_SERVAL_ERROR
 * TODO: Error handling!
 */
retcode_t HttpServer_respond(Msg_T *msg_ptr, Http_StatusCode_T respStatus,
                             const char *contentType, Callable_T *alpCallback_ptr);

/**
 * Converts a given host address to the correct format of either IPv4 or IPv6
 * depending on the definition of SERVAL_IPV6. In the case IPv6 the host IP
 * address will be put in brackets "[...]".
 *
 * @param[in] addr
 * The address to which the formatting is intended.
 *
 * @param[in,out] buf_ptr
 * The pointer to the buffer which will contain the converted address (zero
 * terminating). The
 * buffer must not be smaller than (SERVAL_IP_ADDR_LEN + 1), where
 * SERVAL_IP_ADDR_LEN is
 * the value specified by the Platform Adaption Layer (PAL) as the maximal
 * length of a serialized IP address.
 *
 * @return
 * String representing the formatted address in either IPv4 or IPv6, depending
 * on the definition of SERVAL_IPV6.
 */
char const *HttpServer_formatHost(Ip_Address_T *addr, char *buf_ptr);


#endif /* SERVAL_ENABLE_HTTP_SERVER */

#endif /* SERVAL_HTTPSERVER_H_ */
