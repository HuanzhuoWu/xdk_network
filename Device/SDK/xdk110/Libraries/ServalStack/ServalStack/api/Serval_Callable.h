/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Callable.h
 * @brief An implementation of function objects.
 * @ingroup utilgroup
 *
 * This module implements a data structure Callable_T which encapsulates a
 * function pointer. Callable_T elements are used as jobs which can be enqueued
 * into the scheduler queue such that they are invoked when the scheduler
 * processes its waiting queue. They are also used as callback functions in the
 * case of asynchronous (non-blocking) calls, such as sending.
 *
 */

#ifndef SERVAL_CALLABLE_H_
#define SERVAL_CALLABLE_H_

#include <Serval_Types.h>
#include <Serval_StructCtx.h>

/**
 * The data type for callable elements.
 *
 * @see struct Callable_S
 */
typedef struct Callable_S Callable_T;

/**
 * This data type represents a function pointer for the function carried by the
 * Callable_T element.
 *
 * The parameter callable_ptr generally refers to the Callable_T element
 * carrying this functions. This pointer is passed into the function in order to
 * allow the retrieval of the context structure using the macro
 * CALLABLE_GET_CONTEXT(). The other parameter status is used to
 * pass the status which should be signaled to the invoked function.
 *
 * @see CALLABLE_GET_CONTEXT()
 */
typedef retcode_t (*CallableFunc_T)(Callable_T *callable_ptr, retcode_t status);

/**
 * The data structure for callable elements.
 *
 * This data structure is used to hold a function pointer allowing it to be
 * scheduled as a scheduler job or to be used as a callback function. It is
 * highly recommended not to directly access the member of this data structure.
 * Instead the functions Callable_assign(), Callable_isAssigned(),
 * Callable_callback(), Callable_call(), Callable_clear() should be used.
 */
struct Callable_S
{
	CallableFunc_T func;
};


/**
 * This macro provides a pointer to the context structure of the given callable
 * object. It is assumed here that the callable object is member of a
 * struct. A pointer to the struct will be returned.
 *
 * @param[in] ContextTypeName
 * the name of the expected data type of the containing structure
 *
 * @param[in] ContextMemberNameOfCallable
 * the member name of the callable object inside the enclosing structure
 *
 * @param[in] callable_ptr
 * a pointer to the callable object
 *
 * @return a pointer to the enclosing structure which contains the given
 * callable object as a member.
 */
#define CALLABLE_GET_CONTEXT(\
   ContextTypeName, ContextMemberNameOfCallable, callable_ptr) \
   GET_CONTEXT_OF_MEMBER(ContextTypeName, ContextMemberNameOfCallable, \
      callable_ptr)

/**
 * This function assigns a function to the given Callable.
 *
 * @param[in] callable_ptr
 * Reference to a Callable_T object to which the function needs to be assigned.
 * It has to be a valid pointer.
 *
 * @param[in] function
 * Reference to a CallableFunc_T object, this holds the function to be assigned.
 *
 * @return pointer to Callable_T object.
 */
inline static Callable_T *Callable_assign(
    Callable_T* callable_ptr, CallableFunc_T function)
{
	callable_ptr->func = function;
	return callable_ptr;
}

/**
 * This function checks the given callable pointer on being valid (not NULL)
 * and a valid function is assigned to it.
 *
 * @param[in] callable_ptr
 * The reference of object Callable_T to check for the state.
 *
 * @return  TRUE if the callable pointer is valid (not NULL) and it holds a
 * valid callable function pointer. Otherwise, FALSE is returned.
 */
inline static bool Callable_isAssigned(Callable_T* callable_ptr)
{
	return (callable_ptr != null)
	       && (callable_ptr->func != (CallableFunc_T)null);
}


/**
 * This function is called to invoke the callable element. It propagates the
 * error code returned by the encapsulated function. also this function does not
 * handle any exception returned by the invoked callback, instead it return the
 * return code from callbeck as it is to the calling function.
 *
 * @param[in] callable_ptr
 * Reference to a Callable_T element to be invoked. It has to be
 * a valid pointer.
 *
 * @param[in] status
 * Status to be passed to callable element.
 *
 * @return the return value of the encapsulated function.
 */
retcode_t Callable_call(
    Callable_T* callable_ptr, retcode_t status);


/**
 * This function is called to invoke the callable element when it is used as a
 * callback function. It works as Callable_call() with the difference that any
 * potential error codes are handled inside the function without propagating
 * them to the caller. This is done this way since the caller of a callback
 * function is not resposible and usually not able to handle errors in this
 * context.
 *
 * @param[in] callable_ptr
 * Reference to a Callable_T object which holds the callback function. It has
 * to be a valid pointer.
 *
 * @param[in] status
 * Status to be passed to callback.
 */
void Callable_callback(
    Callable_T* callable_ptr, retcode_t status);


/**
 * This function clears or resets the callable element by setting its function
 * pointer to NULL.
 *
 * @param[in] callable_ptr
 * Reference to a Callable_T object to be cleared. It
 * has to be a valid pointer.
 */
inline static void Callable_clear(Callable_T* callable_ptr)
{
	callable_ptr->func = NULL;
}


#endif /* SERVAL_CALLABLE_H_ */
