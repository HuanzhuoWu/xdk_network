/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Policy.h
 * @brief Definition of backward compatibility.
 *
 * The Serval communication stack provides API compatibility within the major releases.
 * However, improvements to the stack can sometimes require an API change that is not
 * backwards compatible. In these cases the notion of a policy is used in order to
 * allow the new API to be introduced while still maintaining backwards compatibility.
 *
 * A policy is thus a set of changes to the API or the way the API is used that, if
 * turned on, will provide a more optimized API (in terms of either use or in terms
 * of resource consumption). Policies are API changes that are likely to become part
 * of the official API and thus provide a good preview mechanism for the next major
 * release.
 */

#ifndef SERVAL_POLICY_H_
#define SERVAL_POLICY_H_

#ifndef SERVAL_POLICY_COAP_STRICT_TOKEN_API
/**
 * The COAP_STRICT_TOKEN_API policy enforces proper use of the new token API
 * of the CoAP parser and serializer added in patch 1.3.
 *
 * In particular:
 *  * CoapSerializer_serializeToken() must be called before serializing options
 *  * CoapParser now has an API to retrieve the token
 *  * The option COAP_TOKEN no longer exists.
 *
 * Porting the API to this policy is only required if the CoAP API is used directly.
 * The REST API already uses the new API internally. The porting process is fairly
 * straightforward. The main task is to handle the TOKEN correctly. In the old API,
 * the token was set and retrieved using the COAP_TOKEN option. For implementations
 * setting the token, the token needs to be set using CoapSerializer_serializeToken()
 * _before_ setting an option. For implementations retrieving the Token, CoapParser
 * provides the function CoapParser_getToken with which the token can be retrieved.
 * The code that parses the options for the token can then be deleted.
 *
 * @since 1.3
 */
#define SERVAL_POLICY_COAP_STRICT_TOKEN_API 0
#endif

#ifndef SERVAL_POLICY_COAP_REMOVE_SERIALIZER_OPTION_COUNT
/**
 * The COAP_OPTION_COUNT policy provides backwards compatibility with the count
 * for serialized options. This is a remnant of the old coap-12 option construction.
 * In particular, this policy allows access to the optionSerializeCount field in
 * Coap_Serializer_T. This struct appears in an API header but is not really meant
 * to be used for any more than an opaque type. The use of this field is _highly_
 * discouraged. For this reason, this policy is enabled by default.
 *
 * @since 1.3
 */
#define SERVAL_POLICY_COAP_REMOVE_SERIALIZER_OPTION_COUNT 1
#endif

#ifndef SERVAL_POLICY_LARGE_PAYLOAD
/**
 * The original implementation of the CoAP and Rest parsers only allowed a payload
 * of 256 bytes since the data type storing the length of the payload was only
 * 8 bits long. This policy extends the length of this data type to 16 bits.
 * @since 1.3
 */
#define SERVAL_POLICY_LARGE_PAYLOAD 0
#endif

#ifndef SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK
/**
 * The original HTTP Authentication implementation required a table to be supplied
 * to the stack. This fixed the data structure and prevented additional information
 * to be stored alongside it and also prevented the application from implementing
 * any brute-force countermeasures.
 * This policy changes this mechanism by requiring the application to provide a callback
 * that handles user authentication using utility functions provided by the stack.
 */
#define SERVAL_POLICY_HTTP_AUTHENTICATION_CALLBACK 0
#endif

#ifndef SERVAL_POLICY_COAP_EXPLICIT_ERROR_CODES
/**
 * The CoAP layer hides certain error conditions behind generic ones, such as
 * RC_COAP_SENDING_ERROR. This policy disables this behavior and passes the
 * original error codes up to the application in order to allow more fine-grained
 * application responses to error codes.
 *
 * Enabled by default, since a check for status != RC_OK is most likely to be done
 * in the applications, anyways.
 */
#define SERVAL_POLICY_COAP_EXPLICIT_ERROR_CODES 1
#endif

#ifndef SERVAL_POLICY_PAL_LOG_CONSTRUCTION
/**
 * The current logging API constructs log messages before passing them into the
 * log appender for processing. This requires an explicit buffer (logbuf) in the
 * stack for generation of these messages and also prevents the PAL from making
 * decisions based on the severity of the log messages (e.g. print errors in red).
 *
 * This policy changes the signature of the log appender to allow passing messages
 * directly to this log appender.
 *
 * It also makes the construction of the message header accessible to the PAL.
 *
 * @since 1.6
 */
#define SERVAL_POLICY_PAL_LOG_CONSTRUCTION 0
#endif

#ifndef SERVAL_POLICY_STACK_CALLS_TLS_API
/**
 * The Stack calls Udp_* and Tcp_* functions internally.
 * This requires a PAL implementation of security to adjust the interface.
 * With this policy, the calls from the stack are made to Dtls_* and Tls_* where
 * appropriate when injecting security.
 */
#define SERVAL_POLICY_STACK_CALLS_TLS_API 0
#endif

#ifndef SERVAL_POLICY_LWM2M_DEFAULT_TO_IANA_DEFINED_TLV_FORMAT_NUMBER
/**
 * The Lwm2m implementation of the stack has been started when the TLV format
 * of Lwm2m did not have its own number assigned by the IETF.
 * The leshan project, somewhat arbitrarily, defined format numbers and assigned
 * 1542 to TLV internally. Due to Leshan being Open Source, this number has seen some
 * adoption.
 * Unfortunately, when the content format was registered, 1542 well into a reserved
 * area of numbers and thus a different number, 11542 was assigned.
 *
 * If no accept option is specified, thus indicating that any content type
 * is to be accepted, the stack defaults to sending TLV encoded content.
 * Starting from 1.7, it uses the IANA defined number for the TLV concent.
 * Unset this policy in order to revert to the old, leshan defined content number.
 *
 * @since 1.7
 */
#define SERVAL_POLICY_LWM2M_DEFAULT_TO_IANA_DEFINED_TLV_FORMAT_NUMBER 1
#endif

#ifndef SERVAL_POLICY_LWM2M_DROP_DEPRECATED_FORMATS
/**
 * The Lwm2m implementation of the stack has been started when the formats
 * of Lwm2m did not have their own number assigned by the IANA.
 * The leshan project, somewhat arbitrarily, defined format numbers.
 * These numbers were in a range for testing only and thus different numbers were
 * assigned when actually registering the formats.
 *
 * This policy drops support for the deprecated numbers.
 * Implicitly, this also forces SERVAL_POLICY_LWM2M_IANA_DEFINED_TLV_FORMAT_NUMBER
 *
 * @since 1.7
 */
#define SERVAL_POLICY_LWM2M_DROP_DEPRECATED_FORMATS 0
#endif
#endif /* SERVAL_POLICY_H_ */
