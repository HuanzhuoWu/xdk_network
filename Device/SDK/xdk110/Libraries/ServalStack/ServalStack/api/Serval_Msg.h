/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Msg.h
 * @brief An interface to the Messaging Module.
 * @ingroup msggroup
 *
 * This interface provides type definitions, data types and functions to handle
 * messages.
 */

#ifndef MSG_H_
#define MSG_H_

#include <Serval_Defines.h>
#include <Serval_Types.h>
#include <Serval_StringDescr.h>
#include <Serval_XmlParser.h>
#include <Serval_Filesystem.h>
#include <Serval_Callable.h>
#include <Serval_Ip.h>


/**
 * The threshold on the number of message part factories. Notice that an
 * OutMsgFactory consists of a list of part factories which are responsible
 * for serializing the message parts.
 */
#define SERVAL_MAX_NUM_MSG_PARTS           6

/**
 * define the timeout in ms for garbage collection of messages
 * it must be a Multiple of RESOURCE_MONITOR_PERIODE
 */
#ifndef SERVAL_MSG_MONITOR_TIMEOUT
#define SERVAL_MSG_MONITOR_TIMEOUT 2000
#endif

/* Message timeout correction in case of trace level */
#define SERVAL_MSG_MONITOR_TRACE_TIMEOUT \
   (SERVAL_MSG_MONITOR_TIMEOUT * (1 + SERVAL_LOG_LEVEL/3))


/**
 *******************************************************************************
 * Message Data Types
 *****************************************************************************
 */

/**
 * This enum specifies the supported message types of the lower protocols which
 * are used by the application protocols as transport protocols.
 */
typedef enum
{
	/**
	 * This is dummy type meaning that the type of message has not been yet
	 * specified.
	 */
	MSG_TLP_NONE = 0,

	/**
	 * This means that the message on the lower-layer is a UDP message.
	 */
	MSG_TLP_UDP,

#if 0
	/**
	 * This means that the message on the lower-layer is a TCP message.
	 */
	MSG_TLP_TCP,
#endif

	/**
	 * This means that the message on the lower-layer is an HTTP message.
	 */
	MSG_TLP_HTTP,

#if SERVAL_ENABLE_COAP
	/**
	 * This means that the message on the lower-layer is a CoAP message.
	 */
	MSG_TLP_COAP,
#endif



} Msg_TLP_T;


/**
 * This enum specifies the message types for the supported application protocol.
 */
typedef enum
{
	/**
	 * This is dummy type meaning that the type of message has not been yet
	 * specified.
	 */
	MSG_ALP_NONE = 0,

#if SERVAL_ENABLE_DPWS
	/**
	 * This means that the message is a DPWS message.
	 */
	MSG_ALP_DPWS,
#endif /* SERVAL_ENABLE_DPWS */

#if SERVAL_ENABLE_WEBSERVER
	/**
	* This means that the message is a webserver message.
	*/
	MSG_ALP_WEB,
#endif /* SERVAL_ENABLE_WEBSERVER */

#if SERVAL_ENABLE_REST
	MSG_ALP_REST,
#endif

#if SERVAL_ENABLE_XUDP
	/**
	 * This means that the message is a XUDP message.
	 */
	MSG_ALP_XUDP,
#endif /* SERVAL_ENABLE_XUDP */
#if SERVAL_ENABLE_XTCP
    /**
     * This means that the message is a XTCP message.
     * @since 1.4
     */
    MSG_ALP_XTCP,
#endif /* SERVAL_ENABLE_XTCP */

#if SERVAL_ENABLE_LWM2M
	/**
	 * This means that the message is an lwm2m message
	 * @since 1.4
	 */
	MSG_ALP_LWM2M
#endif


} Msg_ALP_T;

/**
 * The data structure for a messaging context. An element of this struct
 * represents the context of a messaging session of any supported pattern, e.g.
 * Request/Response, Request/No-Response, ... This means, it holds all
 * information about a complete communication cycle, rather than just one
 * message.
 *
 * @see struct Msg_S
 */
typedef struct Msg_S Msg_T;

/**
 * The data structure for messages on the lower layer
 *
 * @see struct TLPMsg_S
 */
typedef struct TLPMsg_S TLPMsg_T;


#if SERVAL_ENABLE_DPWS
/**
 * The data structure for DPWS messages
 *
 * @see struct DpwsMsg_S
 */
typedef struct DpwsMsg_S DpwsMsg_T;
#endif /* SERVAL_ENABLE_DPWS */

#if SERVAL_ENABLE_WEBSERVER
/**
 * The data structure for Webserver messages.
 *
 * @see struct WebMsg_S
 */
typedef struct WebMsg_S WebMsg_T;
#endif /* SERVAL_ENABLE_WEBSERVER */


#if SERVAL_ENABLE_REST
/**
 * The data structure for RESTful messages.
 *
 * @see struct RestMsg_S
 */
typedef struct RestMsg_S RestMsg_T;
#endif /* SERVAL_ENABLE_REST */

#if SERVAL_ENABLE_LWM2M
/**
 * The data structure for lightweigth M2M messages.
 * @since 1.4
 *
 * @see struct Lwm2mMsg_S
 */
typedef struct Lwm2mMsg_S Lwm2mMsg_T;
#endif

#if SERVAL_ENABLE_XUDP
/**
 * The data structure for messages of the generic proprietary UDP based
 * application protocol XUDP.
 *
 * @see struct XUdpMsg_S
 */
typedef struct XUdpMsg_S XUdpMsg_T;
#endif

#if SERVAL_ENABLE_XTCP
/**
 * The data structure for messages of the generic proprietary TCP based
 * application protocol XTCP.
 * @since 1.4
 *
 * @see struct XTcpMsg_S
 */
typedef struct XTcpMsg_S XTcpMsg_T;
#endif

#if SERVAL_ENABLE_COAP
/**
 * The data structure for messages of the UDP based application protocol COAP.
 *
 * @see struct CoapMsg_S
 */
typedef struct CoapMsg_S CoapMsg_T;
#endif

/**
 * The data structure for OutMsgFactory of a message, which is responsible for
 * serializing the outgoing packets.
 *
 * @see struct OutMsgFactory_S
 */
typedef struct OutMsgFactory_S OutMsgFactory_T;


/**
 * This data structure holds the data needed for sending a message.
 *
 * @see struct MsgSendingCtx_S
 */
typedef struct MsgSendingCtx_S MsgSendingCtx_T;

/**
 * This data structure holds the data needed for sending a message.
 *
 * It is used by the message sending functions to save and retrieve the
 * details of the sending job between the sending steps, e.g. saving the
 * details before and retrieving them after scheduling the sending procedure.
 */
struct MsgSendingCtx_S
{
	/**
	 * A union storing either the TCP or UDP socket via which the message
	 * should be sent.
	 */
	union Socket_U
	{
		/** The TCP socket via which the message should be sent. */
		Tcp_Socket_T tcp;

		/** The UDP socket via which the message should be sent. */
		Udp_Socket_T udp;
	} conn;

	/**
	 * The destination IP address. It is just need together with
	 * MsgSender_sendToViaUdp(). In the case of MsgSender_sendViaTcp() and
	 * MsgSender_sendViaUdp() the destination IP address is attached to the
	 * socket.
	 */
	Ip_Address_T destAddr;

	/**
	 * The destination port. It is just need together with
	 * MsgSender_sendToViaUdp(). In the case of MsgSender_sendViaTcp() and
	 * MsgSender_sendViaUdp() the destination port is attached to the
	 * socket.
	 */
	Ip_Port_T destPort;

	/**
	 * The callback function which will be invoked after the message has been
	 * completely sent.
	 */
	Callable_T *callback_ptr;

	/**
	 * A callable function object referencing the sending procedure.
	 */
	Callable_T sendingFunc;

	/**
	 * The handle of the communication buffer which will be allocated at some
	 * point of time for serializing and sending the message.
	 */
	CommBuff_T buffer;

	/**
	 * A flag which indicates the need to re-send the last chunk. This flag
	 * will be set when TCP asks for retransmitting the last packet.
	 */
	bool retx;
};

/**
 * This represents a recipe for message construction. It is a list of entries.
 * Each entry references an element of a given content base. Notice
 * that a recipe is only defined in relation to a certain content base.
 * Each entry of the recipe consists of 8 bits.
 * If the most significant bit is set to 1, then the entry is interpreted as
 * a reference to an external element of the content base. The
 * remaining 7 bits indicate the index of the element in the external elements
 * table. If the most significant bit is 0 and the following bit is set to 1,
 * then the entry is interpreted as a reference to a static element of the
 * content base. The remaining 6 bits indicate the index of the element in the
 * static elements table. If both most significant 2 bits are 0, then the entry
 * is interpreted as a reference to a dynamic element of the content base.
 * The remaining 6 bits indicate the index of the element in the
 * dynamic elements table.
 *
 * This scheme results in that the recipe can reference at most 128 external
 * elements, 64 static elements, and 64 dynamic elements.
 *
 * @see ContentBase_T
 */
typedef uint8_t const * RecipeTable_T;

/**
 * This holds the information about various content bases from where a message
 * will be constructed using message recipe.
 *
 * @see struct ContentBase_S
 */
typedef struct ContentBase_S ContentBase_T;

/**
 * This represents a handle for a content from content base.
 *
 * @see struct ContentHandle_S
 */
typedef struct ContentHandle_S ContentHandle_T;

/**
 * This represents a recipe for outgoing message construction.
 *
 * @see struct OutMsgRecipe_S
 */
typedef struct OutMsgRecipe_S OutMsgRecipe_T;

/**
 * This data structure is used hold the buffer and information needed by the
 * serializer to append the data at the right place.
 *
 * @see struct OutMsgSerializationHandover_S
 */
typedef struct OutMsgSerializationHandover_S OutMsgSerializationHandover_T;

/**
 * A data type for functions which serialize a dynamic content
 * of an outgoing message. The OutMsgSerializationHandover_T
 * object handed in as a a parameter encapsulates all input and
 * output information which might be needed by the function.
 * The function should serialize the content into the buffer given in the
 * OutMsgSerializationHandover_T object and return RC_OK. The field bufLen
 * gives the maximal length which the function is allowed to write into
 * the buffer. If the bufLen is too small to serialize the entire
 * content, then no bytes should be written and the function should return
 * RC_MSG_FACTORY_INCOMPLETE. The function must set the field len to
 * the number of written bytes.
 * Both offset and position must not be touched by the
 * function.
 */
typedef retcode_t (*DynamicContent_T)(OutMsgSerializationHandover_T *);

/**
 * A data type for message part factory functions. These are function
 * which are responsible for serializing a part of an outgoing message.
 * An OutMsgFactory_T object consists of a list of such part factories.
 * The OutMsgSerializationHandover_T object handed in as a a parameter
 * encapsulates all input and output information which might be needed
 * by the part factory.
 * The part factory should serialize the content into the buffer given in the
 *  OutMsgSerializationHandover_T object and return RC_OK. The field bufLen
 *  gives the maximal length which the function is allowed to write into
 *  the buffer. If the bufLen is too small to serialize the entire
 *  content, then the part factory can serialize a segment of its part
 *  and return RC_MSG_FACTORY_INCOMPLETE. Both fields offset and
 *  position can be used by the part factory in order to
 *  mark the position at which it should start the serialization in the
 *  next chunk. In any case, the function must set the field len to
 *  the number of written bytes.
 */
typedef retcode_t (*OutMsgPartFactoryFunc_T)(OutMsgSerializationHandover_T *);


/**
 * This data structure is used hold the buffer and information needed by the
 * serializer to append the data at the right place.
 *
 * This means it encapsulates all input and output information which might be
 * needed by the Serialization function. The function should serialize the
 * content into the buffer provided by this data structure.
 */
struct OutMsgSerializationHandover_S
{
	/** This represents the message structure which contains the information
	 * to be sent.
	 */
	Msg_T* msg_ptr;
	/** This buffer is used for message serialization*/
	char* buf_ptr;
	/** Length of the buffer provided */
	unsigned int bufLen;
	/** Offset of the data */
	unsigned int offset;
	/** Total length of the constructed message */
	unsigned int len;
	/** This holds factory position */
	unsigned int position;
};

/**
 * This data structure holds the information about various content bases for a
 * message.
 *
 * Various parts of a message will be constructed using content bases provided
 * by this data structure using message recipe.
 */
struct ContentBase_S
{
	uint16_t const *externalContentIndex;
	FileHandle_T externalContentFileHandle;
	DynamicContent_T const *dynamicContentIndex;
	char const * const *staticContentIndex;
};

/**
 * This represents a recipe for outgoing message construction.
 */
struct OutMsgRecipe_S
{
	/** This data structure holds the information about various content bases
	 * for a message.
	 */
	ContentBase_T const *contentBase_ptr;
	/** Length of the recipe */
	uint8_t recipeLen;
	/** recipe for message construction */
	RecipeTable_T recipe;
};

/**
 * This represents a handle for a content from content base.
 */
struct ContentHandle_S
{
	/** This data structure holds the information about various content bases
	 * for a message.
	 */
	ContentBase_T *contentBase_ptr;
	/** Index for the content in the content base */
	uint8_t index;
};

#if SERVAL_ENABLE_GENERIC_AUTHORIZATION
/**
 * A generic token that can be used in order to check the authorization of a
 * particular message.
 * The stack will treat this as an opaque type and carry it as-is with the message
 * that the token is attached to.
 */
union AuthorizationToken_U
{
	uint16_t id;
	uint16_t mask;
	void* custom;
};

typedef union AuthorizationToken_U AuthorizationToken_T;
#endif

/*
 *******************************************************************************
 * Application In- and Output and Callback Functions
 ******************************************************************************/

/**
 * A data type representing a callback function pointer for application. The
 * application uses such callback functions in order to be notified about the
 * sending result of outgoing messages.
 */
typedef void (*AppSentCallback_T)(Msg_T *msg_ptr, retcode_t status);

/**
 * A data type representing a callback function pointer for application. The
 * application uses such callback functions in order to be notified about the
 * sending result of outgoing messages.
 * @deprecated The type AppSentCallback_T should be used instead.
 */
typedef AppSentCallback_T AppCallback_T;

/**
 * A data type that represents application's input object.
 *
 * Notice that the union itself is defined in the application.
 *
 * @see union AppInput_U
 */
typedef union AppInput_U AppInput_T;

/**
 * A data type that represents application's output object.
 *
 * Notice that the union itself is defined in the application.
 *
 * @see union AppOutput_U
 */
typedef union AppOutput_U AppOutput_T;

/**
 * The data structure which holds all functions to manage application's
 * input and output objects.
 *
 * @see struct AppInOutput_Manager_S
 */
typedef struct AppInOutput_Manager_S AppInOutput_Manager_T;

/*
 * AppInOutput_AllocInputFunc_T represents a function pointer for allocating
 * application input objects.
 */
typedef retcode_t (*AppInOutput_AllocInputFunc_T)(AppInput_T**);

/*
 * AppInOutput_AllocOutputFunc_T represents a function pointer for allocating
 * application output objects.
 */
typedef retcode_t (*AppInOutput_AllocOutputFunc_T)(AppOutput_T**);

/*
 * AppInOutput_ReleaseInputFunc_T represents a function pointer for releasing
 * application input objects.
 */
typedef retcode_t (*AppInOutput_ReleaseInputFunc_T)(AppInput_T*);

/*
 * AppInOutput_ReleaseOutputFunc_T represents a function pointer for releasing
 * application output objects.
 */
typedef retcode_t (*AppInOutput_ReleaseOutputFunc_T)(AppOutput_T*);

/**
 * The data structure which holds all functions to manage application's
 * input and output objects.
 *
 * This structure provides functions to be used for allocating and releasing an
 * input or output object.
 *
 * @see union AppOutput_U
 */
struct AppInOutput_Manager_S
{
	/**
	 * Represents a function for allocating application input objects.
	 */
	AppInOutput_AllocInputFunc_T const allocInput;

	/**
	 * Represents a function for releasing application input objects.
	 */
	AppInOutput_ReleaseInputFunc_T const releaseInput;

	/**
	 * Represents a function for allocating application output objects.
	 */
	AppInOutput_AllocOutputFunc_T const allocOutput;

	/**
	 * Represents a function for releasing application output objects.
	 */
	AppInOutput_ReleaseOutputFunc_T const releaseOutput;
};

/*
 *******************************************************************************
 * Function Prototypes
 ******************************************************************************/

/**
 * This function is called to open a previously allocated message. Any message
 * must be opened before it can be used. Calling of this function also opens the
 * out message factory related to this message, thus a call to
 * OutMsgFactory_isOpen() must return true after a successful call to this
 * function.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the previously allocated message.
 *  It has to be a valid pointer.
 */
void Msg_open(Msg_T *msg_ptr);

/**
 * This function is called to close a previously allocated message. After this
 * call the message will be released form the message pool.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the message to be closed.
 *  It has to be a valid pointer.
 */
void Msg_close(Msg_T *msg_ptr);


/**
 * This function provides a pointer to the factory structure of the given
 * Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the message factory.
 *  It has to be a valid pointer.
 *
 * @return a pointer to the required factory structure.
 */
OutMsgFactory_T *Msg_getFactory(Msg_T *msg_ptr);


/**
 * This function is called from application layer for pushing down a response
 * for a request. Here based on type of application protocol we decide how to
 * construct the message to be sent.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the message to be constructed.
 *  It has to be a valid pointer.
 *
 * @param[in] appOutput_ptr
 * Reference to a AppOutput_T object, this holds the output pointer for response
 *
 * @param[in] appCallback
 * Reference to a AppCallback_T object, this holds the application callback
 * to be called to inform application about the success or failure of response.
 *
 * @return
 * RC_OK if the message is successfully pushed down to lower layer for
 * transmission.
 * RC_MSG_FACTORY_SIZE_ERROR if there is issue with message factory size <br>
 * RC_MSG_FACTORY_ALREADY_COMMITTED if message factory is already committed <br>
 * RC_HTTP_SEND_ERROR if there is sending error <br>
 * RC_DPWS_RESPOND_FAILED, otherwise
 */
retcode_t Msg_respond(Msg_T *msg_ptr, AppOutput_T *appOutput_ptr,
                      AppCallback_T appCallback);

/**
 * This function is called to initialize application callback defined by
 * application to inform it of some event.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the current message.
 *  It has to be a valid pointer.
 *
 * @param[in] appCallback
 * Reference to a AppCallback_T object, this holds the application callback
 * to be called to inform application about the success or failure of response.
 */
void Msg_setAppCallback(Msg_T* msg_ptr, AppCallback_T appCallback);

/**
 * This function is called to invoke a callback, predefined by application
 * ,on some event to inform application of the event.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the current message.
 *  It has to be a valid pointer.
 *
 * @param[in] status
 * Status to be passed to application indicating the event.
 */
void Msg_callbackApp(Msg_T *msg_ptr, retcode_t status);

/**
 * This function is called to release both the communication buffer acquired
 * from lower layer and also based on the TLP clears the internal pointer to
 * this buffer.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the communication buffer to be
 * released. It has to be a valid pointer.
 *
 *
 * @see CommBuff_free(), CommBuff_getInvalidBuffer()
 */
void Msg_releaseCommBuf(Msg_T *msg_ptr);

#if SERVAL_ENABLE_REST  /* REST is disabled -> no compilation here */
/**
 * This function provides a pointer to the structure holding REST message in the
 * given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type RestMsg_T to the required REST message structure.
 */
RestMsg_T* Msg_getRestMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_REST */

#if SERVAL_ENABLE_LWM2M
/**
 * This function provides a pointer to the structure holding LWM2M message in the
 * given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type Lwm2mMsg_T to the required Lwm2m message structure.
 *
 * @since 1.4
 */
Lwm2mMsg_T* Msg_getLwm2mMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_LWM2M */

#if SERVAL_ENABLE_DPWS  /* DPWS is disabled -> no compilation here */
/**
 * This function provides a pointer to the structure holding DPWS message in the
 * given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type DpwsMsg_T to the required DPWS message structure.
 */
DpwsMsg_T* Msg_getDpwsMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_DPWS */

#if SERVAL_ENABLE_WEBSERVER  /* Webserver is disabled -> no compilation */
/**
 * This function provides a pointer to the structure holding web message in the
 * given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type WebMsg_T to the required web message structure.
 */
WebMsg_T* Msg_getWebMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_WEBSERVER */


#if SERVAL_ENABLE_XUDP  /* XUDP is disabled -> no compilation */
/**
 * This function provides a pointer to the structure holding XUDP message in
 * the given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type XUdpMsg_T to the required web message structure.
 */
XUdpMsg_T* Msg_getXUdMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_XUDP */
#if SERVAL_ENABLE_XTCP  /* XTCP is disabled -> no compilation */
/**
 * This function provides a pointer to the structure holding XTCP message in
 * the given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object. It has to be a valid pointer.
 *
 * @return a pointer of type XTcpMsg_T to the required web message structure.
 *
 * @since 1.4
 */
XTcpMsg_T* Msg_getXTcpMsg(Msg_T* msg_ptr);
#endif /* SERVAL_ENABLE_XTCP */

/**
 * This function adds a function to the factory of the given message, that
 * should add or modify the current data in the buffer by serializing a
 * specific recipe or adding a header or ending etc...
 *
 * @param[in] msg_ptr
 * Pointer to the message.
 * @param[in] partFactoryFunc_ptr
 * The function pointer that should be added to the list of part factory
 * functions.
 *
 * @return
 * RC_OK <BR>
 * RC_MSG_FACTORY_SIZE_ERROR if there is no place to accommodate the new
 * part factory.<BR>
 * RC_MSG_FACTORY_ALREADY_COMMITTED if the factory is already committed.
 */
retcode_t Msg_prependPartFactory(Msg_T *msg_ptr,
                                 OutMsgPartFactoryFunc_T partFactoryFunc_ptr);

/**
 * This function provides a pointer to the context structure of the given
 * callable object. It is assumed here that the callable object is member of a
 * struct. A pointer to the struct will be returned.

 * @param[in] callable_ptr
 * a pointer to the callable object
 *
 * @return a pointer to the enclosing structure which contains the given
 * callable object as a member.
 */
Msg_T *Msg_getContext(Callable_T *callable_ptr);


/**
 * This function is called to set the flag indicating transfer protocol type.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the current message.
 *  It has to be a valid pointer.
 *
 * @param[in] tlp
 * This holds the value of flag to be assigned.
 */
void Msg_setTLP(Msg_T* msg_ptr, Msg_TLP_T tlp);


/**
 * This function is called to get the flag indicating transfer protocol type.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the current message.
 *  It has to be a valid pointer.
 *
 * @return value of the TLP flag.
 */
Msg_TLP_T Msg_getTLP(Msg_T* const msg_ptr);


/**
 * This function provides a pointer to the transport protocol structure of the
 * given Msg_T object.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the TLP message.
 *  It has to be a valid pointer.
 *
 * @return a pointer to the required TLP structure.
 */
TLPMsg_T *Msg_getTLPMsg(Msg_T *msg_ptr);


/**
 * This function is called to set the flag indicating Application protocol type.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_t object. It has to be a valid pointer.
 *
 * @param[in] alp
 * This holds the value of flag to be assigned.
 */
void Msg_setALP(Msg_T *msg_ptr, Msg_ALP_T alp);


/**
 * This function is called to get the flag indicating Application protocol type.
 *
 * @param[in] msg_ptr
 * Reference to the Msg_t object. It has to be a valid pointer.
 *
 * @return value of the ALP flag.
 */
Msg_ALP_T Msg_getALP(Msg_T const *msg_ptr);


/**
 * This function is called to initialize callback in the given message. This
 * callback will be invoked to signal completion of sending.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which holds the current message.
 *  It has to be a valid pointer.
 *
 * @param[in] callback
 * Reference to a CallableFunc_T object, this holds the callback to be assigned.
 */
Callable_T *Msg_defineCallback(Msg_T *msg_ptr, CallableFunc_T callback);


Callable_T* Msg_getCallback(Msg_T *msg_ptr);

#if SERVAL_ENABLE_GENERIC_AUTHORIZATION
/**
 * Retrieve the authorization token from the message
 *
 * @param[in]  msg_ptr  the message from which to obtain the token
 *
 * @return              the authorization token
 */
AuthorizationToken_T Msg_getAuthorizationToken(Msg_T *msg_ptr);

/**
 * Set the authorization token from the message
 *
 * @param[in]  msg_ptr  the message from which to obtain the token
 * @param[in]  token    the authorization token
 */
void Msg_setAuthorizationToken(Msg_T *msg_ptr, AuthorizationToken_T token);

/**
 * Convenience function to use the authorization token as an id
 *
 * @param[in]  msg_ptr  the message from which to obtain the token
 * @param[in]  id       the id to use as an authorization token
 */
void Msg_setAuthorizationTokenId(Msg_T *msg_ptr, uint16_t id);

/**
 * Convenience function to use the authorization token as a mask
 *
 * @param[in]  msg_ptr  the message from which to obtain the token
 * @param[in]  mask     the mask to use as an authorization token
 */
void Msg_setAuthorizationTokenMask(Msg_T *msg_ptr, uint16_t mask);

/**
 * Convenience function to set a custom authorization token
 *
 * @param[in]  msg_ptr  the message from which to obtain the token
 * @param[in]  custom   the custom pointer to use as an authorization token
 */
void Msg_setAuthorizationTokenCustom(Msg_T *msg_ptr, void* custom);
#endif

/**
 *  Check Message Monitor period
 */
#if (SERVAL_MSG_MONITOR_TIMEOUT % SERVAL_RESOURCE_MONITOR_PERIODE)
#error SERVAL_MSG_MONITOR_TIMEOUT must be a multiple of RESOURCE_MONITOR_PERIODE
#endif

#endif /* MSG_H_ */
