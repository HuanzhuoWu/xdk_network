/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_XmlParser.h
 * @brief Interface to XML Parser
 * @ingroup utilgroup
 *
 * The purpose of this module is scanning a XML document. This means that the
 * characters are divided in logical (lexical) parts. All information needed
 * to continue parsing is stored inside a context. That way it is possible
 * to simultanously handle multiple SOAP requests. Every message gets its own
 * context for scanning.
 * Furthermore, the original XML document is not modified.
 *
 *  @verbatim
 *  <node>
 *     <subnode attr="attrValue">
 *        <subsubnode>nodeValue</subsubnode>
 *     </subnode>
 *  </node>
 *  @endverbatim
 *
 * results in (notation ("tag type", "tag name", "tag value", "nesting level")):
 *
 *  @verbatim
 *  (start, node, NULL, 0)
 *     (start, subnode, NULL, 1)
 *     (attribute, attr, attrValue, 1)
 *        (single, subsubnode, nodValue, 2)
 *     (end, subnode, NULL, 1)
 *  (end, node, NULL, 0)
 *  @endverbatim
 */

#ifndef SERVAL_XMLPARSER_H_
#define SERVAL_XMLPARSER_H_

#include <Serval_StringDescr.h>

/**
 * Types of all possible XML tokens
 */
typedef enum
{
	/* e.g. "<node>value</node>" or "<node />" */
	XML_TOKEN_NODE,
	/* e.g. "<node> <children>+ </node>" */
	XML_TOKEN_NODE_START,
	/* e.g. "...</node>" */
	XML_TOKEN_NODE_END,
	/* e.g. "<node attribute="value" />" */
	XML_TOKEN_ATTRIBUTE,
	/* End Of File */
	XML_TOKEN_EOF,
	/* */
	XML_TOKEN_INCOMPLETE,
	/* */
	XML_TOKEN_INVALID
} XmlTokenType_T;

/**
 * The context structure describes the current state of one specific
 * parsing process.
 *
 * @see struct XmlParser_S
 */
typedef struct XmlParser_S XmlParser_T;

/**
 * The context structure describes the current state of one specific parsing
 * process.
 */
struct XmlParser_S
{
	/* Pointer to the current position inside the document. */
	const char *pc_ptr;
	/* Pointer to the end position of the document. */
	const char *end_ptr;
	/* Name of the tag. */
	StringDescr_T tagName;
	/* Value of the tag. */
	StringDescr_T tagValue;
	/**
	 * Nesting depth of current location. Note that end tags match the level of
	* its corresponding start tags. e.g.:
	  @verbatim
	  text           |  level
	  <n1>           |  0
	     <n2>        |  1
	        <n3 />   |  2
	     </n2>       |  1
	  </n1>          |  0
	  @endverbatim
	 */
	uint16_t level;
	/* Pointer to the start of attributes (if any). */
	const char *attributes_ptr;
	/* Pointer to the start of children. */
	const char *children_ptr;
};


/**
 * This function is called to initialize the XML Parser Module.
 *
 * @return
 * RC_OK, if successful<br>
 */
retcode_t XmlParser_initialize(void);


/**
 * This function is called to initialize a given scanner context with a XML
 * document.
 *
 * @param[in] xmlParser_ptr
 * Context to be initialized.
 *
 * @param[in] xml_ptr
 * XML document to be used for a later scanning process.
 *
 * @param[in] len
 * Context-Length
 *
 * @return
 * RC_OK, if successful<br>
 */
retcode_t XmlParser_setup(XmlParser_T *xmlParser_ptr,
                          const char *xml_ptr, unsigned int len);


/**
 * This function returns the next found token. This also causes modification of
 * xmlPraser_ptr.
 *
 * @param[in] xmlPraser_ptr
 * Reference to a XmlParser_T object which gives the context to be processed.
 * It is also modified during the process. Relevant for the application are:
 * @b tagName, @b tagValue and @b level.
 *
 * @return
 * The type of found token.
 *
 * @note
 * In principle, it is also possible that the token type is stored
 * inside the context. In contrast, returning the token type enables a better
 * control flow for the calling function. Thats why this approach is used.
 */
XmlTokenType_T XmlParser_parseNextToken(XmlParser_T *xmlPraser_ptr);


/**
 * This function compares a XML parser context with the given tag name and
 * level, and returns the result.
 *
 * @param[in] ctx_ptr
 * Reference to a XmlParser_T object which contains the context of XML parser
 * to be compared.
 *
 * @param[in] tagName_ptr
 * C string of tag name to be compared.
 *
 * @param[in] level
 *
 * @return
 * true if found equal, otherwise false.
 */
bool XmlParser_matchTag(XmlParser_T *ctx_ptr, const char *tagName_ptr,
                        uint16_t level);

/**
 * This function is called to fetch a tag name from a given context of xml
 * parser into a String Descriptor.
 *
 * @param[in] xmlParser_ptr
 * Reference to a XmlParser_T object which contains the context of xml parser
 * from which contains desired tag name.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_STRING_DESCRIPTOR_ERROR<br>
 */
static inline void XmlParser_getTagName(XmlParser_T *xmlParser_ptr,
                                        StringDescr_T *str_ptr)
{
	StringDescr_clone(str_ptr, &xmlParser_ptr->tagName);
}


/**
 * This function is called to fetch a tag value from a given context of xml
 * parser into a String Descriptor.
 *
 * @param[in] xmlParser_ptr
 * Reference to a XmlParser_T object which contains the context of xml parser
 * from which contains desired tag value.
 *
 * @param[in] str_ptr
 * Reference to a StringDescr_T object which identifies the instance of
 * destination string descriptor.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_STRING_DESCRIPTOR_ERROR<br>
 */
static inline void XmlParser_getTagValue(XmlParser_T *xmlParser_ptr,
        StringDescr_T *str_ptr)
{
	StringDescr_clone(str_ptr, &xmlParser_ptr->tagValue);
}

/**
 * This function is called to fetch level of a given context of XML parser.
 *
 * @param[in] xmlParser_ptr
 * Reference to a XmlParser_T object whose level needs to be fetched.
 *
 * @param[in] level_ptr
 * This will hold the value of level when the function returns.
 */
static inline void XmlParser_getLevel(XmlParser_T *xmlParser_ptr,
                                      uint16_t *level_ptr)
{
	*level_ptr = xmlParser_ptr->level;
}

#endif /* SERVAL_XMLPARSER_H_ */
