/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_StopWatch.h
 * @brief An interface to the stop watch module.
 * @ingroup utilgroup
 *
 * This file provides interfaces to use stop watch functionality. In addition
 * to the data structures needed to represent and manage a stop watch, this file
 * also includes the interfaces which provides functionality like starting a
 * stop watch, getting absolute time in millisecond and second since the start
 * of stop watch and also getting starting time of stop watch.
 */

#ifndef SERVAL_STOPWATCH_H
#define SERVAL_STOPWATCH_H

#include <Serval_Types.h>
#include <Serval_Exceptions.h>

/**
 * This data structure represents a context of a stop watch.
 *
 * @see struct StopWatch_S
 */
typedef struct StopWatch_S StopWatch_T;

/**
 * This data structure represents a context of a stop watch.
 *
 * Each instance of a stop watch will own one instance of this data structure.
 */
struct StopWatch_S
{
	uint64_t start;
};

/**
 * This function is called to mark start of stop watch.
 *
 * @param[in] watch_ptr
 * Reference to a StopWatch_T object which identifies the instance of stop watch
 * been started.
 * It has to be a valid pointer.
 *
 * @return
 * RC_OK on success
 */
retcode_t StopWatch_start(StopWatch_T *watch_ptr);


/**
 * This function is called to fetch the starting time value of given instance of
 * stop watch.
 *
 * @param[in] watch_ptr
 * Reference to a StopWatch_T object which identifies the instance of stop watch
 * been used.
 * It has to be a valid pointer.
 *
 * @param[out] time_ptr
 * Pointer to store the start time to
 *
 * @return
 * RC_OK if successful
 */
retcode_t StopWatch_getStartTime(StopWatch_T *watch_ptr, uint64_t *time_ptr);


/**
 * This function is called to fetch the duration in millisecond(s) since
 * start of given instance of stop watch. The function StopWatch_start() should
 * have been called before calling this function.
 *
 * @param[in] watch_ptr
 * Reference to a StopWatch_T object which identifies the instance of stop watch
 * been used.
 * It has to be a valid pointer.
 *
 * @param[out] time_ptr
 * This will contain time value in millisecond(s) when the function returns.
 *
 * @return
 * RC_OK if successful
 *
 * @see
 * StopWatch_getDuaration()
 */
retcode_t StopWatch_getDuarationMillis(StopWatch_T *watch_ptr,
                                       uint64_t *time_ptr);


/**
 * This function is called to fetch the duration in second(s) since start of
 * given instance of stop watch. The function StopWatch_start() should have been
 * called before calling this function.
 *
 * @param[in] watch_ptr
 * Reference to a StopWatch_T object which identifies the instance of stop watch
 * been used.
 * It has to be a valid pointer.
 *
 * @param[out] time_ptr
 * This will contain time value in second(s) when the function returns.
 *
 * @return
 * RC_OK if successful
 *
 * @see
 * StopWatch_getDuarationMillis()
 */
retcode_t StopWatch_getDuaration(StopWatch_T *watch_ptr, uint32_t *time_ptr);

#endif /* SERVAL_STOPWATCH_H */
