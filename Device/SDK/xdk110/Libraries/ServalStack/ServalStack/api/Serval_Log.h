/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Log.h
 * @brief Logging Module
 * @ingroup utilgroup
 *
 * This module provides function to handle the logging. The logging can be done
 * via UART or UDP/IP. The logging can be done in different modes
 * (e.g. debugging).
 */

#ifndef SERVAL_LOG_H_
#define SERVAL_LOG_H_

#include <Serval_Types.h>
#include <Serval_Defines.h>
#include <Serval_Exceptions.h>


/**
 * Logging level OFF: Disables logging completely.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_OFF      0

/**
 * Logging level FATAL: Severe errors that cause premature termination.
 *          Expect these to be immediately visible on a status console.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_FATAL    1

/**
 * Logging level ERROR: Other runtime errors or unexpected conditions. Expect
 *          these to be immediately visible on a status console.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_ERROR    2

/**
 * Logging level WARN: Use of deprecated APIs, poor use of API, 'almost' errors,
 *          other runtime situations that are undesirable or unexpected,
 *          but not necessarily "wrong". Expect these to be immediately
 *          visCoapMessaging_getMessageTokenCOAPible on a status console.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_WARN     3

/**
 * Logging level INFO: Interesting runtime events (startup/shutdown). Expect
 *          these to
 *          be immediately visible on a console, so be conservative and
 *          keep to a minimum.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_INFO     4


/**
 * Logging level DEBUG: Detailed information on the flow through the system.
 *          Expect these to be written to logs only.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_DEBUG    5

/**
 * Logging level TRACE: More detailed information. Expect these to be written to
 *          logs only.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#define SERVAL_LOG_LEVEL_TRACE    6


/**
 * If the logging level has been defined before, then we set it to
 * SERVAL_LOG_LEVEL_INFO as a default logging level.
 *
 * NOTE: Available logging levels are:
 * TRACE > DEBUG > INFO > WARN > ERROR > FATAL > OFF
 */
#ifndef SERVAL_LOG_LEVEL
#define SERVAL_LOG_LEVEL    SERVAL_LOG_LEVEL_DEBUG
#endif



/**
 * Enable ''SERVAL_ENABLE_NETWORK_LOG'' in order to enable the logging over UDP
 */
#ifndef SERVAL_ENABLE_NETWORK_LOG
#define SERVAL_ENABLE_NETWORK_LOG 0
#endif

/**
 * Enable ''SERVAL_UDP_LOG_DIRECT_SEND'' in order to enable that each log message
 * is sent directly via udp. Disabled means that several log messages are
 * collected until 5% of ''SERVAL_MAX_SIZE_APP_PACKET'' is reached.
 */
#ifndef SERVAL_UDP_LOG_DIRECT_SEND
#define SERVAL_UDP_LOG_DIRECT_SEND 0
#endif


/** allowed length of a module name */
#define SERVAL_LOG_MAX_LEN_MODULE_NAME                3

/** length of a log timestamp */
#define SERVAL_LOG_MAX_LEN_TIMESTAMP                 10

/** Total length of the log header */
#define SERVAL_LOG_HEADER_LENGTH                     19

/** maximum logtext length */
#define SERVAL_LOG_MAX_LEN_TEXT                     300

typedef int LogLevel_T;

#if SERVAL_POLICY_PAL_LOG_CONSTRUCTION == 1
/**
 * Construct the Serval header for a log message.
 * requires SERVAL_LOG_HEADER_LENGTH bytes of buffer space
 *
 * @param buf         the buffer to serialize the message into
 * @param lvl         the log level of the message
 * @param module_name the name of the module of the log message
 *
 * @return the number of serialized bytes
 */
unsigned int Log_constructLogHeader(char *buf, LogLevel_T lvl,
                                    const char *module_name);

/**
 * This type defines the attributes of an log appender.
 */
typedef struct LogAppender_S
{
	/** Function to write a string. */
	void (*vlog)(LogLevel_T lvl, const char *module_name, const char *format, va_list argptr);
	/** Function to write a given number of characters of a string. */
	void (*writeLen)(char const *, unsigned int);
	struct LogAppender_S *next;
#if SERVAL_ENABLE_DUTY_CYCLING
	retcode_t (*sleep)(void);
#endif
} LogAppender_T;
#else
/**
 * This type defines the attributes of an log appender.
 */
typedef struct LogAppender_S
{
	/** Function to write a string. */
	void (*write)(char const *);
	/** Function to write a given number of characters of a string. */
	void (*writeLen)(char const *, unsigned int);
	struct LogAppender_S *next;
#if SERVAL_ENABLE_DUTY_CYCLING
	retcode_t (*sleep)(void);
#endif
} LogAppender_T;
#endif

/**
 * Used to initialize logging. It sets the default logging level to the maximal
 * allowed level, i.e., SERVAL_LOG_LEVEL.
 *
 * @return RC_OK<br>
 * RC_LOG_INIT_ERROR
 */
retcode_t Log_initialize(void);

/**
 * Enables the given appender for logging.
 *
 * @param[in] appender_ptr the appender to enable. It must be a valid pointer.
 */
void Log_enable(LogAppender_T *appender_ptr);

/**
 * Disables the given appender for logging.
 *
 * @param[in] appender_ptr the appender to disable. It must be a valid pointer.
 */
void Log_disable(LogAppender_T *appender_ptr);


#if SERVAL_ENABLE_NETWORK_LOG
#include <Serval_Ip.h>

retcode_t Log_enableUdpLogAppender(
    Ip_Address_T const *destIp, Ip_Port_T destPort);

retcode_t Log_disableUdpLogAppender(void);

#endif


/**
 * Sets the log level. The following levels are supported:
 * SERVAL_LOG_LEVEL_OFF<br>
 * SERVAL_LOG_LEVEL_FATAL<br>
 * SERVAL_LOG_LEVEL_ERROR<br>
 * SERVAL_LOG_LEVEL_WARNING<br>
 * SERVAL_LOG_LEVEL_INFO<br>
 * SERVAL_LOG_LEVEL_DEBUG<br>
 * SERVAL_LOG_LEVEL_TRACE<br>
 *
 * @param[in] lvl
 * Loglevel to set
 *
 *
 * @return
 * RC_OK on success<br>
 * RC_LOG_LEVEL_NOT_SUPPORTED Log level is higher than SERVAL_LOG_LEVEL, which
 * indicates the maximal log level at build time.
 */
retcode_t Log_setLogLevel(LogLevel_T lvl);

/**
 * This function is called to log a message.
 *
 * Depending on the parameter lvl and configured log level, a message will be
 * logged to all enabled appenders.
 *
 * @param[in] lvl
 * Value of log level of message to be logged.
 *
 * @param[in] module_name
 * Name of module which requested the logging, this will be added to log header.
 *
 * @param[in] text_ptr
 * Message to be logged
 *
 * @param[in] ...
 * Additional information for creation of log message.
 *
 * @see loglevel_t
 */
void Log_flog(LogLevel_T lvl, const char *module_name,
              const char *text_ptr, ...);

/**
 * This function is called to log a message.
 *
 * Depending on the parameter lvl and configured log level, a message will be
 * logged to all enabled appenders.
 *
 * @param[in] lvl
 * Value of log level of message to be logged.
 *
 * @param[in] module_name
 * Name of module which requested the logging, this will be added to log header.
 *
 * @param[in] text_ptr
 * Message to be logged
 *
 * @param[in] len
 * Length of message to be logged.
 *
 * @see loglevel_t
 */
void
Log_log(LogLevel_T lvl, const char *module_name, const char *text_ptr, int len);

/* If SERVAL_COMPONENT is defined for a module, use the new logging definition.
 * This allows a finer grained debugging.
 * Thus, if SERVAL_COMPONENT is COAP, then the log level for this component
 * can be tweaked by setting SERVAL_COMPONENT_COAP_LOG_LEVEL=.
 */
#ifdef SERVAL_COMPONENT

/* Various helper functions since programming with the preprocessor is really broken */
#define __SERVAL_LOG_HAS_LEVEL_0 1
#define __SERVAL_LOG_HAS_LEVEL_1 1
#define __SERVAL_LOG_HAS_LEVEL_2 1
#define __SERVAL_LOG_HAS_LEVEL_3 1
#define __SERVAL_LOG_HAS_LEVEL_4 1
#define __SERVAL_LOG_HAS_LEVEL_5 1
#define __SERVAL_LOG_HAS_LEVEL_6 1

#define __SERVAL_LOG_STRINGIFY(x) #x
#define __SERVAL_LOG_TOKENPASTE(x,y,z) x ## y ## z
#define __SERVAL_LOG_CHECK(x) __SERVAL_LOG_TOKENPASTE(__SERVAL_LOG_,HAS_LEVEL_,x)

#define __SERVAL_LOG_LEVEL_FCT(component)      __SERVAL_LOG_TOKENPASTE(SERVAL_COMPONENT_, component, _LOG_LEVEL)
#define __SERVAL_LOG_MODULE_STRING_FCT(module) __SERVAL_LOG_STRINGIFY(module)

#ifndef LOG_MODULE
#define LOG_MODULE __SERVAL_LOG_MODULE_STRING_FCT(SERVAL_COMPONENT)
#endif
#define __SERVAL_LOG_LEVEL __SERVAL_LOG_LEVEL_FCT(SERVAL_COMPONENT)

#if __SERVAL_LOG_CHECK(__SERVAL_LOG_LEVEL)
#undef SERVAL_LOG_LEVEL
#define SERVAL_LOG_LEVEL __SERVAL_LOG_LEVEL
#endif

#endif /* SERVAL_COMPONENT */

/* If the LOG_LEVEL_TRACE is enabled, then allow the use of LOG_TRACE and
 * LOG_TRACE_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_TRACE)
#define LOG_TRACE(...) \
               Log_flog(SERVAL_LOG_LEVEL_TRACE,   LOG_MODULE, __VA_ARGS__)
#define LOG_TRACE_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_TRACE,  LOG_MODULE, ptr, len)
#else
#define LOG_TRACE(...)
#define LOG_TRACE_LEN(ptr,len)
#endif

/* If the LOG_LEVEL_DEBUG is enabled, then allow the use of LOG_DEBUG and
 * LOG_DEBUG_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_DEBUG)
#define LOG_DEBUG(...) \
               Log_flog(SERVAL_LOG_LEVEL_DEBUG,   LOG_MODULE, __VA_ARGS__)
#define LOG_DEBUG_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_DEBUG,  LOG_MODULE, ptr, len)
#else
#define LOG_DEBUG(...)
#define LOG_DEBUG_LEN(ptr,len)
#endif

/* If the LOG_LEVEL_INFO is enabled, then allow the use of LOG_INFO and
 * LOG_INFO_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_INFO)
#define LOG_INFO(...) \
               Log_flog(SERVAL_LOG_LEVEL_INFO,     LOG_MODULE, __VA_ARGS__)
#define LOG_INFO_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_INFO,    LOG_MODULE, ptr, len)
#else
#define LOG_INFO(...)
#define LOG_INFO_LEN(...)
#endif

/* If the LOG_LEVEL_WARN is enabled, then allow the use of LOG_WARNING and
 * LOG_WARNING_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_WARN)
#define LOG_WARNING(...) \
               Log_flog(SERVAL_LOG_LEVEL_WARN,  LOG_MODULE, __VA_ARGS__)
#define LOG_WARNING_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_WARN, LOG_MODULE, ptr, len)
#else
#define LOG_WARNING(...)
#define LOG_WARNING_LEN(...)
#endif

/* If the LOG_LEVEL_ERROR is enabled, then allow the use of LOG_ERROR and
 * LOG_ERROR_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_ERROR)
#define LOG_ERROR(...) \
               Log_flog(SERVAL_LOG_LEVEL_ERROR,   LOG_MODULE, __VA_ARGS__)
#define LOG_ERROR_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_ERROR,  LOG_MODULE, ptr, len)
#else
#define LOG_ERROR(...)
#define LOG_ERROR_LEN(ptr,len)
#endif


/* If the LOG_LEVEL_FATAL is enabled, then allow the use of LOG_FATAL and
 * LOG_FATAL_LEN */
#if (SERVAL_LOG_LEVEL >= SERVAL_LOG_LEVEL_FATAL)
#define LOG_FATAL(...) \
               Log_flog(SERVAL_LOG_LEVEL_FATAL,   LOG_MODULE, __VA_ARGS__)
#define LOG_FATAL_LEN(ptr,len) \
               Log_log(SERVAL_LOG_LEVEL_FATAL,  LOG_MODULE, ptr, len)
#else
#define LOG_FATAL(...)
#define LOG_FATAL_LEN(ptr,len)
#endif


/* Enable to use LOG_TIMING */
#if SERVAL_ENABLE_PROFILING
#define LOG_TIMING(...) \
      Log_flog(SERVAL_LOG_LEVEL_OFF, LOG_MODULE, __VA_ARGS__)
#else
#define LOG_TIMING(...)
#endif

#define LOG_TRACE_ENTER() \
               LOG_TRACE(">> %s()\n", __FUNCTION__)
#define LOG_TRACE_ENTER_ARGS(x, ...) \
               LOG_TRACE(">> %s(" x ")\n", __FUNCTION__, __VA_ARGS__)
#define LOG_TRACE_EXIT() \
               LOG_TRACE("<< %s()\n", __FUNCTION__)
#define LOG_TRACE_EXIT_RETURN(x, ...) \
               LOG_TRACE("<< %s() = " x "\n", __FUNCTION__, __VA_ARGS__)

#endif /* SERVAL_LOG_H_ */
