/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Security.h
 * @brief Security configurations of the library
 *
 * This header contains the security configurations which the user can use to
 * customize the security settings and resources of the library.
 */

#ifndef SERVAL_SECURITY_H_
#define SERVAL_SECURITY_H_

#include <Serval_Defines.h>
#include <Serval_Msg.h>
#include <Serval_Basics.h>

#ifndef SERVAL_SECURITY_API_VERSION
#define SERVAL_SECURITY_API_VERSION 1
#endif

#if SERVAL_SECURITY_API_VERSION != 1 && SERVAL_SECURITY_API_VERSION != 2
#error "SERVAL_SECURITY_API_VERSION must be 1 or 2"
#endif

/* UNIX uses 01. Jan 1970, NTP uses 01. Jan 1900
 * This contains 17 leap years
 * ( 70*365 + 17 ) * 24 * 60 * 60 = 2208988800
 */
#define UNIX_TIMESTAMP_OFFSET 2208988800

#if SERVAL_ENABLE_DTLS || SERVAL_ENABLE_TLS
#if SERVAL_SECURITY_API_VERSION == 2
enum SecurityTokenType_e
{
	CURRENT_TIME         = 0x0,
	CIPHERS              = 0x1,

	PSK_SERVER_ID_HINT   = 0x2,
	PSK_PEER_KEY_AND_ID  = 0x3,

	CRT_PEER_NAME        = 0x4,
	CRT_CA               = 0x5,

	CRT_OWN_CERTIFICATE  = 0x6,
};
typedef enum SecurityTokenType_e SecurityTokenType_t;

enum SecurityDeviceRole_e
{
	SERVER = 0x0,
	CLIENT = 0x1
};
typedef enum SecurityDeviceRole_e SecurityDeviceRole_t;

enum SecurityConnection_e
{
	TLS  = 0x0,
	DTLS = 0x1,
};
typedef enum SecurityConnection_e SecurityConnection_t;

struct SecurityToken_S
{
	SecurityTokenType_t  type:3;
	SecurityConnection_t connection:1;
	SecurityDeviceRole_t deviceRole:1;
	uint8_t              unused:3;
};
typedef struct SecurityToken_S SecurityToken_T;

struct InputBuffer_S
{
	uint16_t    length;
	const char* content;
};
typedef struct InputBuffer_S InputBuffer_T;

struct OutputBuffer_S
{
	uint16_t writableLength; // if this is 0 for an output, only provide reference
	uint16_t length;
	char*    content;
};
typedef struct OutputBuffer_S OutputBuffer_T;

struct PeerData_S
{
	union {
		Tcp_Listener_T* tcpServer; /// Our server socket. Valid if TLS and SERVER are set.
		Tcp_Socket_T*   tcpClient; /// Our TCP socket. Valid if TLS and CLIENT are set.
		Udp_Socket_T*   udp;       /// Our UDP socket over which the connection is opened. Valid if DTLS is set
	} socket;

	Ip_Address_T* address; /// Address of the peer. May be NULL to indicate validity for all peers
	Ip_Port_T     port;    /// Port of peer. May be 0 to indicate validity for all ports
};
typedef struct PeerData_S PeerData_T;

struct CurrentTimeData_S
{
	PeerData_T peer;
	uint32_t timestamp; ///time starting from Jan. 1 1900 (same as NTP seconds part)
};
typedef struct CurrentTimeData_S CurrentTimeData_T;

struct CiphersData_S
{
	PeerData_T peer;
	OutputBuffer_T ciphers;
};
typedef struct CiphersData_S CiphersData_T;

struct ServerIdHintData_S
{
	PeerData_T peer;
	OutputBuffer_T identity;
};
typedef struct ServerIdHintData_S ServerIdHintData_T;

struct PeerKeyAndIdData_S
{
	PeerData_T peer;

	InputBuffer_T peerIdentity;

	OutputBuffer_T key;
	OutputBuffer_T ourIdentity; //only to be used if CLIENT is set
};
typedef struct PeerKeyAndIdData_S PeerKeyAndIdData_T;

struct PeerNameData_S
{
	PeerData_T peer;
	OutputBuffer_T acceptedName;
};
typedef struct PeerNameData_S PeerNameData_T;

struct CaData_S
{
	PeerData_T peer;
	OutputBuffer_T trustedCertificates;
};
typedef struct CaData_S CaData_T;

struct OwnCertificateData_S
{
	PeerData_T peer;
	OutputBuffer_T certificate;
	OutputBuffer_T privateKey;
};
typedef struct OwnCertificateData_S OwnCertificateData_T;

/* Peer is always the first entry. This allows an easier handling
 * of PAL implementations.
 */
union SecurityData_U {
	CurrentTimeData_T    timeData;
	PeerData_T           peer;
	CiphersData_T        ciphersData;
	ServerIdHintData_T   serverIdHintData;
	PeerKeyAndIdData_T   peerKeyAndIdData;
	PeerNameData_T       peerNameData;
	CaData_T             caData;
	OwnCertificateData_T ownCertificateData;
};
typedef union SecurityData_U SecurityData_T;

static inline retcode_t Serval_copyToOutputBuffer(OutputBuffer_T* buffer, const char* data, uint16_t len)
{
	if ( buffer->writableLength == 0 )
	{
		buffer->length  = len;
		buffer->content = (char*) data;
		return RC_OK;
	}
	else if ( buffer->writableLength >= len )
	{
		buffer->length = len;
		memcpy(buffer->content, data, len);
		return RC_OK;
	}
	else
	{
		return RC_DTLS_INSUFFICIENT_MEMORY;
	}
}

/**
 * NOTE: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * Callback function used to obtain security information such as pre-shared-keys, certificates, etc.
 *
 * @param[in]      token
 *                 The type of token that is requested.
 *                 The token.type field determines the data type of tokenData.
 *                 The other fields in token determine the validity of fields in that data type
 *                 and provide additional information to the application about the type of
 *                 connection involved.
 *
 *                 The mapping of the token.type field to data types of tokenData is
 *                 CIPHERS              -> CiphersData_T,
 *                 PSK_SERVER_ID_HINT   -> ServerIdHintData_T,
 *                 PSK_PEER_KEY_AND_ID  -> PeerKeyAndIdData_T,
 *                 CRT_PEER_NAME        -> PeerNameData_T,
 *                 CRT_CA               -> CaData_T,
 *                 CRT_OWN_CERTIFICATE  -> OwnCertificateData_T,
 *
 * @param[in/out]  tokenData
 *                 Token data contains additional data to help identify what to provide to the
 *                 implementation as well as everything required to actually provide this data.
 *                 An application implementing the callback is expected to cast token data to
 *                 a type depending on the type field of the provided token.
 *
 *                 The data that is provided in output buffers must be in a format that the
 *                 underlying SSL implementation can understand. No conversion is performed.
 *
 * @return         RC_OK if the token was provided
 *                 RC_DTLS_TOKEN_NOT_PROVIDED if the application does not want to provide the token (e.g. client certificate)
 *                 RC_DTLS_PEER_REJECTED if the application does not want to communicate with that peer
 *                 RC_DTLS_UNSUPPORTED_TOKEN if the token type cannot be provided by the application
 *                 RC_DTLS_INSUFFICIENT_MEMORY if the token does not fit into the allocated space
 *
 * @since 1.7
 */
typedef retcode_t (*SecurityCallback_T) (SecurityToken_T token, SecurityData_T* tokenData);

/**
 * WARNING: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * This function sets the security callback which is called whenever security information, such
 * as keys, certificates, etc., must be provided to the application.
 *
 * @param[in] securityCallback
 *            the callback to be executed to ask the application for security-related information
 *
 * @see SecurityCallback_T
 * @since 1.7
 */
void Security_setCallback(SecurityCallback_T securityCallback);

#else /* SERVAL_SECURITY_SERVAL_SECURITY_API_VERSION == 2 */

/**
 * NOTE: this is a preliminary API implementation. This is subject to changes in futur releases
 *
 * Callback function used to get the client key material from the application
 *
 * @param[in] peer_identiy, this is the server identity
 * @param[out] my_identity
 * @param[in] id_max_len
 * @param[out] key
 * @param[in] key_max_len
 *
 * @return
 * length of the key.
 *
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @since 1.4
 */
typedef uint32_t (ClientPskCallback_T) (const MsgSendingCtx_T *msgCtx_ptr, const char* peer_identiy,
		char* my_identity, const unsigned int id_max_len,
		unsigned char* key, const unsigned int key_max_len);
/**
 * NOTE: this is a preliminary API implementation. This is subject to changes in futur releases
 *
 * Callback function used to get the server key material from the application
 *
 * @param[in] peer_identity, this is the client identity
 * @param[out] key
 * @param[in] key_max_len
 *
 * @return
 * length of the key.
 *
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @since 1.4
 */
typedef uint32_t (ServerPskCallback_T) (const MsgSendingCtx_T *msgCtx_ptr, const char* peer_identity,
		unsigned char* key, const unsigned int key_max_len);
#endif

#endif /* SERVAL_SECURITY_API_VERSION == 2 */

#if SERVAL_ENABLE_DTLS
/**
 * Enable SERVAL_TEST_DTLS in order to enable
 * the Datagram Transport Layer Security (dTLS) Testmode
 * this is only temporary used and will be removed
 */
#ifndef SERVAL_TEST_DTLS
#define SERVAL_TEST_DTLS 0
#endif

/**
 * Enable SERVAL_ENABLE_DTLS_PARALLEL_HANDSHAKE in order to enable
 * parallel processing of multiple Handshakes. If not enabled DTLS
 * handshakes are processed in sequential manner.
 */
#ifndef SERVAL_ENABLE_DTLS_PARALLEL_HANDSHAKE
#define SERVAL_ENABLE_DTLS_PARALLEL_HANDSHAKE 1
#endif /* SERVAL_ENABLE_DTLS_PARALLEL_HANDSHAKE */

/**
 * Set the number of retries for a particular flight until giving up.
 * Setting this parameter to 0 disables retries completely.
 * Note that this could lead to poor performance if packet loss is
 * possible on the transmission link and not mitigated through other means.
 */
#ifndef SERVAL_DTLS_FLIGHT_MAX_RETRIES
#define SERVAL_DTLS_FLIGHT_MAX_RETRIES 4
#endif

/**
 * Defines the interval in ms with which the resource monitor checks for DTLS timeouts.
 * Only relevant if SERVAL_DTLS_FLIGHT_MAX_RETRIES > 0 and DTLS is enabled
 */
#ifndef DTLS_RETRANSMISSION_MONITOR_INTERVAL
#define DTLS_RETRANSMISSION_MONITOR_INTERVAL 1000
#endif

/**
 * Enable SERVAL_ENABLE_DTLS_PSK in order to enable
 * pre-shared keys key exchange/creation in DTLS.
 */
#ifndef SERVAL_ENABLE_DTLS_PSK
#define SERVAL_ENABLE_DTLS_PSK 1
#endif /* SERVAL_ENABLE_DTLS_PSK */

/**
 * Enable SERVAL_ENABLE_DTLS_ECC in order to enable
 * elliptic curves key exchange/creation in DTLS.
 */
#ifndef SERVAL_ENABLE_DTLS_ECC
#define SERVAL_ENABLE_DTLS_ECC 0
#endif /* SERVAL_ENABLE_DTLS_ECC */

/**
 * define the timeout in ms for garbage collection of secure connections
 * of DTLS server.
 * it should be a Multiple of RESOURCE_MONITOR_PERIODE
 * of 0, if disabled (server application must clean up by itself)
 */
#ifndef SERVAL_SECURE_SERVER_CONNECTION_TIMEOUT
#define SERVAL_SECURE_SERVER_CONNECTION_TIMEOUT (60*1000)
#endif

#if SERVAL_ENABLE_DTLS_PSK && SERVAL_ENABLE_DTLS_ECC
#error "Only one of PSK or ECC for DTLS can be configured"
#endif

#if !(SERVAL_ENABLE_DTLS_PSK || SERVAL_ENABLE_DTLS_ECC)
#error "One of PSK or ECC for DTLS must be configured"
#endif

#if SERVAL_SECURITY_API_VERSION == 1
/**
 * WARNING: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * This function permits the application to set the PSK callback for the
 * client side
 *
 * @param[in] clientPskCallback
 *            callback used by serval to get the key material from the application
 *
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @see ClientPskCallback_T
 * @since 1.4
 */
void Dtls_setClientPskCallback(ClientPskCallback_T *clientPskCallback);

/**
 * WARNING: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * This function permits the application to set the PSK callback for the
 * server side
 *
 * @param[in] serverPskCallback
 *            callback used by serval to get the key material from the application
 *
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @see ServerPskCallback_T
 * @since 1.4
 */
void Dtls_setServerPskCallback(ServerPskCallback_T *serverPskCallback);

#endif /* SERVAL_SECURITY_API_VERSION == 1 */
#endif /* #if SERVAL_ENABLE_DTLS */


#if SERVAL_ENABLE_TLS

/**
 * Enable SERVAL_ENABLE_TLS_PSK in order to enable
 * pre-shared keys key exchange/creation in TLS.
 */
#ifndef SERVAL_ENABLE_TLS_PSK
#define SERVAL_ENABLE_TLS_PSK 0
#endif /* SERVAL_ENABLE_TLS_PSK */

/**
 * Enable SERVAL_ENABLE_TLS_ECC in order to enable
 * elliptic curves key exchange/creation in TLS.
 */
#ifndef SERVAL_ENABLE_TLS_ECC
#define SERVAL_ENABLE_TLS_ECC 1
#endif /* SERVAL_ENABLE_TLS_ECC */

#if SERVAL_ENABLE_TLS_PSK && SERVAL_ENABLE_TLS_ECC
#error "Only one of PSK or ECC for TLS can be configered"
#endif

#if !(SERVAL_ENABLE_TLS_PSK || SERVAL_ENABLE_TLS_ECC)
#error "One of PSK or ECC for TLS must be configered"
#endif

#if SERVAL_SECURITY_API_VERSION == 1
/**
 * WARNING: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * This function permits the application to set the PSK callback for the
 * client side.
 *
 * @param[in] clientPskCallback
 *            callback used by serval to get the key material from the application
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @see ClientPskCallback_T
 * @since 1.4
 */
void Tls_setClientPskCallback(ClientPskCallback_T *clientPskCallback);

/**
 * WARNING: this is a preliminary API implementation. This is subject to changes in future releases
 *
 * This function permits the application to set the PSK callback for the
 * server side
 *
 * @param[in] serverPskCallback
 *            callback used by serval to get the key material from the application
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 *
 * @see serverPskCallback_T
 * @since 1.4
 */
void Tls_setServerPskCallback(ServerPskCallback_T *serverPskCallback);
#endif /* SERVAL_SECURITY_API_VERSION == 1 */

#endif /* SERVAL_ENABLE_TLS */


#if SERVAL_ENABLE_DTLS || SERVAL_ENABLE_TLS

/**
 * Enum for DTLS Connection States
 * (used i.e. for getSecureConnectionState() )
 */

typedef enum
{
	/**
	 * Connection is free
	 */
	SECURE_STATE_FREE = 0,
	/**
	 * Connection is in negotiation handshake
	 */
	SECURE_STATE_IN_NEGOTIATION,
	/**
	 * Connection is established in view of cyassl, but
	 * send callback is pending
	 */
	SECURE_STATE_WAIT_ESTABLISHED,
	/**
	 * Connection is established, normal app data can be send/rceived
	 */
	SECURE_STATE_ESTABLISHED,
	/**
	 * Connection is in teardown handshake
	 */
	SECURE_STATE_HALFCLOSED,
	/**
	 * Connection teardown handshake ready but socket still not closed
	 */
	SECURE_STATE_CLOSING
} SecureConnectionState_T;

/**
 * Set SERVAL_MAX_SECURE_SOCKETS in order to enable
 * maximal amount of secure DTLS and TLS sockets.
 */
#ifndef SERVAL_MAX_SECURE_SOCKETS
#define SERVAL_MAX_SECURE_SOCKETS  5
#endif /* SERVAL_MAX_SECURE_SOCKETS */

/**
 * Set SERVAL_MAX_SECURE_CONNECTIONS in order to enable
 * maximal amount of secure DTLS and TLS connections.
 */
#ifndef SERVAL_MAX_SECURE_CONNECTIONS
#define SERVAL_MAX_SECURE_CONNECTIONS  6
#endif /* SERVAL_MAX_SECURE_CONNECTIONS */

#endif /* SERVAL_ENABLE_DTLS || SERVAL_ENABLE_TLS */

#endif /* SERVAL_SECURITY_H_ */
