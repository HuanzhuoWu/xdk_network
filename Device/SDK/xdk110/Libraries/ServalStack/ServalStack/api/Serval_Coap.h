/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Coap.h
 * @brief Interface to CoAP (RFC 7253) constants and methods
 * @ingroup coapgroup
 *
 * This module provides declarations which are commonly used by CoAP.
 * This also provide the common interfaces provided by CoAP module
 * e.g. interfaces to serialize or parse a CoAP message.
 */

#ifndef SERVAL_COAP_H_
#define SERVAL_COAP_H_

#include <Serval_Defines.h>
#include <Serval_Policy.h>

#if SERVAL_ENABLE_COAP  /* if CoAP is disabled -> no compilation */

#include <Serval_Types.h>
#include <Serval_Msg.h>

/**
 * This is the maximal number of parallel CoAP sessions. By default,
 * COAP_MAX_NUM_ENDPOINTS should be equal to SERVAL_MAX_NUM_MESSAGES, which
 * means that all supported requests may be COAP requests.
 *
 * Note: Should be at least as large as number of UDP sockets provided on target
 * platform.
 */
#ifndef COAP_MAX_NUM_ENDPOINTS
#define COAP_MAX_NUM_ENDPOINTS    SERVAL_MAX_NUM_MESSAGES
#endif

/*
 *******************************************************************************
 * Parameters for COAP application layer protocol
 *******************************************************************************
 */
/**
 * This is the maximum length of token of a COAP message.
 * @since 1.4
 */
#ifndef COAP_MAX_TOKEN_SIZE
#define COAP_MAX_TOKEN_SIZE 8
#endif

/**
 * This is the length of the message buffer of a COAP message.
 * Increased to 200 for LWM2M since 1.4
 */
#ifndef COAP_MSG_MAX_LEN
#if SERVAL_ENABLE_LWM2M
#define COAP_MSG_MAX_LEN    200
#else
#define COAP_MSG_MAX_LEN    100
#endif
#endif

/**
 *  Check COAP COAP_MSG_MAX_LEN
 */
#if COAP_MSG_MAX_LEN > 1024
#error COAP_MSG_MAX_LEN must not exceed 1024
#endif /* COAP_MSG_MAX_LEN */

/**
 * This is the maximal count of remembered sent COAP message IDs
 * to identify duplicate messages.
 */
#ifndef COAP_MAX_MSGID_COUNT
#define COAP_MAX_MSGID_COUNT    10
#endif

/**
 * Restrict COAP to non-confirmable messages only
 */
#ifndef COAP_ENABLE_CONFIRMABLE_MSG
#define COAP_ENABLE_CONFIRMABLE_MSG   1
#endif

/**
 *  if COAP_ENABLE_CONFIRMABLE_MSG => SERVAL_ENABLE_COAP must be enabled
 */
#if COAP_ENABLE_CONFIRMABLE_MSG
#if !SERVAL_ENABLE_COAP
#error CoAP CONFIRMABLE should be enabled only if coap is enabled
#endif
#endif /* COAP_ENABLE_CONFIRMABLE_MSG */

/**
 * Enable support for CoAP a client to observe resources in on a CoAP server.
 * Essentially this enables notifications sent by the Server.
 * See draft-ietf-core-observe
 * @since 1.4
 */
#ifndef SERVAL_ENABLE_COAP_OBSERVE
#define SERVAL_ENABLE_COAP_OBSERVE 0
#endif

/**
 * The maximum number of observations that can be registered
 * @since 1.4
 */
#ifndef COAP_MAX_NUM_OBSERVATIONS
#define COAP_MAX_NUM_OBSERVATIONS COAP_MAX_NUM_ENDPOINTS
#endif

#if SERVAL_ENABLE_DTLS_SERVER && SERVAL_ENABLE_COAP_OBSERVE
#if ! SERVAL_ENABLE_DTLS_CLIENT
#error Coap observe requires DTLS client functionality at the moment
#endif
#endif

/**
 * Enables the processing of duplicated Confirmable POST requests. This should
 * be only done if the request can be handled in an idempotent fashion. For
 * example, if the result of a POST request is just the creation of some short-
 * lived state at the server.
 * Disabling leads to discarding duplicated CON POST requests and sending a RST
 * back.
 */
#ifndef COAP_CON_PROCESS_DUPLICATE_POST
#define COAP_CON_PROCESS_DUPLICATE_POST   0
#endif


/**
 *  Check CoAP session value
 */
#if (COAP_MAX_NUM_ENDPOINTS < SERVAL_MAX_NUM_MESSAGES)
#error COAP_MAX_NUM_ENDPOINTS must be greater or equal to SERVAL_MAX_NUM_MESSAGES
#endif

#if (COAP_ENABLE_CONFIRMABLE_MSG && SERVAL_MAX_NUM_MESSAGES == 1)
#error "Support for confirmable messages requires at least two messages"
#endif
/*
 *******************************************************************************
 * Parameters for COAP Endpoint monitoring
 *******************************************************************************
 */

/**
 * define the timeout in ms for garbage collection of CoAP endpoint
 * it should be a Multiple of RESOURCE_MONITOR_PERIODE
 */
#ifndef COAP_ENDPOINT_MONITOR_TIMEOUT
#define COAP_ENDPOINT_MONITOR_TIMEOUT ((uint32_t)(COAP_MAX_RTT * 1000))
#endif

/**
 * define the timeout in ms for garbage collection of CoAP received message Ids
 * it should be a Multiple of RESOURCE_MONITOR_PERIODE
 */
#ifndef COAP_RECMSG_MONITOR_TIMEOUT
#define COAP_RECMSG_MONITOR_TIMEOUT ((uint32_t)(COAP_NON_LIFETIME * 1000))
#endif


/**
 * ACK_TIMEOUT in seconds (default value 1)
 */
#ifndef COAP_ACK_TIMEOUT
#define COAP_ACK_TIMEOUT   1
#endif

/**
 * COAP_ACK_CHECK_INTERVAL is the interval (in ms) with which
 * a message is checked for ACK_TIMEOUT expiration.
 * It is very strongly recommended that this number is a
 * divisor of COAP_ACK_TIMEOUT * 1000.
 *
 * Increasing this number improves energy efficiency since the
 * check is done less frequently. However, the jitter of the
 * retransmission is increased as well, since more time can pass
 * between two checks.
 */
#ifndef COAP_ACK_CHECK_INTERVAL
#define COAP_ACK_CHECK_INTERVAL (COAP_ACK_TIMEOUT * 1000)
#endif

/**
 * ACK_RANDOM_FACTOR (default value 1.5)
 */
#define COAP_ACK_RANDOM_FACTOR   1.5

/**
 * MAX_RETRANSMIT (default value 4)
 */
#ifndef COAP_MAX_RETRANSMIT
#define COAP_MAX_RETRANSMIT   4
#endif

/**
 * NSTART (default value 1)
 */
#define COAP_NSTART   1

/**
 * DEFAULT_LEISURE in seconds (default value 5)
 */
#define COAP_DEFAULT_LEISURE   5

/**
 * PROBING_RATE in byte/second (default value 1)
 */
#define COAP_PROBING_RATE   1

/**
 * PROCESSING_DELAY is the time a node takes to turn around a confirmable
 * message into an acknowledgement.
 * As a conservative assumption we set it equal to ACK_TIMEOUT.
 */
#ifndef COAP_PROCESSING_DELAY
#define COAP_PROCESSING_DELAY    COAP_ACK_TIMEOUT
#endif

/**
 * MAX_LATENCY (in seconds) is the maximum time a datagram is expected to take
 * from the start of its transmission to the completion of its reception.
 * We define MAX_LATENCY to be 2 seconds. (default value in RFC 100)
 */
#ifndef COAP_MAX_LATENCY
#define COAP_MAX_LATENCY   2
#endif

/**
 * Overload queue size, This macro gives maximum number of simultaneous overload
 * response the module can handle
 */
#ifndef COAP_OVERLOAD_QUEUE_SIZE
#define COAP_OVERLOAD_QUEUE_SIZE 10
#endif

/**
 * This macro gives maximum expected size of token.
 */
#define COAP_TOKEN_MAX_LENGTH 8

/**
 * This macro gives default value in seconds for a max age in service
 * unavailable response.
 */
#define COAP_DEFAULT_MAX_AGE 2

/**
 * This macro gives two to the power of given exponent (2^exp)
 */
#define POW_OF_TWO(n) (1 << n)


#define COAP_MAX_RTT \
   (2 * COAP_MAX_LATENCY + COAP_PROCESSING_DELAY)


/**
 * MAX_TRANSMIT_SPAN is the maximum time from the first transmission of a
 * confirmable message to its last retransmission.
 */
#define COAP_MAX_TRANSMIT_SPAN \
   (COAP_ACK_TIMEOUT * \
   (POW_OF_TWO(COAP_MAX_RETRANSMIT) - 1) * \
   COAP_ACK_RANDOM_FACTOR)


/**
 * MAX_TRANSMIT_WAIT is the maximum time from the first transmission of a
 * confirmable message to the time when the sender gives up on receiving an
 * acknowledgement or reset.
 */
#define COAP_MAX_TRANSMIT_WAIT \
   (COAP_ACK_TIMEOUT * \
   (POW_OF_TWO((COAP_MAX_RETRANSMIT + 1)) - 1) * \
   COAP_ACK_RANDOM_FACTOR)


/**
 * EXCHANGE_LIFETIME (in seconds) is the time from starting to send a
 * confirmable message to the time when an acknowledgement is no longer
 * expected.
 */
#define COAP_EXCHANGE_LIFETIME \
   ((COAP_ACK_TIMEOUT * \
   (POW_OF_TWO(COAP_MAX_RETRANSMIT) - 1) * \
   COAP_ACK_RANDOM_FACTOR) + \
   (2 * COAP_MAX_LATENCY) + \
   COAP_PROCESSING_DELAY)


/**
 * NON_LIFETIME (in seconds) is the time from sending a non-confirmable message
 * to the time its message-ID can be safely reused.
 * If multiple transmission of a NON message is not used, its value is
 * MAX_LATENCY, or 100 seconds. However, a CoAP sender might send a NON message
 * multiple times, in particular for multicast applications. While the period of
 * re-use is not bounded by the specification, an expectation of reliable
 * detection of duplication at the receiver is in the timescales of
 * MAX_TRANSMIT_SPAN (RFC 7252).
 */
#define COAP_NON_LIFETIME \
  (COAP_MAX_TRANSMIT_SPAN + \
  COAP_MAX_LATENCY)

/** This enum gives the type of CoAP message being serialized */
typedef enum
{
	RESPONSE = 0,
	REQUEST
} CoapSerializeType_T;
/** This enum gives the type of CoAP message */
typedef enum
{
	COAP_MSG_CON = 0,
	COAP_MSG_NON,
	COAP_MSG_ACK,
	COAP_MSG_RST
} CoapMsg_Type_T;

/** This enum is used to fetch the option number from the option table */
typedef enum
{
	COAP_RESERVED1 = 0,
	COAP_IF_MATCH,
	COAP_URI_HOST,
	COAP_ETAG,
	COAP_IF_NONE_MATCH,
#if SERVAL_ENABLE_COAP_OBSERVE
	COAP_OBSERVE,
#endif
	COAP_URI_PORT,
	COAP_LOCATION_PATH,
	COAP_URI_PATH,
	COAP_CONTENT_FORMAT,
	COAP_MAX_AGE,
	COAP_URI_QUERY,
	COAP_ACCEPT,
#if ! SERVAL_POLICY_COAP_STRICT_TOKEN_API
	COAP_TOKEN,
#endif
	COAP_LOCATION_QUERY,
	COAP_BLOCK_2,
	COAP_BLOCK_1,
	COAP_PROXY_URI,
	COAP_RESERVED2,
	COAP_RESERVED3,
	COAP_RESERVED4
} Coap_OptionNumbers_T;

/** This enum is used to fetch the content format from the table */
typedef enum
{
	TEXT_PLAINCHARSET_UTF8 = 0,
	APPLICATION_LINK_FORMAT,
	APPLICATION_XML,
	APPLICATION_OCTET_STREAM,
	APPLICATION_EXI,
	APPLICATION_JSON,
	AE_BIKE_SENSOR_FORMAT_201
} Coap_ContentFormatEntry_T;

/** This enum is used to fetch the request or response code from the table */
typedef enum
{
	/** Request codes */
	/* 0.01 */ COAP_GET = 0,
	/* 0.02 */ COAP_POST,
	/* 0.03 */ COAP_PUT,
	/* 0.04 */ COAP_DELETE,

	/** Response codes */
	/* 2.01 */ COAP_CREATED,
	/* 2.02 */ COAP_DELETED,
	/* 2.03 */ COAP_VALID,
	/* 2.04 */ COAP_CHANGED,
	/* 2.05 */ COAP_CONTENT,
	/* 2.31 */ COAP_CONTINUE,
	/* 4.00 */ COAP_BAD_REQUEST,
	/* 4.01 */ COAP_UNAUTHORIZED,
	/* 4.02 */ COAP_BAD_OPTION,
	/* 4.03 */ COAP_FORBIDDEN,
	/* 4.04 */ COAP_NOT_FOUND,
	/* 4.05 */ COAP_METHOD_NOT_ALLOWED,
	/* 4.06 */ COAP_NOT_ACCEPTABLE,
	/* 4.08 */ COAP_REQUEST_ENTITY_INCOMPLETE,
	/* 4.12 */ COAP_PRECONDITION_FAILED,
	/* 4.13 */ COAP_REQUEST_ENTITY_TOO_LARGE,
	/* 4.15 */ COAP_UNSUPPORTED_CONTENT_FORMAT,
	/* 5.00 */ COAP_INTERNAL_SERVER_ERROR,
	/* 5.01 */ COAP_NOT_IMPLEMENTED,
	/* 5.02 */ COAP_BAD_GATEWAY,
	/* 5.03 */ COAP_SERVICE_UNAVAILABLE,
	/* 5.04 */ COAP_GATEWAY_TIMEOUT,
	/* 5.05 */ COAP_PROXYING_NOT_SUPPORTED
} Coap_Codes_T;

/**
 * This array holds the values for various options as defined in CoAP
 * specification.
 */
extern uint16_t const Coap_Options[];

/**
 * This array holds the values for various content formats as defined in CoAP
 * specification.
 */
extern uint16_t const Coap_ContentFormat[];

/**
 * This array holds the values for various request and response codes as defined
 *  in CoAP specification.
 */
extern uint8_t const Coap_Codes[];

/**
 * This structure represents a single CoAP option, this may be used for passing
 * an option to serializer or from parser and in such a case length = 0 means
 * default value for that option.
 */
struct CoapOption_S
{
	/** Option number, not delta*/
	uint16_t OptionNumber;
	/** Length of the option */
	uint16_t length;
	/** This is reference to memory which gives the value of the option */
	uint8_t const* value;
};

/**
 * This structure represents a single CoAP option.
 * @see struct CoapOption_S
 */
typedef struct CoapOption_S CoapOption_T;

/**
 *  This data structure represents a serializing context used during
 *  serialization of an outgoing CoAP message.
 */
struct CoapSerializer_S
{
	struct
	{
#if ! SERVAL_POLICY_COAP_REMOVE_SERIALIZER_OPTION_COUNT
		/** This gives how many options are serialized so far, maximum of 15 */
		uint8_t optionSerializeCount:4;
#endif
#if ! SERVAL_POLICY_COAP_STRICT_TOKEN_API
		/** This indicate if token is serialized so far or not */
		uint8_t tokenSerialized:1;
#endif
		/**
		 * This indicate the whether the message being serialized is request or
		 * response.
		 */
		CoapSerializeType_T type:1;
		uint8_t payloadSerialized:1;
	} flags;
	/** This buffer is used for message serialization*/
	char* buf_ptr;
	/** Length of the buffer provided */
	unsigned int bufLen;
	/** Total length of the constructed message */
	unsigned int len;
	/** This holds last option delta */
	unsigned int lastOptionNum;
};

/**
 * This data structure represents a serializing context.
 *
 * @see struct CoapSerializer_S
 */
typedef struct CoapSerializer_S CoapSerializer_T;

/**
 *  This data structure represents a parsing context used during
 *  parsing of an incoming CoAP message.
 */
struct CoapParser_S
{
	struct
	{
#if ! SERVAL_POLICY_COAP_OPTION_COUNT
		uint8_t optionParsedCount:4;
#endif
#if ! SERVAL_POLICY_COAP_STRICT_TOKEN_API
		uint8_t tokenOptionGenerated:1;
#endif
	} flags;
	/** This buffer is used for message serialization*/
	unsigned char* buf_ptr;
	/** Length of the buffer provided */
	unsigned int bufLen;
	/** Total length of the parsed message */
	unsigned int len;
	/** This holds last option delta */
	uint16_t lastOptionNum;
};

/**
 * This data structure represents a parsing context.
 *
 * @see struct CoapParser_S
 */
typedef struct CoapParser_S CoapParser_T;

/**
* This data type represents a COAP endpoint and holds endpoint related data.
*
* @see struct CoapEndPoint_S
*/
typedef struct CoapEndPoint_S CoapEndPoint_T;

/**
 * This data type represents a COAP Session and holds Session related data.
 * A session is actually represented by the corresponding endpoint.
 */
typedef CoapEndPoint_T CoapSession_T;

/**
 * This function may be called by application from sent callback to get the
 * current session.
 *
 * @param[in] callback_ptr
 * This is the callback passed in the sent callback
 *
 * @return, reference to the current session
 */
CoapSession_T* CoapSession_getSessionFromCallable(Callable_T *callback_ptr);

/**
 * This function may be called by application from sent callback to get the
 * current session.
 *
 * @param[in] msg_ptr
 * This is the reference to message associated with the session.
 *
 * @return, reference to the current session
 */
CoapSession_T* CoapSession_getSessionFrmMsg(Msg_T *msg_ptr);

/**
 * This function is called to initialize the context of the CoAP serializer for
 * serializing a given message. It's very important to handle the errors thrown
 * by this function. Because it reserves message which never gets released.
 * To resolve this problem, always close the message if CoapSerializer_setup throws
 * any error instead of RC_OK.
 *
 * @param[out] serializer_ptr
 * Reference to a CoapSerializer_T object which identifies the
 * instance of serializing context which should be setup. It has to be a
 * valid pointer.
 *
 * @param[in] msg_ptr
 * Pointer to outing message which should be serialized.
 *
 * @param[in] type
 * Defines the type (request or response) of CoAP message to be serialized.
 *
 * @return RC_OK, on success
 */
retcode_t CoapSerializer_setup(CoapSerializer_T *serializer_ptr,
                               Msg_T *msg_ptr, CoapSerializeType_T type);

/**
 * This function is called to serialize CoAP message type in the outgoing
 * message.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] isCon
 * CON type of CoAP message to be serialized if true else NON.
 */
void CoapSerializer_setConfirmable(Msg_T *msg_ptr, bool isCon);

/*
 * Serializes the token.
 *
 * @param[in] serializer_ptr
 * Reference to a CoapSerializer_T object which is needed for creating a CoAP
 * message. The object must haven been initialized before invoking this function
 * @see CoapSerializer_setup
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents the message to be serialized
 *
 * @param[in] token_ptr
 * Pointer to the token that is to be serialized. NULL if a new token is to be
 * generated. This token will be 2 byte long, unless the tokenSize is also 0.
 *
 * @param[in] tokenSize
 * length in bytes of the token provided in token_ptr. This parameter is ignored
 * if token_ptr is NULL unless it is also 0 in which case no token is used.
 *
 * return RC_OK on success, RC_COAP_SERIALIZE_ERROR otherwise
 */
retcode_t CoapSerializer_serializeToken(CoapSerializer_T *serializer_ptr, Msg_T *msg_ptr, const uint8_t * token_ptr, uint8_t tokenSize);

/*
 * Reuses the token from the request message.
 * Note that this will only work, if msg_ptr is reused between request and response!
 *
 * @param[in] serializer_ptr
 * Reference to a CoapSerializer_T object which is needed for creating a CoAP
 * message. The object must haven been initialized before invoking this function
 * @see CoapSerializer_setup
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents the message to be serialized
 *
 * @since 1.4
 */
retcode_t CoapSerializer_reuseToken(CoapSerializer_T *serializer_ptr, Msg_T *msg_ptr);

/**
 * This function is called to serialize CoAP message code in the outgoing
 * message.
 *
 * @param[in] serializer_ptr
 * Reference to a CoapSerializer_T object which is needed for creating a CoAP
 * message. The object must be initialized before invoking this function.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] code
 * Code of CoAP message to be serialized.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_SERIALIZE_ERROR, if the code does not match to the message type
 * (request/ response).
 */
retcode_t CoapSerializer_setCode(CoapSerializer_T *serializer_ptr,
                                 Msg_T *msg_ptr, uint8_t code);

/**
 * This function is called to serialize a option in the outgoing message. It
 * calculates the delta, length and the position of option value from passed
 * object of type CoapOption_T and the serializing context and populate the
 * buffer with these values.
 *
 * @param[in,out] serializer_ptr
 * Reference to a CoapSerializer_T object which identifies the current
 * instance of serializing context which should be used for serializing. It has
 * to be a valid pointer.
 *
 * @param[out] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] option_ptr
 * Reference to a CoapOption_T object which should hold the value of option to
 * be serialized.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
retcode_t CoapSerializer_serializeOption(CoapSerializer_T *serializer_ptr,
        Msg_T* msg_ptr, CoapOption_T *option_ptr);

/**
 * This function is called to serialize a end of option marker in the outgoing
 * message.
 *
 * @param[in,out] serializer_ptr
 * Reference to a CoapSerializer_T object which identifies the current
 * instance of serializing context which should be used for serializing. It has
 * to be a valid pointer.
 *
 * @param[out] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
retcode_t CoapSerializer_setEndOfOptions(CoapSerializer_T *serializer_ptr,
        Msg_T* msg_ptr);

/**
 * This function is called to serialize CoAP payload in the outgoing
 * message.
 *
 * @param[in,out] serializer_ptr
 * Reference to a CoapSerializer_T object which identifies the current
 * instance of serializing context which should be used for serializing. It has
 * to be a valid pointer.
 *
 * @param[out] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] payload_ptr
 * Reference to a buffer which holds payload for outgoing CoAP message.
 *
 * @param[in] payloadLen
 * Length of buffer which holds payload for outgoing CoAP message.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_SERIALIZE_ERROR otherwise.
 */
retcode_t CoapSerializer_serializePayload(CoapSerializer_T *serializer_ptr,
        Msg_T* msg_ptr, uint8_t *payload_ptr, uint16_t payloadLen);

/**
* This function sets the given 8-bit integer value into the given option and
* sets the length to 1.
*
* @param[in] option_ptr
* Reference to the CoapOption_T to be set. It must be a valid pointer.
*
* @param[in] value
* The value to set.
*
* @param[in] buffer_ptr
* Reference to a buffer to which the value will be written to.
* This buffer must not be overwritten until CoapSerializer_serializeOption is
* called. It must be a valid pointer.
*/
void CoapSerializer_setUint8(CoapOption_T *option_ptr, uint8_t value,
                             uint8_t* buffer_ptr);

/**
* This function sets the given 16-bit integer value into the given option and
* sets the length to 2. It does the needed conversion from the host byte order
* to the network byte order.
*
* @param[in] option_ptr
* Reference to the CoapOption_T to be set. It must be a valid pointer.
*
* @param[in] value
* The value to set.
*
* @param[in] buffer_ptr
* Reference to a buffer to which the converted value will be written to.
* This buffer must not be overwritten until CoapSerializer_serializeOption is
* called. It must be a valid pointer.
*/
void CoapSerializer_setUint16(CoapOption_T *option_ptr, uint16_t value,
                              uint16_t* buffer_ptr);

/**
* This function sets the given 32-bit integer value into the given option and
* sets the length to 4. It does the needed conversion from the host byte order
* to the network byte order.
*
* @param[in] option_ptr
* Reference to the CoapOption_T to be set. It must be a valid pointer.
*
* @param[in] value
* The value to set.
*
* @param[in] buffer_ptr
* Reference to a buffer to which the converted value will be written to.
* This buffer must not be overwritten until CoapSerializer_serializeOption is
* called. It must be a valid pointer.
*/
void CoapSerializer_setUint32(CoapOption_T *option_ptr, uint32_t value, uint32_t* buffer_ptr);

/**
 * This function is called to parse a option from the received message. It
 * calculates the option number, length and the position of option value in the
 * buffer and populate the passed object of type CoapOption_T with these values.
 *
 * @param[in,out] parser_ptr
 * Reference to a CoapParser_T object which identifies the instance of parsing
 * context which should be used for parsing. It has to be a valid pointer.
 *
 * @param[out] option_ptr
 * Reference to a CoapOption_T object which should hold the value of the parsed
 * option, once the function returns.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_COAP_END_OF_OPTIONS if it detects end of option marker<br>
 * RC_COAP_SERIALIZE_ERROR otherwise.
 */
retcode_t CoapParser_getNextOption(CoapParser_T *parser_ptr,
                                   CoapOption_T *option_ptr);

/**
 * This function is called to initialize the context of the CoAP parser for
 * parsing a given message.
 *
 * @param[out] parser_ptr
 * Reference to a CoapParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[in] msg_ptr
 * Pointer to message which should be parsed. It does not need to be
 * zero-terminated. The content will not be changed.
 * It has to be a valid pointer to a CoAP message.
 */
void CoapParser_setup(CoapParser_T *parser_ptr, Msg_T *msg_ptr);

/**
 * This function is called to initialize the context of the CoAP parser for
 * parsing a given buffer with given length.
 *
 * @param[out] parser_ptr
 * Reference to a CoapParser_T object which identifies the instance of parsing
 * context which should be setup. It has to be a valid pointer.
 *
 * @param[in] buff_ptr
 * Pointer to buffer which should be parsed. It does not need to be
 * zero-terminated. The content will not be changed.
 *
 * @param[in] bufLen
 * Length of the buffer which should be parsed.
 */
void CoapParser_setupRaw(CoapParser_T *parser_ptr,
                         uint8_t* buff_ptr, unsigned int bufLen);


/**
 * This function is called to fetch CoAP message type in the incoming
 * message.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @return Type of CoAP incoming message.
 */
CoapMsg_Type_T CoapParser_getType(Msg_T *msg_ptr);

/**
 * This function is called to fetch CoAP message code in the incoming
 * message.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object which represents current outgoing message.
 *
 * @return Code of CoAP incoming message.
 */
uint8_t CoapParser_getCode(Msg_T *msg_ptr);

/**
 * This function returns the option value for a given option number and message.
 * It returns only RC_OK, if the given option was found.
 *
 * @param[in] parser_ptr
 * Reference to a CoapParser_T object which identifies the instance of parsing
 * context which should be used for parsing. It has to be a valid pointer.
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object from which the option should be retrieved.
 *
 * @param[out] option_ptr
 * Reference to a CoapOption_T object where the option value should be stored.
 * It has to be a valid pointer.
 *
 * @param[in] optionNum
 * Coap Option number which should be retrieved.
 *
 * @return
 * RC_OK if given option was found.
 * RC_COAP_END_OF_OPTIONS if the option was not found.
 */
retcode_t CoapParser_getOption(CoapParser_T *parser_ptr, Msg_T *msg_ptr,
                               CoapOption_T *option_ptr, uint16_t optionNum);


/**
 * This function is called to get the payload of the received message. It
 * provides the reference to the payload and its length as output parameters.
 *
 * @param[in] parser_ptr
 * Reference to a CoapParser_T object which identifies the instance of parsing
 * context which should be used for parsing. It has to be a valid pointer.
 *
 * @param[out] payload_pptr
 * An output parameter which references to the payload of the parsed CoAP
 * message.
 *
 * @param[out] payloadLen_ptr
 * An output parameter for the length of the payload of the parsed CoAP message.
 */
#if SERVAL_POLICY_LARGE_PAYLOAD == 1
retcode_t CoapParser_getPayload(CoapParser_T *parser_ptr,
                                uint8_t const **payload_pptr,
                                uint16_t *payloadLen_ptr);
#else
retcode_t CoapParser_getPayload(CoapParser_T *parser_ptr,
                                uint8_t const **payload_pptr,
                                uint8_t *payloadLen_ptr);
#endif

/**
 * This function returns the integer value of the given option. It does the
 * needed conversion from the network byte order to the host byte order.
 *
 * @param[in] option_ptr
 * Reference to the CoapOption_T to be evaluated. It must be a valid pointer.
 *
 * @return Integer value of the given CoAP option.
 */
uint32_t CoapParser_getUint(CoapOption_T const *option_ptr);

/**
 * This function returns the token of the CoAP message and the length of
 * the token. It is safe to call this function even if the message pointed
 * to by msg_ptr does not contain a token (in which case NULL is returned).
 *
 * @param[in] msg_ptr
 * Reference to a Msg_T object from which the token should be retrieved.
 *
 * @param[out] tokenLength_ptr
 * The length of the token present in the message (0-8)
 *
 * @return
 * Pointer to the token field in the message or NULL if no token is present
 *
 * @since 1.3
 */
uint8_t* CoapParser_getToken(Msg_T *msg_ptr, uint8_t *tokenLength_ptr);


/**
 *  Check Timing parameters
 */
#if (COAP_ACK_CHECK_INTERVAL % SERVAL_RESOURCE_MONITOR_PERIODE)
#warning "COAP_ACK_CHECK_INTERVAL is not divided by SERVAL_RESOURCE_MONITOR_PERIODE"
#endif

#if (COAP_ACK_TIMEOUT > 1 && COAP_ACK_TIMEOUT % 2)
#warning "COAP_ACK_TIMEOUT should be divisible by 2"
#endif

/* A little bit convoluted. The Random factor is 1.5 so multiply by 3 and divide by 2
 * since the preprocessor cannot do float.
 */
#if (COAP_ACK_TIMEOUT > 1 && ((COAP_ACK_TIMEOUT * 3 * 1000 / 2) % COAP_ACK_CHECK_INTERVAL))
#warning "COAP_ACK_CHECK_INTERVAL should divide COAP_ACK_TIMEOUT * 1.5"
#endif

#endif /* SERVAL_ENABLE_COAP */

#endif /* SERVAL_COAP_H_ */
