/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Uuid.h
 * @brief Uuid Module
 * @ingroup dpwsgroup
 *
 * Interface providing data types and functions to handle UUIDs.
 */

#ifndef SERVAL_UUID_H_
#define SERVAL_UUID_H_ 1

#include <Serval_Defines.h>
#if SERVAL_ENABLE_DPWS
#include <Serval_Types.h>
#include <Serval_Exceptions.h>

#if 0
typedef unsigned char Uuid_T[DPWS_LENGTH_UUID];
#endif

typedef struct __Uuid_S Uuid_T;

/**
 * This data structure represents a context for universally unique
 * identifier(UUID).
 *
 * UUID consists of 16 x 8 bit integers. These imply in the hex-decimal
 * presentation a string of 32 octets.
 * E.g. "e92ec564-faed-4146-b1a7-775b0f884e20"
 */
struct __Uuid_S
{
	/*array for holding UUID (16 x 8 bit integers) */
	uint8_t a[16];
};

/**
 * This function is called to copy a universally unique identifier as it is to
 * a UUID context.
 *
 * @param[out] uuid_ptr
 * Reference to a Uuid_T object which the UUID is to be copied. It has to be a
 * valid pointer.
 *
 * @param[in] a_ptr
 * Pointer to data from where UUID needs to be copied.
 */
void Uuid_setRaw(Uuid_T *uuid_ptr, uint8_t *a_ptr);

/**
 * This function is called to convert a numeric UUID to its ASCII
 * representation.
 *
 * @param[in] uuid_ptr
 * Reference to a Uuid_T object which the UUID is to be converted. It has to be
 * a valid pointer.
 *
 * @param[out] buf_ptr
 * The buffer to which the converted value should be written.
 *
 * @param[in] bufSize
 * The size of the buffer
 *
 * @return RC_OK, if successful
 * RC_DPWS_UUID_LENGTH, if insufficient bufSize.
 */
retcode_t Uuid_toString(Uuid_T const *uuid_ptr, char *buf_ptr, int bufSize);

/**
 * This function is called to convert a UUID from its ASCII representation to
 * a numeric UUID.
 *
 * @param[out] uuid_ptr
 * Reference to a Uuid_T object which will be destination for converted UUID.
 * It has to be a valid pointer.
 *
 * @param[in] str_ptr
 * Pointer to the ASCII representation of UUID.
 *
 * @return RC_OK, if successful
 * RC_UUID_ERROR, otherwise.
 */
retcode_t Uuid_parse(Uuid_T *uuid_ptr, char const *str_ptr);

/**
 * This function is called to generate a random UUID.
 *
 * @param[out] uuid_ptr
 * Reference to a Uuid_T object which will be destination for generated UUID.
 * It has to be a valid pointer.
 */
void Uuid_random(Uuid_T *uuid_ptr);

/**
 * This function is called to increment a UUID.
 *
 * @param[in,out] uuid_ptr
 * Reference to a Uuid_T object which contains UUID to be incremented.
 * It has to be a valid pointer.
 */
void Uuid_increment(Uuid_T *uuid_ptr);

#endif /* SERVAL_ENABLE_DPWS */
#endif /* SERVAL_UUID_H_ */
