/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Version.h
 * @brief Interface to query the version of the communication stack.
 *
 * This file provides functions that allow the developer to obtain version
 * information about the currently used communication stack.
 */
#ifndef SERVAL_VERSION_H_
#define SERVAL_VERSION_H_

/** This macro gives the major version of the stack */
#define SERVAL_VERSION_MAJOR		1
/** This macro gives the minor version of the stack */
#define SERVAL_VERSION_MINOR		7
/** This macro gives the version of latest patch applied to the stack */
#define SERVAL_VERSION_PATCH		1

/**
 * This is a macro to help compare version numbers in code.
 * Use to compare against the current serval version:
 *
 * e.g.
 * #if CURRENT_SERVAL_VERSION < SERVAL_VERSION(1,4,0)
 * #error "Require at least version 1.4 of the serval stack
 * #endif
 *
 * @sa CURRENT_SERVAL_VERSION
 * @since 1.4.0
 */
#define SERVAL_VERSION(major, minor, patch) (((major) << 16) + ((minor) << 8) + (patch))

/** The current serval version **/
#define CURRENT_SERVAL_VERSION SERVAL_VERSION(SERVAL_VERSION_MAJOR, SERVAL_VERSION_MINOR, SERVAL_VERSION_PATCH)

/**
 * Returns the major version number of the communication stack.
 *
 * @return
 * The major version number of the stack.
 */
unsigned int Version_getMajor(void);


/**
 * Returns the minor version number of the communication stack.
 *
 * @return
 * The minor version number of the stack.
 */
unsigned int Version_getMinor(void);


/**
 * Returns the patch version number of the communication stack.
 *
 * @return
 * The patch version number of the stack.
 */
unsigned int Version_getPatch(void);


/**
 * Returns the build revision number of the communication stack.
 * This number is only non-zero in development builds of the stack.
 * For releases this number will always be zero.
 *
 * @return
 * The build revision number of the stack.
 */
unsigned int Version_getRevision(void);

#endif /* SERVAL_VERSION_H_ */
