/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_OpControl.h
 * @brief An interface to control the operation of stack.
 * @ingroup utilgroup
 *
 * The header file provides functions to pre-initialize the stack components,
 * which means, setting some essential flags to zero in the case this is not
 * done by the boot loader. This must be done exactly once when the
 * devices boots.
 *
 * In some applications, the device needs to periodically switch off and go to
 * a deep-sleep state, where RAM and CPU registers are not retained.
 * The header provides the functions to put the stack to sleep and to wake it
 * up in order to support such a duty-cycling mechanism.
 *
 * It also allows the application to register the application functions which
 * are needed by the stack. These are the following:
 * <UL>
 * <LI>
 * The emergency function function which will be invoked in the case of an
 * fatal error
 * </LI>
 * <LI>
 * The application data access function which enables the stack to access
 * application specific data items such as serial number, model name, etc.
 * </LI>
 * <LI>
 * The access function to the non-volatile memory to save and restore stack
 * status before and after a sleep state.
 * </LI>
 * </UL>
 *
 * Each application needs to include this header and to invoke the following
 * functions at the initialization phase in order to be able to use the stack
 * correctly.
 *
 *
 */


#ifndef SERVAL_OPCONTROL_H_
#define SERVAL_OPCONTROL_H_

#include <Serval_Defines.h>
#include <PBasics.h>
#include <Serval_Types.h>
#include <Serval_Exceptions.h>

#ifndef SERVAL_PREINITIALIZED_DATA
#error PAL must define the macro SERVAL_PREINITIALIZED_DATA with \
the value 1 or 0
#endif



/**
 * This data type represents a function pointer to an emergency function which
 * will be called by the stack to inform application, in case of a fatal error
 * is detected. The application must register such an emergency function.
 * @see OperationControl_registerEmergencyFunc()
 */
typedef void (*EmergencyFunc_T)(retcode_t);


#if SERVAL_ENABLE_APP_DATA_ACCESS

/**
 * An enumeration of the application data items which are relevant for the
 * stack. Notice that the relevant items depend on the stack features which are
 * enabled.
 */
typedef enum
{
#if SERVAL_ENABLE_DPWS
	/** reset counter managed by the application,
	 * it has type uint32_t -> 4 Bytes
	 */
	APPDATA_RESET_COUNTER = 1,

	/** device uuid determined by the application,
	 * length: 36 bytes
	 */
	APPDATA_DEVICE_UUID,

	/** device uuid determined by the application
	 * length <= SERVAL_DEVICE_MAX_LEN_SERIAL_NUM
	 */
	APPDATA_SERIAL_NUM,

	/** firmware version determined by the application
	 * length <= SERVAL_DEVICE_MAX_LEN_FIRMWARE_VERSION
	 */
	APPDATA_FIRMWARE_VERSION,

	/** device friendly name determined by the application
	 * length <= SERVAL_DEVICE_MAX_LEN_FRIENDLY_NAME
	 */
	APPDATA_DEVICE_FRIENDLY_NAME,

#endif
	APPDATA_NO_MORE_ITEM
} AppData_Item_T;


/**
 * This represents a function pointer to a function which is used by the stack
 * to access application data items such as serial number or model name.
 * The data type AppData_Item_T specifies the data
 * items which are relevant for the stack. Notice that the relevant items
 * depend on the stack features which are enabled.
 *
 * @param[in] item
 * Application data item
 *
 * @param[in] buff_ptr
 * Pointer to a buffer provided by the stack to read the data item into.
 *
 * @param[in,out] length_ptr
 * when the stack calls this function, length_ptr will refer to the size of the
 * provided buffer. The application must not overflow this maximal size when
 * copying data to the buffer. After the call, length_ptr will refer to the
 * length of the written data.
 *
 * @return: <br>
 * RC_OK if the data transfer was successful<br>
 * RC_APPDATA_PARAM_NOT_SUPPORTED if the given data item id is not supported<br>
 * RC_APPDATA_SIZE_NOT_FIT if the given buffer is too small and the data item
 * does not fit into it.
 *
 * @see AppData_Item_T, OpControl_registerAppDataFunc()
 */
typedef retcode_t (*AppData_Func_T)(AppData_Item_T item, void *buff_ptr,
                                    uint16_t* length_ptr);

#endif /* #if SERVAL_ENABLE_APP_DATA_ACCESS */


/**
 * A type definition for the non-volatile data of the stack.
 *
 * @see NVData_S
 */
typedef struct NVData_S NVData_T;


#if SERVAL_ENABLE_DUTY_CYCLING

/**
 * This is a typedef for access type (read or write) to the non-volatile memory
 * block.
 */
typedef enum
{
	NVDATA_DIR_READ,
	NVDATA_DIR_WRITE,
} NVData_Dir_T;

/**
 * A data type of a function for accessing the non-volatile data block of
 * the stack.
 *
 * This data type represents a function which is provided by the application
 * and used by the stack to access the non-volatile data block.
 *
 * @param[in] dir
 * The direction of the access, read or write.
 *
 * @param[in] data_ptr
 * pointer to the struct provided by the stack. In the case of a read access,
 * it is used to read the non-volatile data into it. In the case of write
 * access, it contains the data block which should be written to the
 * non-volatile memory.
 *
 * @return: <br>
 * RC_OK if the data transfer was successful<br>
 * RC_NV_MEMORY_ACCESS_ERROR if the access to non-volatile memory failed
 *
 * @see NVData_Dir_T, OpControl_registerNVDataFunc()
 */
typedef retcode_t (*NVData_Func_T)(NVData_Dir_T dir, NVData_T *data_ptr);

#endif /* #if SERVAL_ENABLE_DUTY_CYCLING */


/**
 * This function must be invoked on booting the device software. It prepares
 * the stack memory image. It must be called before calling
 * any other functions/modules of the stack, even before the initialization of
 * any modules of the stack.
 * This function must not called more than once when the system starts!
 * @return
 * RC_OK on success <br>
 * RC_PREINITIALIZE_FATAL fatal error<br>
 */
retcode_t  OpControl_preInitialize(void);


/**
 * This function initializes the operation control and the utility modules
 * including Log, Exception Handling, Resources Monitor, etc. The application
 * needs to explicitly invoke this function only if it needs to use any of the
 * utility feature, such as logging, before initializing any protocols of the
 * stack.
 *
 * @return RC_OK if initialization was successful<br>
 * RC_UTIL_INIT_ERROR if the initialization of the utility module fails.
 */
retcode_t  OpControl_initialize(void);


/**
 * This function registers an emergency function which is invoked in the case
 * of fatal exceptions. The application must provide such an emergency
 * callback which is able to recover the device from this faulty state. The
 * application may for example reboot the device or to put it in a safe state.
 *
 * @param[in] func
 * Reference to a EmergencyFunc_T object which defines a callback which
 * application wants exception handling module to call, in case of a fatal error
 * is detected. It has to be a valid pointer.
 */
void OpControl_registerEmergencyFunc(EmergencyFunc_T func);


/**
 * This function is used to notify the operation control module about
 * exceptions. It should be used when a runtime exception occurs and is being
 * handled. This function will log the exception. In the case of a fatal
 * exception it will also invoke the application emergency function, which has
 * been registered by the application using OpControl_registerEmergencyFunc().
 *
 * @param[in] exception
 * The exception code
 *
 * @see
 * OpControl_registerEmergencyFunc()
 */
void OpControl_notify(retcode_t exception);


#if SERVAL_ENABLE_APP_DATA_ACCESS
/**
 * This function registers application data access function which is used by
 * the stack to access application data items whenever needed.
 *
 * @param[in] func
 * Reference to an application data access function. It must be a valid pointer.
 */
void OpControl_registerAppDataFunc(AppData_Func_T func);

#endif /* SERVAL_ENABLE_APP_DATA_ACCESS */


#if SERVAL_ENABLE_DUTY_CYCLING
/**
 * Using this function the application provides the stack with the function to
 * access the non-volatile memory block.
 *
 * @param[in] func
 * The function to be used to access the non-volatile memory block.
 * It has to be a valid pointer.
 */
void OpControl_registerNVDataFunc(NVData_Func_T func);


/**
 * Try set the stack into sleep state.
 * If there are no ongoing actions, which cannot be stopped, the function
 * will return RC_OK. After this call no further stack requests are
 * committed. The stack enters state SLEEP.
 * If not successful the stack remains state ACTIVE and application shall
 * call OperationControl_trySleep later.
 *
 * @return
 * RC_OK when stack is set to sleep<br>
 * RC_RETRY_SLEEP_LATER stack not ready for sleep, retry function later<br>
 * RC_ALREADY_SLEEP when stack is already sleeping<br>
 * RC_SLEEP_MODE_ERROR<br>
 */
retcode_t OpControl_trySleep(void);


/**
 * Wake up the stack from sleep mode.
 *
 * @return
 * RC_OK when stack is awaken from sleep<br>
 * RC_DOESNT_SLEEP stack is already active<br>
 * RC_SLEEP_MODE_ERROR<br>
 */
retcode_t  OpControl_wakeUp(void);


/**
 * Return state of DutyCycling. If System is in sleep or
 * an OperationControl_trySleep sequence is running, the
 * function will return false.
 * Modules should not operate any activity if OperationControl_isActiv
 * returns false
 *
 * @return true -> active, false->sleep or try sleep
 */
bool OpControl_isActiv(void);


#endif /* SERVAL_ENABLE_DUTY_CYCLING */

#endif /* SERVAL_OPCONTROL_H_ */
