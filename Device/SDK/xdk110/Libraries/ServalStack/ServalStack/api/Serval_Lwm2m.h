/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Lwm2m.h
 * @brief Interface to experimental lightweight M2M support.
 * @ingroup lwm2mgroup
 *
 * This module provides the interface to the implementation of the lightweight M2M
 * protocol specified by the Open Mobile Alliance for machine to machine communication.
 *
 * @warning
 * Please note that the specification of the lightweight M2M protocol has not been
 * finalized, yet. Therefore, the current implementation should be treated as a
 * technology preview and it is subject to change as required to meet the specification.
 * This may include changes to this API!
 *
 * @since 1.4
 */

#ifndef SERVAL_LWM2M_H_
#define SERVAL_LWM2M_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_LWM2M

#include <Serval_Types.h>
#include <Serval_Ip.h>
#include <Serval_Msg.h>
#include <Serval_Coap.h>
#include <Serval_Security.h>

#if ! SERVAL_ENABLE_COAP_OBSERVE
#error "LWM2M requires SERVAL_ENABLE_COAP_OBSERVE"
#endif
#include <Serval_CoapServer.h>

/** Maximum length of the name that the device registers with */
#ifndef LWM2M_MAX_LENGTH_DEVICE_NAME
#define LWM2M_MAX_LENGTH_DEVICE_NAME 20
#endif

/** Maximum length of the address of a (bootstrap) server (coap(s)://xxx.xxx.xxx.xxx:xxxxx) */
#ifndef LWM2M_IP_ADDRESS_MAX_LENGTH
#define LWM2M_IP_ADDRESS_MAX_LENGTH 30
#endif

/** Maximum number of servers supported by LwM2M */
#ifndef LWM2M_MAX_NUMBER_SERVERS
#define LWM2M_MAX_NUMBER_SERVERS 4
#endif

/** Enable to reject any server that does not provide a DTLS Server hint.
 *  Only applicable with DTLS enabled and when using LWm2mSecurity_callback as the security callback */
#ifndef LWM2M_REJECT_ANONYMOUS_DTLS_SERVERS
#define LWM2M_REJECT_ANONYMOUS_DTLS_SERVERS 0
#endif

/** Used to define an object instance as inactive / not allocated */
#define LWM2M_INACTIVE_INSTANCE  0xFFFE

/** Used to define an object instance as invisible (it won't be considered for discovery)
 *  This is mainly useful for testing
 */
#define LWM2M_INVISIBLE_INSTANCE 0xFFFF

/** Macro to point out the semantic difference between a non-multiple object and the instance 0 */
#define LWM2M_SINGLE_INSTANCE 0

/** Used to define a resource as single */
#define LWM2M_SINGLE_RESOURCE_INSTANCE 0xFFFF

/** Used to define a non instanced multiple resource */
#define LWM2M_INACTIVE_RESOURCE_INSTANCE 0xFFFE

/** Helper macro to set the count of objects in the Lwm2m device */
#define LWM2M_OBJECT_INSTANCE_COUNT(objectInstances) sizeof(objectInstances) / sizeof(Lwm2mObjectInstance_T)

/** Helper macro to set the count of resources in an Lwm2m object */
#define LWM2M_RESOURCES(resources)  &resources, sizeof(resources)/sizeof(Lwm2mResource_T)

/** Helper macro to initialize a string resource */
#define LWM2M_STRING_RO(string)   {.s = string},      LWM2M_RESOURCE_STRING | LWM2M_READ_ONLY
/** Helper macro to initialize an integer resource */
#define LWM2M_INTEGER(integer)    {.i = integer},     LWM2M_RESOURCE_INTEGER
/** Helper macro to initialize a floating point resource */
#define LWM2M_FLOAT(floating)     {.f = floating},    LWM2M_RESOURCE_FLOAT
/** Helper macro to initialize a bool resource */
#define LWM2M_BOOL(boolean)       {.b = boolean},     LWM2M_RESOURCE_BOOL
/** Helper macro to initialize a time resource */
#define LWM2M_TIME(time)          {.i = time},        LWM2M_RESOURCE_TIME
/** Helper macro to initialize a dynamic resource */
#define LWM2M_DYNAMIC(dyn)        {.dynamic = dyn},   LWM2M_RESOURCE_DYNAMIC
/** Helper macro to initialize a dynamic resource as an array */
#define LWM2M_DYNAMIC_ARRAY(dyn)  {.dynamic = dyn},   LWM2M_RESOURCE_DYNAMIC_ARRAY
/** Helper macro to initialize a function resource */
#define LWM2M_FUNCTION(function)  {.func = function}, LWM2M_RESOURCE_FUNCTION | LWM2M_NOT_WRITABLE | LWM2M_NOT_READABLE

/**
 * QUEUE_MODE_TIMEOUT in seconds (default value 2)
 */
#define LWM2M_QUEUE_MODE_TIMEOUT   COAP_ACK_TIMEOUT

/**
 * BOOTSTRAP_TIMEOUT in seconds (default value 2)
 */
#define LWM2M_BOOTSTRAP_TIMEOUT   COAP_ACK_TIMEOUT

/** macro used to determine the owner of the access control object */
#define LWM2M_ACCESS_CONTROL_OWNER 0x3F

/** Macros used to set the access right for servers.
 *  Read access means right for Read, Observe, Write Attributes and Discover*/
#define LWM2M_READ_ALLOWED      0x01
#define LWM2M_WRITE_ALLOWED     0x02
#define LWM2M_EXECUTE_ALLOWED   0x04
#define LWM2M_DELETE_ALLOWED    0x08
#define LWM2M_CREATE_ALLOWED    0x10
#define LWM2M_FULL_ACCESS       0x1F

/**
 * Enumeration of different communication modes available to the lwm2m device
 * See section 5.2.1.1 "Behavior with Current Transport Binding and Mode" of the
 * Lightweight M2M technical specification.
 */
enum Lwm2m_Binding_E
{
	/* Initial state indicating that no binding has been expressed */
	LWM2M_BINDING_UNDEFINED = 0x00,
	/* bit to indicate that the connection is queued */
	LWM2M_BINDING_QUEUED    = 0x04,

	/** The device is reachable via UDP (default) */
	UDP                 = 0x01,
	/** The device is reachable via SMS */
	SMS                 = 0x02,
	/** The device is reachable via UDP only after having sent an update to the server */
	UDP_QUEUED          = UDP | LWM2M_BINDING_QUEUED,
	/** The device is reachable via SMS only after having sent an update to the server */
	SMS_QUEUED          = SMS | LWM2M_BINDING_QUEUED,
	/** The device is reachable via either UDP or SMS */
	UDP_AND_SMS         = UDP | SMS,
	/** The device is reachable via either UDP after having sent an update or via SMS */
	UDP_QUEUED_AND_SMS  = UDP | SMS | LWM2M_BINDING_QUEUED,
};

/**
 * Type definition for the lightweight M2M communication binding. @see Lwm2m_Binding_E
 */
typedef enum Lwm2m_Binding_E Lwm2m_Binding_T;

/**
 * Enumeration of resource types. Note that read-only refers to the access from the network,
 * not internal access to the resources.
 *
 * The affixes LWM2M_READ_ONLY and LWM2M_WRITE_ONLY can be combined with the helper macros
 * for resource initialization, like this:
 *
 * \code{.c}
 *     { 6,  LWM2M_INTEGER( 1 )   | LWM2M_WRITE_ONLY},
 * \endcode
 *
 * If this is compiled with a C++ compiler, an error message such as this
 * "invalid conversion from 'int' to 'Lwm2m_Resource_Type_T {aka Lwm2m_Resource_Type_E}' [-fpermissive]"
 * might be encountered.
 *
 * In this case it is required to teach C++ about the conversion by overloading the | operator:
 *
 * \code{.cpp}
 *     Lwm2m_Resource_Type_T operator|(Lwm2m_Resource_Type_T lhs, Lwm2m_Resource_Type_T rhs)
 *     {
 *         return (Lwm2m_Resource_Type_T) ((int) lhs | (int) rhs);
 *     }
 * \endcode
 */
enum Lwm2m_Resource_Type_E
{
	/** Resource is a (read-only) String. Use 'dynamic' for strings writable by the server */
	LWM2M_RESOURCE_STRING        = 0x01,
	/** Resource is an Integer */
	LWM2M_RESOURCE_INTEGER       = 0x02,
	/** Resource is a floating point number */
	LWM2M_RESOURCE_FLOAT         = 0x03,
	/** Resource is a boolean */
	LWM2M_RESOURCE_BOOL          = 0x04,
	/** Resource is a time value */
	LWM2M_RESOURCE_TIME          = 0x05,
	/** Resource is dynamic, i.e. serialized with a callback */
	LWM2M_RESOURCE_DYNAMIC       = 0x06,
	/** Resource is dynamic array, i.e. serialized with a callback */
	LWM2M_RESOURCE_DYNAMIC_ARRAY = 0x07,
	/** Resource is a function that can be executed */
	LWM2M_RESOURCE_FUNCTION      = 0x08,

	/* Resource Type affixes, used as a flag  */
	/** Mark the resource type as not writable */
	LWM2M_NOT_WRITABLE           = 0x80,
	/** Mark the resource type as not readable */
	LWM2M_NOT_READABLE           = 0x40,
	/** Mark the resource to be delivered via confirmable messages */
	LWM2M_CONFIRM_NOTIFY         = 0x20,

	/** Convenience value, same as not writable */
	LWM2M_READ_ONLY              = LWM2M_NOT_WRITABLE,
	/** Convenience value, same as not readable */
	LWM2M_WRITE_ONLY             = LWM2M_NOT_READABLE,

	/** Mask for the bits that contain the raw type */
	LWM2M_RESOURCE_TYPE_MASK     = 0x0F,
	/** Mask for the bits that contain affixes */
	LWM2M_RESOURCE_AFFIX_MASK    = 0xE0,
};


/**
 * Type definition for the resource type. @see Lwm2m_Resource_Type_E
 */
typedef enum Lwm2m_Resource_Type_E Lwm2m_Resource_Type_T;

/**
 * Enumeration of event types that the Lwm2m could notify the application about.
 */
enum Lwm2m_Event_Type_E
{
	/** Bootstrapping */
	LWM2M_EVENT_TYPE_BOOTSTRAP,
	/** Registration */
	LWM2M_EVENT_TYPE_REGISTRATION,
	/** Registration update */
	LWM2M_EVENT_TYPE_REGISTRATION_UPDATE,
	/** Deregistration */
	LWM2M_EVENT_TYPE_DEREGISTRATION,
	/** Write operation executed */
	LWM2M_EVENT_TYPE_WRITE,
	/** Create operation executed */
	LWM2M_EVENT_TYPE_OBJECT_CREATED,
	/** Delete operation executed */
	LWM2M_EVENT_TYPE_OBJECT_DELETED,
	/** Observe operation executed */
	LWM2M_EVENT_TYPE_NEW_OBSERVER,
	/** Notification operation executed */
	LWM2M_EVENT_TYPE_NOTIFICATION,
	/** Cancel observation executed */
	LWM2M_EVENT_TYPE_OBSERVATION_CANCELED,
	/** Cancel observation executed */
	LWM2M_EVENT_TYPE_NEW_SERVER_ADDED
};

/**
 * Type definition for the event type. @see Lwm2m_Event_Type_E
 */
typedef enum Lwm2m_Event_Type_E Lwm2m_Event_Type_T;

/**
 * Structure contains the uri path of the target (can either be an object (with
 * all its instances), an object instance or a resource).
 * If the target is an object, then objectInstanceIndex and resourceIndex
 * should be set to -1 and the firstObjectInstanceIndex is the index of
 * the first instance of that object in the object instance array
 * If the target is an object instance, then resourceIndex should be set to -1.
 */
struct Lwm2m_URI_Path_S
{
	/** index to the first instance of the object */
	int32_t firstObjectInstanceIndex;
	/** object instance index */
	int32_t objectInstanceIndex;
	/** resource index */
	int32_t resourceIndex;
};
/**
 * Type definition for the uri path. @see Lwm2m_URI_Path_S
 */
typedef struct Lwm2m_URI_Path_S Lwm2m_URI_Path_T;

/**
 * Opaque type to use for message serialization.
 * Used only to pass into the serialization API
 */
typedef struct Lwm2mSerializer_S Lwm2mSerializer_T;

/**
 * Opaque type to use for message parsing.
 * Used only to pass into the parsing API
 */
typedef struct Lwm2mParser_S Lwm2mParser_T;

/*
 * Callback function that is used for dynamic resource function calls (resources of type DYNAMIC)
 * @see Lwm2m_Resource_Type_E
 *
 * This function is used for both reading and writing the resource. In case that the resource
 * is queried, serializer_ptr will be provided and parser_ptr will be NULL. In case that the resource
 * is to be written or updated, serializer_ptr will be NULL and parser_ptr will be provided.
 *
 * @param[in] serializer_ptr Pointer to the serializer that must be used to serialize the response
 * @param[in] parser_ptr Pointer to the parser that can be used to parse an incoming payload
 *
 * @return The function is expected to return the return value of the serializer API,
 *         RC_LWM2M_METHOD_NOT_ALLOWED if the resource is read-only but is to be written
 *         RC_LWM2M_INTERNAL_ERROR if a generic error occurred
 */
typedef retcode_t (*Lwm2m_Dynamic_Resource_T)(Lwm2mSerializer_T* serializer_ptr, Lwm2mParser_T* parser_ptr);

/*
 * Callback function that is used for function calls (resources of type FUNCTION)
 * @see Lwm2m_Resource_Type_E
 *
 * For internal reasons, the payload_ptr available in the message will point to a memory area that is likely to
 * be also accessed by msg_ptr, since the reply to a message reuses the same internal message structure.
 * Thus care has to be taken to parse the payload first before serializing the response.
 *
 * @param[in] msg_ptr Pointer to the message structure used for the response
 * @param[in] serializer_ptr Pointer to the serializer that must be used to serialize the response
 *
 * @return The function is expected to return the return value of the serializer API or RC_LWM2M_INTERNAL_ERROR
 */
typedef retcode_t (*Lwm2m_Resource_Function_T)(Lwm2mSerializer_T* serializer_ptr, Lwm2mParser_T* parser_ptr);

/*
 * Structure that contains information about an individual resource
 */
struct Lwm2mResource_S
{
	/** The resource identifier as specified in the object definition */
	uint16_t resourceId;
	/** The value of the resource (essentially a variant) */
	union {
		/** String data */
		char *s;
		/** integer data */
		int32_t i;
		/** float data */
		float f;
		/** bool data */
		bool b;
		/** callback for a dynamic resource */
		Lwm2m_Dynamic_Resource_T dynamic;
		/** callback for a function */
		Lwm2m_Resource_Function_T func;
	} data;
	/** The type of the resource */
	Lwm2m_Resource_Type_T type;
};
typedef struct Lwm2mResource_S Lwm2mResource_T;

/**
 * Structure to hold a single object instance of an object for the lightweight M2M protocol
 */
struct Lwm2mObjectInstance_S
{
	/** The object identifier */
	uint16_t objectId;
	/** The instance id of the instance */
	uint16_t objectInstanceId;
	/** Pointer to the resources associated with this instance */
	void* resources;
	/** The max number of resources for this object */
	uint16_t maxNumberOfResources;
	/** Array of permission sorted by server (first permission refers to the first server
	 * in the server array)
	 * This will be used to synthetise the ACL objects */
	uint8_t permissions[LWM2M_MAX_NUMBER_SERVERS];
};

/**
 * Type definition for the lightweight M2M object. @see Lwm2mObject_S
 */
typedef struct Lwm2mObjectInstance_S Lwm2mObjectInstance_T;

#if SERVAL_ENABLE_DTLS

/** Macro used to set a maximum for the security informations */
#define SECURITY_INFO_MAX_LENGTH 30

/**
 * Structure holding the security information of a server.
 */
struct Lwm2mSecurityInfo_S
{
	char peer_identity[SECURITY_INFO_MAX_LENGTH];
	char my_identity[SECURITY_INFO_MAX_LENGTH];
	char secret_key[SECURITY_INFO_MAX_LENGTH];
};

typedef struct Lwm2mSecurityInfo_S Lwm2mSecurityInfo_T;

#endif

/**
 * Structure holding the minimal information to connect to a bootstrap server.
 * This also contains the mandatory resource to synthetize the bootstrap security
 * object
 */
struct Lwm2mBootstrapServer_S
{
	/** This represents the address of the bootstrap server.
	 *  The format should be the following: "coap(s)://ipAddress:port */
	char serverAddress[LWM2M_IP_ADDRESS_MAX_LENGTH];
#if SERVAL_ENABLE_DTLS
	/** This are the security information shared between the device and
	 *  the bootstrap server*/
	Lwm2mSecurityInfo_T securityInfo;
#endif
	/** boolean use to differentiate between server and client initiated bootstrap */
	bool serverInitiated;
};

typedef struct Lwm2mBootstrapServer_S Lwm2mBootstrapServer_T;

/**
 * Structure holding the minimal information to connect to a bootstrap server.
 * This also contains the mandatory resource to synthetize the bootstrap security
 * object
 */
struct Lwm2mServer_S
{
	/** This represents the address of the server.
	 *  The format should be the following: "coap(s)://ipAddress:port */
	char serverAddress[LWM2M_IP_ADDRESS_MAX_LENGTH];
#if SERVAL_ENABLE_DTLS
	/** This are the security information shared between the device and
	 *  the server */
	Lwm2mSecurityInfo_T securityInfo;
#endif
	/** This is the lifetime of registration in seconds */
	uint32_t lifetime;
	/** Array of permission sorted by server (first permission refers
	 * to the first server in the server array). This is used to determine
	 * the access right to this server's server object
	 * This will be used to synthetise the ACL objects */
	uint8_t permissions[LWM2M_MAX_NUMBER_SERVERS];
};

typedef struct Lwm2mServer_S Lwm2mServer_T;

/**
 * Definition of the general properties of a lightweight M2M device.
 * Note that the objects available are expected to be sorted by objectId!
 */
struct Lwm2mDevice_S
{
	/** Name of the device used during registration */
	const char* name;
	/** Telephone number of the device used during communication via SMS (NULL for not available) */
	const char* sms;
	/** The communication binding used by the device */
	Lwm2m_Binding_T binding:3;
	/** Indicate if UDP communication is secured via DTLS. Set to true to use DTLS */
	bool secure:1;
	/** padding */
	uint8_t unused:4;
	/** The number of object instances contained in the device */
	uint16_t numberOfObjectInstances;
	/** The list of object instance of the device.
	 *  This list will only contain application specific objects and the device object */
	 Lwm2mObjectInstance_T* objectInstances;
};

/**
 * Type definition for the lightweight M2M device. @see Lwm2mDevice_S
 */
typedef struct Lwm2mDevice_S Lwm2mDevice_T;

/**
 * @brief Callback function triggered after an event occurred in the stack that need to be notified to the application.
 * This callback function is used for registration for example or resources changes.
 *
 * @param[in] event type to specify which event we are notifying the application about
 *            (registration success or resources changed)
 * @param[in] structure containing the uri path of the target
 * @param[in] status will tell the application about the kind of error that occurred
 */
typedef void (*Lwm2m_ApplicationCallback_T) (Lwm2m_Event_Type_T eventType, Lwm2m_URI_Path_T* path, retcode_t status);

/**
 * @brief Serializes a string to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] strDescr a descriptor for the string to be serialized
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeString(Lwm2mSerializer_T* serializer_ptr, StringDescr_T *strDescr);

/**
 * @brief Serializes an integer to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] value of the integer to be serialized
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeInt(Lwm2mSerializer_T* serializer_ptr, int32_t value);

/**
 * @brief Serializes an integer to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] value the float to be serialized
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeFloat(Lwm2mSerializer_T* serializer_ptr, float value);

/**
 * @brief Serializes a boolean to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] value the bool to be serialized
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeBool(Lwm2mSerializer_T* serializer_ptr, bool value);

/**
 * @brief Serializes an opaque data pointer to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary.
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] value the data to be serialized
 * @param[in] length the length of the value
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeOpaque(Lwm2mSerializer_T* serializer_ptr, uint8_t *value, uint32_t length);

/**
 * @brief Serializes a timestamp (integer) to an outgoing lwm2m message.
 * The function takes care to handle TLV encoding if necessary
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 * @param[in] value the time value to be serialized
 *
 * @return RC_OK on success
 *         RC_LWM2M_ENTITY_TOO_LARGE if the length is to large to be serialized
 *         RC_LWM2M_SERIALIZATION_ERROR if the data could not be serialized
 */
retcode_t Lwm2mSerializer_serializeTime(Lwm2mSerializer_T* serializer_ptr, uint32_t value);

/**
 * @brief This is used to start the serialization of a resource array.
 * This function prepares the serializer_ptr to serialize resource arrays.
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_SERIALIZATION_ERROR if this is used without tlv enabled
 */
retcode_t Lwm2mSerializer_startSerializingResourceArray(Lwm2mSerializer_T *serializer_ptr);

/**
 * @brief This is used to set the resource instance Id before serializing the corresponding resource instance.
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_SERIALIZATION_ERROR if this is called without calling first @Lwm2mSerializer_startSerializingResourceArray
 */
retcode_t Lwm2mSerializer_setResourceInstanceId(Lwm2mSerializer_T *serializer_ptr, uint16_t instanceId);

/**
 * @brief This is used to end the parsing of a resource array.
 * The function takes care of ending the array by writing the length of the array.
 *
 * @param[in] serializer_ptr Pointer to the serializer structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_SERIALIZATION_ERROR if this is called without calling first @Lwm2mSerializer_startSerializingResourceArray
 */
retcode_t Lwm2mSerializer_endSerializingResourceArray(Lwm2mSerializer_T *serializer_ptr);

/**
 * @brief Get the server index from which the LWM2M request came from.
 *
 * @param[in] serializer_ptr - Pointer to the serializer that is used to extract the server details
 *
 * @return server index      - The index of the server(0 to LWM2M_MAX_NUMBER_SERVERS -1, as it is stored in an array)
 *                             SERVER_INDEX_NOT_FOUND - if the server index could not be found
 */
uint8_t Lwm2mSerializer_getServerIndex(Lwm2mSerializer_T* serializer_ptr);

/**
 * @brief Parses a string from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * Note that the string is not guaranteed to be terminated by '\0'. In case of TLV encoded
 * payload, it is followed by the remaining payload of the message.
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] strDescriptor contains the the string that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getString(Lwm2mParser_T *parser_ptr, StringDescr_T *strDescr);

/**
 * @brief Parses an integer from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] value of the integer that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getInt(Lwm2mParser_T* parser_ptr, int32_t *value);

/**
 * @brief Parses a float from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] value of the float that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getFloat(Lwm2mParser_T* parser_ptr, float *value);

/**
 * @brief Parses a bool from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] value of the bool that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getBool(Lwm2mParser_T* parser_ptr, bool *value);

/**
 * @brief Parses opaque data from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] value the data that was parsed
 * @param[out] length the length of the data that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getOpaque(Lwm2mParser_T* parser_ptr, const uint8_t **value, uint32_t *length);

/**
 * @brief Parses a timestamp from an incoming lwm2m message.
 * The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 * @param[out] value of the timestamp that was parsed
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
retcode_t Lwm2mParser_getTime(Lwm2mParser_T* parser_ptr, int32_t *value);

/**
 * @brief This is used to start the parsing of a resource array.
 * This function prepares the parser_ptr to parse resource arrays.
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data is not a resource array
 */
retcode_t Lwm2mParser_startParsingResourceArray(Lwm2mParser_T *parser_ptr);

/**
 * Function to get the next resource instance in the tlv encoded payload
 */
/**
 * @brief This is used to parse the next resource instance.
 * This function get the next resource instance in the tlv encoded payload to be
 * able to parsing using Lwm2mParser_get functions.
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_PARSER_END if we reached the end of the resource array
 */
retcode_t Lwm2mParser_parseNextResourceInstance(Lwm2mParser_T *parser_ptr);

/**
 * @brief This is used to get the resource instance Id from the Tlv header
 * before parsing the corresponding resource instance.
 *
 * @param[in] parser_ptr Pointer to the parser structure (typically obtained in the callback)
 *
 * @return RC_OK on success
 *         RC_LWM2M_SERIALIZATION_ERROR if the next element is not a resource instance
 */
retcode_t Lwm2mParser_getResourceInstanceId(Lwm2mParser_T *parser_ptr, uint16_t* id);

/**
 * @brief Converts a parsed string to an integer.
 * Use this function when obtaining string data from the parser that you want to convert to at
 * integer value.
 *
 * It rejects white space or any other non-digit characters.
 *
 * The key difference to existing standard functions is that the string provided by
 * the parser is not terminated by '\0'! In memory, the string data is followed by the remaining
 * payload in order to prevent copying the string data. Thus, the following TLV header may resemble
 * an ASCII digit (e.g. object instance with 16-bit ID and 16-bit length will encode to 0x30 == '0')
 *
 * @param[in] string the string data obtained from Lwm2mParser_getString()
 * @param[in] length the length of the string data obtained from Lwm2mParser_getString()
 * @param[out] value the converted string value
 * @return RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the string could not be converted
 */
retcode_t Lwm2mParser_convertStringToInt(const char *string, const uint32_t length, int32_t *value);

/**
 * @brief Request Bootstrap to a bootstrap server for the LwM2M device.
 *
 * Note: This is preliminary API
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mBootstrap_request(void);

/**
 * @brief Register a LwM2M device with an LwM2M server.
 *
 * Note: This is preliminary API
 *
 * @param[in] serverIndex the index of the server(in the array of server)
 * to register to
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_register(uint8_t serverIndex);

/**
 * @brief Register a LwM2M device with all the LwM2M servers.
 *
 * Note: This is preliminary API
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_registerToAllServers(void);

/**
 * @brief Perform a registration update of an LwM2M device with an LwM2M server.
 *
 * A successful registration with Lwm2m_register MUST have been performed.
 *
 * Note: This is preliminary API
 *
 * @param[in] serverIndex the index of the server(in the array of server)
 * to update to
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_update(uint8_t serverIndex);

/**
 * @brief Perform a registration update of an LwM2M device with all the LwM2M servers.
 *
 * A successful registration with Lwm2m_register MUST have been performed.
 *
 * Note: This is preliminary API
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_updateToAllServers(void);

/**
 * @brief Perform a deregistration of an LwM2M device with an LwM2M server.
 *
 * A successful registration with Lwm2m_register MUST have been performed.
 *
 * Note: This is preliminary API
 *
 * @param[in] serverIndex the index of the server(in the array of server)
 * to deregister from
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_deRegister(uint8_t serverIndex);

/**
 * @brief Perform a deregistration of an LwM2M device with all the LwM2M servers.
 *
 * A successful registration with Lwm2m_register MUST have been performed.
 *
 * Note: This is preliminary API
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mRegistration_deRegisterFromAllServers(void);

#if SERVAL_ENABLE_DTLS_CLIENT
/**
 * @brief close and disconnect an existing secure COAP connection.
 *
 * Note: This is preliminary API
 *
 * @param[in] serverIndex the index of the server(in the array of server)
 * to close the secure connection
 *
 * @return RC_OK on success (or if connection is not secure), <br>
 * RC_COAP_SECURE_CONNECTION_ERROR on failure.
 */
retcode_t Lwm2mRegistration_closeSecureConn(uint8_t serverIndex);
#endif

/**
 * @brief Notify the stack that a resource has changed.
 * This will cause the stack to notify any pending observers about the changed resource.
 *
 * @param[in] uripath The uripath of the resource that has changed
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mReporting_resourceChanged(Lwm2m_URI_Path_T *uripath);

/**
 * @brief Notify the stack that a resource has changed.
 * This will cause the stack to notify any pending observers about the changed resource.
 *
 * @param[in] uripath The uripath of the object instance that has changed
 * @param[in] count The number of resources of the object instance that have changed
 * @param[in] resourceIndex The indexes of the resources that have changed
 *
 * @return RC_OK on success
 */
retcode_t Lwm2mReporting_multipleResourcesChanged(Lwm2m_URI_Path_T *objectInstanceUripath, uint8_t count, ...);

/**
 * @brief Suspend sending of notifications to the given server
 *
 * Any notification resulting Lwm2mReporting_resourceChanged or
 * Lwm2mReporting_multipleResourcesChanged will not be sent to the
 * given server. The notification will silently be dropped.
 *
 * @param[in] serverIndex the index of the server in the array of servers
 *
 * @return RC_OK on success
 */
void Lwm2mReporting_suspendNotifications(uint8_t serverIndex);

enum Lwm2m_Notification_Resumption_e
{
	LWM2M_DROP_NOTIFICATIONS,
	LWM2M_SEND_NOTIFICATIONS
};
typedef enum Lwm2m_Notification_Resumption_e Lwm2m_Notification_Resumption_t;

/**
 * @brief Resume sending notifications to a server
 *
 * Resume transmitting notifications resulting from Lwm2mReporting_resourceChanged
 * or Lwm2mReporting_multipleResourcesChanged. This function is only required,
 * if the server has been suspended before by a call to Lwm2mReporting_suspendNotifications.
 *
 * @param[in] serverIndex the index of the server in the array of servers
 * @param[in] mode the way that changed observations are handled
 *                 LWM2M_DROP_NOTIFICATIONS will cause changed observations to be silently ignored.
 *                                          Only future calls to Lwm2m_resourceChanged will trigger notifications.
 *                 LWM2M_SEND_NOTIFICATIONS will cause a single notification to be triggered for changed resources.
 *                                          Thus, the observer will be notified about the _new_ state of the resource,
 *                                          but not necessarily of _all_ state changes.
 *
 * @return RC_OK on success
 */
void Lwm2mReporting_resumeNotifications(uint8_t serverIndex, Lwm2m_Notification_Resumption_t mode);

/**
 * @brief Gets a reference to the bootstrap server.
 * The application can then set the bootstrap informations.
 *
 * @return Lwm2mBootstrapServer: pointer to the LwM2M bootstrap server.
 */
Lwm2mBootstrapServer_T* Lwm2mBootstrap_getBootstrapServer(void);

/**
 * @brief Get a reference to a particular server.
 * The application can then set and access to the server informations.
 *
 * @param[in] serverIndex the index of the server in the array of servers
 *
 * @return Lwm2mServer: pointer to the LwM2M server of serverIndex-
 */
Lwm2mServer_T* Lwm2m_getServer(uint8_t serverIndex);

/**
 * @brief Sets the number of instanciated servers.
 * This function should be called after the application set servers
 * informations to tell the stack about the number of instanciated servers.
 *
 * @param[in] numberOfServers number of active servers (this MUST be less
 *                            than maximum number of servers)
 */
void Lwm2m_setNumberOfServers(uint8_t numberOfServers);

/**
 * @brief Initialize the Lightweight M2M module for a device described by device.
 *
 * @param[in] device structure containing the information about the device
 * @param[in] bootstrapServer structure containing information about the bootstrap server
 *            (in case of factory bootstrap the bootstrapServer argument must be NULL)
 * @param[in] servers structure containing information about the servers
 *            (in case of client initiate bootstrap the servers should be NULL)
 *
 * Initializes the LwM2M module. Device is a pointer to an initialized structure which contains
 * the relevant information about the device.
 */
retcode_t Lwm2m_initialize(Lwm2mDevice_T* device);

/**
 * @brief Start the Lightweight M2M module
 *
 * @param[in] port The coap server port, i.e. the local port used by this device
 * @param[in] applicationCallback, a callback used to tell the application about events (like write, registration, bootstrapping)
 *
 * Starts a CoAP server on the given port.
 */
retcode_t Lwm2m_start(Ip_Port_T port, Lwm2m_ApplicationCallback_T applicationCallback);

#if SERVAL_ENABLE_DTLS
#if SERVAL_SECURITY_API_VERSION == 2
/**
 * @brief Callback that can be provided to Security_setCallback to take over security token handling.
 *
 * Lwm2m provides a notion of servers and the associated keying information for these servers.
 * If the application has no special needs, this function can be provided to Security_setCallback
 * in order to let the Lwm2m module perform security token management.
 *
 * @param[in]      token
 *                 the security token to be provided
 *
 * @param[in/out]  tokenData
 *                 additional data about the token to be provided and memory to provide it
 *
 * return RC_OK on success, see SecurityCallback_T for a description of return codes.
 */
retcode_t Lwm2mSecurity_defaultCallback(SecurityToken_T token, SecurityData_T* tokenData);

#else /* SERVAL_SECURITY_API_VERSION == 2 */

/**
 * @brief Use to get the keying information.
 *
 * @param[in]  ipAddr_ptr ip address of the server
 * @param[in]  port port of the server
 * @param[in]  peer_identity identity of the server
 * @param[out] my_identity identity of the client (a.k.a. me)
 * @param[out] key key for the connection
 *
 * @deprecated
 * The Security API introduced in 1.7 supersedes this API.
 * It will be removed in 1.8
 */
uint32_t Lwm2mSecurity_PskCallback(Ip_Address_T const *ipAddr_ptr, Ip_Port_T const port,
                                   const char* peer_identity,
                                   char* my_identiy, unsigned char* key);
#endif /* SERVAL_SECURITY_API_VERSION == 2 */
#endif //SERVAL_ENABLE_DTLS

#endif //SERVAL_ENABLE_LWM2M
#endif //SERVAL_LWM2M_H_
