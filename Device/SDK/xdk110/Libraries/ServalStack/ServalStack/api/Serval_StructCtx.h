/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_StructCtx.h
 * @brief Struct context module
 * @ingroup utilgroup
 *
 * This header provides the macro GET_CONTEXT_OF_MEMBER which calculates the
 * address of a struct from a given struct member.
 */

#ifndef SERVAL_STRUCTCTX_H_
#define SERVAL_STRUCTCTX_H_

/**
 * This macro computes the address of the context struct of a given struct
 * member. It can be used as in the following example.
 * <code>
 * typedef struct Context_S
 * {
 *   int member;
 * } Context_T;
 *
 * Context_T context;
 * int *p = &context.member;
 *
 * Context_T *ctx_ptr = GET_CONTEXT_OF_MEMBER(Context_T, member, p);
 * </code>
 * In this example it holds that ctx_ptr is equal &context.
 *
 * @param[in] ContextTypeName
 * the name of the expected data type of the containing structure
 *
 * @param[in] ContextMemberName
 * the member name of the callable object inside the enclosing structure
 *
 * @param[in] MemberPtr
 * a pointer to the struct member
 *
 * @return a pointer to the enclosing structure.
 */
#define GET_CONTEXT_OF_MEMBER(\
   ContextTypeName, ContextMemberName, MemberPtr) \
   ((ContextTypeName *)((char *)MemberPtr - \
      (intptr_t)(&((ContextTypeName *)0)->ContextMemberName)))

#endif /* SERVAL_STRUCTCTX_H_ */
