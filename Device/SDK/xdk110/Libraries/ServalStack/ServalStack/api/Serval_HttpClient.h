/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_HttpClient.h
 * @brief Interface for HTTP client
 * @ingroup httpgroup
 *
 * This interface provides a function to send HTTP POST requests.
 */

#ifndef SERVAL_HTTPCLIENT_H_
#define SERVAL_HTTPCLIENT_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_HTTP_CLIENT
#include <Serval_Msg.h>
#include <Serval_Http.h>
#include <Serval_Ip.h>
#include <Serval_Callable.h>

/**
 * A data type representing a callback function pointer for HTTP Client
 * application. The application uses such callback functions in order to be
 * notified about the received messages.
 */
typedef retcode_t (*HttpAppRespCallback_T)(HttpSession_T *httpSession_ptr,
        Msg_T *msg_ptr, retcode_t status);

/**
 * Initializes the HTTP client including all needed used modules.
 *
 * @return RC_OK if successful<br>
 * RC_HTTP_CLIENT_INIT_ERROR if not successful
 */
retcode_t HttpClient_initialize(void);


/**
 * This function is called to initiate a request from HTTP client. This function
 * provides the message to be filled for sending the request.
 *
 * @param[in] addr_ptr
 * The destination IP address.
 *
 * @param[in] port
 * The destination port number.
 *
 * @param[out] msg_pptr
 * This holds the reference to message to be filled by application for request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_HTTP_CLIENT_PENDING_CONNECTION, if there is a request to the same
 * destination already pending<br>
 * RC_HTTP_CLIENT_INIT_REQ_FAILED, if it is not able to initiate a new request
 */
retcode_t HttpClient_initRequest(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                 Msg_T **msg_pptr);


/**
 * This function is called to push a request from HTTP client. This function
 * prepares the message for further processing before passing it to the lower
 * layers.
 *
 * @param[in] msg_ptr
 * This is the reference to the Msg_T object which should be sent.
 *
 * @param[in] sentCallback_ptr
 * This is the reference to the callback function which is invoked to inform the
 * application about the status of sending.
 *
 * @param[in] respCallback_ptr
 * This is callback function which is invoked to inform the application of an
 * incoming response.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_HTTP_SEND_ERROR<br>
 * RC_HTTP_CLIENT_CONNECT_ERROR<br>
 */
retcode_t HttpClient_pushRequest(Msg_T *msg_ptr, Callable_T *sentCallback_ptr,
                                 HttpAppRespCallback_T respCallback_ptr);


#if SERVAL_ENABLE_TLS_CLIENT
/**
 * This function is called to initiate a secure request (TLS) from HTTP client.
 * This function provides the message to be filled for sending the request.
 *
 * @param[in] addr_ptr
 * The destination IP address.
 *
 * @param[in] port
 * The destination port number.
 *
 * @param[out] msg_pptr
 * This holds the reference to message to be filled by application for request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_HTTP_CLIENT_PENDING_CONNECTION, if there is a request to the same
 * destination already pending<br>
 * RC_HTTP_CLIENT_INIT_REQ_FAILED, if it is not able to initiate a new request
 */
retcode_t HttpClient_initSecureRequest(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                       Msg_T **msg_pptr);
#endif /* SERVAL_ENABLE_TLS_CLIENT */


#endif /* SERVAL_ENABLE_HTTP_CLIENT */

#endif /* SERVAL_HTTPCLIENT_H_ */
