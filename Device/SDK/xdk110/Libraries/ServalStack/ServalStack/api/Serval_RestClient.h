/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_RestClient.h
 * @brief REST Client Module
 * @ingroup restgroup
 *
 * The interface description of the REST Client.
 */

#ifndef SERVAL_RESTCLIENT_H_
#define SERVAL_RESTCLIENT_H_

#include <Serval_Defines.h>
#if SERVAL_ENABLE_REST_CLIENT

#include <Serval_Rest.h>
#include <Serval_Msg.h>

/** Defines if the secure REST client instance should be enabled. */
#if (SERVAL_ENABLE_REST_COAP_BINDING && SERVAL_ENABLE_DTLS_CLIENT) || \
    (SERVAL_ENABLE_REST_HTTP_BINDING && SERVAL_ENABLE_TLS_CLIENT)
#define SERVAL_ENABLE_REST_SECURE_CLIENT   1
#else
#define SERVAL_ENABLE_REST_SECURE_CLIENT   0
#endif
#if SERVAL_ENABLE_REST_HTTP_BINDING && SERVAL_ENABLE_TLS_CLIENT
//#error HTTP Client with TLS is currently not supported
#endif

#if SERVAL_ENABLE_REST_SECURE_CLIENT
#include <Serval_Security.h>
#endif

/** This enumeration defines the possible protocols of a REST message */
typedef enum
{
	REST_CLIENT_PROTOCOL_COAP = 0,
	REST_CLIENT_PROTOCOL_HTTP,
} RestClient_Protocol_T;

/** This enumeration defines the reliability mode of a REST message. */
typedef enum
{
	REST_CLIENT_MSG_RELIABLE = 0,
	REST_CLIENT_MSG_UNRELIABLE,
} RestClient_MsgReliabilityMode_T;

/**
 * This data structure holds the data of a REST request.
 */
typedef struct RestClient_ReqInfo_S
{
	/** The URI path of the request */
	char const*             uriPath_ptr;
	/** The URI query of the request */
	char const*             uriQuery_ptr;
	/** A pointer to the payload to be set in the request */
	uint8_t*                payload_ptr;
	/** The length of the payload the payload_ptr points to */
#if SERVAL_POLICY_LARGE_PAYLOAD==1
	uint16_t                payloadLen;
#else
	uint8_t					payloadLen;
#endif
	/** The content format of the payload */
	Rest_ContentFormat_T    contentFormat;
	/** The method to be used for the request */
	Rest_Method_T           method;
	/** A pointer to an array of content formats, which are acceptable */
	Rest_ContentFormat_T *  acceptBuffer_ptr;
	/** The length of the array the acceptBuffer_ptr points to */
	uint8_t                 numAccept;
	/** The reliability mode to be used for the request.
	 *  In case of HTTP this is ignored. */
	RestClient_MsgReliabilityMode_T msgReliabilityMode;
} RestClient_ReqInfo_T;


/**
 * A data type representing a callback function pointer for REST application.
 * The application uses such callback functions in order to be notified about
 * the received messages. Also if there was an error during receiving response
 * for request by CoAP client this callback will be invoked with error code and
 * the RestSession_T, which can be used to identify failed request.
 */
typedef retcode_t (*RestAppRespCallback_T)(RestSession_T *restSession,
        Msg_T *msg_ptr, retcode_t status);


/**
 * Initializes the REST client including all needed used modules.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_CLIENT_INIT_ERROR if the initialization of the REST client module
 * fails.
 */
retcode_t RestClient_initialize(void);


/**
 * This function is called to start the default REST client
 *
 * @param[in] enableCoap
 * Defines if the CoAP binding should be enabled for this REST client instance.
 *
 * @param[in] enableHttp
 * Defines if the HTTP binding should be enabled for this REST client instance.
 *
 * @return
 * RC_OK if successful<br>
 * RC_REST_CLIENT_LISTEN_ERROR if the listening for the client fails.
 */
retcode_t RestClient_startInstance(bool enableCoap, bool enableHttp);


/**
 * This function is called to initiate a request from REST client.
 *
 * @param[in] addr_ptr
 * This is the reference to the IP address of the receiver.
 *
 * @param[in] port
 * This is the server port of the receiver.
 *
 * @param[out] msg_pptr
 * This holds message to be filled by application for request.
 *
 * @param[in] protocol
 * This defines which protocol should be used for the request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_REST_CLIENT_INIT_REQ_ERROR, otherwise.
 */
retcode_t RestClient_initReqMsg(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                Msg_T **msg_pptr, RestClient_Protocol_T protocol);


/**
 * Writes the request into the given message using the given request info.
 *
 * @param[in] msg_ptr
 * A reference to the message to be used for the response.
 *
 * @param[in] requestInfo_ptr
 * A reference to the request info to be used for the request
 *
 * @return
 * RC_OK on success <br>
 * RC_REST_CREATE_REQUEST_ERROR if an error occurs creating the request.
 *
 * @see RestClient_ReqInfo_T
 */
retcode_t RestClient_fillReqMsg(Msg_T* msg_ptr,
                                RestClient_ReqInfo_T const *requestInfo_ptr);


/**
 * This function is called to push a request from REST client.
 *
 * @param[in] msg_ptr
 * A reference to the request message to be pushed.
 *
 * @param[in] appSentCallback
 * This is the application callback to be called for informing application about
 * status of sending. If the application is not interested in the delivery
 * notification, then this function may be NULL.
 *
 * @param[in] respCallback
 * This is the application callback to be called for a received response
 * message for this request. If the application does not expect a response for
 * this request it should be set to NULL.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_REST_CLIENT_SEND_ERROR, otherwise.
 */
retcode_t RestClient_request(Msg_T *msg_ptr,
                             AppSentCallback_T appSentCallback, RestAppRespCallback_T respCallback);

#if SERVAL_ENABLE_REST_SECURE_CLIENT
/**
 * This function is called to start the secure CoAP client
 *
 * @param[in] enableCoap
 * Defines if the CoAP binding should be enabled for this REST client instance.
 *
 * @param[in] enableHttp
 * Defines if the HTTP binding should be enabled for this REST client instance.
 *
 * @return, RC_OK, if successful
 * RC_REST_CLIENT_LISTEN_ERROR if the listening for the client fails.
 */
retcode_t RestClient_startSecureInstance(bool enableCoap, bool enableHttp);


/**
 * This function is called to initiate a secure request from REST client.
 *
 * @param[in] addr_ptr
 * This is the reference to the IP address of the receiver.
 *
 * @param[in] port
 * This is the server port of the receiver.
 *
 * @param[out] msg_pptr
 * This holds message to be filled by application for request.
 *
 * @param[in] protocol
 * This defines which protocol should be used for the request.
 *
 * @return
 * RC_OK, if successful<br>
 * RC_REST_CLIENT_INIT_REQ_ERROR, otherwise.
 */
retcode_t RestClient_initSecureReqMsg(Ip_Address_T *addr_ptr, Ip_Port_T port,
                                      Msg_T **msg_pptr, RestClient_Protocol_T protocol);


/**
 * This function connects and opens a secure COAP client connection.
 *
 * @param[in] peerAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] peerPort
 * port number of the remote server socket
 *
 * @param[in] protocol
 * This defines which protocol should be used for the request.
 *
 * @return when error code available
 * On success, RC_OK is returned. <br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestClient_connectSecure(Ip_Address_T *peerAddr_ptr,
                                   Ip_Port_T peerPort, RestClient_Protocol_T protocol);

/**
 * This function close and disconnect an existing secure COAP connection.
 *
 * @param[in] peerAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] peerPort
 * port number of the remote server socket
 *
 * @param[in] protocol
 * This defines which protocol should be used for the request.
 *
 * @return when error code available
 * On success, RC_OK is returned. <br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestClient_closeSecureConn(Ip_Address_T *peerAddr_ptr,
                                     Ip_Port_T peerPort, RestClient_Protocol_T protocol);

/**
 * This function reports the secure connections.
 *
 * @param[in] iterator_ptr
 * Iterator for this function. Initialized to -1 it reports the first
 * connection. The next returned values will report the next connections.
 * if -1 returns, no more connections are assigned to this socket.
 * Example:
 *    int16_t iter = -1;
 *    while(1) {
 *         rc = _getSecureConnection(&iter,...);
 *         if(iter < 0) break;
 *         do_some_thing(...);
 *    }
 *
 *
 * @param[in] onlyWithError
 * true -> report only connections with failures.
 * false -> report all connections
 *
 * @param[out] ipAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[out] port_ptr
 * returned port number of the connection endpoint
 */
void RestClient_iterateSecureConnections(int16_t *iterator_ptr,
        bool onlyWithError,
        Ip_Address_T *ipAddr_ptr, Ip_Port_T *port_ptr);

/**
 * This function reports the errors of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] rc_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestClient_getSecureConnError(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    retcode_t *rc_ptr);

/**
 * This function reports the status of a secure connection
 *
 * @param[in] peerAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] peerPort
 * returned port number of the connection endpoint
 *
 * @param[out] state_ptr
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_REST_SECURE_CONNECTION_ERROR
 */
retcode_t RestClient_getSecureConnState(
    Ip_Address_T *peerAddr_ptr, Ip_Port_T peerPort,
    SecureConnectionState_T *state_ptr);
#endif /* SERVAL_ENABLE_REST_SECURE_CLIENT */


#endif /* SERVAL_ENABLE_REST_CLIENT */
#endif /* SERVAL_RESTCLIENT_H_ */
