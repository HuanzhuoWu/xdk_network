/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Timer.h
 * @brief An interface to the platform timer.
 *
 * This interface provides functions to handle a periodic timer. It provides
 * functions to initialize, configure, start and stop a timer.
 */

#ifndef SERVAL_TIMER_H_
#define SERVAL_TIMER_H_

#include <Serval_Types.h>

/**
 * A data type definition for timers. The platform exports the declaration of
 * the struct Timer_S in the platform header file PBasics.h.
 */
typedef struct Timer_S Timer_T;

/**
 * A data type for timeout functions. A timer is associated with such a function
 * which is invoked every time the timer times out. It is recommended to keep
 * time-out functions as light as possible. If executing a time-out function
 * takes longer than the timer period, some RT problems might occur.
 */
typedef retcode_t (*TimerFunc_T)(Timer_T *timer_ptr);

/**
 * This function initializes the given timer. After invoking this function
 * the timer can be configured using Timer_config(...) and used (e.g. Timer_start()).
 *
 * @param[out] timer_ptr
 * Handle to timer. It has to be a valid pointer.
 *
 * @return
 * RC_OK if function successful,
 * RC_TIMER_INIT if initialization fails.
 *
 * @see Timer_start()
 */
retcode_t Timer_init(Timer_T *timer_ptr);

/**
 * This function checks if the given timer is active. Before invoking this
 * function the timer should be initialized via Timer_init().
 *
 * @param[in] timer_ptr
 * Pointer reference to timer to be initialized.
 *
 * @return
 * TRUE if timer is active, FALSE if timer is inactive
 *
 * @see Timer_init()
 */
bool Timer_isActive(Timer_T *timer_ptr);

/**
 * This function configures the given timer. Before invoking this function the
 * timer must be initialized via Timer_init(). After invoking this function
 * the given function func is called periodically every x milliseconds.
 * If an existing timer is reconfigured, any internal state must be reset.
 *
 * @param[out] timer_ptr
 * Pointer to the timer to be configured. It is configured with the given
 * milliseconds and the given function func. This must be valid and should have
 * a valid handle. The timer must not be active when this function is called.
 *
 * @param[in] milliseconds
 * Time after which timer expires. It is a periodic timer. It must not be zero.
 * The first execution of the timer function is expected to occur after the
 * value of milliseconds after the timer has been started using Timer_start.
 *
 * @param[in] func
 * Handle to a function to be called periodically when the timer times out.
 *
 * @see Timer_init()
 *
 * @return
 * RC_OK if function successful,
 * RC_TIMER_INVALID_OPERATION if timer is already active,
 * RC_TIMER_ERROR_FATAL otherwise.
 */
retcode_t Timer_config(Timer_T *timer_ptr, unsigned int milliseconds,
                       TimerFunc_T func);

/**
 * This function starts the given timer. Before invoking this function the timer
 * has to be initialized via Timer_init() and configured via Timer_config().
 *
 * @param[in, out] timer_ptr
 * Timer handle. It contains the context of the timer including the period
 * (milliseconds) and the timeout function. This must be valid and should have
 * a valid handle.
 *
 * @see Timer_init(), Tier_config()
 *
 * @return
 * RC_OK if function successful,
 * RC_TIMER_INVALID_OPERATION if timer is already active,
 * RC_TIMER_ERROR_FATAL otherwise.
 */
retcode_t Timer_start(Timer_T *timer_ptr);

/**
 * This function stops the given timer. If the timer is not active, i.e.,
 * started using Timer_start(), this function does not have any effect.
 *
 * @param[in, out] timer_ptr
 * A valid pointer to a Timer. After stopping the timer, it has to be
 * re-configured before it can re-started. This must be valid and should have
 * a valid handle.
 *
 * @see Timer_start()
 *
 * @return
 * RC_OK if function successful,
 * RC_TIMER_ERROR_FATAL otherwise.
 */
retcode_t Timer_stop(Timer_T *timer_ptr);


#endif /* SERVAL_TIMER_H_ */
