/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Scheduler.h
 * @brief An interface to the platform scheduler.
 *
 * This module provides the interface to a platform specific scheduling queue.
 * This should not be confused by the main scheduler of the RTOS which manages
 * the concurrent processes. The platform is expected to provide a queue for
 * tasks, which are basically functions waiting for invocation. This mechanism
 * is used to enable the Serval stack to perform multi-tasking without creating
 * multiple threads. Many of the processing steps of the stack are implemented
 * as Callable_T objects, which can be enqueued to be processed once the
 * application gets time for this.
 *
 */

#ifndef SERVAL_SCHEDULER_H_
#define SERVAL_SCHEDULER_H_

#include <Serval_Callable.h>
#include <Serval_Types.h>

/**
 * This function is called to schedule a given task. On success, stack expects
 * that the enqueued callable will be invoked with the given status.
 *
 * @param[in] callable_ptr
 * Reference to a Callable_T object be scheduled. It has to be a valid pointer.
 *
 * @param[in] status
 * The status which will be passed to the callable when it is invoked.
 *
 * @return
 * On success, RC_OK is returned.
 * RC_SCHED_FULL if the scheduling queue is full.
 *
 */
retcode_t Scheduler_enqueue(Callable_T *callable_ptr, retcode_t status);

#endif /* SERVAL_SCHEDULER_H_ */
