/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Thread.h
 * @brief An interface to platform specific thread handling functions which
 * are needed to implement the multi-threading safety of the stack.
 *
 * This header includes the header PThreading.h which must be provided by the
 * PAL implementation. This header also contains pre-processor directives to
 * double-check the right content of PThreading.h.
 *
 * In the following we document the required content of PThreading.h.
 * <p>
 * \#define SERVAL_MUTEX_MODE_ENABLE <br>
 * \#define SERVAL_MUTEX_MODE_DISABLE <br>
 * \#define SERVAL_MUTEX_MODE_TEST <br>
 * An enumeration of the supported mutex modes given as numbers e.g. 0, 1, 2.
 * It is used by the stack along with the macro SERVAL_MUTEX_SET_MODE(mode).
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_SET_MODE(mode) <br>
 *   this macro is for testing only and by this optional
 *   by this, this macro may be implemented as empty
 *   and this macro has only an effect when the code is
 *   compiled in TRACE or DEBUG Mode
 *   <ul>
 *   <li> SERVAL_MUTEX_MODE_ENABLE: enables normal mutex working
 *   </li>
 *   <li> SERVAL_MUTEX_MODE_TEST: before locking an mutex there
 *        is a short wait time, so other treads get the bigger chance
 *        to be dispatched -> this makes the propability for
 *        bad multithreading access higher (error counter)
 *   </li>
 *   <li> SERVAL_MUTEX_MODE_DISABLE: disable mutexes, but detect
 *        bad multi-threading access (error counter)
 *   </li>
 *   </ul>
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_EXTERN(mutex) <br>
 *   declares the datatype of mutex as external.
 *   for use in headers, if mutex is used more as in one module
 *   this macro must be implemented in the PAL
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_INSTANCE(mutex) <br>
 *   declares the datatype
 *   for use in data declaration section of module source file
 *   this macro must be implemented in the PAL
 * </p>
 * \#define SERVAL_MUTEX_INIT() <br>
 *   initalizes the global data of mutex module
 *   this macro must be implemented in the PAL
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_CREATE(mutex) <br>
 *   creates the operating system resources for this mutex
 *   this macro must be implemented in the PAL
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_LOCK(mutex) <br>
 *   locks the mutex
 *   this macro must be implemented in the PAL
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_UNLOCK(mutex) <br>
 *   locks the mutex
 *   this macro must be implemented in the PAL
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_GET_ERROR_COUNT(mutex) <br>
 *   this macro is for testing only and by this optional
 *   by this, this macro may be implemented as empty (returns 0)
 *   it gets the count of bad multithreading access of
 *   this mutex (error counter)
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_GET_SUM_ERROR_COUNT() <br>
 *   this macro is for testing only and by this optional
 *   by this, this macro may be implemented as empty (returns 0)
 *   it gets the count of bad multithreading access of
 *   all mutexes of the stack together (sum error counter)
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_IS_TRACE_LOCK() <br>
 *    this macro is for testing only and by this optional
 *    by this, this macro may be implemented as empty (returns 0)
 *    this macro only valid for Logging Module. Mutex Tracing would cause
 *    deadlock, because logging itself use mutex.
 *    when macro returns 1 means: logging should not use mutex
 * </p>
 * <p>
 * \#define SERVAL_MUTEX_SET_NO_TRACE(mutex,noTrace) <br>
 *    this macro is for testing only and by this optional
 *    by this, this macro may be implemented as empty (returns 0)
 *    no mutex tracing for this specific mutex. It is mostly
 *    for the logging mutex to reduce logging.
 * </p>
 * <p>
 * \#define SERVAL_THREAD_GET_ID() <br>
 *    this macro is for testing only and by this optional
 *    it returns the thread id of calling thread environment
 * </p>
 * <p>
 * \#define SERVAL_SLEEP_MS(ms) <br>
 *    this macro is for testing only and by this optional
 *    it sets the calling thread into sleep for ms milliseconds
 * </p>
 * <p>
 * \#define SERVAL_PREINITIALIZED_DATA <br>
 *    this macro is optional, but must be at least implemented as
 *    returning the value false (0)
 *    it optimizes the initialization handling of the stack
 *    it shall return true (!=0) if the boot loader supports the copy
 *    of preinitialized data into stack.
 *    And false (0) if not.
 * </p>
 */

#ifndef SERVAL_THREAD_H_
#define SERVAL_THREAD_H_

#include <PThreading.h>


#ifndef SERVAL_MUTEX_INIT
#error "Macro SERVAL_MUTEX_INIT not defined."
#endif

#ifndef SERVAL_MUTEX_INSTANCE
#error "Macro MUTEX_INSTANCE not defined."
#endif

#ifndef SERVAL_MUTEX_EXTERN
#error "Macro MUTEX_EXTERN not defined."
#endif

#ifndef SERVAL_MUTEX_CREATE
#error "Macro MUTEX_CREATE not defined."
#endif

#ifndef SERVAL_MUTEX_LOCK
#error "Macro MUTEX_LOCK not defined."
#endif

#ifndef SERVAL_MUTEX_UNLOCK
#error "Macro MUTEX_UNLOCK not defined."
#endif

#ifndef SERVAL_MUTEX_SET_MODE
#error "Macro SERVAL_MUTEX_SET_MODE not defined."
#endif

#ifndef SERVAL_MUTEX_GET_ERROR_COUNT
#error "Macro SERVAL_MUTEX_GET_ERROR_COUNT not defined."
#endif

#ifndef SERVAL_MUTEX_GET_SUM_ERROR_COUNT
#error "Macro SERVAL_MUTEX_GET_SUM_ERROR_COUNT not defined."
#endif

#ifndef SERVAL_MUTEX_IS_TRACE_LOCK
#error "Macro SERVAL_MUTEX_IS_TRACE_LOCK not defined."
#endif

#ifndef SERVAL_MUTEX_SET_NO_TRACE
#error "Macro SERVAL_MUTEX_SET_NO_TRACE not defined."
#endif


#endif /* SERVAL_THREAD_H_ */


