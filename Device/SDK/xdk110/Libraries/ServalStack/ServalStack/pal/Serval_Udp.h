/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Udp.h
 * @brief An interface to the platform UDP (Transport Layer)
 *
 * This interface provides functions to handle UDP communication.
 */

#ifndef SERVAL_UDP_H_
#define SERVAL_UDP_H_

#include <Serval_Defines.h>
#include <Serval_Types.h>
#include <Serval_Ip.h>
#include <Serval_Callable.h>
#include <Serval_Msg.h>
#if SERVAL_ENABLE_DTLS
#include <Serval_Security.h>
#endif

#if SERVAL_POLICY_STACK_CALLS_TLS_API && SERVAL_ENABLE_DTLS
retcode_t Dtls_delete(Udp_Socket_T socket);
retcode_t Dtls_receive(Udp_Socket_T socket, Ip_Address_T *ipAddr_ptr,
                       Ip_Port_T *port_ptr, CommBuff_T *packet_ptr);
retcode_t Dtls_sendTo(Udp_Socket_T socket, Ip_Address_T *ipAddr_ptr,
                      Ip_Port_T port, CommBuff_T packet, Callable_T *callback_ptr);
retcode_t Dtls_prepareForSending(Udp_Socket_T const socket,
                                 MsgSendingCtx_T *sendingCtx_ptr);
retcode_t Dtls_retrySendingLater(Udp_Socket_T const socket,
                                 MsgSendingCtx_T *sendingCtx_ptr);

#define UDP_DELETE              Dtls_delete
#define UDP_RECEIVE             Dtls_receive
#define UDP_SEND_TO             Dtls_sendTo
#define UDP_PREPARE_FOR_SENDING Dtls_prepareForSending
#define UDP_RETRY_SENDING_LATER Dtls_retrySendingLater
#else
#define UDP_DELETE              Udp_delete
#define UDP_RECEIVE             Udp_receive
#define UDP_SEND_TO             Udp_sendTo
#define UDP_PREPARE_FOR_SENDING Udp_prepareForSending
#define UDP_RETRY_SENDING_LATER Udp_retrySendingLater
#endif

/**
 * This function creates a new UDP socket. Is is used by the stack to create
 * sockets for UDP based communication clients. After calling this function,
 * the function Udp_sendTo() can be used to send data to an arbitrary destination
 * address and port. The source port of the socket can be assigned during creation
 * of the socket or at the first call of Udp_sendTo, however, it must remain fixed
 * during the lifetime of the socket.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Udp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signal errors or other issues at the
 * socket. The status code passed to the callback is either RC_OK on incoming
 * data, or a UDP or an IP error otherwise.
 *
 * @param[in] callback_ptr
 * A reference to a callback function which will be invoked on incoming data or
 * reporting errors and any socket issues. It can be NULL, if the upper-layer
 * is not interested in any incoming data.
 *
 * @param[out] socket_ptr
 * On success, handle to UDP socket is given back.
 * On failure, an invalid socket handle is given back.
 *
 * @return when available
 * RC_OK on success, or a UDP or an IP error otherwise.
 *
 * @see
 * Udp_sendTo()
 */
retcode_t Udp_openSocket(Callable_T *callback_ptr, Udp_Socket_T *socket_ptr);

/**
 * This function creates a new UDP socket that can receive messages on the given port.
 * It is used by the stack in order to obtain sockets for UDP based servers.
 * If this function returns RC_OK, the callback specified in the provided callback
 * pointer must be called for every packet received on the specified port. <br>
 * Note: This function encompasses following Berkeley socket API calls:
 * socket(), bind(), listen()
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Udp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signal errors or other issues at the
 * socket. The status code passed to the callback is either RC_OK on incoming
 * data, or a UDP or an IP error otherwise.
 *
 * @param[in] serverPort
 * Port to open, in network byte order. It has to be in range of 0-65535.
 *
 * @param[in] callback
 * Pointer to callback function which is invoked after data is received on given
 * port. It has to be a valid pointer.
 *
 * @param[out] socket_ptr
 * On success, handle to opened UDP socket is given back.
 * On failure, an invalid socket handle is given back.
 *
 * @return
 * RC_OK on success, or a UDP or an IP error otherwise.
 *
 * @see
 * Callable_T, CommBuff_T, CommBuff_realloc(), CommBuff_free()
 */
retcode_t Udp_listen(Ip_Port_T serverPort, Callable_T *callback,
                     Udp_Socket_T *socket_ptr);


/**
 * IMPORTANT: THIS FUNCTION IS DEPRECATED AND NO LONGER TESTED!
 *            IT WILL BE REMOVED IN VERSION 2.0
 *            As an alternative use Udp_openSocket and replace
 *            Udp_send with calls to Udp_sendTo<br>
 *
 * This function creates a UDP socket and "connects" it to the given address and
 * port. After calling this function, the function Udp_send() can be used to
 * send data to the predefined address and port.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Udp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. The status code passed to the callback is either RC_OK on incoming
 * data, or a UDP or an IP error otherwise.
 *
 * @param[in] ipAddr
 * Pointer to the IP address to which datagrams are sent by default
 *
 * @param[in] port
 * UDP port to which datagrams are sent by default, in network byte order.
 * It has to be in range of 0-65535
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked after data is received. It has
 * to be a valid pointer.
 *
 * @param[out] socket_ptr
 * On success, handle to UDP socket is given back.
 * On failure, an invalid socket handle is given back.
 *
 * @return
 * RC_OK on success, or a UDP or an IP error otherwise.
 *
 * @see
 * Callable_T, CommBuff_T, CommBuff_realloc(), CommBuff_free()
 *
 * @deprecated
 */
retcode_t Udp_connect(Ip_Address_T *ipAddr, Ip_Port_T port,
                      Callable_T *callback_ptr, Udp_Socket_T *socket_ptr);


/**
 * IMPORTANT: THIS FUNCTION IS DEPRECATED AND NO LONGER TESTED!
 *            IT WILL BE REMOVED IN VERSION 2.0
 *            As an alternative use Udp_sendTo<br>
 *
 * This function sends given data over a given bound UDP socket. Before
 * invoking this function the function Udp_connect() has to be invoked in order
 * to specify the IP address and port of the recipient.
 *
 * This function sends the given data over the given socket to the destination
 * IP address and port associated with the socket in the function Udp_connect().
 * Every time before invoking this function, the function
 * Udp_prepareForSending() must be invoked and return RC_OK. The CommBuff_T
 * element provided by Udp_prepareForSending() must be used here to carry the
 * payload.
 *
 * After this function returns, the packet will not necessarily have been sent.
 * The caller will be informed about the completion of sending using the given
 * callback function. The status code passed to the callback is either RC_OK on
 * success,  or a UDP, an IP or a DTLS error otherwise.
 * Successful sending does not guarantee that
 * the remote party received the packet as UDP does not support reliable
 * communication.
 *
 * @param[in] socket
 * UDP socket to use for sending. The socket has to be created before calling
 * this function via Udp_connect().
 *
 * @param[in] packet
 * Pointer to data to send to intended recipient (CommBuff_T
 * contains the data). This CommBuff_T must be the one which has been provided
 * by the function Udp_prepareForSending(). UDP may choose to free the given
 * CommBuff_T inside this function. Hence, the CommBuff_T is not guaranteed to
 * be available after the return of this function.
 *
 * @param[in] callback_ptr
 * Pointer to the function that will be triggered after sending UDP data is
 * finished. It has to be a valid pointer.
 *
 * @return
 * RC_OK on success, or a UDP, an IP or a DTLS error otherwise.
 *
 * @see
 * Udp_connect()
 *
 * @deprecated
 */
retcode_t Udp_send(Udp_Socket_T socket, CommBuff_T packet,
                   Callable_T *callback_ptr);

/**
 * This function sends the given data over the given socket to the given IP
 * address and port. Every time before invoking this function, the function
 * Udp_prepareForSending() must be invoked and return RC_OK. The CommBuff_T
 * element provided by Udp_prepareForSending() must be used here to carry the
 * payload.<br>
 *
 * After this function returns, the packet will not necessarily have been sent.
 * The caller will be informed about the completion of sending using the given
 * callback function. The status code passed to the callback is either RC_OK on
 * success, or a UDP, an IP or a DTLS error otherwise.
 * Successful sending does not guarantee that
 * the remote party received the packet as UDP does not support reliable
 * communication.
 *
 * If socket is a DTLS-socket (opened with Udp_openSecureSocket() or
 * Udp_listenSecure()) the sending data are encrypted before sending
 * them out of the socket. In case of a DTLS client, a DTLS connection
 * must be opened with Udp_connectSecure() before you can send data.
 * In case of a DTLS server the DTLS connections are automatically
 * opened by the DTLS listener socket Udp_listenSecure().
 *
 * @param[in] socket
 * UDP socket to use for sending. The socket has to be created before calling
 * this function using Tcp_connect() or Tcp_accept().
 *
 * @param[in] ipAddr_ptr
 * Pointer to the IP address of the recipient
 *
 * @param[in] port
 * UDP port of the recipient, in network byte order.
 *
 * @param[in] packet
 * Pointer to data to send to intended recipient (CommBuff_T
 * contains the data). This CommBuff_T must be the one which has been provided
 * by the function Udp_prepareForSending().
 * The PAL implementation must free any buffers that have been allocated.
 * Thus, the buffer must not be used after the return of this function.
 *
 * @param[in] callback_ptr
 * Pointer to the function that will be triggered after sending UDP data is
 * finished. It has to be a valid pointer.
 *
 * @return
 * RC_OK on success.
 * RC_UDP_OUT_OF_MEMORY and RC_UDP_SOCKET_BUSY to indicate the respective
 * transient problems and the stack should retry sending using Udp_retrySendingLater.
 * Any other IP, UDP or DTLS return code indicates an error that will cause
 * the stack to abort sending.
 *
 * @see
 * Udp_listen(), Udp_createSocket(), Udp_openSecureSocket(), Udp_listenSecure(),
 * Udp_connectSecure(), Udp_retrySendingLater()
 */
retcode_t Udp_sendTo(Udp_Socket_T socket, Ip_Address_T *ipAddr_ptr,
                     Ip_Port_T port, CommBuff_T packet, Callable_T *callback_ptr);

/**
 * This function deletes the socket of the given socket handle. After invoking
 * this function the given socket handle is invalid, and must not be used for
 * any further communication unless reinitialized.
 *
 * @param[in] socket
 * Handler to the socket to be deleted
 *
 * @return when errorcode available
 * On success, RC_OK is returned. <br>
 * RC_UDP_INVALID_SOCKET<br>
 *
 * @see
 * Udp_listen(), Udp_createSocket(), Udp_openSecureSocket(), Udp_listenSecure()
 */
retcode_t Udp_delete(Udp_Socket_T socket);

/**
 * This function fetches the waiting data, the IP address and the
 * UDP port of the sender from the given UDP connection. This function should
 * only be called if the socket callback passed to Udp_listen() or
 * Udp_connect() is invoked with the status RC_OK, which indicates incoming
 * data.
 *
 * The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().
 *
 * If socket is a DTLS-socket (opened with Udp_openSecureSocket() or
 * Udp_listenSecure()) the received data are decrypted before handling
 * them over to the caller of Udp_receive().
 *
 * @param[in] socket
 * UDP socket to get data from. It has to be a valid UDP socket.
 *
 * @param[out]	ipAddr_ptr
 * Reference to the variable which should hold IP address of the sender, if the
 * function returns success. It has to be a valid pointer.
 *
 * @param[out]	port_ptr
 * Reference to the variable which should hold port of the sender, if the
 * function returns success. It has to be a valid pointer.
 *
 * @param[out]	packet_ptr
 * Pointer to buffer for received data. It has to be a valid pointer. The
 * provided CommBuff_T element should be released by the upper-layer as soon as
 * possible. After the return of the socket callback, the buffer is not
 * guaranteed to stay available.
 *
 * @return
 * RC_OK if data is successfully stored in given CommBuffer. Otherwise a
 * a UDP, an IP or a DTLS error is returned.
 *
 * @see
 * Udp_listen(), Udp_connect(), Udp_openSecureSocket(), Udp_listenSecure()
 */
retcode_t Udp_receive(Udp_Socket_T socket, Ip_Address_T *ipAddr_ptr,
                      Ip_Port_T *port_ptr, CommBuff_T *packet_ptr);

/**
 * This function checks if the given UDP socket is a valid or not and returns
 * the result.
 *
 * @param[in] socket
 * UDP socket to be checked
 *
 * @return
 * TRUE if socket handle is valid, FALSE if socket handle is invalid
 *
 * see Udp_openSecureSocket()
 */
bool Udp_isValidSocket(Udp_Socket_T const socket);

/**
 * This function should be used to un-initialize a UDP socket handle.
 * It returns the handle of an invalid UDP socket. The call
 * Udp_isValidSocket(Udp_getInvalidSocket()) results in FALSE.
 *
 * @return
 * A handle of an invalid socket. Accessing this socket for reading or writing
 * as well as for communication would fail.
 *
 * @see Udp_isValidSocket()
 */
Udp_Socket_T Udp_getInvalidSocket(void);


/**
 * This function checks whether two given sockets are equal or not and returns
 * the result.
 *
 * @param[in] socket1
 * Socket to be compared with other given socket.
 *
 * @param[in] socket2
 * Socket to be compared with other given socket.
 *
 * @return TRUE: sockets are equal, FALSE sockets are not equal
 *
 */
bool Udp_isEqualSocket(Udp_Socket_T const socket1, Udp_Socket_T const socket2);


/**
 * Prepares the socket and the sending context for sending. This function
 * must be called before start sending. It ensures that the socket is ready for
 * sending. It also allocate a communication buffer for serializing the message
 * packets. This means that sendingCtx_ptr->buffer must be a valid buffer.
 *
 * The allocated buffer is used for serialization of the message.
 * It needs to be valid until either
 * a) Udp_sendTo has been invoked with this buffer after which the ownership
 *    of the buffer passes back to the PAL implementation, or
 * b) CommBuff_free has been called on this buffer to indicate that sending
 *    has been aborted due to a serialization error.
 *
 * @param[in] socket
 * The socket over which the message should be sent.
 *
 * @param[in] sendingCtx_ptr
 * A pointer to the context structure of the sending job.
 *
 * @return
 * RC_OK on success.
 * RC_UDP_OUT_OF_MEMORY and RC_UDP_SOCKET_BUSY to indicate the respective
 * transient problems and the stack should retry sending using Udp_retrySendingLater.
 * Any other IP, UDP or DTLS return code indicates an error that will cause
 * the stack to abort sending.
 *
 * @see MsgSendingCtx_T, Udp_retrySendingLater()
 */
retcode_t Udp_prepareForSending(
    Udp_Socket_T const socket, MsgSendingCtx_T *sendingCtx_ptr);

/**
 * This function should schedule the given sending job to be carried out later.
 * This means that this function takes care to invoke the corresponding sending
 * function, i.e., sendingCtx_ptr->sendingFunc, at some point later.
 * Any other return code except RC_OK means that sending cannot be completed by
 * by lower layer at given time and should be dropped.
 *
 * It is up to the PAL implementation when to execute the function.
 * However, the execution MUST be asynchronous, i.e. the caller of this function
 * can remove itself from the execution stack and is not part of the execution
 * stack when sendingCtx_ptr->sendingFunc is executed.
 *
 * An implementation of this function is free to decide whether to call
 * sendingCtx_ptr->sending function after an interval or when the socket is able
 * to send again. In the latter case, however, calling this function on a socket
 * that is not busy, MUST nevertheless trigger execution of sendingCtx_ptr->sendingFunc.
 *
 * Finally, on multi-threaded implementations of the PAL, the execution of
 * sendingCtx_ptr->sendingFunc is expected to happen in the same context that normal
 * network operations take place in.
 *
 * Implementation note:
 *   The interface currently does not provide a meaningful way for aborting a
 *   retry. This can occur if the message times out while waiting for transmission
 *   and is thus deleted. This in turn will result in the MsgSendingCtx_T structure
 *   to have been cleared. Thus an implementation must check the validity of the
 *   sending function in sendingCtx_ptr->sendingFunc before calling it.
 *
 * @param[in] socket
 * The socket over which the message should be sent.
 *
 * @param[in] sendingCtx_ptr
 * A pointer to the context structure of the sending job.
 *
 * @return
 * RC_OK on success, or a UDP, an IP or a DTLS error otherwise.
 *
 * @see MsgSendingCtx_T, Udp_retrySendingLater()
 */
retcode_t Udp_retrySendingLater(
    Udp_Socket_T const socket, MsgSendingCtx_T *sendingCtx_ptr);


#if SERVAL_ENABLE_DTLS_SERVER
/**
 * This function creates a DTLS-UDP server socket and binds to the given port.
 * It registers the callback function included in given callback_ptr for
 * incoming messages. After successful calling of this function a remote client
 * can establish a DTLS connection to this server port via Udp_connectSecure
 * and communicate in a secure way with this server. Unlistening of this
 * secure socket can be called by normal udp_unlisten.
 *
 * If the system is configured to use pre-shared keys, Dtls_setServerPskCallback()
 * MUST be called in order to provide keying material to the implementation via
 * the supplied callback.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Udp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. The status code passed to the callback is either RC_OK on incoming
 * data, or a UDP, an IP or a DTLS error otherwise.
 *
 * @param[in] serverPort
 * Port to open. It has to be in range of 0-65535.
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked after data is received on given
 * port. It has to be a valid pointer.
 * status for callback function:
 * On success, RC_OK is returned.<br>
 * RC_UDP_PORT_ALREADY_USED<br>
 * RC_UDP_NO_FREE_PORT if limit of supported number of ports is reached in
 * udp implementation<br>
 * RC_DTLS_NO_BUFFER_FREE if no more secure connections can be established
 *
 * @param[out] socket
 * On success, handle to opened UDP socket is given back.
 * On failure, an invalid socket handle is given back.
 *
 * @return when errorcode available
 * RC_OK on success, or a UDP, an IP or a DTLS error otherwise.
 *
 * @see Udp_unlisten(),
 * Callable_T, CommBuff_T, CommBuff_realloc(), CommBuff_free(),
 * Dtls_setServerPskCallback()
 */
retcode_t Udp_listenSecure(Ip_Port_T serverPort, Callable_T *callback_ptr,
                           Udp_Socket_T *socket);
#endif /* SERVAL_ENABLE_DTLS_SERVER */


#if SERVAL_ENABLE_DTLS_CLIENT
/**
 * This function creates a DTLS-UDP socket. After calling this function, the
 * function Udp_connectSecure() can be used to create DTLS connections
 * from this socket to remote server ports.<br>
 * Use Udp_delete() to close this socket again.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Udp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. The status code passed to the callback is either RC_OK on incoming
 * data, or a UDP or an IP error otherwise.
 *
 * @param[in] callback_ptr
 * A reference to a callback function which will be invoked on incoming data or
 * reporting errors and any socket issues. It can be NULL, if the upper-layer
 * is not interested in any incoming data.
 *
 * @param[out] socket_ptr
 * On success, handle to UDP socket is given back.
 * On failure, an invalid socket handle is given back.
 *
 * @return when error code available
 * RC_OK on success, or a UDP, an IP or a DTLS error otherwise.
 *
 * @see
 * Udp_connectSecure(), Udp_delete()
 */
retcode_t Udp_openSecureSocket(Callable_T *callback_ptr,
                               Udp_Socket_T *socket_ptr);


/**
 * This function creates a DTLS-UDP connection from the calling client
 * to a remote server port.<br>
 *
 * If the system is configured to use pre-shared keys, Dtls_setClientPskCallback()
 * MUST be called in order to provide keying material to the implementation via
 * the supplied callback.
 *
 * Before this function is called, the local client socket must be
 * opened with Udp_openSecureSocket().<br>
 * After calling this function, the function Udp_sendTo() can be used
 * to send data via DTLS to the specified server address and port.
 * When some data is received, the registered callback of the socket
 * is invoked.
 * Before the return of the callback, Udp_receive() must be invoked in
 * order to receive the incoming packet.<br>
 * The received CommBuff_T element is not guaranteed to stay available
 * after the return of the callback. If the received packet is needed
 * for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate
 * a buffer which is kept until it is explicitly freed using the function
 * CommBuff_free().
 *
 * @param[in] socket
 * Local client socket, which was created by Udp_openSecureSocket.
 *
 * @param[in] destAddr_ptr
 * ip address of the remote server socket
 *
 * @param[in] destPort
 * port number of the remote server socket
 *
 * @return
 * RC_OK on success, or a UDP, an IP or a DTLS error otherwise.
 *
 * @see
 * Udp_sendTo(), Udp_openSecureSocket(), Udp_closeSecureConn(),
 * Dtls_setClientPskCallback()
 */
retcode_t Udp_connectSecure(Udp_Socket_T const socket,
                            Ip_Address_T *destAddr_ptr, Ip_Port_T destPort);
#endif /* SERVAL_ENABLE_DTLS_CLIENT */

#if SERVAL_ENABLE_DTLS
/**
 * This function close and disconnect an existing DTLS-UDP connection.
 * The connection may created from client side by Udp_connectSecure() or from
 * server side implicitly with Udp_listenSecure().
 *
 * @param[in] socket
 * Local client socket.
 *
 * @param[in] destAddr_ptr
 * ip address of the remote socket (may be client or server)
 *
 * @param[in] destPort
 * port number of the remote socket (may be client or server)
 *
 * @return
 * On success, RC_OK is returned. <br>
 * RC_DTLS_CONNECTION_NOT_EXIST no connection to this endpoint exists<br>
 *
 * @see Udp_connectSecure(), Udp_listenSecure()
 */
retcode_t Udp_closeSecureConn(Udp_Socket_T const socket,
                              Ip_Address_T *destAddr_ptr, Ip_Port_T destPort);



/**
 * This function reports the secure connections on a specified
 * socket.
 *
 * @param[in,out] iterator_ptr
 * Iterator for this function. Initialized to -1 it reports the first
 * connection. The next returned values will report the next connections.
 * if -1 is returned, no more connections are assigned to this socket.<br>
 * Example:<br>
 *    int16_t iter = -1;<br>
 *    while(1) {<br>
 *         Udp_iterateSecureConnections(&iter,...);<br>
 *         if(iter < 0) break;<br>
 *         do_some_thing(...);<br>
 *    }<br>
 *
 * @param[in] socket
 * secure udp socket.
 *
 * @param[in] onlyWithError
 * true -> report only connections with failures.
 * false -> report all connections
 *
 * @param[out] destAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[out] port_ptr
 * returned port number of the connection endpoint, in network byte order.
 *
 * @see Udp_openSecureSocket(), Udp_connectSecure()
 */
void Udp_iterateSecureConnections(int16_t *iterator_ptr,
                                  Udp_Socket_T const socket, bool onlyWithError,
                                  Ip_Address_T *destAddr_ptr, Ip_Port_T *port_ptr);

/**
 * This function reports the errors of a secure connection
 *
 * @param[in] socket
 * secure udp socket.
 *
 * @param[in] destAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] destPort
 * returned port number of the connection endpoint
 *
 * @param[out] rc_ptr
 * status of this connection
 *
 * @see Udp_openSecureSocket(), Udp_connectSecure()
 */
retcode_t Udp_getSecureConnError(Udp_Socket_T const socket,
                                 Ip_Address_T *destAddr_ptr, Ip_Port_T destPort,
                                 retcode_t *rc_ptr);

/**
 * This function reports the status of a secure connection
 *
 * @param[in] socket
 * secure udp socket.
 *
 * @param[in] destAddr_ptr
 * returned ip address of the connection endpoint
 *
 * @param[in] destPort
 * returned port number of the connection endpoint
 *
 * @param[out] state
 * status of this connection
 *
 * @return
 * On success, RC_OK is returned.<br>
 * RC_DTLS_NO_CONNECTION<br>
 * RC_UDP_INVALID_SOCKET<br>
 *
 * @see Udp_openSecureSocket(), Udp_connectSecure()
 */
retcode_t Udp_getSecureConnState(Udp_Socket_T const socket,
                                 Ip_Address_T *destAddr_ptr, Ip_Port_T destPort,
                                 SecureConnectionState_T *state);

#endif /* SERVAL_ENABLE_DTLS */

#endif /* SERVAL_UDP_H_ */
