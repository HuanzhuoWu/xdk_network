/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Ip.h
 * @brief Interface to the platform IP management.
 *
 * This module provides functions to convert an IP address String to
 * an Integer and vice versa. It also provides functions to convert a port to
 * an Integer and vice versa.
 * It provides a function to retrieve the IP address of its current interface.
 * <br>
 * Note: This interface includes PIp.h. This header has to be provided by the
 * PAL implementation. It has to provide:
 * <ul>
 * <li> Ip_Address_T a data type representing IP addresses</li>
 * <li> Ip_Port_T, a data type representing port numbers in the network byte
 * order </li>
 * <li> Tcp_Socket_T a data type representing TCP socket handles. <br>
 *      This data type must support assignment using = and equality checking using == </li>
 * <li> Tcp_Listener_T a data type representing TCP server socket handles. <br>
 *      This data type must support assignment using = and equality checking using == </li>
 * <li> Udp_Socket_T a data type representing UDP sockets handles. <br>
 *      This data type must support assignment using = and equality checking using == </li>
 * <li> CommBuff_T a data type representing communication buffer handles,
 * see also Serval_CommBuff.h.<br>
 *      This data type must support assignment using = and equality checking using == </li>
 * <li> SERVAL_IP_ADDR_LEN a Macro which defines the number of characters which
 * an IP may maximally contain when it is presented as a string. Note that
 * converting an IP address into a string is also implemented in PAL, see
 * Ip_convertAddrToString(). This constant and the implementation of
 * Ip_convertAddrToString() must be compatible.
 * </li></ul>
 */

#ifndef SERVAL_IP_H_
#define SERVAL_IP_H_

#include <PIp.h>

#include <Serval_Types.h>


/**
 * This function should be used to un-initialize an IP address. It returns
 * a reference to the unspecified address which is 0.0.0.0 in the case of IPv4
 * or 0:0:0:0:0:0:0:0:0 (i.e. ::) in the case of IPv6.
 *
 * @return
 * A reference to the unspecified IP address.
 */
Ip_Address_T const *Ip_getUnspecifiedAddr(void);


/**
 * This function is called compares two IP addresses on being equal and return
 * the result.
 *
 * @param[in] ip1_ptr
 * A pointer to the first IP address. It has to be valid.
 *
 * @param[in] ip2_ptr
 * A pointer to the second IP address. It has to be valid.
 *
 * @return TRUE if the both IP address are equal, or FALSE otherwise.
 */
bool Ip_compareAddr(Ip_Address_T const *ip1_ptr, Ip_Address_T const *ip2_ptr);

/**
 * This function converts a given IP address to a null-terminated string, and
 * stores the result in the given buffer.
 *
 * @param[in] ipAddr_ptr
 * A valid pointer reference to the IP address to be converted.
 *
 * @param[out] buffer_ptr
 * A valid pointer to the buffer which the IP string should be written to. The
 * given buffer must be larger than the maximal length of the IP string
 * presentation by at least 1 (for the null-character).
 * The written string will be NULL terminated.
 *
 * @return
 * On success, the total number of characters written is returned. This count
 * does not include the additional null-character.
 * On failure, a negative number is returned.
 */
int Ip_convertAddrToString(Ip_Address_T const *ipAddr_ptr, char *buffer_ptr);

/**
 * This function converts a given port from the network byte order to an Integer
 * in the platform byte order and returns the result.
 *
 * @param[in] port
 * Port to be converted.
 *
 * @return
 * Converted port as an Integer in the platform byte order.
 */
uint16_t Ip_convertPortToInt(Ip_Port_T port);

/**
 * This function converts a given Integer to a port and returns the result.
 *
 * @param[in] port
 * The port number as an Integer in the platform byte order
 *
 * @return
 * The port in the network byte order.
 */
Ip_Port_T Ip_convertIntToPort(uint16_t port);

/**
 * This function returns a reference to the IP address assigned to the own
 * network interface. If no IP address has been assigned to the network
 * interface, this function returns the unspecified IP address, i.e. an address
 * which is equal to the result of Ip_getUnspecifiedAddr().
 *
 * @return
 * On success, a reference to the own IP address is returned. On failure Null is
 * returned.
 *
 * @see Ip_getUnspecifiedAddr()
 */
Ip_Address_T* Ip_getMyIpAddr(void);

#ifdef SERVAL_IPV6

/**
 * This function converts the given eight short numbers (2-bytes numbers) to an
 * IPv6 address.
 *
 * @param[in] b1
 * First block of the IPv6 address.
 *
 * @param[in] b2
 * Second block of the IPv6 address.
 *
 * @param[in] b3
 * Third byte of the IPv6 address.
 *
 * @param[in] b4
 * Forth block of the IPv6 address.
 *
 * @param[in] b5
 * Fifth block of the IPv6 address.
 *
 * @param[in] b6
 * Sixth block of the IPv6 address.
 *
 * @param[in] b7
 * Seventh block of the IPv6 address.
 *
 * @param[in] b8
 * Eighth block of the IPv6 address.
 *
 * @param[out] ipAddr_ptr
 * Reference to an IPv6 address for the result of the conversion. It has to be
 * a valid pointer.
 */
void Ip_convertBlocksToAddr(
    uint16_t b1, uint16_t b2, uint16_t b3, uint16_t b4,
    uint16_t b5, uint16_t b6, uint16_t b7, uint16_t b8,
    Ip_Address_T *ipAddr_ptr);

#else /* If SERVAL_IPV6 not defined */

/**
 * This function converts given octets to an IP address.
 *
 * @param[in] first
 * First block of the IP address. It has to be in the range of 0-255.
 *
 * @param[in] second
 * Second block of the IP address. It has to be in the range of 0-255.
 *
 * @param[in] third
 * Third block of the IP address. It has to be in the range of 0-255.
 *
 * @param[in] fourth
 * Forth block of the IP address. It has to be in the range of 0-255.
 *
 * @param[out] ipAddr_ptr
 * Reference to an IPv4 address for the result of the conversion. It has to be
 * a valid pointer.
 *
 * TODO: Rename to Ip_convertBlocksToAddr()
 */
void Ip_convertOctetsToAddr(uint8_t first, uint8_t second,
                            uint8_t third, uint8_t fourth, Ip_Address_T *ipAddr_ptr);

#endif /* SERVAL_IPV6 */

/**
 * This function is called to copy a given IP address to a destination whose
 * reference is given.
 *
 * @param[in] src_ptr
 * Reference to the source IP address to be copied
 *
 * @param[out] dest_ptr
 * Reference to the destination which the given IP will copied to.
 */
void Ip_copyAddr(Ip_Address_T const *src_ptr, Ip_Address_T *dest_ptr);

#endif /* SERVAL_IP_H_ */
