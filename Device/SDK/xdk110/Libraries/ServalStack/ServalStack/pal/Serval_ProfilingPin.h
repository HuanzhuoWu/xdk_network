/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_ProfilingPin.h
 * @brief An interface to the platform Pin.
 *
 * Profiling can be used to do time measurements on the device. Beside that a
 * pin on the platform can be used to watch the activity with an oscilloscope.
 * This module provides methods to initialize a pin on the platform and to set
 * the output data of it.
 *
 * Used by ServalStack/api/Serval_Profiling.h.
 */

#ifndef SERVAL_PROFILINGPIN_H_
#define SERVAL_PROFILINGPIN_H_

#include <Serval_Defines.h>
#include <Serval_Types.h>
#include <Serval_Exceptions.h>

/**
 * This function is called to sets a specified pin as output.
 *
 * @param[in] pin
 * The platform specific id of the pin to be configured. It is retrieved by
 * invoking ProfilingPin_getPin()
 *
 * @return
 * RC_OK if the operation was successful,
 * RC_PROFILING_PIN_ERROR otherwise
 *
 * @see
 * ProfilingPin_getPin()
 */
retcode_t ProfilingPin_init(int pin);

/**
 * This function is called to fetch the platform specific id of the i-th
 * profiling number.
 *
 * @param[in] i
 * A number of the sequence 1, 2, ..., SERVAL_PROFILING_MAX_NUM_PINS
 *
 * @return
 * The platform specific pin id of the i-th profiling pin. If i is smaller than
 * 1 or larger than SERVAL_PROFILING_MAX_NUM_PINS, then the return value is -1.
 */
int ProfilingPin_getPin(int i);

/**
 * This function is called to set the pin output data to zero or one based on
 * the given status. Before invoking this function ProfilingPin_init() must be
 * invoked.
 *
 * @param[in] pin
 * The platform specific id of the pin to be configured.
 *
 * @param[in] status
 * The platform specific pin output data. Can be set to zero or one
 *
 * @return
 * RC_OK if the operation was successful,
 * RC_PROFILING_PIN_ERROR otherwise
 *
 * @see
 * ProfilingPin_init()
 */
retcode_t ProfilingPin_setStatus(int pin, bool status);

#endif /* SERVAL_PROFILINGPIN_H_ */
