/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file Serval_Tcp.h
 * @brief An interface to the platform TCP (Transport Layer)
 *
 * This interface provides functions to handle TCP communication.
 */

#ifndef SERVAL_TCP_H_
#define SERVAL_TCP_H_

#include <Serval_Defines.h>
#include <Serval_Ip.h>
#include <Serval_Msg.h>

/**
 * This enum is used to represent connection state of a TCP socket.
 */
typedef enum
{
	/**
	 * The socket is closed and should be deleted.
	 */
	TCP_SOCKET_STATUS_CLOSED = 0,

	/**
	 * If we initiate connection to a remote party, then the socket is connecting
	 * until the connection has been established (SYN received from the remote
	 * party).
	 */
	TCP_SOCKET_STATUS_CONNECTING = 1,

	/**
	 * If we accept the connection of the other party or the other party has
	 * accepted our connection request, then the socket is open (may be used for
	 * data communication).
	 */
	TCP_SOCKET_STATUS_OPEN = 2,

	/**
	 * If we have initiated the closing of the connection, then the socket is
	 * half-closed until the other party closes the connection at its end
	 * (FIN received from the other party).
	 */
	TCP_SOCKET_STATUS_HALF_CLOSED = 3,

	/**
	 * If the remote party has initiated the closing of the connection
	 * (FIN received), then the socket is half-open until we close the
	 * connection at our end (by sending a FIN to the other party).
	 */
	TCP_SOCKET_STATUS_HALF_OPEN = 4,

	/**
	 * If the remote party has initiated the closing of the connection, i.e.,
	 * the socket is half-open, and we close the connection, then the socket is
	 * closing.
	 */
	TCP_SOCKET_STATUS_CLOSING = 5,

} Tcp_SocketStatus_T;


#if SERVAL_POLICY_STACK_CALLS_TLS_API && SERVAL_ENABLE_TLS
retcode_t Tls_unlisten(Tcp_Listener_T listener);
retcode_t Tls_accept(Tcp_Listener_T listener, Callable_T *callback_ptr,
                     Tcp_Socket_T *socket_ptr);
retcode_t Tls_close(Tcp_Socket_T socket);
retcode_t Tls_delete(Tcp_Socket_T socket);
retcode_t Tls_receive(Tcp_Socket_T socket, CommBuff_T *packet_ptr);
retcode_t Tls_send(Tcp_Socket_T socket, CommBuff_T packet,
                   Callable_T *callback_ptr);
retcode_t Tls_prepareForSending(Tcp_Socket_T const socket,
                                MsgSendingCtx_T *sendingCtx_ptr);
retcode_t Tls_retrySendingLater(Tcp_Socket_T const socket,
                                MsgSendingCtx_T *sendingCtx_ptr);
retcode_t Tls_getSocketStatus(Tcp_Socket_T socket,
                              Tcp_SocketStatus_T *status_ptr);

#define TCP_UNLISTEN            Tls_unlisten
#define TCP_ACCEPT              Tls_accept
#define TCP_CLOSE               Tls_close
#define TCP_DELETE              Tls_delete
#define TCP_RECEIVE             Tls_receive
#define TCP_SEND                Tls_send
#define TCP_PREPARE_FOR_SENDING Tls_prepareForSending
#define TCP_RETRY_SENDING_LATER Tls_retrySendingLater
#define TCP_GET_SOCKET_STATUS   Tls_getSocketStatus
#else
#define TCP_UNLISTEN            Tcp_unlisten
#define TCP_ACCEPT              Tcp_accept
#define TCP_CLOSE               Tcp_close
#define TCP_DELETE              Tcp_delete
#define TCP_RECEIVE             Tcp_receive
#define TCP_SEND                Tcp_send
#define TCP_PREPARE_FOR_SENDING Tcp_prepareForSending
#define TCP_RETRY_SENDING_LATER Tcp_retrySendingLater
#define TCP_GET_SOCKET_STATUS   Tcp_getSocketStatus
#endif


/**
 * This function creates a new TCP listener socket, binds it to the given port
 * number and causes the socket to enter the listening state.
 * After invoking this function the socket is ready to accept incoming
 * connections.
 *
 * When a connection request comes in, the given callback is invoked.
 * Before the callback function returns, Tcp_accept() must be called in order
 * to accept the incoming connection.
 *
 * The callback function is also invoked on errors. In this case the callback
 * is invoked with a TCP or an IP error.
 *
 * Note: This function is only used by a TCP server.
 *
 * Note: This function encompasses following Berkeley socket API calls:
 * socket(), bind(), listen()
 *
 * @param[in] port
 * TCP port to listen at, in network byte order.
 *
 * @param[in] callback
 * Callback function which is invoked on incoming connection
 * requests. It must be valid and assigned.
 *
 * @param[out] listener_ptr
 * On success, a TCP listener handle is given back. On failure,
 * an invalid socket handle is given back.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * Tcp_isValidListener(), Tcp_accept(), Callable_T, Callable_isAssigned()
 */
retcode_t Tcp_listen(Ip_Port_T port, Callable_T *callback,
                     Tcp_Listener_T *listener_ptr);


/**
 * This function stops listening on given TCP listener socket.
 * After invoking this function the given TCP listener handle is invalid.
 * Before invoking this function Tcp_listen() has to be called. <br>
 * This function is only used by a TCP server.
 *
 * @param[in] listener
 * TCP listener handle to be closed
 *
 * @return
 * RC_OK on success <br>
 * RC_TCP_PORT_NOT_USED port not registered as listener<br>
 *
 * @see
 * Tcp_isValidListener(), Tcp_listen()
 */
retcode_t Tcp_unlisten(Tcp_Listener_T listener);


/**
 * This function accepts a received incoming attempt to create a new TCP
 * connection from a remote client. After calling this function data can be
 * sent and received over the returned TCP socket. <br>
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Tcp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. In this case, the callback is invoked with a TCP or an IP retcode.
 *
 * Note: This function is only used by a TCP server.
 *
 * @param[in] listener
 * Handle to a TCP server socket
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked if data is received on given
 * socket. If it is a NULL pointer, then the upper-layer is not interested
 * to be notified about socket activities such as incoming packets or socket
 * errors.
 *
 * @param[out] socket_ptr
 * On success, a TCP socket handle is returned. On failure, an invalid socket
 * handle is returned.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * Tcp_listen()
 */
retcode_t Tcp_accept(Tcp_Listener_T listener, Callable_T *callback_ptr,
                     Tcp_Socket_T *socket_ptr);


/**
 * This function causes to open a TCP connection to the given host
 * (IP address, port).
 * It creates a new TCP socket and causes an attempt to
 * establish a new TCP connection (handshake) to the given IP address and port.
 * After finishing the connection establishment, data can be send and received
 * via the returned TCP socket. <br>
 *
 * After this function returns the connection has not necessarily been entirely
 * established. The function Tcp_prepareForSending() will return
 * RC_TCP_SOCKET_BUSY as long as the connection has not been established.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Tcp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. In this case, the callback is invoked with a TCP or an IP retcode.
 *
 * Note: This function is only used by a TCP client.
 *
 * Note: This function encompasses following Berkeley socket API calls:
 * socket(), connect()
 *
 * @param[in] ipAddr_ptr
 * Pointer to IP address of the remote host (server) to connect to
 *
 * @param[in] port
 * TCP port on remote host to connect to (server port), in network byte order.
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked if data is received on given
 * socket.
 *
 * The callback function should lead to calling Tcp_receive() in order to
 * read the incoming data.
 * It has to be a valid pointer.
 *
 * @param[out] socket_ptr
 * On success, a TCP socket handle is returned. On failure, an invalid socket
 * handle is returned.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * Tcp_isValidSocket(), Tcp_send(), Tcp_receive()
 */
retcode_t Tcp_connect(Ip_Address_T *ipAddr_ptr, Ip_Port_T port,
                      Callable_T *callback_ptr, Tcp_Socket_T *socket_ptr);


/**
 * It returns the status of the given socket.
 *
 * @param[in] socket
 * The socket the status of which is required. It must be a valid socket.
 *
 * @param[out] status_ptr
 * The status of the socket. It must be a valid pointer to save the status to.
 *
 * @return
 * RC_OK on success<br>
 * RC_TCP_INVALID_SOCKET if the given socket is invalid.
 */
retcode_t Tcp_getSocketStatus(
    Tcp_Socket_T socket, Tcp_SocketStatus_T *status_ptr);

/**
 * This function terminates the given TCP connection. Thus calling this function
 * causes TCP to send out a FIN message to the connected partner. After invoking
 * this function the socket may be half-open. This means we cannot send any data
 * to the other side, but we can receive messages from the other side. If the
 * other side also terminates the connection by sending a FIN message, the
 * connection is completely terminated.
 * An incoming FIN message is indicated by an invalid buffer in Tcp_receive().
 * Note: Some platform may implicitly reply with FIN_ACK for a incoming FIN
 * without waiting for confirmation from application. Thus on such platform we
 * never have a half open connection. However application is still expected to
 * call this function.
 *
 * If the socket callback is null, then PAL needs to delete the socket
 * implicitly after the close is done.
 *
 * @param[in] socket
 * Handle to the TCP socket to be terminated.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * Tcp_receive()
 */
retcode_t Tcp_close(Tcp_Socket_T socket);


/**
 * This function causes the system to release resources allocated to the given
 * socket. After invoking this function the given TCP socket handle is invalid.
 * This function should be invoked after Tcp_close() had been closed and the
 * socket callback has been invoked with an invalid buffer, which indicates that
 * the connection has been completely closed.
 *
 * @param[in] socket
 * Handle to the TCP socket to be destroyed.
 *
 * @return
 * RC_OK on success <br>
 * RC_TCP_INVALID_SOCKET<br>
 * RC_TCP_CONNECTED<br>
 *
 * @see Tcp_close()
 */
retcode_t Tcp_delete(Tcp_Socket_T socket);


/**
 * This function identifies the connection partner (IP address, port) for a
 * given socket handle.
 *
 * @param[in] socket
 * Handle to the socket to get information from
 *
 * @param[out]	ipAddr_ptr
 * IP address of the connection partner
 *
 * @param[out] port_ptr
 * TCP port of the connection partner, in network byte order.
 *
 * @return
 * RC_OK on success <br>
 * RC_TCP_INVALID_SOCKET<br>
 * RC_TCP_NOT_CONNECTED<br>
 * RC_TCP_INVALID_PEER<br>
 *
 */
retcode_t Tcp_getPeerName(Tcp_Socket_T socket, Ip_Address_T *ipAddr_ptr,
                          Ip_Port_T *port_ptr);


/**
 * This function sends the data in the given CommBuff via the given TCP
 * socket. Every time before invoking this function, the function
 * Tcp_prepareForSending() must be invoked and return RC_OK. The CommBuff_T
 * element provided by Tcp_prepareForSending() must be used here to carry the
 * payload.
 *
 * After this function returns, the packet will not necessarily have been sent.
 * The caller will be informed about the completion of sending using the given
 * callback function. Furthermore, successful sending does guarantee that
 * the remote party received the packet as TCP does provide reliable
 * communication.
 *
 * The callback is also used to report errors to the callers. If an error
 * occurred after Tcp_send has returned. In this case the callback is
 * invoked with a TCP or an IP retcode.
 *
 * @param[in] socket
 * Handle to the TCP socket to send data
 *
 * @param[in] packet
 * Handle to the CommBuff_T which contains the payload to be sent. This
 * CommBuff_T must be the one which has been provided by the function
 * Tcp_prepareForSending().
 * The PAL implementation must free any buffers that have been allocated.
 * Thus, the buffer must not be used after the return of this function.
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked after sending of given data is
 * complete. The application has to wait with sending a new packet until the
 * callback is invoked.
 *
 * @return
 * RC_OK on success.
 * RC_TCP_OUT_OF_MEMORY or RC_TCP_SOCKET_BUSY to indicate the respective transient
 * problems and the stack should retry sending using Tcp_retrySendingLater.
 * Any other IP, TCP or DTLS return code indicates an error that will cause
 * the stack to abort sending.
 *
 * @see Tcp_retrySendingLater()
 */
retcode_t Tcp_send(Tcp_Socket_T socket, CommBuff_T packet,
                   Callable_T *callback_ptr);


/**
 * This function fetches the received data from the given TCP socket.
 * This function should only be called if the socket callback passed to
 * Tcp_connect() or Tcp_accept() is invoked with the status RC_OK, which
 * indicates incoming data.
 *
 * If the received CommBuff_T element is invalid while the callback status as
 * well as the retcode of Tcp_receive() is RC_OK, then a FIN has been received
 * which means that the remote party has closed the connection. If such an
 * invalid CommBuff_T is received after Tcp_close() has been invoked, then
 * the connection is completely closed and should be deleted using the function
 * Tcp_delete().
 *
 * @param[in] socket
 * Handle to the TCP socket to get data of
 *
 * @param[out]	packet_ptr
 * Pointer to communication buffer to which received data is written.
 * The provided CommBuff_T element should be released by the upper-layer as
 * soon as possible. After the return of the socket callback, the buffer is not
 * guaranteed to stay available.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * Tcp_accept(), Tcp_connect(), CommBuff_isValid()
 */
retcode_t Tcp_receive(Tcp_Socket_T socket, CommBuff_T *packet_ptr);


/**
 * This function checks if the given TCP socket is a valid one.
 *
 * @param[in] socket
 * Handle to the TCP socket to be checked
 *
 * @return
 * TRUE if socket handle is valid, FALSE if socket handle is invalid
 */
bool Tcp_isValidSocket(Tcp_Socket_T const socket);


/**
 * This function checks if the given TCP server socket is a valid one.
 *
 * @param[in] listener
 * Handle to the TCP server socket to be checked
 *
 * @return
 * TRUE if server socket handle is valid, FALSE if listener handle is invalid
 */
bool Tcp_isValidListener(Tcp_Listener_T const listener);


/**
 * This function checks if the connection for a given TCP socket is established.
 *
 * @param[in] socket
 * Handle to the TCP socket to be checked
 *
 * @return
 * TRUE if socket is connected, FALSE if socket is not connected.
 *
 * @deprecated
 * Use Tcp_getSocketStatus() instead!
 */
bool Tcp_isConnected(Tcp_Socket_T const socket);


/**
 * This function is used to check if there are pending errors on a given TCP
 * socket.
 *
 * @param[in] socket
 * Handle to the TCP socket to be checked
 *
 * @param[out] error_ptr
 * Pointer to error value
 *
 * @return
 * RC_OK if error value is successfully stored in given error pointer<br>
 * RC_TCP_INVALID_SOCKET<br>
 */
retcode_t Tcp_getSocketError(Tcp_Socket_T const socket, int *error_ptr);


/**
 * This function should be used to un-initialize a listener socket handle. It
 * returns the handle of an invalid listener socket. The call
 * Tcp_isValidListener(Tcp_getInvalidListener()) results in FALSE.
 *
 * @return
 * A handle of an invalid listener socket. Accessing this socket for reading or
 * writing as well as for communication would fail.
 *
 * @see
 * Tcp_isValidListener()
 */
Tcp_Listener_T Tcp_getInvalidListener(void);


/**
 * This function should be used to un-initialize a socket handle. It returns
 * the handle of an invalid socket. The call
 * Tcp_isValidSocket(Tcp_getInvalidSocket()) results in FALSE.
 *
 * @return
 * A handle of an invalid socket. Accessing this socket for reading or writing
 * as well as for communication would fail.
 *
 * @see
 * Tcp_isValidSocket()
 */
Tcp_Socket_T Tcp_getInvalidSocket(void);


/**
 * Prepares the socket and the sending context for sending. This function
 * must be called before invoking Tcp_send(). It ensures that the socket is
 * ready for sending. It also allocates a communication buffer for serializing
 * the message packets. This means that sendingCtx_ptr->buffer must be a valid
 * buffer.
 *
 * The allocated buffer is used for serialization of the message.
 * It needs to be valid until either
 * a) Tcp_send has been invoked with this buffer after which the ownership
 *    of the buffer passes back to the PAL implementation, or
 * b) CommBuff_free has been called on this buffer to indicate that sending
 *    has been aborted due to a serialization error.
 *
 * @param[in] socket
 * The socket over which the message should be sent.
 *
 * @param[in] sendingCtx_ptr
 * A pointer to the context structure of the sending job.
 *
 * @return
 * RC_OK on success.
 * RC_TCP_OUT_OF_MEMORY or RC_TCP_SOCKET_BUSY to indicate the respective transient
 * problems and the stack should retry sending using Tcp_retrySendingLater.
 * Any other IP, TCP or DTLS return code indicates an error that will cause
 * the stack to abort sending.
 *
 * @see
 * MsgSendingCtx_T, Tcp_retrySendingLater()
 */
retcode_t Tcp_prepareForSending(
    Tcp_Socket_T const socket, MsgSendingCtx_T *sendingCtx_ptr);


/**
 * This function should schedule the given sending job to be carried out later.
 * This means that this function takes care to invoke the corresponding sending
 * function, i.e., sendingCtx_ptr->sendingFunc, at some point later.
 * Any other return code except RC_OK means that sending cannot be completed by
 * by lower layer at given time and should be dropped.
 *
 * It is up to the PAL implementation when to execute the function.
 * However, the execution MUST be asynchronous, i.e. the caller of this function
 * can remove itself from the execution stack and is not part of the execution
 * stack when sendingCtx_ptr->sendingFunc is executed.
 *
 * An implementation of this function is free to decide whether to call
 * sendingCtx_ptr->sending function after an interval or when the socket is able
 * to send again. In the latter case, however, calling this function on a socket
 * that is not busy, MUST nevertheless trigger execution of sendingCtx_ptr->sendingFunc.
 *
 * Finally, on multi-threaded implementations of the PAL, the execution of
 * sendingCtx_ptr->sendingFunc is expected to happen in the same context that normal
 * network operations take place in.
 *
 * Implementation note:
 *   The interface currently does not provide a meaningful way for aborting a
 *   retry. This can occur if the message times out while waiting for transmission
 *   and is thus deleted. This in turn will result in the MsgSendingCtx_T structure
 *   to have been cleared. Thus an implementation must check the validity of the
 *   sending function in sendingCtx_ptr->sendingFunc before calling it.
 *
 * @param[in] socket
 * The socket over which the message should be sent.
 *
 * @param[in] sendingCtx_ptr
 * A pointer to the context structure of the sending job.
 *
 * @return
 * RC_OK on success, or a TCP or an IP error otherwise.
 *
 * @see
 * MsgSendingCtx_T, Tcp_retrySendingLater()
 */
retcode_t Tcp_retrySendingLater(
    Tcp_Socket_T const socket, MsgSendingCtx_T *sendingCtx_ptr);


#if SERVAL_ENABLE_TLS_SERVER

/**
 * This function creates a new TLS TCP listener socket, binds it to the given
 * port number and causes the socket to enter the listening state.
 * After invoking this function the socket is ready to accept incoming
 * secure connections.
 *
 * If the system is configured to use pre-shared keys, Tls_setServerPskCallback()
 * MUST be called in order to provide keying material to the implementation via
 * the supplied callback.
 *
 * When a connection request comes in, the given callback is invoked.
 * Before the callback function returns, Tcp_accept() must be called in order
 * to accept the incoming connection.
 *
 * The callback function is also invoked on errors. The followings error codes
 * can be passed to the callback function as a status parameter:
 * <ul>
 * <li> RC_OK: an incoming connection request to be accepted</li>
 * <li> RC_TCP_SOCKET_ERROR: any other not further specified socket error</li>
 * </ul>
 *
 * Note: This function is only used by a TCP server.
 *
 * Note: This function encompasses following Berkeley socket API calls:
 * socket(), bind(), listen()
 *
 * @param[in] port
 * TCP port to listen at, in network byte order.
 *
 * @param[in] callback_ptr
 * Callback function which is invoked on incoming connection
 * requests. It must be valid and assigned.
 *
 * @param[out] listener_ptr
 * On success, a TCP listener handle is given back. On failure,
 * an invalid socket handle is given back.
 *
 * @return
 * RC_OK: success<br>
 * RC_TCP_PORT_ALREADY_USED: the given port is already in use<br>
 * RC_TCP_NO_FREE_PORT: the limit of supported number of ports is reached in TCP
 * <br>
 * RC_TCP_SOCKET_ERROR: unspecified socket error<br>
 *
 * @see
 * Tcp_isValidListener(), Tcp_accept(), Callable_T, Callable_isAssigned(),
 * Tls_setServerPskCallback()
 */
retcode_t Tcp_listenSecure(Ip_Port_T port, Callable_T *callback_ptr,
                           Tcp_Listener_T *listener_ptr);

#endif /* SERVAL_ENABLE_TLS_SERVER */

#if SERVAL_ENABLE_TLS_CLIENT
/**
 * This function causes to open a TLS TCP connection to the given host
 * (IP address, port).
 * It creates a new secure TCP socket and causes an attempt to
 * establish a new TLS TCP connection (handshake) to the given IP address and
 * port.
 * After finishing the connection establishment, data can be send and received
 * via the returned TCP socket. <br>
 *
 * After this function returns the connection has not necessarily been entirely
 * established. The function Tcp_prepareForSending() will return
 * RC_TCP_SOCKET_BUSY as long as the connection has not been established.
 *
 * If the system is configured to use pre-shared keys, Tls_setClientPskCallback()
 * MUST be called in order to provide keying material to the implementation via
 * the supplied callback.
 *
 * When some data is received, the registered callback is invoked. Before the
 * return of the callback, Tcp_receive() must be received in order to receive
 * the incoming packet. The received CommBuff_T element is not guaranteed to
 * stay available after the return of the callback. If the received packet is
 * needed for further processing after the return of the callback, then the
 * function CommBuff_realloc() should be used in order to allocate a buffer
 * which is kept until it is explicitly freed using the function
 * CommBuff_free().<br>
 *
 * The callback is also invoked to signalize errors or other issues at the
 * socket. Possible codes are:
 * <ul>
 * <li> RC_OK: incoming data to be received</li>
 * <li> RC_TCP_CONNECTION_RESET: RESET received from peer</li>
 * <li> RC_TCP_CONNECTION_ABORT: connection aborted</li>
 * <li> RC_TCP_TIMEOUT: timeout, connection could not be established</li>
 * <li> RC_TCP_OUT_OF_MEMORY: no memory to perform socket operations </li>
 * <li> RC_IP_ROUTING_ERR: IP address could not be resolved </li>
 * <li> RC_TCP_SOCKET_ERROR: any other not further specified socket error </li>
 * </ul>
 *
 * Note: This function is only used by a TCP client.
 *
 * Note: This function encompasses following Berkeley socket API calls:
 * socket(), connect()
 *
 * @param[in] ipAddr_ptr
 * Pointer to IP address of the remote host (server) to connect to
 *
 * @param[in] port
 * TCP port on remote host to connect to (server port), in network byte order.
 *
 * @param[in] callback_ptr
 * Pointer to callback function which is invoked if data is received on given
 * socket.
 *
 * The callback function should lead to calling Tcp_receive() in order to
 * read the incoming data.
 * It has to be a valid pointer.
 *
 * @param[out] socket_ptr
 * On success, a TCP socket handle is returned. On failure, an invalid socket
 * handle is returned.
 *
 * @return
 * RC_OK: success <br>
 * RC_TCP_OUT_OF_MEMORY: no memory to perform socket operations<br>
 * RC_IP_ROUTING_ERR: IP address could not be resolved<br>
 * RC_TCP_CONNECTION_ABORT: connection could not be established
 *
 * @see
 * Tcp_isValidSocket(), Tcp_send(), Tcp_receive(), Tls_setClientPskCallback()
 */
retcode_t Tcp_connectSecure(Ip_Address_T *ipAddr_ptr, Ip_Port_T port,
                            Callable_T *callback_ptr, Tcp_Socket_T *socket_ptr);
#endif /* SERVAL_ENABLE_TLS_CLIENT */

#endif /* SERVAL_TCP_H_ */
