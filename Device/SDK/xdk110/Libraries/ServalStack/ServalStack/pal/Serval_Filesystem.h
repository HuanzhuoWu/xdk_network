/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_Filesystem.h
 * @brief An interface to the platform filesystem.
 *
 * This interface provides a simple flat read-only filesystem. 'Flat' means
 * there are no directories, there is only a set of files in the root. A file
 * represents data stored on flash or external storage.
 *
 * The interface provides functions to open, close and read a file. It provides
 * further functions to retrieve the length of a file and to check if a file
 * handler is a valid one.
 *
 */

#ifndef SERVAL_FILESYSTEM_H_
#define SERVAL_FILESYSTEM_H_

#include <Serval_Types.h>


/**
 * Function wrapper to open a file (read-only). Before invoking this function a
 * directory with a file with the given name should exist. After invoking this
 * function, if successful, the given file handler can be used to read and
 * close the file.
 *
 * @param[in] name_ptr
 * Name of the file to open. A file with the given name has to exist.
 *
 * @param[out] handle_ptr
 * Handle to the file.
 * Invalid file handle if the file cannot be opened for any reason, for example
 * if the file does not exist.
 *
 * @return
 * RC_OK if the file handle has been successfully retrieved<br>
 * RC_FILES_TOO_MANY_OPEN if already too many files are open<br>
 * RC_FILES_NOT_EXIST if file does not exist<br>
 * RC_FILES_NO_SYSTEM if no filesystem or filesystem is broken<br>
 * RC_FILES_BAD_NAME if filename not valid<br>
 * RC_FILES_ACCESS_ERROR access to file failed<br>
 *
 */
retcode_t Filesystem_open(char const *name_ptr,
                          FileHandle_T *handle_ptr);

/**
 * This function is called to check whether a given file handle is valid or
 * not and return the result.
 *
 * @param[in] handle
 * Handle to the file needed to be checked.
 *
 * @return
 * TRUE if file handler is valid, FALSE if file handler is invalid.
 */
bool Filesystem_isValidHandle(FileHandle_T handle);

/**
 * Function wrapper to close a file. After invoking this function the file is
 * closed and read access is not possible with given file handle.
 *
 * @param[in] handle
 * Handler to the file needed to be closed. It has to be valid.
 *
 * @return
 * RC_OK if the file has been successfully closed<br>
 * RC_FILES_BAD_NAME if filename not valid<br>
 * RC_FILES_ACCESS_ERROR access to file failed<br>
 * RC_FILES_BAD_DSCR handle is not valid
 */
retcode_t Filesystem_close(FileHandle_T handle);

/**
 * This function is called to fetch the size of a file, whose handle is given.
 *
 * @param[in] fh
 * Handle to the file whose size is required. A valid file handler is
 * required before calling this function. The file must be open.
 *
 * @return
 * Size of the file
 *
 * @see
 * Filesystem_open()
 */
unsigned int Filesystem_getLength(FileHandle_T fh);

/**
 * Function wrapper to read from a file. This function when called reads up to
 * given count bytes from the given file handle fh (starting at the given
 * offset) into the given buffer reference.
 *
 * @param[in] fh
 * Handler to the file needed to be read. A valid file handler is required
 * before calling this function. The file must be open.
 *
 * @param[out] buffer_ptr
 * Reference to a buffer where read data will be saved
 *
 * @param[in] count
 * Number of bytes to be read
 *
 * @param[in] offset
 * The position to start reading from the given file handle
 *
 * @param[out] readbytes_ptr
 * Number of bytes read
 *
 * @return
 * RC_OK if read operation was successful<br>
 * RC_FILES_ACCESS_ERROR access to file failed<br>
 * RC_FILES_BAD_DSCR handle is not valid
 *
 * @see
 * Filesystem_open()
 *
 */
retcode_t Filesystem_read(
    FileHandle_T fh, char *buffer_ptr, unsigned int count,
    unsigned int offset, unsigned int *readbytes_ptr);

#endif /* SERVAL_FILESYSTEM_H_ */
