/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file  Serval_CommBuff.h
 * @brief An interface to the platform communication buffer management.
 *
 * PAL provides a data structure CommBuff_T to allow working with
 * communication buffers. A communication buffer here means, a buffer which is
 * designed to store communication packets and to pass them between the
 * communication components. This interface provides functions to allocate,
 * reallocate and free buffers.
 * It also offers functions to set and to get the size of a given buffer and a
 * function to get the payload which is stored in a given buffer.
 *
 *
 * @see CommBuff_T
 */

#ifndef SERVAL_COMMBUFFER_H_
#define SERVAL_COMMBUFFER_H_

#include <Serval_Ip.h>

/**
 * This function should be used to un-initialize a buffer handle. It returns
 * the handle of an invalid buffer. The call
 * CommBuff_isValid(CommBuff_getInvalidBuffer()) must results in FALSE.
 *
 * @return
 * A handle of an invalid buffer. Accessing this buffer for reading or writing
 * would fail.
 *
 * @see CommBuff_isValid()
 */
CommBuff_T CommBuff_getInvalidBuffer(void);


/**
 * This function is called to check whether a given buffer handle is valid or
 * not and return the result.
 *
 * @param[in] buffer
 * The buffer handle to be checked.
 *
 * @return TRUE if the buffer handle is valid, or FALSE otherwise.
 */
bool CommBuff_isValid(CommBuff_T buffer);


/**
 * This function allocates a communication buffer and returns its handle.
 * The size of the allocated buffer is equal to or larger than the required
 * size. The new buffer stays allocated until CommBuff_free() is applied. If
 * successful, a call to CommBuff_getSize(), immediately after the function
 * returns, must provide correct size of thus allocated communication buffer
 * and a call to CommBuff_getLength(), immediately after the function returns,
 * must return zero.
 *
 * @param[in] size
 * The size of the required buffer. The size has to be larger than 0.
 *
 * @return
 * The handle of the allocated buffer. If the allocation fails, for instance
 * because not enough space is available, then an invalid buffer handle is
 * returned.
 *
 *@deprecated
 * This function is not used by the stack anymore and should not be used by any
 * new protocol. The PAL functions for receiving and preparing a socket for
 * sending provide the upper-layer protocols with the needed CommBuff_T object.
 *
 * @see CommBuff_free(), Udp_receive(), Tcp_receive(), Udp_prepareForSending(),
 * Tcp_prepareForSending()
 */
CommBuff_T CommBuff_alloc(unsigned int size);


/**
 * This function is usually used by the upper-layer if it needs either to keep a
 * received packet after the return of the socket callback (i.e., the callback
 * function passed to Udp_listen(), Udp_connect(), Tcp_accept() or
 * Tcp_connect()) or to change the size of a previously allocated buffer.
 * A Buffer, which has been allocated using this function, must be explicitly
 * freed using the function CommBuff_free().
 *
 * This function should provide buffers which can be allocated for longer time
 * without interfering with the communication reliability.
 *
 * The provided buffer is not necessarily a new one i.e. the handle to the
 * buffer may not change. It can be the same old buffer, if it is large enough
 * and can be allocated for longer time. PAL decides in each case to allocate a
 * new buffer, to extend the old one or even to return the old buffer without a
 * modification. If a new buffer is allocated, then the old one is implicitly
 * freed after its content is copied to the new buffer, i.e. a call to
 * CommBuff_getLength(), should return same value before and after the function
 * is called.
 *
 * @param[in] oldBuffer
 * The handle of the buffer which should be reallocated. It must be valid and
 * buffer has to be allocated before calling this function.
 *
 * @param[in] newSize
 * The new required size of the buffer. It has to be larger than 0.
 *
 * @return
 * The handle of a buffer with the given size. Allocating this
 * buffer for longer time is allowed and does not interfere with the
 * communication. If the re-allocation fails, an invalid buffer handle is
 * returned. In this case the oldBuffer will neither released nor changed.
 */
CommBuff_T CommBuff_realloc(CommBuff_T oldBuffer, unsigned int newSize);

/**
 * This function frees the memory of the given buffer. After invoking this
 * function the given buffer handle is invalid and must not be used without
 * allocation with CommBuff_alloc(). The buffer thus freed will be again
 * available for allocation. If this function is applied to a CommBuff_T which
 * is invalid or already free, then it should do nothing.
 *
 * @param[in] buf
 * The handle of the buffer to be freed. It has to be valid. If the
 * corresponding buffer is already free, then this function will not have an
 * effect.
 */
void CommBuff_free(CommBuff_T buf);

/**
 * This function returns the size of the given buffer. The size of the buffer
 * is its maximal capacity.
 *
 * @param[in] buf
 * The handle of the buffer to get the size of. The handle has to be valid and
 * the corresponding buffer has to be allocated before calling this
 * function.
 *
 * @return
 * The size of buffer. The size of the buffer is its maximal capacity. Thus it
 * can be larger than the buffer length.
 */
unsigned int CommBuff_getSize(CommBuff_T buf);

/**
 * This function returns the length of the given buffer. The length of the
 * buffer gives the size of the valid data in the payload of the buffer. Length
 * of a buffer must never be more the the size of the buffer.
 *
 * @param[in] buf
 * The handle of the buffer to get the length of. The handle has to be valid and
 * the corresponding buffer has to be allocated before calling this
 * function.
 *
 * @return
 * The length of buffer. The length of the used part of the given buffer.
 */
unsigned int CommBuff_getLength(CommBuff_T buf);

/**
 * This function sets the length of a given buffer. After calling this function
 * the buffer length is set to the given value. After the following code x will
 * be equal to y.
 * <code>
 * CommBuff_setLength(buf, x);
 * y = CommBuff_getLength(buf)
 * </code>
 *
 * @param[in] buf
 * The handle of the buffer to set the length of. The handle has to be valid and
 * the corresponding buffer has to be allocated before calling this
 * function.
 *
 * @param[in] len
 * Length to be set. The length must not be larger than the actual size of the
 * given buffer.
 */
void CommBuff_setLength(CommBuff_T buf, unsigned int len);

/**
 * This function returns a pointer to the payload of the given buffer. This
 * pointer can be used to read the payload which is stored in the buffer or to
 * write a new payload into the buffer.
 *
 * @param[in] buf
 * The handle of the buffer to get the payload of. The handle has to be valid
 * and the corresponding buffer has to be allocated before calling this
 * function.
 *
 * @return
 * A pointer to the payload of the given buffer
 */
char* CommBuff_getPayload(CommBuff_T buf);

#endif /* SERVAL_COMMBUFFER_H_ */
