##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

#Build chain settings
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra \
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections \
-Wa,-ahlms=$(@:.o=.lst)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g -DDEBUG
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) -Wno-extra

CFLAGS_DEBUG = $(BCDS_CFLAGS_DEBUG_COMMON) $(CFLAGS_COMMON)
CFLAGS_RELEASE = $(BCDS_CFLAGS_RELEASE_COMMON) $(CFLAGS_COMMON)

ARFLAGS = -cr

BCDS_BSTLIB_SOURCE_FILES = \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMA2x2_driver/bma2x2.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BME280_driver/bme280.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMG160_driver/bmg160.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMI160_driver/bmi160.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMM050_driver/bmm050.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMP180_driver/bmp180.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BMP280_driver/bmp280.c \
	$(BCDS_BSTLIB_SOURCE_PATH)/BNO055_driver/bno055.c

# Objects
BCDS_BSTLIB_SOURCE_OBJECT_FILES = \
	$(patsubst $(BCDS_BSTLIB_SOURCE_PATH)/%.c, %.o, $(BCDS_BSTLIB_SOURCE_FILES))

BCDS_BSTLIB_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_BSTLIB_DEBUG_OBJECT_PATH)/, $(BCDS_BSTLIB_SOURCE_OBJECT_FILES)) 
	
BCDS_BSTLIB_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_BSTLIB_RELEASE_OBJECT_PATH)/, $(BCDS_BSTLIB_SOURCE_OBJECT_FILES)) 
	
##################### Build Targets #########################
debug: $(BCDS_BSTLIB_DEBUG_LIB)	
release: $(BCDS_BSTLIB_RELEASE_LIB)
	
$(BCDS_BSTLIB_DEBUG_LIB): $(BCDS_BSTLIB_DEBUG_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BSTLIB for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BSTLIB_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_BSTLIB_RELEASE_LIB): $(BCDS_BSTLIB_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BSTLIB for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BSTLIB_RELEASE_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_BSTLIB_DEBUG_OBJECT_PATH)/%.o: $(BCDS_BSTLIB_SOURCE_PATH)/%.c
	@echo Build $?
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
        -I . $(BCDS_BSTLIB_INCLUDES) $< -o $@

$(BCDS_BSTLIB_RELEASE_OBJECT_PATH)/%.o: $(BCDS_BSTLIB_SOURCE_PATH)/%.c
	@echo Build $?
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
		-I . $(BCDS_BSTLIB_INCLUDES) $< -o $@
		
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_BSTLIB_DEBUG_PATH) $(BCDS_BSTLIB_RELEASE_PATH)

diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_BSTLIB_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_BSTLIB_DEBUG_OBJECT_PATH)
	@echo $(BCDS_BSTLIB_DEBUG_OBJECT_FILES)
