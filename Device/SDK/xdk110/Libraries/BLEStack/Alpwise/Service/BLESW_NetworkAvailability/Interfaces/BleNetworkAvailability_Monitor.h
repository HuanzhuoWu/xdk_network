#ifndef __BLENETWORK_MONITOR_H
#define __BLENETWORK_MONITOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleNetworkAvailability_Monitor.h
 *
 * Description:   Contains interfaces definition for Network Availability 
 *				  Profile when the local device operates in the Monitor role.
 * 
 * Created:       July, 2013
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/
/**
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR
 * Defines if the BLE NETWORK AVAILABILITY Profile implementation supports the 
 * MONITOR Role.
 * If enabled ( set to 1 ) it enables all the following BleNetworkAvailability
 * MONITOR configuration and Application Interface
 * 
 * The default value for this option is disabled (0). 
 */
#ifndef BLENETWORKAVAILABILITY_SUPPORT_MONITOR
#define BLENETWORKAVAILABILITY_SUPPORT_MONITOR							0
#endif //BLENETWORKAVAILABILITY_SUPPORT_MONITOR

#if (BLENETWORKAVAILABILITY_SUPPORT_MONITOR == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/
/**
 * BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION	
 * Defines if the BLENETWORKAVAILABILITYMONITOR Profile supports configuring 
 * the remote REPORTER to deliver the New NETWORK AVAILABILITY 
 * when it is adjusted.
 * 
 * When enabled (set to 1), once connected and linked the MONITOR can call
 * BLENETWORK_MONITOR_SubscribeNewNetworkAvailabilityNotification() to
 * subscribe to the remote REPORTER notification to the MONITOR when 
 * the NETWORK AVAILABILITY has changed, in this case 
 * the BLENETWORKAVAILABILITYMONITOR_EVENT_NETWORKAVAILABILITY event 
 * is received by the MONITOR upon change.
 * 
 * The MONITOR can stop the NETWORK AVAILABILITY change notification 
 * subscription by calling
 * BLENETWORK_MONITOR_UnsubscribeNewNetworkNotification().
 * 
 * Note that when bonded, the subcription state is saved into the 
 * remote SERVER persistent memory and restored upon reconnexion.
 *
 * The default value for this option is enabled (1). 
 */
#ifndef BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION	
#define BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION			1
#endif //BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION


#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when Network Monitor Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when Network Monitor Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when Network Monitor Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when Network Monitor Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when Network Monitor Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)

#if (BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when Network AvailabilityMonitor Role is enabled\
and BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION is enabled
#endif //(BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION ==1) && (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when Network Availability Monitor Role is enabled\
	and BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION is enabled
#endif //(BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION ==1) && (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION

 * Defines if the BLE NETWORK AVAILABILITY Profile implementaton in 
 * MONITOR role supports saving the NETWORK AVAILABIITY REPORTER information
 * when both devices are bonded. 

 * When enabled (set to 1), once connected and bonded the MONITOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the REPORTER information.

 * Upon reconnection with the NETWORK AVAILABILITY REPORTER,
 * as this information is already known when the linkUp process can be 
 * simplified.
 * It saves 5 bytes into persistent memory.
 *
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is enabled (0). 
 */
#ifndef BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION	
#define BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_NETWORK_NETWORKAVAILABILITYHANDLE		                    0X67    //2 bytes
#define BLEINFOTYPE_NETWORK_NETWORKAVAILABILITYCONFIGHANDLE					    0x68	//2 bytes
#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported force 
// the BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION
#define BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION				0
#endif //( BLE_SECURITY == 0) 

/***************************************************************************\
 *	Type definition
\***************************************************************************/
/*BleNetworkAvailabilityServiceNetworkAvailability  type 
* Define the different kind of network value that could be received by Monitor
* There are two values: 
*  0 : Network is not available
*  1 : Network is available
*/
typedef U8 BleNetworkAvailabilityServiceNetworkAvailability;

/** BLENETWORKAVAILABILITYSERVICE_NETWORKAVAILABILITY_NONETWORKAVAILABLE  
 * The network value is 0, so there is no network available
 */
#define BLENETWORKAVAILABILITYSERVICE_NETWORKAVAILABILITY_NONETWORKAVAILABLE              0

/** BLENETWROKAVAILABILITYSERVICE_NETWORKAVAILABILITY_ONEORMORENETWORKAVAILABLE 
 * The network value is 1, it means the network is available.
 */
#define BLENETWROKAVAILABILITYSERVICE_NETWORKAVAILABILITY_ONEORMORENETWORKAVAILABLE       1

/** 
 * BleNetworkAvailabilityMonitorEvent type
 * Define the different kind of events that could be received by the 
 * BleNetworkAvailabilityMonitorCallBack
 */
typedef	U8	BleNetworkAvailabilityMonitorEvent;

/** BLENETWORK_MONITOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleNetworkAvailabilityMonitorLinkUpInfo pointer.
 */
#define BLENETWORKAVAILABILITYMONITOR_EVENT_LINKED									0xCA

/** BLENETWORKAVAILABILITY_MONITOR_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleNetworkAvailabilityMonitorLinkUpInfo pointer.
 */
#define BLENETWORKAVAILABILITY_MONITOR_EVENT_UNLINKED								0xCB

/** BLENETWORKAVAILABILITY_MONITOR_EVENT_NETWORKAVAILABILITY	
 * Event received when a network availability value has been received.
 * The status field indicates the status of the operation.
 * The parms field indicates the network availability information within a
 * BleNetworkAvailabilityMonitorLinkUpInfo pointer.
 */
#define BLENETWORKAVAILABILITY_MONITOR_EVENT_NETWORKAVAILABILITY					0xCC

/** BLENETWORKAVAILABILITY_MONITOR_EVENT_SUBSCRIPTION_RSP
 * Event received when the Subscription of new network availability 
 * notification is completed. The status field indicates the status 
 * of the operation.
 * Only received if 
 * BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION is enabled (Set to 1)
 * The parms field indicates the monitor for which the subscription
 * response is associated within a 
 * BleNetworkAvailabilityMonitorLinkUpInfo pointer.
 */
#define BLENETWORKAVAILABILITY_MONITOR_EVENT_SUBSCRIPTION_RSP						0xCD

/** BLENETWORK_MONITOR_EVENT_UNSUBSCRIPTION_RSP
 * Event received when the Un-Subscription of new network availability 
 * notification is completed. The status field indicates the status 
 * of the operation.
 * Only received if 
 * BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION is enabled (Set to 1)
 * The parms field indicates the monitor for which the unsubscription
 * response is associated within a 
 * BleNetworkAvailabilityMonitorLinkUpInfo pointer.
 */
#define BLENETWORKAVAILABILITY_MONITOR_EVENT_UNSUBSCRIPTION_RSP						0xCE

/**
 * BleNetworkAvailabilityMonitorCallBack
 *	This callback receives the BLE NETWORK MONITOR profile events. 
 *  Each of these events can be associated with a defined status and parameter
 *	The callback is executed during the stack context,  be careful to not 
 * doing heavy process in this function.
 */
typedef void (*BleNetworkAvailabilityMonitorCallBack)( 
				  BleNetworkAvailabilityMonitorEvent	 event,
				  BleStatus				            	 status,
				  void*						             parms); 

/** 
 * BleNetworkAvailabilityMonitor type
 * Define a local MONITOR, it represents a memory block
 * that should be passed by the application during the 
 * BLENETWORKAVAILABILITY_MONITOR_LinkUpNetworkAvailabilityProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLENETWORKAVAILABILITY_MONITOR_LinkUpNetworkAvailabilityProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * MONITOR receive the BLENETWORKAVAILABILITYMONITOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this MONITOR memory block while 
 * linked with a remote REPORTER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local MONITOR could be then passed as parameter to 
 * profile API in order to identify to which linked REPORTER the operation  
 * is destinated.
 *
 * A MONITOR is unique per remote NETWORK AVAILABILITY REPORTER, it means 
 * that if this local device is connected to multiple remote REPORTERS the 
 * application shall register multiple MONITORs.
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;

	AttHandle				networkAvailabilityServiceStartingHandle;
	AttHandle				networkAvailabilityServiceEndingHandle;
	AttHandle				networkAvailabilityHandle;
    // Notify is optionnal
#if (BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1) 
	AttHandle				networkAvailabilityConfigHandle;
    U8						value[2];
#endif //(BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1) 

	U8						reporterFeatures;

} BleNetworkAvailabilityMonitor;

/** 
 * BleNetworkAvailabilityMonitorLinkUpInfo type
 * Structure passed in parameter in the BleNetworkAvailabilityMonitorCallBack
 * parms fields during the BLENetworkAvailabilityMonitor_EVENT_LINKED event.
 * It indicates :
 * bleNetworkAvailabilityMonitor : The MONITOR from which the event is comming
 * connHandle : The connection handle from which the Link Up information
 * is coming
 * Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleNetworkAvailabilityMonitor					*bleNetworkAvailabilityMonitor;
	U16									            connHandle;
} BleNetworkAvailabilityMonitorLinkUpInfo;

/** 
 * BleNetworkAvailabilityMonitorNetworkInfo type
 * Structure passed in parameter in the BleNetworkAvailabilityMonitorCallBack
 * parms fields during the 
 * BLENETWORKAVAILABILITYMONITOR_EVENT_NETWORKAVAILABILITY event.
 * It indicates :
 * bleNetworkAvailabilityMonitor : The monitor from which the event is comming
 * connHandle : The connection handle from which the network availability 
 * information is coming
 * networkValue: the network availability information provided by the REPORTER;
 *                Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleNetworkAvailabilityMonitor						*bleNetworkAvailabilityMonitor;
	U16									            	connHandle;
	BleNetworkAvailabilityServiceNetworkAvailability    networkValue;
} BleNetworkAvailabilityMonitorNetworkInfo;

/** 
 * BleNetworkAvailabilityMonitorLinkMode type
 * Type passed during the 
 * BLENETWORKAVAILABILITY_MONITOR_LinkUpNetworAvailabilityProfile
 * API call.

 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLENETWORKAVAILABILITY_MONITOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.

 * Note that if BLENetworkAvailabilityMonitor_SUPPORT_SAVINGINFORMATION is 
 * DISABLED, a linkUP with the BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE 
 mode will always failed.
 */
typedef U8 BleNetworkAvailabilityMonitorLinkMode;

#define BLENETWORKAVAILABILITY_MONITOR_LINKUP_COMPLETE					1
#define BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE					2


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/
/** Init the Network Availability MONITOR profile
 *
 * BLENETWORKAVAILABILITY_MONITOR_Init()
 *	This function is the entry point of the NETWORK MONITOR profile, it 
 *	inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled
 *
 * @param callback: The BleNetworkAvailabilityMonitorCallBack in which 
 * the Monitors events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLENETWORKAVAILABILITYMONITOR Profile is already 
 *		initialized).
 *
 * @author My Huong NHAN
 */ 
BleStatus BLENETWORKAVAILABILITY_MONITOR_Init(
			BleNetworkAvailabilityMonitorCallBack callback);

/** De-Init the Network Availability MONITOR Profile
 *
 * BLENETWORKAVAILABILITY_MONITOR_Deinit
 *	This function is used to De-Init the NETWORK MONITOR Role of 
 * the Network Availability Profile.
 *
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_Deinit( void );

/** Link Up the NETWORK AVAILABILITY Profile with the specified remote Device
 *
 * BLENETWORKAVAILABILITY_MONITOR_LinkUpNetworkAvailabilityProfile()
 * This function is used to link-Up the NETWORK AVAILABILITY profile with the 
 * specified remote device.
 * It will try to discover the NETWORK AVAILABILITY profile in the remote 
 * device using the given MONITOR.
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enable

 * @param bleNetworkAvailabilityMonitor: A valid BleNetworkAvailabilityMonitor
 * to link-up with a remote NETWORK AVAILABILITY REPORTER.

 * This memory block is allocated by the application prior to call this API 
 * and shall stay valid while the devices are linked together. 

 *  It is up to profile internal mechanism to manage this memory block.
 *  Once linked, this REPORTER could be then passed as parameter to profile
 API in order to identify to which linked MONITOR the operation is destinated.   
 * @param connHandle: The connection Handle on which the link up between this
 *	 bleNetworkAvailabilityMonitor and the remote NETWORK AVAILABILITY 
 REPORTER profile shall be done.
 * @param linkUpmode: The desired link-up mode, restore from persistent memory 
 *	or complete link-up.
 * @param info: Only valid if mode is 
 *  BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE,
 *	and return status is BLESTATUS_SUCCESS.
 *	It reports the link-Up information .
 *	It may be equal to (BleNetworkAvailabilityMonitorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *	the BLENETWORKAVAILABILITY_MONITOR_EVENT_LINKED event will be received 
 *  in the callback with the status of the operation. This status is returned 
 *  only if mode is BLENETWORKAVAILABILITYMONITOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 * This status is returned only if mode is 
 * BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE. It means that the 
 * persistent information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 * is BLENETWORKAVAILABILITY_MONITOR_LINKUP_RESTORE, it means that 
 * BLENETWORKAVAILABILITYMONITOR_SUPPORT_SAVINGINFORMATION is not enabled 
 * or if enabled that the remote device is not bonded or the persistent memory 
 * does not contains enough information about the remote NETWORK REPORTER.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 * another NETWORK AVAILABILITY Profile Operation is in Progress, wait for this
 * operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *	an invalid parameter (connHandle does not match with a specified link).
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_LinkUpNetworkAvailabilityProfile(
								BleNetworkAvailabilityMonitor               *monitor,
								U16                                         connHandle,
								BleNetworkAvailabilityMonitorLinkMode	    linkUpMode,
								BleNetworkAvailabilityMonitorLinkUpInfo	    *info );


/** Un Link the Network Availability Profile with the specified linked MONITOR
 *
 * BLENETWORKAVAILABILITY_MONITOR_UnLinkNetworkAvailabilityProfile()
 * This function is used to UnLink the Network Availability Profile with the 
 * specified previously linked MONITOR.
 * This MONITOR will not receive any more information from the linked 
 * REPORTER. 
 *
 * The Bonded information may also been removed from the 
 * persistent memory by the application removing any 
 * BLEINFOTYPE_NETWORKAVAILABILITYMONITOR_* from persistent memory
 *
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled
 *
 * @param bleNetworkAvailabilityMonitor: A linked Network Availability Monitor
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the MONITOR memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_UnLinkNetworkAvailabilityProfile(
					                    BleNetworkAvailabilityMonitor *monitor);

/** Get the network availability from a linked REPORTER
 *
 * BLENETWORKAVAILABILITY_MONITOR_GetNetworkAvailability()
 *	This function is used to Get the network availability on the remote
 * REPORTER defined by the previously linked MONITOR.
 *   
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled
 *
 * @param bleNetworkAvailabilityMonitor: A linked Network Monitor. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLENETWORKAVAILABILITYMONITOR_EVENT_NETWORKAVAILABILITY event will 
 * be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Network Availability Profile Operation is in Progress, wait
 * for this operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter 
 * (the Netowrk Availability Profile seems not been initialized).
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_GetNetworkAvailability(
					BleNetworkAvailabilityMonitor *monitor );

#if (BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1) 

/** Configure the NETWORK REPORTER to deliver the New Network Availability 
 * when it is adjusted
 *
 * BLENETWORK_MONITOR_SubscribeNewNetworkAvailabilityNotification()

 *	This function is used by the given linked NETWORK MONITOR to configure the  
 *  remote NETWORK REPORTER to deliver new Network Availability
 *  when it is changed.
 *
 *  When new Network Availability information is received the 
 *  BLENETWORKAVAILABILITYMONITOR_EVENT_NETWORKAVAILABILITY event will be 
 *  received in the callback with the adjusted network availability information
 *   
 * BLENETWORK_SUPPORT_MONITOR shall be enabled
 * BLENETWORKAVAILABILITYMONITOR_SUPPORT_RECEIVENEWNETWORKNOTIFICATION 
 * shall be enabled
 *
 * @param bleNetworkAvailabilityMonitor: A linked Network Monitor. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLENETWORKAVAILABILITY_MONITOR_EVENT_SUBSCRIPTION_RSP event will
 *      be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Network Availability Profile Operation is in Progress, wait 
 * for this operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *	an invalid parameter 
 * (the Network Availability Profile seems not been initialized).
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_SubscribeNewNetworkNotification(
									BleNetworkAvailabilityMonitor  *bleNetworkAvailabilityMonitor );


/** Configure the NETWORK AVAILABILITY REPORTER to not deliver the 
 * New Network Availability when it is modified
 *
 * BLENETWORKAVAILABILITY_MONITOR_UnSubscribeNewNetworkNotification()
 * This function is used by the given linked NETWORKAVAILABILITY MONITOR to
 * configure the remote NETWORKAVAILABILITY REPORTER to not deliver new 
 * Network Availability when it is changed.
 *   
 * BLENETWORKAVAILABILITY_SUPPORT_MONITOR shall be enabled
 * BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION shall 
 * be enabled
 *
 * @param bleNetworkAvailabilityMonitor: A linked Network Availability MONITOR. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLENETWORKAVAILABILITY_MONITOR_EVENT_UNSUBSCRIPTION_RSP event will 
 * be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other NETWORKAVAILABILITY Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *	an invalid parameter 
 *  (the NETWORKAVAILABILITY Profile seems not been initialized).
 *
 * @author My Huong NHAN
 */
BleStatus BLENETWORKAVAILABILITY_MONITOR_UnSubscribeNewNetworkNotification(
					BleNetworkAvailabilityMonitor  *bleNetworkAvailabilityMonitor );
#endif //(BLENETWORKAVAILABILITY_SUPPORT_RECEIVENEWNETWORKNOTIFICATION == 1)

#endif //(BLENETWORKAVAILABILITY_SUPPORT_MONITOR == 1)

#endif //__BLENETWORKAVAILABILITY_MONITOR_H
