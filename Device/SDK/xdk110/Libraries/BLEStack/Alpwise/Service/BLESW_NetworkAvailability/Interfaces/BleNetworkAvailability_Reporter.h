#ifndef __BLENETWORKAVAILABILITY_REPORTER_H
#define __BLENETWORKAVAILABILITY_REPORTER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleNetworkAvailability_Reporter.h
 *
 * Description:   Contains interfaces routines for NetworkAvailability Profile
 *					when the local device operates in the REPORTER role.
 * 
 * Created:       July, 2013
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"

/***************************************************************************\
 *	Macro and constant definition
\***************************************************************************/

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLENETWORKAVAILABILITY_SUPPORT_REPORTER
 * Defines if the BLE NETWORKAVAILABILITY Profile implementation supports the 
 * REPORETER Role

 * If enabled ( set to 1 ) it enables all the following BleNetworkAvailability
 * REPORTER configuration and Application Interface.
 * The default value for this option is disabled (0).
 */
#ifndef BLENETWORKAVAILABILITY_SUPPORT_REPORTER
#define BLENETWORKAVAILABILITY_SUPPORT_REPORTER								0
#endif //BLENETWORKAVAILABILITYY_SUPPORT_REPORTER

#if (BLENETWORKAVAILABILITY_SUPPORT_REPORTER == 1)

// Then check dependencies
#include "Services/BleNetworkAvailability_Service.h"

// Network Availability Service is mandatory
#if (BLE_SUPPORT_NETWORKAVAILABILITY_SERVICE == 0)
#error BLE_SUPPORT_NETWORKAVAILABILITY_SERVICE shall be enabled when BleNetworkAvailability REPORTER Role is enabled
#endif //(BLE_SUPPORT_NETWORKAVAILABILITY_SERVICE == 0)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/***************************************************************************\
 * CONSTANTS
\***************************************************************************/

/***************************************************************************\
 *	Type definition
\***************************************************************************/

/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/* Register a BLE NETWORKAVAILABILITY REPORTER.
 *
 * BLENETWORKAVAILABILITY_REPORTER_Register()
 * This function is used to register and initialise a BLE NETWORK 
 * REPORTER role.

 * It will add the supported Network Reporter characteristics into the Attribute
 * database.
 *
 * BLENETWORKAVAILABILITY_SUPPORT_REPORTER shall be enabled.
 */
BleStatus BLENETWORKAVAILABILITY_REPORTER_Register( );

#endif //(BLENETWORKAVAILABILITY_SUPPORT_REPORTER== 1)

#endif //__BLENETWORKAVAILABILITY_REPORTER_H
