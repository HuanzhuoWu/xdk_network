#ifndef __BLERUNNINGSPEEDANDCADENCE_SENSOR_H
#define __BLERUNNINGSPEEDANDCADENCE_SENSOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleRunningSpeedAndCadence_Sensor.h
 *
 * Description:   Contains interfaces definitions for Running Speed And Cadence 
 *					Profile when the local device operates in the SENSOR
 *					role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR
 * Defines if the BLE RUNNINGSPEEDANDCADENCE SENSOR Profile implementation supports the 
 * SENSOR Role
 * If enabled ( set to 1 ) it enables all the following BLE RUNNINGSPEEDANDCADENCE 
 * SENSOR Profile SENSOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR
#define BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR					0
#endif //BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR

#if (BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR== 1)
//
// Then check dependencies
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleRunningSpeedAndCadence_Service.h"


// Running Speed And Cadence Service is mandatory
#if (BLE_SUPPORT_RUNNINGSPEEDANDCADENCE_SERVICE == 0)
#error BLE_SUPPORT_RUNNINGSPEEDANDCADENCE_SERVICE shall be enabled when BleRunningSpeedAndCadence SENSOR Role is enabled
#endif //(BLE_SUPPORT_RUNNINGSPEEDANDCADENCE_SERVICE == 0)

// device Information Service is mandatory
#if (BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)
#error BLE_SUPPORT_DEVICEINFORMATION_SERVICE shall be enabled when BleRunningSpeedAndCadence SENSOR Role is enabled
#endif //(BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)

#if (BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER shall be enabled when BleRunningSpeedAndCadence SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME shall be enabled when BleRunningSpeedAndCadence SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 



/***************************************************************************\
 * OPTIONAL FEATURES FOR RUNNINGSPEEDANDCADENCE SENSOR PROFILE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE RUNNING SPEED AND CADENCE profile in SENSOR Role.
 *
 * BLERUNNINGSPEEDANDCADENCE_SENSOR_Register()
 *	This function is used to register and initialise a BLE  
 *  RUNNING SPEED AND CADENCE profile in SENSOR role.
 *  It will add the supported RUNNING SPEED AND CADENCE Service 
 *  characteristics and the supported DEVICE INFORMATION Service  
 *  characteristics into the Attribute database.
 *	This Interface shall be the entry point of a BLE 
 *  RUNNING SPEED AND CADENCE profile in SENSOR Role.
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR shall be enabled.
 *
 * @param callback : The function callback where the Running Speed and Cadence
 *  service event will be posted.
 *  As the only event supported is received when the total distance is updated
 *  this parameter is ignored and can be null, i.e. equal to 
 * (BleRunningSpeedAndCadenceServiceCallBack) 0, if 
 * BLE_RUNNINGSPEEDANDCADENCESERVICE_SUPPORT_TOTAL_DISTANCE is disabled (set to 0)
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLERUNNINGSPEEDANDCADENCE_SENSOR_Register( 
        BleRunningSpeedAndCadenceServiceCallBack callback );


/***************************************************************************\
 * OPTIONAL API functions definition RUNNINGSPEEDANDCADENCE SENSOR PROFILE
\***************************************************************************/

#endif //(BLERUNNINGSPEEDANDCADENCE_SUPPORT_SENSOR== 1)

#endif /*__BLERUNNINGSPEEDANDCADENCE_SENSOR_H*/
