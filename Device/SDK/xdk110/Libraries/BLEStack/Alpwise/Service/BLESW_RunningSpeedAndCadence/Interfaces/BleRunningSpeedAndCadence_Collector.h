#ifndef __BLERUNNINGSPEEDANDCADENCE_COLLECTOR_H
#define __BLERUNNINGSPEEDANDCADENCE_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleRunningSpeedAndCadence_Collector.h
 *
 * Description:   Contains interfaces definition for Running Speed And 
 *					Cadence Profile when the local device operates in 
 *					the COLLECTOR role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR
 * Defines if the BLE RUNNING SPEED AND CADENCE Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE RUNNING SPEED
 * AND CADENCE Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR
#define BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR							0
#endif //BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR

#if (BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when RUNNING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE RUNNING SPEED AND CADENCE Profile implementation in 
 * COLLECTOR role supports saving the RUNNING SPEED AND CADENCE SENSOR information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the RUNNING SPEED AND CADENCE SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and RSC 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * it saves 5 bytes into persistent memory
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION


#define BLEINFOTYPE_RUNNINGSPEEDANDCADENCE_REMOTEFEATURES		0xB3	//1 byte
#define BLEINFOTYPE_RUNNINGSPEEDANDCADENCE_RSCMEASUREMENTHANDLE	0xB4	//2 bytes
#define BLEINFOTYPE_RUNNINGSPEEDANDCADENCE_SCCONTROLPOINTHANDLE	0xB5	//2 bytes


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/
/** 
 *BleRunningSpeedAndCadenceCollectorFeatureBitfield type
 * Bitfield defining the remote Running Speed and Cadence SENSOR capabilities.
 * It defines after the linkUP has completed, what are the 
 * running speed and cadence specification optional defined features supported by
 * the remote device.
 */
typedef	U16	BleRunningSpeedAndCadenceCollectorFeatureBitfield;
// Remote SENSOR supports the "Instantaneous measurement" feature
#define BLERSCCOLLECTOR_FEATUREBITMASK_INSTANTANEOUSSTRIDELENGTHMEASUREMENTSUPPORTED	(0x0001u)
// Remote SENSOR supports the "Total distance measurement" feature
#define BLERSCCOLLECTOR_FEATUREBITMASK_TOTALDISTANCEMEASUREMENTSUPPORTED				(0x0002u)
// Remote SENSOR supports the "Running or Walking status" feature
#define BLERSCCOLLECTOR_FEATUREBITMASK_WALKINGORRUNNINGSTATUSSUPPORTED					(0x0004u)
// Remote SENSOR supports the "Calibration Procedure" feature
#define BLERSCCOLLECTOR_FEATUREBITMASK_CALIBRATIONPROCEDURESUPPORTED					(0x0008u)
// Remote SENSOR supports the "Multiple sensor locations" feature
#define BLERSCCOLLECTOR_FEATUREBITMASK_MULTIPLESENSORLOCATIONSSUPPORTED					(0x0010u)

/** 
 * BleRunningSpeedAndCadenceCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleRunningSpeedAndCadenceCollectorCallback
 */
typedef	U8	BleRunningSpeedAndCadenceCollectorEvent;

/** BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleRunningSpeedAndCadenceCollectorLinkUPInfo pointer.
 */
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED						0x92

/** BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleRunningSpeedAndCadenceCollectorLinkUPInfo pointer.
 */
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED					0x93

/** BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a RSC
 * SENSOR measurement from the SENSOR.
 * It follows an alert from the Sensor
 * the status field indicates the status of the operation.
 * The parms field indicates the RSC measurement information within the 
 * BleRunningSpeedAndCadenceCollectorRscMeasurementInfo type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleRunningSpeedAndCadenceCollector fields are consistent. Other fields
 *   of BleRunningSpeedAndCadenceCollectorRscMeasurementInfo must be ignored
 * - If status is BLESTATUS_SUCCESS the measurement fields contains consistent
 *   information.
 */
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT	            0x94

/** BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_TOTALDISTANCESET
 * Event received to indicate that this COLLECTOR has set a new total distance
 * value.
 * It follows the call of BLERUNNINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_TOTALDISTANCESET		    0x95

/**
 * BleRunningSpeedAndCadenceCollectorCallback
 *	This callback receives the BLE RUNNING SPEED AND CADENCE COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleRunningSpeedAndCadenceCollectorCallback)(
	BleRunningSpeedAndCadenceCollectorEvent	event,
	BleStatus					status,
	void					   *parms); 

/** 
 * BleRunningSpeedAndCadenceCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LinkUpRunningSpeedAndCadenceProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LinkUpRunningSpeedAndCadenceProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Running Speed And Cadence SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								runningSpeedAndCadenceServiceStartingHandle;
	AttHandle								runningSpeedAndCadenceServiceEndingHandle;

	AttHandle								rscMeasurementValueHandle;
	AttHandle								rscMeasurementConfigHandle;

	AttHandle								sensorLocationHandle;

	AttHandle								rscFeatureValueHandle;

    BleRunningSpeedAndCadenceCollectorFeatureBitfield   
                                            rscFeatureValue;

	AttHandle								scControlPointValueHandle;
	AttHandle								scControlPointConfigHandle;
	U8										scControlPointValue[5];

	U8										rscConfigValue[2];

	U16										runningSpeedAndCadenceSensorFeatures;

} BleRunningSpeedAndCadenceCollector;


/** 
 * BleRunningSpeedAndCadenceCollectorLinkUPInfo type
 * structure passed in parameter in the BleRunningSpeedAndCadenceCollectorCallback
 * parms fields during the BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleRunningSpeedAndCadenceCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * supportedFeatures : The sensor supported features
 */
typedef struct {

	BleRunningSpeedAndCadenceCollector			        *bleRunningSpeedAndCadenceCollector;
	U16											        connHandle;
    BleRunningSpeedAndCadenceCollectorFeatureBitfield   supportedFeatures;
} BleRunningSpeedAndCadenceCollectorLinkUPInfo;

/** 
 * BleRunningSpeedAndCadenceCollectorFlags type
 * Bitfield defining the available flags values.
 */
typedef U8 BleRunningSpeedAndCadenceCollectorFlags;
#define BLERSCCOLLECTOR_FLAGBITMASK_INSTANTANEOUSSTRIDELENGTHMEASUREMENT      (0x01u)
#define BLERSCCOLLECTOR_FLAGBITMASK_TOTALDISTANCEMEASUREMENT		          (0x02u)
#define BLERSCCOLLECTOR_FLAGBITMASK_WALKINGORRUNNINGSTATUS		        	  (0x04u)

/** 
 * BleRunningSpeedAndCadenceCollectorMeasurementFlags type
 * Bitfield defining the flags that can be present in the RSC measurement
 * can be accessed directly via the rscMeasurementFlagsValue byte or 
 * bit-by-bit in the fields structure.
 */
typedef union {
    /** rscMeasurementFlagsValue:
     * A byte indicating what are the fields included in the measurement
     */
	BleRunningSpeedAndCadenceCollectorFlags rscMeasurementFlagsValue;

	struct 
	{
        /** instantaneousStrideLengthPresent:
         * Field indicating if the instantaneous Stride Length is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLERSCCOLLECTOR_FEATUREBITMASK_INSTANTANEOUSSTRIDELENGTHMEASUREMENTSUPPORTED
         * is not supported.
         */
		unsigned int instantaneousStrideLengthPresent:1;

        /** totalDistancePresent:
         * Field indicating if the total Distance is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLERSCCOLLECTOR_FEATUREBITMASK_TOTALDISTANCEMEASUREMENTSUPPORTED
         * is not supported.
         */
		unsigned int totalDistancePresent:1;

        /** isRunning:
         * Field indicating the current running/walking status
         * isRunning = 0 : Walking ; isRunning = 1 : Running 	
         * Shall be ignored if in the remote sensor features,
         * BLERSCCOLLECTOR_FEATUREBITMASK_WALKINGORRUNNINGSTATUSSUPPORTED
         * is not supported.
         */
		unsigned int runningStatusbit:1; 

		//unsigned int reserved:5;
	} fields;

} BleRunningSpeedAndCadenceCollectorMeasurementFlags;


/** 
 * BleRunningSpeedAndCadenceCollectorRscMeasurementInfo type
 * structure passed in parameter in the BleRunningSpeedAndCadenceCollectorCallback
 * parms fields during the BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT event.
 * It describes the Running Speed And cadence SENSOR measurement.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleRunningSpeedAndCadenceCollector		*bleRunningSpeedAndCadenceCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16									    connHandle;

    /*
     * The flags indicating which Value is present in the measurement,
     * only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     */
	BleRunningSpeedAndCadenceCollectorMeasurementFlags
                                            flags;
    /*
     * The instantaneous Speed Value,
     * only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     * Unit is in m/s with a resolution of 1/256 s
     */
	U16									    instantaneousSpeedValue;

    /*
     * The instantaneous cadence Value,
     * only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     * Unit is in 1/minute (or RPM) with a resolutions of 1 (1/min or 1 RPM) 
     */
	U16									    instantaneousCadenceValue;

    /*
     * The instantaneous Stride length Value,
     * only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *  Unit is in meter with a resolution of 1/100 m (or centimeter).
     *
     * Only reported when in the remote sensor features,
     * BLERSCCOLLECTOR_FEATUREBITMASK_INSTANTANEOUSSTRIDELENGTHMEASUREMENTSUPPORTED
     * is supported.
     */
	U16									    instantaneousStrideLengthValue;

    /*
     * The total distance Value,
     * only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise
     *  Unit is in meter with a resolution of 1/10 m (or decimeter).
     *
     * Only reported when in the remote sensor features,
     * BLERSCCOLLECTOR_FEATUREBITMASK_TOTALDISTANCEMEASUREMENTSUPPORTED
     * is supported.
     */
	U32									    totalDistanceValue;

} BleRunningSpeedAndCadenceCollectorRscMeasurementInfo;


/** 
 * BleRunningSpeedAndCadenceCollectorLinkMode type
 * Type passed during the 
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LinkUpRunningSpeedAndCadenceProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleRunningSpeedAndCadenceCollectorLinkMode;

#define BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE			1
#define BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Running Speed And Cadence profile 
 *
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_Init()
 *	This function is the entry point of a Running Speed And Cadence profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleRunningSpeedAndCadenceCollectorCallback in which 
 *				 the collector events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLERUNNINGSPEEDANDCADENCE_COLLECTOR_Init(
			BleRunningSpeedAndCadenceCollectorCallback callback);

/** De-Init the Collector Role of the Running Speed And Cadence Profile.
 *
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Running Speed And Cadence Profile.
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Mahault ANDRIA
 */
BleStatus BLERUNNINGSPEEDANDCADENCE_COLLECTOR_Deinit( void ) ;

/** Link Up the Running Speed And Cadence Profile with the specified remote Device
 *
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LinkUpRunningSpeedAndCadenceProfile()
 *	This function is used to link-Up the Running Speed And Cadence Profile with the 
 *  specified remote device.
 *  It will try to discover the Running Speed And Cadence profile in the remote device 
 *  using the given collector.
 * BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Running Speed And Cadence COLLECTOR to link-up
 *					with a remote Running Speed And Cadence SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to the profile API in order to identify to which linked 
 *                  SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE RUNNING SPEED AND CADENCE COLLECTOR and the remote BLE RUNNING SPEED AND CADENCE
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleRunningSpeedAndCadenceCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *		BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED event will be received in the
 *		callback registered during the call of
 *		BLERUNNINGSPEEDANDCADENCE_COLLECTOR_Init. This status is returned only if mode
 *		is BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLERUNNINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Running Speed And Cadence SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	    an other Running Speed And Cadence Operation is in Progress, wait for this
 *      operation completion.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLERUNNINGSPEEDANDCADENCE_COLLECTOR_LinkUpRunningSpeedAndCadenceProfile(
						BleRunningSpeedAndCadenceCollector *collector,
						U16 connHandle,
						BleRunningSpeedAndCadenceCollectorLinkMode linkUpMode,
						BleRunningSpeedAndCadenceCollectorLinkUPInfo *info
						);

/** Un Link the Running Speed And Cadence Profile with the specified linked COLLECTOR
 *
 * BLERUNNINGSPEEDANDCADENCE_COLLECTOR_UnLinkRunningSpeedAndCadenceProfile()
 *	This function is used to UnLink the Running Speed And Cadence Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Running Speed And Cadence Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_RUNNINGSPEEDANDCADENCE_* from persistent memory
 *
 * BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Running Speed And Cadence COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLERUNNINGSPEEDANDCADENCE_COLLECTOR_UnLinkRunningSpeedAndCadenceProfile(
		BleRunningSpeedAndCadenceCollector *collector);


/** set a new cumulative (total) distance in remote linked SENSOR
*
* BLERUNNINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue()
*	This function is used to set a new cumulative (total) distance
* in remote linked SENSOR, if the remote sensor supports total distance
* feature.
*   
* BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked RUNNING SPEED AND CADENCE COLLECTOR. 
* @param value: The new value to set as cumulative value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_TOTALDISTANCESET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*	an other RUNNING SPEED AND CADENCE profile Operation is in Progress, wait for this
*  operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the RUNNING SPEED AND CADENCE profile seems not been initialized).
*
* @author Alexandre GIMARD
*/
BleStatus BLERUNNINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue( 
    BleRunningSpeedAndCadenceCollector *collector,
    U32                                 value);

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLERUNNINGSPEEDANDCADENCE_SUPPORT_COLLECTOR== 1)

#endif /*__BLERUNNINGSPEEDANDCADENCE_COLLECTOR_H*/
