#ifndef __BLEALERTNOTIFICATION_CLIENT_H
#define __BLEALERTNOTIFICATION_CLIENT_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleAlertNotification_Client.h
 *
 * Description:   Contains interfaces definition for Alert Notification 
 *					Profile when the local device operates in the CLIENT
 *					role.
 * 
 * Created:       March, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2718 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "BleEngine.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT
 * Defines if the BLE ALERT NOTIFICATION Profile implementation supports the 
 * CLIENT Role
 * If enabled ( set to 1 ) it enables all the following BLE ALERT
 * NOTIFICATION Profile CLIENT configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEALERTNOTIFICATION_SUPPORT_CLIENT
#define BLEALERTNOTIFICATION_SUPPORT_CLIENT							0
#endif //BLEALERTNOTIFICATION_SUPPORT_CLIENT

#if (BLEALERTNOTIFICATION_SUPPORT_CLIENT == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when ALERT NOTIFICATION Profile CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE ALERT NOTIFICATION Profile implementation in 
 * CLIENT role supports saving the reporter information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the CLIENT saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the REPORTER information.
 * Upon reconnection with the reporter, as these information are already known
 * then the linkUp process can be simplified, and new alert and unread alert status Notifications
 * could be automaticaly understood by this collector.
 * it saves 9 bytes into persistent memory
 *
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION	
#define BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION							1
#endif //BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_REMOTEFEATURES								0xA0	
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_SUPPORTEDNEWALERTCATEGORYHANDLE			0xA1
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_NEWALERTHANDLE								0xA2
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_NEWALERTCONFIGHANDLE						0xA3
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_SUPPORTEDUNREADALERTSTATUSCATEGORYHANDLE	0xA4
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_UNREADALERTSTATUSHANDLE					0xA5
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_UNREADALERTSTATUSCONFIGHANDLE				0xA6
#define BLEINFOTYPE_ALERTNOTIFICATIONCLIENT_ALERTNOTIFICATIONCONTROLPOINTHANDLE		0xA7

#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION
#define BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/

/** 
 * BleAlertNotificationAlertCategory type
 * Defines the different category of alert type that could be received 
 * by the BleAlertNotificationCallBack during the 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY event or 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_CATEGORY event.
 * These are 2 bytes used as a mask that describe the supported categories
 * for new alert or unread alert status. 
 */
typedef	U16	BleAlertNotificationAlertCategory;
#define BLEALERTNOTIFICATION_CLIENT_CATEGORY_NONE								0x0000
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_SIMPLEALERT				0x0001
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_EMAIL					0x0002
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_NEWS						0x0004
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_CALL						0x0008
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_MISSEDCALL				0x0010
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_SMSMMS					0x0020
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_VOICEMAIL				0x0040
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_SCHEDULE					0x0080
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_HIGHPRIORITIZEDALERT		0x0100
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYIDBITMASK_INSTANTMESSAGE			0x0200


/** 
 * BleAlertNotificationAlertCategoryId type
 * Defines the specific category of alert type to which the command applies 
 * It is a byte that describes the category for new alert or unread 
 * alert status. 
 */
typedef U8 BleAlertNotificationAlertCategoryId;
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_SIMPLEALERT						0x00
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_EMAIL							0x01
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_NEWS								0x02
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_CALL								0x03
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_MISSEDCALL						0x04
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_SMSMMS							0x05
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_VOICEMAIL						0x06
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_SCHEDULE							0x07
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_HIGHPRIORITIZEDALERT				0x08
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_INSTANTMESSAGE					0x09
#define BLEALERTNOTIFICATION_CLIENT_CATEGORYID_ALLCATEGORIESWILDCARD			0xFF

/** 
 * BleAlertNotificationCommandId type
 * Defines the different command Ids of control point type that could 
 * be sent to the REPORTER.
 */
typedef U8 BleAlertNotificationCommandId;
#define BLEALERTNOTIFICATION_ENABLE_NEW_ALERT_NOTIFICATION					0
#define BLEALERTNOTIFICATION_ENABLE_UNREAD_ALERT_STATUS_NOTIFICATION		1	
#define BLEALERTNOTIFICATION_DISABLE_NEW_ALERT_NOTIFICATION					2
#define BLEALERTNOTIFICATION_DISABLE_UNREAD_ALERT_STATUS_NOTIFICATION		3	
#define BLEALERTNOTIFICATION_NOTIFY_NEW_ALERT_IMMEDIATELY					4
#define BLEALERTNOTIFICATION_NOTIFY_UNREAD_ALERT_STATUS_IMMEDIATELY			5	


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/
/** 
 * BleAlertNotificationClientEvent type
 * Define the different kind of events that could be received by the 
 * BleAlertNotificationCallBack
 */
typedef	U8	BleAlertNotificationClientEvent;

/** BLEALERTNOTIFICATION_EVENT_LINKED
 * Event received when the LinkUP is completed 
 * The status field indicates the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleAlertNotificationLinkUPInfo pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_LINKED								0xF0

/** BLEALERTNOTIFICATION_EVENT_UNLINKED
 * Event received when the UnLink is completed 
 * The status field indicates the status of the operation.
 * The parms field indicates the link up information within a
 * BleAlertNotificationLinkUPInfo pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_UNLINKED								0xF1

/** BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY
 * Event received when the supported new alert categories have been received.
 * It is automatically received after a connection establishement or it may 
 * follow a call of the
 * BLEALERTNOTIFICATION_CLIENT_ReadSupportedNewAlertCategory() API.
 * The status field indicates the status of the operation.
 * The parms field indicates the supported new alert categories withn the
 * BleAlertNotificationClientAlertCategoryInfo pointer and 
 * supportedAlertCategory field.
 */
#define BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY			0xF2	

/** BLEALERTNOTIFICATION_EVENT_NEW_ALERT_COUNT
 * Event received when new alerts have been received, It follows a 
 * new alert notification from the SERVER, or it can follow a 
 * BLEALERTNOTIFICATION_NOTIFY_NEW_ALERT_IMMEDIATELY command send to the 
 * SERVER via the BLEALERTNOTIFICATION_CLIENT_RequestAlertNotificationState()
 * API
 * The status field is always BLESTATUS_SUCCESS.
 * The parms field indicates the new alert categories within the 
 * count of alerts of the BleAlertNotificationClientNewAlertInfo
 */
#define BLEALERTNOTIFICATION_EVENT_NEW_ALERT							0xF3

/** BLEALERTNOTIFICATION_EVENT_NEWALERTSUBSCRIPTION_RSP
 * Event received when the Subscription of new alert notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BleAlertNotificationClient pointer.*/
#define BLEALERTNOTIFICATION_EVENT_NEWALERTSUBSCRIPTION_RSP				0xF4		

/** BLEALERTNOTIFICATION_EVENT_NEWALERTUNSUBSCRIPTION_RSP
 * Event received when the unsubscription of new alert notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BleAlertNotificationClient pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_NEWALERTUNSUBSCRIPTION_RSP			0xF5

/** BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_CATEGORY
 * Event received when the supported unread alert status categories have
 * been received.
 * It is automatically received after a connection establishement or it may 
 * follows a call of the
 * BLEALERTNOTIFICATION_CLIENT_ReadSupportedUnreadAlertCategory() API.
 * The status field indicates the status of the operation.
 * The parms field indicates the supported new alert categories withn the
 * BleAlertNotificationClientAlertCategoryInfo pointer and 
 * supportedAlertCategory field.
 */
#define BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_CATEGORY		0xF6

/** BLEALERTNOTIFICATION_EVENT_UNREAD_ALERT_STATUS_COUNT
 * Event received when unread alert status is received. , It follows a 
 * unread alert status notification from the SERVER, or it can follow a 
 * BLEALERTNOTIFICATION_NOTIFY_UNREAD_ALERT_STATUS_IMMEDIATELY command send  
 * to the SERVER via the 
 * BLEALERTNOTIFICATION_CLIENT_RequestAlertNotificationState API
 * The status field is always BLESTATUS_SUCCESS.
 * The parms field indicates the unread alert status categories within the 
 * BleAlertNotificationClientUnreadAlertStatusInfo pointer
 */
#define BLEALERTNOTIFICATION_EVENT_UNREAD_ALERT_STATUS					0xF7

/** BLEALERTNOTIFICATION_EVENT_UNREADALERTSTATUSSUBSCRIPTION_RSP
 * Event received when the subscription of unread alert status notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BleAlertNotificationClient pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_UNREADALERTSTATUSSUBSCRIPTION_RSP	0xF8

/** BLEALERTNOTIFICATION_EVENT_UNREADALERTSTATUSUNSUBSCRIPTION_RSP
 * Event received when the unsubscription of unread alert status notification 
 * is completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BleAlertNotificationClient pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_UNREADALERTSTATUSUNSUBSCRIPTION_RSP	0xFA

/** BLEALERTNOTIFICATION_EVENT_CONTROLLINGALERTNOTIFICATION_RSP
 * Event received when the control of new alert notification or 
 * unread alert status is completed.
 * The status field indicates the status of the operation.
 * The parms field indicates the client for which the command
 * has been sent within a BleAlertNotificationClient pointer.
 */
#define BLEALERTNOTIFICATION_EVENT_CONTROLLINGALERTNOTIFICATION_RSP		0xFB

/**
 * BleAlertNotificationCallBack
 *	This callback receives the BLE ALERT NOTIFICATION CLIENT events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleAlertNotificationCallBack)(
	BleAlertNotificationClientEvent	event,
	BleStatus					status,
	void					   *parms); 

/** 
 * BleAlertNotificationClient type
 * Define a local CLIENT, it represent a memory block
 * that should be passed by the application during the 
 * BLEALERTNOTIFICATION_CLIENT_LinkUpAlertNotificationProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEALERTNOTIFICATION_CLIENT_LinkUpAlertNotificationProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * CLIENT receive the BLEALERTNOTIFICATION_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this CLIENT memory block while 
 * linked with a remote REPORTER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local CLIENT could be then passed as parameter to 
 * profile API in order to identify to which linked REPORTERthe operation  
 * is destinated.
 *
 * A CLIENT is unique per remote AlertNotification REPORTER, it means 
 * that if this local device is connected to multiple remote REPORTER the 
 * application shall register multiple CLIENTs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattInternalCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	U8										internalstate;
//	U8										reporterFeatures;
	U16										reporterFeatures;
	//passage de reporterFeatures de U8 � U16 
	//pour faire toute la state machine 
	// "BLEALERTNOTIFICATIONCLIENT_REMOTESUPPORT..."
	U8 										configValue[2];
	AttHandle								alertNotificationServiceStartingHandle;
	AttHandle								alertNotificationServiceEndingHandle;

	AttHandle								supportedNewAlertCategoryHandle;
	AttHandle								newAlertHandle;
	AttHandle								newAlertConfigHandle;
	AttHandle								supportedUnreadAlertCategoryHandle;
	AttHandle								unreadAlertStatusHandle;
	AttHandle								unreadAlertStatusConfigHandle;
	AttHandle								alertNotificationControlPointHandle;

	BleAlertNotificationAlertCategory		enabledNewAlerts;
	BleAlertNotificationAlertCategory		enabledUnreadAlerts;

} BleAlertNotificationClient;


/** 
 * BleAlertNotificationLinkUPInfo type
 * structure passed in parameter in the BleAlertNotificationCallBack
 * parms fields during the BLEALERTNOTIFICATION_EVENT_LINKED event.
 * It indicates :
 * bleAlertNotificationClient : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 */
typedef struct {
	BleAlertNotificationClient				*bleAlertNotificationClient;
	U16										connHandle; 
} BleAlertNotificationLinkUPInfo;

/** 
 * BleAlertNotificationClientAlertCategoryInfo type
 * Structure passed in parameter in the BleAlertNotificationClientCallBack
 * parms fields during the 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY event or 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_CATEGORY event.
 * It indicates :
 * bleAlertNotificationClient : The client from which the event is coming
 * connHandle : The connection handle from which the supported alert
 * category information is coming
 * supportedAlertCategory : the supported alert category information 
 * provided by the SERVER;
 * Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {
	BleAlertNotificationClient				*bleAlertNotificationClient;
	U16										connHandle;
	BleAlertNotificationAlertCategory		supportedAlertCategory;

} BleAlertNotificationClientAlertCategoryInfo;

/** 
 * BleAlertNotificationClientNewAlertInfo type
 * Structure passed in parameter in the BleAlertNotificationClientCallBack
 * parms fields during the 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY event.
 * It indicates :
 * bleAlertNotificationClient : The client from which the event is coming
 * connHandle : The connection handle from which the supported alert
 * category information is coming
 * BleAlertNotificationAlertCategoryId : the category to which the alert 
 * applies to.
 * numberOfNewAlerts : the number of new alerts in the specified category
 * textStringInformation : a pointer on a text string info  
 * textStringInformationLen : the length of the text string information  
 * Text string info is an optionnal feature so the text info len MIN is 0. 
 * the text string information MAX len is 18.
 * Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleAlertNotificationClient				*bleAlertNotificationClient;
	U16										connHandle;
	BleAlertNotificationAlertCategoryId		newAlertCategoryId;
	U8										numberOfNewAlerts;
	U8										*textStringInformation;
	U8										textStringInformationLen;
	//U8										textStringInformation[];
} BleAlertNotificationClientNewAlertInfo;

/** 
 * BleAlertNotificationClientUnreadAlertStatusInfo type
 * Structure passed in parameter in the BleAlertNotificationClientCallBack
 * parms fields during the 
 * BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_STATUS_CATEGORY event.
 * It indicates :
 * bleAlertNotificationClient : The client from which the event is coming
 * connHandle : The connection handle from which the supported alert
 * category information is coming
 * unreadAlertStatusCategoryId : the category to which the alert 
 * applies to.
 * numberOfUnreadAlerts : the number of unread alerts in the specified category
 * Only applicable if event status is BLESTATUS_SUCCESS
 */ 
typedef struct {

	BleAlertNotificationClient				*bleAlertNotificationClient;
	U16										connHandle;
	BleAlertNotificationAlertCategoryId		unreadAlertStatusCategoryId;
	U8										numberOfUnreadAlerts;
} BleAlertNotificationClientUnreadAlertStatusInfo;

/** 
 * BleAlertNotificationClientLinkMode type
 * Type passed during the 
 * BLEALERTNOTIFICATION_CLIENT_LinkUpAlertNotificationProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEALERTNOTIFICATION_CLIENT_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEALERTNOTIFICATION_CLIENT_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleAlertNotificationClientLinkMode;
#define BLEALERTNOTIFICATION_CLIENT_LINKUP_COMPLETE			1
#define BLEALERTNOTIFICATION_CLIENT_LINKUP_RESTORE				2

/****************************************************************************
 *	APPLICATION INTERFACE functions declaration
 ****************************************************************************/

/** Init the CLIENT role of Alert Notification profile 
 *
 * BLEALERTNOTIFICATION_CLIENT_Init()
 *	This function is the entry point of a Alert Notification profile
 *  application thta runs the CLIENT role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param callback: The BleAlertNotificationCallBack in which the collector 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_Init(
			BleAlertNotificationCallBack callback);

/** De-Init the Client Role of the Alert Notification Profile.
 *
 * BLEALERTNOTIFICATION_CLIENT_Deinit
 *	This function is used to De-Init the Client Role of the 
 *  Alert Notification Profile.
 *
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_Deinit( void ) ;

/** Link Up the Alert Notification Profile with the specified remote Device
 *
 * BLEALERTNOTIFICATION_CLIENT_LinkUpAlertNotificationProfile()
 *	This function is used to link-Up the Alert Notification Profile with the 
 *  specified remote device.
 *  It will try to discover the Alert Notification profile in the remote device 
 *  using the given collector.
 * BLEALERTNOTIFICATION_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param collector: A valid Alert Notification CLIENT to link-up
 *					with a remote Alert Notification REPORTER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this CLIENT could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  REPORTER the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE ALERT NOTIFICATION CLIENT and the remote BLE ALERT 
 *			NOTIFICATION REPORTER shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *			BLEALERTNOTIFICATION_EVENT_LINKED event will be received in the
 *			callback registered during the call of
 *			BLEALERTNOTIFICATION_CLIENT_Init. This status is returned only if mode
 *			is BLEALERTNOTIFICATION_CLIENT_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEALERTNOTIFICATION_CLIENT_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEALERTNOTIFICATION_CLIENT_LINKUP_RESTORE, it means that 
 *		BLEALERTNOTIFICATIONCLIENT_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Alert Notification REPORTER. 
 *		It may also indicates that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Alert Notification  Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_LinkUpAlertNotificationProfile(
						BleAlertNotificationClient *client,
						U16 connHandle,
						BleAlertNotificationClientLinkMode linkUpMode
						);

/** Un Link the Alert Notification Profile with the specified linked CLIENT
 *
 * BLEALERTNOTIFICATION_CLIENT_UnLinkAlertNotificationProfile()
 *	This function is used to UnLink the Alert Notification Profile with the 
 *  specified previously linked CLIENT.
 *  This CLIENT will not receive any more information from the linked 
 *	REPORTER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_ALERTNOTIFICATION_* from persistent memory
 *
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled 
 *
 * @param collector: A linked Alert Notification CLIENT 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_UnLinkAlertNotificationProfile(
		BleAlertNotificationClient *client);

/** Get the current Ringer Setting from a linked SERVER
 *
 * BLEALERTNOTIFICATION_CLIENT_ReadSupportedNewAlertCategory()
 *	This function is used to read the supported new alert category
 *  on the remote REPORTER.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEALERTNOTIFICATION_EVENT_SUPPORTED_NEW_ALERT_CATEGORY event will be received in the 
 *		callback with the status of the operation and the Ringer Setting information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_ReadSupportedNewAlertCategory( 
		BleAlertNotificationClient *client );

/** Read the supported Unread Alert Status category from a linked REPORTER
 *
 * BLEALERTNOTIFICATION_CLIENT_ReadSupportedNewAlertCategory()
 *	This function is used to Read the supported Unread Alert Status category
 *  on the remote SERVER
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEALERTNOTIFICATION_EVENT_SUPPORTED_UNREAD_ALERT_CATEGORY event will be received in the 
 *		callback with the status of the operation and the Ringer Setting information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_ReadSupportedUnreadAlertCategory( 
		BleAlertNotificationClient *client );

/** Configure the ALERT NOTIFICATION REPORTER to deliver the New Alert Status
 * when modified
 *
 * BLEALERTNOTIFICATION_CLIENT_SubscribeNewAlertNotification()
 *	This function is used by the given linked ALERT NOTIFICATION CLIENT to 
 *  configure the remote ALERT NOTIFICATION REPORTER to deliver new current Alert
 *  when new alert count changes on the SERVER.
 *  When new alert information is received the 
 *  BLEALERTNOTIFICATION_EVENT_NEW_ALERT_COUNT event will be received in the 
 *  callback with the new alert status information.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEALERTNOTIFICATION_EVENT_NEWALERTSUBSCRIPTION_RSP event 
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_SubscribeNewAlertNotification(
					BleAlertNotificationClient *bleAlertNotificationClient );

/** Configure the ALERT NOTIFICATION REPORTER to not deliver the New alert
 *  COunt when it is modified
 *
 * BLEALERTNOTIFICATION_CLIENT_UnsubscribeNewAlertNotification()
 *	This function is used by the given linked ALERT NOTIFICATION CLIENT to 
 *  configure the remote ALERT NOTIFICATION REPORTER to not deliver new
 *  Alert when new alert count change on the SERVER.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEALERTNOTIFICATION_EVENT_NEWALERTUNSUBSCRIPTION_RSP  
 *		event will be received in the callback with the status of the 
 *		operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_UnsubscribeNewAlertNotification(
		BleAlertNotificationClient *bleAlertNotificationClient );



/** Configure the ALERT NOTIFICATION REPORTER to deliver the New Alert Status
 * when modified
 *
 * BLEALERTNOTIFICATION_CLIENT_SubscribeAlertStatusNotification()
 *	This function is used by the given linked ALERT NOTIFICATION CLIENT to 
 *  configure the remote ALERT NOTIFICATION REPORTER to deliver new current Alert
 *  status when alert status change on the SERVER.
 *  When new alert Status information is received the 
 *  BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS event will be received in the 
 *  callback with the new alert status information.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSSUBSCRIPTION_RSP event 
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_SubscribeUnreadAlertStatusNotification(
					BleAlertNotificationClient *bleAlertNotificationClient );

/** Configure the ALERT NOTIFICATION REPORTER to not deliver the New alert
 *  status when it is modified
 *
 * BLEALERTNOTIFICATION_CLIENT_UnSubscribeAlertStatusNotification()
 *	This function is used by the given linked ALERT NOTIFICATION CLIENT to 
 *  configure the remote ALERT NOTIFICATION REPORTER to not deliver new
 *  current Alert status when alert status change on the SERVER.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked ALERT NOTIFICATION CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSSTATUSUNSUBSCRIPTION_RSP  
 *		event will be received in the callback with the status of the 
 *		operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other ALERT NOTIFICATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the ALERT NOTIFICATION profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_UnsubscribeUnreadAlertStatusNotification(
		BleAlertNotificationClient *bleAlertNotificationClient );


/** request the ALERT NOTIFICATION SERVER to set the control point.
 *
 * BLEALERTNOTIFICATION_CLIENT_RequestAlertNotificationState
 *	This function is used by the given linked ALERT NOTIFICATION CLIENT to 
 *  request the remote ALERT NOTIFICATION SERVER to write the value of 
 *  the Control Point.
 *  Control point may have 6 settings : enable/disable new alert 
 *  notifications, enable/disable unread alert status notifications,
 *  notify immediately new alert or unread alert status.
 *  When request has been sent to the remote SERVER
 *  BLEALERTNOTIFICATION_EVENT_CONTROLLINGALERTNOTIFICATION_RSP event will 
 *  be received in the callback with the new COntrol Point Setting information.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blebleAlertNotificationClient: A linked ALERT NOTIFICATION CLIENT.
 * commandId : the ID of the command which is sent
 * requestedCategoryId : the category to which the command applies 
 *						 The command applies to a single category or to all categories
 *						 if the wildcard is used.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERCONTROL_SENT event  
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.It can be also because the
 *	    remote SERVER does not support the RINGER CONTROL characteristic, the 
 *      support of this feature by the SERVER is returned upon successful linkup
 *		event.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *		initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEALERTNOTIFICATION_CLIENT_RequestAlertNotificationState(
	BleAlertNotificationClient		*bleAlertNotificationClient,
	BleAlertNotificationCommandId		commandId,
	BleAlertNotificationAlertCategoryId	requestedCategoryId );

/** request the ALERT NOTIFICATION SERVER to set the control point.
 *
 * BLEALERTNOTIFICATION_CLIENT_GetReconnectionEnabledNewAlertCategories
 *	The profile need sometimes toknow what are the Application enabled 
 * new alert categories.
 *  This API must be implemented in the application and must return 
 *  immediatly the the current Application enabled new alert categories.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blebleAlertNotificationClient: A linked ALERT NOTIFICATION CLIENT.
 *
 * @return The Application enabled new alert categories.
 *
 * @author Mahault ANDRIA
 */
BleAlertNotificationAlertCategory 
	BLEALERTNOTIFICATION_CLIENT_GetReconnectionEnabledNewAlertCategories(
		BleAlertNotificationClient		*bleAlertNotificationClient);


/** request the ALERT NOTIFICATION SERVER to set the control point.
 *
 * BLEALERTNOTIFICATION_CLIENT_GetReconnectionEnabledUnreadAlertCategories
 *	The profile need sometimes toknow what are the Application enabled 
 * new alert categories.
 *  This API must be implemented in the application and must return 
 *  immediatly the the current Application enabled new alert categories.
 *   
 * BLEALERTNOTIFICATION_SUPPORT_CLIENT shall be enabled
 *
 * @param blebleAlertNotificationClient: A linked ALERT NOTIFICATION CLIENT.
 *
 * @return The Application enabled unread alert categories.
 *
 * @author Mahault ANDRIA
 */
BleAlertNotificationAlertCategory 
	BLEALERTNOTIFICATION_CLIENT_GetReconnectionEnabledUnreadAlertCategories(
		BleAlertNotificationClient		*bleAlertNotificationClient);

#endif //(BLEALERTNOTIFICATION_SUPPORT_CLIENT== 1)

#endif /*__BLEALERTNOTIFICATION_CLIENT_H*/
