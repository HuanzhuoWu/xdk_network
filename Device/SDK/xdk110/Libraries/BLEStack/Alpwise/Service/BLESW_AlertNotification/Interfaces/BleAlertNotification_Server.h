#ifndef __BLEALERTNOTIFICATION_SERVER_H
#define __BLEALERTNOTIFICATION_SERVER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 * 
 * File:          BleAlertNotification_Server.h
 *
 * Description:   Contains interfaces definitions for Alert Notification 
 *					Profile when the local device operates in the SERVER
 *					role.
 * 
 * Created:       December, 2011
 * Version:		  0.1
 *
 * File Revision: $Rev: 2718 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEALERTNOTIFICATION_SUPPORT_SERVER
 * Defines if the BLE ALERT NOTIFICATION Profile implementation supports the 
 * SERVER Role
 * If enabled ( set to 1 ) it enables all the following BLE ALERT 
 * NOTIFICATION Profile SERVER configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEALERTNOTIFICATION_SUPPORT_SERVER
#define BLEALERTNOTIFICATION_SUPPORT_SERVER					1
#endif //BLEALERTNOTIFICATION_SUPPORT_SERVER

#if (BLEALERTNOTIFICATION_SUPPORT_SERVER== 1)

// Then check dependencies
#include "Services/BleAlertNotification_Service.h"

// Alert Notification Service is mandatory
#if (BLE_SUPPORT_ALERTNOTIFICATION_SERVICE == 0)
#error BLE_SUPPORT_ALERTNOTIFICATION_SERVICE shall be enabled when BleAlertNotification SERVER Role is enabled
#endif //(BLE_SUPPORT_ALERTNOTIFICATION_SERVICE == 0)

/***************************************************************************\
 * OPTIONAL FEATURES FOR ALERT NOTIFICATION SERVICE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE ALERT NOTIFICATION in SERVER Role.
 *
 * BLEALERTNOTIFICATION_SERVER_Register()
 *	This function is used to register and initialise a BLE ALERT NOTIFICATION 
 *  in SERVER role.
 *  It will add the supported  ALERT NOTIFICATION Service characteristics and
 *  into the Attribute database.
 *	This Interface shall be the entry point of a BLE ALERT NOTIFICATION
 *  in SERVER Role.
 *
 *	@see BLEALERTNOTIFICATION_SERVER_Deregister()
 *
 * BLEALERTNOTIFICATION_SUPPORT_SERVER shall be enabled.
 *
 * @param none.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Mahault ANDRIA
 */


//BleStatus BLEALERTNOTIFICATION_SERVER_Register( BleAlertNotificationServiceCallback callback );
BleStatus BLEALERTNOTIFICATION_SERVER_Register(
	BleAlertNotificationServiceCallBack alertNotificationCallback);

/***************************************************************************\
 * OPTIONAL API functions definition ALERT NOTIFICATION PROFILE
\***************************************************************************/

#endif //(BLEALERTNOTIFICATION_SUPPORT_SERVER== 1)

#endif /*__BLEALERTNOTIFICATION_SERVER_H*/
