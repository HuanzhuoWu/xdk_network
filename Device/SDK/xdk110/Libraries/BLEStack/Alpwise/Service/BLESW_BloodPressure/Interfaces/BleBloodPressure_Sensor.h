#ifndef __BLEBLOODPRESSURE_SENSOR_H
#define __BLEBLOODPRESSURE_SENSOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleBloodPressure_Sensor.h
 *
 * Description:   Contains interfaces definitions for Blood Pressure 
 *					Profile when the local device operates in the SENSOR
 *					role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2720 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEBLOODPRESSURE_SUPPORT_SENSOR
 * Defines if the BLE BLOODPRESSURE SENSOR Profile implementation supports the 
 * SENSOR Role
 * If enabled ( set to 1 ) it enables all the following BLE BLOODPRESSURE 
 * SENSOR Profile SENSOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEBLOODPRESSURE_SUPPORT_SENSOR
#define BLEBLOODPRESSURE_SUPPORT_SENSOR					0
#endif //BLEBLOODPRESSURE_SUPPORT_SENSOR

#if (BLEBLOODPRESSURE_SUPPORT_SENSOR== 1)
//
// Then check dependencies
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleBloodPressure_Service.h"


// Blood Pressure Service is mandatory
#if (BLE_SUPPORT_BLOODPRESSURE_SERVICE == 0)
#error BLE_SUPPORT_BLOODPRESSURE_SERVICE shall be enabled when BleBloodPressure SENSOR Role is enabled
#endif //(BLE_SUPPORT_BLOODPRESSURE_SERVICE == 0)

// device Information Service is mandatory
#if (BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)
#error BLE_SUPPORT_DEVICEINFORMATION_SERVICE shall be enabled when BleBloodPressure SENSOR Role is enabled
#endif //(BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)

#if (BLEDEVICEINFORMATION_SUPPORT_SYSTEMID == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_SYSTEMID shall be enabled when BleBloodPressure SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_SYSTEMID == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER shall be enabled when BleBloodPressure SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME shall be enabled when BleBloodPressure SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 



/***************************************************************************\
 * OPTIONAL FEATURES FOR BLOODPRESSURE SENSOR PROFILE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE BLOOD PRESSURE profile in SENSOR Role.
 *
 * BLEBLOODPRESSURE_SENSOR_Register()
 *	This function is used to register and initialise a BLE BLOOD PRESSURE profile 
 *  in SENSOR role.
 *  It will add the supported BLOOD PRESSURE Service characteristics and
 *  the supported DEVICE INFORMATION Service characteristics into the 
 *  Attribute database.
 *	This Interface shall be the entry point of a BLE BLOOD PRESSURE profile
 *  in SENSOR Role.
 *
 * BLEBLOODPRESSURE_SUPPORT_SENSOR shall be enabled.
 *
 * @param : none
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_SENSOR_Register( void );


/***************************************************************************\
 * OPTIONAL API functions definition BLOODPRESSURE SENSOR PROFILE
\***************************************************************************/

#endif //(BLEBLOODPRESSURE_SUPPORT_SENSOR== 1)

#endif /*__BLEBLOODPRESSURE_SENSOR_H*/
