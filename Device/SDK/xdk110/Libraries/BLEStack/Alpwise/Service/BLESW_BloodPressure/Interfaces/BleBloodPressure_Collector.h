#ifndef __BLEBLOODPRESSURE_COLLECTOR_H
#define __BLEBLOODPRESSURE_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleBloodPressure_Collector.h
 *
 * Description:   Contains interfaces definition for Blood Pressure 
 *					Profile when the local device operates in the COLLECTOR
 *					role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2720 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "BleEngine.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR
 * Defines if the BLE BLOOD PRESSURE Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE BLOOD 
 * PRESSURE Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEBLOODPRESSURE_SUPPORT_COLLECTOR
#define BLEBLOODPRESSURE_SUPPORT_COLLECTOR							0
#endif //BLEBLOODPRESSURE_SUPPORT_COLLECTOR

#if (BLEBLOODPRESSURE_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when BLOOD PRESSURE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE BLOOD PRESSURE Profile implementation in 
 * COLLECTOR role supports saving the BLOOD PRESSURE SENSOR information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the BLOOD PRESSURE SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and Blood Pressure 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * it saves 7 bytes into persistent memory
 *
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION


#define BLEINFOTYPE_BLOODPRESSURE_REMOTEFEATURES					0x57	//1 byte
#define BLEINFOTYPE_BLOODPRESSURE_BLOODPRESSUREMEASUREMENTHANDLE	0x58	//2 bytes
#define BLEINFOTYPE_BLOODPRESSURE_BLOODPRESSUREFEATUREHANDLE		0x59	//2 bytes


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/
//TOCHANGE : Event numbers 
/** 
 * BleBloodPressureCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleBloodPressureCollectorCallBack
 */
typedef	U8	BleBloodPressureCollectorEvent;

/** BLEBLOODPRESSURECOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleBloodPressureCollectorLinkUPInfo pointer.
 */
#define BLEBLOODPRESSURECOLLECTOR_EVENT_LINKED						0x9A

/** BLEBLOODPRESSURECOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleBloodPressureCollectorLinkUPInfo pointer.
 */
#define BLEBLOODPRESSURECOLLECTOR_EVENT_UNLINKED					0x9B

/** BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a blood pressure
 * SENSOR measurement from the SENSOR.
 * It follows an alert from the Sensor
 * The parms field indicates the blood pressure information within the 
 * BleBloodPressureCollectorBloodPressureMeasurementInfo type.
 */
#define BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREMEASUREMENT	0x9C

/** BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREFEATURE
 * Event received to indicate that this COLLECTOR has received a blood pressure
 * SENSOR blood pressure feature from the SENSOR.
 * It follows the call of BLEBLOODPRESSURE_COLLECTOR_ReadBloodPressureFeature().
 * The parms field indicates the Sensor blood pressure feature information  
 * within the BleBloodPressureCollectorBloodPressureFeatureInfo type.
 */
#define BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREFEATURE		0x9D

/** 
 * BleBloodPressureCollectorMeasurementFlag type
 * Define the different kind of flag that could be received by the 
 * BleBloodPressureCollectorCallBack during the 
 * BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREFEATURE event, it is a bitfield 
 * that describe  which field are included within this event.
 */
typedef U8 BleBloodPressureCollectorMeasurementFlag;
#define BLEBLOODPRESSURE_MEASUREMENT_FLAG_UNIT_VALUE				(0x01u)
#define BLEBLOODPRESSURE_MEASUREMENT_FLAG_TIMESTAMP					(0x02u)
#define BLEBLOODPRESSURE_MEASUREMENT_FLAG_PULSE_RATE				(0x04u)
#define BLEBLOODPRESSURE_MEASUREMENT_FLAG_USER_ID					(0x08u)
#define BLEBLOODPRESSURE_MEASUREMENT_FLAG_MEASUREMENT_STATUS		(0x10u)

/** 
 *BleBloodPressureCollectorFeatureBitfield type
 * Bitfield defining the remote blood pressure SENSOR capabilities
 * it defines after the linkUP has completed, what are the 
 * blood pressure specification optional defined features supported by
 * the remote device.
 */
typedef	U16	BleBloodPressureCollectorFeatureBitfield;
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_BODYMOVEMENT			0x0001
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_CUFFFIT				0x0002
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_IRREGULARPULSE			0x0004
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_PULSERATERANGE			0x0008
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_MEASUREMENTPOSITION	0x0010
#define BLEBLOODPRESSURECOLLECTOR_FEATUREBITMASK_MULTIPLEBOND			0x0020


/**
 * BleBloodPressureCollectorCallBack
 *	This callback receives the BLE BLOOD PRESSURE COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleBloodPressureCollectorCallBack)(
	BleBloodPressureCollectorEvent	event,
	BleStatus					status,
	void					   *parms); 

/** 
 * BleBloodPressureCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLEBLOODPRESSURE_COLLECTOR_LinkUpBloodPressureProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEBLOODPRESSURE_COLLECTOR_LinkUpBloodPressureProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLEBLOODPRESSURECOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Blood Pressure SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								bloodPressureServiceStartingHandle;
	AttHandle								bloodPressureServiceEndingHandle;

	AttHandle								bloodPressureMeasurementValueHandle;
	AttHandle								bloodPressureMeasurementConfigHandle;
	U8										value[2];

	AttHandle								bloodPressureIntermediateCuffPressureHandle;

	AttHandle								bloodPressureFeatureValueHandle;

	U8										bloodPressureSensorFeatures;

} BleBloodPressureCollector;


/** 
 * BleBloodPressureCollectorLinkUPInfo type
 * structure passed in parameter in the BleBloodPressureCollectorCallBack
 * parms fields during the BLEBLOODPRESSURECOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleBloodPressureCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * remoteSensorFeatures : The supported features of the remote sensor
 */
typedef struct {

	BleBloodPressureCollector					*bleBloodPressureCollector;
	U16											connHandle;
	U8											remoteSensorFeatures;

} BleBloodPressureCollectorLinkUPInfo;


typedef struct {

	BleBloodPressureCollector					*bleBloodPressureCollector;
	U16											connHandle;
	BleBloodPressureCollectorFeatureBitfield	bloodPressureCollectorFeatureBitfield;

} BleBloodPressureCollectorBloodPressureFeatureInfo;


/** 
 * BleBloodPressureCollectorBloodPressureMeasurementInfo type
 * structure passed in parameter in the BleBloodPressureCollectorCallBack
 * parms fields during the BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREMEASUREMENT event.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleBloodPressureCollector			*bleBloodPressureCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16									connHandle;

	BleBloodPressureCollectorMeasurementFlag		flags;

	U16									systolicCompoundValue;
	U16									diastolicCompoundValue;
	U16									mapCompoundValue;

	U8									*timeStamp;

	/*
	 * the pulse Rate measurement Value, in beats per minute (bpm)
	 */
	U16									pulseRateValue;


	/*
	* The measurement status field is a bitfield representing the blood pressure
	* measurement flags
	*/
	U16									measurementStatusValue;


	U8									userIdValue;

} BleBloodPressureCollectorBloodPressureMeasurementInfo;


/** 
 * BleBloodPressureCollectorLinkMode type
 * Type passed during the 
 * BLEBLOODPRESSURE_COLLECTOR_LinkUpBloodPressureProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEBLOODPRESSURE_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLEBLOODPRESSURE_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleBloodPressureCollectorLinkMode;

#define BLEBLOODPRESSURE_COLLECTOR_LINKUP_COMPLETE			1
#define BLEBLOODPRESSURE_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Blood Pressure profile 
 *
 * BLEBLOODPRESSURE_COLLECTOR_Init()
 *	This function is the entry point of a Blood Pressure profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleBloodPressureCollectorCallBack in which the collector 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_COLLECTOR_Init(
			BleBloodPressureCollectorCallBack callback);

/** De-Init the Collector Role of the Blood Pressure Profile.
 *
 * BLEBLOODPRESSURE_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Blood Pressure Profile.
 *
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_COLLECTOR_Deinit( void ) ;

/** Link Up the Blood Pressure Profile with the specified remote Device
 *
 * BLEBLOODPRESSURE_COLLECTOR_LinkUpBloodPressureProfile()
 *	This function is used to link-Up the Blood Pressure Profile with the 
 *  specified remote device.
 *  It will try to discover the Blood Pressure profile in the remote device 
 *  using the given collector.
 * BLEBLOODPRESSURECOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Blood Pressure COLLECTOR to link-up
 *					with a remote Blood Pressure SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to the profile API in order to identify to which linked 
 *                  SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE BLOOD PRESSURE COLLECTOR and the remote BLE BLOOD PRESSURE
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLEBLOODPRESSURE_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleBloodPressureCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *			BLEBLOODPRESSURECOLLECTOR_EVENT_LINKED event will be received in the
 *			callback registered during the call of
 *			BLEBLOODPRESSURE_COLLECTOR_Init. This status is returned only if mode
 *			is BLEBLOODPRESSURE_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEBLOODPRESSURE_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEBLOODPRESSURE_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLEBLOODPRESSURECOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Blood Pressure SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Blood Pressure Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_COLLECTOR_LinkUpBloodPressureProfile(
						BleBloodPressureCollector *collector,
						U16 connHandle,
						BleBloodPressureCollectorLinkMode linkUpMode,
						BleBloodPressureCollectorLinkUPInfo *info
						);

/** Un Link the Blood Pressure Profile with the specified linked COLLECTOR
 *
 * BLEBLOODPRESSURE_COLLECTOR_UnLinkBloodPressureProfile()
 *	This function is used to UnLink the Blood Pressure Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Blood Pressure Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_BLOODPRESSURE_* from persistent memory
 *
 * BLEBLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Blood Pressure COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_COLLECTOR_UnLinkBloodPressureProfile(
		BleBloodPressureCollector *collector);


/** Get the current blood pressure feature from a linked SENSOR
 *
 * BLEBLOODPRESSURE_COLLECTOR_ReadBloodPressureFeature()
 *	This function is used to read the blood pressure feature
 *  supported by the sensor
 *   
 * BLOODPRESSURE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked BLOOD PRESSURE COLLECTOR. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
 *		completed the BLEBLOODPRESSURECOLLECTOR_EVENT_BLOODPRESSUREFEATURE  
 *		event will be received in the callback with the status of the 
  *		operation and the blood pressure feature information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other BLOOD PRESSURE profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the BLOOD PRESSURE profile seems not been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLEBLOODPRESSURE_COLLECTOR_ReadBloodPressureFeature( 
	BleBloodPressureCollector *collector );

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLEBLOODPRESSURE_SUPPORT_COLLECTOR== 1)

#endif /*__BLEBLOODPRESSURE_COLLECTOR_H*/
