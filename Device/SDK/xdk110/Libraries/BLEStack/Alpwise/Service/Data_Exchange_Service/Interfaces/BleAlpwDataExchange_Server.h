#ifndef __BLEALPWDATAEXCHANGE_SERVER_H
#define __BLEALPWDATAEXCHANGE_SERVER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleAlpwDataExchange_Server.h
 *
 * Description:   Contains interfaces routines for AlpwDataExchange Profile
 *					when the local device operates in the SERVER role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2719 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"

/***************************************************************************\
 *	Macro and constant definition
\***************************************************************************/

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEALPWDATAEXCHANGE_SUPPORT_SERVER
 * Defines if the BLE ALPWDATAEXCHANGE Profile implementation supports the 
 * SERVER Role
 * If enabled ( set to 1 ) it enables all the following BleAlpwDataExchange
 * SERVER configuration and Application Interface
 * The default value for this option is disabled (0).
 */
#ifndef BLEALPWDATAEXCHANGE_SUPPORT_SERVER
#define BLEALPWDATAEXCHANGE_SUPPORT_SERVER								0
#endif //BLEALPWDATAEXCHANGE_SUPPORT_SERVER

#if (BLEALPWDATAEXCHANGE_SUPPORT_SERVER == 1)

// Then check dependencies
#include "Services/BleAlpwDataExchange_Service.h"

// Immediate Alert Service is mandatory
#if (BLE_SUPPORT_ALPWDATAEXCHANGE_SERVICE == 0)
#error BLE_SUPPORT_ALPWDATAEXCHANGE_SERVICE shall be enabled when BleAlpwDataExchange SERVER Role is enabled
#endif //(BLE_SUPPORT_IMMEDIATEALERT_SERVICE == 0)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/***************************************************************************\
 * CONSTANTS
\***************************************************************************/

/***************************************************************************\
 *	Type definition
\***************************************************************************/



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/* Register a BLE ALPWDATAEXCHANGE Server.
 *
 * BLEALPWDATAEXCHANGE_SERVER_Register()
 *	This function is used to register and initialise a BLE ALPWDATAEXCHANGE 
 * SERVER role.
 * It will add the supported alpwise DataExchange characteristics into the Attribute
 * database.
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_SERVER shall be enabled.
 *
 * alpwDataExchangeCallBack : The BleAlpwDataExchangeCallBack in which the 
 *				 SERVER events will be received.
 *
 * return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 */
BleStatus BLEALPWDATAEXCHANGE_SERVER_Register( 
			BleAlpwDataExchangeCallBack	alpwDataExchangeCallBack );

/** This function is used to send some data to the CLIENT device.
 *
 * BLEALPWDATAEXCHANGE_SERVER_SendData()
 *	This function is used to send some data to the CLIENT device.
 *
 * BLE_SUPPORT_ALPWDATAEXCHANGE_SERVICE shall be enabled.
 *
 * @param connHandle: the connection Handle identifying the AlpwDataExchange
 *			CLIENT device
 * @param txData : data to send
 * @param txDataLen : Length of the data to send, shall not exceed 20 bytes
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded.
 *		BLEALPWDATAEXCHANGE_EVENT_TXCOMPLETE will be received by the application
 *		when data has been acknowledged by the client or when internal timeout 
 *		occurs.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed, merely because
 * Alpwise Data Exchange Service is not registered by any profile.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_SERVER_SendData(U16 connHandle,
											  U8 *txData,
											  U8 txDataLen);
#endif //(BLEALPWDATAEXCHANGE_SUPPORT_SERVER== 1)

#endif //__BLEALPWDATAEXCHANGE_SERVER_H
