#ifndef __BLEALPWDATAEXCHANGE_CLIENT_H
#define __BLEALPWDATAEXCHANGE_CLIENT_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleAlpwDataExchange_Client.h
 *
 * Description:   Contains interfaces definition for AlpwDataExchange Profile
 *					when the local device operates in the CLIENT role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2733 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT
 * Defines if the BLE ALPWDATAEXCHANGE Profile implementation supports the 
 * CLIENT Role.
 * If enabled ( set to 1 ) it enables all the following BleAlpwDataExchange CLIENT
 * configuration and Application Interface.
 * The default value for this option is disabled (0). 
 */
#ifndef BLEALPWDATAEXCHANGE_SUPPORT_CLIENT
#define BLEALPWDATAEXCHANGE_SUPPORT_CLIENT								0
#endif //BLEALPWDATAEXCHANGE_SUPPORT_CLIENT


#if (BLEALPWDATAEXCHANGE_SUPPORT_CLIENT == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when AlpwDataExchange CLIENT Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when AlpwDataExchange CLIENT Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when AlpwDataExchange CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when AlpwDataExchange CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

// GATT Write request is mandatory
#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when ALPWDATAEXCHANGE CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when ALPWDATAEXCHANGE CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLEALPWDATAEXCHANGE Profile CLIENT supports 
 * saving the SERVER information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the CLIENT saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SERVER information.
 * Upon reconnection with the SERVER, as these information are already known
 * then the linkUp process can be simplified, and RX Notification
 * could be automaticaly understood by this client.
 * There is 8 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION	
#define BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION					0
#endif //BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_BLEALPWDATAEXCHANGE_RXHANDLE						0x6A	// 2 bytes
#define BLEINFOTYPE_BLEALPWDATAEXCHANGE_TXHANDLE						0x6B	// 2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported and 
// BLEALPWDATAEXCHANGE_SUPPORT_BATTERYLEVELSTATEALERT is not supported force 
// the BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION
#define BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 

/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BleAlpwDataExchangeEvent type
 * Define the different kind of events that could be received by the 
 * BleAlpwDataExchangeCallBack
 */
typedef	U8	BleAlpwDataExchangeClientEvent;

/** BLEALPWDATAEXCHANGECLIENT_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleAlpwDataExchangeClientLinkUpInfo pointer.
 */
#define BLEALPWDATAEXCHANGECLIENT_EVENT_LINKED									0xA0

/** BLEALPWDATAEXCHANGECLIENT_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleAlpwDataExchangeClientLinkUpInfo pointer.
 */
#define BLEALPWDATAEXCHANGECLIENT_EVENT_UNLINKED								0xA1

/** BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE
 * Event received when the write process is completed, the status field indicates the status
 * of the operation.
 * The parms field indicates the write complete information within a
 * BleAlpwDataExchangeClientWriteCompleteInfo pointer.
 */
#define BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE							0xA2

/** BLEALPWDATAEXCHANGECLIENT_EVENT_RXDATA
 * Event received when a new data is receieved
 * The parms field indicates the data information within the 
 * BleAlpwDataExchangeClientRxData type
 */
#define BLEALPWDATAEXCHANGECLIENT_EVENT_RXDATA									0xA3

/**
 * BleAlpwDataExchangeCallBack
 *	This callback receives the BLE ALPWDATAEXCHANGE CLIENT profile events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleAlpwDataExchangeClientCallBack)(BleAlpwDataExchangeClientEvent event,
										 BleStatus status,
										 void* parms); 

/** 
 * BleAlpwDataExchangeClient type
 * Define a local CLIENT, it represent a memory block
 * that should be passed by the application during the 
 * BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile() API ,and shall 
 * be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this CLIENT 
 * receive the BLEALPWDATAEXCHANGECLIENT_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this CLIENT memory block while linked
 * with a remote SERVER. It is used internally by the profile and shall not be
 * accessed by the application.
 * A pointer to this local CLIENT could be then passed as parameter to profile
 * API in order to identify to which linked SERVER the operation is destinated.
 *
 * A CLIENT is unique per remote AlpwDataExchange SERVER, it means that if 
 * this local device is connected to multiple remote SERVER the application
 * shall register multiple CLIENTs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;
	BleGattCommand			AckGattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	U8						serverFeatures;

	AttHandle				dtxServiceStartingHandle;
	AttHandle				dtxServiceEndingHandle;

	AttHandle				serverRxDataHandle;
	AttHandle				serverTxDataHandle;
    AttHandle               serverTxDataConfigHandle;

    U8                      value[2];
} BleAlpwDataExchangeClient;


/** 
 * BleAlpwDataExchangeClientLinkUpInfo type
 * Structure passed in parameter in the BleAlpwDataExchangeClientCallBack
 * parms fields during the BLEALPWDATAEXCHANGECLIENT_EVENT_LINKED and
 * BLEALPWDATAEXCHANGECLIENT_EVENT_UNLINKED events.
 * It indicates :
 * BleAlpwDataExchangeClient : The local CLIENT from which the event is coming.
 * connHandle : The connection handle from which the Link Up information
 *	is coming.
 */
typedef struct {

	BleAlpwDataExchangeClient				*bleAlpwDataExchangeClient;
	U16										connHandle;

} BleAlpwDataExchangeClientLinkUpInfo;

/** 
 * BleAlpwDataExchangeClientLinkMode type
 * Type passed during the BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (ALPWDATAEXCHANGE_CLIENT_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (ALPWDATAEXCHANGE_CLIENT_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleAlpwDataExchangeClientLinkMode;

#define BLEALPWDATAEXCHANGE_CLIENT_LINKUP_COMPLETE			1
#define BLEALPWDATAEXCHANGE_CLIENT_LINKUP_RESTORE			2

/** 
 * BleAlpwDataExchangeClientReliability type
 * Type passed during the BLEALPWDATAEXCHANGE_CLIENT_SendData API call and
 * during the BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile API 
 * call.
 * It defines the reliability of the packet to send or to receive. Reliable
 * packets (BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE) use acknowledged 
 * packets, but reduce the bit rate. Unreliable packet may be lost without
 * prompt but the bit rate is higher than using Reliable way.
 *
 * During the BLEALPWDATAEXCHANGE_CLIENT_SendData
 * - If the packet is sent using BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE, 
 *   the packet is sent using a reliable way, meaning that the packet is 
 *   acknowledged by the remote Device Alpwise data exchange profile. 
 *   The BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE event is received when
 *   the acknowledgement is received.
 * - If the packet is sent using BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE,
 *   the packet is sent using a non-reliable way, meaning that the packet is
 *   NOT acknowledged by the remote Device Alpwise data exchange profile and 
 *   may be lost. The BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE event is 
 *   received when the packet is sent over the air. In this case, at this 
 *   layer, there is now chance to know if the remote application has received
 *   this packet.
 *
 * During the BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile, it 
 * defines the way how the remote SERVER device is configured to send its own  
 * data to this client, as it is up to this CLIENT to configure how the SERVER 
 * must send the packet.
 * - If the remote device is configured using 
 *   BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE, the remote device will send 
 *   data packet using a reliable way, meaning that the packet is acknowledged
 *   by the local client. 
 * - If the remote device is configured using 
 *   BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE, the remote device will send
 *   data using a non-reliable way, meaning that the packet is NOT acknowledged
 *   by the local Device Alpwise data exchange profile and may be lost without
 *   the local device is receiving the packet.
 * 
 */
typedef U8 BleAlpwDataExchangeClientReliability;

#define BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE		0
#define BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE    		1


/** 
 * BleAlpwDataExchangeClientWriteCompleteInfo type
 * Define what is received by the 
 * BleAlpwDataExchangeCallBack during the 
 * BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE event
 * It indicates :
 * BleAlpwDataExchangeClient : The local CLIENT from which the event is coming.
 * connHandle : The connection handle from which the information
 *	is coming.
 */
typedef struct {

	BleAlpwDataExchangeClient				*bleAlpwDataExchangeClient;
	U16										connHandle;

} BleAlpwDataExchangeClientWriteCompleteInfo;

/** 
 * BleAlpwDataExchangeClientRxData type
 * Define what is received by the 
 * BleAlpwDataExchangeCallBack during BLEALPWDATAEXCHANGECLIENT_EVENT_RXDATA event
 * It indicates :
 * BleAlpwDataExchangeClient : The local CLIENT from which the event is coming.
 * connHandle : The connection handle from which the data information
 *	is coming.
 * rxData : The data received
 * rxDataLen : The data length received
 */
typedef struct{

	BleAlpwDataExchangeClient  *bleAlpwDataExchangeClient;
	U16	connHandle;
	U8	*rxData;
	U8	rxDataLen;

} BleAlpwDataExchangeClientRxData;



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Init the AlpwDataExchange CLIENT profile
 *
 * BLEALPWDATAEXCHANGE_CLIENT_Init()
 *	This function is the entry point of the AlpwDataExchange CLIENT profile, it 
 *	inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT shall be enabled
 *
 * @param callback: The BleAlpwDataExchangeClientCallBack in which the CLIENT 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_CLIENT_Init(
        BleAlpwDataExchangeClientCallBack   callback);

/** De-Init the AlpwDataExchange CLIENT profile
 *
 * BLEALPWDATAEXCHANGE_CLIENT_Deinit
 *	This function is used to De-Init the AlpwDataExchange CLIENT profile.
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_CLIENT_Deinit( void );


/** Link Up the AlpwDataExchange CLIENT Profile with the specified remote Device
 *
 * BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile()
 *	This function is used to link-Up the AlpwDataExchange CLIENT profile with  
 *  the specified remote device.
 *  It will try to discover the AlpwDataExchange SERVER profile in the remote device 
 *  using the given CLIENT.
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT shall be enabled
 *
 * @param bleAlpwDataExchangeClient: A valid AlpwDataExchange CLIENT to link-up
 *					with a remote AlpwDataExchange SERVER. This memory block
 *                  is allocated by the application prior to call this API and
 *                  shall stay valid while the devices are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this CLIENT could be then passed as parameter  
 *                  to profile API in order to identify to which linked SERVER
 *                  the operation is destinated.
 * @param connHandle: The connection Handle on which the link up between this
 *		 bleAlpwDataExchangeClient and the remote AlpwDataExchange SERVER profile
 *		 shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 * @param isReliableRXData : isReliableRXData defines how the SERVER will sent
 *      data, by using and acknowledged request or an unacknowledged request.
 *     - If the remote SERVER device is configured using 
 *       BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE, the remote device will send
 *       data packet using a reliable way, meaning that the packet is 
 *       acknowledged by the local client. 
 *     - If the remote device is configured using 
 *       BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE, the remote device will 
 *       send data using a non-reliable way, meaning that the packet is NOT 
 *       acknowledged by the local Device Alpwise data exchange profile and 
 *       may be lost without the local device is receiving the packet.
 *     This parameter is ignored if mode is 
 *     BLEALPWDATAEXCHANGE_CLIENT_LINKUP_RESTORE
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEALPWDATAEXCHANGECLIENT_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode is 
 *		BLEALPWDATAEXCHANGE_CLIENT_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEALPWDATAEXCHANGE_CLIENT_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEALPWDATAEXCHANGE_CLIENT_LINKUP_RESTORE, it means that 
 *		BLEALPWDATAEXCHANGE_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote AlpwDataExchange SERVER.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other AlpwDataExchange Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_CLIENT_LinkUpAlpwDataExchangeProfile(
			BleAlpwDataExchangeClient               *bleAlpwDataExchangeClient,
			U16                                     connHandle,
			BleAlpwDataExchangeClientLinkMode       mode,
            BleAlpwDataExchangeClientReliability    isReliableRXData);


/** Un Link the AlpwDataExchange Profile with the specified linked CLIENT
 *
 * BLEALPWDATAEXCHANGE_CLIENT_UnLinkAlpwDataExchangeProfile()
 *	This function is used to UnLink the AlpwDataExchange Profile with the 
 *  specified previously linked CLIENT.
 *  This CLIENT wil not receive any more information from the linked 
 *	SERVER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_BLEALPWDATAEXCHANGE_* from persistent memory
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT shall be enabled
 *
 * @param bleAlpwDataExchangeClient: A linked AlpwDataExchange CLIENT 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the client memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_CLIENT_UnLinkAlpwDataExchangeProfile(
		BleAlpwDataExchangeClient *bleAlpwDataExchangeClient);


/** Send the data to the remote AlpwDataExchange SERVER
 *
 * BLEALPWDATAEXCHANGE_CLIENT_SendData()
 *	This function send the data to the remote linked AlpwDataExchange SERVER
 *
 * BLEALPWDATAEXCHANGE_SUPPORT_CLIENT shall be enabled
 *
 * @param bleAlpwDataExchangeClient: A linked AlpwDataExchange CLIENT
 * @param data : data to send
 * @param dataLen : Length of the data to send, shall not exceed 20 bytes
 * @param isReliableTXData : isReliableTXData defines if the data is sent using an
 *  acknowledged request or an unacknowledged request.
 * - If the packet is sent using BLEALPWDATAEXCHANGE_CLIENT_DATA_RELIABLE, 
 *   the packet is sent using a reliable way, meaning that the packet is 
 *   acknowledged by the remote Device Alpwise data exchange profile. 
 *   The BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE event is received when
 *   the acknowledgement is received.
 * - If the packet is sent using BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE,
 *   the packet is sent using a non-reliable way, meaning that the packet is
 *   NOT acknowledged by the remote Device Alpwise data exchange profile and 
 *   may be lost. The BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE event is 
 *   received when the packet is sent over the air. In this case, at this 
 *   layer, there is now chance to know if the remote application has received
 *   this packet. 
 *  Note that BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be 
 *  supported when using BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE, 
 *  otherwise the API will always return BLESTATUS_FAILED.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation have successfully started
 *  BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE will be received when the 
 *  operation is complete.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed, it may be du
 * because :
 *      - There is no more buffer available to sent the packet, wait for some
 * BLEALPWDATAEXCHANGECLIENT_EVENT_WRITECOMPLETE events. 
 *      - The BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE option is
 * not enabled and the application try to send data using 
 * BLEALPWDATAEXCHANGE_CLIENT_DATA_NOT_RELIABLE parameter.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other AlpwDataExchange Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (bleAlpwDataExchangeClient is not linked or
 *		txDataLen is greater than 20)
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEALPWDATAEXCHANGE_CLIENT_SendData(
	BleAlpwDataExchangeClient		        *bleAlpwDataExchangeClient,
	U8								        *txData,
	U8								        txDataLen,
    BleAlpwDataExchangeClientReliability    isReliableTXData );

#endif //(BLEALPWDATAEXCHANGE_SUPPORT_CLIENT== 1)

#endif //__BLEALPWDATAEXCHANGE_CLIENT_H
