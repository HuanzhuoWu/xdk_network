#ifndef __BLEHEALTHTHERMOMETER_THERMOMETER_H
#define __BLEHEALTHTHERMOMETER_THERMOMETER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleHealthThermometer_Thermometer.h
 *
 * Description:   Contains interfaces definitions for Health Thermometer 
 *					Profile when the local device operates in the THERMOMETER
 *					role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER
 * Defines if the BLE HEALTH THERMOMETER Profile implementation supports the 
 * THERMOMETER Role
 * If enabled ( set to 1 ) it enables all the following BLE HEALTH 
 * THERMOMETER Profile THERMOMETER configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER
#define BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER					0
#endif //BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER

#if (BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER== 1)

// Then check dependencies
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleHealthThermometer_Service.h"


// Health Thermometer Service is mandatory
#if (BLE_SUPPORT_HEALTHTHERMOMETER_SERVICE == 0)
#error BLE_SUPPORT_HEALTHTHERMOMETER_SERVICE shall be enabled when BlehealthThermometer THERMOMETER Role is enabled
#endif //(BLE_SUPPORT_HEALTHTHERMOMETER_SERVICE == 0)

// device Information Service is mandatory
#if (BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)
#error BLE_SUPPORT_DEVICEINFORMATION_SERVICE shall be enabled when BlehealthThermometer THERMOMETER Role is enabled
#endif //(BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)

#if (BLEDEVICEINFORMATION_SUPPORT_SYSTEMID == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_SYSTEMID shall be enabled when BlehealthThermometer THERMOMETER Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_SYSTEMID == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER shall be enabled when BlehealthThermometer THERMOMETER Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME shall be enabled when BlehealthThermometer THERMOMETER Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 



/***************************************************************************\
 * OPTIONAL FEATURES FOR HEALTH THERMOMETER SERVICE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE HEALTH THERMOMETER in THERMOMETER Role.
 *
 * BLEHEALTHTHERMOMETER_THERMOMETER_Register()
 *	This function is used to register and initialise a BLE HEALTH THERMOMETER 
 *  in THERMOMETER role.
 *  It will add the supported HEALTH THERMOMETER Service characteristics and
 *  the supported DEVICE INFORMATION Service characteristics into the 
 *  Attribute database.
 *	This Interface shall be the entry point of a BLE HEALTH THERMOMETER
 *  in THERMOMETER Role.
 *
 *	@see BLEHEALTHTHERMOMETER_THERMOMETER_Deregister()
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER shall be enabled.
 *
 * @param none.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEALTHTHERMOMETER_THERMOMETER_Register( void );


/***************************************************************************\
 * OPTIONAL API functions definition HEALTH THERMOMETER PROFILE
\***************************************************************************/

#endif //(BLEHEALTHTHERMOMETER_SUPPORT_THERMOMETER== 1)

#endif /*__BLEHEALTHTHERMOMETER_THERMOMETER_H*/
