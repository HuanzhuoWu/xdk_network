#ifndef __BLEHEALTHTHERMOMETER_COLLECTOR_H
#define __BLEHEALTHTHERMOMETER_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleHealthThermometer_Collector.h
 *
 * Description:   Contains interfaces definition for Health Thermometer 
 *					Profile when the local device operates in the COLLECTOR
 *					role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "BleEngine.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR
 * Defines if the BLE HEALTH THERMOMETER Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE HEALTH 
 * THERMOMETER Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR
#define BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR							0
#endif //BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR

#if (BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE HEALTH THERMOMETER Profile implementation in 
 * COLLECTOR role supports saving the Thermometers information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the THERMOMETER information.
 * Upon reconnection with the THERMOMETER, as these information are already known
 * then the linkUp process can be simplified, and Temperature Notification
 * could be automaticaly understood by this collector.
 * it saves 9 bytes into persistent memory
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION	
#define BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION					0
#endif //BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION


#define BLEINFOTYPE_HEALTHTHERMOMETER_TEMPERATUREHANDLE				0x52	//2 bytes
#define BLEINFOTYPE_HEALTHTHERMOMETER_REMOTECONFIGURATIONHANDLE		0x53	//2 bytes
#define BLEINFOTYPE_HEALTHTHERMOMETER_REMOTETEMPERATURETYPE			0x54	//1 byte


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION
#define BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/

/** 
 * BleHealthThermometerTemperatureType type
 * Define the different kind of temperature type that could be received 
 * by the BleHealthThermometerCallBack during the 
 * BLEHEALTHTHERMOMETER_EVENT_TEMPERATURE event or if static over the 
 * connexion during the BLEHEALTHTHERMOMETER_EVENT_LINKED event.
 * It is a Byte that describe the temperature measurement site.
 */
typedef	U8	BleHealthThermometerTemperatureType;
#define BLEHEALTHTHERMOMETER_TYPE_ARMPIT			0x01
#define BLEHEALTHTHERMOMETER_TYPE_BODY				0x02
#define BLEHEALTHTHERMOMETER_TYPE_EAR				0x03
#define BLEHEALTHTHERMOMETER_TYPE_FINGER			0x04
#define BLEHEALTHTHERMOMETER_TYPE_GASTROINTESTINAL	0x05
#define BLEHEALTHTHERMOMETER_TYPE_MOUTH				0x06
#define BLEHEALTHTHERMOMETER_TYPE_RECTUM			0x07
#define BLEHEALTHTHERMOMETER_TYPE_TOE				0x08
#define BLEHEALTHTHERMOMETER_TYPE_TYMPANUM			0x09

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/

/** 
 * BleHealthThermometerEvent type
 * Define the different kind of events that could be received by the 
 * BleHealthThermometerCallBack
 */
typedef	U8	BleHealthThermometerEvent;

/** BLEHEALTHTHERMOMETER_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleHealthThermometerLinkUPInfo pointer.
 */
#define BLEHEALTHTHERMOMETER_EVENT_LINKED						0x88

/** BLEHEALTHTHERMOMETER_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleHealthThermometerLinkUPInfo pointer.
 */
#define BLEHEALTHTHERMOMETER_EVENT_UNLINKED						0x89

/** BLEHEALTHTHERMOMETER_EVENT_TEMPERATURE
 * Event received to indicate the THERMOMETER temperature Level.
 * It follows an alert from the Thermometer.
 * The parms field indicates the temperature information within the 
 * BleHealthThermometerTemperatureInfo type.
 */
#define BLEHEALTHTHERMOMETER_EVENT_TEMPERATURE					0x8A


/** 
 * BleHealthThermometerFlag type
 * Define the different kind of flag that could be received by the 
 * BleHealthThermometerCallBack during the 
 * BLEHEALTHTHERMOMETER_EVENT_TEMPERATURE event, it is a bitfield that
 * describe the unit of the value wich field are included within this
 * event.
 */
typedef	U8	BleHealthThermometerFlag;
#define BLEHEALTHTHERMOMETER_FLAG_FARHENHEIT					0x01
#define BLEHEALTHTHERMOMETER_FLAG_TIMESTAMP						0x02
#define BLEHEALTHTHERMOMETER_FLAG_MEASUREMENTSITE				0x04


/**
 * BleHealthThermometerCallBack
 *	This callback receives the BLE HEALTH THERMOMETER COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleHealthThermometerCallBack)(
	BleHealthThermometerEvent	event,
	BleStatus					status,
	void					   *parms); 

/** 
 * BleHealthThermometerCollector type
 * Define a local COLLECTOR, it represent a memory block
 * that should be passed by the application during the 
 * BLEHEALTHTHERMOMETER_COLLECTOR_LinkUpHealthThermometerProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEHEALTHTHERMOMETER_COLLECTOR_LinkUpHealthThermometerProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receive the BLEHEALTHTHERMOMETER_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote THERMOMETER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked THERMOMETER the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote HealthThermometer THERMOMETER, it means 
 * that if this local device is connected to multiple remote THERMOMETER the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								thermometerServiceStartingHandle;
	AttHandle								thermometerServiceEndingHandle;

	AttHandle								temperatureMeasurementValueHandle;
	AttHandle								temperatureMeasurementConfigHandle;
	U8										value[2];

	U8										temperatureMeasurementType;
	AttHandle								temperatureMeasurementTypeHandle;
	U8										thermometerFeatures;

} BleHealthThermometerCollector;


/** 
 * BleHealthThermometerLinkUPInfo type
 * structure passed in parameter in the BleHealthThermometerCallBack
 * parms fields during the BLEHEALTHTHERMOMETER_EVENT_LINKED event.
 * It indicates :
 * bleHealthThermometerCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * temperatureMeasurementType : if the temperature measurement site is static
 * in the THERMOMETER ( can not change during the connexion), it is reported
 * here
 */
typedef struct {

	BleHealthThermometerCollector			*bleHealthThermometerCollector;
	U16										connHandle;
	BleHealthThermometerTemperatureType		temperatureMeasurementType;

} BleHealthThermometerLinkUPInfo;

/** 
 * BleHealthThermometerTemperatureInfo type
 * structure passed in parameter in the BleHealthThermometerCallBack
 * parms fields during the BLEHEALTHTHERMOMETER_EVENT_TEMPERATURE event.
 * It indicates :
 * connHandle : The connection handle from which the temperature information
 *	is coming
 * flags : The flags associated to this temperature value, it indicates the 
 *	unit for this temperature (�C or �F) and which other information is
 *  included
 * value : The temperature measurement in the float format defined in the 
 *  IEEE 11073-20601. 
 *  The Temperature Measurement Value field may contain special float value 
 * Not a Number (NaN)(0x007FFFFF) defined in IEEE 11073-20601 [4] to report 
 * an invalid result from a computation step or missing data due to the 
 * hardware�s inability to provide a valid measurement.
 * measurementSite : If available ( set in flags) the  measurement site for this
 * temperature ( one of the BLEHEALTHTHERMOMETER_TYPE_* defined)
 * timeStamp : If available ( set in flags) the the date-time for this
 * temperature measurement.
 */
typedef struct {

	BleHealthThermometerCollector			*bleHealthThermometerCollector;
	U16										connHandle;
	BleHealthThermometerFlag				flags;
	U32										value;
	BleHealthThermometerTemperatureType		temperatureMeasurementType;
	U8										*timeStamp;

} BleHealthThermometerTemperatureInfo;


/** 
 * BleHealthThermometerCollectorLinkMode type
 * Type passed during the 
 * BLEHEALTHTHERMOMETER_COLLECTOR_LinkUpHealthThermometerProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleHealthThermometerCollectorLinkMode;

#define BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_COMPLETE			1
#define BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Health Thermometer profile 
 *
 * BLEHEALTHTHERMOMETER_COLLECTOR_Init()
 *	This function is the entry point of a Health thermometer profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleHealthThermometerCallBack in which the collector 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEALTHTHERMOMETER_COLLECTOR_Init(
			BleHealthThermometerCallBack callback);

/** De-Init the Collector Role of the Health thermometer Profile.
 *
 * BLEHEALTHTHERMOMETER_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Health thermometer Profile.
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEALTHTHERMOMETER_COLLECTOR_Deinit( void ) ;

/** Link Up the Health Thermometer Profile with the specified remote Device
 *
 * BLEHEALTHTHERMOMETER_COLLECTOR_LinkUpHealthThermometerProfile()
 *	This function is used to link-Up the Health Thermometer Profile with the 
 *  specified remote device.
 *  It will try to discover the Health Thermometer profile in the remote device 
 *  using the given collector.
 * BLEHEALTHTHERMOMETER_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Health Thermometer COLLECTOR to link-up
 *					with a remote Health Thermometer THERMOMETER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  THERMOMETER the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE HEALTH THERMOMETER COLLECTOR and the remote BLE HEALTH  
 *			THERMOMETER THERMOMETER shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information (temperatureMeasurementType)
 *			 upon LinkUp completion.
 *			It may be equal to (BleHealthThermometerLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *			BLEHEALTHTHERMOMETER_EVENT_LINKED event will be received in the
 *			callback registered during the call of
 *			BLEHEALTHTHERMOMETER_COLLECTOR_Init. This status is returned only if mode
 *			is BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEHEALTHTHERMOMETER_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLEHEALTHTHERMOMETER_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Health THERMOMETER. 
 *		It may also indicates that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Health Thermometer Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEALTHTHERMOMETER_COLLECTOR_LinkUpHealthThermometerProfile(
						BleHealthThermometerCollector *collector,
						U16 connHandle,
						BleHealthThermometerCollectorLinkMode linkUpMode,
						BleHealthThermometerLinkUPInfo *info
						);

/** Un Link the Health Thermometer Profile with the specified linked COLLECTOR
 *
 * BLEHEALTHTHERMOMETER_COLLECTOR_UnLinkHealthThermometerProfile()
 *	This function is used to UnLink the Health Thermometer Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	THERMOMETER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_HEALTHTHERMOMETER_* from persistent memory
 *
 * BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Health Thermometer COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEALTHTHERMOMETER_COLLECTOR_UnLinkHealthThermometerProfile(
		BleHealthThermometerCollector *collector);

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLEHEALTHTHERMOMETER_SUPPORT_COLLECTOR== 1)

#endif /*__BLEHEALTHTHERMOMETER_COLLECTOR_H*/
