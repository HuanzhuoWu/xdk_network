#ifndef __BLEHID_DEVICE_H
#define __BLEHID_DEVICE_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleHumanInterfaceDevice_Device.h
 *
 * Description:   Contains interfaces definitions for HUMAN INTERFACE DEVICE 
 *					( HID) Profile when the local device operates in the 
 *					DEVICE role.
 * 
 * Created:       April, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEHID_SUPPORT_DEVICE
 * Defines if the BLESW_HumanInterfaceDevice Profile implementation 
 * supports the DEVICE Role
 * If enabled ( set to 1 ) it enables all the following 
 * BLESW_HumanInterfaceDevice DEVICE Profile configurations and Application 
 * Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEHID_SUPPORT_DEVICE
#define BLEHID_SUPPORT_DEVICE					0
#endif //BLEHID_SUPPORT_DEVICE

#if (BLEHID_SUPPORT_DEVICE== 1)

// Then check dependencies
#include "Services/BleHumanInterfaceDevice_Service.h"
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleBattery_Service.h"

// HID Service is mandatory
#if (BLE_SUPPORT_HID_SERVICE == 0)
#error BLE_SUPPORT_HID_SERVICE shall be enabled when BleHumanInterfaceDevice DEVICE Role is enabled
#endif //(BLE_SUPPORT_HID_SERVICE == 0)

// Battery Service is mandatory
#if (BLE_SUPPORT_BATTERY_SERVICE == 0)
#error BLE_SUPPORT_BATTERY_SERVICE shall be enabled when BleHumanInterfaceDevice DEVICE Role is enabled
#endif //(BLE_SUPPORT_HID_SERVICE == 0)

// Device Information Service is mandatory
#if (BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)
#error BLE_SUPPORT_DEVICEINFORMATION_SERVICE shall be enabled when BleHumanInterfaceDevice DEVICE Role is enabled
#endif //(BLE_SUPPORT_HID_SERVICE == 0)

// Device Information Service's PNP ID characteristic is mandatory
#if (BLEDEVICEINFORMATION_SUPPORT_PNPID == 0)
#error BLEDEVICEINFORMATION_SUPPORT_PNPID shall be enabled in DEVICEINFORMATION SERVICE when BleHumanInterfaceDevice DEVICE Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_PNPID == 0)

/***************************************************************************\
 * OPTIONAL FEATURES FOR TIME DEVICE SERVICE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register the BLE HUMAN INTERFACE DEVICE (HID) Profile in DEVICE Role.
 *
 * BLEHID_DEVICE_Register()
 *	This function is used to register and initialise the BLE HUMAN INTERFACE 
 * DEVICE (HID) Profile in DEVICE role.
 *  It will add the supported HUMAN INTERFACE DEVICE (HID) Profile Services  
 * and characteristics into the Attribute database (HID Service, Device 
 * Information Service and Battery Service).
 *  This Interface shall be the entry point of a BLE HUMAN INTERFACE DEVICE
 * (HID) Profilein DEVICE Role.
 *  This function superseeds the services registration functions, just to be  
 * sure that all the mandatory SERVICEs are registered when register an HID
 * DEVICE.
 *
 * BLEHID_SUPPORT_DEVICE shall be enabled.
 *
 * @param reports: The DEVICE reports available for this HID device, it 
 *			includes the REPORT reports and the BOOT reports.
 *			For each report, the report type must be filled, and if the type
 *			is a REPORT one, (not a BOOT one) then the reportID is filled.
 *			The reports pointer must be persistent and valid during all the
 *			application lifetime.
 *
 * @param numberOfReports: The number of reports available in the reports
 *			memory block.
 *
 * @param hidServiceCallback : The BleHIDServiceCallBack callback 
 *          where the HID Service's events will be received, note that
 *          if several profiles implement the HID Service the callback
 *          shall be the same.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Alexandre GIMARD
 */

BleStatus BLEHID_DEVICE_Register( 
      BleHIDService					*hidDevices,
	  U8							numberOfHIDDevices,
	  BleHIDServiceCallBack			hidServiceCallback  );


/***************************************************************************\
 * OPTIONAL API functions definition BLE TIME PROFILE
\***************************************************************************/

#endif //(BLEHID_SUPPORT_DEVICE== 1)

#endif /*__BLEHID_DEVICE_H*/
