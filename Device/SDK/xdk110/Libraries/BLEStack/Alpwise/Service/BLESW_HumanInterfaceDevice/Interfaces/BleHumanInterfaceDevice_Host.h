#ifndef __BLEHUMANINTERFACEDEVICE_HOST_H
#define __BLEHUMANINTERFACEDEVICE_HOST_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleHumanInterfaceDevice_Host.h
 *
 * Description:   Contains interfaces definition for Human Interface Device 
 *					(HID) Profile when the local device operates in the HOST
 *					role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "BleEngine.h"

/**
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST
 * Defines if the BLE HUMAN INTERFACE DEVICE Profile implementation supports 
 * the HOST Role
 * If enabled ( set to 1 ) it enables all the following BLE Human Interface 
 * Device Profile HOST configurations and Application Interfaces.
 *
 * The default value for this option is disabled (0).
 */
#ifndef BLEHUMANINTERFACEDEVICE_SUPPORT_HOST
#define BLEHUMANINTERFACEDEVICE_SUPPORT_HOST							0
#endif //BLEHUMANINTERFACEDEVICE_SUPPORT_HOST

#if (BLEHUMANINTERFACEDEVICE_SUPPORT_HOST == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/**
 * BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT
 * Defines the maximum number of remote report that the local device can 
 * reference
 * During the LinkUp procedure the HID HOST is discovering every remote
 * reports and save the reference of these reports like the reportID, 
 * report Type (INPUT, OUTPUT, FEATURE) and the attribute handle.
 * Every Record reference adds 7 bytes in RAM memory.
 * The default value for this option is 6.
 */
#ifndef BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT
#define BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT			6
#endif //BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT

/***************************************************************************\
 * CONFIGURATION
\***************************************************************************/
#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)

#if (BLEGATT_SUPPORT_READ_LONG_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_READ_LONG_CHARACTERISTIC_VALUE shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_READ_LONG_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_RELATIONSHIP_DISCOVERY == 0)
#error BLEGATT_SUPPORT_RELATIONSHIP_DISCOVERY shall be enabled when when HUMAN INTERFACE DEVICE Profile HOST Role is enabled
#endif //(BLEGATT_SUPPORT_RELATIONSHIP_DISCOVERY == 0)
/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BleHIDHostEvent type
 * Defines the different kind of events that could be received by the 
 * BleHIDHostCallBack
 */
typedef	U8	BleHIDHostEvent;

/** BLEHIDHOST_EVENT_LINKED
 * Event received when the LinkUP is completed, 
 * The status field indicates the status of the operation.
 * The parms field indicates the link up information within a
 * BleHIDHostLinkUpInfo pointer.
 */
#define BLEHIDHOST_EVENT_LINKED									0x40

/** BLEHIDHOST_EVENT_UNLINKED
 * Event received when the UnLink is completed,  
 * The status field indicates the status of the operation.
 * The parms field indicates the link up information within a
 * BleHIDHostLinkUpInfo pointer.
 * The HID HOST (BleHIDHost) and HID Devices (BleHIDRemoteHIDDevice) 
 * memory blocks could be reused.
 */
#define BLEHIDHOST_EVENT_UNLINKED								0x41


/** BLEHIDHOST_EVENT_HIDDEVICEINFORMATION
 * Event received when the remote device information is received, 
 * This event follows a call of the BLEHID_HOST_GetHIDDeviceInformation()
 * API.
 * The status field indicates the status of the operation.
 * The parms field indicates the remote device information within a
 * BleHIDHostDeviceInfo pointer.
 */
#define BLEHIDHOST_EVENT_HIDDEVICEINFORMATION					0x42


/** BLEHIDHOST_EVENT_PROTOCOLMODEINFORMATION
 * Event received when the current remote device protocol mode is received, 
 * This event follows a call of the BLEHID_HOST_GetProtocol()
 * API.
 * The status field indicates the status of the operation.
 * The parms field indicates the remote device protocol mode information 
 * within a BleHIDHostProtocolInfo pointer.
 */
#define BLEHIDHOST_EVENT_PROTOCOLMODEINFORMATION				0x43

/** BLEHIDHOST_EVENT_REPORT_IND
 * Event received when a report has been received from the remote
 * HID device,  
 * Note that during a REPORT_IND the maximum report Size received is 
 * 20 bytes (ATT_MTU -3), if the application is interested to retrieve
 * more than 20 bytes, it must then call the BLEHID_HOST_GetReport() API
 * with a reportMemoryLen set to the number of bytes desired by the 
 * application.
 * This event is received only if the application has call the
 * BLEHID_HOST_AcceptReportDataInput API for this report identied by its
 * report ID
 * The status field is always BLESATUS_SUCCESS.
 * The parms field indicates the report information within a
 * BleHIDHostReportInfo pointer.
 */
#define BLEHIDHOST_EVENT_REPORT_IND								0x44

/** BLEHIDHOST_EVENT_REPORT_RSP
 * Event received when a report has been received from the remote
 * HID device,  
 * This event follows a call of the BLEHID_HOST_GetReport() API.
 * The status field indicate the status of the operation.
 * The parms field indicates the report information within a
 * BleHIDHostReportInfo pointer, the report value pointer
 * points to the reportMemory memory block passed during the call of the
 * BLEHID_HOST_GetReport() API.
 */
#define BLEHIDHOST_EVENT_REPORT_RSP								0x45

/** BLEHIDHOST_EVENT_REPORT_MAP_RSP
 * Event received when a descriptor has been received from the remote
 * HID device,  
 * This event follows a call of the BLEHID_HOST_GetDescriptor() API.
 * The status field indicate the status of the operation.
 * The parms field indicates the report map information within a
 * BleHIDHostReportMapInfo pointer, the report map value pointer
 * points to the descriptorMemory memory block passed during the call of 
 * the BLEHID_HOST_GetDescriptor() API.
 */
#define BLEHIDHOST_EVENT_REPORT_MAP_RSP							0x46

/** BLEHIDHOST_EVENT_ACCEPTREPORT_CNF
 * Event received when the remote HID device configuration is done,  
 * This event follows a call of the BLEHID_HOST_AcceptReportDataInput API.
 * The status field indicate the status of the operation.
 * The parms field indicates the HID Host device for which this event is
 * received (BleHidHost *blehidHost = (BleHidHost *) parms )
 */
#define BLEHIDHOST_EVENT_ACCEPTREPORT_CNF						0x47

/** BLEHIDHOST_EVENT_SETPROTOCOL_CNF
 * Event received when the Protocol Change request is complete.
 * This event follows a call of the BLEHID_HOST_SetProtocol API.
 * The status field indicate the status of the operation.
 * The parms field indicates the HID Host device for which this event is
 * received (BleHidHost *blehidHost = (BleHidHost *) parms )
 */
#define BLEHIDHOST_EVENT_SETPROTOCOL_CNF						0x48

/** BLEHIDHOST_EVENT_SETREPORT_CNF
 * Event received when the "set report" request is complete
 * This event follows a call of the BLEHID_HOST_SetReport API.
 * The status field indicate the status of the operation.
 * The parms field indicates the HID Host device for which this event is
 * received (BleHidHost *blehidHost = (BleHidHost *) parms )
 */
#define BLEHIDHOST_EVENT_SETREPORT_CNF							0x49

/** BLEHIDHOST_EVENT_COMMAND_CNF
 * Event received when the command has been sent to the remote HID device.
 * This event follows a call of the BLEHID_HOST_SendCommand API.
 * The status field indicate the status of the operation.
 * The parms field indicates the HID Host device for which this event is
 * received (BleHidHost *blehidHost = (BleHidHost *) parms )
 */
#define BLEHIDHOST_EVENT_COMMAND_CNF							0x4A

/** BLEHIDHOST_EVENT_BATTERYLEVEL
 * Event received when the battery level of the remote HID DEVICE is received
 * The status field indicate the status of the operation.
 * The parms field indicates the Battery Level information for which this
 * event is received (BleHidHostBatteryLevel *bleBatteryLevel= 
 * (BleHIDHostBatteryLevelInfo *) parms )
 */
#define BLEHIDHOST_EVENT_BATTERYLEVEL							0x4B

/**
 * BleHIDHostCallBack
 *	This callback receives the BLE HUMAN INTERFACE DEVICE HOST events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful not to do 
 *	heavy process in this function.
 */
typedef void (*BleHIDHostCallBack)(
	BleHIDHostEvent				event,
	BleStatus					status,
	U8						   *parms); 


/** 
 * BleHIDHostReportID type
 * Defines a report ID used when the device wants to get or set the
 * report value or when a report value is received in "data input" 
 * mode.
 */
typedef U8	BleHIDHostReportID;

/** 
 * BleHIDReportReference type
 * It defines a remote HID DEVICE report, it represents a memory block
 * used by the Profile to store the remote HID Device REPORTS information.
 *
 * During the linkUp procedure the HID Profile will try to discover the remote
 * HID DEVICE Services and then for each service the reports inside any 
 * services (BOOT and REPORT), it will strore any remote HID DEVICEs' Report 
 * information in this memory block in order the Profile to access them later.
 *
 * The maximum report reference saved by the profile for each remote service  
 * is BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT
 * The Profile API will then access to a remote HID DEVICEs' Report by the 
 * report type and eventually the report ID.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleHIDHostReportID					reportID;
	U8									type;
	AttHandle							attHandle;
	AttHandle							attConfigHandle;
	AttHandle							reportReferenceHandle;

} BleHIDReportReference;

/** 
 * BleHIDRemoteHIDDevice type
 * Define a remote HID DEVICE, it represents a memory block
 * that should be passed by the application during the 
 * BLEHID_HOST_LinkUpHIDProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEHID_HOST_LinkUpHIDProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * HID HOST receive the BLEHIDHOST_EVENT_UNLINKED event.
 *
 * During the linkUp procedure the HID Profile will try to discover the remote
 * HID DEVICE Services,it will strore any remote HID DEVICE Service information
 * in this memory block in order the Profile to access them later.
 * This BleHIDRemoteHIDDevice will be an handle of teh remote device to wich the 
 * API is destinated.
 * The remote HID DEVICE Service information is used internally by the profile   
 * and shall not be accessed by the application.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	U8								features;
	U8								internalValue[1];

	AttHandle						serviceStartingHandle;
	AttHandle						serviceEndingHandle;

	AttHandle						incServiceStartingHandle;
	AttHandle						incServiceEndingHandle;
	AttHandle						batteryLevelHandle;

	AttHandle						protocolModeHandle;
	AttHandle						informationHandle;
	AttHandle						controlPointHandle;
	AttHandle						reportMapHandle;
	AttHandle						reportMapDeclHandle;

	U8								numberOfReport;
	BleHIDReportReference			reportReference[
				BLEHUMANINTERFACEDEVICE_HOST_MAXIMUM_NUMBER_OF_REPORT];


} BleHIDRemoteHIDDevice;


/** 
 * BleHIDHostPnpId type
 * Structure passed in parameter in the BLEHID_HOST_GetDevicePnpId,
 * in order to read the pnp ID information.
 * It describes the remote HID PnpId Information value.

 * It indicates :
 * vendorIdSource : Identifies the source of the Vendor ID field.
 *						- 1 for Bluetooth SIG assigned Company Identifier 
 *							value from the Assigned Numbers document
 *						- 2 for USB Implementer�s Forum assigned Vendor ID 
 *							value
 * vId : Identifies the product vendor from the namespace in the Vendor ID Source.
 * pId : Manufacturer managed identifier for this product.
 * productVersion : Manufacturer managed version for this product
 */
typedef struct {
	U8						vendorIdSource;
	U16						vId;
	U16						pId;
	U16						productVersion;

} BleHIDHostPnpId;

/** 
 * BleHIDHost type
 * Define a local HID HOST, it represents a memory block
 * that should be passed by the application during the 
 * BLEHID_HOST_LinkUpHIDProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEHID_HOST_LinkUpHIDProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * HID HOST receive the BLEHIDHOST_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this HID HOST memory block while 
 * linked with remote HID DEVICE. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local HID HOST could be then passed as parameter to 
 * profile API in order to identify to which linked HID DEVICE the operation  
 * is destinated.
 *
 * A HID HOST is unique per remote HID DEVICE , it means 
 * that if this local device is connected to multiple remote HID DEVICE the 
 * application shall register multiple HID HOSTs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	

	U8						value[2];

	BleHIDRemoteHIDDevice	*remoteHIDDevices;
	U8						maxHIDDevices;
	U8						numHIDDevicesLinked;
	U8						numHIDDevicesFound;

	AttHandle				dISStartingHandle;
	AttHandle				dISEndingHandle;
	AttHandle				pnpIDHandle;
	BleHIDHostPnpId			pnpId;

	AttHandle				bSStartingHandle;
	AttHandle				bSEndingHandle;
	AttHandle				batteryLevelHandle;
	U8						numBatteryServices;

	U8						*memory;
	U16						memoryLen;
	U16						objectLen;

} BleHIDHost;

/** 
 * BleHIDHostLinkMode type
 * Type passed during the BLEHID_HOST_LinkUpHIDProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEHID_HOST_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEHID_HOST_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 * Note that if BLEHUMANINTERFACEDEVICE_SUPPORT_SAVINGINFORMATION is DISABLED, 
 * a linkUP with the BLEHID_HOST_LINKUP_RESTORE mode will always 
 * failed.
 */
typedef U8 BleHIDHostLinkMode;

#define BLEHID_HOST_LINKUP_COMPLETE							1
#define BLEHID_HOST_LINKUP_RESTORE							2

/** 
 * BleHIDHostReportType type
 * Enumerate the possible values for the report type when receiving or 
 * setting a report Value
 */
typedef U8	BleHIDHostReportType;
#define BLEHIDHOST_REPORTTYPE_NOTKNOWN							0x00
#define BLEHIDHOST_REPORTTYPE_INPUT								0x01
#define BLEHIDHOST_REPORTTYPE_OUTPUT							0x02
#define BLEHIDHOST_REPORTTYPE_FEATURE							0x03
// Not defined by the HID and BT specifications but used internally
#define BLEHIDHOST_REPORTTYPE_BOOTMOUSEINPUT					0x11
#define BLEHIDHOST_REPORTTYPE_BOOTKEYBOARDINPUT					0x12
#define BLEHIDHOST_REPORTTYPE_BOOTKEYBOARDOUTPUT				0x13
#define BLEHIDHOST_REPORTTYPE_BOOTMASK							0x10

/** 
 * BleHIDHostProtocolMode type
 * Enumerate the possible values for the PROTOCOL MODE value.
 */
typedef U8 BleHIDHostProtocolMode;
#define BLEHIDHOST_PROTOCOLMODE_BOOT							0x00
#define BLEHIDHOST_PROTOCOLMODE_REPORT							0x01


/** 
 * BleHIDHostCommandId type
 * Enumerate the possible values for the available commands.
 */
typedef U8 BleHIDHostCommandId;
/** BLEHIDHOST_COMMANDID_SUSPEND
 * Informs HID Device that HID Host is entering the Suspend State as 
 * defined in (�7.4.2, Bluetooth HID Profile Specification) 
 */
#define BLEHIDHOST_COMMANDID_SUSPEND							0x00

/** BLEHIDHOST_COMMANDID_EXITSUSPEND
 * Informs HID Device that HID Host is exiting the Suspend State as defined 
 * in (�7.4.2, Bluetooth HID Profile Specification ) 
 */
#define BLEHIDHOST_COMMANDID_EXITSUSPEND						0x01

/** 
 * BleHIDHostFlags type
 * It is a bitfield used to define the possible values for the 
 * flag field receive when reading the remote HID device information
 */
typedef U8 BleHIDHostFlags;

/** BLEHIDHOST_FLAG_HOSTWAKEUP
 * The remote HID DEVICE is not designed to be capable of providing 
 * wake-up signal to a HID HOST
 */
#define BLEHIDHOST_FLAG_HOSTWAKEUP								0x01

/** BLEHIDHOST_FLAG_CONNECTABLE
 * The remote HID DEVICE is normally connectable
 */
#define BLEHIDHOST_FLAG_CONNECTABLE								0x02


/** 
 * BleHIDHostRemoteSupportedFeatures type
 * It is a bitfield used to define the possible values for the 
 * supported features received when reading the remote HID DEVICE
 * supported features using the 
 * BLEHID_HOST_GetRemoteDeviceSupportedFeatures() API.
 */
typedef U8 BleHIDHostRemoteSupportedFeatures;
#define BLEHIDHOST_REMOTESUPPORT_HIDINFORMATION					0x01
#define BLEHIDHOST_REMOTESUPPORT_REPORTMAP						0x02
#define BLEHIDHOST_REMOTESUPPORT_HIDCONTROLPPOINT				0x04
#define BLEHIDHOST_REMOTESUPPORT_HIDREPORT						0x08
#define BLEHIDHOST_REMOTESUPPORT_BOOTMOUSEINPUTREPORT			0x10
#define BLEHIDHOST_REMOTESUPPORT_BOOTKEYBOARDINPUTREPORT		0x20
#define BLEHIDHOST_REMOTESUPPORT_BOOTKEYBOARDOUTPUTREPORT		0x40
#define BLEHIDHOST_REMOTESUPPORT_PROTOCOLMODE					0x80


/** 
 * BleHIDHostLinkUpInfo type
 * structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_LINKED event.
 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle for which the linkup has completed.
 * numHidDevicesFound : The number of HID Service found of the remote HID Host
 *      devices.
 * numHidDevicesLinked : The number of HID Service linked in the local HID 
 *      Host devices, it means the number of HID service for which the 
 *		HID HOST Profile has saved the needed information localy.
 *
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	U8										numHidDevicesLinked;
	U8										numHidDevicesFound;

} BleHIDHostLinkUpInfo;

/** 
 * BleHIDHostReportMapInfo type
 * structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_REPORT_MAP_RSP event.
 * It describes the Report Map ( HID Descriptor) value.
 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle of the remote device.
 * hIDDevice : The HID devic handle for which the report Map information is
 *		received.
 * value : The value of the report Map (HID descriptor), this pointer is
 *		pointing to the descriptorMemory memory block passed during the call 
 *		of the BLEHID_HOST_GetDescriptor() API.
 * valueLen : The length of the report Map (HID descriptor). The Length is up
 *      to descriptorMemoryLen passed during the call of the 
 *		BLEHID_HOST_GetDescriptor() API, some data may be missing if the 
 *		descriptorMemoryLen is reached.
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	BleHIDRemoteHIDDevice					*hIDDevice;

	U8										*value;
	U16										valueLen;
	
}BleHIDHostReportMapInfo;


/** 
 * BleHIDHostReportInfo type
 * Structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_REPORT_RSP event or 
 * BLEHIDHOST_EVENT_REPORT_IND event.
 * It describes the Report value.
 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle of the remote device.
 * hIDDevice : The HID devic handle for which the report value information is
 *		received.
 * reportType : The Report Type for which the report value information is
 *		received.
 * reportID : In case of the report type is a REPORT mode one (not a BOOT 
 *		one), the report ID of the report for which the report value 
 *		information is received. Not applicable in case of a BOOT report.
 * value : The value of the report:
 *		- during a BLEHIDHOST_EVENT_REPORT_RSP this pointer is
 *			pointing to reportMemory memory block passed during the call of the
 *			BLEHID_HOST_GetReport() API.
 *		- during a BLEHIDHOST_EVENT_REPORT_IND this pointer is
 *			pointing to stack internal memory
 * valueLen : The length of the report value:
 *		- during a BLEHIDHOST_EVENT_REPORT_RSP, the Length is up to 
 *			reportMemoryLen passed during the call of the
 *			BLEHID_HOST_GetReport() API, some data may be missing if the 
 *			reportMemoryLen is reached.
 *		- during a BLEHIDHOST_EVENT_REPORT_IND, the Length is up to 20 bytes 
 *			(ATT_MTU -3), if the application is interested to retrieve
 *			more than 20 bytes, it must then call the BLEHID_HOST_GetReport() 
 *			API with a reportMemoryLen set to the number of bytes desired by 
 *			the application.
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	BleHIDRemoteHIDDevice					*hIDDevice;

	BleHIDHostReportType					reportType;
	BleHIDHostReportID						reportID;
	U8										*value;
	U16										valueLen;
	
}BleHIDHostReportInfo;


/** 
 * BleHIDHostProtocolInfo type
 * Structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_PROTOCOLMODEINFORMATION event.
 * It describes the remote protocol mode value.

 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle of the remote device.
 * hIDDevice : The HID device handle for which the protocol mode value 
 *		information is received.
 * protocolMode : The current protocol mode of the remote HID DEVICE
 *
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	BleHIDRemoteHIDDevice					*hIDDevice;
	BleHIDHostProtocolMode					protocolMode;
	
} BleHIDHostProtocolInfo;


/** 
 * BleHIDHostProtocolInfo type
 * Structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_PROTOCOLMODEINFORMATION event.
 * It describes the remote protocol mode value.

 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle of the remote device.
 * hIDDevice : The HID device handle for which the battery level value 
 *		information is received.
 * level : The battery level of th remote HID device.
 *
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	BleHIDRemoteHIDDevice					*hIDDevice;
	U8										level;
	
} BleHIDHostBatteryLevelInfo;

/** 
 * BleHIDHostDeviceInfo type
 * Structure passed in parameter in the BleHIDHostCallBack
 * parms fields during the BLEHIDHOST_EVENT_HIDDEVICEINFORMATION event.
 * It describes the remote HID Device Information value.

 * It indicates :
 * hIDHost : The local HID Host for which the event is
 *		destinated.
 * connHandle : The connection handle of the remote device.
 * hIDDevice : The HID devic handle for which the protocol mode value 
 *		information is received.
 * hidSpecification : A 16-bit unsigned integer representing version number 
 *		of base USB HID Specification implemented by HID Device, see USB HID
 *		specifictaion for possible values.
 * countryCode : Identifies which country the hardware is localized for. 
 *		Most hardware is not localized and thus this value would be zero (0).
 *		see USB HID	specifictaion for possible values.
 * flags : Flags representing the Remote device capabilities like Host wakeUp 
 *		or connectability.
 */
typedef struct {

	BleHIDHost								*hIDHost;
	U16										connHandle;
	BleHIDRemoteHIDDevice					*hIDDevice;

	U16										hidSpecification;
	U8										countryCode;
	BleHIDHostFlags							flags;

} BleHIDHostDeviceInfo;



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Init the HID HOST profile
 *
 * BLEHID_HOST_Init()
 *	This function is the entry point of the HID HOST profile, it 
 *	inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param callback: The BleHIDHostCallBack in which the HID HOSTs 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (HID HOST Profile is already 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */ 
BleStatus BLEHID_HOST_Init(
	BleHIDHostCallBack callback);

/** De-Init the HID HOST Profile
 *
 * BLEHID_HOST_Deinit
 *	This function is used to De-Init the HID HOST Profile.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_Deinit( void );


/** Link Up the Human Interface Profile(HID) with the specified remote Device
 *
 * BLEHID_HOST_LinkUpHIDProfile()
 *	This function is used to link-Up the HID profile with the 
 *  specified remote device.
 *  It will try to discover the HID profile and the supported HID devices
 * in the remote device using the given HID Host.
 *	As a single remote device may Host several different HID DEVICE, the 
 * application must also provide memory for the number of HID DEVICE it is 
 * interested to.
 * As well, if an application just want to know the number of HID DEVICEs 
 *  supported in the remote device, the maxHIDDevices must be set to 0.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A valid BleHIDHost HID HOST to link-up
 *		with a remote HID DEVICEs.
 *      This memory block is allocated by the application prior to call this
 *      API and shall stay valid while the devices are linked together. 
 *      It is up to profile internal mechanism to manage this memory block.
 *		Once linked, this HID HOST could be then passed as parameter to 
 *      profile API in order to identify to which linked HID HOST the 
 *      operation is destinated.   
 * @param connHandle: The connection Handle on which the link up between this
 *		 BleHIDHost and the remote HID DEVICE profile
 *		 shall be done.
 *
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 *
 * @param hidDevices:	A valid pointer to an array of BleHIDRemoteHIDDevice
 *		used by the profile to store the remote HID devices� Services 
 *		information.
 *		Depending of the numHidDevicesLinked reported in the 
 *		BleHIDHostLinkUpInfo parameter during BLEHIDHOST_EVENT_LINKED event,
 *		the pointer to the linked BleHIDRemoteHIDDevice can then be passed to 
 *		interact with this specific remote device.
 *		As exemple, if the array contains three BleHIDRemoteHIDDevice and the 
 *		linkUp operation mange to discover two remote HID DEVICE SERVICE in the 
 *		remote device, then hidDevices[0] and hidDevices[1] can be used to 
 *		interact with these two services.
 *
 * @param maxHidDevices:	The number of BleHIDRemoteHIDDevice in the HIDDevice
 *		Array, the profile will link at maximum this number of HID DEVICE.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode  
 *		is BLEHID_HOST_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEHID_HOST_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of 
 *		mode is BLEHID_HOST_LINKUP_RESTORE, it means that 
 *		BLEHIDHOST_SUPPORT_SAVINGINFORMATION is not enabled or 
 *		if enabled that the remote device is not bonded or the persistent  
 *		memory does not contains enough information about the remote HID 
 *		Device.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_LinkUpHIDProfile(
						BleHIDHost *hidHost,
						U16 connHandle,
						BleHIDHostLinkMode mode,
						BleHIDRemoteHIDDevice *hidDevices,
						U8 maxHidDevices
						);

/** Un Link the Human Interface Profile(HID) with the specified linked HID HOST
 *
 * BLEHID_HOST_UnLinkHIDProfile()
 *	This function is used to UnLink the HID Profile with the 
 *  specified previously linked HID HOST.
 *  This HID HOST will not receive any more information from the linked 
 *	HID Devices. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_HIDHOST_* from persistent memory.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *      The HID HOST (BleHIDHost) and HID Devices (BleHIDRemoteHIDDevice)
 *		memory blocks could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_UnLinkHIDProfile(	BleHIDHost *hidHost );

/** Get the given HID remote DEVICE supported feature
 *
 * BLEHID_HOST_GetRemoteDeviceSupportedFeatures()
 *	This function is used to Get the supported features from a remote HID device
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param remoteFeatures: A pointer to an BleHIDHostRemoteSupportedFeatures
 *			to where the supported features accessed if the function succeeds.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded, the 
 *		remoteFeatures pointer points to the remote hidDevice 
 *		supported features.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetRemoteDeviceSupportedFeatures(
						BleHIDHost							*hidHost,
						BleHIDRemoteHIDDevice				*hidDevice,
						BleHIDHostRemoteSupportedFeatures	*remoteFeatures);

/** Get the given HID remote device information
 *
 * BLEHID_HOST_GetHIDDeviceInformation()
 *	This function is used to Get the information from a remote HID device.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_HIDDEVICEINFORMATION event will be received in the 
 *		callback with the status of the operation and the device
 *		information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetHIDDeviceInformation( 
								BleHIDHost				*hidHost,
								BleHIDRemoteHIDDevice	*hidDevice);


/** Get the current Protocol mode from a linked HID HOST and the given
 *	HID remote device.
 *
 * BLEHID_HOST_GetProtocol()
 *	This function is used to Get the current Protocol mode (BOOT-REPORT)
 *	 from a linked HID HOST and the given HID remote device.
 *  It is is analogous to sending the "Get Protocol" request defined in the
 *   USB HID Specification.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_PROTOCOLMODEINFORMATION event will be received in  
 *		the callback with the status of the operation and the current protocol
 *		information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetProtocol( BleHIDHost			*hidHost,
								  BleHIDRemoteHIDDevice *hidDevice);

/** Set the current Protocol mode from a linked HID HOST and the given
 *	HID remote device.
 *
 * BLEHID_HOST_SetProtocol()
 *	This function is used to Set the current Protocol mode (BOOT-REPORT)
 *	 from a linked HID HOST and the given HID remote device.
 *  It is is analogous to sending the "Set Protocol" request defined in the
 *   USB HID Specification.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param protocolMode : The protocol mode to activate in the remote device.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_SETPROTOCOL_CNF event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_SetProtocol( 
					BleHIDHost					*hidHost,
					BleHIDRemoteHIDDevice		*hidDevice,
					BleHIDHostProtocolMode		protocolMode);

/** Get the current Descriptor (Report Map) from a linked HID HOST and the given
 *	HID remote device.
 *
 * BLEHID_HOST_GetDescriptor()
 *	This function is used to Get the current descriptor (Report map) value
 *	 from a linked HID HOST and the given HID remote device operating in REPORT
 *	 mode.
 *  It is is analogous to sending the "Get Descriptor" request defined in the
 *   USB HID Specification.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param descriptorMemory:	A valid pointer to a buffer that will be used to
 *			receive the descriptor (report map) value.
 * @param descriptorMemoryLen: The number of available bytes in the descriptor
 *			memory buffer.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_REPORT_MAP_RSP event will be received in the 
 *		callback with the status of the operation and the current report Map
 *		value.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because:
 *		 - the device is not linked. 
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetDescriptor( BleHIDHost			*hidHost,
								 BleHIDRemoteHIDDevice	*hidDevice,
								 U8						*descriptorMemory,
								 U16					descriptorMemoryLen);

/** Get the current Report from a linked HID HOST and the given
 *	HID remote device.
 *
 * BLEHID_HOST_GetReport()
 *	This function is used to Get the current Report value
 *	 from a linked HID HOST and the given HID remote device operating in REPORT
 *	 mode.
 *  It is is analogous to sending the "Get Report" request defined in the
 *   USB HID Specification.
 *  The Report Mode or the report Type may not allowing to read the report 
 *   value, in this case the function will failed directly. 
 *  The report Type supported by a device is returned by the 
 *   BLEHID_HOST_GetRemoteDeviceSupportedFeatures() API.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param reportType : The report type used to identifying the remote report 
 *			to read.
 * @param reportID: The reportID used to identifying the the REPORT type
 *			report to read, ignored in case of BOOT report Type.
 * @param reportMemory:	A valid pointer to a buffer that will be used to
 *			receive the report value.
 * @param reportMemoryLen: The number of available bytes in the report
 *			memory buffer.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_REPORT_RSP event will be received in the 
 *		callback with the status of the operation and the current report
 *		value.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because:
 *		 - The device is not linked. 
 *		 - The remote HID DEVICE does not support the REPORT mode
 *		 - The Remote HID DEVICE does not support INPUT or FEATURE report type.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetReport( BleHIDHost				*hidHost,
								 BleHIDRemoteHIDDevice	*hidDevice,
								 BleHIDHostReportType	reportType,
								 BleHIDHostReportID		reportID,
								 U8						*reportMemory,
								 U16					reportMemoryLen);



/** Accept the current Report Input from a HID remote device.
 *
 * BLEHID_HOST_AcceptReportDataInput()
 *	This function is used to configure the HOST and the remote device
 *   to accept the transfert of Report value from HID DEVICE to HID HOST
 *
 *  It is is analogous to the "Data Input" defined in the
 *   USB HID Specification.
 *  The Report Mode or the report Type may not allowing to accept report data
 *   input value, in this case the function will failed directly. 
 *  The report Type supported by a device is returned by the 
 *   BLEHID_HOST_GetRemoteDeviceSupportedFeatures() API.
 * Note that with the "Data Input" mode the maximum byte received during the
 * BLEHIDHOST_EVENT_ACCEPTREPORT_CNF is 20 bytes, if teh application need to
 * retrieve more than 20 bytes, it should use the BLEHID_HOST_GetReport() API.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param reportType : The report type used to identifying the remote report 
 *			to read.
 * @param reportID: The reportID used to identifying the the REPORT type
 *			report to read, ignored in case of BOOT report Type.
 * @param isInputAccepted : if 1 configure the remote device to provide the 
 *              local device INPUT reports
 *                          if 0 configure the remote device to not provide the 
 *              local device INPUT reports
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_ACCEPTREPORT_CNF event will be received in the 
 *		callback with the status of the operation, then the 
 *		BLEHIDHOST_EVENT_REPORT_IND event will be received in the 
 *		callback with the report value upon report data reception from the remote 
 *		device.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because:
 *		 - The device is not linked. 
 *		 - The remote HID DEVICE does not support the REPORT mode
 *		 - The Remote HID DEVICE does not support INPUT report type.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_AcceptReportDataInput( BleHIDHost	*hidHost,
											BleHIDRemoteHIDDevice	*hidDevice,
											BleHIDHostReportType	reportType,
											BleHIDHostReportID		reportID,
                                            U8                      isInputAccepted);

/** Set the current output Report value from a HID remote device.
 *
 * BLEHID_HOST_SetReportValue()
 *	This function is used by the HOST to set a remote HID Device's output 
 * Report value.
 *
 *  It is is analogous to the "Set Report" request defined in the
 *   USB HID Specification.
 *  The Report Mode or the report Type may not allowing to set report 
 *   output value, in this case the function will failed directly. 
 *  The report Type supported by a device is returned by the 
 *   BLEHID_HOST_GetRemoteDeviceSupportedFeatures() API.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param reportType : The report type used to identifying the remote report 
 *			to write.
 * @param reportID: The reportID used to identifying the the REPORT type
 *			report to write, ignored in case of BOOT report Type.
 * @param value: A valid pointer to the value to set into the report value,
 *			this pointer must be valid and must contains the report value 
 *			still the BLEHIDHOST_EVENT_SETREPORT_CNF have been received.
 * @param valueLen: A len of the report value, it also correspond to the 
 *			number of valid bytes into the value buffer.
 * @param isReliable: Define the kind of reliabilty choose to send the report,
 *          if zero ( 0 ) then the HID profile will use a non acknoledged 
 *          request to send the data, it is faster but not reliable, it may 
 *          be recommended in case of high amount of data to be transfered.
 *          if not zero, the HID profile will use an acknoledged request.
 *          (note that in case of long value > 20, this parameter is ignored
 *          and reliable mode is always choosen)
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_SETREPORT_CNF event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because:
 *		 - The device is not linked. 
 *		 - The remote HID DEVICE does not support the REPORT mode
 *		 - The Remote HID DEVICE does not support OUTPUT report type.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_SetReportValue( BleHIDHost		*hidHost,
								 BleHIDRemoteHIDDevice	*hidDevice,
								 BleHIDHostReportType	reportType,
								 BleHIDHostReportID		reportID,
								 U8						*value,
								 U16					valueLen,
                                 U8                     isReliable);

/** Get the current PnpId information from a HID remote device.
 *
 * BLEHID_HOST_GetDevicePnpId()
 *	This function is used to get the current PnpId information (VID & PID)
 * from a HID remote device.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param pnpIdInformation: A valid pointer to a structure that will point
 *			to a valid pnpId information.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded, 
 *		the pnpIdInformation event is filled with pnpId information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because:
 *		 - The device is not linked. 
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetDevicePnpId( BleHIDHost		*hidHost,
									  BleHIDHostPnpId	**pnpIdInformation);


/** Send the given command to the given linked remote HID Device.
 *
 * BLEHID_HOST_SendCommand()
 *	This function is used by a linked HID HOST to send a command to a remote
 * HID device.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 * @param commandId : The command to send to the remote device.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_COMMAND_CNF event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_SendCommand( 
					BleHIDHost					*hidHost,
					BleHIDRemoteHIDDevice		*hidDevice,
					BleHIDHostCommandId			commandId);


/** Get the remote device Battery Level.
 *
 * BLEHID_HOST_GetDeviceBatteryLevel()
 *	This function is used by a linked HID HOST to get the battery level of 
 * a remote HID device.
 *
 * BLEHUMANINTERFACEDEVICE_SUPPORT_HOST shall be enabled
 *
 * @param hidHost: A linked HID HOST.
 * @param hidDevice: A linked HID DEVICE reported during the linkUp operation.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEHIDHOST_EVENT_BATTERYLEVEL event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other HID HOST Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HID HOST Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHID_HOST_GetDeviceBatteryLevel( 
					BleHIDHost					*hidHost,
					BleHIDRemoteHIDDevice		*hidDevice);

#endif //(BLEHUMANINTERFACEDEVICE_SUPPORT_HOST== 1)

#endif /*__BLEHUMANINTERFACEDEVICE_HOST_H*/
