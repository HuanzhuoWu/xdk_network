#ifndef __BLEFINDME_TARGET_H
#define __BLEFINDME_TARGET_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleFindMe_Target.h
 *
 * Description:   Contains interfaces routines for FindMe Profile
 *					when the local device operates in the TARGET role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"

/***************************************************************************\
 *	Macro and constant definition
\***************************************************************************/

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEFINDME_SUPPORT_TARGET
 * Defines if the BLE FINDME Profile implementation supports the 
 * TARGET Role
 * If enabled ( set to 1 ) it enables all the following BleFindMe
 * TARGET configuration and Application Interface
 * The default value for this option is disabled (0).
 */
#ifndef BLEFINDME_SUPPORT_TARGET
#define BLEFINDME_SUPPORT_TARGET								0
#endif //BLEFINDME_SUPPORT_TARGET

#if (BLEFINDME_SUPPORT_TARGET == 1)

// Then check dependencies
#include "Services/BleImmediateAlert_Service.h"

// Immediate Alert Service is mandatory
#if (BLE_SUPPORT_IMMEDIATEALERT_SERVICE == 0)
#error BLE_SUPPORT_IMMEDIATEALERT_SERVICE shall be enabled when BleFindMe TARGET Role is enabled
#endif //(BLE_SUPPORT_IMMEDIATEALERT_SERVICE == 0)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/***************************************************************************\
 * CONSTANTS
\***************************************************************************/

/***************************************************************************\
 *	Type definition
\***************************************************************************/



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/* Register a BLE FINDME Target.
 *
 * BLEFINDME_TARGET_Register()
 *	This function is used to register and initialise a BLE FINDME 
 * TARGET role.
 * It will add the supported Find Me characteristics into the Attribute
 * database.
 *
 * BLEFINDME_SUPPORT_TARGET shall be enabled.
 *
 * immediateAlertCallback : The callback that will receive the immediate
 *		alert events.
 *
 * return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 */
BleStatus BLEFINDME_TARGET_Register( 
			BleImmediateAlertCallBack	immediateAlertCallback );

#endif //(BLEFINDME_SUPPORT_TARGET== 1)

#endif //__BLEFINDME_TARGET_H
