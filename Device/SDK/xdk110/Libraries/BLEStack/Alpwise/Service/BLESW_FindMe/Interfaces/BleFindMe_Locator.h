#ifndef __BLEFINDME_LOCATOR_H
#define __BLEFINDME_LOCATOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleFindMe_Locator.h
 *
 * Description:   Contains interfaces definition for FindMe Profile
 *					when the local device operates in the LOCATOR role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "Services/BleImmediateAlert_Service.h"

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEFINDME_SUPPORT_LOCATOR
 * Defines if the BLE FINDME Profile implementation supports the 
 * LOCATOR Role.
 * If enabled ( set to 1 ) it enables all the following BleFindMe LOCATOR
 * configuration and Application Interface.
 * The default value for this option is disabled (0). 
 */
#ifndef BLEFINDME_SUPPORT_LOCATOR
#define BLEFINDME_SUPPORT_LOCATOR								0
#endif //BLEFINDME_SUPPORT_LOCATOR

#if (BLEFINDME_SUPPORT_LOCATOR == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when FindMe LOCATOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when FindMe LOCATOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when FindMe LOCATOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when FindMe LOCATOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

// GATT Write Without Response is mandatory
#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be enabled when FINDME LOCATOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)

#if (BLE_SECURITY == 1 )
/**
 * BLEFINDME_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLEFINDME Profile LOCATOR supports 
 * saving the TARGET information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the LOCATOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the TARGET information.
 * Upon reconnection with the TARGET, as these information are already known
 * then the linkUp process can be simplified.
 * There is 6 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEFINDME_SUPPORT_SAVINGINFORMATION	
#define BLEFINDME_SUPPORT_SAVINGINFORMATION					0
#endif //BLEFINDME_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_FINDME_IASERVICEHANDLES					0x65	//4 bytes
#define BLEINFOTYPE_FINDME_IAALERTHANDLE					0x66	//2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported and 
// BLEFINDME_SUPPORT_BATTERYLEVELSTATEALERT is not supported force 
// the BLEFINDME_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEFINDME_SUPPORT_SAVINGINFORMATION
#define BLEFINDME_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 

/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BleFindMeEvent type
 * Define the different kind of events that could be received by the 
 * BleFindMeCallBack
 */
typedef	U8	BleFindMeLocatorEvent;

/** BLEFINDMELOCATOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleFindMeLocatorLinkUpInfo pointer.
 */
#define BLEFINDMELOCATOR_EVENT_LINKED								0x90

/** BLEFINDMELOCATOR_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleFindMeLocatorLinkUpInfo pointer.
 */
#define BLEFINDMELOCATOR_EVENT_UNLINKED								0x91

/**
 * BleFindMeCallBack
 *	This callback receives the BLE FINDME LOCATOR profile events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleFindMeLocatorCallBack)(BleFindMeLocatorEvent event,
										 BleStatus status,
										 void* parms); 

/** 
 * BleFindMeLocator type
 * Define a local LOCATOR, it represent a memory block
 * that should be passed by the application during the 
 * BLEFINDME_LOCATOR_LinkUpFindMeProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEFINDME_LOCATOR_LinkUpFindMeProfile() API ,and shall 
 * be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this LOCATOR 
 * receive the BLEFINDMELOCATOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this LOCATOR memory block while linked
 * with a remote TARGET. It is used internally by the profile and shall 
 * not be accessed by the application.
 * A pointer to this local LOCATOR could be then passed as parameter to 
 * profile API in order to identify to which linked TARGET the operation  
 * is destinated.
 *
 * A LOCATOR is unique per remote FindMe TARGET, it means that if 
 * this local device is connected to multiple remote TARGET the application
 * shall register multiple LOCATORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	U8						targetFeatures;

	AttHandle				iaServiceStartingHandle;
	AttHandle				iaServiceEndingHandle;

	AttHandle				iaLevelHandle;
	U8						iaValue[1];
} BleFindMeLocator;


/** 
 * BleFindMeLocatorLinkUpInfo type
 * Structure passed in parameter in the BleFindMeLocatorCallBack
 * parms fields during the BLEFINDMELOCATOR_EVENT_LINKED and
 * BLEFINDMELOCATOR_EVENT_UNLINKED events.
 * It indicates :
 * BleFindMeLocator : The locator from which the event is comming
 * connHandle : The connection handle from which the Link Up information
 *	is coming.
 */
typedef struct {

	BleFindMeLocator						*bleFindMeLocator;
	U16										connHandle;

} BleFindMeLocatorLinkUpInfo;

/** 
 * BleFindMeLocatorLinkMode type
 * Type passed during the BLEFINDME_LOCATOR_LinkUpFindMeProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEFINDME_LOCATOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEFINDME_LOCATOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 * Note that if BLEFINDME_SUPPORT_SAVINGINFORMATION is DISABLED, a linkUP 
 * with the BLEFINDME_LOCATOR _LINKUP_RESTORE mode will always failed.
 */
typedef U8 BleFindMeLocatorLinkMode;

#define BLEFINDME_LOCATOR_LINKUP_COMPLETE				1
#define BLEFINDME_LOCATOR_LINKUP_RESTORE				2

/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Init the FindMe profile
 *
 * BLEFINDME_LOCATOR_Init()
 *	This function is the entry point of the FindMe profile,it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEFINDME_SUPPORT_LOCATOR shall be enabled
 *
 * @param callback: The BleFindMeCallBack in which the locator events
 *				 will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEFINDME_LOCATOR_Init(BleFindMeLocatorCallBack callback);

/** De-Init the Find Me LOCATOR Profile
 *
 * BLEFINDME_LOCATOR_Deinit
 *	This function is used to De-Init the Find Me LOCATOR Profile.
 *
 * BLEFINDME_SUPPORT_LOCATOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEFINDME_LOCATOR_Deinit( void );


/** Link Up the FindMe Profile with the specified remote Device
 *
 * BLEFINDME_LOCATOR_LinkUpFindMeProfile()
 *	This function is used to link-Up the FindMe profile with the 
 *  specified remote device.
 *  It will try to discover the FindMe Profile in the remote device 
 *  using the given LOCATOR.
 *
 * BLEFINDME_SUPPORT_LOCATOR shall be enabled
 *
 * @param bleFindMeLocator: A valid BleFindMeLocator LOCATOR to link-up
 *					with a remote FindMe TARGET.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this LOCATOR could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  TARGET the operation is destinated.
 * @param connHandle: The connection Handle on which the link up between this
 *		 bleFindMeLocator and the remote FindMe TARGET profile
 *		 shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEFINDME_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode is 
 *		BLEFINDME_LOCATOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEFINDME_LOCATOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEFINDME_LOCATOR_LINKUP_RESTORE, it means that 
 *		BLEFINDME_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote FindMe TARGET.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other FindMe Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEFINDME_LOCATOR_LinkUpFindMeProfile(
								BleFindMeLocator *bleFindMeLocator,
								U16 connHandle,
								BleFindMeLocatorLinkMode mode);

/** Un Link the FindMe Profile with the specified linked LOCATOR
 *
 * BLEFINDME_LOCATOR_UnLinkFindMeProfile()
 *	This function is used to UnLink the FindMe Profile with the 
 *  specified previously linked LOCATOR.
 *  This LOCATOR will not receive any more information from the linked 
 *	TARGET. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_FINDME_* from persistent memory
 *
 * BLEFINDME_SUPPORT_LOCATOR shall be enabled
 *
 * @param bleFindMeLocator: A linked FindMe LOCATOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the locator memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEFINDME_LOCATOR_UnLinkFindMeProfile(
		BleFindMeLocator *bleFindMeLocator);

/** Set the remote Immediate Alert level for link Loss Alert
 *
 * BLEFINDME_LOCATOR_SetImmediateAlertLevel()
 *	This function set the immediate alert level on the linked
 *	FindMe TARGET.
 *
 * BLEFINDME_SUPPORT_LOCATOR shall be enabled
 *
 * @param bleFindMeLocator: A linked FindMe LOCATOR
 * @param immediateAlertlevel : The desired immediate Alert level
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other FindMe Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (bleFindMeLocator is not linked).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEFINDME_LOCATOR_SetImmediateAlertLevel(
	BleFindMeLocator			*bleFindMeLocator,
	BleImmediateAlertAlertLevel	immediateAlertlevel);

#endif //(BLEFINDME_SUPPORT_LOCATOR== 1)

#endif //__BLEFINDME_LOCATOR_H
