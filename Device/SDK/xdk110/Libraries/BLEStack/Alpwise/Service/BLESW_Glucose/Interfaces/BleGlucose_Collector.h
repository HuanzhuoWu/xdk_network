#ifndef __BLEGLUCOSE_COLLECTOR_H
#define __BLEGLUCOSE_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************\
 *
 * File:          BleGlucose_Collector.h
 *
 * Description:   Contains interfaces definition for Glucose 
 *				Profile when the local device operates in the COLLECTOR
 *				role.
 * 
 * Created:       April, 2013
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleTimeTypes.h"
#include "BleGatt.h"

/****************************************************************************\
 *	CONFIGURATION
\****************************************************************************/

/**
 * BLEGLUCOSE_SUPPORT_COLLECTOR
 * Defines if the BLE GLUCOSE Profile implementation supports the 
 * COLLECTOR Role.
 * If enabled ( set to 1 ) it enables all the following BLE GLUCOSE
 * Profile COLLECTOR configurations and Application Interfaces.
 * The default config for this option is disabled (0).
 */
#ifndef BLEGLUCOSE_SUPPORT_COLLECTOR
#define BLEGLUCOSE_SUPPORT_COLLECTOR						                                0
#endif //BLEGLUCOSE_SUPPORT_COLLECTOR

#if (BLEGLUCOSE_SUPPORT_COLLECTOR == 1)

/****************************************************************************\
 * OPTIONAL FEATURES
\****************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when GLUCOSE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE GLUCOSE Profile implementation in 
 * COLLECTOR role supports saving the GLUCOSE SENSOR information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the GLUCOSE SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and Glucose 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * It saves 8 bytes (or 10 bytes if Glucose Measurement Context is supported)
 * into persistent memory.
 *
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default config for this option is disabled (0). 
 */
#ifndef BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION		                                0
#endif //BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_GLUCOSE_REMOTEFEATURES					                                0x5C	//2 bytes
#define BLEINFOTYPE_GLUCOSE_GLUCOSEFEATURES                                                 0x5D    //2 bytes
#define BLEINFOTYPE_GLUCOSE_GLUCOSEMEASUREMENTHANDLE	                                    0x5E	//2 bytes
#define BLEINFOTYPE_GLUCOSE_GLUCOSEMEASUREMENTCONTEXTHANDLE	                                0x5F	//2 bytes
#define BLEINFOTYPE_GLUCOSE_RECORDACCESSCONTROLPOINTHANDLE	                                0x60	//2 bytes


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION is not defined force
// the BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION                                       0
#endif //( BLE_SECURITY == 0) 


/****************************************************************************\
 * CONSTANTS Definition
\****************************************************************************/

/** 
 * Constant for sensor status annunciation bitfield 
 */
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_LOWBATTERYATMEASUREMENT               (0x0001u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_MALFUNCTIONATMEASUREMENT       		(0x0002u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_SAMPLESIZEINSUFFICIENTATMEASUREMENT 	(0x0004u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_STRIPINSERTIONERROR		            (0x0008u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_STRIPTYPEINCORRECT				    (0x0010u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_RESULTHIGHERTHANTHEDEVICECANPROCESS	(0x0020u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_RESULTLOWERTHANTHEDEVICECANPROCESS	(0x0040u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_TEMPERATURETOOHIGHATMEASUREMENT		(0x0080u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_TEMPERATURETOOLOWATMEASUREMENT		(0x0100u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_STRIPPULLEDTOOSOON					(0x0200u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_GENERALDEVICEFAULT					(0x0400u)
#define BLE_GLUCOSECOLLECTOR_SENSORSTATUSANNUNCIATION_TIMEFAULT								(0x0800u)

/****************************************************************************\
 *	TYPES definition
\****************************************************************************/
//Define in BleGlucose_Collector.h because app needs
/** 
 * BleGlucoseCollectorControlPointOperation type
 * Define the different Record Access Control Point operation and response.
 */
typedef U8  BleGlucoseCollectorControlPointOperation;
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_REPORTSTOREDRECORDS				                (0x01u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_DELETESTOREDRECORDS	                            (0x02u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_ABORTOPERATION					                    (0x03u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_REPORTNUMBEROFSTOREDRECORDS		                (0x04u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_NUMBEROFSTOREDRECORDSRESPONSE	                    (0x05u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_RESPONSECODE						                (0x06u)

/** 
 * BleGlucoseCollectorControlPointOperator type
 * Define the different Record Access Control Point operator.
 * One operand is needed if operator is Less Than Or Equal To
 * or Greater Than Or Equal To.
 * Two operands are needed if operator is Within Range Of
 */
typedef U8  BleGlucoseCollectorControlPointOperator;
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_NULL     						                    (0x00u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_ALLRECORDS						                    (0x01u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_LESSTHANOREQUALTO	                                (0x02u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_GREATERTHANOREQUALTO			                    (0x03u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_WITHINRANGEOF					                    (0x04u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_FIRSTRECORD				                        (0x05u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_LASTRECORD						                    (0x06u)

/** 
 * BleGlucoseCollectorControlPointStatus type
 * Define the different Record Access Control Point status.
 */
typedef U8  BleGlucoseCollectorControlPointStatus;
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_SUCCESS                 	                (0x01u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_OPCODENOTSUPPORTED                          (0x02u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_INVALIDOPERATOR          	                (0x03u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_OPERATORNOTSUPPORTED                        (0x04u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_INVALIDOPERAND                              (0x05u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_NORECORDSFOUND                              (0x06u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_ABORTUNSUCCESSFUL                           (0x07u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_PROCEDURENOTCOMPLETED    	                (0x08u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_STATUS_OPERANDNOTSUPPORTED                         (0x09u)

/** 
 * BleGlucoseCollectorControlPointFilterType type
 * Define the different Record Access Control Point operand filter type.
 */
typedef U8  BleGlucoseCollectorControlPointFilterType;
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_FILTERTYPE_SEQUENCENUMBER						    (0x01u)
#define BLEGLUCOSECOLLECTOR_CONTROLPOINT_FILTERTYPE_USERFACINGTIME						    (0x02u)

/** 
 * BleGlucoseCollectorControlPointOperand type
 */
typedef U16 BleGlucoseCollectorControlPointOperand;

/** 
 * BleGlucoseCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleGlucoseCollectorCallBack.
 */
typedef	U8	BleGlucoseCollectorEvent;

/** BLEGLUCOSECOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleGlucoseCollectorLinkUPInfo pointer.
 */
#define BLEGLUCOSECOLLECTOR_EVENT_LINKED						                            0x70

/** BLEGLUCOSECOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleGlucoseCollectorLinkUPInfo pointer.
 */
#define BLEGLUCOSECOLLECTOR_EVENT_UNLINKED					                                0x71

/** BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a glucose
 * measurement from the SENSOR.
 * The parms field indicates the glucose measurement information within the 
 * BleGlucoseCollectorGlucoseMeasurementInfo type.
 */
#define BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENT	                                    0x72

/** BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENTCONTEXT
 * Event received to indicate that this COLLECTOR has received a glucose
 * measurement context from the SENSOR.
 * The parms field indicates the glucose context information within the 
 * BleGlucoseCollectorGlucoseMeasurementContextInfo type.
 */
#define BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENTCONTEXT                                 0x73

/** BLEGLUCOSECOLLECTOR_EVENT_RECORDACCESSRESPONSE
 * Event received to indicate that this COLLECTOR has received a record
 * access response from the SENSOR.
 * It follows the call of BLEGLUCOSE_COLLECTOR_RecordAccessOperation().
 * The parms field indicates the record access operation information within
 * the BleGlucoseCollectorRecordAccessOperationInfo type.
 */
#define BLEGLUCOSECOLLECTOR_EVENT_RECORDACCESSRESPONSE                                      0x74

/** 
 * BleGlucoseCollectorFeatureBitfield type
 * Bitfield defining the remote Glucose SENSOR capabilities.
 * It defines after the linkUP has completed, what are the 
 * glucose specification optional features supported by
 * the remote device.
 */
typedef	U16	BleGlucoseCollectorFeatureBitfield;
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_LOWBATTERYDETECTIONDURINGMEASUREMENT		    (0x0001u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORMALFUNCTIONDETECTION				        (0x0002u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORSAMPLESIZE							    (0x0004u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORSTRIPINSERTIONERRORDETECTION		        (0x0008u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORSTRIPTYPEERRORDETECTION				    (0x0010u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORRESULTHIGHLOWDETECTION				    (0x0020u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORTEMPERATUREHIGHLOWDETECTION			    (0x0040u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_SENSORREADINTERRUPTDETECTION				    (0x0080u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_GENERALDEVICEFAULT						        (0x0100u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_TIMEFAULT									    (0x0200u)
#define BLE_GLUCOSECOLLECTOR_GLUCOSEFEATURE_MULTIPLEBONDDETECTION						    (0x0400u)

/** 
 * BleGlucoseCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLEGLUCOSE_COLLECTOR_LinkUpGlucoseProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEGLUCOSE_COLLECTOR_LinkUpGlucoseProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLEGLUCOSECOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Glucose SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly.
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
    U8										configValue[2];
	AttHandle								glucoseServiceStartingHandle;
	AttHandle								glucoseServiceEndingHandle;

    AttHandle								deviceInformationServiceStartingHandle;
	AttHandle								deviceInformationServiceEndingHandle;

	AttHandle								glucoseMeasurementValueHandle;
	AttHandle								glucoseMeasurementConfigHandle;
	
    AttHandle								glucoseMeasurementContextValueHandle;
	AttHandle								glucoseMeasurementContextConfigHandle;

	AttHandle								glucoseFeatureValueHandle;
    BleGlucoseCollectorFeatureBitfield      glucoseFeatureValue;

    AttHandle								recordAccessControlPointValueHandle;
    AttHandle								recordAccessControlPointConfigHandle;
    U8										recordAccessControlPointValue[7];
    U8										recordAccessConfigValue[2];

    //Remote sensor features is different than glucoseFeatureValue 
    //which indicates sensor supported detection or fault  
    U16                                     remoteSensorFeatures;

} BleGlucoseCollector;

/** 
 * BleGlucoseCollectorMeasurementFlags type
 * Define the current Glucose Measurement flags that have been set during the 
 * call of BLEGLUCOSECOLLECTOR_SetGlucoseMeasurement() by the SENSOR.
 */
typedef union {
    U8  glucoseMeasurementFlagsValue;

    struct 
    {
        //The Time Offset field shall be included in the Glucose Measurement
        //characteristic value whenever the value of the Time Offset changes
        //from the last reported measurement.
        unsigned int timeOffsetPresent:1;

        //The Glucose Concentration field is optional, but if it is present, 
        //the Type-Sample Location field shall also be present.
        unsigned int concentrationPresent:1;

        //Concentration unit can be specified by using this field.
        //In that case 0 is for kg/L and 1 for mol/L.
        unsigned int concentrationUnit:1;

        //The Sensor Status Annunciation field may be included in the Glucose
        //Measurement characteristic value if the device supports Sensor 
        //Status Annunciation flags.The sensor status annunciation indicates 
        //sensor features information. It has to be used if at least one 
        //glucose feature is supported.
        unsigned int sensorStatusAnnunciationPresent:1;

        //If a Glucose Measurement characteristic includes contextual 
        //information (i.e., a corresponding Glucose Measurement Context 
        //characteristic), the Context Information Follows Flag (bit 4 
        //of the Flags field) shall be set to 1, otherwise the Flag shall
        //be set to 0.
        unsigned int contextInformationFollows:1;
        unsigned int reserved:3;

    } fields;
} BleGlucoseCollectorMeasurementFlags;

/** 
 * BleGlucoseCollectorMeasurementContextFlags type
 * Define the current Glucose Measurement Context flags that have been set 
 * during the call of BLEGLUCOSECOLLECTOR_SetGlucoseMeasurementContext() by
 * the SENSOR.
 */
typedef union {
    U8  glucoseMeasurementContextFlagsValue;

    struct 
    {
        //The Carbohydrate ID field and the Carbohydrate field are optional,
        //but when one is used, both shall be used.
        unsigned int carbohydrateIDAndCarbohydratePresent:1;

        //The Meal field is optional.
        unsigned int mealPresent:1;

        //The Tester nibble and the Health nibble comprise one octet.
        //Therefore when one nibble is present, both nibbles shall be present.
        unsigned int testerHealthPresent:1;

        //The Exercise Duration field and the Exercise Intensity are optional,
        //but when one is used, both shall be used.
        unsigned int exerciseDurationAndExerciseIntensityPresent:1;

        //The Medication ID field and the Medication field are optional, but
        //when one is used, both shall be used.
        unsigned int medicationIDAndMedicationPresent:1;

        //Units of kilograms or liters.
        unsigned int medicationValueUnits:1;

        //The HbA1c field is optional.
        unsigned int hbA1cPresent:1;

        //The Extended Flags field is optional.
        //Reserved for future use.
        unsigned int extendedFlagsPresent:1;                   	
    } fields;
} BleGlucoseCollectorMeasurementContextFlags;

/** 
 * BleGlucoseCollectorLinkMode type
 * Type passed during the 
 * BLEGLUCOSE_COLLECTOR_LinkUpGlucoseProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEGLUCOSE_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLEGLUCOSE_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleGlucoseCollectorLinkMode;

#define BLEGLUCOSE_COLLECTOR_LINKUP_COMPLETE			                                    1
#define BLEGLUCOSE_COLLECTOR_LINKUP_RESTORE		    	                                    2

/** 
 * BleGlucoseCollectorLinkUPInfo type
 * structure passed in parameter in the BleGlucoseCollectorCallBack
 * parms fields during the BLEGLUCOSECOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleGlucoseCollector : The collector for which the event is destinated.
 * connHandle : The connection handle for which the linkup has completed.
 * glucoseFeatureBitfield : The supported features of the glucose sensor.
 */
typedef struct {

	BleGlucoseCollector					    *bleGlucoseCollector;
	U16										connHandle;
	BleGlucoseCollectorFeatureBitfield      glucoseFeatureBitfield;

} BleGlucoseCollectorLinkUPInfo;

/** 
 * BleGlucoseCollectorRecordAccessOperationInfo type
 * structure passed in parameter in the BleGlucoseCollectorCallBack
 * parms fields during the BLEGLUCOSECOLLECTOR_EVENT_RECORDACCESSRESPONSE
 * event. If operation is Report/Delete or Count number of records
 * in case of failure, the internal structure responseCodeInfo shall be used.
 * If Count number of records operation is success the internal structure 
 * numberOfRecordsInfo shall be used.
 * 
 * bleGlucoseCollector : The collector for which the event is destinated.
 * opCode : The record access operation response op code.
 * operator : The record access operation response operator (Null).
 *
 * responseCodeInfo: Internal structure used in case of Op code = Response Code.
 *      requestOpCode : The record access request op code.
 *      status : The record access operation status.
 *
 * numberOfRecordsInfo: Internal structure used in case of Op code = Number of
 * Stored Records Response.
 *      numberOfRecords : The number of records.
 */

typedef struct {

	BleGlucoseCollector					        *bleGlucoseCollector;
	BleGlucoseCollectorControlPointOperation    opCode;
    BleGlucoseCollectorControlPointOperator     operator;
    union {
        struct {
            BleGlucoseCollectorControlPointOperation    requestOpCode;
            BleGlucoseCollectorControlPointStatus       status;
        } responseCodeInfo;
        struct {
            BleGlucoseCollectorControlPointOperand      numberOfRecords;
        } numberOfRecordsInfo;
    } recordAccessOperationInfo;

} BleGlucoseCollectorRecordAccessOperationInfo;
/** 
 * BleGlucoseCollectorGlucoseMeasurementInfo type
 * structure passed in parameter in the BleGlucoseCollectorCallBack
 * parms fields during the BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENT event.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleGlucoseCollector			            *bleGlucoseCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16									    connHandle;

    // The Flags field is an 8-bit bit field which indicates the unit used in
    // the Glucose Concentration field (if used), what fields are present in 
    // the Glucose Measurement characteristic, and whether or not context 
    // information is included.
    BleGlucoseCollectorMeasurementFlags		flagsValue;

    // The Sequence Number is an unsigned 16-bit integer that represents the
    // chronological order of the patient records in the Server measurement 
    // database.
    U16									    sequenceNumberValue;

    // The Base Time field represents the value of an internal real-time clock
    // or equivalent that keeps time relative to its initial setting in 
    // resolution of seconds.
    BleDateTime							    baseTimeValue;

    //The Time Offset field shall be included in the Glucose Measurement
    //characteristic value whenever the value of the Time Offset changes
    //from the last reported measurement.
    //The Time Offset field is defined as a 16-bit signed integer representing
    //the number of minutes the user-facing time differs from the Base Time.
    S16									    timeOffsetValue;

    //Glucose measurement Value, in kg/L or mol/L	
    //The Glucose Concentration field is optional, but if it is present, 
    //the Type-Sample Location field shall also be present
    U16     	    					    concentrationValue;
    U8									    sampleTypeValue;
    U8									    sampleLocationValue;

    //The Sensor Status Annunciation field may be included in the Glucose
    //Measurement characteristic value if the device supports Sensor 
    //Status Annunciation flags.
    U16									    sensorStatusAnnunciationValue;
  
} BleGlucoseCollectorGlucoseMeasurementInfo;

/** 
 * BleGlucoseCollectorGlucoseMeasurementContextInfo type
 * structure passed in parameter in the BleGlucoseCollectorCallBack
 * parms fields during the BLEGLUCOSECOLLECTOR_EVENT_GLUCOSEMEASUREMENTCONTEXT
 * event.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleGlucoseCollector			            *bleGlucoseCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming.
	 */
	U16									    connHandle;

    //Containing units of medication and used to show presence of optional
    //fields
	BleGlucoseCollectorMeasurementContextFlags	
                                            flagsValue;

    // The Sequence Number value shall be the same as the value of the 
    // Sequence Number of its corresponding Glucose Measurement 
    // characteristic.
    U16                                     sequenceNumberValue;

    // The Extended Flags field is optional. The existence of the Extended 
    // Flags field is indicated by setting bit 7 of the Flags field.
    U8     	    					        extendedFlagsValue;

    // The Carbohydrate ID field and the Carbohydrate field are optional, but
    // when one is used, both shall be used.
    U8									    carbohydrateID;
    U16									    carbohydrateValue;
    
    // The Meal field is optional.
    U8									    mealValue;

    // The Tester-Health field is optional. The Tester nibble and the Health 
    // nibble comprise one octet. Therefore, when one nibble is present, both
    // nibbles shall be present.
    // In memory we will use U8 but during sending only one octet will be 
    // used.
    U8									    testerValue;
    U8									    healthValue;

    // The Exercise Duration field and the Exercise Intensity are optional, 
    // but when one is used, both shall be used.
    // Exercice duration in seconds
    // Intensity in percentage
    U16                                     exerciseDurationValue;
    U8                                      exerciseIntensityValue;

    // The Medication ID field and the Medication field are optional, but when
    // one is used, both shall be used.
    U8                                      medicationID;
    //Medication - units of kilograms or liters
    U16                                     medicationValue;
    // The HbA1c field is optional.
    // Unit of percentage
    U16                                     hbA1cValue;
     
} BleGlucoseCollectorGlucoseMeasurementContextInfo;

/**
 * BleGlucoseCollectorCallBack
 *	This callback receives the BLE GLUCOSE COLLECTOR events. 
 *  Each of these events can be associated with a defined status and 
 *  parameter.
 *	The callback is executed during the stack context,  be careful to not 
 *  doing heavy process in this function.
 */
typedef void (*BleGlucoseCollectorCallBack)(
    	BleGlucoseCollectorEvent	event,
    	BleStatus					status,
    	void					    *parms);

/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Glucose profile 
 *
 * BLEGLUCOSE_COLLECTOR_Init()
 *	This function is the entry point of a Glucose profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled.
 *
 * @param callback: The BleGlucoseCollectorCallBack in which the collector 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Guillaume FERLIN
 */
BleStatus BLEGLUCOSE_COLLECTOR_Init(
		BleGlucoseCollectorCallBack callback);

/** De-Init the Collector Role of the Glucose Profile.
 *
 * BLEGLUCOSE_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Glucose Profile.
 *
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Guillaume FERLIN
 */
BleStatus BLEGLUCOSE_COLLECTOR_Deinit( void ) ;

/** Link Up the Glucose Profile with the specified remote Device
 *
 * BLEGLUCOSE_COLLECTOR_LinkUpGlucoseProfile()
 *	This function is used to link-Up the Glucose Profile with the 
 *  specified remote device.
 *  It will try to discover the Glucose profile in the remote device 
 *  using the given collector.
 * BLEGLUCOSECOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Glucose COLLECTOR to link-up
 *					with a remote Glucose SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as 
 *                  parameter to the profile API in order to identify to which
 *                  linked SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE GLUCOSE COLLECTOR and the remote BLE GLUCOSE
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory 
 *			or complete link-up.
 * @param info: Only valid if mode is BLEGLUCOSE_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleGlucoseCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *			BLEGLUCOSECOLLECTOR_EVENT_LINKED event will be received in the
 *			callback registered during the call of
 *			BLEGLUCOSE_COLLECTOR_Init. This status is returned only if mode
 *			is BLEGLUCOSE_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEGLUCOSE_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEGLUCOSE_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLEGLUCOSECOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if  
 *		that the remote device is not bonded or the persistent memory does not 
 *		enabled contains enough information about the remote Glucose SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Glucose Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Guillaume FERLIN
 */
BleStatus BLEGLUCOSE_COLLECTOR_LinkUpGlucoseProfile(
		BleGlucoseCollector             *collector,
		U16                             connHandle,
		BleGlucoseCollectorLinkMode     linkUpMode,
		BleGlucoseCollectorLinkUPInfo   *info);

/** Un Link the Glucose Profile with the specified linked COLLECTOR
 *
 * BLEGLUCOSE_COLLECTOR_UnLinkGlucoseProfile()
 *	This function is used to UnLink the Glucose Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Glucose Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_GLUCOSE_* from persistent memory
 *
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Glucose COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Guillaume FERLIN
 */
BleStatus BLEGLUCOSE_COLLECTOR_UnLinkGlucoseProfile(
		BleGlucoseCollector *collector);

/** Record Access Operation on remote linked SENSOR
 *
 * BLEGLUCOSE_COLLECTOR_RecordAccessOperation()
 *	This function is used to run operations on linked SENSOR 
 *  glucose records database. Operation can be report/delete or count
 *  number of records.
 *   
 * BLEGLUCOSE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked GLUCOSE COLLECTOR.
 * @param operation: A Record Access Operation.
 * @param operator: An operator.
 * @param operand1: First operand.
 * @param operand2: Second operand.
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
 *		completed the BLEGLUCOSECOLLECTOR_EVENT_RECORDACCESSRESULT  
 *		event will be received in the callback with the status of the 
 *		operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked, or the remote sensor is
 *       not supporting the record access control point.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	    an other GLUCOSE profile Operation is in Progress, wait 
 *       for this operation completion.
 *
 * @author Guillaume FERLIN
 */
BleStatus BLEGLUCOSE_COLLECTOR_RecordAccessOperation(
        BleGlucoseCollector                         *collector,
        BleGlucoseCollectorControlPointOperation    operation, 
        BleGlucoseCollectorControlPointOperator     operator,
        BleGlucoseCollectorControlPointFilterType   filterType,
        BleGlucoseCollectorControlPointOperand      operand1,
        BleGlucoseCollectorControlPointOperand      operand2);

/****************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\****************************************************************************/

#endif //(BLEGLUCOSE_SUPPORT_COLLECTOR== 1)

#endif /*__BLEGLUCOSE_COLLECTOR_H*/
