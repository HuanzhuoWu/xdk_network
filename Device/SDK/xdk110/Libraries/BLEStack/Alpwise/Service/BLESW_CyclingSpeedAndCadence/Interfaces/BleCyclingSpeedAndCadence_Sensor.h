#ifndef __BLECYCLINGSPEEDANDCADENCE_SENSOR_H
#define __BLECYCLINGSPEEDANDCADENCE_SENSOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleCyclingSpeedAndCadence_Sensor.h
 *
 * Description:   Contains interfaces definitions for Cycling Speed And Cadence 
 *					Profile when the local device operates in the SENSOR
 *					role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR
 * Defines if the BLE CYCLINGSPEEDANDCADENCE SENSOR Profile implementation supports the 
 * SENSOR Role
 * If enabled ( set to 1 ) it enables all the following BLE CYCLINGSPEEDANDCADENCE 
 * SENSOR Profile SENSOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR
#define BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR					0
#endif //BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR

#if (BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR== 1)
//
// Then check dependencies
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleCyclingSpeedAndCadence_Service.h"


// Cycling Speed And Cadence Service is mandatory
#if (BLE_SUPPORT_CYCLINGSPEEDANDCADENCE_SERVICE == 0)
#error BLE_SUPPORT_CYCLINGSPEEDANDCADENCE_SERVICE shall be enabled when BleCyclingSpeedAndCadence SENSOR Role is enabled
#endif //(BLE_SUPPORT_CYCLINGSPEEDANDCADENCE_SERVICE == 0)

// device Information Service is mandatory
#if (BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)
#error BLE_SUPPORT_DEVICEINFORMATION_SERVICE shall be enabled when BleCyclingSpeedAndCadence SENSOR Role is enabled
#endif //(BLE_SUPPORT_DEVICEINFORMATION_SERVICE == 0)

#if (BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER shall be enabled when BleCyclingSpeedAndCadence SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MODELNUMBER == 0) 

#if (BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 
#error BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME shall be enabled when BleCyclingSpeedAndCadence SENSOR Role is enabled
#endif //(BLEDEVICEINFORMATION_SUPPORT_MANUFACTURERNAME == 0) 



/***************************************************************************\
 * OPTIONAL FEATURES FOR CYCLINGSPEEDANDCADENCE SENSOR PROFILE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE CYCLING SPEED AND CADENCE profile in SENSOR Role.
 *
 * BLECYCLINGSPEEDANDCADENCE_SENSOR_Register()
 *	This function is used to register and initialise a BLE  
 *  CYCLING SPEED AND CADENCE profile in SENSOR role.
 *  It will add the supported CYCLING SPEED AND CADENCE Service 
 *  characteristics and the supported DEVICE INFORMATION Service  
 *  characteristics into the Attribute database.
 *	This Interface shall be the entry point of a BLE 
 *  CYCLING SPEED AND CADENCE profile in SENSOR Role.
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR shall be enabled.
 *
 * @param callback : The function callback where the Cycling Speed and Cadence
 *  service event will be posted.
 *  As the only event supported is received when the total distance is updated
 *  this parameter is ignored and can be null, i.e. equal to 
 * (BleCyclingSpeedAndCadenceServiceCallBack) 0, if 
 * BLE_CYCLINGSPEEDANDCADENCESERVICE_SUPPORT_TOTAL_DISTANCE is disabled (set to 0)
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLECYCLINGSPEEDANDCADENCE_SENSOR_Register( 
        BleCyclingSpeedAndCadenceServiceCallBack callback );


/***************************************************************************\
 * OPTIONAL API functions definition CYCLINGSPEEDANDCADENCE SENSOR PROFILE
\***************************************************************************/

#endif //(BLECYCLINGSPEEDANDCADENCE_SUPPORT_SENSOR== 1)

#endif /*__BLECYCLINGSPEEDANDCADENCE_SENSOR_H*/
