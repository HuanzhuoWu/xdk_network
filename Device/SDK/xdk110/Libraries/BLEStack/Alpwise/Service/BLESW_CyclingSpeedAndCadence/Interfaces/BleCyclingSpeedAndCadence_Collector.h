#ifndef __BLECYCLINGSPEEDANDCADENCE_COLLECTOR_H
#define __BLECYCLINGSPEEDANDCADENCE_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleCyclingSpeedAndCadence_Collector.h
 *
 * Description:   Contains interfaces definition for Cycling Speed And 
 *					Cadence Profile when the local device operates in 
 *					the COLLECTOR role.
 * 
 * Created:       August, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR
 * Defines if the BLE CYCLING SPEED AND CADENCE Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE CYCLING SPEED
 * AND CADENCE Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR
#define BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR							0
#endif //BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR

#if (BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when CYCLING SPEED AND CADENCE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE CYCLING SPEED AND CADENCE Profile implementation in 
 * COLLECTOR role supports saving the CYCLING SPEED AND CADENCE SENSOR information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the CYCLING SPEED AND CADENCE SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and CSC 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * it saves 5 bytes into persistent memory
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION


#define BLEINFOTYPE_CYCLINGSPEEDANDCADENCE_REMOTEFEATURES		0xBA	//1 byte
#define BLEINFOTYPE_CYCLINGSPEEDANDCADENCE_CSCMEASUREMENTHANDLE	0xBB	//2 bytes
#define BLEINFOTYPE_CYCLINGSPEEDANDCADENCE_SCCONTROLPOINTHANDLE	0xBC	//2 bytes


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/
/** 
 *BleCyclingSpeedAndCadenceCollectorFeatureBitfield type
 * Bitfield defining the remote Cycling Speed and Cadence SENSOR capabilities.
 * It defines after the linkUP has completed, what are the 
 * Cycling Speed and Cadence specification optional defined features supported by
 * the remote device.
 */
typedef	U16	BleCyclingSpeedAndCadenceCollectorFeatureBitfield;
// Remote SENSOR supports the "Wheel revolution data" feature
#define BLECSCCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED	        (0x0001u)
// Remote SENSOR supports the "Crank Revolution data" feature
#define BLECSCCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED			(0x0002u)
// Remote SENSOR supports the "Multiple sensor locations" feature
#define BLECSCCOLLECTOR_FEATUREBITMASK_MULTIPLESENSORLOCATIONSSUPPORTED		(0x0004u)

/** 
 * BleCyclingSpeedAndCadenceCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleCyclingSpeedAndCadenceCollectorCallback
 */
typedef	U8	BleCyclingSpeedAndCadenceCollectorEvent;

/** BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleCyclingSpeedAndCadenceCollectorLinkUPInfo pointer.
 */
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED						0x8B

/** BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleCyclingSpeedAndCadenceCollectorLinkUPInfo pointer.
 */
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED					0x8C

/** BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CSCMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a CSC
 * SENSOR measurement from the SENSOR.
 * It follows an alert from the Sensor
 * the status field indicates the status of the operation.
 * The parms field indicates the CSC measurement information within the 
 * BleCyclingSpeedAndCadenceCollectorCscMeasurementInfo type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleCyclingSpeedAndCadenceCollector fields are consistent. Other fields
 *   of BleCyclingSpeedAndCadenceCollectorCscMeasurementInfo must be ignored
 * - If status is BLESTATUS_SUCCESS the measurement fields contains consistent
 *   information.
 */
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CSCMEASUREMENT	            0x8D

/** BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CUMULATIVEVALUESET
 * Event received to indicate that this COLLECTOR has set a new cumulative
 * value.
 * It follows the call of BLECYCLINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CUMULATIVEVALUESET		    0x8E

/**
 * BleCyclingSpeedAndCadenceCollectorCallback
 *	This callback receives the BLE CYCLING SPEED AND CADENCE COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleCyclingSpeedAndCadenceCollectorCallback)(
	BleCyclingSpeedAndCadenceCollectorEvent	event,
	BleStatus					            status,
	void					                *parms); 

/** 
 * BleCyclingSpeedAndCadenceCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LinkUpCyclingSpeedAndCadenceProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LinkUpCyclingSpeedAndCadenceProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Cycling Speed And Cadence SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								cyclingSpeedAndCadenceServiceStartingHandle;
	AttHandle								cyclingSpeedAndCadenceServiceEndingHandle;

	AttHandle								cscMeasurementValueHandle;
	AttHandle								cscMeasurementConfigHandle;

	AttHandle								sensorLocationHandle;

	AttHandle								cscFeatureValueHandle;

    BleCyclingSpeedAndCadenceCollectorFeatureBitfield   
                                            cscFeatureValue;

	AttHandle								scControlPointValueHandle;
	AttHandle								scControlPointConfigHandle;
	U8										scControlPointValue[5];

	U8										cscConfigValue[2];

	U16										cyclingSpeedAndCadenceSensorFeatures;

} BleCyclingSpeedAndCadenceCollector;


/** 
 * BleCyclingSpeedAndCadenceCollectorLinkUPInfo type
 * structure passed in parameter in the BleCyclingSpeedAndCadenceCollectorCallback
 * parms fields during the BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleCyclingSpeedAndCadenceCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * supportedFeatures : The sensor supported features
 */
typedef struct {

	BleCyclingSpeedAndCadenceCollector			        *bleCyclingSpeedAndCadenceCollector;
	U16											        connHandle;
    BleCyclingSpeedAndCadenceCollectorFeatureBitfield   supportedFeatures;
} BleCyclingSpeedAndCadenceCollectorLinkUPInfo;

/** 
 * BleCyclingSpeedAndCadenceCollectorFlags type
 * Bitfield defining the available flags values.
 */
typedef U8 BleCyclingSpeedAndCadenceCollectorFlags;
#define BLECSCCOLLECTOR_FLAGBITMASK_WHEELREVOLUTIONDATAMEASUREMENT              (0x01u)
#define BLECSCCOLLECTOR_FLAGBITMASK_CRANKREVOLUTIONDATAMEASUREMENT              (0x02u)

/** 
 * BleCyclingSpeedAndCadenceCollectorMeasurementFlags type
 * Bitfield defining the flags that can be present in the CSC measurement
 * can be accessed directly via the cscMeasurementFlagsValue byte or 
 * bit-by-bit in the fields structure.
 */
typedef union {
    /** cscMeasurementFlagsValue:
     * A byte indicating what are the fields included in the measurement
     */
	BleCyclingSpeedAndCadenceCollectorFlags cscMeasurementFlagsValue;

	struct 
	{
        /** wheelRevolutionDataPresent:
         * Field indicating if the wheel revolution data 
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECSCCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
         * is not supported.
         */
		unsigned int wheelRevolutionDataPresent:1;

        /** crankRevolutionDataPresent:
         * Field indicating if the crank revolution data  is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECSCCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
         * is not supported.
         */
		unsigned int crankRevolutionDataPresent:1;

		//unsigned int reserved:6;
	} fields;

} BleCyclingSpeedAndCadenceCollectorMeasurementFlags;


/** 
 * BleCyclingSpeedAndCadenceCollectorCscMeasurementInfo type
 * structure passed in parameter in the BleCyclingSpeedAndCadenceCollectorCallback
 * parms fields during the BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CSCMEASUREMENT event.
 * It describes the Cycling Speed And cadence SENSOR measurement.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleCyclingSpeedAndCadenceCollector		*bleCyclingSpeedAndCadenceCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16									    connHandle;

    /*
     * The flags indicating which Value is present in the measurement,
     * only valid if BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     */
	BleCyclingSpeedAndCadenceCollectorMeasurementFlags
                                            flags;

    /** CumulativeWheelRevolution:
     * The Cumulative Wheel Revolutions value represents the number of times a 
     * wheel rotates, is used in combination with the Last Wheel Event Time
     * and the wheel circumference stored on the Client to determine:
     *  - the speed of the bicycle
     *  - the distance traveled. 
     * This value is expected to be set to 0 (or another desired value in case 
     * of e.g. a sensor upgrade) at initial installation on a bicycle.
     *
     * Only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECSCCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
     * is supported.
     */
	U32									     cumulativeWheelRevolution ;

    /** lastWheelEvent:
     * The 'wheel event time' is a free-running-count of 1/1024 second units 
     * and it represents the time when the wheel revolution was detected by 
     * the wheel rotation sensor. Since several wheel events can occur between
     * transmissions, only the Last Wheel Event Time value is transmitted. 
     * The Last Wheel Event Time value rolls over every 64 seconds.
     *
     * Only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECSCCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
     * is supported.
     */
	U16									    lastWheelEvent;

    /** CumulativeCrankRevolution:
     * The Cumulative Crank Revolutions value, which represents the number of 
     * times a crank rotates, is used in combination with the Last Crank Event
     * Time to determine:
     *  - if the cyclist is coasting 
     *  - the average cadence. 
     * Average cadence is not accurate unless 0 cadence events (i.e. coasting) 
     * are subtracted.  This value is intended to roll over and is not 
     * configurable.
     *
     * Only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECSCCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
     * is supported.
     */
	U16									     cumulativeCrankRevolution ;

    /** lastCrankEvent:
     * The 'crank event time' is a free-running-count of 1/1024 second units
     * and it represents the time when the crank revolution was detected by
     * the crank rotation sensor. Since several crank events can occur between
     * transmissions, only the Last Crank Event Time value is transmitted. 
     * This value is used in combination with the Cumulative Crank Revolutions
     * value to enable the Client to calculate cadence. 
     * The Last Crank Event Time value rolls over every 64 seconds.
     *
     * Only valid if BLERUNNINGSPEEDANDCADENCECOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECSCCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
     * is supported.
     */
	U16									    lastCrankEvent;

} BleCyclingSpeedAndCadenceCollectorCscMeasurementInfo;


/** 
 * BleCyclingSpeedAndCadenceCollectorLinkMode type
 * Type passed during the 
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LinkUpCyclingSpeedAndCadenceProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleCyclingSpeedAndCadenceCollectorLinkMode;

#define BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE			1
#define BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Cycling Speed And Cadence profile 
 *
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_Init()
 *	This function is the entry point of a Cycling Speed And Cadence profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleCyclingSpeedAndCadenceCollectorCallback in which 
 *				 the collector events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLECYCLINGSPEEDANDCADENCE_COLLECTOR_Init(
			BleCyclingSpeedAndCadenceCollectorCallback callback);

/** De-Init the Collector Role of the Cycling Speed And Cadence Profile.
 *
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Cycling Speed And Cadence Profile.
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLECYCLINGSPEEDANDCADENCE_COLLECTOR_Deinit( void ) ;

/** Link Up the Cycling Speed And Cadence Profile with the specified remote Device
 *
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LinkUpCyclingSpeedAndCadenceProfile()
 *	This function is used to link-Up the Cycling Speed And Cadence Profile with the 
 *  specified remote device.
 *  It will try to discover the Cycling Speed And Cadence profile in the remote device 
 *  using the given collector.
 * BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Cycling Speed And Cadence COLLECTOR to link-up
 *					with a remote Cycling Speed And Cadence SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to the profile API in order to identify to which linked 
 *                  SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE CYCLING SPEED AND CADENCE COLLECTOR and the remote BLE CYCLING SPEED AND CADENCE
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleCyclingSpeedAndCadenceCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *		BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_LINKED event will be received in the
 *		callback registered during the call of
 *		BLECYCLINGSPEEDANDCADENCE_COLLECTOR_Init. This status is returned only if mode
 *		is BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLECYCLINGSPEEDANDCADENCECOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Cycling Speed And Cadence SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	    an other Cycling Speed And Cadence Operation is in Progress, wait for this
 *      operation completion.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLECYCLINGSPEEDANDCADENCE_COLLECTOR_LinkUpCyclingSpeedAndCadenceProfile(
						BleCyclingSpeedAndCadenceCollector *collector,
						U16 connHandle,
						BleCyclingSpeedAndCadenceCollectorLinkMode linkUpMode,
						BleCyclingSpeedAndCadenceCollectorLinkUPInfo *info
						);

/** Un Link the Cycling Speed And Cadence Profile with the specified linked COLLECTOR
 *
 * BLECYCLINGSPEEDANDCADENCE_COLLECTOR_UnLinkCyclingSpeedAndCadenceProfile()
 *	This function is used to UnLink the Cycling Speed And Cadence Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Cycling Speed And Cadence Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_CYCLINGSPEEDANDCADENCE_* from persistent memory
 *
 * BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Cycling Speed And Cadence COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLECYCLINGSPEEDANDCADENCE_COLLECTOR_UnLinkCyclingSpeedAndCadenceProfile(
		BleCyclingSpeedAndCadenceCollector *collector);


/** set a new cumulative (total) distance in remote linked SENSOR
*
* BLECYCLINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue()
*	This function is used to set a new cumulative (total) distance
* in remote linked SENSOR, if the remote sensor supports total distance
* feature.
*   
* BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING SPEED AND CADENCE COLLECTOR. 
* @param value: The new value to set as cumulative value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGSPEEDANDCADENCECOLLECTOR_EVENT_CUMULATIVEVALUESET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*	an other CYCLING SPEED AND CADENCE profile Operation is in Progress, wait for this
*  operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING SPEED AND CADENCE profile seems not been initialized).
*
* @author Alexandre GIMARD
*/
BleStatus BLECYCLINGSPEEDANDCADENCE_COLLECTOR_SetCumulativeValue( 
    BleCyclingSpeedAndCadenceCollector *collector,
    U32                                 value);

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLECYCLINGSPEEDANDCADENCE_SUPPORT_COLLECTOR== 1)

#endif /*__BLECYCLINGSPEEDANDCADENCE_COLLECTOR_H*/
