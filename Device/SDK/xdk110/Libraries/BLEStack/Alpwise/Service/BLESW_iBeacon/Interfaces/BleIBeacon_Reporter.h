#ifndef __BLEIBEACON_REPORTER_H
#define __BLEIBEACON_REPORTER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleIBeacon_Reporter.h
 *
 * Description:   Contains interfaces routines for iBeacon Profile
 *                  when the local device operates in the REPORTER role.
 * 
 * Created:       January, 2014
 * Version:       0.1
 *
 * File Revision: $Rev: 2824 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"

/***************************************************************************\
 *  Macro and constant definition
\***************************************************************************/

/***************************************************************************
 *  Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEIBEACON_SUPPORT_REPORTER
 * Defines if the BLE iBeacon Profile implementation supports the 
 * REPORTER Role
 * If enabled ( set to 1 ) it enables all the following BleIBeacon
 * REPORTER configuration and Application Interface
 * The default value for this option is disabled (0).
 */
#ifndef BLEIBEACON_SUPPORT_REPORTER
#define BLEIBEACON_SUPPORT_REPORTER                             0
#endif //BLEIBEACON_SUPPORT_REPORTER

#if (BLEIBEACON_SUPPORT_REPORTER == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/***************************************************************************\
 * CONSTANTS
\***************************************************************************/

/***************************************************************************\
 *  Type definition
\***************************************************************************/

/****************************************************************************\
 *  APPLICATION INTERFACE functions definition
\****************************************************************************/

/* Register a BLE IBEACON REPORTER.
 *
 * BLEIBEACON_REPORTER_Register()
 *  This function is used to register and initialise a BLE iBeacon 
 * REPORTER role.
 * It will add the specific advertisement data.
 *
 * BLEIBEACON_SUPPORT_REPORTER shall be enabled.
 *
 * @param  uuid: A proximity UUID (universally unique identifier), which 
 *                  is a 128-bit value that uniquely identifies one or more
 *                  beacons as being of a certain type or from a certain 
 *                  organization.
 * @param minor: A minor value, which is a 16-bit unsigned integer that is
 *                  used to differentiate beacons that have the same
 *                  proximity UUID and major value.
 * @param major: A major value, which is a 16-bit unsigned integer that is
 *                  used to differentiate beacons that have the same 
 *                  proximity UUID.
 * @param  rssi: RSSI of the device observed from one meter
 *               in its intended environment.
 *
 * return The status of the operation:
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed.
 */
BleStatus BLEIBEACON_REPORTER_Register( 
        U8 *uuid, 
        U16 minor, 
        U16 major, 
        U8 rssi );

#endif //(BLEIBEACON_SUPPORT_REPORTER== 1)

#endif //__BLEIBEACON_REPORTER_H
