#ifndef __BLELOCATIONANDNAVIGATION_SENSOR_H
#define __BLELOCATIONANDNAVIGATION_SENSOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleLocationAndNavigation_Sensor.h
 *
 * Description:   Contains interfaces definitions for Blood Pressure 
 *					Profile when the local device operates in the SENSOR
 *					role.
 * 
 * Created:       November, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR
 * Defines if the BLE LOCATIONANDNAVIGATION SENSOR Profile implementation supports the 
 * SENSOR Role
 * If enabled ( set to 1 ) it enables all the following BLE LOCATIONANDNAVIGATION 
 * SENSOR Profile SENSOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR
#define BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR					0
#endif //BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR

#if (BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR== 1)
//
// Then check dependencies
#include "Services/BleDeviceInformation_Service.h"
#include "Services/BleLocationAndNavigation_Service.h"
#include "Services/BleBattery_Service.h"


// Location And Navigation Service is mandatory
#if (BLE_SUPPORT_LOCATIONANDNAVIGATION_SERVICE == 0)
#error BLE_SUPPORT_LOCATIONANDNAVIGATION_SERVICE shall be enabled when BleLocationAndNavigation SENSOR Role is enabled
#endif //(BLE_SUPPORT_LOCATIONANDNAVIGATION_SERVICE == 0)


/***************************************************************************\
 * OPTIONAL FEATURES FOR LOCATIONANDNAVIGATION SENSOR PROFILE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE LOCATION AND NAVIGATION profile in SENSOR Role.
 *
 * BLELOCATIONANDNAVIGATION_SENSOR_Register() 
 *	This function is used to register and initialise a BLE LOCATION AND 
 *  NAVIGATION profile in SENSOR role.
 *  It will add the supported LOCATION AND NAVIGATION Service characteristics 
 *  and if supported, the DEVICE INFORMATION  and BATTERY Services and 
 *  characteristics into the Attribute database.
 *	This Interface shall be the entry point of a BLE LOCATION AND NAVIGATION
 *  profile in SENSOR Role.
 *
 * BLE_SUPPORT_LOCATIONANDNAVIGATION_SERVICE shall be enabled.
 *
 * @param callback : The function callback where the Location and navigation
 *  service event will be posted.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_SENSOR_Register( 
	BleLocationAndNavigationServiceCallBack callback );


/***************************************************************************\
 * OPTIONAL API functions definition LOCATIONANDNAVIGATION SENSOR PROFILE
\***************************************************************************/

#endif //(BLELOCATIONANDNAVIGATION_SUPPORT_SENSOR== 1)

#endif /*__BLELOCATIONANDNAVIGATION_SENSOR_H*/
