#ifndef __BLELOCATIONANDNAVIGATION_COLLECTOR_H
#define __BLELOCATIONANDNAVIGATION_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleLocationAndNavigation_Collector.h
 *
 * Description:   Contains interfaces definition for Location And Navigation 
 *                  Profile when the local device operates in the COLLECTOR
 *                  role.
 * 
 * Created:       March, 2013
 * Version:       0.1
 *
 * File Revision: $Rev: 2741 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************\
 *  CONFIGURATION
\***************************************************************************/

/**
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR
 * Defines if the BLE LOCATION AND NAVIGATION  Profile implementation
 * supports the COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE 
 * LOCATION AND NAVIGATION Profile COLLECTOR configurations
 *  and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR
#define BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR                          0
#endif //BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR

#if (BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when LOCATION AND NAVIGATION Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION  
 * Defines if the BLE LOCATION AND NAVIGATION  Profile implementation in 
 * COLLECTOR role supports saving the LOCATION AND NAVIGATION SENSOR 
 * information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the LOCATION AND NAVIGATION SENSOR, as this
 * information is  already known then the linkUp process can be simplified,
 * and Location And Navigation measurement Notification could be automaticaly
 * understood by this collector.
 * it saves 7 bytes into persistent memory
 *
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION 
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION                 0
#endif //BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_LOCATIONANDNAVIGATION_REMOTEFEATURES            0xC4    //1 byte
#define BLEINFOTYPE_LOCATIONANDNAVIGATION_LOCATIONANDSPEEDHANDLE    0xC5    //2 bytes
#define BLEINFOTYPE_LOCATIONANDNAVIGATION_NAVIGATIONHANDLE          0xC6    //2 bytes
#define BLEINFOTYPE_LOCATIONANDNAVIGATION_LNFEATUREHANDLE           0xC7    //2 bytes

#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION                 0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *  TYPES definition
\***************************************************************************/
//TOCHANGE : Event numbers 
/** 
 * BleLocationAndNavigationCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleLocationAndNavigationCollectorCallBack
 */
typedef U8  BleLocationAndNavigationCollectorEvent;

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleLocationAndNavigationCollectorLinkUPInfo pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LINKED                                      0x7D

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleLocationAndNavigationCollectorLinkUPInfo pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_UNLINKED                                    0x7E


/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEED
 * Event received to indicate that this COLLECTOR has received a location and speed
 * from the SENSOR.
 * The status field indicates the status of the operation.
 * The parms field indicates the location and speed information within the 
 * BleLocationAndNavigationCollectorLocationAndSpeedInfo type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleLocationAndNavigationCollector fields are consistent. Other fields
 *   of BleLocationAndNavigationCollectorLocationAndSpeedInfo must be ignored
 * - If status is BLESTATUS_SUCCESS the location and speed fields contains consistent
 *   information.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEED                            0x7F

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATION
 * Event received to indicate that this COLLECTOR has received a navigation info
 * from the SENSOR.
 * the status field indicates the status of the operation.
 * The parms field indicates the navigation information within the 
 * BleLocationAndNavigationCollectorNavigationInfo type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleLocationAndNavigationCollector fields are consistent. Other fields
 *   of BleLocationAndNavigationCollectorNavigationInfo must be ignored
 * - If status is BLESTATUS_SUCCESS the location and speed fields contains consistent
 *   information.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATION                                  0x80

//TOCOMMENT correctly
/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LNFEATURE
 * Event received to indicate that this COLLECTOR has received 
 * LN feature from the SENSOR.
 * The parms field indicates the Sensor LN feature information  
 * within the BleLocationAndNavigationCollectorLnFeatureInfo type.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LNFEATURE                                   0x81

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_TOTALDISTANCESET
 * Event received to indicate that this COLLECTOR has set a new total distance
 * value.
 * It follows the call of BLELOCATIONANDNAVIGATION_COLLECTOR_SetCumulativeValue().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_TOTALDISTANCESET                            0x82

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NUMBEROFROUTES
 * Event received to indicate that this COLLECTOR has received the number of routes 
 * stored in the SENSOR value.
 * It follows the call of BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNumberOfRoutes().
 * The parms field indicates the Control Point information  
 * within the BleLocationAndNavigationCollectorLnControlPointInfo type.
 * The status field indicates the status of the operation.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NUMBEROFROUTES                              0x83

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAMEOFROUTE
 * Event received to indicate that this COLLECTOR has received the name of a 
 * desired route stored in the SENSOR.
 * It follows the call of BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNameOfRoute().
 * The parms field indicates the Control Point information  
 * within the BleLocationAndNavigationCollectorLnControlPointInfo type.
 * The status field indicates the status of the operation.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAMEOFROUTE                                 0x84


/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONREQUESTPROCESSED
 * Event received to indicate that this COLLECTOR has set a navigation
 * control command.
 * It follows the call of BLELOCATIONANDNAVIGATION_COLLECTOR_ControlNavigation().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONREQUESTPROCESSED                  0x85

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDSUBSCRIPTION_RSP
 * Event received when the subscription of location and speed notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the collector for which the subscription
 * response is associated within a BleLocationAndNavigationCollector pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDSUBSCRIPTION_RSP            0x86        

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDUNSUBSCRIPTION_RSP
 * Event received when the unsubscription of location and speed notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the collector for which the unsubscription
 * response is associated within a BleLocationAndNavigationCollector pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDUNSUBSCRIPTION_RSP          0x87

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONSUBSCRIPTION_RSP
 * Event received when the subscription of navigation notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the collector for which the subscription
 * response is associated within a BleLocationAndNavigationCollector pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONSUBSCRIPTION_RSP                  0x88

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONUNSUBSCRIPTION_RSP
 * Event received when the unsubscription of navigation notification
 * is completed. The status field indicates the status of the operation.
 * The parms field indicates the collector for which the unsubscription
 * response is associated within a BleLocationAndNavigationCollector pointer.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONUNSUBSCRIPTION_RSP                0x89

/** BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_ROUTESELECTED
 * Event received to indicate that this COLLECTOR has set a command
 * to select a route.
 * It follows the call of BLELOCATIONANDNAVIGATION_COLLECTOR_SelectRoute.
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_ROUTESELECTED                               0x8A

/** 
 *BleLocationAndNavigationCollectorFeatureBitfield type
 * Bitfield defining the remote Location And Navigation SENSOR capabilities
 * it defines after the linkUP has completed, what are the 
 * Location And Navigation specification optional defined features supported by
 * the remote device.
 */
typedef U32 BleLocationAndNavigationCollectorFeatureBitfield;
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_INSTANTANEOUSSPEED                     (0x00000001u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_TOTALDISTANCE                          (0x00000002u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_LOCATION                               (0x00000004u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ELEVATION                              (0x00000008u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_HEADING                                (0x00000010u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ROLLINGTIME                            (0x00000020u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_UTCTIME                                (0x00000040u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_REMAININGDISTANCE                      (0x00000080u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_REMAININGVERTICALDISTANCE              (0x00000100u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ESTIMATEDTIMEOFARRIVAL                 (0x00000200u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_NUMBEROFSATELLITESINSOLUTION           (0x00000400u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_NUMBEROFSATELLITESINVIEW               (0x00000800u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_TIMETOFIRSTFIX                         (0x00001000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ESTIMATEDHORIZONTALPRECISIONERROR      (0x00002000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ESTIMATEDVERTICALPRECISIONERROR        (0x00004000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_HORIZONTALDILUTIONOFPRECISION          (0x00008000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_VERTICALDILUTIONOFPRECISION            (0x00010000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_LOCATIONANDSPEEDCONTENTMASKING         (0x00020000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_FIXRATESETTING                         (0x00040000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_ELEVATIONSETTING                       (0x00080000u)
#define BLE_LOCATIONANDNAVIGATIONCOLLECTOR_LNFEATURE_POSITIONSTATUS                         (0x00100000u)

//NAVIGATION CONTROL PROCEDURES
typedef U8 BleLocationAndNavigationCollectorControlNavigation;
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_STOPNAVIGATION              (0x00u)
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_STARTNAVIGATION             (0x01u)
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_PAUSENAVIGATION             (0x02u)
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_CONTINUENAVIGATION          (0x03u)
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_SKIPWAYPOINT                (0x04u)
#define BLELOCATIONANDNAVIGATIONCOLLECTOR_NAVIGATIONCONTROL_STARTNOTIFICATION           (0x05u)

/** 
 * BleLocationAndNavigationCollectorLocationAndSpeedFlags type
 * Define the current Location And Speed flags to be set during the 
 * call of BLELOCATIONANDNAVIGATIONSERVICE_SetLocationAndSpeed
 * It can be builded using the flag value or bit to bit definition
 */
typedef union {
    U16 locationAndSpeedFlagsValue;

    struct 
    {
        unsigned int instantaneousSpeedPresent:1;
        unsigned int totalDistancePresent:1;
        unsigned int locationPresent:1;
        unsigned int elevationPresent:1;
        unsigned int headingPresent:1;
        unsigned int rollingTimePresent:1;
        unsigned int utcTimePresent:1;
        unsigned int positionStatus:2;
        unsigned int speedandDistanceFormat:1;
        unsigned int elevationSource:2;
        unsigned int headingSource:1;
        //unsigned int reserved:3;
    } fields;

} BleLocationAndNavigationCollectorLocationAndSpeedFlags;

/** 
 * BleLocationAndNavigationCollectorNavigationFlags type
 * Define the current Navigation flags to be set during the 
 * call of BLELOCATIONANDNAVIGATIONSERVICE_SetNavigation()
 * It can be builded using the flag value or bit to bit definition
 */
typedef union {
    U16 navigationFlagsValue;

    struct 
    {
        unsigned int remainingDistancePresent:1;
        unsigned int remainingVerticalDistancePresent:1;
        unsigned int estimatedTimeofArrivalPresent:1;
        unsigned int positionStatus:2;
        unsigned int headingSource:1;
        unsigned int navigationIndicatorType:1;
        unsigned int waypointReached:1;
        unsigned int destinationReached:1;
        //unsigned int reserved:7;
    } fields;

} BleLocationAndNavigationCollectorNavigationFlags;

/**
 * BleLocationAndNavigationCollectorCallBack
 *  This callback receives the BLE LOCATION AND NAVIGATION COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *  The callback is executed during the stack context,  be careful to not doing 
 *  heavy process in this function.
 */
typedef void (*BleLocationAndNavigationCollectorCallBack)(
    BleLocationAndNavigationCollectorEvent  event,
    BleStatus                   status,
    void                       *parms); 

/** 
 * BleLocationAndNavigationCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLELOCATIONANDNAVIGATION_COLLECTOR_LinkUpLocationAndNavigationProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLELOCATIONANDNAVIGATION_COLLECTOR_LinkUpLocationAndNavigationProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Location And Navigation SENSOR, it means 
 * that if this local device is connected to multiple remote SENSORs the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
    /** 
     *  The Following fields are for INTERNAL USE ONLY
     * Do not address them directly
     */
    BleGattCommand                          gattCommand;
    BleGattCommand                          gattCnfCommand;

    U16                                     connHandle;
    U8                                      linked;
    U8                                      state;

    U8                                      configValue[2];
    AttHandle                               locationAndNavigationServiceStartingHandle;
    AttHandle                               locationAndNavigationServiceEndingHandle;

    AttHandle                               locationAndSpeedValueHandle;
    AttHandle                               locationAndSpeedConfigHandle;

    AttHandle                               navigationValueHandle;
    AttHandle                               navigationConfigHandle;

    AttHandle                               lnFeatureValueHandle;
    BleLocationAndNavigationCollectorFeatureBitfield lnFeatureValue;

    AttHandle                               positionQualityValueHandle;

    AttHandle                               lnControlPointValueHandle;
    AttHandle                               lnControlPointConfigHandle;
    U8                                      lnControlPointValue[4];
    U8                                      lnConfigValue[2];


    U16                                     locationAndNavigationSensorFeatures;

} BleLocationAndNavigationCollector;


/** 
 * BleLocationAndNavigationCollectorLinkUPInfo type
 * structure passed in parameter in the BleLocationAndNavigationCollectorCallBack
 * parms fields during the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleLocationAndNavigationCollector : the collector for which the event is
 *      destinated
 * connHandle : The connection handle for which the linkup has completed
 * remoteSensorFeatures : The supported features of the remote sensor
 */
typedef struct {

    BleLocationAndNavigationCollector           *bleLocationAndNavigationCollector;
    U16                                         connHandle;
    U16                                         remoteSensorFeatures;

} BleLocationAndNavigationCollectorLinkUPInfo;

/** 
 * BleLocationAndNavigationCollectorLnFeatureInfo type
 * structure passed in parameter in the BleLocationAndNavigationCollectorCallBack
 * parms fields during the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LNFEATURE event.
 * It indicates :
 * bleLocationAndNavigationCollector : the collector for which the event is
 *      destinated
 * connHandle : The connection handle for which the linkup has completed
 * BleLocationAndNavigationCollectorFeatureBitfield : The LN features bitfield
 */
typedef struct {

    BleLocationAndNavigationCollector                   *bleLocationAndNavigationCollector;
    U16                                                 connHandle;
    BleLocationAndNavigationCollectorFeatureBitfield    lnFeatureBitfield;
} BleLocationAndNavigationCollectorLnFeatureInfo;


/** 
 * BleLocationAndNavigationCollectorLocationAndSpeedInfo type
 * structure passed in parameter in the BleLocationAndNavigationCollectorCallBack
 * parms fields during the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEED event.
 */
typedef struct {
    /*
     * The link with the remote Sensor for which this 
     *  event has been received.
     */ 
    BleLocationAndNavigationCollector       *bleLocationAndNavigationCollector;
    /*
     * The connection handle from which the measurement information
     *  is coming
     */
    U16                                     connHandle;
    BleLocationAndNavigationCollectorLocationAndSpeedFlags
                                            flagsValue;
    U16                                     instantaneousSpeedValue;
    U32                                     totalDistanceValue;
//  S32                                     locationDataValue;
//  S24                                     elevationValue;
//  U8                                      rollingTimeValue;
//  BleDayDateTime                          utcTimeValue;
} BleLocationAndNavigationCollectorLocationAndSpeedInfo;

/** 
 * BleLocationAndNavigationCollectorNavigationInfo type
 * structure passed in parameter in the BleLocationAndNavigationCollectorCallBack
 * parms fields during the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATION event.
 */
typedef struct{ 
    /*
     * The link with the remote Sensor for which this 
     *  event has been received.
     */ 
    BleLocationAndNavigationCollector       *bleLocationAndNavigationCollector;
    /*
     * The connection handle from which the measurement information
     *  is coming
     */
    U16                                     connHandle;
    BleLocationAndNavigationCollectorNavigationFlags    
                                            flagsValue;
    U16                                     bearingValue;
    U16                                     headingValue;
    U32                                     remainingDistanceValue;
//  S24                                     remainingVerticalDistanceValue;
//  BleDayDateTime                          estimatedTimeOfArrivalValue;
} BleLocationAndNavigationCollectorNavigationInfo;


/** 
 * BleLocationAndNavigationCollectorLnControlPointInfo type
 * structure passed in parameter in the BleLocationAndNavigationCollectorCallBack
 * parms fields during the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAMEOFROUTE or 
 * BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NUMBEROFROUTES event.
 *
 */
typedef struct{ 
    /*
     * The link with the remote Sensor for which this 
     *  event has been received.
     */ 
    BleLocationAndNavigationCollector       *bleLocationAndNavigationCollector;
    /*
     * The connection handle from which the measurement information
     *  is coming
     */
    U16                                     connHandle;
    U16                                     nbOfRoutesStored;
    U8                                      *nameOfRoute;
    U8                                      valueLen;

} BleLocationAndNavigationCollectorLnControlPointInfo;




/** 
 * BleLocationAndNavigationCollectorLinkMode type
 * Type passed during the 
 * BLELOCATIONANDNAVIGATION_COLLECTOR_LinkUpLocationAndNavigationProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleLocationAndNavigationCollectorLinkMode;

#define BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_COMPLETE          1
#define BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_RESTORE           2

/****************************************************************************
 *  APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Location And Navigation profile 
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_Init()
 *  This function is the entry point of a Location And Navigation profile
 *  application that runs the COLLECTOR role, it inits the
 *  profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *  initialisation.
 *
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleLocationAndNavigationCollectorCallBack in which 
 *          the collector events will be received.
 *
 * @return The status of the operation:
 *  - BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed.
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (Profile is already initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_Init(
            BleLocationAndNavigationCollectorCallBack callback);

/** De-Init the Collector Role of the Location And Navigation Profile.
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_Deinit
 *  This function is used to De-Init the Collector Role of the 
 *  Location And Navigation Profile.
 *
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_Deinit( void ) ;

/** Link Up the Location And Navigation Profile with the specified remote 
 * Device
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_LinkUpLocationAndNavigationProfile()
 *  This function is used to link-Up the Location And Navigation Profile with
 *  the specified remote device.
 *  It will try to discover the Location And Navigation profile in the remote 
 *  device   using the given collector.
 * BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LINKED will be received in the 
 * callback with the status of the operation. 
 *
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Location And Navigation COLLECTOR to link-up
 *                  with a remote Location And Navigation SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *                  Once linked, this COLLECTOR could be then passed as   
 *                  parameter to the profile API in order to identify to which  
 *                  linked SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *          BLE LOCATION AND NAVIGATION  COLLECTOR and the remote BLE LOCATION 
 *          AND NAVIGATION SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *          complete link-up.
 * @param info: Only valid if mode is BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_RESTORE,
 *          and return status is BLESTATUS_SUCCESS.
 *          It reports the link-Up information ( sensor Features).
 *          It may be equal to (BleLocationAndNavigationCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *  - BLESTATUS_PENDING indicates that the operation succeeded, the 
 *          BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LINKED event will be received 
 *          in the callback registered during the call of
 *          BLELOCATIONANDNAVIGATION_COLLECTOR_Init. This status is returned only 
 *          if mode is BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_COMPLETE.
 *
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *      This status is returned only if mode is 
 *      BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_RESTORE. It means that the  
 *      persistent information has been restored and the devices are now linked.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *      is BLELOCATIONANDNAVIGATION_COLLECTOR_LINKUP_RESTORE, it means that 
 *      BLELOCATIONANDNAVIGATIONCOLLECTOR_SUPPORT_SAVINGINFORMATION is not 
 *      enabled or if enabled that the remote device is not bonded or the 
 *      persistent memory does not contains enough information about the remote 
 *      Location And Navigation SENSOR. 
 *      It may also indicate that the device is already Linked.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other Location And Navigation Operation is in Progress, wait for this
 *  operation completion
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_LinkUpLocationAndNavigationProfile(
                        BleLocationAndNavigationCollector *collector,
                        U16 connHandle,
                        BleLocationAndNavigationCollectorLinkMode linkUpMode,
                        BleLocationAndNavigationCollectorLinkUPInfo *info
                        );

/** Un Link the Location And Navigation Profile with the specified linked COLLECTOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_UnLinkLocationAndNavigationProfile()
 *  This function is used to UnLink the Location And Navigation Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *  Location And Navigation Sensor. 
 *  The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_LOCATIONANDNAVIGATION_* from persistent memory
 *
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Location And Navigation COLLECTOR 
 
 * @return The status of the operation:
 *
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded.
 *      the collector memory could be reused.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_UnLinkLocationAndNavigationProfile(
        BleLocationAndNavigationCollector *collector);

/** Configure the BLE LOCATION AND NAVIGATION SENSOR to deliver the Location And Speed 
 * when modified
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_SubscribeLocationAndSpeed()
 *  This function is used by the given linked LOCATION AND NAVIGATION COLLECTOR to 
 *  configure the remote LOCATION AND NAVIGATION SENSOR to deliver location and speed
 *  when it changes on the SERVER.
 *  When location and speed information is received the 
 *  BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEED event will be received in the 
 *  callback with the location and speed information.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *      the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDSUBSCRIPTION_RSP event 
 *      will be received in the callback with the status of the operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been 
 *      initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_SubscribeLocationAndSpeed(
                    BleLocationAndNavigationCollector *collector );

/** Configure the BLE LOCATION AND NAVIGATION SENSOR to not deliver the Location And Speed 
 * when modified
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_UnsubscribeLocationAndSpeed()
 *  This function is used by the given linked LOCATION AND NAVIGATION COLLECTOR to 
 *  configure the remote LOCATION AND NAVIGATION SENSOR to not deliver location and speed
 *  when it changes on the SERVER.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *      the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_LOCATIONANDSPEEDUNSUBSCRIPTION_RSP event 
 *      will be received in the callback with the status of the operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been 
 *      initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_UnsubscribeLocationAndSpeed(
                    BleLocationAndNavigationCollector *collector );

/** Configure the BLE LOCATION AND NAVIGATION SENSOR to deliver the Navigation
 * when modified
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_SubscribeNavigation()
 *  This function is used by the given linked LOCATION AND NAVIGATION COLLECTOR to 
 *  configure the remote LOCATION AND NAVIGATION SENSOR to deliver navigation
 *  when it changes on the SERVER.
 *  When navigation information is received the 
 *  BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATION event will be received in the 
 *  callback with the navigation information.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *      the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONSUBSCRIPTION_RSP event 
 *      will be received in the callback with the status of the operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been 
 *      initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_SubscribeNavigation(
                    BleLocationAndNavigationCollector *collector );

/** Configure the BLE LOCATION AND NAVIGATION SENSOR to not deliver the Navigation
 * when modified
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_UnsubscribeNavigation()
 *  This function is used by the given linked LOCATION AND NAVIGATION COLLECTOR to 
 *  configure the remote LOCATION AND NAVIGATION SENSOR to not deliver navigation
 *  when it changes on the SERVER.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *      the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONSUBSCRIPTION_RSP event 
 *      will be received in the callback with the status of the operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been 
 *      initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_UnsubscribeNavigation(
                    BleLocationAndNavigationCollector *collector );

/** Set a new cumulative (total) distance in remote linked SENSOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_SetCumulativeValue()
 *  This function is used to set a new cumulative (total) distance
 *   in remote linked SENSOR, if the remote sensor supports total distance
 *   feature.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 * @param value: The new value to set as cumulative value. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once 
 *      completed the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_TOTALDISTANCESET  
 *      event will be received in the callback with the status of the 
 *      operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked, or the remote sensor is
 *       not supporting the total distance feature
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_SetCumulativeValue( 
    BleLocationAndNavigationCollector   *collector,
    U32                                 value); 

/** Request number of routes stored in remote linked SENSOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNumberOfRoutes()
 *  This function is used to request the number of routes stored
 *   in remote linked SENSOR, if the remote sensor supports navigation
 *   feature.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once 
 *      completed the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NUMBEROFROUTES  
 *      event will be received in the callback with the status of the 
 *      operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked, or the remote sensor is
 *       not supporting the navigation feature
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *      an other LOCATION AND NAVIGATION profile Operation is in Progress, wait 
 *       for this operation completion 
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *   an invalid parameter (the LOCATION AND NAVIGATION profile seems
 *    not been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNumberOfRoutes( 
    BleLocationAndNavigationCollector *collector);

/** Request the name of a selected route in remote linked SENSOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNameOfRoute()
 *  This function is used to request the name of a selected route
 *   in remote linked SENSOR, if the remote sensor supports navigation
 *   feature.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 * @param desiredRoute: The selected route for which the name is requested
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once 
 *      completed the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAMEOFROUTE  
 *      event will be received in the callback with the status of the 
 *      operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked, or the remote sensor is
 *       not supporting the navigation feature
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *      an other LOCATION AND NAVIGATION profile Operation is in Progress, 
 *       wait for this operation completion 
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not
 *       been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_RequestNameOfRoute( 
    BleLocationAndNavigationCollector *collector,
    U16 desiredRoute);

/** Control navigation feature in remote linked SENSOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_ControlNavigation()
 *  This function is used to control navigation
 *   in remote linked SENSOR, if the remote sensor supports navigation
 *   feature.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 * @param navigationProcedure: The control procedure of navigation
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once 
 *      completed the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_NAVIGATIONREQUESTPROCESSED 
 *      event will be received in the callback with the status of the 
 *      operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked, or the remote sensor is
 *       not supporting the navigation feature
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *      an other LOCATION AND NAVIGATION profile Operation is in Progress,
 *      wait for this operation completion 
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not 
 *      been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_ControlNavigation( 
    BleLocationAndNavigationCollector *collector,
    BleLocationAndNavigationCollectorControlNavigation navigationProcedure);

/** Select the route in remote linked SENSOR
 *
 * BLELOCATIONANDNAVIGATION_COLLECTOR_SelectRoute()
 *  This function is used to select a route
 *   in remote linked SENSOR, if the remote sensor supports navigation
 *   feature.
 *   
 * BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked LOCATION AND NAVIGATION COLLECTOR. 
 * @param navigationProcedure: The selected route you want to navigate
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once 
 *      completed the BLELOCATIONANDNAVIGATIONCOLLECTOR_EVENT_ROUTESELECTED 
 *      event will be received in the callback with the status of the 
 *      operation.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked, or the remote sensor is
 *       not supporting the navigation feature
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *  an other LOCATION AND NAVIGATION profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (the LOCATION AND NAVIGATION profile seems not been initialized).
 *
 * @author Mahault ANDRIA
 */
BleStatus BLELOCATIONANDNAVIGATION_COLLECTOR_SelectRoute( 
    BleLocationAndNavigationCollector *collector,
    U16                                selectedRoute);

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLELOCATIONANDNAVIGATION_SUPPORT_COLLECTOR== 1)

#endif /*__BLELOCATIONANDNAVIGATION_COLLECTOR_H*/
