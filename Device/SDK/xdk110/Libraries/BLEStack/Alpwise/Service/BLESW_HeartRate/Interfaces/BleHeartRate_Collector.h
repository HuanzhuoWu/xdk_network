#ifndef __BLEHEARTRATE_COLLECTOR_H
#define __BLEHEARTRATE_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleHeartRate_Collector.h
 *
 * Description:   Contains interfaces definition for Heart Rate 
 *					Profile when the local device operates in the COLLECTOR
 *					role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "BleEngine.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEHEARTRATE_SUPPORT_COLLECTOR
 * Defines if the BLE HEART RATE Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE HEART 
 * RATE Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEHEARTRATE_SUPPORT_COLLECTOR
#define BLEHEARTRATE_SUPPORT_COLLECTOR							0
#endif //BLEHEARTRATE_SUPPORT_COLLECTOR

#if (BLEHEARTRATE_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when HEART RATE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when HEART RATE Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE HEART RATE Profile implementation in 
 * COLLECTOR role supports saving the HEART RATE SENSOR information when both
 * devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the HEART RATE SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and Heart Rate 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * it saves 7 bytes into persistent memory
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_HEARTRATE_REMOTEFEATURES				0x90	//1 byte
#define BLEINFOTYPE_HEARTRATE_HEARTRATEMEASUREMENTHANDLE	0x91	//2 bytes
#define BLEINFOTYPE_HEARTRATE_SENSORLOCATIONHANDLE			0x92	//2 bytes
#define BLEINFOTYPE_HEARTRATE_CONTROLPOINTHANDLE			0x93	//2 bytes


#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/

/** 
 * BleHeartRateCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleHeartRateCollectorCallBack
 */
typedef	U8	BleHeartRateCollectorEvent;

/** BLEHEARTRATECOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleHeartRateCollectorLinkUPInfo pointer.
 */
#define BLEHEARTRATECOLLECTOR_EVENT_LINKED						0xE0

/** BLEHEARTRATECOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the status
 * of the operation.
 * The parms field indicates the link up information within a
 * BleHeartRateCollectorLinkUPInfo pointer.
 */
#define BLEHEARTRATECOLLECTOR_EVENT_UNLINKED					0xE1

/** BLEHEARTRATECOLLECTOR_EVENT_HEARTRATEMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a heart Rate
 * measurement from the SENSOR.
 * It follows an alert from the SENSOR.
 * The status field is always BLESTATUS_SUCCESS
 * The parms field indicates the Heart Rate information within the 
 * BleHeartRateCollectorMeasurementInfo type.
 */
#define BLEHEARTRATECOLLECTOR_EVENT_HEARTRATEMEASUREMENT		0xE2

/** BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION
 * Event received to indicate that this COLLECTOR has received a heart Rate
 * SENSOR body location from the SENSOR.
 * It follows the call of BLEHEARTRATE_COLLECTOR_GetSensorBodyLocation().
 * The parms field indicates the Sensor Body Location information within the 
 * BleHeartRateCollectorSensorLocationInfo type.
 */
#define BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION			0xE3

/** BLEHEARTRATECOLLECTOR_EVENT_ENERGYEXPENDEDRESET
 * Event received to indicate that this COLLECTOR has complete to request
 * the remote sensor to reset its energy expended count.
 * It follows the call of 
 * BLEHEARTRATE_COLLECTOR_ResetSensorEnergyExpendedCount().
 * The status field indicate the status of the operation
 * The parms field is null (void *)0 and not applicable.
 */
#define BLEHEARTRATECOLLECTOR_EVENT_ENERGYEXPENDEDRESET			0xE4



/**
 * BleHeartRateCollectorCallBack
 *	This callback receives the BLE HEART RATE COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleHeartRateCollectorCallBack)(
	BleHeartRateCollectorEvent	event,
	BleStatus					status,
	void					   *parms); 

/** 
 * BleHeartRateCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLEHEARTRATE_COLLECTOR_LinkUpHeartRateProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEHEARTRATE_COLLECTOR_LinkUpHeartRateProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLEHEARTRATECOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Heart Rate SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								heartRateServiceStartingHandle;
	AttHandle								heartRateServiceEndingHandle;

	AttHandle								heartRateMeasurementValueHandle;
	AttHandle								heartRateMeasurementConfigHandle;
	U8										value[2];

	AttHandle								heartRateBodySensorLocationHandle;
	AttHandle								heartRateControlPointHandle;
	U8										heartRateSensorFeatures;

} BleHeartRateCollector;

/** 
 * BleHeartRateCollectorSensorFeature type
 * Bitfield defining the remote heart rate SENSOR capabilities
 * it defines after the linkUP has completed, what are the 
 * Heart Rate specification optional defined features supported by
 * the remote device.
 */
typedef U8 BleHeartRateCollectorSensorFeature;

#define BLEHEARTRATECOLLECTOR_SENSORSUPPORT_BODY_LOCATION	0x01
#define BLEHEARTRATECOLLECTOR_SENSORSUPPORT_CONTROL_POINT	0x02 


/** 
 *BleHeartRateCollectorSensorBodyLocation type
 * Defines the possible value describing the remote Heart Rate SENSOR
 * location.
 * The Body Sensor Location of the device is used to describe the intended 
 *  location of the heart rate measurement for the device.  
 * It is included in the BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION
 * event BleHeartRateCollectorSensorLocationInfo parameter 
 */
typedef U8 BleHeartRateCollectorSensorBodyLocation;

#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_OTHER							0
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_CHEST							1
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_WRIST							2
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_FINGER						3
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_HAND							4
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_EARLOBE						5
#define BLEHEARTRATECOLLECTOR_SENSOR_LOCATION_FOOT							6

/** 
 * BleHeartRateCollectorSensorContactStatus type
 * The Sensor Contact Status bits indicate whether or not, 
 * the Sensor Contact feature is supported by the remote SENSOR and if 
 * supported whether or not skin contact is detected. 
 * This information is returned for each measurement received during the
 * BLEHEARTRATECOLLECTOR_EVENT_HEARTRATEMEASUREMENT event.
 */
typedef U8 BleHeartRateCollectorSensorContactStatus;
#define BLEHEARTRATECOLLECTOR_SENSOR_CONTACT_STATUS_NOTSUPPORTED			0
#define BLEHEARTRATECOLLECTOR_SENSOR_CONTACT_STATUS_DETECTED				1
#define BLEHEARTRATECOLLECTOR_SENSOR_CONTACT_STATUS_NOT_DETECTED			2

/** 
 * BleHeartRateCollectorEnergyExpendedSupport type
 * Defines if the energy expended value is included in the heart Rate 
 * mesurement (BLEHEARTRATECOLLECTOR_ENERGY_EXPENDED_INCLUDED) or not 
 * (BLEHEARTRATECOLLECTOR_ENERGY_EXPENDED_NOT_INCLUDED)
 * This information is returned for each measurement received during the
 * BLEHEARTRATECOLLECTOR_EVENT_HEARTRATEMEASUREMENT event.
 */
typedef U8 BleHeartRateCollectorEnergyExpendedSupport;
#define BLEHEARTRATECOLLECTOR_ENERGY_EXPENDED_NOT_INCLUDED					0
#define BLEHEARTRATECOLLECTOR_ENERGY_EXPENDED_INCLUDED						1


/** 
 * BleHeartRateCollectorLinkUPInfo type
 * structure passed in parameter in the BleHeartRateCollectorCallBack
 * parms fields during the BLEHEARTRATECOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleHeartRateCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * remoteSensorFeatures : The supported features of the remote sensor
 */
typedef struct {

	BleHeartRateCollector					*bleHeartRateCollector;
	U16										connHandle;
	BleHeartRateCollectorSensorFeature		remoteSensorFeatures;

} BleHeartRateCollectorLinkUPInfo;


/** 
 * BleHeartRateCollectorSensorLocationInfo type
 * structure passed in parameter in the BleHeartRateCollectorCallBack
 * parms fields during the BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION event.
 * It indicates :
 * bleHeartRateCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the body sensor location
 *		have been received
 * bodyLocation : The body sensor location, only applicable if the callback
 *		status is BLESTATUS_SUCCESS
 * 
 */
typedef struct {

	BleHeartRateCollector					*bleHeartRateCollector;
	U16										connHandle;
	BleHeartRateCollectorSensorBodyLocation	bodyLocation;

} BleHeartRateCollectorSensorLocationInfo;

/** 
 * BleHeartRateCollectorHeartRateMeasurementInfo type
 * structure passed in parameter in the BleHeartRateCollectorCallBack
 * parms fields during the BLEHEARTRATECOLLECTOR_EVENT_HEARTRATEMEASUREMENT event.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleHeartRateCollector						*bleHeartRateCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16											connHandle;
	/*
	 * the heart Rate measurement Value, in beats per minute (bpm)
	 */
	U16											heartRateValue;

	/*
	 * define the current status of the sensor contact, one of the 
	 * BLEHEARTRATE_SENSOR_CONTACT_STATUS available value.
	 */
	BleHeartRateCollectorSensorContactStatus	sensorContactStatus;

	/* Define if the energy expended is included or not in to the 
	 * measurement.
	 * If energy expended is not included in the current measurement the value 
	 * is set to BLEHEARTRATE_COLLECTOR_ENERGY_EXPENDED_NOT_INCLUDED,
	 * the energyExpendedValue field is not valid and does not contain  
	 * a valid energy Expended Value, it must be ignored
	 * If energy expended is included in the current measurement the value 
	 * is set to BLEHEARTRATE_COLLECTOR_ENERGY_EXPENDED_INCLUDED,
	 * the energyExpendedValue is valid and contains a valid energy Expended 
	 * Value
	 */
	BleHeartRateCollectorEnergyExpendedSupport	isEnergyExpendedIncluded;

	/*
	* The Energy Expended field represents the accumulated energy expended 
	* in kilo Joules since the last time it was reset. 
	*
	* If energy expended is used, it is typically only included in the heart
	* Rate Measurement characteristic once every 10 measurements at a regular
	* interval.
	*
	* Since Energy Expended is a UINT16, the highest value that can be 
	* represented is 65535 kilo Joules. If the maximum value of 65535 kilo
	* Joules is attained (0xFFFF), the field value should remain at 0xFFFF
	* so that the client can be made aware that a reset of the Energy
	* Expended Field is required.
	*
	*/
	U16											energyExpendedValue;						

	/*
	 * Define the number of RR interval included in this measurement,
	 * It can be zero, if the remote SENSOR is only interested to send the 
	 * current heart Rate measurement Value beats per minutes.
	 */
	U8											numberOfRRIntervals;

	/*
	 * A pointer to an Array of RRIntervals, the number of valid RRIntervals 
	 * is numberOfRRIntervals.
	 * The RR interval unit is 1/1024 second.
	 * This field must be ignored and is valid when numberOfRRIntervals is 0
	 */
	U16											*RRIntervals;

} BleHeartRateCollectorHeartRateMeasurementInfo;


/** 
 * BleHeartRateCollectorLinkMode type
 * Type passed during the 
 * BLEHEARTRATE_COLLECTOR_LinkUpHeartRateProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEHEARTRATE_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLEHEARTRATE_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleHeartRateCollectorLinkMode;

#define BLEHEARTRATE_COLLECTOR_LINKUP_COMPLETE			1
#define BLEHEARTRATE_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Heart Rate profile 
 *
 * BLEHEARTRATE_COLLECTOR_Init()
 *	This function is the entry point of a Heart Rate profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleHeartRateCollectorCallBack in which the collector 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_Init(
			BleHeartRateCollectorCallBack callback);

/** De-Init the Collector Role of the Heart Rate Profile.
 *
 * BLEHEARTRATE_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Heart Rate Profile.
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_Deinit( void ) ;

/** Link Up the Heart Rate Profile with the specified remote Device
 *
 * BLEHEARTRATE_COLLECTOR_LinkUpHeartRateProfile()
 *	This function is used to link-Up the Heart Rate Profile with the 
 *  specified remote device.
 *  It will try to discover the Heart Rate profile in the remote device 
 *  using the given collector.
 * BLEHEARTRATECOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Heart Rate COLLECTOR to link-up
 *					with a remote Heart Rate SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to the profile API in order to identify to which linked 
 *                  SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE HEART RATE COLLECTOR and the remote BLE HEART RATE  
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLEHEARTRATE_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleHeartRateCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *			BLEHEARTRATECOLLECTOR_EVENT_LINKED event will be received in the
 *			callback registered during the call of
 *			BLEHEARTRATE_COLLECTOR_Init. This status is returned only if mode
 *			is BLEHEARTRATE_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEHEARTRATE_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEHEARTRATE_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLEHEARTRATECOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Heart Rate SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Heart Rate Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_LinkUpHeartRateProfile(
						BleHeartRateCollector *collector,
						U16 connHandle,
						BleHeartRateCollectorLinkMode linkUpMode,
						BleHeartRateCollectorLinkUPInfo *info
						);

/** Un Link the Heart Rate Profile with the specified linked COLLECTOR
 *
 * BLEHEARTRATE_COLLECTOR_UnLinkHeartRateProfile()
 *	This function is used to UnLink the Heart Rate Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Heart Rate Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_HEARTRATE_* from persistent memory
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Heart Rate COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_UnLinkHeartRateProfile(
		BleHeartRateCollector *collector);

/** Get the remote SENSOR body location
 *
 * BLEHEARTRATE_COLLECTOR_GetSensorBodyLocation()
 *	This function is used to retrieve the remote SENSOR body location
 *  of a linked / connected Heart Rate SENSOR.
 *  Once completed successfully, the 
 * BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION event will be received 
 * in the collector callback, with the body sensor location.
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Heart Rate COLLECTOR 
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once 
 *		completed the BLEHEARTRATECOLLECTOR_EVENT_SENSORBODYLOCATION event  
 *		will be received in the callback with the body sensor location 
 *      Information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked 
 *      SENSOR does not support the body sensor location characteristic.
 *      Note that the remote sensor supported features are returned during the
 *		BLEHEARTRATECOLLECTOR_EVENT_LINKED event.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *		an other HEART RATE Profile Operation is in Progress, wait for this
 *		operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		of an invalid parameter (the HEART RATE Profile seems not to have been 
 *      initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_GetSensorBodyLocation(
		BleHeartRateCollector *collector);


/** Request to reset the remote Energy Expended Count
 *
 * BLEHEARTRATE_COLLECTOR_ResetSensorEnergyExpendedCount()
 *	This function is used to Request the remote linked / connected Heart
 * Rate SENSOR to reset its energy expended count.
 *
 *  Once completed successfully, the 
 * BLEHEARTRATECOLLECTOR_EVENT_ENERGYEXPENDEDRESET event will be received 
 * in the collector callback, with the status of the operation.
 *
 * BLEHEARTRATE_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Heart Rate COLLECTOR 
 *
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once 
 *		completed the BLEHEARTRATECOLLECTOR_EVENT_ENERGYEXPENDEDRESET event  
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked 
 *      SENSOR does not support the control point characteristic.
 *      Note that the remote sensor supported features are returned during the
 *		BLEHEARTRATECOLLECTOR_EVENT_LINKED event.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *		an other HEART RATE Profile Operation is in Progress, wait for this
 *		operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the HEART RATE Profile seems not to have been 
 *      initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEHEARTRATE_COLLECTOR_ResetSensorEnergyExpendedCount(
	BleHeartRateCollector *collector);

/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLEHEARTRATE_SUPPORT_COLLECTOR== 1)

#endif /*__BLEHEARTRATE_COLLECTOR_H*/
