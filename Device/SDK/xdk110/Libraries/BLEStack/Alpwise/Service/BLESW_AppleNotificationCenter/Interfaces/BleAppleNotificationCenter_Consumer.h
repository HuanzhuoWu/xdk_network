#ifndef __BLEAPPLENOTIFICATIONCENTER_CONSUMER_H
#define __BLEAPPLENOTIFICATIONCENTER_CONSUMER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleAppleNotificationCenter_Consumer.h
 *
 * Description:   Contains interfaces definition for Apple Notification 
 *                 Center Profile when the local device operates in 
 *                 the CONSUMER role.
 * 
 * Created:       December, 2103
 * Version:       0.1
 *
 * File Revision: $Rev: 2825 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************
 *  Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER
 * Defines if the BLE AppleNotificationCenter Profile implementation 
 * supports the CONSUMER Role.
 * If enabled ( set to 1 ) it enables all the following 
 * BleAppleNotificationCenter Consumer configuration and 
 * Application Interface.
 * The default value for this option is disabled (0). 
 */
#ifndef BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER
#define BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER                     0
#endif //BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER

#if (BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when AppleNotificationCenter CONSUMER Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when AppleNotificationCenter CONSUMER Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when AppleNotificationCenter CONSUMER Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when AppleNotificationCenter CONSUMER Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when HEALTH THERMOMETER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

// GATT notifiable required
#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when APPLE NOTIFICATION CENTER Service CONSUMER Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)




#if (BLE_SECURITY == 1 )
/**
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION 
 * Defines if the BLEAPPLENOTIFICATIONCENTER Profile CONSUMER supports 
 * saving the PROVIDER information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the CONSUMER saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the PROVIDER information.
 * Upon reconnection with the PROVIDER, as these information are already
 * known then the linkUp process can be simplified.
 * There is 6 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION    
#define BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION            0
#endif //BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_APPLENOTIFICATIONCENTER_NOTIFICATIONSOURCEHANDLE    0x9C    //2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported force 
// the BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION
#define BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION            0
#endif //( BLE_SECURITY == 0) 

/***************************************************************************\
 *  Type definition
\***************************************************************************/

/** 
 * BleAppleNotificationCenterEvent type
 * Define the different kind of events that could be received by the 
 * BleAppleNotificationCenterCallBack
 */
typedef U8  BleAppleNotificationCenterConsumerEvent;

/** BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleAppleNotificationCenterConsumerLinkUpInfo pointer.
 */
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_LINKED                 0xD9

/** BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleAppleNotificationCenterConsumerLinkUpInfo pointer.
 */
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_UNLINKED               0xDA

/** BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_NOTIFICATIONSOURCE
 * Event received to indicate that this NOTIFICATION CONSUMER has 
 * received an Apple Notification Center Service PROVIDER notification
 * source from the NOTIFICATION PROVIDER.
 * The status field indicates the status of the operation.
 * The parms field indicates the Apple Notification Center information
 * within the BleAppleNotificationCenterConsumerNotification type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleAppleNotificationCenterConsumer fields are consistent. Other fields
 *   of BleAppleNotificationCenterConsumerNotification must be ignored
 * - If status is BLESTATUS_SUCCESS all fields contains consistent
 *   information.
 */
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_NOTIFICATIONSOURCE     0xDB

/**
 * BleAppleNotificationCenterConsumerCallBack
 *  This callback receives the BLE APPLE NOTIFICATION CENTER CONSUMER 
 *  profile events. Each of these events can be associated with a defined
 *  status and parameter. The callback is executed during the stack context,
 *  be careful to not doing heavy process in this function.
 */
typedef void (*BleAppleNotificationCenterConsumerCallBack)(
                                BleAppleNotificationCenterConsumerEvent event,
                                BleStatus status,
                                void* parms); 

/**
 * BleAppleNotificationCenterConsumerNotificationEventID type
 * This field informs the accessory whether the given iOS notification 
 * was added, modified, or removed.
 */
typedef U8 BleAppleNotificationCenterConsumerNotificationEventID;
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENTID_NOTIFICATIONADDED    0
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENTID_NOTIFICATIONMODIFIED 1
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENTID_NOTIFICATIONREMOVED  2

/**
 * BleAppleNotificationCenterConsumerNotificationEventFlags type
 * A bitmask whose set bits inform an Notification Consumer of specificities
 * with the iOS notification. For example, if an iOS notification is 
 * considered �important�, the Notification Center may want to display a more
 * aggressive user interface (UI) to make sure the user is properly alerted.
 */
typedef U8 BleAppleNotificationCenterConsumerNotificationEventFlags;
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENTFLAGS_SILENT            1
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENTFLAGS_IMPORTANT         2

/**
 * BleAppleNotificationCenterConsumerNotificationCategoryID type
 * A numerical value providing a category in which the iOS 
 * notification can be classified. The Notification Provider will make a best 
 * effort to provide an accurate category for each iOS notification.
 */
typedef U8 BleAppleNotificationCenterConsumerNotificationCategoryID;
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_OTHER              0
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_INCOMINGCALL       1
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_MISSEDCALL         2
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_VOICEMAIL          3
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_SOCIAL             4
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_SCHEDULE           5
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_EMAIL              6
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_NEWS               7
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_HEALTHANDFITNESS   8
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_BUSINESSANDFINANCE 9
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_LOCATION           10
#define BLEAPPLENOTIFICATIONCENTERCONSUMER_CATEGORYID_ENTERTAINMENT      11

/**
 * BleAppleNotificationCenterConsumerNotificationCategoryCount type
 * The current number of active iOS notifications in the given category.
 * For example, if two unread emails are sitting in a user�s email
 * inbox, and a new email is pushed to the user�s iOS device, the
 * value of CategoryCount is 3.
 */
typedef U8 BleAppleNotificationCenterConsumerNotificationCategoryCount;

/**
 * BleAppleNotificationCenterConsumerNotificationUID type
 * A 32-bit numerical value that is the unique identifier (UID) for 
 * the iOS notification. This value can be used as a handle in commands
 * sent to the Control Point characteristic to retrieve more
 * information about the iOS notification
 */
typedef U32 BleAppleNotificationCenterConsumerNotificationUID;


/** 
 * BleAppleNotificationCenterConsumer type
 * Define a local CONSUMER, it represent a memory block that should be passed 
 * by the application during the 
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_LinkUpAppleNotificationCenterProfile() 
 * API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_LinkUpAppleNotificationCenterProfile()
 * API ,and shall be valid while the Profile and the remote device are linked
 * together. 
 * So, this memory block could be reused by the application once this CONSUMER
 * receive the BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this CONSUMER memory block while linked
 * with a remote PROVIDER. It is used internally by the profile and shall 
 * not be accessed by the application.
 * A pointer to this local CONSUMER could be then passed as parameter to 
 * profile API in order to identify to which linked PROVIDER the operation  
 * is destinated.
 *
 * A CONSUMER is unique per remote AppleNotificationCenter PROVIDER, it means 
 * that if this local device is connected to multiple remote PROVIDER
 * the application shall register multiple CONSUMERS.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
    /** 
     *  The Following fields are for INTERNAL USE ONLY
     * Do not address them directly
     */
    BleGattCommand          gattCommand;

    U16                     connHandle;
    U8                      linked;
    U8                      state;
    U8                      targetFeatures;

    AttHandle               ancServiceStartingHandle;
    AttHandle               ancServiceEndingHandle;

    AttHandle               notificationSourceCharacteristicHandle;
    AttHandle               notificationSourceCharacteristicConfigHandle;

    U8                      configValue[2];
} BleAppleNotificationCenterConsumer;


/** 
 * BleAppleNotificationCenterConsumerLinkUpInfo type
 * Structure passed in parameter in the 
 * BleAppleNotificationCenterConsumerCallBack parms fields during the
 * BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_LINKED and
 * BLEAPPLENOTIFICATIONCENTERCONSUMER_EVENT_UNLINKED events.
 * It indicates :
 * BleAppleNotificationCenterConsumer : The consumer from which the 
 *  event is comming;
 * connHandle : The connection handle from which the Link Up information
 *  is coming.
 */
typedef struct {

    BleAppleNotificationCenterConsumer  *bleAppleNotificationCenterConsumer;
    U16                                 connHandle;

} BleAppleNotificationCenterConsumerLinkUpInfo;

/** 
 * BleAppleNotificationCenterConsumerLinkMode type
 * Type passed during the 
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_LinkUpAppleNotificationCenterProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_COMPLETE) with
 * remote service and characteritic discovery, or if the linkUp information
 * are restored (BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_RESTORE) from 
 * previous complete linkUP from persistent memory.
 * Note that if BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION is 
 * DISABLED, a linkUP with the 
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_RESTORE mode 
 * will always failed.
 */
typedef U8 BleAppleNotificationCenterConsumerLinkMode;

#define BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_COMPLETE             1
#define BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_RESTORE              2





/**
 * BleAppleNotificationCenterConsumerNotification type
 *
 * The Notification Source characteristic is the characteristic
 * upon which an Notification Consumer is informed of:
 * - the arrival of a new iOS notification on the Notification Provider
 * - the modification of an iOS notification on the Notification Provider
 * - the removal of an iOS notification on the Notification Provider
 */
typedef struct {
    /*
     * The link with the remote Sensor for which this 
     * event has been received.
     */ 
    BleAppleNotificationCenterConsumer                              
                                        *bleAppleNotificationCenterConsumer;

    /*
     * The connection handle from which the measurement information
     * is coming.
     */
    U16                                 connHandle;

    /*
     * The event id.
     */
    BleAppleNotificationCenterConsumerNotificationEventID       
                                        eventId;

    /*
     * The event flags.
     */
    BleAppleNotificationCenterConsumerNotificationEventFlags    
                                        eventFlags;

    /*
     * The category id.
     */
    BleAppleNotificationCenterConsumerNotificationCategoryID    
                                        categoryID;

    /*
     * The category count.
     */
    BleAppleNotificationCenterConsumerNotificationCategoryCount 
                                        categoryCount;

    /*
     * The notification UID.
     */
    BleAppleNotificationCenterConsumerNotificationUID       
                                        notificationUID;
} BleAppleNotificationCenterConsumerNotification;

/****************************************************************************\
 *  APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Init the AppleNotificationCenter profile
 *
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_Init()
 *  This function is the entry point of the AppleNotificationCenter profile ,
 *  it inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *  Initialization.
 *
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER shall be enabled
 *
 * @param callback: The BleAppleNotificationCenterConsumerCallBack in which
 *                  the consumer events will be received.
 *
 * @return The status of the operation:
 *  - BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed.
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (Profile is already initialized).
 *
 * @author Samuel DE SANTIS
 */
BleStatus BLEAPPLENOTIFICATIONCENTER_CONSUMER_Init(
                        BleAppleNotificationCenterConsumerCallBack callback);

/** De-Init the Apple Notification Center CONSUMER Profile
 *
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_Deinit
 *  This function is used to De-Init the Apple Notification Center 
 *  CONSUMER Profile.
 *
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER shall be enabled
 *
 * @return The status of the operation:
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Samuel DE SANTIS
 */
BleStatus BLEAPPLENOTIFICATIONCENTER_CONSUMER_Deinit( void );


/** Link Up the AppleNotificationCenter Profile with the specified 
 *  remote Device
 *
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_LinkUpAppleNotificationCenterProfile()
 *  This function is used to link-Up the AppleNotificationCenter profile with
 *  the specified remote device.
 *  It will try to discover the AppleNotificationCenter Profile in the remote
 *  device using the given CONSUMER.
 *
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER shall be enabled
 *
 * @param bleAppleNotificationCenterConsumer: A valid 
 *                  BleAppleNotificationCenterConsumer CONSUMER to link-up
 *                  with a remote AppleNotificationCenter PROVIDER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *                  Once linked, this CONSUMER could be then passed as
 *                  parameter to profile API in order to identify to which
 *                  linked PROVIDER the operation is destinated.
 * @param connHandle: The connection Handle on which the link up between this
 *       bleAppleNotificationCenterConsumer and the remote
 *       AppleNotificationCenter PROVIDER profile shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *      complete link-up.
 *
 * @return The status of the operation:
 *  - BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *      the BLEAPPLENOTIFICATIONCENTER_EVENT_LINKED event will be received in
 *      the callback with the status of the operation. This status is returned
 *      only if mode is BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_COMPLETE.
 *
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded and is
 *      completed. This status is returned only if mode is 
 *      BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_RESTORE. It means that the
 *      persistent information has been restored and the devices are
 *      now linked.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. In case of
 *      mode is BLEAPPLENOTIFICATIONCENTER_CONSUMER_LINKUP_RESTORE, it means
 *      that BLEAPPLENOTIFICATIONCENTER_SUPPORT_SAVINGINFORMATION is not
 *      enabled or if enabled that the remote device is not bonded or the
 *      persistent memory does not contains enough information about the
 *      remote AppleNotificationCenter PROVIDER.
 *
 *  - BLESTATUS_BUSY indicates that the operation has failed, because 
 *      an other AppleNotificationCenter Profile Operation is in Progress,
 *      wait for this operation completion.
 *
 *  if BLE_PARMS_CHECK is set to 1:
 *  - BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *      an invalid parameter (connHandle does not match with a specified link)
 *
 * @author Samuel DE SANTIS
 */
BleStatus BLEAPPLENOTIFICATIONCENTER_CONSUMER_LinkUpAppleNotificationCenterProfile(
    BleAppleNotificationCenterConsumer *bleAppleNotificationCenterConsumer,
    U16 connHandle,
    BleAppleNotificationCenterConsumerLinkMode mode);

/** Un Link the AppleNotificationCenter Profile with the specified
 *  linked CONSUMER
 *
 * BLEAPPLENOTIFICATIONCENTER_CONSUMER_UnLinkAppleNotificationCenterProfile()
 *  This function is used to UnLink the AppleNotificationCenter Profile with
 *  the specified previously linked CONSUMER.
 *  This CONSUMER will not receive any more information from the linked 
 *  PROVIDER. 
 *  The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_APPLENOTIFICATIONCENTER_* from persistent memory
 *
 * BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER shall be enabled
 *
 * @param bleAppleNotificationCenterConsumer: A linked AppleNotificationCenter
 *                                            CONSUMER 
 *
 * @return The status of the operation:
 *
 *  - BLESTATUS_SUCCESS indicates that the operation succeeded.
 *      the consumer memory could be reused.
 *
 *  - BLESTATUS_FAILED indicates that the operation has failed. 
 *      It could be because the device is not linked.
 *
 * @author Samuel DE SANTIS
 */
BleStatus BLEAPPLENOTIFICATIONCENTER_CONSUMER_UnLinkAppleNotificationCenterProfile(
        BleAppleNotificationCenterConsumer *bleAppleNotificationCenterConsumer);

#endif //(BLEAPPLENOTIFICATIONCENTER_SUPPORT_CONSUMER== 1)

#endif //__BLEAPPLENOTIFICATIONCENTER_CONSUMER_H
