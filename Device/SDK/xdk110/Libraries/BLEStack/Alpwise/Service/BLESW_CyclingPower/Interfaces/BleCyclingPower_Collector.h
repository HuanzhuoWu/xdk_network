#ifndef __BLECYCLINGPOWER_COLLECTOR_H
#define __BLECYCLINGPOWER_COLLECTOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleCyclingPower_Collector.h
 *
 * Description:   Contains interfaces definition for Cycling Power Profile
 * when the local device operates in the COLLECTOR role.
 * 
 * Created:       August, 2013
 * Version:		  0.1
 *
 * File Revision: $Rev: 2740 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR
 * Defines if the BLE CYCLING POWER Profile implementation supports the 
 * COLLECTOR Role
 * If enabled ( set to 1 ) it enables all the following BLE CYCLING POWER 
 * Profile COLLECTOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLECYCLINGPOWER_SUPPORT_COLLECTOR
#define BLECYCLINGPOWER_SUPPORT_COLLECTOR							0
#endif //BLECYCLINGPOWER_SUPPORT_COLLECTOR

#if (BLECYCLINGPOWER_SUPPORT_COLLECTOR == 1)

/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when CYCLING POWER Profile COLLECTOR Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLE CYCLING POWER Profile implementation in 
 * COLLECTOR role supports saving the CYCLING POWER SENSOR information when 
 * both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the COLLECTOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SENSOR information.
 * Upon reconnection with the CYCLING POWER SENSOR, as this information is  
 * already known then the linkUp process can be simplified, and CYCLINGPOWER 
 * measurement Notification could be automaticaly understood by this 
 * collector.
 * it saves 5 bytes into persistent memory
 *
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled.
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION	
#define BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION


#define BLEINFOTYPE_CYCLINGPOWER_REMOTEFEATURES					    0xBA	//4 bytes

#define BLEINFOTYPE_CYCLINGPOWER_CYCLINGPOWERMEASUREMENTHANDLE	    0xBB	//2 bytes
#define BLEINFOTYPE_CYCLINGPOWER_CYCLINGPOWERCONTROLPOINTHANDLE 	0xBC	//2 bytes
#define BLEINFOTYPE_CYCLINGPOWER_CYCLINGPOWERSENSORLOCATIONHANDLE	0xBD    //1 byte

#endif //(BLE_SECURITY == 1 )

// Otherwise if BLE_SECURITY is not supported and 
// BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION is not supported force 
// the BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION
#define BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 


/***************************************************************************\
 * CONSTANTS Definition
\***************************************************************************/


/***************************************************************************\
 *	TYPES definition
\***************************************************************************/
/** 
 *BleCyclingPowerCollectorFeatureBitfield type
 * Bitfield defining the remote Cycling Power SENSOR capabilities.
 * It defines after the linkUP has completed, what are the 
 * Cycling Power specification optional defined features supported by
 * the remote device.
 */
typedef	U32	BleCyclingPowerCollectorFeatureBitfield;
// Remote SENSOR supports the "Pedal Power balance" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_PEDALPOWERBALANCESUPPORTED									(0x00000001u)
// Remote SENSOR supports the "Accumulated Torque" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDTORQUESUPPORTED									(0x00000002u)
// Remote SENSOR supports the "Wheel revolution data" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED								(0x00000004u)
// Remote SENSOR supports the "Crank Revolution data" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED								(0x00000008u)
// Remote SENSOR supports the "Extreme Magnitudes" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED									(0x00000010u)
// Remote SENSOR supports the "Extreme Angles" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEANGLESSUPPORTED										(0x00000020u)
// Remote SENSOR supports the "" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_TNBDEADANGLESSUPPORTED										(0x00000040u)
// Remote SENSOR supports the "Accumulated Energy" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDENERGYSUPPORTED									(0x00000080u)
// Remote SENSOR supports the "Offset Compensation Indicator" feature
//#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_OFFSETCOMPENSATIONSINDICATORUPPORTED					    (0x00000100u)
// Remote SENSOR supports the "Offset Compensation" feature
//#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_OFFSETCOMPENSATIONSSUPPORTED						    	(0x00000200u)
// Remote SENSOR supports the "CP measurement characteristic masking" feature
//#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CPMEASUREMENTCHARACTERISTICMASKINGSUPPORTED				(0x00000400u)
// Remote SENSOR supports the "Multiple sensor locations" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_MULTIPLESENSORLOCATIONSSUPPORTED							(0x00000800u)
// Remote SENSOR supports the "Crank Length Adjustment" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CRANKLENGTHADJUSTMENTSUPPORTED	        					(0x00001000u)
// Remote SENSOR supports the "Chain length adjusment" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CHAINLENGTHADJUSTMENTSUPPORTED							(0x00002000u)
// Remote SENSOR supports the "Chain Weight adjusment" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CHAINWEIGHTADJUSTMENTSUPPORTED							(0x00004000u)
// Remote SENSOR supports the "Span Length Adjustment" feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SPANLENGTHADJUSTMENTSUPPORTED							    (0x00008000u)
// Remote SENSOR supports the "Sensor Measurement " feature
#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT		   					    	(0x00010000u)
// Remote SENSOR supports the "Instantaneous Measurement Direction" feature	
//#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_INSTANTANEOUSMEASUREMENTDIRECTIONSUPPORTED			    (0x020000u)
// Remote SENSOR supports the "Factory Calibration" feature
//#define BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_FACTORYCALIBRATIONDATESUPPORTED	

/** 
 *BleCyclingPowerCollectorSensorLocationBitfield type
 * Bitfield defining the remote Cycling Power SENSOR sensor location.
 * It defines after the linkUP has completed, what are the 
 * Cycling Power specification optional defined supported sensor location by
 * the remote device.
 */
typedef U8  BleCyclingPowerCollectorSensorLocation;

#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_OTHER						          0
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_TOPOFSHOE					          1
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_INSHOE						          2
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_HIP							          3
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_FRONTWHEEL					          4
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_LEFTCRANK					          5
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_RIGHTCRANK					          6
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_LEFTPEDAL					          7
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_RIGHTPEDAL					          8
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_FRONTHUB					          9
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_REARDROPOUT					         10
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_CHAINSTAY					         11
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_REARWHEEL					         12
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_REARHUB						         13
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATION_CHEST					             14

// BleCyclingPowerServiceSensorLocationSupported 
typedef U16 BleCyclingPowerCollectorSensorLocationSupported;
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_OTHER						  (0x0001u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_TOPOFSHOE					  (0x0002u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_INSHOE						  (0x0004u)
#define BLECYCLINGPOWERCOLLECOTR_SENSORLOCATIONSUPPORTED_HIP						  (0x0008u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_FRONTWHEEL					  (0x0010u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_LEFTCRANK					  (0x0020u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_RIGHTCRANK					  (0x0040u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_LEFTPEDAL					  (0x0080u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_RIGHTPEDAL					  (0x0100u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_FRONTHUB					  (0x0200u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_REARDROPOUT				  (0x0400u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_CHAINSTAY					  (0x0800u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_REARWHEEL					  (0x1000u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_REARHUB				      (0x2000u)
#define BLECYCLINGPOWERCOLLECTOR_SENSORLOCATIONSUPPORTED_CHEST						  (0x4000u)

/** 
 * BleCyclingPowerCollectorEvent type
 * Define the different kinds of events that could be received by the 
 * BleCyclingPowerRCollectorCallback
 */
typedef	U8	BleCyclingPowerCollectorEvent;

/** BLECYCLINGPOWERCOLLECTOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the linkUp information within the 
 * BleCyclingPowerCollectorLinkUPInfo pointer.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_LINKED						        0xAC

/** BLECYCLINGPOWERCOLLECTOR_EVENT_UNLINKED
 * Event received when the UnLink is completed the status field indicates the 
 * status of the operation.
 * The parms field indicates the link up information within a
 * BleCyclingPowerCollectorLinkUPInfo pointer.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_UNLINKED						        0xAD

/** BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
 * Event received to indicate that this COLLECTOR has received a CYCLINGPOWER
 * SENSOR measurement from the SENSOR.
 * It follows an alert from the Sensor
 * the status field indicates the status of the operation.
 * The parms field indicates the Cycling Power measurement information within the 
 * BleCyclingPowerCollectorCyclingPowerMeasurementInfo type.
 * - If status is BLE_STATUS_FAILED only the connHandle and the
 *   bleCyclingPowerCollector fields are consistent. Other fields
 *   of BleCyclingPowerCollectorCyclingPowerMeasurementInfo must be ignored
 * - If status is BLESTATUS_SUCCESS the measurement fields contains consistent
 *   information.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT	            0xAE

/** BLECYCLINGPOWERCOLLECTOR_EVENT_CUMULATIVEVALUESET
 * Event received to indicate that this COLLECTOR has set a new cumulative
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetCumulativeValue().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_CUMULATIVEVALUESET		            0xAF

/** BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_UPDATED	
 * Event received to indicate that this COLLECTOR has set a new sensor location
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetSensorLocationValue().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_UPDATED		        0xB9

/** BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_CURRENT	
 * Event received to indicate that this COLLECTOR has get a new sensor location 
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_GetCurrentSensorLocation().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_CURRENT			    0xBA

/** BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_SUPPORTED	
 * Event received to indicate that this COLLECTOR has get a list of sensor 
 * supported value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_GetSensorLocationSupported().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_SUPPORTED			    0xBB

/** BLECYCLINGPOWERCOLLECTOR_EVENT_CRANKLENGTHSET	
 * Event received to indicate that this COLLECTOR has set new crank length 
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetCrankLength().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_CRANKLENGTHSET			            0xBC

/** BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINLENGTHSET	
 * Event received to indicate that this COLLECTOR has set new chain length 
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetChainLength().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINLENGTHSET			            0xBD

/** BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINWEIGHTSET	
 * Event received to indicate that this COLLECTOR has set new chain weight 
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetChainWeight().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINWEIGHTSET			            0xBE

/** BLECYCLINGPOWERCOLLECTOR_EVENT_SPANLENGTHSET	
 * Event received to indicate that this COLLECTOR has set new span length 
 * value.
 * It follows the call of BLECYCLINGPOWER_COLLECTOR_SetSpanLength().
 * The status field indicates the status of the operation.
 * The parms field is not applicable.
 */
#define BLECYCLINGPOWERCOLLECTOR_EVENT_SPANLENGTHSET			            0xBF



/**
 * BleCyclingPowerCollectorCallback
 *	This callback receives the BLE CYCLING POWER COLLECTOR events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleCyclingPowerCollectorCallback)(
	BleCyclingPowerCollectorEvent	        event,
	BleStatus					            status,
	void					                *parms); 


/** 
 * BleCyclingPowerCollector type
 * Define a local COLLECTOR, it represents a memory block
 * that should be passed by the application during the 
 * BLECYCLINGPOWER_COLLECTOR_LinkUpCyclingPowerProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLECYCLINGPOWER_COLLECTOR_LinkUpCyclingPowerProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * COLLECTOR receives the BLECYCLINGPOWERCOLLECTOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this COLLECTOR memory block while 
 * linked with a remote SENSOR. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local COLLECTOR could be then passed as parameter to 
 * profile API in order to identify to which linked SENSOR the operation  
 * is destinated.
 *
 * A COLLECTOR is unique per remote Cycling Power SENSOR, it means 
 * that if this local device is connected to multiple remote SENSOR the 
 * application shall register multiple COLLECTORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand							gattCommand;
	BleGattCommand							gattCnfCommand;

	U16										connHandle;
	U8										linked;
	U8										state;
	
	AttHandle								cyclingPowerServiceStartingHandle;
	AttHandle								cyclingPowerServiceEndingHandle;

	AttHandle								cyclingPowerMeasurementValueHandle;
	AttHandle								cyclingPowerMeasurementConfigHandle;

	

	AttHandle								cyclingPowerFeatureValueHandle;

    BleCyclingPowerCollectorFeatureBitfield   
                                            cyclingPowerFeatureValue;

	AttHandle							    cyclingPowerSensorLocationValueHandle;
	AttHandle								cyclingPowerSensorLocationValueSupportedHandle;



	AttHandle								cyclingPowerControlPointValueHandle;
	AttHandle								cyclingPowerControlPointConfigHandle;
	U8										cyclingPowerControlPointValue[17];


	U8										cyclingPowerConfigValue[2];

	U32										cyclingPowerSensorFeatures;
	U8										cyclingPowerSensorLocation;	


} BleCyclingPowerCollector;


/** 
 * BleCyclingPowerCollectorLinkUPInfo type
 * structure passed in parameter in the BleCyclingPowerCollectorCallback
 * parms fields during the BLECYCLINGPOWERCOLLECTOR_EVENT_LINKED event.
 * It indicates :
 * bleCyclingPowerCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * supportedFeatures : The sensor supported features
 */
typedef struct {

	BleCyclingPowerCollector			        *bleCyclingPowerCollector;
	U16											connHandle;
    BleCyclingPowerCollectorFeatureBitfield     supportedFeatures;
} BleCyclingPowerCollectorLinkUPInfo;



/** 
 * BleCyclingPowerCollectorSensorLocationInfo type
 * structure passed in parameter in the BleCyclingPowerCollectorCallBack
 * parms fields during the BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_CURRENT event.
 * It indicates :
 * bleCyclingPowerCollector : the collector for which the event is
 *		destinated
 * connHandle : The connection handle for which the linkup has completed
 * sensorLocation: sensor location value
 */
typedef struct {

    BleCyclingPowerCollector							*bleCyclingPowerCollector;
    U16													connHandle;
    BleCyclingPowerCollectorSensorLocation				sensorLocation;

} BleCyclingPowerCollectorSensorLocationInfo;



/** 
 * BleCyclingPowerCollectorSensorLocationSupportedInfo type
 * structure passed in parameter in the BleCyclingPowerCollectorCallBack
 * parms fields during the 
 * BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_SUPPORTED event.
 * It indicates :
 * bleCyclingPowerCollector : the collector for which the event is
 * destinated
 * connHandle : The connection handle for which the linkup has completed
 * sensorLocationSupportedBitfield: a list of sensor location supported 
 * by Cycling Power Sensor
 */

typedef struct {

    BleCyclingPowerCollector									*bleCyclingPowerCollector;
    U16															connHandle;
    BleCyclingPowerCollectorSensorLocationSupported			    sensorLocationSupportedBitfield;

} BleCyclingPowerCollectorSensorLocationSupportedInfo;

/** 
 * BleCyclingPowerCollectorFlags type
 * Bitfield defining the available flags values.
 */
typedef U16 BleCyclingPowerCollectorFlags;
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_PEDALPOWERBALANCEMEASUREMENT				 (0x0001u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_PEDALPOWERBALANCEREFERENCEMEASUREMENT		 (0x0002u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_ACCUMULATEDTORQUEMEASUREMENT				 (0x0004u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_ACCUMULATEDTORQUESOURCEMEASUREMENT          (0x0008u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_WHEELREVOLUTIONDATAMEASUREMENT				 (0x0010u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_CRANKREVOLUTIONDATAMEASUREMENT				 (0x0020u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_EXTREMEFORCEMAGNITUDESMEASUREMENT           (0x0040u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_EXTREMETORQUEMAGNITUDESMEASUREMENT          (0x0080u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_EXTREMANGLESMEASUREMENT			         (0x0100u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_TOPDEADSPOTANGLEMEASUREMENT	    		 (0x0200u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_BOTTOMDEADSPOTANGLEMEASUREMENT              (0x0400u)
#define BLECYCLINGPOWERCOLLECTOR_FLAGBITMASK_ACCUMULATEDENERGYMEASUREMENT                (0x0800u)
//#define BLECYCLINGOWERCOLLECTOR_FLAGBITMASK_OFFSETCOMPENSATIONMEASUREMENT              (0x1000u)
/** 
 * BleCyclingPowerCollectorMeasurementFlags type
 * Bitfield defining the flags that can be present in the CYCLINGPOWER measurement
 * can be accessed directly via the cyclingpowerMeasurementFlagsValue byte or 
 * bit-by-bit in the fields structure.
 */
typedef union {
    /** cyclingPowerMeasurementFlagsValue:
     * A byte indicating what are the fields included in the measurement
     */
	BleCyclingPowerCollectorFlags cyclingPowerMeasurementFlagsValue;

	struct 
	{
		/** Pedal Power Balance Present et Pedal Power Balance Reference:
         * Fields indicating if Pedal Power Balance
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
		 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_PEDALPOWERBALANCESUPPORTED	
         * is not supported.
		 * The Pedal Power Balance Reference bit of the Flags field (bit 1) 
         * describes whether the value is referenced from �left� or 
		 * the reference is �unknown�.
         */
         unsigned int pedalPowerBalancePresent:1;
		 unsigned int pedalPowerBalanceReference:1;
		
		
		/** Accumulated torque present and Accumulated torque source:
         * Fields indicating if Accumulated Torque 
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features, 
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDTORQUESUPPORTED	
         * is not supported.
		 * The Accumulated Torque Source bit of the Flags field (bit 3) 
		 * describes whether the value is �wheel based� or �crank based�
         */
		 unsigned int accumulatedTorquePresent:1;
	     unsigned int accumulatedTorqueSource:1;

        /** wheelRevolutionDataPresent:
         * Field indicating if the wheel revolution data 
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
         * is not supported.
         */
		 unsigned int wheelRevolutionDataPresent:1;

        /** crankRevolutionDataPresent:
         * Field indicating if the crank revolution data  is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
         * is not supported.
         */
		 unsigned int crankRevolutionDataPresent:1;
		/** extremeForceMagnitudesPresent:
         * Field indicating if the extreme force magnitudes is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
         * is not supported and 
		 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
		 * is not set to 0
         */
		 unsigned int extremeForceMagnitudesPresent:1;
		
		/** extremeTorqueMagnitudesPresent:
         * Field indicating if the extreme torque magnitudes is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
         * is not supported and 
		 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
		 * is not set to 1
         */
		 unsigned int extremeTorqueMagnitudesPresent:1;
		/** extremeAnglesPresent:
         * Field indicating if the extreme angle is included
         * in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
         * is not supported.
         */
		 unsigned int extremeAnglesPresent:1;


		/** Top Dead Spot Angle Present:
         * Field indicating if the top dead spot angle
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_TNBDEADANGLESSUPPORTED			
         * is not supported 
         */
		 unsigned int topDeadSpotAnglePresent:1;	
       
		/** Bottom Dead Spot Angle Present:
         * Field indicating if the bottom dead angle
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
		 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_TNBDEADANGLESSUPPORTED		
         * is not supported 
         */
	     unsigned int bottomDeadSpotAnglePresent:1;	
		
		 /** Accumulated Energy Present:
         * Field indicating if the accumulated energy
         * is included in the measurement.
         * Shall be ignored if in the remote sensor features,
         * BBLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDENERGYSUPPORTED		
         * is not supported 
         */
		 unsigned int accumulatedEnergyPresent:1;	

         unsigned int offsetCompensationIndicator:1; 
		 
		 //The other 3 bits are reserved for future use
		 unsigned int reservedB:3;
	
	} fields;

} BleCyclingPowerCollectorMeasurementFlags;

/** 
 * BleCyclingPowerCollectorMeasurementInfo type
 * structure passed in parameter in the BleCyclingPowerCollectorCallback
 * parms fields during the
 * BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT event.
 * It describes the Cycling Power SENSOR measurement.
 */
typedef struct {

	/*
	 * The link with the remote Sensor for which this 
	 *	event has been received.
	 */	
	BleCyclingPowerCollector				*bleCyclingPowerCollector;

	/*
	 * The connection handle from which the measurement information
	 *	is coming
	 */
	U16									    connHandle;

    /*
     * The flags indicating which Value is present in the measurement,
     * only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     */
	BleCyclingPowerCollectorMeasurementFlags
                                            flags;
	/** Instantaneous Power:
	 * The Instantaneous Power value represents either the total power the user 
	 * is producing or a part of the total power depending on the type of sensor
     *
	 * only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     * Unit is in watts with a resolution of 1.
     */
     S16								    instantaneousPowerValue ;
	
	 /**Pedal Power Balance :
	 * The Pedal Power Balance value represents the ratio between the total 
	 * amount of power measured by the sensor and a reference.
	 *
	 * only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in percentage with a resolution of 1/2
	 *
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_PEDALPOWERBALANCESUPPORTED
     * is supported.
	 */
	 U8						   		        pedalPowerBalanceValue ;

	/**Cumulative Torque  :
	 * The Cumulative Torque value represents the cumulative value of
	 * the torque measured by the Sensor.When a connection is established, 
	 * this value starts at 0 Newton meter and is may roll over
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in newton metres with a resolution of 1/32.
	 *
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDTORQUESUPPORTED	
     * is supported.
	 */
	 U16									cumulativeTorqueValue ;
    
	 /** CumulativeWheelRevolution:
     * The Cumulative Wheel Revolutions value represents the number of times a 
     * wheel rotates, is used in combination with the Last Wheel Event Time
     * and the wheel circumference stored on the Client to determine:
     *  - the speed of the bicycle
     *  - the distance traveled. 
	 *  - the power if combined with the Crank Revolution Data
     * This value is expected to be set to 0 (or another desired value in case 
     * of e.g. a sensor upgrade) at initial installation on a bicycle.
     *
     * Only valid if BLECYCLINGPOWERPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
     * is supported.
     */
 	 U32									 cumulativeWheelRevolution ;

    /** lastWheelEvent:
     * The 'wheel event time' is a free-running-count of 1/2048 second units 
     * and it represents the time when the wheel revolution was detected by 
     * the wheel rotation sensor. Since several wheel events can occur between
     * transmissions, only the Last Wheel Event Time value is transmitted. 
     * The Last Wheel Event Time value rolls over every 64 seconds.
     *
     * Only valid if BLERUNNINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_WHEELREVOLUTIONDATASUPPORTED
     * is supported.
     */
	U16										lastWheelEvent;

    /** CumulativeCrankRevolution:
     * The Cumulative Crank Revolutions value, which represents the number of 
     * times a crank rotates, is used in combination with the Last Crank Event
     * Time to determine:
     *  - if the cyclist is coasting 
     *  - the average cadence
	 *	- the power if combined with the Wheel Revolution Data
     * Average cadence is not accurate unless 0 cadence events (i.e. coasting) 
     * are subtracted.  This value is intended to roll over and is not 
     * configurable.
     *
     * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
     * is supported.
     */
	 U16									cumulativeCrankRevolution ;

    /** lastCrankEvent:
     * The 'crank event time' is a free-running-count of 1/1024 second units
     * and it represents the time when the crank revolution was detected by
     * the crank rotation sensor. Since several crank events can occur between
     * transmissions, only the Last Crank Event Time value is transmitted. 
     * This value is used in combination with the Cumulative Crank Revolutions
     * value to enable the Client to calculate cadence. 
     * The Last Crank Event Time value rolls over every 64 seconds.
     *
     * Only valid if BLERUNNINGPOWERCOLLECTOR_EVENT_RSCMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
     *
     * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_CRANKREVOLUTIONDATASUPPORTED
     * is supported.
     */
	
	 U16									lastCrankEvent;
	 /** Maximum Force   :
	 * The Maximum Force value represents the maximum force value measured in 
     * a single crank revolution
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in newtons with a resolution of 1. 
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
     * is supported and 
	 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
	 * is set to 0.
	 */
	 S16									maximumForceValue  ;
	/** Minimum Force   :
	 * The Minimum Force value represents the minimum force value measured 
	 * in a single crank revolution
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in newtons with a resolution of 1. 
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
     * is supported and 
	 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
	 * is set to 0.
	 */
	 S16									minimumForceValue  ;
	/** Maximum Torque    :
	 * The Minimum Toruqe value represents the maximum force value measured 
	 * in a single crank revolution
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in newton metres with a resolution of 1/32.  
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
     * is supported and 
	 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
	 * is set to 1.
	 */
	 S16									maximumTorqueValue  ;
	 /** Minimum Torque    :
	 * The Minimum Toruqe value represents the minimum force value measured 
	 * in a single crank revolution
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in newton metres with a resolution of 1/32. 
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEMAGNITUDESSUPPORTED
     * is supported and 
	 * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_SENSORMEASUREMENTCONTEXT
	 * is set to 1.
	 */
	 S16									minimumTorqueValue  ;
	 /**Maximum Angle    :
	 * The Maximum Angle value rrepresents the angle of the crank when 
	 * the maximum value is measured in a single crank revolution
	
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	  * Unit is in degrees with a resolution of 1 
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEANGLESSUPPORTED
     * is supported.
	 
	 */
	 U16									maximumAngleValue  ;
	  /**Minimum Angle    :
	 * The Miniimum Angle value represents the angle of the crank when 
	 * the minimum value is measured in the same crank revolution.
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in degrees with a resolution of 1 
	 * 
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_EXTREMEANGLESSUPPORTED
     * is supported. 
	 
	 */
	 U16									minimumAngleValue  ;
	 /**Top Dead Spot Angle  :
	 * The Top Dead Spot Angle value represents the crank angle when the value 
	 * of the Instantaneous Power value becomes positive
	 *
   	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in degrees with a resolution of 1
	 *
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_TNBDEADANGLESSUPPORTED		
     * is supported.
	 */
	 U16									topDeadSpotAngleValue ;

	/**Bottom Dead Spot Angle  :
	 * The Bottom Dead Spot Angle value represents the crank angle when the value
	 * of the Instantaneous Power value becomes negative
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in degrees with a resolution of 1
	 *
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_TNBDEADANGLESSUPPORTED		
     * is supported.
	 */
	 U16									bottomDeadSpotAngleValue ;
	
	 /**Accumulated Energy:
	 * The Accumulated Energy represents the accumulated value of the energy 
     * measured by the sensor. When a connection is established, this value starts 
	 * at 0 kilo Joules and is not expected to roll over.
	 *
	 * Only valid if BLECYCLINGPOWERCOLLECTOR_EVENT_CYCLINGPOWERMEASUREMENT
     * event status is BLESTATUS_SUCCESS, must be ignored otherwise.
	 * Unit is in kilojoules with a resolution of 1.
	 *
	 * Only reported when in the remote sensor features,
     * BLECYCLINGPOWERCOLLECTOR_FEATUREBITMASK_ACCUMULATEDENERGYSUPPORTED		
     * is supported.
	 */
	 U16									accumulatedEnergyValue ;


} BleCyclingPowerCollectorMeasurementInfo;


/** 
 * BleCyclingPowerCollectorLinkMode type
 * Type passed during the 
 * BLECYCLINGPOWER_COLLECTOR_LinkUpCyclingPowerProfile API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLECYCLINGPOWER_COLLECTOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information is 
 * restored (BLECYCLINGPOWER_COLLECTOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 */
typedef U8 BleCyclingPowerCollectorLinkMode;

#define BLECYCLINGPOWER_COLLECTOR_LINKUP_COMPLETE			1
#define BLECYCLINGPOWER_COLLECTOR_LINKUP_RESTORE			2

/****************************************************************************
 *	APPLICATION INTERFACE functions definition
 ****************************************************************************/

/** Init the COLLECTOR role of Cycling Power profile 
 *
 * BLECYCLINGPOWER_COLLECTOR_Init()
 *	This function is the entry point of a Cycling Power profile
 *  application that runs the COLLECTOR role, it inits the
 *	profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param callback: The BleCyclingPowerCollectorCallback in which 
 *				 the collector events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (Profile is already initialized).
 *
 * @author My Huong NHAN
 */
BleStatus BLECYCLINGPOWER_COLLECTOR_Init(
			BleCyclingPowerCollectorCallback callback);

/** De-Init the Collector Role of the Cycling Power Profile.
 *
 * BLECYCLINGPOWER_COLLECTOR_Deinit
 *	This function is used to De-Init the Collector Role of the 
 *  Cycling Power Profile.
 *
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLECYCLINGPOWER_COLLECTOR_Deinit( void ) ;

/** Link Up the Cycling Power Profile with the specified remote Device
 *
 * BLECYCLINGPOWER_COLLECTOR_LinkUpCyclingPowerProfile()
 * This function is used to link-Up the Cycling Power Profile with the 
 * specified remote device.
 * It will try to discover the Cycling Power profile in the remote device 
 * using the given collector.
 * BLECYCLINGPOWERCOLLECTOR_EVENT_LINKED will be received in the callback with the
 * status of the operation. 
 *
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A valid Cycling Power COLLECTOR to link-up
 *					with a remote Cycling Power SENSOR.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to the profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this COLLECTOR could be then passed as parameter  
 *                  to the profile API in order to identify to which linked 
 *                  SENSOR the operation is destinated. 
 * @param connHandle: The connection Handle on which the link up between this 
 *			BLE CYCLING POWER COLLECTOR and the remote BLE CYCLING POWER
 *			SENSOR shall be done.
 * @param linkUpMode: The desired link-up mode, restore from persistent memory or
 *			complete link-up.
 * @param info: Only valid if mode is BLECYCLINGPOWER_COLLECTOR_LINKUP_RESTORE,
 *			and return status is BLESTATUS_SUCCESS.
 *			It reports the link-Up information ( sensor Features).
 *			It may be equal to (BleCyclingPowerCollectorLinkUPInfo *)0.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, the 
 *		BLECYCLINGPOWERCOLLECTOR_EVENT_LINKED event will be received in the
 *		callback registered during the call of
 *		BLECYCLINGPOWER_COLLECTOR_Init. This status is returned only if mode
 *		is BLECYCLINGPOWER_COLLECTOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLECYCLINGPOWER_COLLECTOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLECYCLINGPOWER_COLLECTOR_LINKUP_RESTORE, it means that 
 *		BLECYCLINGPOWERCOLLECTOR_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Cycling Power SENSOR. 
 *		It may also indicate that the device is already Linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	    an other Cycling Power Operation is in Progress, wait for this
 *      operation completion.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author My Huong NHAN
 */
BleStatus BLECYCLINGPOWER_COLLECTOR_LinkUpCyclingPowerProfile(
						BleCyclingPowerCollector *collector,
						U16 connHandle,
						BleCyclingPowerCollectorLinkMode linkUpMode,
						BleCyclingPowerCollectorLinkUPInfo *info
						);

/** Un Link the Cycling Power Profile with the specified linked COLLECTOR
 *
 * BLECYCLINGPOWER_COLLECTOR_UnLinkCyclingPowerProfile()
 *	This function is used to UnLink the Cycling Power Profile with the 
 *  specified previously linked COLLECTOR.
 *  This COLLECTOR will not receive any more information from the linked 
 *	Cycling Power Sensor. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_CYCLINGPOWER_* from persistent memory
 *
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Cycling Power COLLECTOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the collector memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author My Huong NHAN
 */
BleStatus BLECYCLINGPOWER_COLLECTOR_UnLinkCyclingPowerProfile(
		BleCyclingPowerCollector *collector);


/** set a new cumulative (total) distance in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetCumulativeValue()
*	This function is used to set a new cumulative (total) distance
* in remote linked SENSOR, if the remote sensor supports total distance
* feature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as cumulative value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_CUMULATIVEVALUESET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetCumulativeValue( 
		BleCyclingPowerCollector			*collector,
		U32                                 value);

/** set a Cycling Power Sensor Location  in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetSensorLocation()
*	This function is used to set a new sensor location value
* in remote linked SENSOR, if the remote sensor supports multiple Sensor
* locationfeature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as sensor location value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_UPDATED 
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetSensorLocationValue( 
							BleCyclingPowerCollector			        *collector,
							BleCyclingPowerCollectorSensorLocation		value);

/** Get the CyclingPower SensorLocation in remote linked SENSOR
 *
 * BLECYCLINGPOWER_COLLECTOR_GetCurrentSensorLocation()
 *	This function is used to Get the sensor location on the remote
 * SENSOR defined by the previously linked COLLECTOR.
 *   
 * BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param bleCyclingPowerCollector: Cycling Power COLLECTOR.  
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_CURRENT event will 
 * be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Cycling Power Profile Operation is in Progress, wait
 * for this operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter 
 * (the Netowrk Availability Profile seems not been initialized).
 *
 * @author My Huong NHAN
 */
BleStatus BLECYCLINGPOWER_COLLECTOR_GetCurrentSensorLocation(
				      				BleCyclingPowerCollector *collector);

/** Get the Sensor Location Supported from a linked Sensor
 *
 * BLECYCLINGPOWER_COLLECTOR_GetSensorLocationSupported()
 *	This function is used to Get the List of Sensor Location Supported on
 *  the Cycling Power Sensor defined by the previously linked COLLECTOR.
 *	if the  feature Multiple Sensor Location is supported
 *   
 *  BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
 *
 * @param collector: A linked Cycling Power Collector. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLECYCLINGPOWERCOLLECTOR_EVENT_SENSORLOCATION_SUPPORTED	 event will 
 *      be received in the callback with the status of the operation and the 
 *		time information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	    an other Cycling PowerProfile Operation is in Progress, wait for this
 *      operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the Cycling Power Profile seems not been initialized).
 *
 * @author My Huong NHAN
 */

BleStatus BLECYCLINGPOWER_COLLECTOR_GetSensorLocationSupported(
				      				BleCyclingPowerCollector *collector);

/** set a new a new crank length in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetCrankLength()
*	This function is used to set a new crank length value in remote
*  linked SENSOR, if the remote sensor supports Crank Length Adjustment 
* feature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as crank length value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_CRANKLENGTHSET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetCrankLength( 
		BleCyclingPowerCollector			*collector,
		U16                                 valueCrank);

/** set a new a new Chain length in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetChainLength()
*	This function is used to set a new crank length value in remote
*  linked SENSOR, if the remote sensor supports Chain Length Adjustment 
* feature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as chain length value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINLENGTHSET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetChainLength( 
		BleCyclingPowerCollector			*collector,
		U16                                 valueChainLength);
/** set a new a new Chain Weight in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetChainWeight()
*	This function is used to set a new chain weight value in remote
*  linked SENSOR, if the remote sensor supports Chain Weight Adjustment 
* feature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as chain weight value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_CHAINWEIGHTSET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetChainWeight( 
		BleCyclingPowerCollector			*collector,
		U16                                 valueChainWeight);

/** set a new a new Span Length in remote linked SENSOR
*
* BLECYCLINGPOWER_COLLECTOR_SetChainWeight()
*	This function is used to set a new chain weight value in remote
*  linked SENSOR, if the remote sensor supports Span Length Adjustment 
* feature.
*   
* BLECYCLINGPOWER_SUPPORT_COLLECTOR shall be enabled
*
* @param collector: A linked CYCLING POWER COLLECTOR. 
* @param value: The new value to set as span length value. 
*
* @return The status of the operation:
*
* 	- BLESTATUS_PENDING indicates that the operation succeeded, once 
*		completed the BLECYCLINGPOWERCOLLECTOR_EVENT_SPANLENGTHSET  
*		event will be received in the callback with the status of the 
*		operation.
*
*	- BLESTATUS_FAILED indicates that the operation has failed. 
*		It could be because the device is not linked, or the remote sensor is
*       not supporting the total distance feature
*
*	- BLESTATUS_BUSY indicates that the operation has failed, because 
*		an other CYCLING POWER profile Operation is in Progress, wait for this
*		operation completion 
*
*	if BLE_PARMS_CHECK is set to 1:
*	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
*		an invalid parameter (the CYCLING POWER profile seems not been initialized).
*
* @author My Huong NHAN
*/
BleStatus BLECYCLINGPOWER_COLLECTOR_SetSpanLength( 
		BleCyclingPowerCollector			*collector,
		U16                                 valueSpan);
									
/***************************************************************************\
 * OPTIONAL APPLICATION INTERFACE functions definition
\***************************************************************************/

#endif //(BLECYCLINGPOWER_SUPPORT_COLLECTOR== 1)

#endif /*__BLECYCLINGPOWER_COLLECTOR_H*/
