#ifndef __BLECYCLINGPOWER_SENSOR_H
#define __BLECYCLINGPOWER_SENSOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleCyclingPower_Sensor.h
 *
 * Description:   Contains interfaces definitions for Cycling Power
 *					Profile when the local device operates in the SENSOR
 *					role.
 * 
 * Created:     May , 2013
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLECYCLINGPOWER_SUPPORT_SENSOR
 * Defines if the BLE CYCLINGPOWERSENSOR Profile implementation supports the 
 * SENSOR Role
 * If enabled ( set to 1 ) it enables all the following BLE CYCLINGPOWER
 * SENSOR Profile SENSOR configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLECYCLINGPOWER_SUPPORT_SENSOR
#define BLECYCLINGPOWER_SUPPORT_SENSOR					0
#endif //BLECYCLINGPOWER_SUPPORT_SENSOR

#if (BLECYCLINGPOWER_SUPPORT_SENSOR== 1)
//
// Then check dependencies

#include "Services/BleCyclingPower_Service.h"


// Cycling Power Service is mandatory
#if (BLE_SUPPORT_CYCLINGPOWER_SERVICE == 0)
#error BLE_SUPPORT_CYCLINGPOWER_SERVICE shall be enabled when BleCyclingPower SENSOR Role is enabled
#endif //(BLE_SUPPORT_CYCLINGPOWER_SERVICE == 0)

/***************************************************************************\
 * OPTIONAL FEATURES FOR CYCLINGSPEEDANDCADENCE SENSOR PROFILE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE CYCLING POWER profile in SENSOR Role.
 *
 * BLECYCLINGPOWER_SENSOR_Register()
 *	This function is used to register and initialise a BLE  
 *  CYCLING POWER profile in SENSOR role.
 *  It will add the supported CYCLING POWER Service 
 *  characteristics into the Attribute database.
 *	This Interface shall be the entry point of a BLE 
 *  CYCLING POWER profile in SENSOR Role.
 *
 * BLECYCLINGPOWER_SUPPORT_SENSOR shall be enabled.
 *
 * @param callback : The function callback where the Cycling Power
 *  service event will be posted.
 *  As the only event supported is received when the wheel revolution is updated
 *  this parameter is ignored and can be null, i.e. equal to 
 * (BleCyclingPowerServiceCallBack) 0, if 
 * BLE_CYCLINGPOWERSERVICE_SUPPORT_WHEEL_REVOLUTION_DATA is disabled 
 * (set to 0)
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author My Huong NHAN
 */
BleStatus BLECYCLINGPOWER_SENSOR_Register( 
        BleCyclingPowerServiceCallBack callback );


/***************************************************************************\
 * OPTIONAL API functions definition CYCLINGPOWER SENSOR PROFILE
\***************************************************************************/

#endif //(BLECYCLINGPOWER_SUPPORT_SENSOR== 1)

#endif /*__BLECYCLINGPOWER_SENSOR_H*/
