#ifndef __BLEPHONEALERTSTATUS_CLIENT_H
#define __BLEPHONEALERTSTATUS_CLIENT_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BlePhoneAlertStatus_Client.h
 *
 * Description:   Contains interfaces definition for PHONE ALERT STATUS 
 *					Profile when the local device operates in the CLIENT role.
 * 
 * Created:       January, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT
 * Defines if the BLE PHONE ALERT STATUS Profile implementation supports the 
 * CLIENT Role.
 * If enabled ( set to 1 ) it enables all the following BlePhoneAlertStatus 
 * CLIENT configuration and Application Interface
 * The default value for this option is disabled (0). 
 */
#ifndef BLEPHONEALERTSTATUS_SUPPORT_CLIENT
#define BLEPHONEALERTSTATUS_SUPPORT_CLIENT								0
#endif //BLEPHONEALERTSTATUS_SUPPORT_CLIENT

#if (BLEPHONEALERTSTATUS_SUPPORT_CLIENT == 1)
//to get access to global service-profile type
#include "Services/BlePhoneAlertStatus_Service.h"
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when PHONE ALERT STATUS  CLIENT Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when Phone Alert Status CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)

#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION	
 * Defines if the PHONE ALERT STATUS Profile CLIENT supports 
 * saving the SERVER information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the CLIENT saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SERVER information.
 * Upon reconnection with the SERVER, as these information are already known
 * then the linkUp process can be simplified.
 * There is 12 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is enabled (1). 
 */
#ifndef BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION	
#define BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION				1
#endif //BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_REMOTEFEATURES				0x83	//2 byte
#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_ALERTSTATUSHANDLE			0x84	//2 bytes
#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_ALERTSTATUSCONFIGHANDLE		0x85	//2 bytes
#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_RINGERSETTINGHANDLE			0x86	//2 bytes
#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_RINGERSETTINGCONFIGHANDLE	0x87	//2 bytes
#define BLEINFOTYPE_PHONEALERTSTATUSCLIENT_RINGERCONTROLHANDLE			0x88	//2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported force 
// the BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION
#define BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 

/**
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION	
 * Defines if the PHONE ALERT STATUS Profile CLIENT supports 
 * configuring the remote SERVER Ringer setting to be notified by the SERVER
 * when ringer setting change. ringer setting is either SLIENT or NORMAL
 *
 * It enables the 
 * BLEPHONEALERTSTATUS_CLIENT_SubscribeRingerSettingNotification to configure
 * SERVER to notify this CLIENT when Ringer Setting change; and
 * BLEPHONEALERTSTATUS_CLIENT_UnSubscribeRingerSettingNotification to 
 * configure the SERVER to not more notify this CLIENT when Ringer Setting 
 * change.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION	
#define BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION			0
#endif //BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION


/**
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE	
 * Defines if the PHONE ALERT STATUS Profile CLIENT supports 
 * configuring the remote SERVER Ringer state by setting the SERVER ringer state
 * either to SLIENT or NORMAL or MUTE ONCE state
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE	
#define BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE			0
#endif //BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE


#if (BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE == 1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be enabled when PHONE ALERT STATUS CLIENT Role is enabled\
	and BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE is enabled
#endif //(BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE ==1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE== 0)


/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BlePhoneAlertStatusClientEvent type
 * Define the different kind of events that could be received by the 
 * BlePhoneAlertStatusClientCallBack
 */
typedef	U8	BlePhoneAlertStatusClientEvent;

/** BLEPHONEALERTSTATUSCLIENT_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BlePhoneAlertStatusClientLinkUpInfo pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_LINKED									0xD0

/** BLEPHONEALERTSTATUSCLIENT_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BlePhoneAlertStatusClientLinkUpInfo pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_UNLINKED								0xD1

/** BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS
 * Event received when a Alert Status value has been received.
 * The alert Status can be received:
 * - After successfull linkUp, as defined in the specification, the Profile
 *   will automaticaly read the Alert status value.
 * - After issuing BLEPHONEALERTSTATUS_CLIENT_GetAlertStatus() to read the alert
 *   status value in the SERVER
 * - After a SERVER notification, if the CLIENT has previously subscribe such 
 *   kind of notification thanks to 
 *   BLEPHONEALERTSTATUS_CLIENT_SubscribeAlertStatusNotification
 *
 * The status field indicates the status of the operation.
 * The parms field indicates the Aler tStatus information within a
 * BlePhoneAlertStatusClientAlertStatusInfo pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS								0xD2

/** BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSSUBSCRIPTION_RSP
 * Event received when the Subscription of alert status notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BlePhoneAlertStatusClient pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSSUBSCRIPTION_RSP				0xD3

/** BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSUNSUBSCRIPTION_RSP
 * Event received when the Un-Subscription of alert status notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the unsubscription
 * response is associated within a BlePhoneAlertStatusClient pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSUNSUBSCRIPTION_RSP			0xD4

/** BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING
 * Event received when the Ringer Setting information has been received
 * The Ringer Setting  can be received:
 * - After issuing BLEPHONEALERTSTATUS_CLIENT_GetRingerSetting() to read the 
 *   ringer setting value in the SERVER
 * - After a SERVER notification, if the CLIENT has previously subscribe such 
 *   kind of notification thanks to 
 *   BLEPHONEALERTSTATUS_CLIENT_SubscribeRingerSettingNotification
 *
 * The status field indicates the status of the operation.
 * The parms field indicates the Ringer Setting  Information within a
 * BlePhoneAlertStatusClientRingerSettingInfo pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING							0xD5

/** BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGSUBSCRIPTION_RSP
 * Event received when the Subscription of Ringer Setting notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BlePhoneAlertStatusClient pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGSUBSCRIPTION_RSP			0xD6

/** BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGUNSUBSCRIPTION_RSP
 * Event received when the Un-Subscription of Ringer Setting notification is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the unsubscription
 * response is associated within a BlePhoneAlertStatusClient pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGUNSUBSCRIPTION_RSP			0xD7

/** BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERCONTROL_SENT
 * Event received when the control of Ringer Setting notifictaion has been 
 * sent to the remote SERVER.
 * The status field indicates the status of the operation.
 * The parms field indicates the client for which the ringer control
 * has been sent within a BlePhoneAlertStatusClient pointer.
 */
#define BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERCONTROL_SENT						0xD8

/**
 * BlePhoneAlertStatusClientCallBack
 *	This callback receives the BLE PHONE ALERT STATUS CLIENT profile events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BlePhoneAlertStatusClientCallBack)(
								BlePhoneAlertStatusClientEvent event,
								BleStatus status,
								void* parms); 

/** 
 * BlePhoneAlertStatusClient type
 * Define a local CLIENT, it represents a memory block
 * that should be passed by the application during the 
 * BLEPHONEALERTSTATUS_CLIENT_LinkUpPhoneAlertStatusProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEPHONEALERTSTATUS_CLIENT_LinkUpPhoneAlertStatusProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * CLIENT receive the BLEPHONEALERTSTATUSCLIENT_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this CLIENT memory block while 
 * linked with a remote SERVER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local CLIENT could be then passed as parameter to 
 * profile API in order to identify to which linked SERVER the operation  
 * is destinated.
 *
 * A CLIENT is unique per remote PHONE ALERT STATUS SERVER, it means 
 * that if this local device is connected to multiple remote SERVER the 
 * application shall register multiple CLIENTs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;
	BleGattCommand			gattInternalCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	U8						serverFeatures;
	U8 						configValue[2];

	AttHandle				PASSStartingHandle;
	AttHandle				PASSEndingHandle;
	AttHandle				alertStatusHandle;
	AttHandle				alertStatusConfigHandle;
	AttHandle				ringerSettingHandle;
	AttHandle				ringerSettingConfigHandle;
	AttHandle				ringerControlHandle;

} BlePhoneAlertStatusClient;

/** 
 * BlePhoneAlertStatusClientRemoteFeatures type
 * Bitfield passed in parms fields of the BlePhoneAlertStatusClientLinkUpInfo
 * during the BLEPHONEALERTSTATUSCLIENT_EVENT_LINKED event.
 * It indicates if the remote PHONE ALERT STATUS SERVER supports the optionnal features
 * like RINGER CONTROL characteristic.
 */
typedef U8 BlePhoneAlertStatusClientRemoteFeatures;

/** BLEPHONEALERTSTATUSCLIENT_REMOTESUPPORT_RINGERCONTROL
 * The remote PHONE ALERT STATUS SERVER supports the optionnal
 * Ringer Control characteristic within the PHONE ALERT STATUS service.
 */
#define	BLEPHONEALERTSTATUSCLIENT_REMOTESUPPORT_RINGERCONTROL		0x01



/** 
 * BlePhoneAlertStatusClientLinkUpInfo type
 * Structure passed in parameter in the BlePhoneAlertStatusClientCallBack
 * parms fields during the BLEPHONEALERTSTATUSCLIENT_EVENT_LINKED event.
 * It indicates :
 * blePhoneAlertStatusClient : The CLIENT from which the event is comming
 * connHandle : The connection handle from which the Link Up information
 *	is coming
 * remoteFeatures : A bitfield that indicates if the SERVER supports
 *	some optional features.
 *  Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BlePhoneAlertStatusClient				*blePhoneAlertStatusClient;
	U16										connHandle;
	BlePhoneAlertStatusClientRemoteFeatures	remoteFeatures;

} BlePhoneAlertStatusClientLinkUpInfo;

/** 
 * BlePhoneAlertStatusClientLinkMode type
 * Type passed during the BLEPHONEALERTSTATUS_CLIENT_LinkUpPhoneAlertStatusProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEPHONEALERTSTATUS_CLIENT_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEPHONEALERTSTATUS_CLIENT_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 * Note that if BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION is DISABLED, 
 * a linkUP with the BLEPHONEALERTSTATUS_CLIENT_LINKUP_RESTORE mode will always 
 * failed.
 */
typedef U8 BlePhoneAlertStatusClientLinkMode;

#define BLEPHONEALERTSTATUS_CLIENT_LINKUP_COMPLETE					1
#define BLEPHONEALERTSTATUS_CLIENT_LINKUP_RESTORE					2


/** 
 * BlePhoneAlertStatusClientAlertStatusInfo type
 * Structure passed in parameter in the BlePhoneAlertStatusClientCallBack
 * parms fields during the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS event.
 * It indicates :
 * blePhoneAlertStatusClient : The client from which the event is comming
 * connHandle : The connection handle from which the Alert Status information
 *	is coming
 * alertStatus: the Alert Status information provided by the SERVER;
 *              Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BlePhoneAlertStatusClient				*blePhoneAlertStatusClient;
	U16										connHandle;
	BlePhoneAlertStatusServiceAlertStatus	alertStatus;

} BlePhoneAlertStatusClientAlertStatusInfo;

/** 
 * BlePhoneAlertStatusClientRingerSettingInfo type
 * Structure passed in parameter in the BlePhoneAlertStatusClientCallBack
 * parms fields during the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING event.
 * It indicates :
 * blePhoneAlertStatusClient : The client from which the event is comming
 * connHandle : The connection handle from which the Ringer Setting
 *	is coming
 * ringerSetting: the current ringer setting provided by the SERVER;
 *           Only applicable if event status is BLESTATUS_SUCCESS.
 */
typedef struct {

	BlePhoneAlertStatusClient				*blePhoneAlertStatusClient;
	U16										connHandle;
	BlePhoneAlertStatusServiceRingerSetting	ringerSetting;

} BlePhoneAlertStatusClientRingerSettingInfo;



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/
/** Init the PHONE ALERT STATUS CLIENT profile
 *
 * BLEPHONEALERTSTATUS_CLIENT_Init()
 *	This function is the entry point of the PHONE ALERT STATUS CLIENT profile,  
 *	it inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param callback: The BlePhoneAlertStatusClientCallBack in which the CLIENTs 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPHONEALERTSTATUS CLIENT Profile is already 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */ 
BleStatus BLEPHONEALERTSTATUS_CLIENT_Init(BlePhoneAlertStatusClientCallBack callback);

/** De-Init the PHONE ALERT STATUS CLIENT Profile
 *
 * BLEPHONEALERTSTATUS_CLIENT_Deinit
 *	This function is used to De-Init the PHONE ALERT STATUS CLIENT Profile.
 *
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_Deinit( void );

/** Link Up the PHONE ALERT STATUS Profile with the specified remote Device
 *
 * BLEPHONEALERTSTATUS_CLIENT_LinkUpPhoneAlertStatusProfile()
 *	This function is used to link-Up the PHONE ALERT STATUS profile with the 
 *  specified remote device.
 *  It will try to discover the PHONE ALERT STATUS profile in the remote  
 *  device using the given CLIENT.
 *  Note that, after a successfull linkup and as defined in the specification
 *  the profile will automaticaly read the alert status value.
 *
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A valid BlePhoneAlertStatusClient CLIENT 
 *					to link-up with a remote PHONE ALERT STATUS SERVER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this SERVER could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  CLIENT the operation is destinated.   
 * @param connHandle: The connection Handle on which the link up between this
 *		 blePhoneAlertStatusClient and the remote PHONE ALERT STATUS SERVER profile
 *		 shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUS_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode is 
 *		BLEPHONEALERTSTATUS_CLIENT_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEPHONEALERTSTATUS_CLIENT_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEPHONEALERTSTATUS_CLIENT_LINKUP_RESTORE, it means that 
 *		BLEPHONEALERTSTATUSCLIENT_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote PHONE ALERT STATUS SERVER.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_LinkUpPhoneAlertStatusProfile(
								BlePhoneAlertStatusClient *blePhoneAlertStatusClient,
								U16 connHandle,
								BlePhoneAlertStatusClientLinkMode mode);


/** Un Link the PHONE ALERT STATUS Profile with the specified linked CLIENT
 *
 * BLEPHONEALERTSTATUS_CLIENT_UnLinkPhoneAlertStatusProfile()
 *	This function is used to UnLink the PHONE ALERT STATUS Profile with the 
 *  specified previously linked CLIENT.
 *  This CLIENT will not receive any more information from the linked 
 *	SERVER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_PHONEALERTSTATUSCLIENT_* from persistent memory
 *
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the CLIENT memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_UnLinkPhoneAlertStatusProfile(
		BlePhoneAlertStatusClient *blePhoneAlertStatusClient);

/** Get the current Alert Status from a linked SERVER
 *
 * BLEPHONEALERTSTATUS_CLIENT_GetAlertStatus()
 *	This function is used to Get the alert Status on the remote SERVER 
 *  defined by the previously linked CLIENT.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS event will be received in the 
 *		callback with the status of the operation and the Alert Status information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_GetAlertStatus( 
	BlePhoneAlertStatusClient *blePhoneAlertStatusClient );


/** Get the current Ringer Setting from a linked SERVER
 *
 * BLEPHONEALERTSTATUS_CLIENT_GetRingerSetting()
 *	This function is used to Get the Ringer Setting (either SILENT or NORMAL)
 *  on the remote SERVER defined by the previously linked CLIENT.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING event will be received in the 
 *		callback with the status of the operation and the Ringer Setting information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_GetRingerSetting( 
	BlePhoneAlertStatusClient *blePhoneAlertStatusClient );

/** Configure the PHONE ALERT STATUS SERVER to deliver the New Alert Status
 * when modified
 *
 * BLEPHONEALERTSTATUS_CLIENT_SubscribeAlertStatusNotification()
 *	This function is used by the given linked PHONE ALERT STATUS CLIENT to 
 *  configure the remote PHONE ALERT STATUS SERVER to deliver new current Alert
 *  status when alert status change on the SERVER.
 *  When new alert Status information is received the 
 *  BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUS event will be received in the 
 *  callback with the new alert status information.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSTATUSSUBSCRIPTION_RSP event 
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_SubscribeAlertStatusNotification(
					BlePhoneAlertStatusClient *blePhoneAlertStatusClient );

/** Configure the PHONE ALERT STATUS SERVER to not deliver the New alert
 *  status when it is modified
 *
 * BLEPHONEALERTSTATUS_CLIENT_UnSubscribeAlertStatusNotification()
 *	This function is used by the given linked PHONE ALERT STATUS CLIENT to 
 *  configure the remote PHONE ALERT STATUS SERVER to not deliver new
 *  current Alert status when alert status change on the SERVER.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_ALERTSSTATUSUNSUBSCRIPTION_RSP  
 *		event will be received in the callback with the status of the 
 *		operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_UnSubscribeAlertStatusNotification(
					BlePhoneAlertStatusClient *blePhoneAlertStatusClient );

#if (BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION == 1) 
/** Configure the PHONE ALERT STATUS SERVER to deliver the New Ringer Setting
 * when modified
 *
 * BLEPHONEALERTSTATUS_CLIENT_SubscribeRingerSettingNotification()
 *	This function is used by the given linked PHONE ALERT STATUS CLIENT to 
 *  configure the remote PHONE ALERT STATUS SERVER to deliver new Ringer Setting 
 *  value when Ringer Setting change on the SERVER.
 *  Ringer Setting may change between either SILENT or NORMAL.
 *  When new Ringer Setting information is received the 
 *  BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING event will be received in the 
 *  callback with the new Ringer Setting information.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGSUBSCRIPTION_RSP event  
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_SubscribeRingerSettingNotification(
					BlePhoneAlertStatusClient *blePhoneAlertStatusClient );


/** Configure the PHONE ALERT STATUS SERVER to not deliver the New Ringer
 *  Setting when it is modified
 *
 * BLEPHONEALERTSTATUS_CLIENT_UnSubscribeRingerSettingNotification()
 *	This function is used by the given linked PHONE ALERT STATUS CLIENT to 
 *  configure the remote PHONE ALERT STATUS SERVER to not deliver
 *  new Ringer Setting  value when Ringer Setting change on the SERVER.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTINGUNSUBSCRIPTION_RSP
 *      event will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *      initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_UnSubscribeRingerSettingNotification(
								BlePhoneAlertStatusClient *blePhoneAlertStatusClient );

#endif //(BLEPHONEALERTSTATUSCLIENT_SUPPORT_RINGERSETTINGNOTIFICATION == 1) 

#if (BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE == 1)
/** request the PHONE ALERT STATUS SERVER to set the ringer to the given state
 *
 * BLEPHONEALERTSTATUS_CLIENT_RequestRingerState()
 *	This function is used by the given linked PHONE ALERT STATUS CLIENT to 
 *  request the remote PHONE ALERT STATUS SERVER to change the value of Ringer 
 *  Setting to the Given one.
 *  Ringer Setting may change between either SILENT or NORMAL or MUTE ONCE.
 *  When request has been sent to the remote SERVER
 *  BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERCONTROL_SENT event will be received in  
 *  the callback.
 * If the remote SERVER accepts the request and change the value of the Ringer 
 *  Setting State then a BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERSETTING event will
 *  be received in the callback.
 *
 *
 * Note that this function will failed directly if the remote does not support
 *  the ringer control characteristic.
 *   
 * BLEPHONEALERTSTATUS_SUPPORT_CLIENT shall be enabled
 * BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE shall be enabled
 *
 * @param blePhoneAlertStatusClient: A linked PHONE ALERT STATUS CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPHONEALERTSTATUSCLIENT_EVENT_RINGERCONTROL_SENT event  
 *		will be received in the callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.It can be also because the
 *	    remote SERVER does not support the RINGER CONTROL characteristic, the 
 *      support of this feature by the SERVER is returned upon successful linkup
 *		event.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other PHONE ALERT STATUS profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the PHONE ALERT STATUS profile seems not been 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_CLIENT_RequestRingerState(
					BlePhoneAlertStatusClient *blePhoneAlertStatusClient,
					BlePhoneAlertStatusServiceRingerRequestedState requestedState);
#endif //(BLEPHONEALERTSTATUSCLIENT_SUPPORT_CONTROLLINGRINGERSTATE == 1)

#endif //(BLEPHONEALERTSTATUS_SUPPORT_CLIENT== 1)

#endif //__BLEPHONEALERTSTATUS_CLIENT_H
