#ifndef __BLEPHONEALERTSTATUS_SERVER_H
#define __BLEPHONEALERTSTATUS_SERVER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BlePhoneAlertStatus_Server.h
 *
 * Description:   Contains interfaces definitions for Time 
 *					Profile when the local device operates in the SERVER
 *					role.
 * 
 * Created:       January, 2012
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLEPHONEALERTSTATUS_SUPPORT_SERVER
 * Defines if the BLESW_PhoneAlertStatus SERVER Profile implementation 
 * supports the SERVER Role
 * If enabled ( set to 1 ) it enables all the following BLESW_PhoneAlertStatus
 * SERVER Profile configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLEPHONEALERTSTATUS_SUPPORT_SERVER
#define BLEPHONEALERTSTATUS_SUPPORT_SERVER					0
#endif //BLEPHONEALERTSTATUS_SUPPORT_SERVER

#if (BLEPHONEALERTSTATUS_SUPPORT_SERVER== 1)

// Then check dependencies
#include "Services/BlePhoneAlertStatus_Service.h"

// Current Time Service is mandatory
#if (BLE_SUPPORT_PHONEALERTSTATUS_SERVICE == 0)
#error BLE_SUPPORT_PHONEALERTSTATUS_SERVICE shall be enabled when BlePhoneAlertStatus SERVER Role is enabled
#endif //(BLE_SUPPORT_PHONEALERTSTATUS_SERVICE == 0)


/***************************************************************************\
 * OPTIONAL FEATURES FOR PHONE ALERT STATUS SERVER SERVICE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register the BLE PHONE ALERT STATUS Profile in SERVER Role.
 *
 * BLEPHONEALERTSTATUS_SERVER_Register()
 *	This function is used to register and initialise the BLE PHONE ALERT STATUS 
 *  Profile in SERVER role.
 *  It will add the supported PHONE ALERT STATUS Profile Services and 
 *  characteristics into the Attribute database.
 *	This Interface shall be the entry point of a BLE PHONE ALERT STATUS Profile
 *  in SERVER Role.
 *
 * BLEPHONEALERTSTATUS_SUPPORT_SERVER shall be enabled.
 *
 * @param phoneAlertStatusCallback : the BlePhoneAlertStatusServiceCallBack  
 *          callback where the PHONE ALERT STATUS Service's events will be 
 *          received, note that if several profiles implement the 
 *          PHONE ALERT STATUS Service the callback shall be the same.
 *
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPHONEALERTSTATUS_SERVER_Register( 
      BlePhoneAlertStatusServiceCallBack phoneAlertStatusCallback );


/***************************************************************************\
 * OPTIONAL API functions definition BLE TIME PROFILE
\***************************************************************************/

#endif //(BLEPHONEALERTSTATUS_SUPPORT_SERVER== 1)

#endif /*__BLEPHONEALERTSTATUS_SERVER_H*/
