#ifndef __BLETIME_CLIENT_H
#define __BLETIME_CLIENT_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleTime_Client.h
 *
 * Description:   Contains interfaces definition for Time Profile
 *					when the local device operates in the CLIENT role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleTimeTypes.h"
#include "BleGatt.h"

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLETIME_SUPPORT_CLIENT
 * Defines if the BLE TIME Profile implementation supports the 
 * CLIENT Role.
 * If enabled ( set to 1 ) it enables all the following BleTime CLIENT
 * configuration and Application Interface
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_CLIENT
#define BLETIME_SUPPORT_CLIENT								0
#endif //BLETIME_SUPPORT_CLIENT

#if (BLETIME_SUPPORT_CLIENT == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when Time CLIENT Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when Time CLIENT Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when Time CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when Time CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when Time CLIENT Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)


#if (BLE_SECURITY == 1 )
/**
 * BLETIME_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLETIME Profile CLIENT supports 
 * saving the SERVER information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the CLIENT saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the SERVER information.
 * Upon reconnection with the SERVER, as these information are already known
 * then the linkUp process can be simplified.
 * There is 20 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is enabled (0). 
 */
#ifndef BLETIME_SUPPORT_SAVINGINFORMATION	
#define BLETIME_SUPPORT_SAVINGINFORMATION					1
#endif //BLETIME_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_TIME_REMOTEFEATURES						0x72	//2 byte
#define BLEINFOTYPE_TIME_CTSSERVICEHANDLES					0x73	//4 bytes
#define BLEINFOTYPE_TIME_CURRENTTIMEHANDLE					0x74	//2 bytes
#define BLEINFOTYPE_TIME_CURRENTTIMECONFIGHANDLE			0x75	//2 bytes
#define BLEINFOTYPE_TIME_LOCALTIMEINFORMATIONHANDLE			0x76	//2 bytes
#define BLEINFOTYPE_TIME_REFERENCETIMEINFORMATIONHANDLE		0x77	//2 bytes
#define BLEINFOTYPE_TIME_TIMEWITHDSTHANDLE					0x78	//2 bytes
#define BLEINFOTYPE_TIME_REFERENCETIMEUPDATECPHANDLE		0x79	//2 bytes
#define BLEINFOTYPE_TIME_REFERENCETIMEUPDATESTATEHANDLE		0x7A	//2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported force 
// the BLETIME_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLETIME_SUPPORT_SAVINGINFORMATION
#define BLETIME_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 

/**
 * BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION	
 * Defines if the BLETIME Profile CLIENT supports 
 * configuring the remote SERVER to deliver the New Time when it is adjusted.
 * When enabled (set to 1), once connected and linked the CLIENT can call
 * BLETIME_CLIENT_SubscribeNewTimeNotification() to subscribe to the remote 
 * SERVER notification to the CLIENT when the current TIME has change, in this 
 * case the BLETIMECLIENT_EVENT_CURRENTTIME event is received by the CLIENT  
 * upon change.
 * The CLIENT can stop the time change notificcation subscription by calling
 * BLETIME_CLIENT_UnsubscribeNewTimeNotification().
 * Note that when bonded, the subcription state should be saved into the 
 * remote SERVER persistent memory and restored upon reconnexion.
 *
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION	
#define BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION			0
#endif //BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION

#if (BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION ==1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when Time CLIENT Role is enabled\
	and BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION is enabled
#endif //(BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION ==1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION ==1) && (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)
#error BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST shall be enabled when Time CLIENT Role is enabled\
	and BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION is enabled
#endif //(BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION ==1) && (BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST == 0)

/**
 * BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION	
 * Defines if the BLETIME Profile CLIENT supports 
 * retrieving in the remote SERVER the Local Time Information (as long as the
 * remote SERVER hosts this optional characteristic)
 * When enabled (set to 1), once connected and linked the CLIENT can call
 * BLETIME_CLIENT_GetLocalTimeInformation() to retrieve the Local Time
 * Information from the remote TIME SERVER.
 * Note that the information about the support of the Local Time Information
 * characteristic in the TIME SERVER is returned in the linkUp information.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION	
#define BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION			0
#endif //BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION

/**
 * BLETIME_SUPPORT_GETTINGTIMEACCURACY	
 * Defines if the BLETIME Profile CLIENT supports 
 * retrieving in the remote SERVER the Time Accuracy (as long as the
 * remote SERVER hosts this optional characteristic)
 * When enabled (set to 1), once connected and linked the CLIENT can call
 * BLETIME_CLIENT_GetTimeAccuracy() to retrieve the Time Accuracy
 * Information from the remote TIME SERVER.
 * Note that the information about the support of the Time Accuracy
 * characteristic in the TIME SERVER is returned in the linkUp information.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_GETTINGTIMEACCURACY	
#define BLETIME_SUPPORT_GETTINGTIMEACCURACY					0
#endif //BLETIME_SUPPORT_GETTINGTIMEACCURACY

/**
 * BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION
 * Defines if the BLETIME Profile CLIENT supports 
 * retrieving in the remote SERVER the Next DST Change Information (as long as
 * the remote SERVER hosts this optional characteristic)
 * When enabled (set to 1), once connected and linked the CLIENT can call
 * BLETIME_CLIENT_GetNextDSTChangeInformation () to retrieve the Next DST  
 * Change Information from the remote TIME SERVER.
 * Note that the information about the support of the Next DST Change 
 * Information characteristic in the TIME SERVER is returned in the linkUp
 * information.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION	
#define BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION		0
#endif //BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION


/**
 * BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE
 * Defines if the BLETIME Profile CLIENT supports 
 * controlling in the remote SERVER the Reference Time Update Process (as 
 * long as the remote SERVER hosts this optional characteristic)
 * When enabled (set to 1), once connected and linked the CLIENT can call
 * BLETIME_CLIENT_ReadReferenceTimeUpdateState () to retrieve the state of   
 * the Reference Time update in the remote TIME SERVER. As well it can call
 * BLETIME_CLIENT_SendGetReferenceTimeUpdateCommand() to requiere the SERVER
 * to update the reference time and 
 * BLETIME_CLIENT_SendCancelReferenceTimeUpdateCommand() to requiere the 
 * SERVER to cancel an ongoing Reference Time Update
 * Note that the information about the support of the Reference Time Update
 * feature in the TIME SERVER is returned in the linkUp
 * information.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE	
#define BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE		0
#endif //BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE

#if (BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE ==1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be enabled when Time CLIENT Role is enabled\
	and BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE is enabled
#endif //(BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION ==1) && (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)

/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BleTimeClientEvent type
 * Define the different kind of events that could be received by the 
 * BleTimeClientCallBack
 */
typedef	U8	BleTimeClientEvent;

/** BLETIMECLIENT_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleTimeClientLinkUpInfo pointer.
 */
#define BLETIMECLIENT_EVENT_LINKED									0xB0

/** BLETIMECLIENT_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleTimeClientLinkUpInfo pointer.
 */
#define BLETIMECLIENT_EVENT_UNLINKED								0xB1

/** BLETIMECLIENT_EVENT_CURRENTTIME
 * Event received when a current time value has been received.
 * The status field indicates the status of the operation.
 * The parms field indicates the current Time information within a
 * BleTimeClientCurrentTimeInfo pointer.
 */
#define BLETIMECLIENT_EVENT_CURRENTTIME								0xB2

/** BLETIMECLIENT_EVENT_SUBSCRIPTION_RSP
 * Event received when the Subscription of new time notifictaion is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the subscription
 * response is associated within a BleTimeClient pointer.
 */
#define BLETIMECLIENT_EVENT_SUBSCRIPTION_RSP						0xB3

/** BLETIMECLIENT_EVENT_UNSUBSCRIPTION_RSP
 * Event received when the Un-Subscription of new time notifictaion is
 * completed. The status field indicates the status of the operation.
 * The parms field indicates the client for which the unsubscription
 * response is associated within a BleTimeClient pointer.
 */
#define BLETIMECLIENT_EVENT_UNSUBSCRIPTION_RSP						0xB4

/** BLETIMECLIENT_EVENT_LOCALTIMEINFORMATION
 * Event received when the Local Time information has been received
 * The status field indicates the status of the operation.
 * The parms field indicates the Local Time Information within a
 * BleTimeClientLocalTimeInfo pointer.
 */
#define BLETIMECLIENT_EVENT_LOCALTIMEINFORMATION					0xB5

/** BLETIMECLIENT_EVENT_TIMEACCURACY
 * Event received when the Time Accuracy Information has been received
 * The status field indicates the status of the operation.
 * The parms field indicates the Time Accuracy Information within a
 * BleTimeClientTimeAccuracyInfo pointer.
 */
#define BLETIMECLIENT_EVENT_TIMEACCURACY							0xB6

/** BLETIMECLIENT_EVENT_NEXTDSTCHANGEINFORMATION
 * Event received when the Next DST Change Information has been received
 * The status field indicates the status of the operation.
 * The parms field indicates the Next DST Change Information within a
 * BleTimeClientNextDSTChangeInfo pointer.
 */
#define BLETIMECLIENT_EVENT_NEXTDSTCHANGEINFORMATION				0xB7

/** BLETIMECLIENT_EVENT_REFERENCEUPDATESTATEINFORMATION
 * Event received when the Reference Time Update State has been received
 * The status field indicates the status of the operation.
 * The parms field indicates the Reference Time Update State Information 
 * within a BleTimeClientReferenceTimeUpdateState pointer.
 */
#define BLETIMECLIENT_EVENT_REFERENCEUPDATESTATEINFORMATION			0xB8

/**
 * BleTimeClientCallBack
 *	This callback receives the BLE TIME CLIENT profile events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleTimeClientCallBack)(BleTimeClientEvent event,
								BleStatus status,
								void* parms); 

/** 
 * BleTimeClient type
 * Define a local CLIENT, it represents a memory block
 * that should be passed by the application during the 
 * BLETIME_CLIENT_LinkUpTimeProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLETIME_CLIENT_LinkUpTimeProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * CLIENT receive the BLETIMECLIENT_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this CLIENT memory block while 
 * linked with a remote SERVER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local CLIENT could be then passed as parameter to 
 * profile API in order to identify to which linked SERVER the operation  
 * is destinated.
 *
 * A CLIENT is unique per remote Time SERVER, it means 
 * that if this local device is connected to multiple remote SERVER the 
 * application shall register multiple CLIENTs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	U16						serverFeatures;

	U8						value[2];

	AttHandle				CTSStartingHandle;
	AttHandle				CTSEndingHandle;
	AttHandle				currentTimeHandle;
	AttHandle				currentTimeConfigHandle;
	AttHandle				localTimeInfoHandle;
	AttHandle				referenceTimeInfoHandle;

	AttHandle				NDCSStartingHandle;
	AttHandle				NDCSEndingHandle;
	AttHandle				timeToNdcHandle;

	AttHandle				RTUSStartingHandle;
	AttHandle				RTUSEndingHandle;
	AttHandle				rtusCPHandle;
	AttHandle				rtusUpdateStateHandle;

} BleTimeClient;

/** 
 * BleTimeClientRemoteFeatures type
 * Bitfield passed in parms fields of the BleTimeClientLinkUpInfo
 * during the BLETIMECLIENT_EVENT_LINKED event.
 * It indicates if the remote Time SERVER supports the optionnal features
 * like LOCAL TIME INFORMATION or REFERNCE TIME INFORMATION characteristic of
 * CURRENT TIME Service and/or REFERENCE TIME UPDATE Service and/or NEXT DST 
 * CHANGE Service.
 */
typedef U8 BleTimeClientRemoteFeatures;

/** BLETIMECLIENT_REMOTESUPPORT_LOCALTIMEINFORMATION
 * The remote Time SERVER supports the optionnal
 * Local Time Information characteristic within the CURRENT TIME service.
 */
#define	BLETIMECLIENT_REMOTESUPPORT_LOCALTIMEINFORMATION		0x01

/** BLETIMECLIENT_REMOTESUPPORT_REFERENCETIMEINFORMATION
 * The remote Time SERVER supports the optionnal
 * Reference Time Information characteristic within the CURRENT TIME service.
 */
#define	BLETIMECLIENT_REMOTESUPPORT_REFERENCETIMEINFORMATION	0x02

/** BLETIMECLIENT_REMOTESUPPORT_NEXTDSTCHANGE
 * The remote Time SERVER supports the optionnal
 * NEXT DST CHANGE Service and characteristics.
 */
#define	BLETIMECLIENT_REMOTESUPPORT_NEXTDSTCHANGE				0x04

/** BLETIMECLIENT_REMOTESUPPORT_REFERENCETIMEUPDATE
 * The remote Time SERVER supports the optionnal
 * REFERENCE TIME UPDATE Service and characteristics.
 */
#define	BLETIMECLIENT_REMOTESUPPORT_REFERENCETIMEUPDATE			0x08

/** 
 * BleTimeClientLinkUpInfo type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_LINKED event.
 * It indicates :
 * bleTimeClient : The CLIENT from which the event is comming
 * connHandle : The connection handle from which the Link Up information
 *	is coming
 * remoteFeatures : A bitfield that indicates if the SERVER supports
 *	some optional features.
 *  Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleTimeClient						*bleTimeClient;
	U16									connHandle;
	BleTimeClientRemoteFeatures			remoteFeatures;

} BleTimeClientLinkUpInfo;

/** 
 * BleTimeClientLinkMode type
 * Type passed during the BLETIME_CLIENT_LinkUpTimeProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLETIME_CLIENT_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLETIME_CLIENT_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 * Note that if BLETIME_SUPPORT_SAVINGINFORMATION is DISABLED, 
 * a linkUP with the BLETIME_CLIENT_LINKUP_RESTORE mode will always 
 * failed.
 */
typedef U8 BleTimeClientLinkMode;

#define BLETIME_CLIENT_LINKUP_COMPLETE					1
#define BLETIME_CLIENT_LINKUP_RESTORE					2


/** 
 * BleTimeClientCurrentTimeInfo type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_CURRENTIME event.
 * It indicates :
 * bleTimeClient : The client from which the event is comming
 * connHandle : The connection handle from which the currentTime information
 *	is coming
 * currentDayDateTime: the currentTime information provided by the SERVER;
 *                     Only applicable if event status is BLESTATUS_SUCCESS
 * fraction256 : if available, the fraction of second in fraction256;
 *               Only applicable if event status is BLESTATUS_SUCCESS
 * adjustReason : if available, the adjust reason of the current Time;
 *                Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleTimeClient						*bleTimeClient;
	U16									connHandle;
	BleDayDateTime						currentDayDateTime;
	BleTimeFraction256					fraction256;
	U8									adjustReason;

} BleTimeClientCurrentTimeInfo;

/** 
 * BleTimeClientLocalTimeInfo type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_LOCALTIMEINFORMATION event.
 * It indicates :
 * bleTimeClient : The client from which the event is comming
 * connHandle : The connection handle from which the local time information
 *	is coming
 * timeZone: the current TimeZone information provided by the server;
 *           Only applicable if event status is BLESTATUS_SUCCESS.
 * dstOffset : the current DST offset information provided by the server;
 *             Only applicable if event status is BLESTATUS_SUCCESS.
 */
typedef struct {

	BleTimeClient						*bleTimeClient;
	U16									connHandle;
	BleTimeZone							timeZone;
	BleTimeDSTOffset					dstOffset;

} BleTimeClientLocalTimeInfo;

/** 
 * BleTimeClientTimeAccuracyInfo type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_TIMEACCURACY event.
 * It indicates :
 * bleTimeClient : The client from which the event is comming
 * connHandle : The connection handle from which the time accuracy information
 *	is coming
 * updateSource: the update source;
 *               Only applicable if event status is BLESTATUS_SUCCESS
 * accuracy : Accuracy (drift) of time information in steps of 1/8 of a second
 *           (125ms) compared to a reference time source. Valid range from 0 
 *           to 253 (0s to 31.5s). A value of 254 means Accuracy is out of
 *           range (> 31.5s). A value of 255 means Accuracy is unknown.
 *           Only applicable if event status is BLESTATUS_SUCCESS
 * daysSinceUpdate : Days Since Update;
 *                   Only applicable if event status is BLESTATUS_SUCCESS
 * hoursSinceUpdate : Hours Since Update, If Days Since Update = 255, then
 *                    Hours Since Update shall also be set to 255;
 *                    Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleTimeClient						*bleTimeClient;
	U16									connHandle;
	BleTimeSource						updateSource;
	U8									accuracy;
	U8									daysSinceUpdate;
	U8									hoursSinceUpdate;

} BleTimeClientTimeAccuracyInfo;

/** 
 * BleTimeClientNextDSTChangeInfo type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_NEXTDSTCHANGEINFORMATION event.
 * It indicates :
 * bleTimeClient : The client from which the event is comming
 * connHandle : The connection handle from which the Next DST change information
 *	is coming
 * timeOfNextDST : the exact date of the next DST change;
 *             Only applicable if event status is BLESTATUS_SUCCESS
 * nextDST : the Next DST value;
 *           Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleTimeClient						*bleTimeClient;
	U16									connHandle;
	BleDateTime							timeOfNextDST;
	BleTimeDSTOffset					nextDST;

} BleTimeClientNextDSTChangeInfo;

/** 
 * BleTimeReferenceTimeUpdateCurrentState type
 * It indicates the current state of the reference time Update
 * Possible values are:
 */
typedef U8 	BleTimeReferenceTimeUpdateCurrentState;

#define BLETIMEREFERENCETIMEUPDATE_STATE_IDLE						0
#define BLETIMEREFERENCETIMEUPDATE_STATE_UPDATEPENDING				1

/** 
 * BleTimeReferenceTimeUpdateResult type
 * It indicates the current result of the reference time Update
 * Possible values are:
 */
typedef U8	BleTimeReferenceTimeUpdateResult;

#define BLETIMEREFERENCETIMEUPDATE_RESULT_SUCCESSFUL				0
#define BLETIMEREFERENCETIMEUPDATE_RESULT_CANCELED					1
#define BLETIMEREFERENCETIMEUPDATE_RESULT_NOREFERENCECONNECTION		2
#define BLETIMEREFERENCETIMEUPDATE_RESULT_REFERENCEERROR			3
#define BLETIMEREFERENCETIMEUPDATE_RESULT_TIMEOUT					4
#define BLETIMEREFERENCETIMEUPDATE_RESULT_NOUPDATEAFTERRESET		5


/** 
 * BleTimeClientReferenceTimeUpdateState type
 * Structure passed in parameter in the BleTimeClientCallBack
 * parms fields during the BLETIMECLIENT_EVENT_REFERENCEUPDATESTATEINFORMATION
 * event.
 * It indicates :
 * bleTimeClient : The client from which the event is comming
 * connHandle : The connection handle from which the Next DST change 
 *	information is coming
 * referenceTimeUpdateState: the current state of the reference time update;
 *               Only applicable if event status is BLESTATUS_SUCCESS
 * referenceTimeUpdateResult : the current result of the reference time 
 *               update; Only applicable if event status is BLESTATUS_SUCCESS
 */
typedef struct {

	BleTimeClient								*bleTimeClient;
	U16											connHandle;
	BleTimeReferenceTimeUpdateCurrentState		referenceTimeUpdateState;
	BleTimeReferenceTimeUpdateResult			referenceTimeUpdateResult;

} BleTimeClientReferenceTimeUpdateState;

/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/
/** Init the Time CLIENT profile
 *
 * BLETIME_CLIENT_Init()
 *	This function is the entry point of the Time CLIENT profile, it 
 *	inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLETIME_SUPPORT_CLIENT shall be enabled
 *
 * @param callback: The BleTimeClientCallBack in which the CLIENTs 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLETIME CLIENT Profile is already 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */ 
BleStatus BLETIME_CLIENT_Init(BleTimeClientCallBack callback);

/** De-Init the Time CLIENT Profile
 *
 * BLETIME_CLIENT_Deinit
 *	This function is used to De-Init the Time CLIENT Profile.
 *
 * BLETIME_SUPPORT_CLIENT shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_Deinit( void );

/** Link Up the Time Profile with the specified remote Device
 *
 * BLETIME_CLIENT_LinkUpTimeProfile()
 *	This function is used to link-Up the Time profile with the 
 *  specified remote device.
 *  It will try to discover the Time profile in the remote device 
 *  using the given CLIENT.
 *
 * BLETIME_SUPPORT_CLIENT shall be enabled
 *
 * @param bleTimeClient: A valid BleTimeClient CLIENT to link-up
 *					with a remote Time SERVER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this SERVER could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  CLIENT the operation is destinated.   
 * @param connHandle: The connection Handle on which the link up between this
 *		 bleTimeClient and the remote Time SERVER profile
 *		 shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLETIME_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode is 
 *		BLETIME_CLIENT_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLETIME_CLIENT_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLETIME_CLIENT_LINKUP_RESTORE, it means that 
 *		BLETIME_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Time SERVER.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_LinkUpTimeProfile(
								BleTimeClient *bleTimeClient,
								U16 connHandle,
								BleTimeClientLinkMode mode);


/** Un Link the Time Profile with the specified linked CLIENT
 *
 * BLETIME_CLIENT_UnLinkTimeProfile()
 *	This function is used to UnLink the Time Profile with the 
 *  specified previously linked CLIENT.
 *  This CLIENT will not receive any more information from the linked 
 *	SERVER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_TIME_* from persistent memory
 *
 * BLETIME_SUPPORT_CLIENT shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the CLIENT memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_UnLinkTimeProfile(
		BleTimeClient *bleTimeClient);

/** Get the current time from a linked SERVER
 *
 * BLETIME_CLIENT_GetCurrentTime()
 *	This function is used to Get the current time on the remote SERVER 
 *  defined by the previously linked CLIENT.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLETIMECLIENT_EVENT_CURRENTTIME event will be received in the 
 *		callback with the status of the operation and the time information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_GetCurrentTime( BleTimeClient *bleTimeClient );

#if (BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION == 1) 

/** Configure the TIME SERVER to deliver the New Time when it is adjusted
 *
 * BLETIME_CLIENT_SubscribeNewTimeNotification()
 *	This function is used by the given linked TIME CLIENT to configure the  
 *  remote TIME SERVER to deliver new current Time when it is changed.
 *  When new time information is received the BLETIMECLIENT_EVENT_CURRENTTIME
 *  event will be receive din the callback with the adjusted time information.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLETIMECLIENT_EVENT_SUBSCRIPTION_RSP event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_SubscribeNewTimeNotification(
									BleTimeClient *bleTimeClient );


/** Configure the TIME SERVER to not deliver the New Time when it is adjusted
 *
 * BLETIME_CLIENT_UnSubscribeNewTimeNotification()
 *	This function is used by the given linked TIME CLIENT to configure the  
 *  remote TIME SERVER to not deliver new current Time when it is changed.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLETIMECLIENT_EVENT_UNSUBSCRIPTION_RSP event will be received in the 
 *		callback with the status of the operation.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_UnSubscribeNewTimeNotification(
									BleTimeClient *bleTimeClient );
#endif //(BLETIME_SUPPORT_RECEIVENEWTIMENOTIFICATION == 1)


#if (BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION == 1) 

/** Retrieve in the TIME SERVER the Local time information
 *
 * BLETIME_CLIENT_GetLocalTimeInformation()
 *	This function is used by the given linked TIME CLIENT to retrieve in the  
 *  remote TIME SERVER the current Local Time Information.
 *  When local time information is received the 
 *  BLETIMECLIENT_EVENT_LOCALTIMEINFORMATION event will be received in the 
 *  callback with the local time information.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once completed
 *		the BLETIMECLIENT_EVENT_LOCALTIMEINFORMATION event will be received in the 
 *		callback with the Local Time Information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Local Time Information characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_GetLocalTimeInformation(
									BleTimeClient *bleTimeClient );

#endif //(BLETIME_SUPPORT_GETTINGLOCALTIMEINFORMATION == 1)

#if (BLETIME_SUPPORT_GETTINGTIMEACCURACY == 1) 
/** Retrieve in the TIME SERVER the time accuracy
 *
 * BLETIME_CLIENT_GetTimeAccuracy()
 *	This function is used by the given linked TIME CLIENT to retrieve in the  
 *  remote TIME SERVER the Time Accuracy Information.
 *  When time accuracy information is received the 
 *  BLETIMECLIENT_EVENT_TIMEACCURACY event will be received in the 
 *  callback with the time accuracy information.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_GETTINGTIMEACCURACY shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once completed
 *		the BLETIMECLIENT_EVENT_TIMEACCURACY event will be received in the 
 *		callback with the Time Accuracy Information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Time Accuracy characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_GetTimeAccuracy(
									BleTimeClient *bleTimeClient );
#endif //(BLETIME_SUPPORT_GETTINGTIMEACCURACY == 1)

#if (BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION == 1) 
/** Retrieve in the TIME SERVER the Next DST Change Information 
 *
 * BLETIME_CLIENT_GetNextDSTChangeInformation()
 *	This function is used by the given linked TIME CLIENT to retrieve in the  
 *  remote TIME SERVER the Next DST Change Information.
 *  When Next DST Change Information is received the 
 *  BLETIMECLIENT_EVENT_NEXTDSTCHANGEINFORMATION event will be received in the 
 *  callback with the Next DST Change Information.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once 
 *      completed the BLETIMECLIENT_EVENT_NEXTDSTCHANGEINFORMATION event will
 *      be received in the callback with the Next DST Change Information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Next DST Change Information characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_GetNextDSTChangeInformation(
									BleTimeClient *bleTimeClient );
#endif //(BLETIME_SUPPORT_GETTINGNEXTDSTCHANGEINFORMATION == 1)

#if (BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE == 1)
/** Retrieve in the TIME SERVER the Reference Time Update State
 *
 * BLETIME_CLIENT_GetReferenceTimeUpdateState()
 *	This function is used by the given linked TIME CLIENT to retrieve in the  
 *  remote TIME SERVER the Reference Time Update State.
 *  When Reference Time Update State is received the 
 *  BLETIMECLIENT_EVENT_REFERENCEUPDATESTATEINFORMATION event will be received 
 *  in the callback with the Reference Time Update State Information.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_PENDING indicates that the operation as started, once 
 *      completed the BLETIMECLIENT_EVENT_REFERENCEUPDATESTATEINFORMATION 
 *      event will be received in the callback with the reference Update state 
 *      Information.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Reference Time Update service or state 
 *      characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_GetReferenceTimeUpdateState(
									BleTimeClient *bleTimeClient );


/** Request the TIME SERVER to update reference Time
 *
 * BLETIME_CLIENT_SendGetReferenceTimeUpdateCommand()
 *	This function is used by the given linked TIME CLIENT to request the  
 *  remote TIME SERVER to update its Reference Time.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the Request should have been sent to the
 *     TIME SERVER.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Reference Time Update service or state 
 *      characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_SendGetReferenceTimeUpdateCommand(
									BleTimeClient *bleTimeClient );


/** Request the TIME SERVER to cancel ongoing reference Time update procedure
 *
 * BLETIME_CLIENT_SendGetReferenceTimeUpdateCommand()
 *	This function is used by the given linked TIME CLIENT to request the  
 *  remote TIME SERVER to cancel any ongoing reference Time update procedure.
 *   
 * BLETIME_SUPPORT_CLIENT shall be enabled
 * BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE shall be enabled
 *
 * @param bleTimeClient: A linked Time CLIENT. 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCES indicates that the Requset should have been sent to the
 *     TIME SERVER.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked or the remote linked SERVER
 *      does not support the Reference Time Update service or state 
 *      characteristic.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other TIME Profile Operation is in Progress, wait for this
 *  operation completion 
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (the TIME Profile seems not been initialized).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_CLIENT_SendCancelReferenceTimeUpdateCommand(
									BleTimeClient *bleTimeClient );
#endif //(BLETIME_SUPPORT_CONTROLLINGREFERENCETIMEUPDATE == 1)


#endif //(BLETIME_SUPPORT_CLIENT== 1)

#endif //__BLETIME_CLIENT_H
