#ifndef __BLETIME_SERVER_H
#define __BLETIME_SERVER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleTime_Server.h
 *
 * Description:   Contains interfaces definitions for Time 
 *					Profile when the local device operates in the SERVER
 *					role.
 * 
 * Created:       October, 2011
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"


/***************************************************************************\
 *	CONFIGURATION
\***************************************************************************/

/**
 * BLETIME_SUPPORT_SERVER
 * Defines if the BLESW_Time SERVER Profile implementation supports the 
 * SERVER Role
 * If enabled ( set to 1 ) it enables all the following BLESW_Time
 * SERVER Profile configurations and Application Interfaces.
 * The default value for this option is disabled (0).
 */
#ifndef BLETIME_SUPPORT_SERVER
#define BLETIME_SUPPORT_SERVER					0
#endif //BLETIME_SUPPORT_SERVER

#if (BLETIME_SUPPORT_SERVER== 1)

// Then check dependencies
#include "Services/BleCurrentTime_Service.h"
#include "Services/BleNextDSTChange_Service.h"
#include "Services/BleReferenceTimeUpdate_Service.h"

// Current Time Service is mandatory
#if (BLE_SUPPORT_CURRENTTIME_SERVICE == 0)
#error BLE_SUPPORT_CURRENTTIME_SERVICE shall be enabled when BleTime SERVER Role is enabled
#endif //(BLE_SUPPORT_CURRENTTIME_SERVICE == 0)


/***************************************************************************\
 * OPTIONAL FEATURES FOR TIME SERVER SERVICE
\***************************************************************************/

/***************************************************************************\
 *	CONSTANTS definition
\***************************************************************************/

/***************************************************************************\
 *	TYPES definition
\***************************************************************************/


/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register the BLE TIME Profile in SERVER Role.
 *
 * BLETIME_SERVER_Register()
 *	This function is used to register and initialise the BLE TIME 
 *  Profile in SERVER role.
 *  It will add the supported TIME Profile Services and characteristics 
 *  into the Attribute database.
 *	This Interface shall be the entry point of a BLE TIME Profile
 *  in SERVER Role.
 *
 * BLETIME_SUPPORT_SERVER shall be enabled.
 *
 * @param currentTimeCallback : the BleCurrentTimeServiceCallBack callback 
 *          where the CURRENTTIME Service's events will be received, note that
 *          if several profiles implement the CURRENTTIME Service the callback
 *          shall be the same.
 *
 * @param nextDSTChangeCallback : the BleNextDSTChangeServiceCallBack callback 
 *          where the NEXTDSTCHANGE Service's event will be received, note 
 *          that if several profiles implement the NEXTDSTCHANGE Service the 
 *          callback shall be the same.
 *			Supporting the NEXTDSTCHANGE Service is optional:
 *			- if BLE_SUPPORT_NEXTDSTCHANGE_SERVICE is defined to 0 (means 
 *          service is not supported by this device, then this parameter is
 *          ignored and may be set to (BleNextDSTChangeServiceCallBack)0;
 *			- if BLE_SUPPORT_NEXTDSTCHANGE_SERVICE is defined to 1 (means 
 *          service can be supported by this device, the application may 
 *          decide to not register now this service anyway by setting 
 *          nextDSTChangeCallback to (BleNextDSTChangeServiceCallBack)0;
 *
 * @param referenceTimeUpdateCallback : the BleReferenceTimeUpdateCallback  
 *          callback where the REFERENCETIMEUPADTE Service's event will be  
 *          received, note that if several profiles implement the   
 *          REFERENCETIMEUPADTE Service the callback shall be the same.
 *			Supporting the REFERENCETIMEUPADTE Service is optional:
 *			- if BLE_SUPPORT_REFERENCETIMEUPDATE_SERVICE is defined to 0 (means 
 *          service is not supported by this device, then this parameter is
 *          ignored and may be set to (referenceTimeUpdateCallback)0;
 *			- if BLE_SUPPORT_REFERENCETIMEUPDATE_SERVICE is defined to 1 (means 
 *          service can be supported by this device, the application may 
 *          decide to not register now this service anyway by setting 
 *          referenceTimeUpdateCallback to (referenceTimeUpdateCallback)0;
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLETIME_SERVER_Register( 
      BleCurrentTimeServiceCallBack currentTimeCallback ,
      BleNextDSTChangeServiceCallBack nextDSTChangeCallback,
      BleReferenceTimeUpdateServiceCallBack referenceTimeUpdateCallback );


/***************************************************************************\
 * OPTIONAL API functions definition BLE TIME PROFILE
\***************************************************************************/

#endif //(BLETIME_SUPPORT_SERVER== 1)

#endif /*__BLETIME_SERVER_H*/
