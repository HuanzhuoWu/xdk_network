#ifndef __BLEPROXIMITY_REPORTER_H
#define __BLEPROXIMITY_REPORTER_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleProximity_Reporter.h
 *
 * Description:   Contains interfaces routines for Proximity Profile
 *					when the local device operates in the REPORTER role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"

/***************************************************************************\
 *	Macro and constant definition
\***************************************************************************/

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEPROXIMITY_SUPPORT_REPORTER
 * Defines if the BLE PROXIMITY Profile implementation supports the 
 * REPORTER Role
 * If enabled ( set to 1 ) it enables all the following BleProximity
 * REPORTER configuration and Application Interface.
 * If enabled the LINK LOSS Service shall be enabled.
 * The default value for this option is disabled (0).
 */
#ifndef BLEPROXIMITY_SUPPORT_REPORTER
#define BLEPROXIMITY_SUPPORT_REPORTER								0
#endif //BLEPROXIMITY_SUPPORT_REPORTER

#if (BLEPROXIMITY_SUPPORT_REPORTER== 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

/**
 * BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING
 * Defines if the BLE PROXIMITY Profile implementation supports reporting  
 * the connection TX Power Level
 * If enabled ( set to 1 ) it enables the Tx Power level service and
 * calculate the current connection TX POWER LEVEL.
 * If enabled the TX POWER LEVEL Service and the IMMEDIATE ALERT Service
 * shall be enabled.
 * The default value for this option is disabled (0).
 */
#ifndef BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING
#define BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING						0
#endif //BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING

// Then check dependencies
#include "Services/BleLinkLoss_Service.h"

// LinkLoss Service is mandatory
#if (BLE_SUPPORT_LINKLOSS_SERVICE == 0)
#error BLE_SUPPORT_LINKLOSS_SERVICE shall be enabled when BleProximity REPORTER Role is enabled
#endif //(BLE_SUPPORT_LINKLOSS_SERVICE == 0)



// Immediate Alert Service is mandatory if Tx Power Level reporting is enabled
// Tx Power Level Service is mandatory if Tx Power Level reporting is enabled
#if (BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING == 1)

// Then check dependencies
#include "Services/BleImmediateAlert_Service.h"
#include "Services/BleTxPowerLevel_Service.h"

#if (BLE_SUPPORT_IMMEDIATEALERT_SERVICE == 0)
#error BLE_SUPPORT_IMMEDIATEALERT_SERVICE shall be enabled when BleProximity REPORTER Role is enabled
#endif //(BLE_SUPPORT_IMMEDIATEALERT_SERVICE == 0)
#if (BLE_SUPPORT_TXPOWERLEVEL_SERVICE == 0)
#error BLE_SUPPORT_TXPOWERLEVEL_SERVICE shall be enabled when BleProximity REPORTER Role is enabled
#endif //(BLE_SUPPORT_TXPOWERLEVEL_SERVICE== 0)
#endif //(BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING == 1)

/***************************************************************************\
 * CONSTANTS
\***************************************************************************/



/***************************************************************************\
 *	Type definition
\***************************************************************************/



/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/

/** Register a BLE PROXIMITY REPORTER.
 *
 * BLEPROXIMITY_REPORTER_Register()
 *	This function is used to register and initialise a BLE PROXIMITY 
 * REPORTER role.
 * It will add the supported proximity characteristics into the Attribute
 * database.
 *
 * BLEPROXIMITY_SUPPORT_REPORTER shall be enabled.
 *
 * @param linkLossCallback: The callback that will receive the LINK LOSS
 *			service events.
 * @param immediateAlertCallback: The callback that will receive the 
 *			IMMEDIATE ALERT service events.
 *			it is used only when BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING
 *			is set to enabled, otherwise it is ignored.
 *			This callback shall be 	the same for all PROFILES that use the 
 *			IMMEDIATE ALERT service.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 * @author Alexandre GIMARD
 */
#if (BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING == 1)
BleStatus BLEPROXIMITY_REPORTER_Register( BleLinkLossCallBack linkLossCallback,
										 BleImmediateAlertCallBack immediateAlertCallback);
#else // so (BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING == 0)
BleStatus BLEPROXIMITY_REPORTER_Register( BleLinkLossCallBack linkLossCallback,
										 void* immediateAlertCallback);
#endif //(BLEPROXIMITY_SUPPORT_TX_POWER_LEVEL_REPORTING == 1)

#endif //(BLEPROXIMITY_SUPPORT_REPORTER== 1)

#endif //__BLEPROXIMITY_REPORTER_H
