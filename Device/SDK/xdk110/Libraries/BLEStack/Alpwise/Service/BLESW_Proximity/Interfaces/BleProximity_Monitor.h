#ifndef __BLEPROXIMITY_MONITOR_H
#define __BLEPROXIMITY_MONITOR_H
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BleProximity_Monitor.h
 *
 * Description:   Contains interfaces definition for Proximity Profile
 *					when the local device operates in the MONITOR role.
 * 
 * Created:       December, 2010
 * Version:		  0.1
 *
 * File Revision: $Rev: 2721 $
 * $Project$
 *
 *
 ****************************************************************************/

#include "BleTypes.h"
#include "BleGatt.h"
#include "services/BleLinkLoss_Service.h"
#include "services/BleImmediateAlert_Service.h"

/***************************************************************************
 *	Overidible CONSTANT
 ***************************************************************************/
/**
 * BLEPROXIMITY_SUPPORT_MONITOR
 * Defines if the BLE PROXIMITY Profile implementation supports the 
 * MONITOR Role.
 * If enabled ( set to 1 ) it enables all the following BleProximity MONITOR
 * configuration and Application Interface
 * The default value for this option is disabled (0). 
 */
#ifndef BLEPROXIMITY_SUPPORT_MONITOR
#define BLEPROXIMITY_SUPPORT_MONITOR								0
#endif //BLEPROXIMITY_SUPPORT_MONITOR

#if (BLEPROXIMITY_SUPPORT_MONITOR == 1)
/***************************************************************************\
 * OPTIONAL FEATURES
\***************************************************************************/

#if (GENERIC_ATTRIBUTE_PROFILE == 0)
#error GENERIC_ATTRIBUTE_PROFILE shall be enabled when Proximity MONITOR Role is enabled
#endif //(GENERIC_ATTRIBUTE_PROFILE == 0)

#if (ATT_ROLE_CLIENT == 0)
#error ATT_ROLE_CLIENT shall be enabled when Proximity MONITOR Role is enabled
#endif //(ATT_ROLE_CLIENT == 0)

#if (BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)
#error BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY shall be enabled when Proximity MONITOR Role is enabled
#endif //(BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)
#error BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY shall be enabled when Proximity MONITOR Role is enabled
#endif //(BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY == 0)

#if (BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE == 0)
#error BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE shall be enabled when Proximity MONITOR Role is enabled
#endif //(BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE == 0)

// Write without response is mandatory to write the remote immediate alert level service
#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE shall be enabled when Proximity MONITOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

// Write is mandatory to write the remote tx power level service
#if (BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)
#error BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE shall be enabled when Proximity MONITOR Role is enabled
#endif //(BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE == 0)

#if (BLE_SECURITY == 1 )
/**
 * BLEPROXIMITY_SUPPORT_SAVINGINFORMATION	
 * Defines if the BLEPROXIMITY Profile MONITOR supports 
 * saving the REPORTER information when both devices are bonded. 
 * When enabled (set to 1), once connected and bonded the MONITOR saves into  
 * the persistent memory thanks to a call to SYSTEM_SetPersistentInformation
 * the REPORTER information.
 * Upon reconnection with the REPORTER, as these information are already known
 * then the linkUp process can be simplified.
 * There is 19 bytes of information required to be saved into the persistent 
 * memory.
 *
 * BLE_SECURITY shall be enabled.
 *
 * The default value for this option is disabled (0). 
 */
#ifndef BLEPROXIMITY_SUPPORT_SAVINGINFORMATION	
#define BLEPROXIMITY_SUPPORT_SAVINGINFORMATION					0
#endif //BLEPROXIMITY_SUPPORT_SAVINGINFORMATION

#define BLEINFOTYPE_PROXIMITY_REMOTEFEATURES					0x60	//1 byte
#define BLEINFOTYPE_PROXIMITY_LLSERVICEHANDLES					0x61	//4 bytes
#define BLEINFOTYPE_PROXIMITY_LLALERTHANDLE						0x62	//2 bytes
#define BLEINFOTYPE_PROXIMITY_TXPSERVICEHANDLES					0x63	//4 bytes
#define BLEINFOTYPE_PROXIMITY_TXPLEVELHANDLE					0x64	//2 bytes
#define BLEINFOTYPE_PROXIMITY_IASERVICEHANDLES					0x65	//4 bytes
#define BLEINFOTYPE_PROXIMITY_IAALERTHANDLE						0x66	//2 bytes

#endif //(BLE_SECURITY == 1 )


// Otherwise if BLE_SECURITY is not supported and 
// BLEPROXIMITY_SUPPORT_BATTERYLEVELSTATEALERT is not supported force 
// the BLEPROXIMITY_SUPPORT_SAVINGINFORMATION to 0
#if (BLE_SECURITY == 0)
#undef BLEPROXIMITY_SUPPORT_SAVINGINFORMATION
#define BLEPROXIMITY_SUPPORT_SAVINGINFORMATION					0
#endif //( BLE_SECURITY == 0) 

/** BLEPROXIMITYMONITOR_RSSI_TIMER
 * Once connected, define the number of milliseconds to wait before two 
 * retrieves of the current connection RSSI value.
 * The default value for this option is fixed to 1000ms.
 */
#ifndef BLEPROXIMITYMONITOR_RSSI_TIMER
#define BLEPROXIMITYMONITOR_RSSI_TIMER								1000
#endif //BLEPROXIMITYMONITOR_RSSI_TIMER

/***************************************************************************\
 *	Type definition
\***************************************************************************/

/** 
 * BleProximityMonitorEvent type
 * Define the different kind of events that could be received by the 
 * BleProximityMonitorCallBack
 */
typedef	U8	BleProximityMonitorEvent;

/** BLEPROXIMITYMONITOR_EVENT_LINKED
 * Event received when the LinkUP is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleProximityMonitorLinkUpInfo pointer.
 */
#define BLEPROXIMITYMONITOR_EVENT_LINKED								0x96

/** BLEPROXIMITYMONITOR_EVENT_UNLINKED
 * Event received when the UnLink is completed, the status field indicates 
 * the status of the operation.
 * The parms field indicates the link up information within a
 * BleProximityMonitorLinkUpInfo pointer.
 */
#define BLEPROXIMITYMONITOR_EVENT_UNLINKED								0x97

/** BLEPROXIMITYMONITOR_EVENT_PATHLOSS
 * Event received when a path Loss calculation has been done.
 * The parms field indicates the path loss information within a
 * BleProximityMonitorPathLossInfo pointer.
 */
#define BLEPROXIMITYMONITOR_EVENT_PATHLOSS								0x98


/**
 * BleProximityMonitorCallBack
 *	This callback receives the BLE PROXIMITY MONITOR profile events. 
 *  Each of these events can be associated with a defined status and parameter.
 *	The callback is executed during the stack context,  be careful to not doing 
 *	heavy process in this function.
 */
typedef void (*BleProximityMonitorCallBack)(BleProximityMonitorEvent event,
										 BleStatus status,
										 void* parms); 

/** 
 * BleProximityMonitor type
 * Define a local MONITOR, it represent a memory block
 * that should be passed by the application during the 
 * BLEPROXIMITY_MONITOR_LinkUpProximityProfile() API call.
 *
 * This memory Block shall be allocated by the application prior to call
 * BLEPROXIMITY_MONITOR_LinkUpProximityProfile() API ,and  
 * shall be valid while the Profile and the remote device are linked together. 
 * So, this memory block could be reused by the application once this  
 * MONITOR receive the BLEPROXIMITYMONITOR_EVENT_UNLINKED event.
 *
 * The Profile will initialize and use this MONITOR memory block while 
 * linked with a remote REPORTER. It is used internally by the profile and  
 * shall not be accessed by the application.
 * A pointer to this local MONITOR could be then passed as parameter to 
 * profile API in order to identify to which linked REPORTER the operation  
 * is destinated.
 *
 * A MONITOR is unique per remote Proximity REPORTER, it means 
 * that if this local device is connected to multiple remote REPORTER the 
 * application shall register multiple MONITORs.
 *
 * All the structure fields are PRIVATE and shall not be accessed
 * directly
 */
typedef struct {
	/** 
	 *	The Following fields are for INTERNAL USE ONLY
	 * Do not address them directly
	 */
	BleGattCommand			gattCommand;

	U16						connHandle;
	U8						linked;
	U8						state;
	U8						reporterFeatures;

	AttHandle				llServiceStartingHandle;
	AttHandle				llServiceEndingHandle;
	AttHandle				alertLevelHandle;
	U8						llValue[1];

	AttHandle				txpServiceStartingHandle;
	AttHandle				txpServiceEndingHandle;
	AttHandle				powerLevelHandle;

	AttHandle				iaServiceStartingHandle;
	AttHandle				iaServiceEndingHandle;
	AttHandle				iaLevelHandle;
	U8						iaValue[1];

	U8						set; //1bit used
	S16						threshold;
	U8						below; //2 bit used
	U8						over; //2 bit used
	U8						lastWas; //2 bit used (0: not done, 1: last was below, 2: last was over)

	U8						monitored;
	S8						txPowerLevel;
	S16						pathLoss;
	S8						rssi;

} BleProximityMonitor;

/** 
 * BleProximityMonitorRemoteFeatures type
 * Bitfield passed in parms fields of the BleProximityMonitorLinkUpInfo
 * during the BLEPROXIMITYMONITOR_EVENT_LINKED event.
 * It indicates if the remote proximity REPORTER supports the optionnal
 * TX POWER LEVEL reporting and TX POWER LEVEL threshold immediate alert.
 */
typedef U8 BleProximityMonitorRemoteFeatures;

/** BLEPROXIMITYMONITOR_REMOTESUPPORT_TXPOWER
 * The remote proximity REPORTER supports the optionnal
 * TX POWER LEVEL reporting and TX POWER LEVEL threshold immediate alert.
 */
#define	BLEPROXIMITYMONITOR_REMOTESUPPORT_TXPOWER					0x01

/** 
 * BleProximityMonitorLinkUpInfo type
 * Structure passed in parameter in the BleProximityMonitorCallBack
 * parms fields during the BLEPROXIMITYMONITOR_EVENT_LINKED event.
 * It indicates :
 * bleProximityMonitor : The MONITOR from which the event is comming
 * connHandle : The connection handle from which the Link Up information
 *	is coming
 * remoteFeatures : A bitfield that indicates if the reporter supports
 *	some optional features.
 */
typedef struct {

	BleProximityMonitor						*bleProximityMonitor;
	U16										connHandle;
	BleProximityMonitorRemoteFeatures		remoteFeatures;

} BleProximityMonitorLinkUpInfo;

/** 
 * BleProximityMonitorLinkMode type
 * Type passed during the BLEPROXIMITY_MONITOR_LinkUpProximityProfile
 * API call.
 * It defines the type of desired linkUP, if the linkUp procedure shall be
 * a complete one (BLEPROXIMITY_MONITOR_LINKUP_COMPLETE) with remote
 * service and characteritic discovery, or if the linkUp information are 
 * restored (BLEPROXIMITY_MONITOR_LINKUP_RESTORE) from previous 
 * complete linkUP from persistent memory.
 * Note that if BLEPROXIMITY_SUPPORT_SAVINGINFORMATION is DISABLED, 
 * a linkUP with the BLEPROXIMITY_MONITOR_LINKUP_RESTORE mode will always 
 * failed.
 */
typedef U8 BleProximityMonitorLinkMode;

#define BLEPROXIMITY_MONITOR_LINKUP_COMPLETE			1
#define BLEPROXIMITY_MONITOR_LINKUP_RESTORE				2


/** 
 * BleProximityMonitorPathLossInfo type
 * Structure passed in parameter in the BleProximityMonitorCallBack
 * parms fields during the BLEPROXIMITYMONITOR_EVENT_PATHLOSS event.
 * It indicates :
 * bleProximityMonitor : The monitor from which the event is comming
 * connHandle : The connection handle from which the pathLoss information
 *	is coming
 * pathLoss : A signed interger representing the path Loss value for
 *  this connection.
 */
typedef struct {

	BleProximityMonitor						*bleProximityMonitor;
	U16										connHandle;
	S16										pathLoss;

} BleProximityMonitorPathLossInfo;

/****************************************************************************\
 *	APPLICATION INTERFACE functions definition
\****************************************************************************/
/** Init the Proximity MONITOR profile
 *
 * BLEPROXIMITY_MONITOR_Init()
 *	This function is the entry point of the Proximity MONITOR profile, it 
 *	inits the profile memory and mechanism.
 *  It should be called during the application startup after the stack
 *	Initialisation.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @param callback: The BleProximityMonitorCallBack in which the MONITORs 
 *				 events will be received.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCESS indicates that the operation succeeded.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed.
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPROXIMITY MONITOR Profile is already 
 *		initialized).
 *
 * @author Alexandre GIMARD
 */ 
BleStatus BLEPROXIMITY_MONITOR_Init(BleProximityMonitorCallBack callback);

/** De-Init the Proximity MONITOR Profile
 *
 * BLEPROXIMITY_SUPPORT_MONITOR
 *	This function is used to De-Init the Proximity MONITOR Profile.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeded
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_Deinit( void );

/** Link Up the Proximity Profile with the specified remote Device
 *
 * BLEPROXIMITY_MONITOR_LinkUpProximityProfile()
 *	This function is used to link-Up the Proximity profile with the 
 *  specified remote device.
 *  It will try to discover the Proximity profile in the remote device 
 *  using the given MONITOR.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @param bleProximityMonitor: A valid BleProximityMonitor MONITOR to link-up
 *					with a remote Proximity REPORTER.
 *                  This memory block is allocated by the application prior to
 *                  call this API and shall stay valid while the devices
 *                  are linked together. 
 *                  It is up to profile internal mechanism to manage this
 *                  memory block.
 *					Once linked, this MONITOR could be then passed as parameter  
 *                  to profile API in order to identify to which linked 
 *                  REPORTER the operation is destinated.  
 * @param connHandle: The connection Handle on which the link up between this
 *		 bleProximityMonitor and the remote Proximity REPORTER profile
 *		 shall be done.
 * @param mode: The desired link-up mode, restore from persistent memory or
 *		complete link-up.
 *
 * @return The status of the operation:
 *	- BLESTATUS_PENDING indicates that the operation succeeded, once completed
 *		the BLEPROXIMITY_EVENT_LINKED event will be received in the callback
 *		with the status of the operation. This status is returned only if mode is 
 *		BLEPROXIMITY_MONITOR_LINKUP_COMPLETE.
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded and is completed
 *		This status is returned only if mode is 
 *		BLEPROXIMITY_MONITOR_LINKUP_RESTORE. It means that the persistent 
 *		information has been restored and the devices are now linked.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. In case of mode
 *		is BLEPROXIMITY_MONITOR_LINKUP_RESTORE, it means that 
 *		BLEPROXIMITY_SUPPORT_SAVINGINFORMATION is not enabled or if enabled 
 *		that the remote device is not bonded or the persistent memory does not 
 *		contains enough information about the remote Proximity REPORTER.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other Proximity Profile Operation is in Progress, wait for this
 *  operation completion
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (connHandle does not match with a specified link).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_LinkUpProximityProfile(
								BleProximityMonitor *bleProximityMonitor,
								U16 connHandle,
								BleProximityMonitorLinkMode mode);


/** Un Link the Proximity Profile with the specified linked MONITOR
 *
 * BLEPROXIMITY_MONITOR_UnLinkProximityProfile()
 *	This function is used to UnLink the Proximity Profile with the 
 *  specified previously linked MONITOR.
 *  This MONITOR will not receive any more information from the linked 
 *	REPORTER. 
 *	The Bonded information may also been removed from the 
 *  persistent memory by the application removing any 
 *  BLEINFOTYPE_PROXIMITY_* from persistent memory
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @param bleProximityMonitor: A linked Proximity MONITOR 
 
 * @return The status of the operation:
 *
 * 	- BLESTATUS_SUCCESS indicates that the operation succeeded.
 *		the monitor memory could be reused.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed. 
 *		It could be because the device is not linked.
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_UnLinkProximityProfile(
		BleProximityMonitor *bleProximityMonitor);


/** Set the remote Alert level for link Loss Alert
 *
 * BLEPROXIMITY_MONITOR_SetLinkLossAlertLevel()
  *	This function set the link loss alert level on the linked
 *	PROXIMITY REPORTER. This will be the alert level that will be triggered
 *	in case of the link is disconnected du to a LINK LOSS.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @param bleProximityMonitor: A linked Proximity MONITOR
 * @param linkLossAlertLevel : the desired link loss Alert level
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeds.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other proximity Operation is in Progress, wait for this
 *  operation completion
 *
 *	- BLESTATUS_FAILED indicates that the operation failed,
 *			(bleProximityMonitor is not linked).
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPROXIMITY MONITOR Profile is not initialised).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_SetLinkLossAlertLevel(
	BleProximityMonitor		*bleProximityMonitor,
	BleLinkLossAlertLevel	linkLossAlertlevel);


/** Start to monitor the connection path Loss.
 *
 * BLEPROXIMITY_MONITOR_StartPathLossMonitoring()
 *	This function start the monitoring of the connection between the given
 *  monitor and the Remote PROXIMITY REPORTER.
 *  The monitor will start to retreive the RSSI of teh connection every
 *  the defined BLEPROXIMITYMONITOR_RSSI_TIMER milliseconds, and compute the
 *  path loss.
 *	Once computed the path loss information will be reported to the
 *  application thanks to the BLEPROXIMITYMONITOR_EVENT_PATHLOSS event.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @see : BLEPROXIMITY_MONITOR_StopPathLossMonitoring()
 *
 * @param bleProximityMonitor: A linked Proximity MONITOR.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeds.
 *
 *	- BLESTATUS_FAILED indicates that the operation has failed; It may be 
 *  because the remote REPORTER is not linked or does not support TX POWER 
 *  LEVEL reporting.
 *
 *	- BLESTATUS_BUSY indicates that the operation has failed, because 
 *	an other proximity Operation is in Progress, wait for this
 *  operation completion.
 *
 *	- BLESTATUS_FAILED indicates that the operation failed,
 *			(bleProximityMonitor is not linked).
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPROXIMITY MONITOR Profile is not initialised).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_StartPathLossMonitoring(
				BleProximityMonitor		*bleProximityMonitor );


/** Stop to monitor the connection path Loss.
 *
 * BLEPROXIMITY_MONITOR_StopPathLossMonitoring()
 *	This function stop the previously started path loss monitoring.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @see : BLEPROXIMITY_MONITOR_StartPathLossMonitoring()
 *
 * @param bleProximityMonitor: A linked Proximity Monitor.
 *
 * @return none
 *
 * @author Alexandre GIMARD
 */
void BLEPROXIMITY_MONITOR_StopPathLossMonitoring(
			BleProximityMonitor		*bleProximityMonitor);


/** Set the remote Alert level for thresold
 *
 * BLEPROXIMITY_MONITOR_SetThreshold()
 *	This function set the threshold and the associated alert level on 
 *	the linked PROXIMITY REPORTER.
 *  The monitor will automatically check the Path Loss for the threshold
 * and set the given alerts on the PROXIMIMTY REPORTER .
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @see : BLEPROXIMITY_MONITOR_ClearThreshold()
 *
 * @param bleProximityMonitor: A linked Proximity MONITOR
 * @param threshold: A path loss threshold to MONITOR
 * @param belowImmediateAlertlevel : The alert level to raise on the 
 *	PROXIMIMTY REPORTER when the current connection path loss is below
 *  the threshold.
 * @param overImmediateAlertlevel : The alert level to raise on the 
 *  PROXIMIMTY REPORTER when the current connection path loss is over 
 *  the threshold.
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeds.
 *
 *	- BLESTATUS_FAILED indicates that the operation failed,
 *			(bleProximityMonitor is not linked).
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPROXIMITY MONITOR Profile is not initialised).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_SetThreshold (
	BleProximityMonitor			*bleProximityMonitor,
	S16							threshold,
	BleImmediateAlertAlertLevel	belowImmediateAlertlevel,
	BleImmediateAlertAlertLevel	overImmediateAlertlevel
	);


/** Clear the remote Alert level for thresold
 *
 * BLEPROXIMITY_MONITOR_ClearThreshold()
 *	This function clear the threshold and alert for the given monitor,
 *	the monitor will stop to check the monitor connection path loss,
 *  and raise alerts.
 *
 * BLEPROXIMITY_SUPPORT_MONITOR shall be enabled
 *
 * @see : BLEPROXIMITY_MONITOR_SetThreshold()
 *
 * @param bleProximityMonitor: A linked Proximity MONITOR
 *
 * @return The status of the operation:
 *	- BLESTATUS_SUCCESS indicates that the operation succeeds.
 *
 *	- BLESTATUS_FAILED indicates that the operation failed,
 *			(bleProximityMonitor is not linked).
 *
 *	if BLE_PARMS_CHECK is set to 1:
 *	- BLESTATUS_INVALID_PARMS indicates that the function has failed because
 *		an invalid parameter (BLEPROXIMITY MONITOR Profile is not initialised).
 *
 * @author Alexandre GIMARD
 */
BleStatus BLEPROXIMITY_MONITOR_ClearThreshold (
	BleProximityMonitor			*bleProximityMonitor
	);

#endif //(BLEPROXIMITY_SUPPORT_MONITOR== 1)

#endif //__BLEPROXIMITY_MONITOR_H
