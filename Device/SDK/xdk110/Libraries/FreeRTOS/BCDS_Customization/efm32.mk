##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

#Build chain settings	
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra \
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections \
-Wa,-ahlms=$(@:.o=.lst)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID)
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

# A variable which evaluates to a list of all source code files (*.c)
BCDS_FREERTOS_SOURCE_FILES = \
	$(BCDS_FREERTOS_SOURCE_PATH)/croutine.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/list.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/queue.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/tasks.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/timers.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/$(BCDS_FREERTOS_SOURCE_PORT_PATH)/MemMang/heap_4.c \
	$(BCDS_FREERTOS_SOURCE_PATH)/$(BCDS_FREERTOS_SOURCE_PORT_PATH)/ARM_CM3/port.c    

# Objects
BCDS_FREERTOS_SOURCE_OBJECT_FILES = \
	$(patsubst $(BCDS_FREERTOS_SOURCE_PATH)/%.c, %.o, $(BCDS_FREERTOS_SOURCE_FILES))

BCDS_FREERTOS_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_FREERTOS_DEBUG_OBJECT_PATH)/, $(BCDS_FREERTOS_SOURCE_OBJECT_FILES))

BCDS_FREERTOS_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_FREERTOS_DEBUG_OBJECT_PATH)/, $(BCDS_FREERTOS_SOURCE_OBJECT_FILES:.o=.d))
	
BCDS_FREERTOS_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_FREERTOS_RELEASE_OBJECT_PATH)/, $(BCDS_FREERTOS_SOURCE_OBJECT_FILES)) 

BCDS_FREERTOS_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_FREERTOS_RELEASE_OBJECT_PATH)/, $(BCDS_FREERTOS_SOURCE_OBJECT_FILES:.o=.d))
	
##################### Build Targets #########################
.PHONY: debug
debug: $(BCDS_FREERTOS_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_FREERTOS_RELEASE_LIB)
	
$(BCDS_FREERTOS_DEBUG_LIB): $(BCDS_FREERTOS_DEBUG_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build FreeRTOS for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_FREERTOS_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_FREERTOS_RELEASE_LIB): $(BCDS_FREERTOS_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build FreeRTOS for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_FREERTOS_RELEASE_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_FREERTOS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_FREERTOS_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
		-D FREE_RTOS_SYSTEM_HEAP_SIZE_KB=$(FREE_RTOS_SYSTEM_HEAP_SIZE_KB) \
		-I . $(BCDS_FREERTOS_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) $< -o $@

$(BCDS_FREERTOS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_FREERTOS_SOURCE_PATH)/%.c 
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
		-D FREE_RTOS_SYSTEM_HEAP_SIZE_KB=$(FREE_RTOS_SYSTEM_HEAP_SIZE_KB) \
		-I . $(BCDS_FREERTOS_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) $< -o $@

-include $(BCDS_FREERTOS_DEBUG_DEPENDENCY_FILES) $(BCDS_FREERTOS_RELEASE_DEPENDENCY_FILES)

.PHONY: clean		
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_FREERTOS_DEBUG_PATH) $(BCDS_FREERTOS_RELEASE_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_FREERTOS_OBJECT_FILES)
	@echo ""
	
	@echo "Debug Objects"
	@echo $(BCDS_FREERTOS_DEBUG_OBJECT_PATH)
	@echo $(BCDS_FREERTOS_DEBUG_OBJECT_FILES)
