# Copyright (c) 2015 - 2016 Robert Bosch GmbH and its subsidiaries.
# All rights reserved.
# This program is Bosch Connected Device Solution Source.
# It can be used, redistributed and/or modified under the terms the terms of
# the BCDS License v1.0 (BCDSL).

include Settings.mk

LOWER_CASE = $(subst A,a,$(subst B,b,$(subst C,c,$(subst D,d,$(subst E,e,$(subst F,f,$(subst G,g,$(subst H,h,$(subst I,i,$(subst J,j,$(subst \
K,k,$(subst L,l,$(subst M,m,$(subst N,n,$(subst O,o,$(subst P,p,$(subst Q,q,$(subst R,r,$(subst S,s,$(subst T,t,$(subst U,u,$(subst \
V,v,$(subst W,w,$(subst X,x,$(subst Y,y,$(subst Z,z,$1))))))))))))))))))))))))))

#Build chain settings	
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra \
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections \
-Wa,-ahlms=$(@:.o=.lst) -D$(BCDS_DEVICE_ID)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g -DDEBUG
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS)
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

# A variable which evaluates to a list of all source code files (*.c)
BCDS_EMLIB_SOURCE_FILES =\
    $(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_assert.c \
    $(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_adc.c \
    $(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_burtc.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_cmu.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_msc.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_dma.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_ebi.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_emu.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_gpio.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_int.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_i2c.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_leuart.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_usart.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_rtc.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_system.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_letimer.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_timer.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_lcd.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_rmu.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/emlib/src/em_wdog.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbd.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbdch9.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbhal.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbdep.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbdint.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/usb/src/em_usbtimer.c \
	$(BCDS_EMLIB_LIB_SOURCE_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Source/system_$(call LOWER_CASE,$(BCDS_DEVICE_TYPE)).c

# Objects
BCDS_EMLIB_SOURCE_OBJECT_FILES = \
	$(patsubst $(BCDS_EMLIB_LIB_SOURCE_PATH)/%.c, %.o, $(BCDS_EMLIB_SOURCE_FILES))

BCDS_EMLIB_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_EMLIB_DEBUG_OBJECT_PATH)/, $(BCDS_EMLIB_SOURCE_OBJECT_FILES))
	
BCDS_EMLIB_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_EMLIB_DEBUG_OBJECT_PATH)/, $(BCDS_EMLIB_SOURCE_OBJECT_FILES:.o=.d))	
	
BCDS_EMLIB_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_EMLIB_RELEASE_OBJECT_PATH)/, $(BCDS_EMLIB_SOURCE_OBJECT_FILES))

BCDS_EMLIB_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_EMLIB_RELEASE_OBJECT_PATH)/, $(BCDS_EMLIB_SOURCE_OBJECT_FILES:.o=.d))		

##################### Build Targets #########################
.PHONY: debug
debug: $(BCDS_EMLIB_DEBUG_LIB)

.PHONY: release
release: $(BCDS_EMLIB_RELEASE_LIB)

all: debug release

############# Generate EMLIB Library for BCDS ###################
$(BCDS_EMLIB_DEBUG_LIB): $(BCDS_EMLIB_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build EMLIB for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_EMLIB_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

############# Generate EMLIB Library for BCDS ###################
$(BCDS_EMLIB_RELEASE_LIB): $(BCDS_EMLIB_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build EMLIB for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_EMLIB_RELEASE_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_EMLIB_DEBUG_OBJECT_PATH)/%.o: $(BCDS_EMLIB_LIB_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
           -I . $(BCDS_EMLIB_INCLUDES) $< -o $@

$(BCDS_EMLIB_RELEASE_OBJECT_PATH)/%.o: $(BCDS_EMLIB_LIB_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_EMLIB_INCLUDES) $< -o $@

-include $(BCDS_EMLIB_DEBUG_DEPENDENCY_FILES) $(BCDS_EMLIB_RELEASE_DEPENDENCY_FILES)

.PHONY: clean		   
clean:
	@echo "Cleaning project"	
	@rm -rf $(BCDS_EMLIB_DEBUG_PATH) $(BCDS_EMLIB_RELEASE_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_EMLIB_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_EMLIB_DEBUG_OBJECT_PATH)
	@echo $(BCDS_EMLIB_DEBUG_OBJECT_FILES)
