##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

# Compiler settings    
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra -Wstrict-prototypes -D$(BCDS_DEVICE_ID) -D$(BCDS_SYSTEM_STARTUP_METHOD) \
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections \
 -D$(BCDS_HW_VERSION)   
endif
export BCDS_CFLAGS_DEBUG_COMMON = $(BCDS_CFLAGS_COMMON) -O0 -g 
export BCDS_CFLAGS_RELEASE_COMMON = $(BCDS_CFLAGS_COMMON) -Os -DNDEBUG

ASMFLAGS += -x assembler-with-cpp -Wall -Wextra -mcpu=cortex-m3 -mthumb

LIBS = -Wl,--start-group -lgcc -lc -lm -lnosys   -Wl,--end-group

ARFLAGS = -cr

LINT_CONFIG = \
	+d$(BCDS_DEVICE_TYPE) +dASSERT_FILENAME $(BCDS_SERVAL_CONFIG_LINT) $(BCDS_CYASLL_CONFIG_LINT) \
	+d$(BCDS_TARGET_PLATFORM) +d$(BCDS_DEVICE_ID) \
	
#The static libraries of the platform and third party sources are grouped here. Inorder to scan the libraries
#for undefined reference again and again, the libraries are listed between the --start-group and --end-group.
BCDS_DEBUG_LIBS_GROUP = -Wl,--start-group $(BCDS_LIBS_DEBUG) $(BCDS_THIRD_PARTY_LIBS) -Wl,--end-group
BCDS_RELEASE_LIBS_GROUP = -Wl,--start-group $(BCDS_LIBS_RELEASE) $(BCDS_THIRD_PARTY_LIBS) -Wl,--end-group

# Source files
BCDS_XDK_COMMON_SOURCE_FILES += \
	$(BCDS_XDK_SOURCE_DIR)/SystemStartup.c \
	$(BCDS_XDK_SOURCE_DIR)/BoardHandle.c \
	$(BCDS_XDK_SOURCE_DIR)/SensorHandle.c \
	$(BCDS_XDK_SOURCE_DIR)/GpioConfig.c \
	$(BCDS_XDK_SOURCE_DIR)/BoardInitialization.c \
	$(BCDS_XDK_SOURCE_DIR)/UsbResetUtility.c \
	$(BCDS_XDK_SOURCE_DIR)/XdkVersion.c \
	
BCDS_XDK_COMMON_OBJECT_FILES = \
	$(patsubst $(BCDS_XDK_SOURCE_DIR)/%.c, %.o, $(BCDS_XDK_COMMON_SOURCE_FILES))

BCDS_XDK_COMMON_LINT_FILES = \
	$(patsubst $(BCDS_XDK_SOURCE_DIR)/%.c, %.lob, $(BCDS_XDK_COMMON_SOURCE_FILES))
	
BCDS_XDK_COMMON_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_XDK_COMMON_DEBUG_OBJECT_PATH)/,$(BCDS_XDK_COMMON_OBJECT_FILES))
	
BCDS_XDK_COMMON_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_XDK_COMMON_DEBUG_OBJECT_PATH)/, $(BCDS_XDK_COMMON_OBJECT_FILES:.o=.d))
	
BCDS_XDK_COMMON_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_XDK_COMMON_RELEASE_OBJECT_PATH)/,$(BCDS_XDK_COMMON_OBJECT_FILES))

BCDS_XDK_COMMON_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_XDK_COMMON_RELEASE_OBJECT_PATH)/, $(BCDS_XDK_COMMON_OBJECT_FILES:.o=.d))

BCDS_XDK_COMMON_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_XDK_COMMON_DEBUG_LINT_PATH)/, $(BCDS_XDK_COMMON_LINT_FILES))
	
######################## Build Targets #######################################
.PHONY: debug
debug: $(BCDS_XDK_COMMON_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_XDK_COMMON_RELEASE_LIB)

############# Generate Library for XDK COMMON ###################
$(BCDS_XDK_COMMON_DEBUG_LIB): $(BCDS_XDK_COMMON_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build XDK common for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_XDK_COMMON_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_XDK_COMMON_RELEASE_LIB): $(BCDS_XDK_COMMON_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build XDK common  for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_XDK_COMMON_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"
	
# compile C files
$(BCDS_XDK_COMMON_DEBUG_OBJECT_PATH)/%.o: $(BCDS_XDK_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_DEBUG_COMMON) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID)  \
	       -I . $(BCDS_XDK_INCLUDES)  $(BCDS_EXTERNAL_INCLUDES) $< -o $@

$(BCDS_XDK_COMMON_RELEASE_OBJECT_PATH)/%.o: $(BCDS_XDK_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_RELEASE_COMMON) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) \
           -I . $(BCDS_XDK_INCLUDES)  $(BCDS_EXTERNAL_INCLUDES) $< -o $@

.PHONY: lint		   
lint: $(BCDS_XDK_COMMON_DEBUG_LINT_FILES)
	@echo "Lint End"

$(BCDS_XDK_COMMON_DEBUG_LINT_PATH)/%.lob: $(BCDS_XDK_SOURCE_DIR)/%.c
	@echo "===================================="
	@mkdir -p $(@D)
	@echo $@
	@$(LINT_EXE) $(LINT_CONFIG) $(BCDS_EXTERNAL_INCLUDES_LINT) $(BCDS_LINT_CONFIG) $(BCDS_XDK_INCLUDES_LINT) $(BCDS_LINT_CONFIG_FILE) +dDBG_ASSERT_FILENAME $< -oo[$@]
	@echo "===================================="
	
-include $(BCDS_XDK_COMMON_DEBUG_DEPENDENCY_FILES) $(BCDS_XDK_COMMON_RELEASE_DEPENDENCY_FILES)

.PHONY: clean
clean:
	@echo "Cleaning project"	
	@rm -rf $(BCDS_XDK_COMMON_DEBUG_PATH)
	@rm -rf $(BCDS_XDK_COMMON_RELEASE_PATH)

#Clean libraries of platform and third party code
.PHONY: clean_libraries
clean_libraries:
	@echo "Cleaning libraries of platform and third party sources"
	@rm -rf $(BCDS_LIBS_DEBUG_PATH) $(BCDS_LIBS_RELEASE_PATH)
	
# Clean only the archives of platform and third party code
.PHONY: quick_clean
quick_clean:
	@echo "Cleaning only the archives..."
	@$(RMDIRS) $(BCDS_LIBS_DEBUG) $(BCDS_LIBS_RELEASE)

.PHONY: cleanlint	
cleanlint:
	@echo "Cleaning lint output files"
	@rm -rf $(BCDS_XDK_COMMON_DEBUG_LINT_PATH)	
	