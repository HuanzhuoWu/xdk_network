/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */

#ifndef PTHREAD_H_
#define PTHREAD_H_

/* public interface declaration ********************************************* */
#include <stdio.h>
#include "Serval_Defines.h"
#include "Serval_Types.h"
#include "Serval_Log.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
/* public type and macro definitions */

#define SERVAL_THREAD_GET_ID() (uint32_t)(0)

static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");

#define SERVAL_SLEEP_MS(ms)  vTaskDelay((portTickType) ms / portTICK_RATE_MS)

/* defines for SERVAL_MUTEX_SET_MODE(mode) */
#define SERVAL_MUTEX_MODE_ENABLE    0
#define SERVAL_MUTEX_MODE_DISABLE   1
#define SERVAL_MUTEX_MODE_TEST      2

#define ENABLE_MUTEX 1

#if ENABLE_MUTEX && ((SERVAL_LOG_LEVEL) >= (SERVAL_LOG_LEVEL_DEBUG))
#define SERVAL_MUTEX_DEBUG 1
#endif

#define SERVAL_THREAD_RUN(func,param) \
        xTaskCreate(func, (const char * const) "SERVAL_THREAD", configMINIMAL_STACK_SIZE,param, 4, NULL);

typedef void (*SERVAL_THREAD_FUNC_T)(void *);
typedef void *SERVAL_THREAD_FUNC_PARAM_T;

#if ENABLE_MUTEX

#define SERVAL_MUTEX_INIT()
#define SERVAL_MUTEX_SET_MODE(mode)
#define SERVAL_MUTEX_INSTANCE(mutex) \
    xSemaphoreHandle mutex
#define SERVAL_MUTEX_EXTERN(mutex) \
   extern xSemaphoreHandle mutex
#define SERVAL_MUTEX_CREATE(mutex) \
    mutex = xSemaphoreCreateMutex()
#define SERVAL_MUTEX_LOCK(mutex) \
    xSemaphoreTake(mutex, portMAX_DELAY)
#define SERVAL_MUTEX_UNLOCK(mutex) \
    xSemaphoreGive(mutex)
#define SERVAL_MUTEX_GET_ERROR_COUNT(mutex) \
   (0)
#define SERVAL_MUTEX_GET_SUM_ERROR_COUNT() \
   (0)
#define SERVAL_MUTEX_IS_TRACE_LOCK() \
   (1)
#define SERVAL_MUTEX_SET_NO_TRACE(mutex,noTrace)

#else

#define SERVAL_MUTEX_INIT()
#define SERVAL_MUTEX_SET_MODE(mode)
#define SERVAL_MUTEX_INSTANCE(mutex)
#define SERVAL_MUTEX_EXTERN(mutex)
#define SERVAL_MUTEX_CREATE(mutex)
#define SERVAL_MUTEX_LOCK(mutex)
#define SERVAL_MUTEX_UNLOCK(mutex)
#define SERVAL_MUTEX_GET_ERROR_COUNT(mutex) (0)
#define SERVAL_MUTEX_GET_SUM_ERROR_COUNT() 	(0)
#define SERVAL_MUTEX_IS_TRACE_LOCK() (1)
#define SERVAL_MUTEX_SET_NO_TRACE(mutex,noTrace)

#endif /* SERVAL_MUTEX_DEBUG */

#endif /* PTHREAD_H_ */

