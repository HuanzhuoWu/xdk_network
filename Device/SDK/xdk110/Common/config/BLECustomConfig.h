/******************************************************************************/
/**
 * This software is copyrighted by Alpwise, 2016.
 * The use of this software is subject to the XDK SDK EULA
 */
/****************************************************************************
 *
 * File:          BLECustomConfig.h
 *
 * Description:   Contains stack specific configuration.
 * 
 * Created:       June, 2008
 * Version:       0.1
 *
 * File Revision: $Rev: 2811 $
 * $Project$
 * trademark and other intellectual property rights.
 *
 ****************************************************************************/

/**@file BLECustomConfig.h
 * @section BLECustomConfig.h description
 * 
 * This file allows the application to fine tune the STACK and PROFILES 
 * configuration to fit the most as possible with the application requested 
 * features.
 *
 * This file is included within the whole STACK and PROFILES prior any other 
 * header files, so configuration defined/overrided in this file will be   
 * supersede any other CORE STACK and PROFILES configuration.
 *
 * So it override:
 * - The CORE STACK configuration defined in bleConfig.h
 * - The ATTribute protocol configuration defined in attConfig.h
 * - Any SERVICE configuration defined in SERVICEs interfaces
 * - Any PROFILE configuration defined in PROFILEs interfaces
 *      ( including GATT and GAP PROFILEs)
 * - maybe the STACK default types (U8, U16, ...) defined in bleTypes.h
 * 
 */

#ifndef BLECUSTOMCONFIG_H_
#define BLECUSTOMCONFIG_H_

/***************************************************************************\
 * PLATFORM Options
\***************************************************************************/

/**
 * @defgroup BLECustom BTLE
 * @brief Configuration for the Bluetooth Low Energy Stack
 * @ingroup Configuration
 *
 * @{
 * @brief  This file Contains stack specific configuration
 */
// Platform specific

#define BLE_PROTECT_ISR_FUNCTION_CALL                           1
#define BLE_USE_RESTRICTED_LOCAL_MEMORY                         1
#define BLETYPES_ALREADY_DEFINED                                0
#define BLECONTROLLER_NEED_SPECIFIC_INIT                        1
#define BLECONTROLLER_USED                       BLECONTROLLER_EM

#define BLE_PUBLIC_ADDRESS_GETTER_CALLBACK                      1

#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
/**
 * @brief Custom function pointer type definition for the get public BLE address
 *  callback interface.
 *
 * @param [in/out] addressPointer: Pointer to a BD_ADDR structure that could hold the valid address.
 * @return NULL in case of error or the same pointer as addressPointer.
 */
typedef void* (*BLE_getPublicAddressCallback_t)(void* addressPointer);

/**
 * @brief Register a callback function to get the public BLE address.
 *
 * @param [in] getPublicAddress: user specified callback function.
 *
 * @retval BLESTATUS_INVALID_PARMS in case the argument is NULL.
 * @retval BLESTATUS_SUCCESS in case of successful callback registration.
 */
extern uint8_t BLERADIO_RegisterGetPublicAddressCallback(BLE_getPublicAddressCallback_t getPublicAddress);

/**
 * @brief Query the validity status of the BLE public address.
 *
 * @return Non-zero in case there is a valid, configured BLE public address, zero otherwise.
 */
extern uint8_t BLERADIO_IsPublicAddressValid(void);

#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */

//place the constant declaration in code area
#define CONST_DECL                                              const

/***************************************************************************\
 * CORE STACK Options
\***************************************************************************/

#define BLE_USERPAGE_PATCH                                      1 // Patch for workbench release V1.5.X and V1.6.0.
#define BLE_ROLE_SCANNER                                        0
#define BLE_ROLE_ADVERTISER                                     1
#define BLE_ADVERTSING_TX_POWER_SUPPORT                         0
#define BLE_PARMS_CHECK                                         1
#define BLEERROR_HANDLER                                        0
#define BLE_CONNECTION_SUPPORT                                  1
#define BLE_RANDOM_ADDRESSING_SUPPORT                           1
#define BLE_SECURITY                                            1
#define BLE_SM_DYNAMIC_IO_CAPABILITIES                          0
#define BLE_SM_SIGNATURE_SUPPORT                                0
#define BLE_WHITE_LIST_SUPPORT                                  0
#define BLE_ENABLE_TEST_COMMANDS                                0
#define BLE_USE_HOST_CHANNEL_CLASSIFICATION                     0
#define BLE_ENABLE_VENDOR_SPECIFIC                              1
#define BLEL2CAP_ENABLE_API                                     0

#define SM_IO_CAPABILITIES                  SM_IO_NOINPUTNOOUTPUT

/***************************************************************************\
 * ATT Options
\***************************************************************************/

#define ATTRIBUTE_PROTOCOL                                      1
#define ATT_ROLE_CLIENT                                         0

/***************************************************************************\
 * GATT Options
\***************************************************************************/

#define GENERIC_ATTRIBUTE_PROFILE                               0

#define BLEGATT_SUPPORT_ALL_SERVICE_DISCOVERY                   0
#define BLEGATT_SUPPORT_SINGLE_SERVICE_DISCOVERY                1
#define BLEGATT_SUPPORT_RELATIONSHIP_DISCOVERY                  0
#define BLEGATT_SUPPORT_SINGLE_CHARACTERISTIC_DISCOVERY         0
#define BLEGATT_SUPPORT_ALL_CHARACTERISTIC_DISCOVERY            1
#define BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_HANDLE     0
#define BLEGATT_SUPPORT_READ_CHARACTERISTIC_VALUE_BY_TYPE       1
#define BLEGATT_SUPPORT_READ_LONG_CHARACTERISTIC_VALUE          0
#define BLEGATT_SUPPORT_GET_CHARACTERISTIC_DESCRIPTOR_LIST      0
#define BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_WITHOUT_RESPONSE   0
#define BLEGATT_SUPPORT_WRITE_CHARACTERISTIC_VALUE              1
#define BLEGATT_SUPPORT_WRITE_LONG_CHARACTERISTIC_VALUE         0

/***************************************************************************\
 * LOGGING Options
\***************************************************************************/

#define BLE_PRINT_DEBUG_TRACES                                  0
#define ATT_DEBUG_TRACES                                        0
#define SMP_DEBUG_TRACES                                        0
#if (BLE_PRINT_DEBUG_TRACES == 1)
#define BLEHCI_HANDLER                                          0
#endif //(BLE_PRINT_DEBUG_TRACES == 1)

/***************************************************************************\
 * SERVICES Options
\***************************************************************************/
//They will be defined in the Service custom configuration based on this one

/***************************************************************************\
 * PROFILES Options
\***************************************************************************/

#define BLEALPWDATAEXCHANGE_SUPPORT_SERVER						1
#define BLE_SUPPORT_ALPWDATAEXCHANGE_SERVICE					1

#endif /* BLECUSTOMCONFIG_H_ */

/**@}*/
