/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BCDS_BATTERYMONITORCONFIG_H_
#define BCDS_BATTERYMONITORCONFIG_H_

#define BATTERY_CHARGE_PORT                     (gpioPortD)         /**< GPIO port of pin for battery charging */
#define BATTERY_CHARGE_PIN                      (UINT32_C(7))       /**< GPIO pin for battery charging */
#define BATTERY_CHARGE_MODE_ENABLE              (gpioModeInput)     /**< GPIO pin for battery charging input mode */
#define BATTERY_CHARGE_MODE_DISABLE             (gpioModeDisabled)  /**< GPIO pin for battery charging GPIO mode disabled */
#define BATTERY_CHARGE_DOUT_ENABLE              (UINT16_C(1))       /**< Enable GPIO pin for battery charging */
#define BATTERY_CHARGE_DOUT_DISABLE             (UINT16_C(0))       /**< Disable GPIO pin from battery charging */

#define BATTERY_CRITICAL_THRESHOLD_LL           (UINT16_C(3500))    /**< Low threshold level for critical battery level */
#define BATTERY_CRITICAL_THRESHOLD_HL           (UINT16_C(3550))    /**< High threshold level for critical battery level */
#define BATTERY_LOW_THRESHOLD_LL                (UINT16_C(3600))    /**< Low level threshold for low battery level */
#define BATTERY_LOW_THRESHOLD_HL                (UINT16_C(3700))    /**< High level threshold for low battery level */
#define BATTERY_HIGH_THRESHOLD_LL               (UINT16_C(3900))    /**< Low level threshold for high battery level */
#define BATTERY_HIGH_THRESHOLD_HL               (UINT16_C(4000))    /**< High level threshold for high battery level */
#define BATTERY_HIGHEST_THRESHOLD               (UINT16_C(4200))    /**< Highest Battery Level */

#define BATTERY_MONITORING_RATE_MS              (UINT32_C(5000))    /**< Battery monitoring rate */
#define BATTERY_STABLITY_COUNT_LIMIT            (UINT8_C(2))        /**< Stability count limit */

/**< Macro to configure(enable/disable) the interrupt for rising edge */
#define BATTERY_ENABLE_INT_RISING_EDGE          (UINT16_C(1))

/**< Macro to configure(enable/disable) the interrupt for falling edge */
#define BATTERY_ENABLE_INT_FALLING_EDGE         (UINT16_C(1))

/**< Macro to configure (enable/disable) the interrupt after the PTD configuration is completed.
 * See GPIO_IntDisable() and GPIO_IntEnable() for more info.
 */
#define BATTERY_ENABLE_INT_AFTER_CONFIG         (UINT32_C(1))

#define POWER_CHARGER_PORT                 	(gpioPortA)
#define POWER_CHARGER_PIN                  	(UINT32_C(9))
#define POWER_CHARGER_ENABLE          	   	(gpioModePushPull)
#define POWER_CHARGER_DISABLE         	   	(gpioModeDisabled)
#define POWER_CHARGER_DOUT_ENABLE          	(UINT16_C(1))
#define POWER_CHARGER_DOUT_DISABLE         	(UINT16_C(0))

#define POWER_BATTERY_PORT                 	(gpioPortA)
#define POWER_BATTERY_PIN                  	(UINT32_C(10))
#define POWER_BATTERY_ENABLE          		(gpioModePushPull)
#define POWER_BATTERY_DISABLE         		(gpioModeDisabled)
#define POWER_BATTERY_DOUT_ENABLE          	(UINT16_C(1))
#define POWER_BATTERY_DOUT_DISABLE         	(UINT16_C(0))

#define POWER_RESET_PORT                   (gpioPortB)
#define POWER_RESET_PIN                    (UINT32_C(11))
#define POWER_RESET_ENABLE            	   (gpioModePushPull)
#define POWER_RESET_DISABLE           	   (gpioModeDisabled)
#define POWER_RESET_DOUT_ENABLE            (UINT16_C(1))
#define POWER_RESET_DOUT_DISABLE           (UINT16_C(0))

#define POWER_DEREGISTER_TIME_OUT           (UINT32_C(6000))

#endif /* BCDS_BATTERYMONITORCONFIG_H_ */