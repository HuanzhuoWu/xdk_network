/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
 /**  @file
 *
 *  This module defines the hardware/device specific configuration
 *
 * ****************************************************************************/

/**
 * @defgroup GpioHandles GPIO Handles
 * @brief Handles used for the GPIO
 * @ingroup xdk_peripherals
 *
 * @{
 * @brief  This Module contains XDK device handles 
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef XDK110_XDKBOARDHANDLE_H_
#define XDK110_XDKBOARDHANDLE_H_

/* public interface declaration ********************************************** */
#include "PTD_portDriver_ph.h"
#include "gpio.h"
#include "I2C_ih.h"

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */

/* Led Handle definitions */
/**
 * @brief GPIO handle for Red LED
 */
extern GPIO_handle_tp const gpioRedLed_Handle;

/**
 * @brief GPIO handle for Yelow LED
 */
extern GPIO_handle_tp const gpioYellowLed_Handle;

/**
 * @brief GPIO handle for Orange LED
 */
extern GPIO_handle_tp const gpioOrangeLed_Handle;

/* External bus Handle definitions */
/**
 * @brief GPIO handle for external pin
 */
extern GPIO_handle_tp const externalInput_Handle;

/**
 * @brief GPIO handle for external pin
 */
extern GPIO_handle_tp const externalOutput_Handle;

/* Button Handle definitions */
/**
 * @brief GPIO handle for Button 1
 */
extern GPIO_handle_tp const gpioButton1_Handle;

/**
 * @brief GPIO handle for Button 2
 */
extern GPIO_handle_tp const gpioButton2_Handle;

/* I2C Handle definitions */
/**
 * @brief I2C handle for channel 0
 */
extern I2C_init_tp const i2cChannel0_Handle;

/**
 * @brief I2C handle for Extension bus channel 1
 */
extern I2C_init_tp const i2cChannel1_Handle;

#endif /* XDK110_XDKBOARDHANDLE_H_ */
