/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**  @file        UsbResetUtility.h
 *
 * @defgroup USBResetUtility USB Reset Utility
 * @brief USB Reset Utility for the USB
 *
 * @{
 * @brief  The interface header exports the following features:
 *  													- UsbResetUtility_Init()
 *  													- UsbResetUtility_UsbISRCallback()
 *  													- UsbResetUtility_RegAppISR()
 * 													    - UsbResetUtility_DeInit()
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef XDK110_XDKUSBRESETUTILITY_H_
#define XDK110_XDKUSBRESETUTILITY_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */
#include "BCDS_Retcode.h"
/**
 *  @brief Return status for operations using corresponding Retcodes
 */
enum UsbResetUtility_ErrorCode_E
{
    USB_RECIEVE_FAILED = RETCODE_FIRST_CUSTOM_CODE, /**Usb Receive buffer status*/
    TIMER_PEND_FAILED
};

/**
 * Define the prototype to which USB recieve callback functions must conform in application layer.
 * Functions implementing USB recieve callbacks do not need to return, therefore their return type
 * is void none.
 */
typedef void (*UsbResetUtility_UsbAppCallback_T)(uint8_t *, uint32_t);

/* public function prototype declarations */

/* public global variable declarations */
/**
 * @brief This API will initialize the URU module by mapping the ISR callback of URU to USB
 *
 * @retval returns success or failure upon initialization of UsbResetUtility
 */
Retcode_T UsbResetUtility_Init(void);

/**
 * @brief This API is using in USB configuration to trigger USB Receive Interrupt callback
 *        from application layer.
 * @param[in]	usbRcvBuffer
 *         Receive buffer where the data is received in the USB Interrupt callback.
 * @param[in]	usbRcvBufLength
 *       The size of the data received.
 *
 * @retval returns success or failure upon execution of  UsbResetUtility_UsbISRCallback
 *
 */
Retcode_T UsbResetUtility_UsbISRCallback(uint8_t *usbRcvBuffer, uint8_t usbRcvBufLength);

/**
 * @brief       This API used to register the application call back
 *
 * @param[in]   UsbResetUtility_UsbAppCallback_T - function pointer
 *
 * @retval returns success or failure upon execution of register Usb resetUtility ISR
 */
Retcode_T UsbResetUtility_RegAppISR(UsbResetUtility_UsbAppCallback_T uruAppCallback);

/**
 * @brief       This API used to deinit the URU module
 *
 * @retval returns success or failure upon execution
 */
Retcode_T UsbResetUtility_DeInit(void);

#endif /* XDK110_XDKUSBRESETUTILITY_H_ */

/**@}*/
/** ************************************************************************* */
