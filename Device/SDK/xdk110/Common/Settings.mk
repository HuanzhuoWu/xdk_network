#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

MAKE_FILE_ROOT ?= .

export BCDS_TARGET_PLATFORM = efm32
export BCDS_DEVICE_TYPE = EFM32GG
export BCDS_DEVICE_ID = EFM32GG390F1024
BCDS_UNIQUE_MAC ?= 0xFCD6BD090000
export XDK_FOTA_ENABLED_BOOTLOADER ?=0

#This variable needs to define the path of the project specific config folder
#during compilation in the libraries and platform 
export BCDS_PROJECT_SPECIFIC_CONFIG = $(CURDIR)/config
export BCDS_CONFIG_PATH = $(CURDIR)/config
export BCDS_PROJECT_PACKAGE_NAME = Common
BCDS_PACKAGE_ID = 153
export BCDS_HW_VERSION ?= HW_XDK_v1
export BCDS_SYSTEM_STARTUP_METHOD ?= DEFAULT_STARTUP
export BCDS_TOOL_CHAIN_PATH ?= $(CURDIR)/../Tools/gcc-arm-none-eabi/bin
FLASH_TOOL_PATH = $(CURDIR)/../Tools/EA_commander/V2.82/eACommander
BCDS_FOTA_TOOL_PATH =  $(CURDIR)/../Tools/Fota_Tools
BCDS_PYTHON_PATH =  $(CURDIR)/../Tools/Python34/python

# The below defined values is to update firmware version formed by given MAJOR MINOR and PATCH 
MAJOR_SW_NO ?= 0x00#Defines MAJOR number and maximum value is 255
MINOR_SW_NO ?= 0x00#Defines MINOR number and maximum value is 255
PATCH_SW_NO ?= 0x01#Defines PATCH number and maximum value is 255
HEADER_VERSION =0100#Defines the current used header version
PRODUCT_CLASS =0001#(Productcode[12bit]=001 for APLM, Minimum Hardwarerevision[4bit]
PRODUCT_VARIANT =0001
FIRMWARE_VERSION = $(subst 0x,,00$(MAJOR_SW_NO)$(MINOR_SW_NO)$(PATCH_SW_NO))
CREATE_CONTAINER_SCRIPT = $(BCDS_FOTA_TOOL_PATH)/create_fota_container.py

#Path where the LINT config files are present(compiler and environment configurations)
BCDS_LINT_CONFIG_PATH = $(CURDIR)/../Tools/PCLint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt
LINT_EXE = $(CURDIR)/../Tools/PCLint/exe/lint-nt.launch

# Build chain settings
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
OBJCOPY = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-objcopy
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
OBJCOPY = arm-none-eabi-objcopy
export BCDS_BUILD_LINUX = 1
endif
RMDIRS := rm -rf

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Project specific directories
BCDS_XDK_COMMON_DIR = ../Common
BCDS_XDK_DEBUG_DIR = debug
BCDS_XDK_RELEASE_DIR = release
BCDS_XDK_LINT_DIR = lint
BCDS_XDK_INCLUDE_DIR = $(MAKE_FILE_ROOT)/include
BCDS_XDK_SOURCE_DIR = $(MAKE_FILE_ROOT)/source
BCDS_XDK_PROTECTED_DIR = protected
BCDS_XDK_CONFIG_DIR = $(MAKE_FILE_ROOT)/config
BCDS_XDK_OBJECT_DIR = objects
BCDS_XDK_APP_PATH= $(BCDS_APP_DIR)
BCDS_APP_NAME_WITH_HEADER = $(BCDS_APP_NAME)_with_header

ifeq ($(XDK_FOTA_ENABLED_BOOTLOADER),0)
XDK_APP_ADDRESS = 0x00010000 # @see efm32gg.ld , This is the flash start address if EA commander tool used for flashing 
BCDS_XDK_LD_FILE = efm32gg.ld
$(info old_bootloader)
else
XDK_APP_ADDRESS = 0x00020000 # @see efm32gg_new.ld, This is the flash start address if EA commander tool used for flashing 
BCDS_XDK_LD_FILE = efm32gg_new.ld
$(info new_bootloader)
endif

#Path of the object files
BCDS_XDK_APP_DEBUG_OBJECT_DIR = $(BCDS_XDK_APP_PATH)/$(BCDS_XDK_DEBUG_DIR)/$(BCDS_XDK_OBJECT_DIR)
BCDS_XDK_APP_RELEASE_OBJECT_DIR =  $(BCDS_XDK_APP_PATH)/$(BCDS_XDK_RELEASE_DIR)/$(BCDS_XDK_OBJECT_DIR)
BCDS_XDK_APP_DEBUG_DIR = $(BCDS_XDK_APP_PATH)/$(BCDS_XDK_DEBUG_DIR)
BCDS_XDK_APP_RELEASE_DIR = $(BCDS_XDK_APP_PATH)/$(BCDS_XDK_RELEASE_DIR)

#Lint Path
BCDS_XDK_APP_LINT_PATH = $(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_XDK_LINT_DIR)

#Path of other libraries and platform
BCDS_LIBRARIES_DIR = $(MAKE_FILE_ROOT)/../Libraries
BCDS_PLATFORM_DIR = $(MAKE_FILE_ROOT)/../Platform

BCDS_BLE_LIB_PATH = $(BCDS_LIBRARIES_DIR)/BLEStack/Alpwise
BCDS_BLE_CORE_PATH = $(BCDS_BLE_LIB_PATH)/Core_Stack
BCDS_BLE_INTERFACE_PATH = $(BCDS_BLE_CORE_PATH)/Interfaces
BCDS_BLE_SERVICE_PATH = $(BCDS_BLE_LIB_PATH)/Service
BCDS_BSXLITE_LIB_PATH = $(BCDS_LIBRARIES_DIR)/BSXlite
BCDS_BST_LIB_DIR = $(BCDS_LIBRARIES_DIR)/BSTLib/BST_Github
BCDS_EM_LIB_DIR = $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FATFS_LIB_DIR = $(BCDS_LIBRARIES_DIR)/FATfs
BCDS_SERVALSTACK_LIB_DIR = $(BCDS_LIBRARIES_DIR)/ServalStack/ServalStack
BCDS_SERVAL_LIB_DIR = $(BCDS_LIBRARIES_DIR)/ServalStack
BCDS_OS_LIB_DIR = $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS
BCDS_WIFI_LIB_DIR = $(BCDS_LIBRARIES_DIR)/WiFi/TI
BCDS_BLE_DIR = $(BCDS_PLATFORM_DIR)/BLE
BCDS_PERIPHERALS_DIR = $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_MISCDRIVERS_DIR = $(BCDS_PLATFORM_DIR)/MiscDrivers
BCDS_SENSORS_DIR = $(BCDS_PLATFORM_DIR)/Sensors
BCDS_SENSORS_UTILS_DIR = $(BCDS_PLATFORM_DIR)/SensorUtils

BCDS_SERVALPAL_WIFI_DIR = $(BCDS_PLATFORM_DIR)/ServalPAL_WiFi
BCDS_UTILS_DIR = $(BCDS_PLATFORM_DIR)/Utils
BCDS_WIFI_DIR = $(BCDS_PLATFORM_DIR)/Wlan
BCDS_SENSOR_TOOLBOX_DIR = $(BCDS_PLATFORM_DIR)/SensorToolbox
BCDS_ALGO_DIR = $(BCDS_PLATFORM_DIR)/Algo
BCDS_BASICS_DIR = $(BCDS_PLATFORM_DIR)/Basics
BCDS_SENSORDRIVERS_DIR = $(BCDS_PLATFORM_DIR)/SensorDrivers
BCDS_POWER_DIR = $(BCDS_PLATFORM_DIR)/Power
BCDS_CYCURL_LIB_DIR = $(BCDS_LIBRARIES_DIR)/Escrypt_CycurTLS/CycurTLS/src/cycurlib
BCDS_FOTA_DIR = $(BCDS_PLATFORM_DIR)/FOTA

#Path of the generated static library and its object files
BCDS_XDK_COMMON_DEBUG_PATH ?= $(CURDIR)/debug
BCDS_XDK_COMMON_RELEASE_PATH ?= $(CURDIR)/release

BCDS_XDK_COMMON_DEBUG_OBJECT_PATH = $(BCDS_XDK_COMMON_DEBUG_PATH)/$(BCDS_XDK_OBJECT_DIR)
BCDS_XDK_COMMON_RELEASE_OBJECT_PATH = $(BCDS_XDK_COMMON_RELEASE_PATH)/$(BCDS_XDK_OBJECT_DIR)

# Archive File Settings for common
BCDS_XDK_COMMON_BASE_NAME = XDK$(BCDS_PROJECT_PACKAGE_NAME)
BCDS_XDK_COMMON_DEBUG_NAME = $(BCDS_XDK_COMMON_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_XDK_COMMON_RELEASE_NAME = $(BCDS_XDK_COMMON_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a

BCDS_XDK_COMMON_DEBUG_LIB  = $(BCDS_XDK_COMMON_DEBUG_PATH)/$(BCDS_XDK_COMMON_DEBUG_NAME)
BCDS_XDK_COMMON_RELEASE_LIB = $(BCDS_XDK_COMMON_RELEASE_PATH)/$(BCDS_XDK_COMMON_RELEASE_NAME)
BCDS_XDK_COMMON_DEBUG_LINT_PATH = $(BCDS_XDK_COMMON_DEBUG_PATH)/$(BCDS_XDK_LINT_DIR)
 

# Define the path for the include directories.
BCDS_XDK_INCLUDES += \
                  -I$(BCDS_XDK_COMMON_DIR)/$(BCDS_XDK_INCLUDE_DIR) \
                  -I$(BCDS_XDK_COMMON_DIR)/$(BCDS_XDK_CONFIG_DIR) \
                  -I$(BCDS_XDK_COMMON_DIR)/$(BCDS_XDK_CONFIG_DIR)/MiscDrivers \
                  -I$(BCDS_XDK_COMMON_DIR)/$(BCDS_XDK_CONFIG_DIR)/FOTA \
                  -I$(BCDS_XDK_COMMON_DIR)/$(BCDS_XDK_CONFIG_DIR)/$(BCDS_HW_VERSION) \
       		      -I$(BCDS_BLE_DIR)/include \
		  		  -I$(BCDS_POWER_DIR)/include \
                  -I$(BCDS_PERIPHERALS_DIR)/include \
                  -I$(BCDS_BASICS_DIR)/include \
                  -I$(BCDS_MISCDRIVERS_DIR)/include \
                  -I$(BCDS_SENSORS_DIR)/include \
				  -I$(BCDS_SENSORS_UTILS_DIR)/include \
                  -I$(BCDS_SERVALPAL_WIFI_DIR)/include \
                  -I$(BCDS_UTILS_DIR)/include \
                  -I$(BCDS_ALGO_DIR)/include \
				  -I$(BCDS_SENSOR_TOOLBOX_DIR)/include \
				  -I$(BCDS_FOTA_DIR)/include \
				  -I$(BCDS_FOTA_DIR)/source/protected \
				  -I$(BCDS_WIFI_DIR)/include 
				  
# By using -isystem, headers found in that directory will be considered as system headers, because of that 
# all warnings, other than those generated by #warning, are suppressed.	
BCDS_EXTERNAL_INCLUDES += \
				  -isystem $(BCDS_SERVALSTACK_LIB_DIR)/api \
				  -isystem $(BCDS_SERVALSTACK_LIB_DIR)/pal \
				  -isystem $(BCDS_OS_LIB_DIR)/source/portable/ARM_CM3 \
                  -isystem $(BCDS_OS_LIB_DIR)/source/include \
                  -isystem $(BCDS_EM_LIB_DIR)/emlib/inc \
                  -isystem $(BCDS_EM_LIB_DIR)/usb/inc \
                  -isystem $(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
                  -isystem $(BCDS_EM_LIB_DIR)/CMSIS/Include \
	              -isystem $(BCDS_FATFS_LIB_DIR)/src \
	              -isystem $(BCDS_BLE_CORE_PATH) \
                  -isystem $(BCDS_BLE_INTERFACE_PATH) \
                  -isystem $(BCDS_BLE_INTERFACE_PATH)/ATT \
                  -isystem $(BCDS_BLE_INTERFACE_PATH)/Services \
				  -isystem $(BCDS_BLE_SERVICE_PATH)/Data_Exchange_Service/Interfaces \
				  -isystem $(BCDS_WIFI_LIB_DIR)/simplelink/include 
	
# This variable should fully specify the build configuration of the Serval 
# Stack library with regards the enabled and disabled features as well as 
# the configuration of each enabled feature.
export SERVAL_FEATURES_CONFIG = \
	-D SERVAL_LOG_LEVEL=SERVAL_LOG_LEVEL_ERROR\
	-D SERVAL_ENABLE_HTTP_CLIENT=1\
	-D SERVAL_ENABLE_HTTP_SERVER=1\
	-D SERVAL_ENABLE_WEBSERVER=1\
	-D SERVAL_ENABLE_COAP_OBSERVE=1\
	-D SERVAL_ENABLE_COAP_CLIENT=1\
	-D SERVAL_ENABLE_COAP_SERVER=1\
	-D SERVAL_ENABLE_REST_CLIENT=1\
	-D SERVAL_ENABLE_REST_SERVER=1\
	-D SERVAL_ENABLE_REST_HTTP_BINDING=1\
	-D SERVAL_ENABLE_REST_COAP_BINDING=1\
	-D SERVAL_ENABLE_XUDP=1\
	-D SERVAL_ENABLE_DPWS=0\
	-D SERVAL_ENABLE_TLS_CLIENT=0\
	-D SERVAL_ENABLE_TLS_SERVER=0\
	-D SERVAL_ENABLE_TLS_ECC=0\
	-D SERVAL_ENABLE_TLS_PSK=1\
	-D SERVAL_ENABLE_DTLS=1\
	-D SERVAL_ENABLE_DTLS_CLIENT=1\
	-D SERVAL_ENABLE_DTLS_SERVER=1\
	-D SERVAL_ENABLE_DTLS_PSK=1\
	-D SERVAL_ENABLE_HTTP_AUTH=1\
	-D SERVAL_ENABLE_HTTP_AUTH_DIGEST=1\
	-D SERVAL_ENABLE_DUTY_CYCLING=1\
	-D SERVAL_ENABLE_APP_DATA_ACCESS=0\
	-D SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT=1\
	-D SERVAL_ENABLE_COAP_OBSERVE=1\
	-D SERVAL_ENABLE_LWM2M=1\
	-D SERVAL_ENABLE_XTCP=1\
	-D SERVAL_ENABLE_XTCP_SERVER=1\
	-D SERVAL_ENABLE_XTCP_CLIENT=1\
	-D SERVAL_HTTP_MAX_NUM_SESSIONS=3\
	-D SERVAL_HTTP_SESSION_MONITOR_TIMEOUT=4000\
	-D SERVAL_MAX_NUM_MESSAGES=8\
	-D SERVAL_MAX_SIZE_APP_PACKET=600\
	-D SERVAL_MAX_SECURE_SOCKETS=5\
	-D SERVAL_MAX_SECURE_CONNECTIONS=5\
	-D SERVAL_SECURE_SERVER_CONNECTION_TIMEOUT=300000\
	-D SERVAL_DOWNGRADE_TLS=1\
	-D SERVAL_TLS_CYASSL=0 \
	-D SERVAL_ENABLE_DTLS_ECC=0 \
	-D SERVAL_ENABLE_DTLS_RSA=0 \
	-D COAP_MSG_MAX_LEN=224 \
	-D COAP_MAX_NUM_OBSERVATIONS=50 \
	-D LWM2M_MAX_NUM_OBSERVATIONS=50 \
	-D SERVAL_HTTP_MAX_LENGTH_URL=256 \
	-D SERVAL_TLS_CYCURTLS=1 \
	-D LWM2M_MAX_LENGTH_DEVICE_NAME=32 \
	-D SERVAL_ENABLE_DTLS_SESSION_ID=0 \
	-D LWM2M_DISABLE_CLIENT_QUEUEMODE=1 \
	-D PAL_MAX_NUM_ADDITIONAL_COMM_BUFFERS=6 \
	-D SERVAL_ENABLE_DTLS_HEADER_LOGGING=0 \
	-D CYCURTLS_HAVE_GET_CURRENT_TASKNAME=0 \
	-D SERVAL_DTLS_FLIGHT_MAX_RETRIES=4 \
	-D SERVAL_EXPERIMENTAL_DTLS_MONITOR_EXTERN=1 \
	-D SERVAL_POLICY_STACK_CALLS_TLS_API=1 \
	-D SERVAL_SECURITY_API_VERSION=2
	

export CYCURTLS_FEATURES_CONFIG = \
	-DEscTls_CIPHER_SUITE=TLS_PSK_WITH_AES_128_CCM_8 \
	-DEscHashDrbg_SHA_TYPE=256 \
	-DEscDtls_MAX_COOKIE_SIZE=64 \
	-DEscTls_PSK_IDENTITY_LENGTH_MAX=64 \
	-DEscTls_MAXIMUM_FRAGMENT_LENGTH=256

#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------

BCDS_SERVAL_CONFIG_LINT := $(subst -D , +d,$(SERVAL_FEATURES_CONFIG))
BCDS_CYCURTLS_CONFIG_LINT := $(subst -D , +d,$(CYCURTLS_FEATURES_CONFIG))
BCDS_EXTERNAL_EXCLUDES_LINT := $(foreach DIR, $(subst -isystem ,,$(BCDS_EXTERNAL_INCLUDES)), +libdir\($(DIR)\))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -isystem ,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_XDK_INCLUDES_LINT := $(subst -I,-i,$(BCDS_XDK_INCLUDES))
BCDS_LINT_CONFIG = -i$(BCDS_LINT_CONFIG_PATH)
#-------------------------------------------------------------------------------------------------					 
