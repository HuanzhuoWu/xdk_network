#This file has the static library paths of third-party software and BCDS platform


BCDS_DEBUG_LIB_PATH = ../BCDS_Customization/debug
BCDS_RELEASE_LIB_PATH = ../BCDS_Customization/release

clean_libraries: clean_Power clean_MiscDrivers clean_Wifi clean_BLE clean_Sensors clean_Peripherals \
clean_Utils clean_Common clean_ServalPAL clean_Emlib clean_LibWifi clean_LibOS clean_LibBST  \
clean_LibBasics clean_SensorDrivers clean_SensorsUtils clean_SensorToolbox clean_ServalStack clean_FATfs clean_Fota clean_CycurLib

clean_SensorToolbox:
	$(MAKE) -C $(BCDS_SENSOR_TOOLBOX_DIR) clean
clean_MiscDrivers:
	$(MAKE) -C $(BCDS_MISCDRIVERS_DIR) clean
clean_Wifi:
	$(MAKE) -C $(BCDS_WIFI_DIR) clean
clean_BLE:
	$(MAKE) -C $(BCDS_BLE_DIR) clean
clean_Sensors:	   
	$(MAKE) -C $(BCDS_SENSORS_DIR) clean
clean_SensorsUtils:	   
	$(MAKE) -C $(BCDS_SENSORS_UTILS_DIR) clean	
clean_Peripherals:
	$(MAKE) -C $(BCDS_PERIPHERALS_DIR) clean
clean_Utils:
	$(MAKE) -C $(BCDS_UTILS_DIR) clean
clean_Common:
	$(MAKE) -C $(BCDS_XDK_COMMON_DIR) clean
clean_ServalStack:
	$(MAKE) -C $(BCDS_SERVAL_LIB_DIR)/BCDS_Customization clean
clean_ServalPAL:
	$(MAKE) -C $(BCDS_SERVALPAL_WIFI_DIR) clean
clean_Emlib:
	$(MAKE) -C $(BCDS_EM_LIB_DIR)/../BCDS_Customization clean
clean_LibWifi:
	$(MAKE) -C $(BCDS_WIFI_LIB_DIR)/../BCDS_Customization clean
clean_LibOS:
	$(MAKE) -C $(BCDS_OS_LIB_DIR)/../BCDS_Customization clean
clean_LibBST:
	$(MAKE) -C $(BCDS_BST_LIB_DIR)/../BCDS_Customization clean
clean_LibBasics:
	$(MAKE) -C $(BCDS_BASICS_DIR) clean
clean_SensorDrivers:
	$(MAKE) -C $(BCDS_SENSORDRIVERS_DIR) clean
clean_Power:
	$(MAKE) -C $(BCDS_POWER_DIR) clean
clean_FATfs:
	$(MAKE) -C $(BCDS_FATFS_LIB_DIR)/BCDS_Customization clean
clean_Fota:
	$(MAKE) -C $(BCDS_FOTA_DIR) clean
clean_CycurLib:
	$(MAKE) -C $(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization clean	

BCDS_THIRD_PARTY_LIBS= \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_CoreStack_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/Data_Exchange_Service/Libraries/BLESW_ALPWDEP_Server_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_ALPWDES_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_AlertNotification/Libraries/BLESW_ANP_Server_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_ANS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_BloodPressure/Libraries/BLESW_BLP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_BLS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_CyclingPower/Libraries/BLESW_CPP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_CPS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_CyclingSpeedAndCadence/Libraries/BLESW_CSCP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_CSCS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_Glucose/Libraries/BLESW_GLP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_GLS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_HealthThermometer/Libraries/BLESW_HTP_Thermometer_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_HTS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_HeartRate/Libraries/BLESW_HRP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_HRS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_HumanInterfaceDevice/Libraries/BLESW_HID_Host_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_HIDS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_LocationAndNavigation/Libraries/BLESW_LNP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_LNS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_NetworkAvailability/Libraries/BLESW_NWAP_Reporter_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_NWAS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_PhoneAlertStatus/Libraries/BLESW_PASP_Server_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_PASS_CM3.a \
	$(BCDS_BLE_LIB_PATH)/Service/BLESW_RunningSpeedAndCadence/Libraries/BLESW_RSCP_Sensor_CM3.a  \
	$(BCDS_BLE_LIB_PATH)/Core_Stack/Libraries/BLESW_RSCS_CM3.a \
	$(BCDS_EM_LIB_DIR)/CMSIS/Lib/GCC/libarm_cortexM3l_math.a \
		
#Debug libraries
BCDS_LIBS_DEBUG = \
	$(BCDS_SENSOR_TOOLBOX_DIR)/release/libSensorToolbox.a\
	$(BCDS_WIFI_LIB_DIR)/$(BCDS_DEBUG_LIB_PATH)/libWifiLib_efm32_debug.a \
	$(BCDS_WIFI_DIR)/debug/libWlan_efm32_debug.a \
	$(BCDS_SERVAL_LIB_DIR)/BCDS_Customization/release/libServalStack_efm32.a\
    $(BCDS_SERVALPAL_WIFI_DIR)/release/libServalPalWifi_efm32.a\
   	$(BCDS_MISCDRIVERS_DIR)/debug/libMiscDrivers_efm32_debug.a \
	$(BCDS_BLE_DIR)/debug/libBLE_efm32_debug.a \
	$(BCDS_SENSORS_DIR)/debug/libSensors_efm32_debug.a \
	$(BCDS_SENSORS_UTILS_DIR)/debug/libSensorUtils_efm32_debug.a \
    $(BCDS_OS_LIB_DIR)/$(BCDS_DEBUG_LIB_PATH)/libFreeRTOS_efm32_debug.a \
 	$(BCDS_PERIPHERALS_DIR)/debug/libPeripherals_efm32_debug.a \
    $(BCDS_EM_LIB_DIR)/$(BCDS_DEBUG_LIB_PATH)/libEMLib_efm32_debug.a \
    $(BCDS_BST_LIB_DIR)/$(BCDS_DEBUG_LIB_PATH)/libBSTLib_efm32_debug.a \
    $(BCDS_XDK_COMMON_DIR)/debug/XDKCommon_efm32_debug.a \
    $(BCDS_UTILS_DIR)/debug/libUtils_efm32_debug.a \
    $(BCDS_BASICS_DIR)/debug/libBasics_efm32_debug.a \
	$(BCDS_SENSORDRIVERS_DIR)/debug/libSensorDrivers_efm32_debug.a \
    $(BCDS_POWER_DIR)/debug/libPOWER_efm32_debug.a \
	$(BCDS_SENSORDRIVERS_DIR)/debug/libSensorDrivers_efm32_debug.a \
    $(BCDS_FATFS_LIB_DIR)/BCDS_Customization/debug/libFATfs_efm32_debug.a \
	$(BCDS_FOTA_DIR)/debug/libFOTA_efm32_debug.a \
	$(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization/release/libCYCURTLS_efm32.a\
    
$(BCDS_MISCDRIVERS_DIR)/debug/libMiscDrivers_efm32_debug.a:
	$(MAKE) -C $(BCDS_MISCDRIVERS_DIR) debug
	
$(BCDS_WIFI_DIR)/debug/libWlan_efm32_debug.a:
	$(MAKE) -C $(BCDS_WIFI_DIR) debug

$(BCDS_SENSOR_TOOLBOX_DIR)/debug/libSensorToolbox_debug.a:
	$(MAKE) -C $(BCDS_SENSOR_TOOLBOX_DIR) debug

$(BCDS_BLE_DIR)/debug/libBLE_efm32_debug.a:
	$(MAKE) -C $(BCDS_BLE_DIR) debug
	    
$(BCDS_SENSORS_DIR)/debug/libSensors_efm32_debug.a:
	$(MAKE) -C $(BCDS_SENSORS_DIR) debug

$(BCDS_SENSORS_UTILS_DIR)/debug/libSensorUtils_efm32_debug.a:
	$(MAKE) -C $(BCDS_SENSORS_UTILS_DIR) debug
	
$(BCDS_PERIPHERALS_DIR)/debug/libPeripherals_efm32_debug.a:
	$(MAKE) -C $(BCDS_PERIPHERALS_DIR) debug
	
$(BCDS_SERVAL_LIB_DIR)/BCDS_Customization/debug/libServalStack_efm32_debug.a:
	$(MAKE) -C $(BCDS_SERVAL_LIB_DIR)/BCDS_Customization debug

$(BCDS_EM_LIB_DIR)/../BCDS_Customization/debug/libEMLib_efm32_debug.a:
	$(MAKE) -C $(BCDS_EM_LIB_DIR)/../BCDS_Customization debug
	
$(BCDS_WIFI_LIB_DIR)/$(BCDS_DEBUG_LIB_PATH)/libWifiLib_efm32_debug.a:
	$(MAKE) -C $(BCDS_WIFI_LIB_DIR)/../BCDS_Customization debug

$(BCDS_OS_LIB_DIR)/../BCDS_Customization/debug/libFreeRTOS_efm32_debug.a:
	$(MAKE) -C $(BCDS_OS_LIB_DIR)/../BCDS_Customization debug

$(BCDS_BST_LIB_DIR)/../BCDS_Customization/debug/libBSTLib_efm32_debug.a:
	$(MAKE) -C $(BCDS_BST_LIB_DIR)/../BCDS_Customization debug
        	
$(BCDS_UTILS_DIR)/debug/libUtils_efm32_debug.a:
	$(MAKE) -C $(BCDS_UTILS_DIR) debug
	
$(BCDS_XDK_COMMON_DIR)/debug/XDKCommon_efm32_debug.a:
	$(MAKE) -C $(BCDS_XDK_COMMON_DIR) debug

$(BCDS_SERVALPAL_WIFI_DIR)/debug/libServalPalWifi_efm32_debug.a:
	$(MAKE) -C $(BCDS_SERVALPAL_WIFI_DIR) debug

	
$(BCDS_BASICS_DIR)/debug/libBasics_efm32_debug.a:
	$(MAKE) -C $(BCDS_BASICS_DIR) debug

$(BCDS_SENSORDRIVERS_DIR)/debug/libSensorDrivers_efm32_debug.a:
	$(MAKE) -C $(BCDS_SENSORDRIVERS_DIR) debug

$(BCDS_POWER_DIR)/debug/libPOWER_efm32_debug.a:
	$(MAKE) -C $(BCDS_POWER_DIR) debug

$(BCDS_FATFS_LIB_DIR)/BCDS_Customization/debug/libFATfs_efm32_debug.a:
	$(MAKE) -C $(BCDS_FATFS_LIB_DIR)/BCDS_Customization debug
	
$(BCDS_FOTA_DIR)/debug/libFOTA_efm32_debug.a:
	$(MAKE) -C $(BCDS_FOTA_DIR) debug

$(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization/debug/libCYCURTLS_efm32_debug.a:
	$(MAKE) -C $(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization debug
	
#Release libraries
BCDS_LIBS_RELEASE = \
    $(BCDS_WIFI_LIB_DIR)/$(BCDS_RELEASE_LIB_PATH)/libWifiLib_efm32.a \
	$(BCDS_WIFI_DIR)/release/libWlan_efm32.a \
	$(BCDS_SERVAL_LIB_DIR)/BCDS_Customization/release/libServalStack_efm32.a \
	$(BCDS_MISCDRIVERS_DIR)/release/libMiscDrivers_efm32.a \
	$(BCDS_BLE_DIR)/release/libBLE_efm32.a \
	$(BCDS_SENSORS_DIR)/release/libSensors_efm32.a \
	$(BCDS_SENSORS_UTILS_DIR)/release/libSensorUtils_efm32.a \
	$(BCDS_EM_LIB_DIR)/$(BCDS_RELEASE_LIB_PATH)/libEMLib_efm32.a \
    $(BCDS_OS_LIB_DIR)/$(BCDS_RELEASE_LIB_PATH)/libFreeRTOS_efm32.a \
    $(BCDS_BST_LIB_DIR)/$(BCDS_RELEASE_LIB_PATH)/libBSTLib_efm32.a \
    $(BCDS_PERIPHERALS_DIR)/release/libPeripherals_efm32.a \
    $(BCDS_XDK_COMMON_DIR)/release/XDKCommon_efm32.a \
    $(BCDS_UTILS_DIR)/release/libUtils_efm32.a \
	$(BCDS_SERVALPAL_WIFI_DIR)/release/libServalPalWifi_efm32.a \
	$(BCDS_BASICS_DIR)/release/libBasics_efm32.a \
	$(BCDS_SENSORDRIVERS_DIR)/release/libSensorDrivers_efm32.a \
	$(BCDS_SENSOR_TOOLBOX_DIR)/release/libSensorToolbox.a \
	$(BCDS_POWER_DIR)/release/libPOWER_efm32.a \
	$(BCDS_FATFS_LIB_DIR)/BCDS_Customization/release/libFATfs_efm32.a \
	$(BCDS_FOTA_DIR)/release/libFOTA_efm32.a \
	$(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization/release/libCYCURTLS_efm32.a \
	
$(BCDS_MISCDRIVERS_DIR)/release/libMiscDrivers_efm32.a:
	$(MAKE) -C $(BCDS_MISCDRIVERS_DIR) release
	
$(BCDS_BLE_DIR)/release/libBLE_efm32.a:
	$(MAKE) -C $(BCDS_BLE_DIR) release
	
$(BCDS_SENSOR_TOOLBOX_DIR)/release/libSensorToolbox.a:
	$(MAKE) -C $(BCDS_SENSOR_TOOLBOX_DIR) release
	
$(BCDS_EM_LIB_DIR)/../BCDS_Customization/release/libEMLib_efm32.a:
	$(MAKE) -C $(BCDS_EM_LIB_DIR)/../BCDS_Customization release
	
$(BCDS_OS_LIB_DIR)/../BCDS_Customization/release/libFreeRTOS_efm32.a:
	$(MAKE) -C $(BCDS_OS_LIB_DIR)/../BCDS_Customization release
	
$(BCDS_BST_LIB_DIR)/../BCDS_Customization/release/libBSTLib_efm32.a:
	$(MAKE) -C $(BCDS_BST_LIB_DIR)/../BCDS_Customization release
	
$(BCDS_SENSORS_DIR)/release/libSensors_efm32.a:
	$(MAKE) -C $(BCDS_SENSORS_DIR) release
	
$(BCDS_PERIPHERALS_DIR)/release/libPeripherals_efm32.a:
	$(MAKE) -C $(BCDS_PERIPHERALS_DIR) release
	
$(BCDS_UTILS_DIR)/release/libUtils_efm32.a:
	$(MAKE) -C $(BCDS_UTILS_DIR) release
	
$(BCDS_SERVALPAL_WIFI_DIR)/release/libServalPalWifi_efm32.a:
	$(MAKE) -C $(BCDS_SERVALPAL_WIFI_DIR) release
		
$(BCDS_XDK_COMMON_DIR)/release/XDKCommon_efm32.a:
	$(MAKE) -C $(BCDS_XDK_COMMON_DIR) release
	
$(BCDS_WIFI_DIR)/release/libWlan_efm32.a:
	$(MAKE) -C $(BCDS_WIFI_DIR) release
	
$(BCDS_WIFI_LIB_DIR)/../BCDS_Customization/release/libWifiLib_efm32.a:
	$(MAKE) -C $(BCDS_WIFI_LIB_DIR)/../BCDS_Customization release
	
$(BCDS_SERVAL_LIB_DIR)/BCDS_Customization/release/libServalStack_efm32.a:
	$(MAKE) -C $(BCDS_SERVAL_LIB_DIR)/BCDS_Customization release

	
$(BCDS_BASICS_DIR)/release/libBasics_efm32.a:
	$(MAKE) -C $(BCDS_BASICS_DIR) release
	
$(BCDS_SENSORDRIVERS_DIR)/release/libSensorDrivers_efm32.a:
	$(MAKE) -C $(BCDS_SENSORDRIVERS_DIR) release

$(BCDS_SENSORS_UTILS_DIR)/release/libSensorUtils_efm32.a:
	$(MAKE) -C $(BCDS_SENSORS_UTILS_DIR) release

$(BCDS_POWER_DIR)/release/libPOWER_efm32.a:
	$(MAKE) -C $(BCDS_POWER_DIR) release
	
$(BCDS_FATFS_LIB_DIR)/BCDS_Customization/release/libFATfs_efm32.a:
	$(MAKE) -C $(BCDS_FATFS_LIB_DIR)/BCDS_Customization release
	
$(BCDS_FOTA_DIR)/release/libFOTA_efm32.a:
	$(MAKE) -C $(BCDS_FOTA_DIR) release
	
$(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization/release/libCYCURTLS_efm32.a:
	$(MAKE) -C $(BCDS_CYCURL_LIB_DIR)/../../../BCDS_Customization release
	
.PHONY: $(BCDS_LIBS_DEBUG) $(BCDS_LIBS_RELEASE)	
	