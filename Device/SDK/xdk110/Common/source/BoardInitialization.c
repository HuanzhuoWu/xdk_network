/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**  @file
 *
 * This module initializes the chip and on chip's peripheral support system
 * ****************************************************************************/

//lint -emacro(506,*) /* Suppressing "Constant value Boolean" lint warning*/

/* module includes ********************************************************** */

/* Public header files */
#include "XdkBoardInitialization.h"

/* system header files */
#include "BCDS_Basics.h"

/* additional interface header files */

#include "BCDS_Assert.h"
#include "BCDS_Retcode.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "WIFI_ph.h"
#include "I2C_ih.h"
#include "USB_ih.h"
#include "BCDS_NVM.h"
#include "BCDS_NVMConfig.h"
#include "XdkGpioConfig.h"
#include "XdkUsbResetUtility.h"
#include "BLECustomConfig.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "XdkBoardHandle.h"

/* plausibility checks ****************************************************** */

static_assert(BLE_RANDOM_ADDRESSING_SUPPORT == 1, "Random addressing must be enabled for XDK projects that use Bluetooth Smart as"
        "a fall-back solution in case the device cannot load a proper public MAC address.")

/* constant definitions ***************************************************** */
#define BLE_MAC_USERPAGE_FLAG_BIT    UINT8_C(0x02)
#define WIFI_MAC_USERPAGE_FLAG_BIT   UINT8_C(0x01)
#define BTLE_MAC_ADDRESS_LENGTH      UINT8_C(0x06)
/* local variables ********************************************************** */

/* global variables ********************************************************* */
/**
 * @brief Data buffer of the user data NVM section.
 * @details This buffer is used by the NVM module to access and write the non
 * volatile memory (NVM) section in the user data page of the internal flash.
 */
static uint8_t NVMUserData[NVM_SECTION_UserPage_BUFFER_SIZE];

/**
 * @brief Global instance that is used by NVM to maintain internal (secured)
 * data
 */
struct NVM_S NVMUser =
        {
                NVM_SECTION_UserPage,
                NVMUserData,
                NVM_SECTION_UserPage_BUFFER_SIZE
        };
/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief Callback function to handle the error raised from platform.
 */
static void ErrorHandlingFunc(Retcode_T error)
{
    //@TODO ERROR HANDLING SHOULD BE DONE FOR THE ERRORS RAISED FROM PLATFORM
    uint32_t PackageID = Retcode_getPackage(error);
    //wifi package
    if (PackageID == 10)
    {
        printf("Error from wifi package\n\r");
    }
}

/**
 * @brief Callback function to read public BLE address via UPA module.
 *
 * See documentation of BLE_getPublicAddressCallback_t for further details.
 */
static void* bleGetPublicAddress(void* address)
{
    uint8_t isBleAddressValid = UINT8_C(0);
    Retcode_T nvmStatus = (Retcode_T)RETCODE_FAILURE;
#if (BLE_USERPAGE_PATCH)
    uint8_t ContentFlag = (BLE_MAC_USERPAGE_FLAG_BIT | WIFI_MAC_USERPAGE_FLAG_BIT);
    nvmStatus = NVM_WriteUInt8(&NVMUser, NVM_ITEM_CONTENT_INDEX, &ContentFlag);
    if (RETCODE_OK == nvmStatus)
    {
        nvmStatus = NVM_Flush(&NVMUser);
    }
    nvmStatus = NVM_Flush(&NVMUser);
    if (RETCODE_OK != nvmStatus)
    {
        return (NULL);
    }
#endif
    if (RETCODE_OK == NVM_ReadUInt8(&NVMUser, NVM_ITEM_CONTENT_INDEX, &isBleAddressValid))
    {
        if (!(BLE_MAC_USERPAGE_FLAG_BIT == (isBleAddressValid & BLE_MAC_USERPAGE_FLAG_BIT)))
        {
            address = NULL;
        }
        else
        {
            if (RETCODE_OK != NVM_Read(&NVMUser, NVM_BTLE_MAC_INDEX, address, BTLE_MAC_ADDRESS_LENGTH))
            {
                address = NULL;
            }
        }
    }

    return (address);
}

/**
 * @brief Customizing the interrupt priorities
 *
 *	Changing interrupt priority of all interrupts less than or equal to configMAX_SYSCALL_INTERRUPT_PRIORITY 5
 *  Lowering the Priority of all interrupts so that they can be masked by FreeRTOS TaskEnter Critical by Setting PRIMASK to 5
 *  See http://www.freertos.org/a00110.html#configTIMER_TASK_PRIORITY
 *
 */
void customizeInterruptPriority(void)
{
    NVIC_SetPriority(DMA_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(GPIO_EVEN_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(TIMER0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART0_RX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART0_TX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USB_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(ACMP0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(ADC0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(DAC0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(I2C0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(I2C1_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(GPIO_ODD_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(TIMER1_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(TIMER2_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(TIMER3_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART1_RX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART1_TX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(LESENSE_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART2_RX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(USART2_TX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(UART0_RX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(UART0_TX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(UART1_RX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(UART1_TX_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(LEUART0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(LEUART1_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(LETIMER0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(PCNT0_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(PCNT1_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(PCNT2_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(RTC_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(BURTC_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(CMU_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(VCMP_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(MSC_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(AES_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(EBI_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
    NVIC_SetPriority(EMU_IRQn, SYSCALL_INTERRUPT_PRIORITY_MAX);
}

#ifndef NDEBUG /* valid only for debug builds */
/**
 * @brief This API is called when function enters an assert
 *
 * @param[in] line : line number where asserted
 * @param[in] file : file name which is asserted
 *
 */

void assertIndicationMapping(const unsigned long line, const unsigned char * const file)
{
    /* Switch on the LEDs */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_ORANGE));
    PTD_pinOutSet(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);

    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_YELLOW));
    PTD_pinOutSet(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);

    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_RED));
    PTD_pinOutSet(PTD_PORT_LED_RED, PTD_PIN_LED_RED);

    printf("asserted at Filename %s , line no  %ld \n\r", file, line);

}

#endif

/* global functions ********************************************************* */
/**
 * @brief This API initializes the chip for use
 */
void boardInit(void)
{
    /* Initialize chip */
    CHIP_Init();
    customizeInterruptPriority();
}
/**
 * @brief This API initializes all the basic hardware peripherals for the user
 *
 * @see SystemStartup.c file for usage.
 */
void boardStartup(void)
{
    Retcode_T returnValue = (Retcode_T)RETCODE_FAILURE;
#ifndef NDEBUG /* valid only for debug builds */
    Retcode_T retcodeValue = ((Retcode_T) 1);
#endif

    /*Enable the GPIO clock*/
    PTD_portInit();

    /*initialize I2C channel0*/
    I2C_init(i2cChannel0_Handle);

    /*initialize I2C channel1 of Extension Bus*/
    I2C_init(i2cChannel1_Handle);
#ifndef NDEBUG /* valid only for debug builds */

    retcodeValue = Assert_initialize(assertIndicationMapping);
    if (retcodeValue != RETCODE_OK)
    {
        assert(false);
    }
#endif

    Retcode_initialize(ErrorHandlingFunc);

    /* structure to hold the USB initializtion parameters which as to passed as argument to initialize USB*/
    const USB_lineCoding_t UsbInitConfig_gds =
            { USB_SET_BAUDRATE, /**< This macro defines the baudrate used for usb serial communication */
            USB_SET_STOPBITS_MODE,/**< This macro defines the stop bits used for usb serial communication */
            USB_SET_PARITY, /**< This macro defines the parity bits used for usb serial communication */
            USB_SET_DATA_BITS, /**< This macro defines the number of data bits used for usb serial communication */
            0, /**< dummy padding value */
            NULL, /**< no receive callback is used */
            };

    /* initialize the user page module */
    returnValue = NVM_Init(&NVMUser);
    if (RETCODE_OK !=  returnValue )
    {
        assert(false);
    }
    /*initialize USB driver*/
    USB_init((USB_lineCoding_tp) &UsbInitConfig_gds);

    returnValue = UsbResetUtility_Init();
    /* Checking here that the call back was not mapped. This means the board will not reset in such a case */
    if (returnValue == (Retcode_T)RETCODE_FAILURE)
    {
        for (;;)
            ; // @TODO come up with a better handling mechanism
    }

    /*initialize the default pin configurations of the board*/
    resetGpioConfig();

    /* Set BLE default MAC Address to value from Flash User Page */
    (void) BLERADIO_RegisterGetPublicAddressCallback(bleGetPublicAddress);
}

/** ************************************************************************* */
