/*----------------------------------------------------------------------------*/
/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*
 * Copyright (C) Bosch Connected Devices and Solutions GmbH.
 * All Rights Reserved. Confidential.
 *
 * Distribution only to people who need to know this information in
 * order to do their job.(Need-to-know principle).
 * Distribution to persons outside the company, only if these persons
 * signed a non-disclosure agreement.
 * Electronic transmission, e.g. via electronic mail, must be made in
 * encrypted form.
 */
/*----------------------------------------------------------------------------*/

 /**  @file        PDC_pinDefaultConfig_cc.c
 *
 * This Module takes care of resetint the mode of the pins to the default pin at start up
 * 
 *
 * ****************************************************************************/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
/* own header files */
#include "XdkGpioConfig.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/* API documentation is in the interface header */
void resetGpioConfig(void)
{

    /* Push button 1 default pin configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(PUSH_BUTTON_1));
    /* Push button 2 default pin configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(PUSH_BUTTON_2));
    /* SPI MOSI pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_MOSI));
    /*SPI MISO pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_MISO));
    /*SPI Chip Select pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_CS));
    /*SPI clock pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_CLK));
    /*Interrupt pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_IRQ));
    /*Enable/Disable pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_ENABLE_DISABLE));
    /* Disable 2V5 VDD power supply to WIFI module*/
	PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_VDD_2V5_ENABLE));
    /* Disable 2V5 VDD snooze feature for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_VDD_2V5_SNOOZE));
    /* UART TX pin configuration for BLE module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BTLE_TX));
    /* UART RX pin configuration for BLE module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BTLE_RX));
    /* WAKEUP pin configuration for BLE module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BTLE_WAKEUP));
    /* RESET pin configuration for BTLE module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BTLE_RESET));
    /* Interrupt pin for BMA280 Interrupt 1*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMA280_INT1));
    /* Interrupt pin for BMA280 Interrupt 2*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMA280_INT2));
    /*  VDD pin configuration for BMA280 MOdule */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMA280_VDD));
    /*  VDD pin configuration for BME280 MOdule */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BME280_VDD));
    /*   Interrupt pin for BMG160 interrupt 1*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMG160_INT1));
    /*   Interrupt pin for BMG160 interrupt 2*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMG160_INT2));
    /*   Interrupt pin for BMM150*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMM150_INT));
    /*   Interrupt pin for MAX44009 sensor*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(MAX44009_INT));
    /*  Vcc enable pin for MAX44009 sensor*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(MAX44009_VDD_PORT));
    /*   ADC output pin of AKU340 sensor*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(AKU340_ADC_OUT));

    /******************** DEFAULT DRIVE MODE SET *******************************/
    /*  Drive Mode Setting for all  the pins of Port A*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_A));
    /*  Drive Mode Setting for all  the pins of Port B*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_B));
    /*  Drive Mode Setting for all  the pins of Port C*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_C));
    /*  Drive Mode Setting for all  the pins of Port D*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_D));
    /*  Drive Mode Setting for all  the pins of Port E*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_E));
    /*  Drive Mode Setting for all  the pins of Port F*/
    PTD_driveModeSet(PTD_GET_PORT_AND_DRIVE_MODE(PORT_F));
    /* set maximum even and odd interrupts*/
    PTD_setInterruptPinCount(PTD_NO_OF_EVEN_INTERRUPTS, PTD_NO_OF_ODD_INTERRUPTS);
    /* configure the interrupt */
    PTD_portInteruptConfig();

    /*  VDD pin configuration for BMI160 MOdule */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMI160_VDD));
    /*  VDD pin configuration for BMG160 MOdule */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMG160_VDD));
    /* Interrupt pin configuration for BLE module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BTLE_INT));
    /* VDD pin configuration for BMG160 */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMG160_VDD));
    /* VDD pin configuration for BMM150 */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMM150_VDD));
    /* BMM150 data ready pin for configuration*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMM150_DATA_READY));
    /* VDD pin configuration for BMI160 */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMI160_VDD));
    /*   Interrupt pin for BMI160 interrupt 1*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMI160_INT1));
    /*   Interrupt pin for BMI160 interrupt 2*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(BMI160_INT2));
    /* VDD pin configuration for AKU340 */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(AKU340_VDD));
    /* Battery voltage monitoring ADC pin configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(VBAT_ADC_OUT));

    /* ORANGE LED pin configuration  */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_ORANGE));
    /* YELLOW LED pin configuration  */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_YELLOW));
    /* RED LED pin configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_RED));
    /*Reset pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_RESET));
    /*VDD enable port pin configuration for WIFI module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_SD_VDD_PORT));
    /* SD card Level shifter pin  */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_LEVEL_SHIFT));
    /*  SD card detect pin */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_PRESENT));
    /*   SD card chip select pin*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_CS));
    /*   SPI MOSI pin configuration for SD CARD module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_MOSI));
    /*   SPI MISO pin configuration for SD CARD module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_MISO));
    /*  SPI CLK pin configuration for SD CARD module*/
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_CLK));

}
/** ************************************************************************* */
