/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**  @file       UsbResetUtility.c
 *
 * This module has the implementation of Reset from terminal program.
 *
 * ****************************************************************************/
//lint -esym(956,*) /* Suppressing "Non const, non volatile static or external variable" lint warning*/

/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>
#include <string.h>
/* additional interface header files */
#include "em_device.h"
/** @TODO : The following condition and the above inclusion of "em_device.h" needs to be
 * removed once the dependency of "em_usb.h" is removed in USB_ih.h.
 * This condition is added because, all the EM device variants does not support USB,
 * and because of the inclusion of "em_usb.h" in the USB_ih.h, a dependency is created
 * which affects the EM device types which does not support USB(during package creation). */
#if defined( USB_PRESENT ) && ( USB_COUNT == 1 )
#include "resetconfig.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "USB_ih.h"
#include "BCDS_NVMConfig.h"
#include "BCDS_NVM.h"
/* own header files */
#include "XdkUsbResetUtility.h"

/* constant definitions ***************************************************** */
#define URU_BOOT_ENABLE         (UINT8_C(1))                    /**< Enable the Boot flag  */
#define URU_APPL_RESET_ENABLE   (UINT8_C(2))                    /**< Enable Application Rest flag */
#define URU_FLAG_CLEAR          (UINT32_C(0))                   /**< Disable the Reset flag */
#define URU_ZERO_VALUE          (UINT8_C(0))                    /**< Zero Initialization for variable */
#define URU_BOOT_MAGIC_WORD     'b'                             /**< word to write in user page to reboot*/
#define URU_BUFFER_SIZE         (UINT8_C(64))                   /**< size of the buffer in which data is stored*/

/* local variables ********************************************************** */
/**Array to hold the data coming from usb*/
static uint8_t rcvBuffer[URU_BUFFER_SIZE];
/**Length of the data*/
static uint8_t rcvLength;

/* global variables ********************************************************* */
/** Instance for receive Application callback of USB */
UsbResetUtility_UsbAppCallback_T usbAppCallback = NULL;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief The function resets the board
 */
static void UsbResetUtility_SystemReset(void)
{
    /*Disconnects the USB*/
    USBD_Disconnect();

    /*This function will reset the board*/
    NVIC_SystemReset();
}

/**
 * @brief The function is been called from the timer queue when the string received from usb does not match with the magic string.
 * It is called so that the usb handling can be pended from the ISR context.
 *
 * @param [in] callBackParam1 first parameter of the function
 *
 * @param [in] callBackParam2 second parameter of the function
 *
 * @retval returns success or failure upon execution
 *
 */
static Retcode_T UsbResetUtility_InteruptHandling(void *callBackParam1, uint32_t callBackParam2)
{

    (void) callBackParam1;
    (void) callBackParam2;

    Retcode_T returnValue = (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T ) RETCODE_NULL_POINTER));

    if (usbAppCallback != NULL)
    {
        (*usbAppCallback)((uint8_t *) rcvBuffer, rcvLength);
        returnValue = RETCODE_OK;
    }
    return (returnValue);
}

/* global functions ********************************************************* */

/* API documentation is in the interface header */
Retcode_T UsbResetUtility_Init(void)
{
    Retcode_T returnStatus = RETCODE_OK;
    USB_returnCode_t UsbreturnStatus = USB_FAILURE;
    /*Registering the URU callback function*/
    UsbreturnStatus = USB_callBackMapping(
            (USB_rxCallback) UsbResetUtility_UsbISRCallback);
    if (UsbreturnStatus != USB_SUCCESS)
    {
        returnStatus = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t ) RETCODE_FAILURE);
    }

    return (returnStatus);
}

/* API documentation is in the interface header */
/**
 * It seems like this reset feature is application specific and
 * should be moved to the application that is using this function.
 */
Retcode_T UsbResetUtility_UsbISRCallback(uint8_t *usbRcvBuffer, uint8_t usbRcvBufLength)
{
    /* Initialized flag with Zero */

    Retcode_T returnStatus = RETCODE_OK;
    /* Return value for Timer start */
    int8_t returnValueSwTimer = pdPASS;

    if ((usbRcvBuffer != NULL) && (usbRcvBufLength != URU_ZERO_VALUE))
    {

        /*copies the received string and its length to static local buffer*/
        memcpy(rcvBuffer, usbRcvBuffer, usbRcvBufLength);
        rcvLength = usbRcvBufLength;

        /* Validating the system  BootEngage command */
        if ((strcmp((const char*) usbRcvBuffer, (const char*) Boot_Reset_Cmd))
                == URU_ZERO_VALUE)
        {
            uint8_t bootEngage = URU_BOOT_MAGIC_WORD;

            /*engage Bootloader flag in userpage */
            Retcode_T nvmStatus = NVM_WriteUInt8(&NVMUser,NVM_ITEM_BOOTLOADER_ENGAGE, &bootEngage);
            if (RETCODE_OK == nvmStatus)
            {
                nvmStatus = NVM_Flush(&NVMUser);
                if (RETCODE_OK == nvmStatus)
                {
                    /*doing the system reset */
                    UsbResetUtility_SystemReset();
                }
            }
        }
        /* Validating the system  Application Reset command */
        else if ((strcmp((const char*) usbRcvBuffer,
                                (const char*) Appl_Reset_Cmd)) == URU_ZERO_VALUE)
        {
            /*doing the system reset */
            UsbResetUtility_SystemReset();
        }
        else
        {
            portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
            returnValueSwTimer = xTimerPendFunctionCallFromISR((PendedFunction_t)UsbResetUtility_InteruptHandling, NULL, URU_ZERO_VALUE, &xHigherPriorityTaskWoken);
            if (pdPASS == returnValueSwTimer)
            {
                portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
            }
        }
        returnStatus = RETCODE_OK;

    }
    else
    {
        returnStatus = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) USB_RECIEVE_FAILED);

    }
    if (pdPASS != returnValueSwTimer)
    {
        returnStatus = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) TIMER_PEND_FAILED);
    }
    return (returnStatus);
}

/* API documentation is in the interface header */
Retcode_T UsbResetUtility_RegAppISR(UsbResetUtility_UsbAppCallback_T uruAppCallback)
{
    Retcode_T returnStatus = (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T ) RETCODE_NULL_POINTER));

    if (uruAppCallback != NULL)
    {
        usbAppCallback = uruAppCallback;
        returnStatus = RETCODE_OK;
    }
    return (returnStatus);
}

/* API documentation is in the interface header */
Retcode_T UsbResetUtility_DeInit(void)
{
    Retcode_T returnStatus = RETCODE_OK;
    USB_returnCode_t UsbreturnStatus = USB_FAILURE;
    /*Registering the URU callback function*/
    UsbreturnStatus = USB_callBackMapping(
            (USB_rxCallback) usbAppCallback);
    if(UsbreturnStatus != USB_SUCCESS)
    {
        returnStatus = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t ) RETCODE_FAILURE);
    }

    return (returnStatus);

}
#endif /* if (USB_PRESENT ) && ( USB_COUNT == 1 ) */
