/*----------------------------------------------------------------------------*/
/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*
 * Copyright (C) Bosch Connected Devices and Solutions GmbH.
 * All Rights Reserved. Confidential.
 *
 * Distribution only to people who need to know this information in
 * order to do their job.(Need-to-know principle).
 * Distribution to persons outside the company, only if these persons
 * signed a non-disclosure agreement.
 * Electronic transmission, e.g. via electronic mail, must be made in
 * encrypted form.
 */
/*----------------------------------------------------------------------------*/

 /**  @file
 *
 * Handles for the sensors will be returned through this module
 * 
 * ****************************************************************************/
#include "BCDS_Basics.h"

/* module includes ********************************************************** */
#include "XdkSensorHandle.h"

/* additional interface header files */
#include "BCDS_VirtualSensorHandle.h"
#include "PTD_portDriver_ph.h"
#include "sensorsHwConfig.h"
#include "BCDS_AxisRemap.h"
#include "BCDS_Bma280Utils.h"
#include "BCDS_Bme280Utils.h"
#include "BCDS_Bmi160Utils.h"
#include "BCDS_Bmg160Utils.h"
#include "BCDS_Bmm150Utils.h"
#include "BCDS_Max44009Utils.h"

/* constant definitions ***************************************************** */


static Bma280Utils_Info_T bma280Sensor_S =
        {
                .interface =
                        {
                                .bus = BMA280UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x18,
                                                        },
                                        },

                        },
                .sensorPowerInfo =
                        {
                                .SensorPower =
                                        {
                                                .pin = PTD_PIN_BMA280_VDD,
                                                .port = PTD_PORT_BMA280_VDD,
                                        },
                                .PowerControlFlag = 1,
                        },
                .interruptPin1 =
                        {
                                .gpio =
                                        {
                                                .pin = PTD_PIN_BMA280_INT1,
                                                .port = PTD_PORT_BMA280_INT1,
                                        },
                                .risingEdge = UINT8_C(0x00),
                                .fallingEdge = UINT8_C(0x01),
                                .enable = UINT8_C(0x01),
                                .callBack = NULL,
                        },
                .interruptPin2 =
                        {
                                .gpio =
                                        {
                                                .pin = PTD_PIN_BMA280_INT2,
                                                .port = PTD_PORT_BMA280_INT2,
                                        },
                                .risingEdge = UINT8_C(0x00),
                                .fallingEdge = UINT8_C(0x01),
                                .enable = UINT8_C(0x00),
                                .callBack = NULL,
                        },
                .remap =
                        {
                                .axisConfig = RMP_BMA2X2_X_AXIS_CONFIG | RMP_BMA2X2_Y_AXIS_CONFIG | RMP_BMA2X2_Z_AXIS_CONFIG,
                                .axisSign = RMP_BMA2X2_X_AXIS_SIGN_CONFIG | RMP_BMA2X2_Y_AXIS_SIGN_CONFIG | RMP_BMA2X2_Z_AXIS_SIGN_CONFIG,
                        },

        };

static Accelerometer_Handle_T xdkAccelerometers_BMA280_S = {
        .SensorPtr = (Bma280Utils_InfoPtr_T) & bma280Sensor_S,
        .SensorInformation =
                {
                        .sensorID = ACCELEROMETER_BMA280,
                        .initializationStatus = 0x00,
                },
};

/************************************************BMG Handle Info*********************************************************************/

static Bmg160Utils_Info_T bmg160Sensor_S =
        {
                .interface =
                        {
                                .bus = BMG160UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x68,
                                                        },
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_BMG160_VDD,
                                .pin = PTD_PIN_BMG160_VDD,
                        },
                .remap =
                        {
                                .axisConfig = RMP_BMG160_X_AXIS_CONFIG | RMP_BMG160_Y_AXIS_CONFIG | RMP_BMG160_Z_AXIS_CONFIG,
                                .axisSign = RMP_BMG160_X_AXIS_SIGN_CONFIG | RMP_BMG160_Y_AXIS_SIGN_CONFIG | RMP_BMG160_Z_AXIS_SIGN_CONFIG,
                        },
        };

static Gyroscope_Handle_T xdkGyroscope_BMG160_S = {
        .sensorPtr = (Bmg160Utils_Info_T *) &bmg160Sensor_S,
        .sensorInfo =
                {
                        .sensorID = BMG160_GYRO_SENSOR,
                        .initializationStatus = 0x00,
                },
};

/************************************************BME Handle Info*********************************************************************/
static Bme280Utils_Info_T bme280Sensor_S =
        {
                .interface =
                        {
                                .bus = BME280UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x76,
                                                        },
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_BME280_VDD,
                                .pin = PTD_PIN_BME280_VDD,
                        },
        };

static Environmental_Handle_T xdkEnvironmental_BME280_S = {
        .SensorPtr = (Bme280Utils_Info_T *) &bme280Sensor_S,
        .SensorInformation =
                {
                        .sensorID = ENVIRONMENTAL_BME280,
                        .initializationStatus = 0x00,
                },
};
/************************************************BMM Handle Info*********************************************************************/
static Bmm150Utils_Info_T bmm150Sensor_S =
        {
                .interface =
                        {
                                .bus = BMM150UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x10,
                                                        }
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_BMM150_VDD,
                                .pin = PTD_PIN_BMM150_VDD,
                        },
                .remap =
                        {
                                .axisConfig = RMP_BMM150_X_AXIS_CONFIG | RMP_BMM150_Y_AXIS_CONFIG | RMP_BMM150_Z_AXIS_CONFIG,
                                .axisSign = RMP_BMM150_X_AXIS_SIGN_CONFIG | RMP_BMM150_Y_AXIS_SIGN_CONFIG | RMP_BMM150_Z_AXIS_SIGN_CONFIG, },
        };

static Magnetometer_Handle_T xdkMagnetometer_BMM150_S = {
        .SensorPtr = (Bmm150Utils_Info_T *) &bmm150Sensor_S,
        .SensorInformation =
                {
                        .sensorID = MAGNETOMETER_BMM150,
                        .initializationStatus = 0x00,
                },
};
/************************************************MAX Handle Info*********************************************************************/
static Max44009Utils_Info_T max44009Sensor_S =
        {
                .interface =
                        {
                                .bus = MAX44009UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x4a,
                                                        },
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_MAX44009_VDD_PORT,
                                .pin = PTD_PIN_MAX44009_VDD_PORT,
                        },
                .interruptPin =
                        {
                                .gpio =
                                        {
                                                .pin = PTD_PIN_MAX44009_INT,
                                                .port = PTD_PORT_MAX44009_INT,
                                        },
                                .risingEdge = UINT8_C(0x00),
                                .fallingEdge = UINT8_C(0x01),
                                .enable = UINT8_C(0x01),
                                .callBack = NULL,
                        },
        };

static LightSensor_Handle_T xdkLightSensor_MAX44009_S = {
        .SensorPtr = (Max44009Utils_Info_T *) &max44009Sensor_S,
        .SensorInformation =
                {
                        .sensorID = LIGHTSENSOR_MAX44009,
                        .initializationStatus = 0x00,
                },
};

/************************************************BMI Handle Info*********************************************************************/
static Bmi160Utils_Info_T bmi160AccelSensor_S =
        {
                .interface =
                        {
                                .bus = BMI160UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x69,
                                                        },
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_BMI160_VDD,
                                .pin = PTD_PIN_BMI160_VDD,
                        },
                .interruptPin1 =
                        {
                                .gpio =
                                        {
                                                .port = PTD_PORT_BMI160_INT1,
                                                .pin = PTD_PIN_BMI160_INT1,
                                        },
                                .risingEdge = 0,
                                .fallingEdge = 0,
                                .enable = 0,
                                .callBack = 0,
                        },
                .interruptPin2 =
                        {
                                .gpio =
                                        {
                                                .port = PTD_PORT_BMI160_INT2,
                                                .pin = PTD_PIN_BMI160_INT2,
                                        },
                                .risingEdge = 0,
                                .fallingEdge = 0,
                                .enable = 0,
                                .callBack = 0,
                        },
                .remap =
                        {
                                .axisConfig = RMP_BMIACCEL_X_AXIS_CONFIG | RMP_BMIACCEL_Y_AXIS_CONFIG | RMP_BMIACCEL_Z_AXIS_CONFIG,
                                .axisSign = RMP_BMIACCEL_X_AXIS_SIGN_CONFIG | RMP_BMIACCEL_Y_AXIS_SIGN_CONFIG | RMP_BMIACCEL_Z_AXIS_SIGN_CONFIG,
                        },

        };

static Accelerometer_Handle_T xdkAccelerometers_BMI160_S = {
        .SensorPtr = (Bmi160Utils_Info_T *) &bmi160AccelSensor_S,
        .SensorInformation =
                {
                        .sensorID = ACCELEROMETER_BMI160,
                        .initializationStatus = 0x00,
                },
};

static Bmi160Utils_Info_T bmi160GyroSensor_S =
        {
                .interface =
                        {
                                .bus = BMI160UTILS_I2C_BUS,
                                .type =
                                        {
                                                .i2cBus =
                                                        {
                                                                .I2C_Channel = I2CCHANNEL_0,
                                                                .I2C_Address = 0x69,
                                                        },
                                        },
                        },
                .sensorPower =
                        {
                                .port = PTD_PORT_BMI160_VDD,
                                .pin = PTD_PIN_BMI160_VDD,
                        },
                .remap =
                        {
                                .axisConfig = RMP_BMIGYRO_X_AXIS_CONFIG | RMP_BMIGYRO_Y_AXIS_CONFIG | RMP_BMIGYRO_Z_AXIS_CONFIG,
                                .axisSign = RMP_BMIGYRO_X_AXIS_SIGN_CONFIG | RMP_BMIGYRO_Y_AXIS_SIGN_CONFIG | RMP_BMIGYRO_Z_AXIS_SIGN_CONFIG,
                        },

        };

static Gyroscope_Handle_T xdkGyroscope_BMI160_S = {
        .sensorPtr = (Bmi160Utils_Info_T *) &bmi160GyroSensor_S,
        .sensorInfo =
                {
                        .sensorID = BMI160_GYRO_SENSOR,
                        .initializationStatus = 0x00,
                },
};

static VirtualSensor_bsxBaseSensor_T xdkBsxBaseSensors_S =
        {
                .accelerometerHandle = (Accelerometer_HandlePtr_T) & xdkAccelerometers_BMI160_S,
                .magnetometerHandle = (Magnetometer_HandlePtr_T) & xdkMagnetometer_BMM150_S,
                .gyroscopeHandle = (Gyroscope_HandlePtr_T) & xdkGyroscope_BMI160_S,
        };

static VirtualSensor_FPBaseSensor_T xdkFingerprintBaseSensor_S =
        {
                .magnetometerHandle = (Magnetometer_HandlePtr_T) & xdkMagnetometer_BMM150_S,
                .accelerometerHandle = NULL,
        };
/************************************************END of Handle Info******************************************************************/

Accelerometer_HandlePtr_T xdkAccelerometers_BMA280_Handle = &xdkAccelerometers_BMA280_S;
Gyroscope_HandlePtr_T xdkGyroscope_BMG160_Handle = &xdkGyroscope_BMG160_S;
LightSensor_HandlePtr_T xdkLightSensor_MAX44009_Handle = &xdkLightSensor_MAX44009_S;
Accelerometer_HandlePtr_T xdkAccelerometers_BMI160_Handle = &xdkAccelerometers_BMI160_S;
Gyroscope_HandlePtr_T xdkGyroscope_BMI160_Handle = &xdkGyroscope_BMI160_S;
Magnetometer_HandlePtr_T xdkMagnetometer_BMM150_Handle = &xdkMagnetometer_BMM150_S;
Environmental_HandlePtr_T xdkEnvironmental_BME280_Handle = &xdkEnvironmental_BME280_S;
Orientation_HandlePtr_T xdkOrientationSensor_Handle = (Orientation_HandlePtr_T) & xdkBsxBaseSensors_S;
AbsoluteHumidity_HandlePtr_T xdkHumiditySensor_Handle = (AbsoluteHumidity_HandlePtr_T) & xdkEnvironmental_BME280_S;
CalibratedAccel_HandlePtr_T xdkCalibratedAccelerometer_Handle = (CalibratedAccel_HandlePtr_T) & xdkBsxBaseSensors_S;
CalibratedGyro_HandlePtr_T xdkCalibratedGyroscope_Handle = (CalibratedGyro_HandlePtr_T) & xdkBsxBaseSensors_S;
CalibratedMag_HandlePtr_T xdkCalibratedMagnetometer_Handle = (CalibratedMag_HandlePtr_T) & xdkBsxBaseSensors_S;
StepCounter_HandlePtr_T xdkStepCounterSensor_Handle = (StepCounter_HandlePtr_T) & xdkAccelerometers_BMI160_S;
FingerPrint_HandlePtr_T xdkFingerprintSensor_Handle = (FingerPrint_HandlePtr_T) & xdkFingerprintBaseSensor_S;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/** ************************************************************************* */
