/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
 /**  @file
 *
 *  @brief
 *  	  This module defines the hardware/device specific configuration
 * ****************************************************************************/

/* module includes ********************************************************** */

/* system header files */
#include "BCDS_Basics.h"
#include "PTD_portDriver_ph.h"
#include "gpio_efm32gg.h"
#include "gpio.h"
#include "I2C_ih.h"
#include "I2C_ph.h"

/* constant definitions ***************************************************** */

/* type definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/**Handle for I2C channel0*/
static I2C_init_t i2c_channel0HandleInfo =
        {
                .i2cChannel = I2CCHANNEL_0,
                .i2cDataPinConfig =
                        {
                                .port = PTD_PORT_I2C0_SDA2,
                                .pin = PTD_PIN_I2C0_SDA2,
                                .modeEnable = PTD_MODE_I2C0_SDA2,
                                .doutEnable = PTD_DOUT_I2C0_SDA2,
                        },
                .i2cClockPinConfig =
                        {
                                .port = PTD_PORT_I2C0_SCL2,
                                .pin = PTD_PIN_I2C0_SCL2,
                                .modeEnable = PTD_MODE_I2C0_SCL2,
                                .doutEnable = PTD_DOUT_I2C0_SCL2,
                        },
                .i2cLocation = I2C0_LOCATION
        };

/**Handle for I2C channel1 of Extension bus*/
static I2C_init_t i2c_channel1HandleInfo=
{
    .i2cChannel = I2CCHANNEL_1,
    .i2cDataPinConfig =
    {
        .port=PTD_PORT_EXTENSION_I2C1_SDA,
        .pin=PTD_PIN_EXTENSION_I2C1_SDA,
        .modeEnable=PTD_MODE_EXTENSION_I2C1_SDA,
        .doutEnable=PTD_DOUT_EXTENSION_I2C1_SDA,
    },
    .i2cClockPinConfig =
    {
        .port=PTD_PORT_EXTENSION_I2C1_SCL,
        .pin=PTD_PIN_EXTENSION_I2C1_SCL,
        .modeEnable=PTD_MODE_EXTENSION_I2C1_SCL,
        .doutEnable=PTD_DOUT_EXTENSION_I2C1_SCL,
    },
    .i2cLocation=I2C1_LOCATION
};

/** GPIO handle for Red LED */
static GPIO_handleInfo_t gpioRedLed_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_LED_RED,
                .bitIndex = PTD_PIN_LED_RED,
                .Direction = GPIO_DIRECTION_OUTPUT,
                .lockDirection = true,
                .initDone = false,
        };

/** GPIO handle for Orange LED */
static GPIO_handleInfo_t gpioOrangeLed_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_LED_ORANGE,
                .bitIndex = PTD_PIN_LED_ORANGE,
                .Direction = GPIO_DIRECTION_OUTPUT,
                .lockDirection = true,
                .initDone = false,
        };

/** GPIO handle for Yellow LED */
static GPIO_handleInfo_t gpioYellowLed_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_LED_YELLOW,
                .bitIndex = PTD_PIN_LED_YELLOW,
                .Direction = GPIO_DIRECTION_OUTPUT,
                .lockDirection = true,
                .initDone = false,
        };

/**  handle for GPIO pin(gpioPortE) of Extension bus as input */
static GPIO_handleInfo_t externalInput_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_EXTENSION_GPIO_IN_OUT_1,
                .bitIndex = PTD_PIN_EXTENSION_GPIO_IN_OUT_1,
                .Direction = GPIO_DIRECTION_INPUT,
                .lockDirection = false,
                .initDone = false,
        };

/** handle for GPIO pin(gpioPortA) of Extension bus as output */
static GPIO_handleInfo_t externalOutput_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_EXTENSION_GPIO_IN_OUT_0,
                .bitIndex = PTD_PIN_EXTENSION_GPIO_IN_OUT_0,
                .Direction = GPIO_DIRECTION_OUTPUT,
                .lockDirection = false,
                .initDone = false,
        };

/** GPIO handle for Button0 */
static GPIO_handleInfo_t gpioButton1_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_PUSH_BUTTON_1,
                .bitIndex = PTD_PIN_PUSH_BUTTON_1,
                .Direction = GPIO_DIRECTION_INPUT,
                .lockDirection = true,
                .initDone = false,
        };

/** GPIO handle for Button1 */
static GPIO_handleInfo_t gpioButton2_HandleInfo =
        {
                .magicWord = GPIO_HANDLE_MAGIC_WORD,
                .port = PTD_PORT_PUSH_BUTTON_2,
                .bitIndex = PTD_PIN_PUSH_BUTTON_2,
                .Direction = GPIO_DIRECTION_INPUT,
                .lockDirection = true,
                .initDone = false,
        };

GPIO_handle_tp const gpioRedLed_Handle = (GPIO_handle_tp)(&gpioRedLed_HandleInfo);
GPIO_handle_tp const gpioYellowLed_Handle = (GPIO_handle_tp)(&gpioYellowLed_HandleInfo);
GPIO_handle_tp const gpioOrangeLed_Handle = (GPIO_handle_tp)(&gpioOrangeLed_HandleInfo);
I2C_init_tp const i2cChannel0_Handle = (I2C_init_tp)(&i2c_channel0HandleInfo);
I2C_init_tp const i2cChannel1_Handle = (I2C_init_tp)(&i2c_channel1HandleInfo);
GPIO_handle_tp const externalInput_Handle = (GPIO_handle_tp)(&externalInput_HandleInfo);
GPIO_handle_tp const externalOutput_Handle = (GPIO_handle_tp)(&externalOutput_HandleInfo);
GPIO_handle_tp const gpioButton1_Handle = (GPIO_handle_tp)(&gpioButton1_HandleInfo);
GPIO_handle_tp const gpioButton2_Handle = (GPIO_handle_tp)(&gpioButton2_HandleInfo);

