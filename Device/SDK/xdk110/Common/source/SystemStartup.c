/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

 /**  @file
 *
 * This module contains "Main" routine  and the APIs , in which user can easily integrate application
 * specific routines
 *
 * ****************************************************************************/
//lint -emacro(506,*) /* Suppressing "Constant value Boolean" lint warning*/

/* module includes ********************************************************** */

/* public header file */
#include "XdkSystemStartup.h"

/* system header files */
#include "BCDS_Basics.h"
#include <stdio.h>

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include <timers.h>
#include "XdkBoardInitialization.h"
#include "XdkGpioConfig.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "BCDS_Assert.h"



/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * Function to initialize the application process. Inside this application specific initializations and routines of different modules are handled
 * @brief  the function is called from the main by timer ,it is a wrapper of the function in which the application
 *          specific initializations and routines of modules are handled
 *
 * @param[in]  OS_timerHandle_tp freertos allows timertask prototype with argument of this type
 *
 */
extern void appInitSystem(xTimerHandle xTimer);
/**
 * @brief The main function initializes the hardware and starts the RTOS scheduler
 */
void systemStartup(void)
{
    uint32_t Ticks = UINT32_C(0);

    boardInit();

    /* Custom startup is disabled by default. User can customize the start up by overriding the CUSTOM_STARTUP macro
     * used in BCDS_SYSTEM_STARTUP_METHOD in the Settings.mk*/
#if defined (DEFAULT_STARTUP)
    /* Initialize Hardware - initializes chipinit, usb,spi,i2c.. */
    boardStartup();
#endif
    if (Ticks != UINT32_MAX) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
    {
        Ticks /= portTICK_RATE_MS;
    }
    if (UINT32_C(0) == Ticks) /* ticks cannot be 0 in FreeRTOS timer. So ticks is assigned to 1 */
    {
        Ticks = UINT32_C(1);
    }

    /* create timer with call back function( which initializes wifi, sensors...) */
    xTimerHandle SSU_timerHandleSysInit = xTimerCreate(
            (const char * const ) "system init", Ticks, TIMER_AUTORELOAD_OFF, 0,
            appInitSystem);
    /* start timer */
    if (xTimerStart(SSU_timerHandleSysInit, TIMERBLOCKTIME) != pdTRUE)
    {
        assert(false);
    }
    /* start scheduler */
    vTaskStartScheduler();
}

/**
 * This function is called when is stack overflows and the program gets into infinite loop.
 *
 * @param[in]   Task handle
 * @param[in]   Task name
 *
 */
void vApplicationStackOverflowHook(xTaskHandle *pxTask,
        signed char *pcTaskName)
{
    (void) pxTask; /* To suppress "unused parameter" warning */
    (void) pcTaskName;

    printf("----- STACK OVERFLOW -----Task Name:%s-----Current Task Handle:0x%x\n", pcTaskName, (int) pxTask);

    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_YELLOW));
    PTD_pinOutSet(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);

    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_ORANGE));
    PTD_pinOutSet(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);

    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(LED_RED));
    PTD_pinOutClear(PTD_PORT_LED_RED, PTD_PIN_LED_RED);

    /* This function will be called if a task overflows its stack, if
     configCHECK_FOR_STACK_OVERFLOW != 0.  It might be that the function
     parameters have been corrupted, depending on the severity of the stack
     overflow.  When this is the case pxCurrentTCB can be inspected in the
     debugger to find the offending task. */
    for (;;)
    {
        ; /* endless loop */
    }
}

/* global functions ********************************************************* */

/** ************************************************************************* */
