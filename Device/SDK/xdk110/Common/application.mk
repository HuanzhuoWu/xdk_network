##################################################################
#  Makefile for generating the binary file of the application    #
##################################################################
MAKE_FILE_ROOT ?= .

include $(MAKE_FILE_ROOT)/Settings.mk
include $(MAKE_FILE_ROOT)/Libraries.mk

# Compiler settings   
export BCDS_CFLAGS_COMMON += -std=c99 -Wall -Wextra -Wstrict-prototypes -D$(BCDS_DEVICE_ID) -D BCDS_TARGET_EFM32 \
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections \
$(SERVAL_FEATURES_CONFIG) $(CYCURTLS_FEATURES_CONFIG) -D$(BCDS_HW_VERSION) -D$(BCDS_SYSTEM_STARTUP_METHOD) \
-DBLE_DEVICENAME=\"$(BCDS_UNIQUE_NAME)\" -DUNIQUE_MAC=$(BCDS_UNIQUE_MAC) -DENABLE_DMA -DARM_MATH_CM3 

export BCDS_CFLAGS_DEBUG_COMMON ?= $(BCDS_CFLAGS_COMMON) -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG) 
export BCDS_CFLAGS_RELEASE_COMMON ?= $(BCDS_CFLAGS_COMMON) -O0 -DNDEBUG

LDFLAGS_DEBUG += -Xlinker -Map=$(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).map \
-mcpu=cortex-m3 -mthumb -T $(BCDS_XDK_LD_FILE) -Wl,--gc-sections

LDFLAGS_RELEASE += -Xlinker -Map=$(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).map \
-mcpu=cortex-m3 -mthumb -T $(BCDS_XDK_LD_FILE) -Wl,--gc-sections

ASMFLAGS += -x assembler-with-cpp -Wall -Wextra -mcpu=cortex-m3 -mthumb

LIBS = -Wl,--start-group -lgcc -lc -lm  -Wl,--end-group

#The static libraries of the platform and third party sources are grouped here. Inorder to scan the libraries
#for undefined reference again and again, the libraries are listed between the --start-group and --end-group.
BCDS_DEBUG_LIBS_GROUP = -Wl,--start-group $(BCDS_LIBS_DEBUG) $(BCDS_THIRD_PARTY_LIBS) -Wl,--end-group
BCDS_RELEASE_LIBS_GROUP = -Wl,--start-group $(BCDS_LIBS_RELEASE) $(BCDS_THIRD_PARTY_LIBS) -Wl,--end-group


# Lint flags
LINT_CONFIG = \
	+d$(BCDS_DEVICE_TYPE) +dASSERT_FILENAME $(BCDS_SERVAL_CONFIG_LINT) $(BCDS_CYCURTLS_CONFIG_LINT) \
	+d$(BCDS_TARGET_PLATFORM) +d$(BCDS_DEVICE_ID) +d$(BCDS_HW_VERSION) -e506 \
	$(BCDS_EXTERNAL_EXCLUDES_LINT) $(BCDS_EXTERNAL_INCLUDES_LINT) $(BCDS_LINT_CONFIG) \
	$(BCDS_XDK_INCLUDES_LINT) $(BCDS_LINT_CONFIG_FILE) +dDBG_ASSERT_FILENAME \
  
# Source files
BCDS_XDK_PLATFORM_SOURCE_FILES += \
	$(BCDS_UTILS_DIR)/source/RSM_retargetStdioMessage/retargetio.c \
	$(BCDS_EM_LIB_DIR)/usb/src/em_usbdint.c \
	
# Startup files
BCDS_XDK_APP_STARTUP_FILES = \
	startup_efm32gg.S

# Object files
BCDS_XDK_PLATFORM_C_OBJECT_FILES = $(BCDS_XDK_PLATFORM_SOURCE_FILES:.c=.o)
BCDS_XDK_APP_S_OBJECT_FILES = $(BCDS_XDK_APP_STARTUP_FILES:.S=.o)
BCDS_XDK_APP_C_OBJECT_FILES = $(patsubst $(BCDS_APP_SOURCE_DIR)/%.c, %.o, $(BCDS_XDK_APP_SOURCE_FILES))
BCDS_XDK_APP_LINT_FILES = $(patsubst $(BCDS_APP_SOURCE_DIR)/%.c, %.lob, $(BCDS_XDK_APP_SOURCE_FILES))
BCDS_XDK_APP_OBJECT_FILES = $(BCDS_XDK_PLATFORM_C_OBJECT_FILES) $(BCDS_XDK_APP_C_OBJECT_FILES) $(BCDS_XDK_APP_S_OBJECT_FILES)

#This variable holds the depedency files for the application include files.
BCDS_XDK_APP_DEPENDENCY_FILES = $(addprefix $(BCDS_XDK_APP_DEBUG_OBJECT_DIR)/, $(BCDS_XDK_APP_OBJECT_FILES:.o=.d))
BCDS_XDK_APP_OBJECT_FILES_DEBUG = $(addprefix $(BCDS_XDK_APP_DEBUG_OBJECT_DIR)/, $(BCDS_XDK_APP_OBJECT_FILES))
BCDS_XDK_APP_OBJECT_FILES_RELEASE = $(addprefix $(BCDS_XDK_APP_RELEASE_OBJECT_DIR)/, $(BCDS_XDK_APP_OBJECT_FILES))
BCDS_XDK_LINT_FILES = $(addprefix $(BCDS_XDK_APP_LINT_PATH)/, $(BCDS_XDK_APP_LINT_FILES))

#Create debug binary
.PHONY: debug 
debug: $(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).bin

#Create release binary
.PHONY: release 
release: $(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).bin

#Compile, assemble and link for debug target
#Compile the sources from plaform or library
$(BCDS_XDK_APP_DEBUG_OBJECT_DIR)/%.o: %.c
	@mkdir -p $(@D)
	@echo "Building file $<"
	@$(CC) $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_DEBUG_COMMON) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -c $< -o $@
	
#Compile the sources from application
$(BCDS_XDK_APP_DEBUG_OBJECT_DIR)/%.o: $(BCDS_APP_SOURCE_DIR)/%.c
	@mkdir -p $(@D)
	@echo "Building file $<"
	@$(CC) $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_DEBUG_COMMON) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -c $< -o $@
	
$(BCDS_XDK_APP_DEBUG_OBJECT_DIR)/%.o: %.S
	@mkdir -p $(@D)
	@echo "Assembling $<"
	@$(CC) $(ASMFLAGS) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -c $< -o $@
	
$(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).out: $(BCDS_LIBS_DEBUG) $(BCDS_XDK_APP_OBJECT_FILES_DEBUG)   
	@echo "Creating .out $@"
	@$(CC) $(LDFLAGS_DEBUG) $(BCDS_XDK_APP_OBJECT_FILES_DEBUG) $(BCDS_DEBUG_LIBS_GROUP) $(LIBS) -o $@
	
$(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).bin: $(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).out
	@echo "Boot flag value is $(XDK_FOTA_ENABLED_BOOTLOADER)"
	@echo "Creating binary for debug $@"
	@$(OBJCOPY) -O binary $(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).out $@
	

#Compile, assemble and link for release target

#Compile the sources from plaform or library
$(BCDS_XDK_APP_RELEASE_OBJECT_DIR)/%.o: %.c
	@mkdir -p $(@D)
	@echo "Building file $<"
	@$(CC) $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_RELEASE_COMMON) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -c $< -o $@

#Compile the sources from application
$(BCDS_XDK_APP_RELEASE_OBJECT_DIR)/%.o: $(BCDS_APP_SOURCE_DIR)/%.c
	@mkdir -p $(@D)
	@echo "Building file $<"
	@$(CC) $(DEPEDENCY_FLAGS) $(BCDS_CFLAGS_RELEASE_COMMON) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -c $< -o $@

$(BCDS_XDK_APP_RELEASE_OBJECT_DIR)/%.o: %.S
	@mkdir -p $(@D)
	@echo "Assembling $<"
	@$(CC) $(ASMFLAGS) $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) -c $< -o $@

$(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).out: $(BCDS_XDK_APP_OBJECT_FILES_RELEASE) $(BCDS_LIBS_RELEASE)
	@echo "Creating .out $@"
	@$(CC) $(LDFLAGS_RELEASE) $(BCDS_XDK_APP_OBJECT_FILES_RELEASE) $(BCDS_RELEASE_LIBS_GROUP) $(LIBS) -o $@

$(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).bin: $(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).out
	@echo "Creating binary $@"
	@$(OBJCOPY) -R .usrpg -O binary $(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).out $@
	

#if the header file is changed, compiler considers the change in a header file and compiles. Here no need to clean the application
#to take changes of header file during compilation
-include $(BCDS_XDK_APP_DEPENDENCY_FILES)
	
# Clean project
.PHONY: clean clean_libraries
clean: clean_libraries
	@echo "Cleaning project in app.mk"
	@$(RMDIRS) $(BCDS_XDK_APP_DEBUG_DIR) $(BCDS_XDK_APP_RELEASE_DIR)
	
cdt:
	@echo "cdt"
	$(CC) $(BCDS_CFLAGS_DEBUG_COMMON) $(patsubst %,-I%, $(abspath $(patsubst -I%,%, $(BCDS_XDK_INCLUDES) $(BCDS_EXTERNAL_INCLUDES))))  -E -P -v -dD -c ${CDT_INPUT_FILE}
.PHONY: cdt

#Flash the .bin file to the target
flash_debug_bin: $(BCDS_XDK_APP_DEBUG_DIR)/$(BCDS_APP_NAME).bin
	@$(FLASH_TOOL_PATH) --address $(XDK_APP_ADDRESS) -v -f $< -r
	@echo "Flashing is completed successfully"

flash_release_bin: $(BCDS_XDK_APP_RELEASE_DIR)/$(BCDS_APP_NAME).bin
	@$(FLASH_TOOL_PATH) --address $(XDK_APP_ADDRESS) -v -f $< -r
	@echo "Flashing is completed successfully"

.PHONY: lint		   
lint: $(BCDS_XDK_LINT_FILES)
	@echo "Lint End"

$(BCDS_XDK_APP_LINT_PATH)/%.lob:  $(BCDS_APP_SOURCE_DIR)/%.c
	@echo "===================================="
	@mkdir -p $(@D)
	@echo $@
	@$(LINT_EXE) $(LINT_CONFIG) $< -oo[$@]
	@echo "===================================="
	
.PHONY: cleanlint
cleanlint:
	@echo "Cleaning lint output files"
	@rm -rf $(BCDS_XDK_APP_LINT_PATH)
	