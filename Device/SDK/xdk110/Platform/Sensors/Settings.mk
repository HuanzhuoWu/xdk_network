#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

#The base name of the library that is generated from this package
BCDS_BST_LIB_DIR = $(BCDS_LIBRARIES_DIR)/BSTLib

# The package name. The library name is derived from this package name
BCDS_PACKAGE_NAME = Sensors
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 8
BCDS_SENSORS_HOME = $(CURDIR)
BCDS_DEVICE_TYPE ?=EFM32GG
BCDS_DEVICE_ID ?=EFM32GG390F1024
BCDS_TOOL_DIR ?= $(BCDS_SENSORS_HOME)/../../Tools
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOL_DIR)/gcc-arm-none-eabi/bin

#Lint configuration and exe file paths
#Path where the LINT config files are present(compiler and environment configurations)
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOL_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt

LINT_EXE = $(BCDS_TOOL_DIR)/PCLint/exe/lint-nt.launch

#External Path information
BCDS_LIBRARIES_DIR = $(BCDS_SENSORS_HOME)/../../Libraries
BCDS_PLATFORM_DIR =$(BCDS_SENSORS_HOME)/../../Platform

BCDS_EMLIBS_PATH = $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FREE_RTOS_LIB_DIR = $(BCDS_LIBRARIES_DIR)/FreeRTOS
BCDS_PHERIPHERAL_PATH = $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_UTILS_PATH =  $(BCDS_PLATFORM_DIR)/Utils
BCDS_SENSORS_PATH =  $(BCDS_PLATFORM_DIR)/Sensors
BCDS_BASICS_PATH =  $(BCDS_PLATFORM_DIR)/Basics

# Internal Path Settings
BCDS_SENSORS_INCLUDE_DIR := include
BCDS_SENSORS_SOURCE_DIR := source
BCDS_SENSORS_CONFIG_DIR := config
BCDS_SENSORS_OBJECT_DIR:= object
BCDS_SENSORS_LINT_DIR := lint
BCDS_LINT_CONFIG_FILE := $(BCDS_TOOL_DIR)/PCLint/config/bcds.lnt

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)
BCDS_SENSOR_HARDWARE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)/$(BCDS_HW_VERSION)

BCDS_SENSORS_DEBUG_PATH ?= $(BCDS_SENSORS_HOME)/debug
BCDS_SENSORS_RELEASE_PATH ?= $(BCDS_SENSORS_HOME)/release

BCDS_SENSORS_DEBUG_OBJECT_PATH = $(BCDS_SENSORS_DEBUG_PATH)/$(BCDS_SENSORS_OBJECT_DIR)
BCDS_SENSORS_RELEASE_OBJECT_PATH = $(BCDS_SENSORS_RELEASE_PATH)/$(BCDS_SENSORS_OBJECT_DIR)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

BCDS_SENSOR_UTILS_DIR = $(BCDS_PLATFORM_DIR)/SensorUtils
BCDS_SENSOR_DRIVERS_DIR = $(BCDS_PLATFORM_DIR)/SensorDrivers

#Lint Path
BCDS_SENSORS_DEBUG_LINT_PATH = $(BCDS_SENSORS_DEBUG_PATH)/$(BCDS_SENSORS_LINT_DIR)

BCDS_LIB_SENSORS_BASE_NAME ?= lib$(BCDS_PACKAGE_NAME)
BCDS_SENSORS_DEBUG_LIB_NAME = $(BCDS_LIB_SENSORS_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_SENSORS_RELEASE_LIB_NAME = $(BCDS_LIB_SENSORS_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a
BCDS_SENSORS_DEBUG_LIB   = $(BCDS_SENSORS_DEBUG_PATH)/$(BCDS_SENSORS_DEBUG_LIB_NAME)
BCDS_SENSORS_RELEASE_LIB = $(BCDS_SENSORS_RELEASE_PATH)/$(BCDS_SENSORS_RELEASE_LIB_NAME)

# Define the path for the include directories
BCDS_EXTERNAL_INCLUDES = \
	-I$(BCDS_EMLIBS_PATH)/CMSIS/Include \
	-I$(BCDS_EMLIBS_PATH)/emlib/inc \
	-I$(BCDS_EMLIBS_PATH)/usb/inc \
	-I$(BCDS_EMLIBS_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
	-I$(BCDS_FREE_RTOS_LIB_DIR)/FreeRTOS/source/include \
	-I$(BCDS_FREE_RTOS_LIB_DIR)/FreeRTOS/source/portable/ARM_CM3 \
	-I$(BCDS_PHERIPHERAL_PATH)/include \
	-I$(BCDS_UTILS_PATH)/include \
	-I$(BCDS_SENSORS_PATH)/include \
	-I$(BCDS_PACKAGE_CONFIG_PATH) \
	-I$(BCDS_SENSOR_HARDWARE_CONFIG_PATH) \
	-I$(BCDS_CONFIG_PATH) \
	-I$(BCDS_SENSOR_DRIVERS_DIR)/include \
	-I$(BCDS_SENSOR_UTILS_DIR)/include \
	-I$(BCDS_BASICS_PATH)/include \
	
BCDS_SENSORS_INCLUDES += \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMA2x2_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BME280_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMG160_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMI160_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMM050_driver \


#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -I,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_EXTERNAL_INCLUDES_LINT += -i$(BCDS_LINT_CONFIG_PATH)
BCDS_SENSORS_INCLUDES_LINT := $(subst -I,-i,$(BCDS_SENSORS_INCLUDES))
#-------------------------------------------------------------------------------------------------