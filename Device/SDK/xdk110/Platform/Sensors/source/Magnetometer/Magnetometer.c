/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */

/* own header files */
#include "BCDS_Magnetometer.h"
#include "Magnetometer.h"

/* additional interface header files */
#include "BCDS_Bmm150Utils.h"
#include "bmm050.h"

static const uint8_t modeLookUpTable[MAGNETOMETER_POWERMODE_OUT_OF_RANGE] =
        {
                UINT8_C(BMM050_NORMAL_MODE),
                UINT8_C(BMM050_FORCED_MODE),
                UINT8_C(BMM050_SUSPEND_MODE),
                UINT8_C(BMM050_SLEEP_MODE)
        };

static const uint8_t presetModeLookUpTable[MAGNETOMETER_PRESETMODE_OUT_OF_RANGE] =
        {
                UINT8_C(BMM050_PRESETMODE_LOWPOWER),
                UINT8_C(BMM050_PRESETMODE_REGULAR),
                UINT8_C(BMM050_PRESETMODE_HIGHACCURACY),
                UINT8_C(BMM050_PRESETMODE_ENHANCED)
        };

static const uint8_t dataRateLookUpTable[MAGNETOMETER_DATARATE_OUT_OF_RANGE] =
        {
                UINT8_C(BMM050_DR_10HZ),
                UINT8_C(BMM050_DR_02HZ),
                UINT8_C(BMM050_DR_06HZ),
                UINT8_C(BMM050_DR_08HZ),
                UINT8_C(BMM050_DR_15HZ),
                UINT8_C(BMM050_DR_20HZ),
                UINT8_C(BMM050_DR_25HZ),
                UINT8_C(BMM050_DR_30HZ)
        };

static int8_t mappingEnumForGetFunctions(uint8_t start, uint8_t end, uint8_t getValueToBeTransalated, const uint8_t * lookUpTablePtr)
{
    uint8_t i = UINT8_C(0);
    int8_t apiReturnValue = GET_MAPPING_ERROR;

    for (i = start; i <= end; i++)
    {
        if (getValueToBeTransalated == *(lookUpTablePtr + i))
        {
            apiReturnValue = i;
            break;
        }
    }

    return (apiReturnValue);
}

/**
 * @brief       This function maps error codes returned from BMM150 library to retcode values
 *
 * @param [in]   BMM150_RETURN_FUNCTION_TYPE Return value from BMM150 library
 *
 * @retval      RETCODE_OK              BMM150 sensor API call success
 * @retval      RETCODE_FAILURE         BMM150 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMM150 sensor API call failed because of invalid input parameter
 */
static Retcode_T bmm150LibErrorMapping(BMM050_RETURN_FUNCTION_TYPE BMM150_libReturn)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if (BMM150_SUCCESS == BMM150_libReturn)
    {
        sensorReturnValue = RETCODE_OK;
    }
    else if (E_BMM050_NULL_PTR == BMM150_libReturn)
    {
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (sensorReturnValue);
}

/**
 * @brief       This function used read the sensor data
 *
 * @param [in]   bmm050_mag_s32_data_t structure to hold the data
 *
 * @retval      RETCODE_OK              BMM150 sensor API call success
 * @retval      RETCODE_FAILURE         BMM150 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMM150 sensor API call failed because of invalid input parameter
 */

static Retcode_T readMagData(struct bmm050_mag_s32_data_t *magData)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;
    if (NULL == magData)
    {
        return ((Retcode_T) RETCODE_INVALID_PARAM);
    }
    bmmLibReturn = bmm050_read_mag_data_XYZ_s32(magData);
    sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
    if (sensorReturnValue == RETCODE_OK)
    {
        if (BMM050_OVERFLOW_OUTPUT_S32 == magData->datax)
        {
            magData->datax = BMM150_MAX_LIMIT_XAXIS;
        }
        if (BMM050_OVERFLOW_OUTPUT_S32 == magData->datay)
        {
            magData->datay = BMM150_MAX_LIMIT_YAXIS;
        }
        if (BMM050_OVERFLOW_OUTPUT_S32 == magData->dataz)
        {
            magData->dataz = BMM150_MAX_LIMIT_ZAXIS;
        }
    }
    return (sensorReturnValue);
}


/* Public functions */

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_init(Magnetometer_HandlePtr_T handle)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE_OK);
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        sensorReturnValue = Bmm150Utils_initialize((Bmm150Utils_InfoPtr_T) handle->SensorPtr);
        if ((Retcode_T) RETCODE_OK == sensorReturnValue)
        {
            handle->SensorInformation.initializationStatus = INIT_DONE;
        }
        else
        {
            handle->SensorInformation.initializationStatus = INIT_NOT_DONE;
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_setPowerMode(Magnetometer_HandlePtr_T handle,  Magnetometer_PowerMode_T powerMode)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (powerMode >= MAGNETOMETER_POWERMODE_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        if (powerMode > MAGNETOMETER_BMM150_POWERMODE_SLEEP)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        bmmLibReturn = bmm050_set_functional_state(modeLookUpTable[powerMode]);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_setPresetMode(Magnetometer_HandlePtr_T handle, Magnetometer_PresetMode_T presetMode)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (presetMode >= MAGNETOMETER_PRESETMODE_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        if (presetMode > MAGNETOMETER_BMM150_PRESETMODE_ENHANCED)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        bmmLibReturn = bmm050_set_presetmode(presetModeLookUpTable[presetMode]);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_setDataRate(Magnetometer_HandlePtr_T handle, Magnetometer_DataRate_T datarate)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (datarate >= MAGNETOMETER_DATARATE_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        if (datarate > MAGNETOMETER_BMM150_DATARATE_30HZ)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        bmmLibReturn = bmm050_set_data_rate(dataRateLookUpTable[datarate]);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_getPowerMode(Magnetometer_HandlePtr_T handle, Magnetometer_PowerModePtr_T powerMode)
{
    int8_t tempReturnValue = GET_MAPPING_ERROR;
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    Magnetometer_PowerMode_T tempPowerMode = MAGNETOMETER_POWERMODE_OUT_OF_RANGE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == powerMode))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        bmmLibReturn = bmm050_get_functional_state((uint8_t *) powerMode);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        if ((Retcode_T) RETCODE_OK == sensorReturnValue)
        {
            tempPowerMode = *powerMode;
            tempReturnValue = mappingEnumForGetFunctions((uint8_t) MAGNETOMETER_BMM150_POWERMODE_NORMAL,
                    (uint8_t) MAGNETOMETER_BMM150_POWERMODE_SLEEP, (uint8_t) tempPowerMode, modeLookUpTable);
            if (tempReturnValue != GET_MAPPING_ERROR)
            {
                *powerMode = (Magnetometer_PowerMode_T) tempReturnValue;
            }
            else
            {
                sensorReturnValue = (Retcode_T) RETCODE_NOT_SUPPORTED;
            }
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_getPresetMode(Magnetometer_HandlePtr_T handle, Magnetometer_PresetModePtr_T presetMode)
{
    int8_t tempReturnValue = GET_MAPPING_ERROR;
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    Magnetometer_PresetMode_T tempPresetMode = MAGNETOMETER_PRESETMODE_OUT_OF_RANGE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == presetMode))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        bmmLibReturn = bmm050_get_presetmode((uint8_t *) presetMode);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        if ((Retcode_T) RETCODE_OK == sensorReturnValue)
        {
            tempPresetMode = *presetMode;
            tempReturnValue = mappingEnumForGetFunctions((uint8_t) MAGNETOMETER_BMM150_PRESETMODE_LOWPOWER,
                    (uint8_t) MAGNETOMETER_BMM150_PRESETMODE_ENHANCED, (uint8_t) tempPresetMode, presetModeLookUpTable);
            if (tempReturnValue != GET_MAPPING_ERROR)
            {
                *presetMode = (Magnetometer_PresetMode_T) tempReturnValue;
            }
            else
            {
                sensorReturnValue = (Retcode_T) RETCODE_NOT_SUPPORTED;
            }
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_getDataRate(Magnetometer_HandlePtr_T handle, Magnetometer_DataRatePtr_T datarate)
{
    int8_t tempReturnValue = GET_MAPPING_ERROR;
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    Magnetometer_DataRate_T tempDatarate = MAGNETOMETER_DATARATE_OUT_OF_RANGE;
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_NULL_PTR;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == datarate))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:

        bmmLibReturn = bmm050_get_data_rate((uint8_t *) datarate);
        sensorReturnValue = bmm150LibErrorMapping(bmmLibReturn);
        if ((Retcode_T) RETCODE_OK == sensorReturnValue)
        {
            tempDatarate = *datarate;
            tempReturnValue = mappingEnumForGetFunctions((uint8_t) MAGNETOMETER_BMM150_DATARATE_10HZ, (uint8_t) MAGNETOMETER_BMM150_DATARATE_30HZ, (uint8_t) tempDatarate, dataRateLookUpTable);
            if (tempReturnValue != GET_MAPPING_ERROR)
            {
                *datarate = (Magnetometer_DataRate_T) tempReturnValue;
            }
            else
            {
                sensorReturnValue = (Retcode_T) RETCODE_NOT_SUPPORTED;
            }
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_readXyzLsbData(Magnetometer_HandlePtr_T handle, Magnetometer_XyzDataPtr_T data)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    AxisRemap_Data_T readDataMag = { INT32_C(0), INT32_C(0), INT32_C(0) };

    struct bmm050_mag_s32_data_t magData =
    {   INT32_C(0), INT32_C(0), INT32_C(0), UINT16_C(0),UINT8_C(0)};

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == data))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        sensorReturnValue = readMagData(&magData);
        if (RETCODE_OK == sensorReturnValue)
        {
            readDataMag.x = magData.datax;
            readDataMag.y = magData.datay;
            readDataMag.z = magData.dataz;
            sensorReturnValue = Bmm150Utils_remapAxis((Bmm150Utils_InfoPtr_T) handle->SensorPtr, &readDataMag);
        }
        if (RETCODE_OK == sensorReturnValue)
        {
            data->xAxisData = readDataMag.x;
            data->yAxisData = readDataMag.y;
            data->zAxisData = readDataMag.z;
            data->resistance = magData.resistance;
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_readXyzTeslaData(Magnetometer_HandlePtr_T handle, Magnetometer_XyzDataPtr_T data)
{
    struct bmm050_mag_s32_data_t magData =
    {   INT32_C(0), INT32_C(0), INT32_C(0), UINT16_C(0),UINT8_C(0)};
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    AxisRemap_Data_T readDataMag = { INT32_C(0), INT32_C(0), INT32_C(0) };

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == data))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        sensorReturnValue = readMagData(&magData);
        if (RETCODE_OK == sensorReturnValue)
        {
            readDataMag.x = magData.datax >> BMM150_CONVERSION_FACTOR;
            readDataMag.y = magData.datay >> BMM150_CONVERSION_FACTOR;
            readDataMag.z = magData.dataz >> BMM150_CONVERSION_FACTOR;
            sensorReturnValue = Bmm150Utils_remapAxis((Bmm150Utils_InfoPtr_T) handle->SensorPtr, &readDataMag);
        }
        if (RETCODE_OK == sensorReturnValue)
        {
            data->xAxisData = readDataMag.x;
            data->yAxisData = readDataMag.y;
            data->zAxisData = readDataMag.z;
            data->resistance = magData.resistance;
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }
    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }
    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Magnetometer.h' */
Retcode_T Magnetometer_deInit(Magnetometer_HandlePtr_T handle)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE_OK);
    }

    switch (handle->SensorInformation.sensorID)
    {
    case MAGNETOMETER_BMM150:
        sensorReturnValue = Bmm150Utils_uninitialize((Bmm150Utils_InfoPtr_T) handle->SensorPtr);
        handle->SensorInformation.initializationStatus = INIT_NOT_DONE;
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}
