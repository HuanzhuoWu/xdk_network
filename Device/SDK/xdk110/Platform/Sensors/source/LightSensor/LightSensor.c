/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_LightSensor.h"

/* additional interface header files */
#include "BCDS_Basics.h"
#include "BCDS_Max44009Utils.h"
#include "BCDS_Max44009.h"
#include <FreeRTOS.h>
#include <timers.h>

#define INIT_DONE       UINT8_C(1)
#define INIT_NOT_DONE   UINT8_C(0)
#define PACKAGE_ID_DEFAULT                UINT32_C(0)/**< default package ID*/
#define GET_MAPPING_ERROR                 INT8_C(-2)

/** function pointer holding handle to application's deferred routine */
static PendedFunction_t maxDeferredCallbackPtr = NULL;

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static const MAX44009_ConfigMode_T manualModeLookUpTable[LIGHTSENSOR_MODE_OUT_OF_RANGE] =
        {
                MAX44009_ENABLE_MANUAL_MODE,
                MAX44009_DISABLE_MANUAL_MODE,
        };

static const MAX44009_ConfigMode_T continousModeLookUpTable[LIGHTSENSOR_MODE_OUT_OF_RANGE] =
        {
                MAX44009_ENABLE_CONTINOUS_MODE,
                MAX44009_DISABLE_CONTINOUS_MODE,
        };

static const MAX44009_IntegrationTime_T integrationtimeLookupTable[LIGHTSENSOR_TIME_OUT_OF_RANGE] =
        {
                MAX44009_800MS, /**< 800 ms integration time(time taken for capturing lux intensity) */
                MAX44009_400MS, /**< 400 ms integration time */
                MAX44009_200MS, /**< 200 ms integration time */
                MAX44009_100MS, /**< 100 ms integration time */
                MAX44009_50MS, /**< 50 ms integration time */
                MAX44009_25MS, /**< 25 ms integration time */
                MAX44009_12P5MS, /**< 12.5 ms integration time */
                MAX44009_6P5MS /**< 6.5 ms integration time */
        };

static const MAX44009_ConfigBrightness_T brightnessLookupTable[LIGHTSENSOR_BRIGHTNESS_OUT_OF_RANGE] =
        {
                MAX44009_NORMAL_BRIGHTNESS, /**< normal brightness */
                MAX44009_HIGH_BRIGHTNESS /**< high brightness */
        };

static const MAX44009_ConfigMode_T interruptStatusLookupTable[LIGHTSENSOR_CONFIG_INTERRUPT_OUT_OF_RANGE] =
        {
                MAX44009_DISABLE_INTERRUPT,
                MAX44009_ENABLE_INTERRUPT
        };

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief BMA280 deferred callback function for channel2 which is responsible to call application's
 * deferred routine.
 *
 * @param [in]   interruptDataPointer pointer for future extension.
 *
 * @param [in]   interruptData input data from ISR.
 *
 */
static void maxDeferredCallback(void *interruptDataPointer, uint32_t interruptData)
{
    /* call application's deferred callback */
    if (NULL != maxDeferredCallbackPtr)
    {
        maxDeferredCallbackPtr(interruptDataPointer, interruptData);
    }
}

/**
 * @brief BMA280 driver callback function for channel1 that will be called during ISR, when deferred
 * application callback is registered.  This is responsible to defer the context.
 */
static void maxIsrCallback(void)
{
    int8_t returnValue = INT32_C(-1);

    /* pending to deferred procedure call to lower the context and to call application's deferred callback */
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    if (xTimerPendFunctionCallFromISR(maxDeferredCallback, NULL, UINT8_C(0), &xHigherPriorityTaskWoken) == pdPASS )
    {
        returnValue = INT32_C(0);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    if (INT32_C(0) != returnValue)
    {
        Retcode_raiseError(returnValue);
    }
}

static int8_t mappingEnumForGetFunctions(uint8_t start, uint8_t end, uint8_t getValueToBeTransalated, const uint8_t * lookUpTablePtr)
{
    uint8_t i = UINT8_C(0);
    int8_t apiReturnValue = GET_MAPPING_ERROR;

    for (i = start; i <= end; i++)
    {
        if (getValueToBeTransalated == *(lookUpTablePtr + i))
        {
            apiReturnValue = i;
            break;
        }
    }

    return (apiReturnValue);
}

/* global functions ********************************************************* */

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_init(LightSensor_HandlePtr_T handle)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE_OK);
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = Max44009Utils_initialize((Max44009Utils_InfoPtr_T) handle->SensorPtr);
        if ((Retcode_T) RETCODE_OK == sensorReturnValue)
        {
            handle->SensorInformation.initializationStatus = INIT_DONE;
        }
        else
        {
            handle->SensorInformation.initializationStatus = INIT_NOT_DONE;
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_setManualMode(LightSensor_HandlePtr_T handle, LightSensor_ConfigMode_T manualMode)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (manualMode >= LIGHTSENSOR_MODE_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        if (manualMode > LIGHTSENSOR_MAX44009_DISABLE_MODE)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        sensorReturnValue = MAX44009_setManualModeReg(manualModeLookUpTable[manualMode]);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_setContinuousMode(LightSensor_HandlePtr_T handle, LightSensor_ConfigMode_T manualMode)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (manualMode >= LIGHTSENSOR_MODE_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        if (manualMode > LIGHTSENSOR_MAX44009_DISABLE_MODE)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        sensorReturnValue = MAX44009_setContinousModeReg(continousModeLookUpTable[manualMode]);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_setIntegrationTime(LightSensor_HandlePtr_T handle, LightSensor_IntegrationTime_T integrationTime)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (integrationTime >= LIGHTSENSOR_TIME_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        if (integrationTime > LIGHTSENSOR_6P5MS)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        sensorReturnValue = MAX44009_setIntegrationTime(integrationtimeLookupTable[integrationTime]);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_setBrightness(LightSensor_HandlePtr_T handle, LightSensor_ConfigBrightness_T brightness)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (brightness >= LIGHTSENSOR_BRIGHTNESS_OUT_OF_RANGE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        if (brightness > LIGHTSENSOR_HIGH_BRIGHTNESS)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NOT_SUPPORTED));
        }
        sensorReturnValue = MAX44009_setCdr(brightnessLookupTable[brightness]);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_getInterruptStatus(LightSensor_HandlePtr_T handle, LightSensor_ConfigInterruptPtr_T interruptStatus)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t tempretVal = GET_MAPPING_ERROR;

    if ((NULL == handle) || (NULL == interruptStatus))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = MAX44009_getInterptStatus((uint8_t *) interruptStatus);
        if (RETCODE_OK == sensorReturnValue)
        {
            tempretVal = mappingEnumForGetFunctions((uint8_t) LIGHTSENSOR_DISABLE_INTERRUPT, (uint8_t) LIGHTSENSOR_ENABLE_INTERRUPT,
                    (uint8_t) *interruptStatus, (uint8_t *) interruptStatusLookupTable);
            if (tempretVal != GET_MAPPING_ERROR)
            {
                *interruptStatus = (LightSensor_ConfigInterrupt_T) tempretVal;
            }
            else
            {
                sensorReturnValue = (Retcode_T) RETCODE_NOT_SUPPORTED;
            }
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_readRawData(LightSensor_HandlePtr_T handle, uint16_t * luxData)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || ( NULL == handle->SensorPtr) || (NULL == luxData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = MAX44009_getSensorData(luxData);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_readLuxData(LightSensor_HandlePtr_T handle, uint32_t * luxData)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint16_t rawData = UINT16_C(0);

    if ((NULL == handle) || ( NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = MAX44009_getSensorData(&rawData);
        if (RETCODE_OK == sensorReturnValue)
        {
            *luxData = MAX44009_getDataInMilliLux(rawData);
        }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_configureThresholdInterrupt(LightSensor_HandlePtr_T handle, uint32_t upperThreshold,
        uint32_t lowerThreshold, uint32_t thresholdTimer)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;
    Max44009Utils_ThresholdIntrConfig_T interruptConfig;

    if ((NULL == handle) || (NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    interruptConfig.upperThreshold = upperThreshold;
    interruptConfig.lowerThreshold = lowerThreshold;
    interruptConfig.thresholdTimer = thresholdTimer;

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = Max44009Utils_configInterrupt((Max44009Utils_InfoPtr_T) handle->SensorPtr, &interruptConfig);
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_registerRealTimeCallback(LightSensor_HandlePtr_T handle, interruptCallback realTimeCallback)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == realTimeCallback))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        {
        Max44009Utils_InfoPtr_T max44009UtilsInstance = (Max44009Utils_InfoPtr_T) handle->SensorPtr;
        max44009UtilsInstance->interruptPin.enable = UINT8_C(1);
        max44009UtilsInstance->interruptPin.callBack = (Gpio_intrCallback) realTimeCallback;
        sensorReturnValue = RETCODE_OK;
    }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_registerDeferredCallback(LightSensor_HandlePtr_T handle, deferredInterruptCallback deferredCallback)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr) || (NULL == deferredCallback))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        {
        Max44009Utils_InfoPtr_T max44009UtilsInstance = (Max44009Utils_InfoPtr_T) handle->SensorPtr;
        max44009UtilsInstance->interruptPin.enable = UINT8_C(1);
        maxDeferredCallbackPtr = deferredCallback;
        max44009UtilsInstance->interruptPin.callBack = (Gpio_intrCallback) maxIsrCallback;
        sensorReturnValue = RETCODE_OK;
    }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_disableInterrupt(LightSensor_HandlePtr_T handle)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || ( NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        {
        Max44009Utils_InfoPtr_T max44009UtilsInstance = (Max44009Utils_InfoPtr_T) handle->SensorPtr;
        sensorReturnValue = Max44009Utils_disableInterrupt(max44009UtilsInstance);
    }
        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}

/* API documentation is in public interface header file 'BCDS_LightSensor.h' */
Retcode_T LightSensor_deInit(LightSensor_HandlePtr_T handle)
{
    Retcode_T sensorReturnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == handle) || (NULL == handle->SensorPtr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (INIT_NOT_DONE == handle->SensorInformation.initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (handle->SensorInformation.sensorID)
    {
    case LIGHTSENSOR_MAX44009:
        sensorReturnValue = Max44009Utils_uninitialize((Max44009Utils_InfoPtr_T) handle->SensorPtr);
        handle->SensorInformation.initializationStatus = INIT_NOT_DONE;

        break;
    default:
        sensorReturnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != sensorReturnValue) && (Retcode_getPackage(sensorReturnValue) == PACKAGE_ID_DEFAULT))
    {
        sensorReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) sensorReturnValue);
    }

    return (sensorReturnValue);
}
/** ************************************************************************* */
