/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 
#ifndef ENVIRONMENTAL_H_
#define ENVIRONMENTAL_H_

/* private type and macro definitions */

/* constant definitions ***************************************************** */
#define INIT_DONE                         UINT8_C(1)
#define INIT_NOT_DONE                     UINT8_C(0)

#define TEMPERATURE                       UINT8_C(0)
#define PRESSURE                          UINT8_C(1)
#define HUMIDITY                          UINT8_C(2)

#define PACKAGE_ID_DEFAULT                UINT32_C(0) /**< default package ID*/
#define GET_MAPPING_ERROR                 INT8_C(-2)
#define GET_MODE                          UINT8_C(1)
#define GET_STANDBY_TIME                  UINT8_C(2)

/** output value from BME280 library is 42313.value 42313 = 42313/1024 = 41.321 %rH */
#define ENVI_BME280_HUM_RH_CONV           UINT32_C(1024)

/** Output value from BME280 library is "5123" equals 51.23 DegC.
 * 5213 = 5123/100 DegC = (5123/100)*1000 milliDegC so conversion factor is 10*/
#define ENVI_BME280_TEMP_MIILIDEG_CONV    INT32_C(10)

#endif /* ENVIRONMENTAL_H_*/