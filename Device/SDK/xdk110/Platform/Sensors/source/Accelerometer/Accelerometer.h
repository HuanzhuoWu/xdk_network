/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef ACCELEROMETER_H_
#define ACCELEROMETER_H_

/* private type and macro definitions */

/* constant definitions ***************************************************** */
#define ENABLE_UNDERSAMPLING              UINT8_C(1)
#define BMA2X2_FILTERED_LOW_BANDWIDTH     UINT8_C(0) /*< used to set low bandwidth*/
#define INIT_DONE                         UINT8_C(1)
#define INIT_NOT_DONE                     UINT8_C(0)
#define GET_MAPPING_ERROR                 INT8_C(-2)
#define PACKAGE_ID_DEFAULT                UINT32_C(0) /**< default package ID*/

/** macro used to for converting LSB to mg */
#define BMA2X2_SUPPORT_2G_RANGE           UINT8_C(2)
#define BMA2X2_SUPPORT_4G_RANGE           UINT8_C(4)
#define BMA2X2_SUPPORT_8G_RANGE           UINT8_C(8)
#define BMA2X2_SUPPORT_16G_RANGE          UINT8_C(16)
#define BMI160_SUPPORT_2G_RANGE           UINT8_C(2)
#define BMI160_SUPPORT_4G_RANGE           UINT8_C(4)
#define BMI160_SUPPORT_8G_RANGE           UINT8_C(8)
#define BMI160_SUPPORT_16G_RANGE          UINT8_C(16)

/** macro used for getting one bit resolution on a 11 bit data */
#define BMA2X2_ONEBITRESOLUTION                  INT32_C(8192)
/** macro used for converting g to mg */
#define ACCEL_CONV_G_MG                         INT32_C(1000)
/** macro used for getting one bit resolution on a 16 bit accel data */
#define BMI160_ONEBITRESOLUTION                 UINT32_C(32768)

#endif /* ACCELEROMETER_H_*/

