/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef GYROSCOPE_H_
#define GYROSCOPE_H_

/* constant definitions ***************************************************** */
#define FILTERED_LOW_BANDWIDTH           UINT8_C(0) /*< used to set low bandwidth*/
#define INIT_DONE                        UINT8_C(1)
#define INIT_NOT_DONE                    UINT8_C(0)
#define GET_MAPPING_ERROR                INT8_C(-2)

/** macros used for converting BMI160 gyro raw data to radians data */
#define BMI160S_GYRO_CONVERSION_FACTOR           INT32_C(1000)
#define BMI160S_CONVERSION_FACTOR                UINT32_C(1000)

/** macros used for getting one bit resolution on a 16 bit gyro data */
#define BMI160S_GYRO_ONEBITRESOLUTION            INT32_C(32768)
#define BMG160S_GYRO_CONVERSION_FACTOR           INT32_C(1000)
#define BMG160S_GYRO_ONEBITRESOLUTION            INT32_C(32768)

/** macros used for settings the ranges for the power mode */
#define BMG160MODE_NORMAL                UINT8_C(0)
#define BMG160MODE_SUSPEND               UINT8_C(2)

/** macros used for settings the ranges for the Gyroscope BMG160*/
#define BMG160SUPPORT_125S_RANGE         UINT16_C(125)
#define BMG160SUPPORT_250S_RANGE         UINT16_C(250)
#define BMG160SUPPORT_500S_RANGE         UINT16_C(500)
#define BMG160SUPPORT_1000S_RANGE        UINT16_C(1000)
#define BMG160SUPPORT_2000S_RANGE        UINT16_C(2000)
#define BMG160SUPPORT_BSTLIB_125S_RANGE  UINT8_C(0x04)
#define BMG160SUPPORT_BSTLIB_250S_RANGE  UINT8_C(0x03)
#define BMG160SUPPORT_BSTLIB_500S_RANGE  UINT8_C(0x02)
#define BMG160SUPPORT_BSTLIB_1000S_RANGE UINT8_C(0x01)
#define BMG160SUPPORT_BSTLIB_2000S_RANGE UINT8_C(0x00)

/** macros used for settings the ranges for the Gyroscope BMI160*/
#define BMI160SUPPORT_125S_RANGE         UINT16_C(125)
#define BMI160SUPPORT_250S_RANGE         UINT16_C(250)
#define BMI160SUPPORT_500S_RANGE         UINT16_C(500)
#define BMI160SUPPORT_1000S_RANGE        UINT16_C(1000)
#define BMI160SUPPORT_2000S_RANGE        UINT16_C(2000)

/** macros used for settings the auto sleep duration ranges for the Gyroscope*/
#define C_BMG160_NO_SLEEP_DURN_U8X      UINT8_C(0)
#define C_BMG160_4MS_SLEEP_DURN_U8X     UINT8_C(1)
#define C_BMG160_5MS_SLEEP_DURN_U8X     UINT8_C(2)
#define C_BMG160_8MS_SLEEP_DURN_U8X     UINT8_C(3)
#define C_BMG160_10MS_SLEEP_DURN_U8X    UINT8_C(4)
#define C_BMG160_15MS_SLEEP_DURN_U8X    UINT8_C(5)
#define C_BMG160_20MS_SLEEP_DURN_U8X    UINT8_C(6)

/** macros used for output data rate ranges for the Gyroscope*/
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_25HZ    UINT8_C(0x06)
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_50HZ    UINT8_C(0x07 )
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_100HZ   UINT8_C(0x08)
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_200HZ   UINT8_C(0x09 )
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_400HZ   UINT8_C(0x0A )
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_800HZ   UINT8_C(0x0B )
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_1600HZ  UINT8_C(0x0C)
#define BCDS_BMI160_GYRO_OUTPUT_DATA_RATE_3200HZ  UINT8_C(0x0D )

/* macro used to defined here for get Gyro power mode representing PMU status register, since the bmi160.h file
does not give macro it has to be removed if the BMI160 defines the macro in their header file*/
#define BCDS_BMI160_GYRO_MODE_SUSPEND       UINT8_C(0x00)
#define BCDS_BMI160_GYRO_MODE_NORMAL        UINT8_C(0x01)
#define BCDS_BMI160_GYRO_MODE_FASTSTARTUP   UINT8_C(0x03)

#define BCDS_BMI160_GYRO_RANGE_2000_DEG_SEC  UINT8_C(0x00)
#define BCDS_BMI160_GYRO_RANGE_1000_DEG_SEC  UINT8_C(0x01)
#define BCDS_BMI160_GYRO_RANGE_500_DEG_SEC   UINT8_C(0x02)
#define BCDS_BMI160_GYRO_RANGE_250_DEG_SEC   UINT8_C(0x03)
#define BCDS_BMI160_GYRO_RANGE_125_DEG_SEC   UINT8_C(0x04)

#define BCDS_BMG160_NO_AUTO_SLEEP_DURN_U8X     UINT8_C(0)
#define BCDS_BMG160_4MS_AUTO_SLEEP_DURN_U8X    UINT8_C(1)
#define BCDS_BMG160_5MS_AUTO_SLEEP_DURN_U8X    UINT8_C(2)
#define BCDS_BMG160_8MS_AUTO_SLEEP_DURN_U8X    UINT8_C(3)
#define BCDS_BMG160_10MS_AUTO_SLEEP_DURN_U8X   UINT8_C(4)
#define BCDS_BMG160_15MS_AUTO_SLEEP_DURN_U8X   UINT8_C(5)
#define BCDS_BMG160_20MS_AUTO_SLEEP_DURN_U8X   UINT8_C(6)
#define BCDS_BMG160_40MS_AUTO_SLEEP_DURN_U8X   UINT8_C(7)

#define PACKAGE_ID_DEFAULT                UINT32_C(0)/**< default package ID*/

#endif /* GYROSCOPE_H_ */
