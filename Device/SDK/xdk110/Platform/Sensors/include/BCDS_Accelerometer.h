/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    Accelerometer Accelerometer
 *	@ingroup	 advanced_sensor_api
 *
 *  @{
 *  @brief Accelerometer Sensor Interface
 *  @details
 *  											- Accelerometer_init()
 *  											- Accelerometer_setBandwidth()
 *  											- Accelerometer_getBandwidth()
 *  											- Accelerometer_setRange()
 *  											- Accelerometer_getRange()
 *  											- Accelerometer_setMode()
 *  											- Accelerometer_getMode()
 *  											- Accelerometer_setSleepDuration()
 *  											- Accelerometer_getSleepDuration()
 *  											- Accelerometer_readXyzLsbValue()
 *  											- Accelerometer_readXyzGValue()
 *  											- Accelerometer_deInit()
 *  											- Accelerometer_configInterrupt ()
 *  											- Accelerometer_disableInterrupt()
 *  											- Accelerometer_regRealTimeCallback ()
 *  											- Accelerometer_regDeferredCallback ()
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_ACCELEROMETER_H_
#define BCDS_ACCELEROMETER_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/* public type and macro definitions */

/**
 * @brief This data type represents a function pointer that is used to register realtime interrupt callback
 */
typedef void (*accelRealTimeCallback)(void);

/**@brief This data type represents a function pointer that is used to register deferred  interrupt callback */
typedef void (*accelDeferredTimeCallback)(void *, uint32_t param);

/**@brief These below macros shall be used by the user to enable/disable interrupt,
 *        when the user call the accelerometerConfigInterrupt() API
 */
#define ACCELEROMETER_PARAMETER_ENABLE  UINT8_C(0x01)
#define ACCELEROMETER_PARAMETER_DISABLE UINT8_C(0x00)

/**   enum values to represent Bandwidth supported by the  accelerometer */
enum Accelerometer_Bandwidth_E
{
    ACCELEROMETER_BMA280_BANDWIDTH_7_81HZ, /**< ID to represent bma280 accel sensor bandwidth 7.81  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_15_63HZ, /**< ID to represent bma280 accel sensor bandwidth 15.63  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_31_25HZ, /**< ID to represent bma280 accel sensor bandwidth 31.25  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_62_50HZ, /**< ID to represent bma280 accel sensor bandwidth 62.50  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_125HZ, /**< ID to represent bma280 accel sensor bandwidth 125  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_250HZ, /**< ID to represent bma280 accel sensor bandwidth 250  HZ */
    ACCELEROMETER_BMA280_BANDWIDTH_500HZ, /**< ID to represent bma280 accel sensor bandwidth 500  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_0_39HZ,/**< ID to represent bmi160 accel sensor bandwidth 0.39  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_0_78HZ, /**< ID to represent bmi160 accel sensor bandwidth 0.78  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_1_56HZ, /**< ID to represent bmi160 accel sensor bandwidth 1.56  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_3_12HZ, /**< ID to represent bmi160 accel sensor bandwidth 3.12  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_6_25HZ, /**< ID to represent bmi160 accel sensor bandwidth 6.25  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_12_5HZ, /**< ID to represent bmi160 accel sensor bandwidth 12.5  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_25HZ, /**< ID to represent bmi160 accel sensor bandwidth 25  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_50HZ, /**< ID to represent bmi160 accel sensor bandwidth 50  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_100HZ, /**< ID to represent bmi160 accel sensor bandwidth 100  HZ */
    ACCELEROMETER_BMI160_BANDWIDTH_200HZ, /**< ID to represent bmi160 accel sensor bandwidth 200  HZ */
    ACCELEROMETER_BANDWIDTH_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_Bandwidth_E Accelerometer_Bandwidth_T, *Accelerometer_BandwidthPtr_T;


/** enum values to represent Range supported by the  accelerometer */
enum Accelerometer_Range_E
{
    ACCELEROMETER_BMA280_RANGE_2G, /**< ID to represent bma280 range to +/- 2G mode */
    ACCELEROMETER_BMA280_RANGE_4G,  /**< ID to represent bma280 range to +/- 4G mode */
    ACCELEROMETER_BMA280_RANGE_8G,  /**< ID to represent bma280 range to +/- 8G mode */
    ACCELEROMETER_BMA280_RANGE_16G, /**< ID to represent bma280 range to +/- 16G mode */
    ACCELEROMETER_BMI160_RANGE_2G,  /**< ID to represent bmi160 range to +/- 2G mode */
    ACCELEROMETER_BMI160_RANGE_4G,  /**< ID to represent bmi160 range to +/- 4G mode */
    ACCELEROMETER_BMI160_RANGE_8G,  /**< ID to represent bmi160 range to +/- 8G mode */
    ACCELEROMETER_BMI160_RANGE_16G, /**< ID to represent bmi160 range to +/- 16G mode */
    ACCELEROMETER_RANGE_OUT_OF_BOUND /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_Range_E Accelerometer_Range_T, *Accelerometer_RangePtr_T;

/**   enum values to represent Power mode supported by the  accelerometer */
enum Accelerometer_Powermode_E
{
    ACCELEROMETER_BMA280_POWERMODE_NORMAL, /**< ID to represent bma280 Powermode to normal  */
    ACCELEROMETER_BMA280_POWERMODE_LOWPOWER1, /**< ID to represent bma280 Powermode to Lowpower1   */
    ACCELEROMETER_BMA280_POWERMODE_SUSPEND, /**< ID to represent bma280 Powermode to suspend      */
    ACCELEROMETER_BMA280_POWERMODE_DEEPSUSPEND, /**< ID to represent bma280 Powermode to deep suspend */
    ACCELEROMETER_BMA280_POWERMODE_LOWPOWER2, /**< ID to represent bma280 Powermode to Lowpower2    */
    ACCELEROMETER_BMA280_POWERMODE_STANDBY, /**< ID to represent bma280 Powermode to Standby      */
    ACCELEROMETER_BMI160_POWERMODE_NORMAL, /**< ID to represent bmi160 Powermode to normal       */
    ACCELEROMETER_BMI160_POWERMODE_LOWPOWER, /**< ID to represent bmi160 Powermode to Lowpower    */
    ACCELEROMETER_BMI160_POWERMODE_SUSPEND, /**< ID to represent bmi160 Powermode to suspend      */
    ACCELEROMETER_POWERMODE_OUT_OF_RANGE /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_Powermode_E Accelerometer_Powermode_T, *Accelerometer_PowermodePtr_T;

/** enum values to represent various slope interrupt duration for accelerometer */
enum Accelerometer_SlopeInterruptDuration_E
{
    ACCELEROMETER_BMA280_SLOPE_DURATION1, /**< slope threshold interrupt validated considering 1 data point duration  */
    ACCELEROMETER_BMA280_SLOPE_DURATION2, /**< slope threshold interrupt validated considering 2 data point duration  */
    ACCELEROMETER_BMA280_SLOPE_DURATION3, /**< slope threshold interrupt validated considering 3 data point duration  */
    ACCELEROMETER_BMA280_SLOPE_DURATION4, /**< slope threshold interrupt validated considering 4 data point duration  */
    ACCELEROMETER_INVALID_SLOPE_DURATION /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_SlopeInterruptDuration_E Accelerometer_SlopeInterruptDuration_T;

/** enum values to represent various sleep duration applicable only in low power mode  */
enum Accelerometer_SleepDuration_E
{
       ACCELEROMETER_BMA280_SLEEP_DURATION_0_5MS, /**< ID to represent sleep duration of 0.5 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_1MS , /**<ID to represent sleep duration to 1 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_2MS , /**< ID to represent sleep duration to 2 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_4MS , /**< ID to represent sleep duration to 4 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_6MS , /**<ID to represent sleep duration to 6 ms*/
       ACCELEROMETER_BMA280_SLEEP_DURATION_10MS, /**< ID to represent sleep duration to 10 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_25MS, /**< ID to represent sleep duration to 25 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_50MS, /**< ID to represent sleep duration to 50 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_100MS, /**< ID to represent sleep duration to 100 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_500MS, /**< ID to represent sleep duration to 500 ms */
       ACCELEROMETER_BMA280_SLEEP_DURATION_1S,    /**< ID to represent sleep duration to 1 s */
       ACCELEROMETER_SLEEP_DURATION_OUT_OF_RANGE  /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_SleepDuration_E Accelerometer_SleepDuration_T,*Accelerometer_SleepDurationPtr_T;

/**   Enumeration to represent the Accelerometer interrupt channel */
enum Accelerometer_InterruptChannel_E
{
    ACCELEROMETER_BMA280_INTERRUPT_CHANNEL1,
    ACCELEROMETER_BMA280_INTERRUPT_CHANNEL2,
    ACCELEROMETER_INVALID_INTERRUPT_CHANNEL
};
typedef enum Accelerometer_InterruptChannel_E Accelerometer_InterruptChannel_T;

/** enum values to represent accelerometer types supported */
enum Accelerometer_SensorID_E
{
    ACCELEROMETER_BMA280, /**< ID to represents bma280 accelerometer sensor */
    ACCELEROMETER_BMI160, /**< ID to represents bmi160 accelerometer sensor */
    ACCELEROMETER_INVALID_ID = 0XFF /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_SensorID_E Accelerometer_SensorID_T;

/** structure to hold the accelerometer type and its initialization status  */
struct Acceleromter_SensorInfo_S
{
    Accelerometer_SensorID_T sensorID; /**< logical sensor ID to represents physical accelerometer sensor*/
    uint8_t initializationStatus; /**< value to indicate whether the accelerometer sensor is properly initialized before */
};
typedef struct Acceleromter_SensorInfo_S Acceleromter_SensorInfo_T, *Acceleromter_SensorInfoPtr_T;

/** @brief structure to represent corresponding Accelerometer handle 
*/
struct Accelerometer_Handle_S
{
    void *SensorPtr; /**< Pointer to structure containing hardware specific configuration*/
    Acceleromter_SensorInfo_T SensorInformation; /**< Structure to hold logical identity to accelerometer sensor*/
};
typedef struct Accelerometer_Handle_S Accelerometer_Handle_T, *Accelerometer_HandlePtr_T;

/** enum values to represent various type of interrupt supported for accelerometer */
enum Accelerometer_InterruptType_E
{
    ACCELEROMETER_BMA280_NEW_DATA_INTERRUPT, /**< ID to represent bma280 new data interrupt type */
    ACCELEROMETER_BMA280_SLOPE_INTERRUPT, /**< ID to represent bma280 slope interrupt type */
    ACCELEROMETER_INVALID_INTERRUPT /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Accelerometer_InterruptType_E Accelerometer_InterruptType_T;

/** structure used to hold the slop interrupt configuration */
struct Accelerometer_ConfigSlopeIntr_S
{
    uint8_t slopeEnableX; /**< variable to indicate slope interrupt for X axis needs to be enabled or not */
    uint8_t slopeEnableY; /**< variable to indicate slope interrupt for Y axis needs to be enabled or not */
    uint8_t slopeEnableZ; /**< variable to indicate slope interrupt for Z axis needs to be enabled or not */
    uint8_t slopeThreshold; /**< variable holds slope threshold range could is from 0 to 255 */
    Accelerometer_SlopeInterruptDuration_T slopeDuration; /**< Number of data points considered for detecting valid slope threshold interrupt */
};
typedef struct Accelerometer_ConfigSlopeIntr_S Accelerometer_ConfigSlopeIntr_T, *Accelerometer_ConfigSlopeIntrPtr_T;

/** structure required to collect the XYZ axis data */
struct Accelerometer_XyzData_S
{
    int32_t xAxisData;
    int32_t yAxisData;
    int32_t zAxisData;
};
typedef struct Accelerometer_XyzData_S Accelerometer_XyzData_T, *Accelerometer_XyzDataPtr_T;

/**
 * @brief Initializes the accelerometer referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the accelerometer object
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @Warning This function needs to be called before calling any other accelerometer API.
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */

Retcode_T Accelerometer_init(Accelerometer_HandlePtr_T handle);

/**
 * @brief Sets the bandwidth for the accelerometer referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the accelerometer object
 * @param [in]  bandwidth       The desired bandwidth that needs to be set
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_setBandwidth(Accelerometer_HandlePtr_T handle, Accelerometer_Bandwidth_T bandwidth);

/**
 * @brief Reads the bandwidth for the accelerometer referenced by the passed handle.
 *
 * @param [in]  handle        Handle to the accelerometer object
 * @param [in]  bandwidth     Last configured bandwidth of accelerometer sensor
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_getBandwidth(Accelerometer_HandlePtr_T handle, Accelerometer_BandwidthPtr_T bandwidth);

/**
 * @brief Sets the Range for the accelerometer referenced by the passed handle.
 *
 * @param [in]  handle        Handle to the accelerometer object
 * @param [in]  range         The desired range that needs to be set
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_setRange(Accelerometer_HandlePtr_T handle, Accelerometer_Range_T range);

/**
 * @brief Reads the Range for the accelerometer referenced by the passed handle.
 *
 * @param [in] handle       Handle to the accelerometer object
 * @param [in] range        Last configured range of accelerometer sensor
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_getRange(Accelerometer_HandlePtr_T handle, Accelerometer_RangePtr_T range);

/**
 * @brief Sets the SleepDuration for the accelerometer referenced by the passed handle.
 *
 * @param [in] handle                           Handle to the accelerometer object
 * @param [in] lowPowerModeSleepDuration        set value for sleep duration
 * @retval     Retcode_T                        The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_setSleepDuration(Accelerometer_HandlePtr_T handle, Accelerometer_SleepDuration_T lowPowerModeSleepDuration);

/**
 * @brief Reads the last set sleep duration of accelerometer referenced by the passed handle.
 *
 * @param [in] handle       Handle to the accelerometer object
 * @param [in] range        Last configured sleep duration of accelerometer sensor
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_getSleepDuration(Accelerometer_HandlePtr_T handle, Accelerometer_SleepDurationPtr_T lowPowerModeSleepDuration);

/**
 * @brief Sets the powermode for the accelerometer referenced by the passed handle.
 *
 * @param [in]  handle        Handle to the accelerometer object
 * @param [in]  powermode         The desired power mode that needs to be set
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_setMode(Accelerometer_HandlePtr_T handle, Accelerometer_Powermode_T powermode);

/**
 * @brief get the powermode for the accelerometer referenced by the passed handle.
 *
 * @param [in] handle       Handle to the accelerometer object
 * @param [in] powermode    Last configured power mode of accelerometer sensor
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_getMode(Accelerometer_HandlePtr_T handle, Accelerometer_PowermodePtr_T powermode);

/**
 * @brief Reads an LSB xyz value from the accelerometer referenced by the passed handle.
 *
 * @param [in] handle       Handle to the accelerometer object
 * @param [in] value        Parameter in which the accelerometer LSB value of XYZ gets collected
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_readXyzLsbValue(Accelerometer_HandlePtr_T handle, Accelerometer_XyzDataPtr_T value);

/**
 * @brief Reads an milli Gravity values from the accelerometer referenced by the passed handle for the UG correction.
 *
 * @param [in] handle       Handle to the accelerometer object
 * @param [in] value        Parameter in which the accelerometer milli G value of XYZ gets collected
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_readXyzGValue(Accelerometer_HandlePtr_T handle, Accelerometer_XyzDataPtr_T value);

/**
 * @brief DeInitializes the accelerometer referenced by the passed handle.
 *
 * @param [in] handle       Handle to the accelerometer object
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_deInit(Accelerometer_HandlePtr_T handle);

/**
 * @brief Configures the accelerometer Interrupt referenced by the passed handle.
 *
 * @param [in] handle                   Handle to the accelerometer object
 *
 * @param [in] channel                  Channel on which the interrupt line connected to
 *
 * @param [in] interruptType            Type of interrupt requested  for the specified interrupt channel
 *
 * @param [in] InterruptTypeParameters  Parameter related to interrupted type. Note it is void pointer and hence the user has to give corresponding parameters for the selected interrupt y
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_configInterrupt (Accelerometer_HandlePtr_T handle, Accelerometer_InterruptChannel_T channel , Accelerometer_InterruptType_T interruptType, void * InterruptTypeParameters);

/**
 * @brief Disable Interrupt type on the specified channel
 *
 * @param [in] handle                   Handle to the accelerometer object
 *
 * @param [in] channel                  Channel on which the specified interrupt type to be disabled
 *
 * @param [in] interruptType            Type of interrupt to be disabled

 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_disableInterrupt (Accelerometer_HandlePtr_T handle, Accelerometer_InterruptChannel_T channel , Accelerometer_InterruptType_T interruptType);

/**
 * @brief  Register real time user callback function on the specified channel to indicate user that interrupt has occurred
 *
 * @param [in] handle                   Handle to the accelerometer object
 *
 * @param [in] channel                  Channel on which the real time callback to be mapped
 *
 * @param [in] accelRealTimeCallback    user function pointer to be registered for real time callback

 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_regRealTimeCallback (Accelerometer_HandlePtr_T handle, Accelerometer_InterruptChannel_T channel , accelRealTimeCallback realTimeCallback);


/**
 * @brief  Register deferred user callback function on the specified channel to indicate user that interrupt has occurred not
 *         immediately but at deferred context of RTOS
 *
 * @param [in] handle                   Handle to the accelerometer object
 *
 * @param [in] channel                  Channel on which the deferred callback to be mapped
 *
 * @param [in] accelDeferredTimeCallback    user function pointer to be registered for deferred callback

 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Accelerometer_regDeferredCallback (Accelerometer_HandlePtr_T handle, Accelerometer_InterruptChannel_T channel , accelDeferredTimeCallback deferredCallback);

#endif /* BCDS_ACCELEROMETER_H_ */

/**@} */
