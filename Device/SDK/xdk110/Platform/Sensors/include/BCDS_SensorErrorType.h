/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BCDS_SENSOR_ERROR_TYPE_H_
#define BCDS_SENSOR_ERROR_TYPE_H_

/* public interface declaration */
#include "BCDS_Retcode.h"
/* public type and macro definitions */

/** Enum values to represent user level sensor error codes */
enum BCDS_SensorErrorType_E
{
    SENSOR_SUCCESS = 0, /**< API call success */
    SENSOR_ERROR = -1, /**< API call failed */
    SENSOR_INVALID_PARAMETER = -2, /**< API call failed due to passing invalid input parameter*/
    SENSOR_UNSUPPORTED_PARAMETER = -3, /**< API call failed due to passing unsupported input parameter*/
    SENSOR_INIT_NOT_DONE = -4, /**< API call failed due to calling other API without calling sensor initialize API*/
};

typedef enum BCDS_SensorErrorType_E BCDS_SensorErrorType_T;

/* public function prototype declarations */

/**
 * @brief        Function responsible to map the return type of sensor API to user error types
 *
 * @param [in]   Sensor API return value which consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity
 *               and Last 2 LSBytes represents error code)
 *
 * @retval       SENSOR_SUCCESS                    Successfully executed API call
 * @retval       SENSOR_ERROR                      Error occurred during API call execution
 * @retval       SENSOR_INVALID_PARAMETER          Error occurred due to invalid input parameter passed to API call
 * @retval       SENSOR_INIT_NOT_DONE              Error indicating the application not called sensor initialization API before calling other API calls
 * @retval       SENSOR_UNSUPPORTED_PARAMETER      Error indicating the input is not supported for this sensor
 */
BCDS_SensorErrorType_T BCDS_getSensorErrorCode(Retcode_T sensorApiReturnValue);

#endif /* BCDS_SENSOR_ERROR_TYPE_H_*/

/**@} */
