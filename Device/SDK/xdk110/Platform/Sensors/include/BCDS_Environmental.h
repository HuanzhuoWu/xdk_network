/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    Environmental Environmental
 *  @ingroup     advanced_sensor_api
 *
 *  @{
 *  @brief Environmental Sensor Interface
 *  @details
 *  											-	Environmental_init()
 *  											-	Environmental_setPowerMode()
 *  											-	Environmental_getPowerMode()
 *  											-	Environmental_readData()
 *  											-	Environmental_readDataLSB()
 *  											-	Environmental_readTemperatureLSB()
 *  											-	Environmental_readPressureLSB()
 *  											-	Environmental_readHumidityLSB()
 *  											-	Environmental_readTemperature()
 *  											-	Environmental_readPressure()
 *  											-	Environmental_readHumidity()
 *  											-	Environmental_setStandbyDuration()
 *  											-	Environmental_getStandbyDuration()
 *  											-	Environmental_setFilterCoefficient()
 *  											-	Environmental_setOverSamplingTemperature()
 *  											-	Environmental_setOverSamplingPressure()
 *  											-	Environmental_setOverSamplingHumidity()
 *												-	Environmental_deInit()
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_ENVIRONMENTAL_H_
#define BCDS_ENVIRONMENTAL_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/* public type and macro definitions */
/** Data structure for Environmental sensor LSB data */

struct Environmental_LsbData_S
{
    int32_t temperature; /* Temperature raw data   */
    int32_t pressure; /* Pressure raw data  */
    int32_t humidity; /* Humidity raw data  */
};
typedef struct Environmental_LsbData_S Environmental_LsbData_T, *Environmental_LsbDataPtr_T;

/** Data structure for Environmental sensor data in standard units */
struct Environmental_Data_S
{
    int32_t temperature; /* Temperature data in milli degree Celsius  */
    uint32_t pressure; /* Pressure data in Pascal */
    uint32_t humidity; /* Humidity data in \%rh */
};
typedef struct Environmental_Data_S Environmental_Data_T, *Environmental_DataPtr_T;

/** enum values to represent Power Modes supported by the  Environmental sensor */
/* As per BME280 sensor data sheet, mode register accepts '01' or '10' to set BME280 sensor into FORCED mode.
 * The design decision is XDK supports only '01' value to set BME280 sensor into FORCED mode. For value '10' XDK returns
 * SENSOR_UNSUPPORTED_PARAMETER
 **/
enum Environmental_PowerModes_E
{
    ENVIRONMENTAL_BME280_POWERMODE_SLEEP, /**< no operation, all registers accessible, lowest power, selected after startup*/
    ENVIRONMENTAL_BME280_POWERMODE_FORCED, /**< perform one measurement, store results and return to sleep mode*/
    ENVIRONMENTAL_BME280_POWERMODE_NORMAL, /**< perpetual cycling of measurements and inactive periods*/
    ENVIRONMENTAL_POWERMODE_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};

typedef enum Environmental_PowerModes_E Environmental_PowerModes_T,  *Environmental_PowerModesPtr_T;

/** enum values to represent StandbyTime(ms) supported by the  Environmental sensor*/
enum Environmental_StandbyTime_E
{
    ENVIRONMENTAL_BME280_STANDBY_TIME_1_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_63_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_125_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_250_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_500_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_1000_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_10_MS,
    ENVIRONMENTAL_BME280_STANDBY_TIME_20_MS,
    ENVIRONMENTAL_STANDBY_TIME_OUT_OF_RANGE
};
typedef enum Environmental_StandbyTime_E Environmental_StandbyTime_T, *Environmental_StandbyTimePtr_T;

/** enum values to represent Time constant of IIR filter supported by the  Environmental sensor*/
/* As per BME280 sensor data sheet, filter register accepts 4,5,6 and 7 values
 * to set the filter coefficient rate as 16x. The design decision is XDK supports only value 4 to set
 * Filter coefficient as 16. For values 5,6 and 7 XDK returns SENSOR_UNSUPPORTED_PARAMETER
 **/
enum Environmental_FilterCoefficient_E
{
    ENVIRONMENTAL_BME280_FILTER_COEFF_OFF,
    ENVIRONMENTAL_BME280_FILTER_COEFF_2,
    ENVIRONMENTAL_BME280_FILTER_COEFF_4,
    ENVIRONMENTAL_BME280_FILTER_COEFF_8,
    ENVIRONMENTAL_BME280_FILTER_COEFF_16,
    ENVIRONMENTAL_FILTER_COEFF_OUT_OF_RANGE
};
typedef enum Environmental_FilterCoefficient_E Environmental_FilterCoefficient_T, *Environmental_FilterCoefficientPtr_T;

/** enum values to represent Oversampling supported by the  Environmental sensor */
/* As per BME280 sensor data sheet, Registers osrs_t,osrs_p and osrs_h are accepting 5,6 and 7 values
 * to set the oversampling rate as 16x. The design decision is XDK supports only value 5 to set
 * oversampling as 16. For values 6 and 7 XDK returns SENSOR_UNSUPPORTED_PARAMETER
 **/
enum Environmental_OverSampling_E
{
    ENVIRONMENTAL_BME280_OVERSAMP_SKIPPED, /**< This option could be used to disable data measurement */
    ENVIRONMENTAL_BME280_OVERSAMP_1X,
    ENVIRONMENTAL_BME280_OVERSAMP_2X,
    ENVIRONMENTAL_BME280_OVERSAMP_4X,
    ENVIRONMENTAL_BME280_OVERSAMP_8X,
    ENVIRONMENTAL_BME280_OVERSAMP_16X,
    ENVIRONMENTAL_OVERSAMP_OUT_OF_RANGE
};
typedef enum Environmental_OverSampling_E Environmental_OverSampling_T, *Environmental_OverSamplingPtr_T;

/** enum values to represent environmental types supported */
enum Environmental_SensorID_E
{
    ENVIRONMENTAL_BME280, /**< ID to represents bme280 environmental sensor */
    ENVIRONMENTAL_INVALID_ID = 0XFF /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Environmental_SensorID_E Environmental_SensorID_T;

/** @structure to hold the environmental type and its initialization status  */
struct Environmental_SensorInfo_S
{
    Environmental_SensorID_T sensorID; /**< logical sensor ID to represents physical environmental sensor*/
    uint8_t initializationStatus; /**< value to indicate whether the environmental sensor is properly initialized before */
};
typedef struct Environmental_SensorInfo_S Environmental_SensorInfo_T, *Environmental_SensorInfoPtr_T;

/** @brief structure to hold the Environmental sensor specific GPIO ,I2c configurations and the function pointers to initialize
 * configure and get the data from the sensor*/
struct Environmental_Handle_S
{
	void *SensorPtr; /**< Pointer to structure containing hardware specific configuration*/
	Environmental_SensorInfo_T SensorInformation; /**< Structure to hold logical identity to environmental sensor*/
};
typedef struct Environmental_Handle_S Environmental_Handle_T, *Environmental_HandlePtr_T;


/* public function prototype declarations */

/**
 * @brief Initializes the environmental sensor referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the environmental sensor object
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @Warning This function needs to be called before calling any other environmental API.
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_init(Environmental_HandlePtr_T handle);

/**
 * @brief Sets the power mode for the environmental sensor referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the environmental object
 *
 * @param [in]  mode        The desired power mode that needs to be set
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_setPowerMode(Environmental_HandlePtr_T handle,
    Environmental_PowerModes_T mode);

/**
 * @brief Gets the power mode for the environmental sensor referenced by the passed handle.

 * @param [in]  handle 		Handle to the environmental object
 *
 * @param [in]  mode 		Last configured power mode of environmental sensor
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_getPowerMode(Environmental_HandlePtr_T handle,
    Environmental_PowerModesPtr_T mode);

/**
 * @brief Reads Temperature in milli degree Celsius,pressure in Pascal and humidity in(\%rh) values from the environmental sensor
 *
 * @param [in] handle      Handle to the environmental object
 *
 * @param [in] sensorData  Parameter in which Temperature(mDegC),pressure(Pascal) and Humidity(\%rh) data from environmental sensor
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readData(Environmental_HandlePtr_T handle,
    Environmental_DataPtr_T sensorData);

/**
 * @brief Reads Temperature,pressure and humidity Raw values from the environmental sensor.
 *
 * @param [in] handle      Handle to the environmental object
 *
 * @param [in] sensorData  Parameter in which Temperature,pressure and humidity LSB raw data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readDataLSB(Environmental_HandlePtr_T handle,
    Environmental_LsbDataPtr_T sensorData);

/**
 * @brief Reads Temperature LSB(raw) values from the environmental sensor.
 *
 * @param [in] handle         Handle to the environmental object
 *
 * @param [in] temperaturelsb Parameter in which Temperature LSB raw data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readTemperatureLSB(Environmental_HandlePtr_T handle,
    int32_t *temperaturelsb);

/**
 * @brief Reads Pressure LSB(raw) values from the environmental sensor.
 *
 * @param [in] handle    Handle to the environmental object
 *
 * @param [in] pressurelsb  Parameter in which pressure LSB raw data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readPressureLSB(Environmental_HandlePtr_T handle,
    int32_t *pressurelsb);

/**
 * @brief Reads Humidity LSB(raw) values from the environmental sensor.
 *
 * @param [in] handle     Handle to the environmental object
 *
 * @param [in] humiditylsb   Parameter in which Humidity LSB raw data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readHumidityLSB(Environmental_HandlePtr_T handle,
    int32_t *humiditylsb);

/**
 * @brief Reads Temperature in milli degree Celsius values from the environmental sensor.
 *
 * @param [in] handle      Handle to the environmental object
 *
 * @param [in] temperature Parameter in which Temperature(mDegC) data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readTemperature(Environmental_HandlePtr_T handle,
    int32_t *temperature);

/**
 * @brief Reads Pressure in Pascal values from the environmental sensor
 *
 * @param [in] handle    Handle to the environmental object
 *
 * @param [in] pressure  Parameter in which Pressure(Pascal) data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readPressure(Environmental_HandlePtr_T handle,
    uint32_t *pressure);

/**
 * @brief Reads Humidity values in(\%rh) from the environmental sensor
 *
 * @param [in] handle    Handle to the environmental object
 *
 * @param [in] humidity Parameter in which Humidity(\%rh) data from environmental sensor gets collected.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_readHumidity(Environmental_HandlePtr_T handle,
    uint32_t *humidity);

/**
 * @brief Sets the StandbyDuration for the environmental sensor
 *
 * @param [in] handle    Handle to the environmental object
 *
 * @param [in] duration   set value for the desired Standby duration
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_setStandbyDuration(Environmental_HandlePtr_T handle,
    Environmental_StandbyTime_T duration);

/**
 * @brief reads the StandbyDuration for the environmental sensor
 *
 * @param [in] handle  Handle to the environmental object
 *
 * @param [in] duration  Last configured Standby duration of environmental 
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_getStandbyDuration(Environmental_HandlePtr_T handle,
    Environmental_StandbyTimePtr_T duration);

/**
 * @brief sets the Filter Coefficient for the environmental sensor
 *
 * @param [in] handle  Handle to the environmental object
 *
 * @param [in] filter set value for the Filter Coefficient
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_setFilterCoefficient(
    Environmental_HandlePtr_T handle, Environmental_FilterCoefficient_T filter);

/**
 * @brief sets the OverSampling rate of temperature data for the environmental sensor referenced by the passed handle.
 *
 * @note If this function is called with option ENVIRONMENTAL_BME280_OVERSAMP_SKIPPED then temperature measurement will be disabled and
 * default value 0x80000 returned for all the subsequent read operation.
 *
 * @param [in] handle     Handle to the environmental object
 *
 * @param [in] samplingRate set value for the Temperature OverSampling rate.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 *
 * @warning In order to have this setting effective for subsequent measurements,
 *          User needs to provide minimum 100 milliseconds delay after calling this API and then the sensor values can be read
 *
 * Example Usage:
 * <pre>
 * \code{.c}
 * #include "BCDS_Environmental.h"
 * void delayAPI(uint32_t milliSecondsDelay)
 * {
 *   // Implement the delay logic here
 * }
 *  if (RETCODE_OK == Environmental_setOverSamplingTemperature(xdkEnvironmental_BME280_Handle,ENVIRONMENTAL_BME280_OVERSAMP_8X))
 *   {
 *       printf("Environmental_setOverSamplingTemperature Success\n\r");
 *       delayAPI(100);
 *   }
 * \endcode
 * </pre>
 */
Retcode_T Environmental_setOverSamplingTemperature(
    Environmental_HandlePtr_T handle, Environmental_OverSampling_T samplingRate);

/**
 * @brief sets the OverSampling rate of pressure data for the environmental sensor referenced by the passed handle.
 *
 * @note If this function is called with option ENVIRONMENTAL_BME280_OVERSAMP_SKIPPED then pressure measurement will be disabled and
 * default value 0x80000 returned for all the subsequent read operation.
 *
 * @param [in] handle    Handle to the environmental object
 *
 * @param [in] samplingRate  set value for the Pressure OverSampling rate.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 *
 * @warning In order to have this setting effective for subsequent measurements,
 *        User needs to provide minimum 100 milliseconds delay after calling this API and then the sensor values can be read
 *
 * Example Usage:
 * <pre>
 * \code{.c}
 * #include "BCDS_Environmental.h"
 * void delayAPI(uint32_t milliSecondsDelay)
 * {
 *   // Implement the delay logic here
 * }
 *
 *  if (RETCODE_OK == Environmental_setOverSamplingPressure(xdkEnvironmental_BME280_Handle,ENVIRONMENTAL_BME280_OVERSAMP_8X))
 *   {
 *       printf("Environmental_setOverSamplingPressure Success\n\r");
 *       delayAPI(100);
 *   }
 * \endcode
 * </pre>
 */
Retcode_T Environmental_setOverSamplingPressure(
    Environmental_HandlePtr_T handle, Environmental_OverSampling_T samplingRate);

/**
 * @brief sets the OverSampling rate of Humidity data for the environmental sensor referenced by the passed handle.
 *
 * @note If this function is called with option ENVIRONMENTAL_BME280_OVERSAMP_SKIPPED then humidity measurement will be disabled and
 * default value 0x8000 returned for all the subsequent read operation.
 *
 * @param [in] handle   Handle to the environmental object
 *
 * @param [in] samplingRate set value for the Humidity OverSampling rate.
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 *
 * @warning In order to have this setting effective for subsequent measurements,
 *        User needs to provide minimum 100 milliseconds delay after calling this API and then the sensor values can be read
 *
 * Example Usage:
 * <pre>
 * \code{.c}
 * #include "BCDS_Environmental.h"
 * void delayAPI(uint32_t milliSecondsDelay)
 * {
 *   // Implement the delay logic here
 * }
 *
 *  if (RETCODE_OK == Environmental_setOverSamplingHumidity(xdkEnvironmental_BME280_Handle,ENVIRONMENTAL_BME280_OVERSAMP_8X))
 *   {
 *       printf("Environmental_setOverSamplingHumidity Success\n\r");
 *       delayAPI(100);
 *   }
 * \endcode
 * </pre>
 */
Retcode_T Environmental_setOverSamplingHumidity(
    Environmental_HandlePtr_T handle, Environmental_OverSampling_T samplingRate);

/**
 * @brief DeInitializes the environmental sensor referenced by the passed handle.
 *
 * @param [in] handle       Handle to the environmental object
 *
 * @retval      Retcode_T     The return value consist of (First 1 MSByte represents Package ID, 
 *							  Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Environmental_deInit(Environmental_HandlePtr_T handle);
	
#endif /* BCDS_ENVIRONMENTAL_H_ */

/**@} */
