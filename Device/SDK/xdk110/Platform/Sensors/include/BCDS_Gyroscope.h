/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/****************************************************************************/
/**
 *  @defgroup    Gyroscope Gyroscope
 *  @ingroup     advanced_sensor_api
 *
 *  @{
 *  @brief Gyroscope Sensor Interface
 *  @details
 *                                              - Gyroscope_init()
 *                                              - Gyroscope_deInit()
 *                                              - Gyroscope_readXyzValue()
 *                                              - Gyroscope_readXyzDegreeValue()
 *                                              - Gyroscope_setBandwidth()
 *                                              - Gyroscope_getBandwidth()
 *                                              - Gyroscope_setRange()
 *                                              - Gyroscope_getRange()
 *                                              - Gyroscope_setMode()
 *                                              - Gyroscope_getMode()
 *                                              - Gyroscope_setSleepDuration()
 *                                              - Gyroscope_setAutoSleepDuration()
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_GYROSCOPE_H_
#define BCDS_GYROSCOPE_H_

#include "BCDS_Retcode.h"
/* public type and macro definitions */

/**enum values to represent Bandwidth supported by the  Gyroscope */
enum Gyroscope_Bandwidth_E
{
    GYROSCOPE_BMG160_BANDWIDTH_12HZ, /**< sets bandwidth to LowPass 12 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_23HZ, /**< sets bandwidth to LowPass 23 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_32HZ, /**< sets bandwidth to LowPass 32 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_47HZ, /**< sets bandwidth to LowPass 47 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_64HZ, /**< sets bandwidth to LowPass 64 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_116HZ, /**< sets bandwidth to LowPass 116 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_230HZ, /**< sets bandwidth to LowPass 230 HZ */
    GYROSCOPE_BMG160_BANDWIDTH_523HZ, /**< sets bandwidth to LowPass 523  HZ */
    GYROSCOPE_BMI160_BANDWIDTH_10_7HZ, /**< sets bandwidth to LowPass 10.7 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_20_8HZ, /**< sets bandwidth to LowPass 20.8 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_39_9HZ, /**< sets bandwidth to LowPass 39.9 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_74_6HZ, /**< sets bandwidth to LowPass 74.6 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_136_6HZ, /**< sets bandwidth to LowPass 136.6 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_254_6HZ, /**< sets bandwidth to LowPass 254.6 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_523_9HZ, /**< sets bandwidth to LowPass 523.9 HZ */
    GYROSCOPE_BMI160_BANDWIDTH_890HZ, /**< sets bandwidth to LowPass 890 HZ */
    GYROSCOPE_BANDWIDTH_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_Bandwidth_E Gyroscope_Bandwidth_T, *Gyroscope_BandwidthPtr_T;

/**enum values to represent Range supported by the  Gyroscope */
enum Gyroscope_Range_E
{
    GYROSCOPE_BMG160_RANGE_125s, /**< set to 0.0625 deg/s in 125 deg/s range */
    GYROSCOPE_BMG160_RANGE_250s, /**< set to 0.125 deg/s in 250 deg/s range */
    GYROSCOPE_BMG160_RANGE_500s, /**< set to 0.25 deg/s in 500 deg/s range */
    GYROSCOPE_BMG160_RANGE_1000s,/**< set to 0.5 deg/s in 1000 deg/s range */
    GYROSCOPE_BMG160_RANGE_2000s, /**< set to 1 deg/s in 2000 deg/s range */
    GYROSCOPE_BMI160_125S_RANGE, /**< set to 0.0625 deg/s in 125 deg/s range */
    GYROSCOPE_BMI160_250S_RANGE, /**< set to 0.125 deg/s in 250 deg/s range */
    GYROSCOPE_BMI160_500S_RANGE, /**< set to 0.25 deg/s in 500 deg/s range */
    GYROSCOPE_BMI160_1000S_RANGE, /**< set to 0.5 deg/s in 1000 deg/s range */
    GYROSCOPE_BMI160_2000S_RANGE, /**< set to 1 deg/s in 2000 deg/s range */
    GYROSCOPE_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_Range_E Gyroscope_Range_T, *Gyroscope_RangePtr_T;

/**enum values to represent Power mode supported by the  Gyroscope */
enum Gyroscope_Powermode_E
{
    GYROSCOPE_BMG160_POWERMODE_NORMAL, /**< sets Powermode to normal */
    GYROSCOPE_BMG160_POWERMODE_DEEPSUSPEND,/**< sets Powermode to deep suspend */
    GYROSCOPE_BMG160_POWERMODE_SUSPEND, /**< sets Powermode to suspend */
    GYROSCOPE_BMG160_POWERMODE_FAST_POWERUP, /**< sets Powermode to fast powerup */
    GYROSCOPE_BMG160_POWERMODE_ADVANCE_POWERUP, /**< sets Powermode to advanced  powerup */
    GYROSCOPE_BMI160_POWERMODE_NORMAL, /**< sets Powermode to normal */
    GYROSCOPE_BMI160_POWERMODE_FAST_STARTUP,/**< sets Powermode to fast startup  */
    GYROSCOPE_BMI160_POWERMODE_SUSPEND, /**< sets Powermode to suspend      */
    GYROSCOPE_POWERMODE_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_Powermode_E Gyroscope_Powermode_T, *Gyroscope_PowermodePtr_T;

/** enum values to represent various sleep duration applicable only in low power mode  */
enum Gyroscope_SleepDuration_E
{
    GYROSCOPE_BMG160_SLEEP_DURATION_2MS, /**< ID to represent sleep duration of 2 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_4MS, /**<ID to represent sleep duration to 4 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_5MS, /**< ID to represent sleep duration to 5 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_8MS, /**< ID to represent sleep duration to 8 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_10MS, /**<ID to represent sleep duration to 10 ms*/
    GYROSCOPE_BMG160_SLEEP_DURATION_15MS, /**< ID to represent sleep duration to 15 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_18MS, /**< ID to represent sleep duration to 18 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_20MS, /**< ID to represent sleep duration to 20 ms */
    GYROSCOPE_BMG160_SLEEP_DURATION_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_SleepDuration_E Gyroscope_SleepDuration_T, *Gyroscope_SleepDurationPtr_T;

/** enum values to represent various Auto-sleep duration applicable only in low power mode  */
enum Gyroscope_AutoSleepDuration_E
{
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_0MS, /**< ID to represent No Auto sleep duration */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_4MS, /**<ID to represent auto sleep duration to 4 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_5MS, /**< ID to represent auto sleep duration to 5 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_8MS, /**< ID to represent auto sleep duration to 8 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_10MS, /**<ID to represent auto sleep duration to 10 ms*/
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_15MS, /**< ID to represent auto sleep duration to 15 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_20MS, /**< ID to represent auto sleep duration to 20 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_40MS, /**< ID to represent auto sleep duration to 40 ms */
    GYROSCOPE_BMG160_AUTOSLEEP_DURATION_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_AutoSleepDuration_E Gyroscope_AutoSleepDuration_T, *Gyroscope_AutoSleepDurationPtr_T;

/*Enum to hold the ID for different Gyro sensor */
enum Gyroscope_SensorID_E
{
    BMG160_GYRO_SENSOR,/**< ID to represents bmg160 Gyroscope sensor */
    BMI160_GYRO_SENSOR, /**< ID to represents bmi160 Gyroscope sensor */
    GYRO_SENSOR_INVALID_ID = 0xFF /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Gyroscope_SensorID_E Gyroscope_SensorID_T;

/** @structure to hold the Gyroscope type and its initialization status  */
struct Gyroscope_SensorInfo_S
{
    Gyroscope_SensorID_T sensorID; /**< logical sensor ID to represents physical Gyroscope sensor*/
    uint8_t initializationStatus;/**< value to indicate whether the Gyroscope sensor is properly initialized before */
};
typedef struct Gyroscope_SensorInfo_S Gyroscope_SensorInfo_T, *Gyroscope_SensorInfoPtr_T;

/** @brief structure to represent corresponding Gyroscope handle */
struct Gyroscope_Handle_S
{
    void *sensorPtr; /**< Pointer to structure containing hardware specific configuration*/
    Gyroscope_SensorInfo_T sensorInfo;/**< Structure to hold logical identity to Gyroscope sensor*/
};
typedef struct Gyroscope_Handle_S Gyroscope_Handle_T, *Gyroscope_HandlePtr_T;

/** structure required to collect the XYZ axis data */
struct Gyroscope_XyzData_S
{
    int32_t xAxisData;
    int32_t yAxisData;
    int32_t zAxisData;
};
typedef struct Gyroscope_XyzData_S Gyroscope_XyzData_T, *Gyroscope_XyzDataPtr_T;

/* public function prototype declarations */
/**
 * @brief  Function to initialize Gyroscope.This needs to be called before calling any other API.
 *
 * @param [in]  handle          Handle to the Gyroscope object
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 *
 * @Warning This function needs to be called before calling any other Gyroscope API.
 */
Retcode_T Gyroscope_init(Gyroscope_HandlePtr_T handle);

/* public function prototype declarations */
/**
 * @brief  Function to DeInitializes the  Gyroscope referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the Gyroscope object
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_deInit(Gyroscope_HandlePtr_T handle);

/**
 * @brief Sets the bandwidth for the Gyroscope referenced by the passed handle.
 *
 * @param [in]  handle           Handle to the Gyroscope object
 *
 * @param [in]  bandwidth        Represents the bandwidth that needs to be configured for gyro sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_setBandwidth(Gyroscope_HandlePtr_T handle, Gyroscope_Bandwidth_T bandwidth);

/**
 * @brief Reads the bandwidth for the Gyroscope referenced by the passed handle.
 *
 * @param [in]      handle       Handle to the Gyroscope object
 *
 * @param [in/out]  bandwidth    Collects the last set Bandwidth value from the gyro sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_getBandwidth(Gyroscope_HandlePtr_T handle, Gyroscope_BandwidthPtr_T bandwidth);

/**
 * @brief Sets the Range for the  Gyroscope by the passed handle.
 *
 * @param [in]  handle         Handle to the Gyroscope object
 *
 * @param [in]  range          Represents the measurement range that needs to be configured for gyro sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_setRange(Gyroscope_HandlePtr_T handle, Gyroscope_Range_T range);

/**
 * @brief Reads the Range for the Gyroscope referenced by the passed handle.
 *
 * @param [in]      handle        Handle to the Gyroscope object
 *
 * @param [in/out]  range         Collects the last set measurement range value from the sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_getRange(Gyroscope_HandlePtr_T handle, Gyroscope_RangePtr_T range);

/**
 * @brief Sets the powermode  for the  Gyroscope by the passed handle.
 *
 * @param [in]  handle            Handle to the Gyroscope object
 *
 * @param [in]  powermode         Represents the power mode that needs to be configured for gyro sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_setMode(Gyroscope_HandlePtr_T handle, Gyroscope_Powermode_T powermode);

/**
 * @brief Reads the power mode  for the  Gyroscope by the passed handle.
 *
 * @param [in]      handle        Handle to the Gyroscope object
 *
 * @param [in/out]  powermode     Collects the last set power mode value from gyro sensor
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *          Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_getMode(Gyroscope_HandlePtr_T handle, Gyroscope_PowermodePtr_T powermode);

/**
 * @brief Reads an  xyz values from the Gyroscope referenced by the passed handle.
 *
 * @param [in]      handle       Handle to the Gyroscope object
 *
 * @param [in/out]  value        Collects the XYZ value of gyro sensor in LSB
 *
 * @retval     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *             Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_readXyzValue(Gyroscope_HandlePtr_T handle, Gyroscope_XyzDataPtr_T value);

/**
 * @brief Reads an Xyz degree value from the Gyroscope referenced by the passed handle.
 *
 * @param [in]      handle       Handle to the Gyroscope object
 *
 * @param [in/out]  value        Collects the XYZ value of gyro sensor in milli Degree
 *
 * @retval     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *             Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_readXyzDegreeValue(Gyroscope_HandlePtr_T handle, Gyroscope_XyzDataPtr_T value);

/**
 * @brief Sets the SleepDuration for Gyroscope referenced by the passed handle.
 *
 * @param [in]  handle                      Handle to the Gyroscope object
 *
 * @param [in]  lowPowerModeSleepDuration   Represents the SleepDuration that needs to be configured for gyro sensor
 *
 * @retval     The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *              Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_setSleepDuration(Gyroscope_HandlePtr_T handle, Gyroscope_SleepDuration_T lowPowerModeSleepDuration);

/**
 * @brief Sets the Auto-SleepDuration for Gyroscope referenced by the passed handle.
 *
 * @param [in]  handle                      Handle to the Gyroscope object
 *
 * @param [in]  lowPowerModeSleepDuration   Represents the AutoSleepDuration that needs to be configured for gyro sensor
 *
 * @param [in]  bandwidthForAutoSleep       It needs to be the value read from gyroscopeGetBandwidth(), this is required for proper operation of gyro sensor
 *                                          in advanced power saving mode only
 *
 * @retval  The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code).
 *              Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in 'BCDS_SensorErrorType.h'.
 */
Retcode_T Gyroscope_setAutoSleepDuration(Gyroscope_HandlePtr_T handle, Gyroscope_AutoSleepDuration_T lowPowerModeSleepDuration, Gyroscope_Bandwidth_T bandwidthForAutoSleep);

#endif /* BCDS_GYROSCOPE_H_ */

