/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    LightSensor LightSensor
 *  @ingroup     advanced_sensor_api
 *
 *  @{
 *  @brief LightSensor Interface
 *  @details
 *                                              - LightSensor_init()
 *                                              - LightSensor_setManualMode()
 *                                              - LightSensor_setContinuousMode()
 *                                              - LightSensor_readRawData()
 *                                              - LightSensor_readLuxData()
 *                                              - LightSensor_deInit()
 *                                              - LightSensor_setIntegrationTime()
 *                                              - LightSensor_setBrightness()
 *                                              - LightSensor_configureThresholdInterrupt()
 *                                              - LightSensor_registerRealTimeCallback()
 *                                              - LightSensor_registerDeferredCallback()
 *                                              - LightSensor_getInterruptStatus()
 *                                              - LightSensor_disableInterrupt()
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_LIGHTSENSOR_H_
#define BCDS_LIGHTSENSOR_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/* public type and macro definitions */

/** enum values to represent the supported integration time values by the ambient light sensor */
enum LightSensor_IntegrationTime_E
{
    LIGHTSENSOR_800_MS, /**< 800 ms integration time(time taken for capturing lux intensity) */
    LIGHTSENSOR_400MS, /**< 400 ms integration time */
    LIGHTSENSOR_200MS , /**< 200 ms integration time */
    LIGHTSENSOR_100MS, /**< 100 ms integration time */
    LIGHTSENSOR_50MS, /**< 50 ms integration time */
    LIGHTSENSOR_25MS , /**< 25 ms integration time */
    LIGHTSENSOR_12P5MS , /**< 12.5 ms integration time */
    LIGHTSENSOR_6P5MS, /**< 6.5 ms integration time */
    LIGHTSENSOR_TIME_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum LightSensor_IntegrationTime_E LightSensor_IntegrationTime_T,*LightSensor_IntegrationTimePtr_T;

/** enum values to configure the mode of the lightsensor */
enum LightSensor_ConfigMode_E
{
    LIGHTSENSOR_MAX44009_ENABLE_MODE, /**< ID to enable the manual/continuous mode of the lightsensor */
    LIGHTSENSOR_MAX44009_DISABLE_MODE, /**< ID to disable the manual/continuous mode of the lightsensor */
    LIGHTSENSOR_MODE_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum LightSensor_ConfigMode_E LightSensor_ConfigMode_T, *LightSensor_ConfigModePtr_T;

/** enum values to represent lightsensor types supported */
enum LightSensor_SensorID_E
{
    LIGHTSENSOR_MAX44009 , /**< ID to represents lightsensor */
    LIGHTSENSOR_INVALID_ID /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum LightSensor_SensorID_E LightSensor_SensorID_T;

/** @structure to hold the lightsensor type and its initialization status  */
struct LightSensor_SensorInfo_S
{
    LightSensor_SensorID_T sensorID; /**< logical sensor ID to represents physical lightsensor */
    uint8_t initializationStatus; /**< value to indicate whether the lightsensor is properly initialized before */
};
typedef struct LightSensor_SensorInfo_S LightSensor_SensorInfo_T, *LightSensor_SensorInfoPtr_T;

/** @brief structure to represent corresponding lightsensor handle */
struct LightSensor_Handle_S
{
    void *SensorPtr; /**< Pointer to structure containing hardware specific configuration*/
    LightSensor_SensorInfo_T SensorInformation; /**< Structure to hold logical identity to lightsensor*/
};
typedef struct LightSensor_Handle_S LightSensor_Handle_T, *LightSensor_HandlePtr_T;

/** enum to represent Ambient light sensor supported brightness configurations*/
enum LightSensor_ConfigBrightness_E
{
    LIGHTSENSOR_NORMAL_BRIGHTNESS, /**< normal brightness */
    LIGHTSENSOR_HIGH_BRIGHTNESS, /**< high brightness */
    LIGHTSENSOR_BRIGHTNESS_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum LightSensor_ConfigBrightness_E LightSensor_ConfigBrightness_T, *LightSensor_ConfigBrightnessPtr_T;

/** enum values to represent the interrupt status */
enum LightSensor_ConfigInterrupt_E
{
    LIGHTSENSOR_DISABLE_INTERRUPT, /**< interrupt disable */
    LIGHTSENSOR_ENABLE_INTERRUPT, /**< enable interrupt */
    LIGHTSENSOR_CONFIG_INTERRUPT_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum LightSensor_ConfigInterrupt_E LightSensor_ConfigInterrupt_T, *LightSensor_ConfigInterruptPtr_T;

/** function pointer type used in deferred interrupt callback registration */
typedef void (*deferredInterruptCallback)(void *, uint32_t);
/** function pointer type used in real time interrupt callback registration */
typedef void (*interruptCallback)(void);

/**
 * @brief Initializes the LightSensor referenced by the passed handle.
 *
 * @param [in]  handle          Handle to the lightsensor object
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @Warning This function needs to be called before calling any other lightsensor API.
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_init(LightSensor_HandlePtr_T handle);

/**
 * @brief Function to enable/disable the Manual mode for the lightsensor referenced by the passed handle.
 *
 * @param [in]  handle            Handle to the lightSensor object
 *
 * @param [in]  manualMode       This variable holds the manual mode configuration
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_setManualMode(LightSensor_HandlePtr_T handle, LightSensor_ConfigMode_T manualMode);


/**
 * @brief Function to enable/disable the Continuous mode for the lightsensor referenced by the passed handle.
 *
 * @param [in]  handle            Handle to the lightSensor object
 *
 * @param [in]  continuousMode       This variable holds the Continuous mode configuration
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_setContinuousMode(LightSensor_HandlePtr_T handle, LightSensor_ConfigMode_T continuousMode);

/**
 * @brief Reads raw sensor data from light sensor referenced by the passed handle.
 *
 * @param [in]  handle       global handle of sensor
 *
 * @param [out] luxData      holds sensor data
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_readRawData(LightSensor_HandlePtr_T handle,uint16_t * luxData);

/**
 * @brief function to read sensor data in milli lux.
 *
 * @param [in]  handle       global handle of sensor
 *
 * @param [out] luxData      holds sensor data in milli lux
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_readLuxData(LightSensor_HandlePtr_T handle,uint32_t * luxData);

/**
 * @brief function to configure integration time of light sensor.
 *
 * @param [in]  handle          global handle of sensor
 *
 * @param [in] integrationTime  LightSensor_IntegrationTime_T holds the integration time configuration
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_setIntegrationTime(LightSensor_HandlePtr_T handle,LightSensor_IntegrationTime_T integrationTime);


/**
 * @brief function to set brightness to low or high for light sensor
 *
 * @param [in]  handle          global handle of sensor
 *
 * @param [in] brightness  LightSensor_ConfigBrightness_T holds brightness configuration
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_setBrightness(LightSensor_HandlePtr_T handle,LightSensor_ConfigBrightness_T brightness);

/**
 * @brief function responsible to set interrupt conditions, that means, threshold boundaries and threshold timer
 *              for lightsensor interrupt.  Interrupt will be fired if the light intensity is crossed threshold
 *              boundaries for a window period defined in threshold timer.User must register the callback before
 *              calling configuring interrupt
 *
 * @param [in]  handle          global handle of sensor
 *
 * @param [in]  upperThreshold  upper threshold value to be programmed in upper threshold register for lightsensor interrupt.
 *
 * @param [in]  lowerThreshold  lower threshold value to be programmed in lower threshold register for lightsensor interrupt.
 *
 * @param [in]  thresholdTimer  threshold timer value to be programmed in threshold timer register for lightsensor interrupt.
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_configureThresholdInterrupt(LightSensor_HandlePtr_T handle, uint32_t upperThreshold,uint32_t lowerThreshold, uint32_t thresholdTimer);

/**
 * @brief function responsible to register real time interrupt handler for lightsensor.  When user registers
 *              real time callback their callback will be executed in ISR context. User must not use any delay functions
 *              or bus access in ISR context.
 *
 * @param [in]  handle           global handle of sensor
 *
 * @param [in]  realTimeCallback function handle to register
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_registerRealTimeCallback(LightSensor_HandlePtr_T handle, interruptCallback realTimeCallback);

/**
  * @brief       function responsible to register deferred interrupt handler for lightsensor.  When user registers deferred
 *              callback their callback will be executed in timer daemon context, where there is no restriction of using
 *              delay functions and/or APIs related to bus access.
 *
 * @param [in]  handle          global handle of sensor
 *
 * @param [in] deferredInterruptCallback function handle to register
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_registerDeferredCallback(LightSensor_HandlePtr_T handle, deferredInterruptCallback deferredCallback);

/**
 * @brief       function to read light sensor interrupt status register
 *
 * @param [in]  handle          global handle of sensor
 *
 * @param [out]   interruptStatus holds the interrupt configuration that is configured
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_getInterruptStatus(LightSensor_HandlePtr_T handle, LightSensor_ConfigInterruptPtr_T interruptStatus);

/**
 * @brief Disable Interrupt type on the handle
 *
 * @param [in] handle        Handle to the lightSensor object
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_disableInterrupt (LightSensor_HandlePtr_T handle);

/**
 * @brief DeInitializes the lightsensor referenced by the passed handle.
 *
 * @param [in] handle       Handle to the lightsensor object
 *
 * @retval     Retcode_T    The return value consist of (First 1 MSByte represents Package ID,
 *                          Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code
 *        present in 'BCDS_SensorErrorType.h'
 */
Retcode_T LightSensor_deInit(LightSensor_HandlePtr_T handle);

#endif /* BCDS_LIGHTSENSOR_H_ */

/**@} */
