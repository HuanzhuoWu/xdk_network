/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    Magnetometer Magnetometer
 *  @ingroup     advanced_sensor_api
 *
 *  @{
 *  @brief Magnetometer Interface
 *  @details
 *  The interface header exports the following features:
 *                                                      -Magnetometer_init()
 *                                                      -Magnetometer_setPowerMode()
 *                                                      -Magnetometer_getPowerMode()
 *                                                      -Magnetometer_setPresetMode()
 *                                                      -Magnetometer_getPresetMode()
 *                                                      -Magnetometer_setDataRate()
 *                                                      -Magnetometer_getDataRate()
 *                                                      -Magnetometer_readXyzLsbData()
 *                                                      -Magnetometer_readXyzTeslaData()
 *                                                      -Magnetometer_deInit()
 *
 * ****************************************************************************/

#ifndef BCDS_MAGNETOMETER_H_
#define BCDS_MAGNETOMETER_H_

/* public interface declaration */
#include "BCDS_Retcode.h"

/* public type and macro definitions */

/** enum values to represent Manetometer types supported */
enum Magnetometer_SensorID_E
{
    MAGNETOMETER_BMM150, /**< ID to represents bmm150 magnetometer sensor */
    MAGNETOMETER_INVALID_ID/**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum Magnetometer_SensorID_E Magnetometer_SensorID_T;

/** enum values to represent Manetometer power modes */
enum Magnetometer_PowerMode_E
{
    MAGNETOMETER_BMM150_POWERMODE_NORMAL, /**< ID to represent bmm150 Powermode to normal  */
    MAGNETOMETER_BMM150_POWERMODE_FORCE, /**< ID to represent bmm150 Powermode to forced  */
    MAGNETOMETER_BMM150_POWERMODE_SUSPEND, /**< ID to represent bmm150 Powermode to suspend  */
    MAGNETOMETER_BMM150_POWERMODE_SLEEP,/**< ID to represent bmm150 Powermode to sleep  */
    MAGNETOMETER_POWERMODE_OUT_OF_RANGE, /**< This element is used for error handling, User not allowed to use it */
};
typedef enum Magnetometer_PowerMode_E Magnetometer_PowerMode_T, *Magnetometer_PowerModePtr_T;

/** enum values to represent Manetometer preset modes */
enum Magnetometer_PresetMode_E
{
    MAGNETOMETER_BMM150_PRESETMODE_LOWPOWER, /**< ID to represent bmm150 Presetmode to lowpower  */
    MAGNETOMETER_BMM150_PRESETMODE_REGULAR,/**< ID to represent bmm150 Presetmode to regular  */
    MAGNETOMETER_BMM150_PRESETMODE_HIGHACCURACY,/**< ID to represent bmm150 Presetmode to highaccuracy  */
    MAGNETOMETER_BMM150_PRESETMODE_ENHANCED,/**< ID to represent bmm150 Presetmode to enhanced  */
    MAGNETOMETER_PRESETMODE_OUT_OF_RANGE, /**< This element is used for error handling, User not allowed to use it */
};
typedef enum Magnetometer_PresetMode_E Magnetometer_PresetMode_T, *Magnetometer_PresetModePtr_T;

/** enum values to represent Manetometer datarate modes */
enum Magnetometer_DataRate_E
{
    MAGNETOMETER_BMM150_DATARATE_10HZ,/**< ID to represent bmm150 datarate to 10Hz  */
    MAGNETOMETER_BMM150_DATARATE_2HZ,/**< ID to represent bmm150 datarate to 2Hz  */
    MAGNETOMETER_BMM150_DATARATE_6HZ,/**< ID to represent bmm150 datarate to 6Hz  */
    MAGNETOMETER_BMM150_DATARATE_8HZ,/**< ID to represent bmm150 datarate to 8Hz  */
    MAGNETOMETER_BMM150_DATARATE_15HZ,/**< ID to represent bmm150 datarate to 15Hz  */
    MAGNETOMETER_BMM150_DATARATE_20HZ,/**< ID to represent bmm150 datarate to 20Hz  */
    MAGNETOMETER_BMM150_DATARATE_25HZ,/**< ID to represent bmm150 datarate to 25Hz  */
    MAGNETOMETER_BMM150_DATARATE_30HZ,/**< ID to represent bmm150 datarate to 30Hz  */
    MAGNETOMETER_DATARATE_OUT_OF_RANGE, /**< This element is used for error handling, User not allowed to use it */
};
typedef enum Magnetometer_DataRate_E Magnetometer_DataRate_T, *Magnetometer_DataRatePtr_T;

/** structure required to collect the XYZ axis data */
struct Magnetometer_XyzData_S
{
    int32_t xAxisData;
    int32_t yAxisData;
    int32_t zAxisData;
    uint16_t resistance;
};
typedef struct Magnetometer_XyzData_S Magnetometer_XyzData_T, *Magnetometer_XyzDataPtr_T;

/** @structure to hold the Manetometer type and its initialization status  */
struct Magnetometer_SensorInfo_S
{
    Magnetometer_SensorID_T sensorID; /**< logical sensor ID to represents physical magnetometer sensor*/
    uint8_t initializationStatus;/**< value to indicate whether the magnetometer sensor is properly initialized before */
};
typedef struct Magnetometer_SensorInfo_S Magnetometer_SensorInfo_T, *Magnetometer_SensorInfoPtr_T;

/** @brief structure to represent corresponding Accelerometer handle */
struct Magnetometer_Handle_S
{
    void *SensorPtr;/**< Pointer to structure containing hardware specific configuration*/
    Magnetometer_SensorInfo_T SensorInformation;/**< Structure to hold logical identity to magnetometer sensor*/
};
typedef struct Magnetometer_Handle_S Magnetometer_Handle_T, *Magnetometer_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the Magnetometer sensor referenced by the passed handle.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @Warning This function needs to be called before calling any other accelerometer API.
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_init(Magnetometer_HandlePtr_T handle);

/**
 * @brief Function to set the powermode of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  powerMode       powermode to be set to the sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_setPowerMode(Magnetometer_HandlePtr_T handle, Magnetometer_PowerMode_T powerMode);

/**
 * @brief Function to read the powermode of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  powerMode       variable to be update the powermode
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_getPowerMode(Magnetometer_HandlePtr_T handle, Magnetometer_PowerModePtr_T powerMode);

/**
 * @brief Function to set the preset mode of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  presetMode      presetMode to be set to the sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_setPresetMode(Magnetometer_HandlePtr_T handle, Magnetometer_PresetMode_T presetMode);

/**
 * @brief Function to read the preset mode of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  presetMode      variable to store the presetMode of the sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_getPresetMode(Magnetometer_HandlePtr_T handle, Magnetometer_PresetModePtr_T presetMode);

/**
 * @brief Function to set the Datarate of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  datarate        Datarate to be set to the sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_setDataRate(Magnetometer_HandlePtr_T handle, Magnetometer_DataRate_T datarate);

/**
 * @brief Function to read the Datarate of the Magnetometer sensor.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  datarate        variable to store datarate of the sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_getDataRate(Magnetometer_HandlePtr_T handle, Magnetometer_DataRatePtr_T datarate);

/**
 * @brief Function to DeInitialize the magnetometer sensor referenced by the passed handle.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'BCDS_SensorErrorType.h'
 */
Retcode_T Magnetometer_deInit(Magnetometer_HandlePtr_T handle);

/**
 * @brief Function to Read the magnetometer sensor data referenced by the passed handle.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  data            Parameter in which the magnetometer XYZ data gets collected
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  1) Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 *           'BCDS_SensorErrorType.h'
 *        2) Whenever there is an overflow in any axis this API will return the maximum value the sensor could read for that
 *           axis below are the maximum value the sensor could read
 *         x-axis - 1500, y-axis - 1500, z-axis - 2500
 */
Retcode_T Magnetometer_readXyzLsbData(Magnetometer_HandlePtr_T handle, Magnetometer_XyzDataPtr_T data);

/**
 * @brief Function to read micro tesla xyz value from the magnetometer referenced by the passed handle.
 *
 * @param [in]  handle          handle of the Magnetometer object
 *
 * @param [in]  data            Parameter in which the magnetometer XYZ data gets collected in micro tesla
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID,
 *                              Next  Byte represents Severity and Last 2 LSBytes represents error code
 *
 * @note  1) Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 *           'BCDS_SensorErrorType.h'
 *        2) Whenever there is an overflow in any axis this API will return the maximum value the sensor could read for that
 *           axis below are the maximum value the sensor could read
 *         x-axis - 20800, y-axis - 20800, z-axis - 40000
 */
Retcode_T Magnetometer_readXyzTeslaData(Magnetometer_HandlePtr_T handle, Magnetometer_XyzDataPtr_T data);

#endif /* BCDS_MAGNETOMETER_H_ */

/**@} */
