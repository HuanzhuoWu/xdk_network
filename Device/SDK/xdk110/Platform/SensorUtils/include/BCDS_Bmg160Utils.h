/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup BMG160_SensorUtils BMG160_SensorUtils
 *  @{
 *  @brief SensorUtils Interfaces of BMG160
 *  @details
 *
 *  The interface header exports the following features:
 *                                                      -Bmg160Utils_initialize()
 *                                                      -Bmg160Utils_uninitialize()
 *                                                      -Bmg160Utils_remapAxis()
 *
 * ****************************************************************************/

#ifndef BCDS_BMG160UTILS_H_
#define BCDS_BMG160UTILS_H_

/* public interface declaration */
#include "BCDS_Retcode.h"
#include "gpio.h"
#include "SPI_ih.h"
#include "BCDS_AxisRemap.h"
#include "I2C_ih.h"

/* public type and macro definitions */
/**   Enumeration to represent the communication bus supported by bmg160*/
enum Bmg160Utils_Bus_E
{
    BMG160UTILS_I2C_BUS,
    BMG160UTILS_SPI_BUS
};
typedef enum Bmg160Utils_Bus_E Bmg160Utils_Bus_T;

/** @brief union holding the information regarding the interface configurations*/
union Bmg160Utils__InterfaceConfig_U
   {
       I2C_BusInfo_T i2cBus;
       SPI_device_t spiBus;
   };
typedef union Bmg160Utils__InterfaceConfig_U Bmg160Utils__InterfaceConfig_T;

/** structure to hold information regarding the communication channel */
struct Bmg160Utils_Interface_S
{
    Bmg160Utils_Bus_T bus;
    Bmg160Utils__InterfaceConfig_T type;
};
typedef struct Bmg160Utils_Interface_S Bmg160Utils_Interface_T;

/** @brief structure to hold the bmg160 specific GPIO ,I2c configurations,Remap configuration and the function pointers to initialize
 * configure and get the data from the sensor*/
struct Bmg160Utils_Info_S
{
    Bmg160Utils_Interface_T interface; /**< communication Bus configuration to be used for this sensor*/
    Gpio_T sensorPower; /**< Power enable Pin for sensor*/
    Gpio_PinInterrupt_T interruptPin1; /**< First Interrupt Pin*/
    Gpio_PinInterrupt_T interruptPin2; /**< Second Interrupt Pin*/
    AxisRemap_Config_T remap; /**< Configuration Settings for Remap*/
};
typedef struct Bmg160Utils_Info_S Bmg160Utils_Info_T,*Bmg160Utils_InfoPtr_T;

/* public function prototype declarations */
/**
 * @brief        Function to initialize bmg160.This needs to be called before calling any other API
 *
 * @param [in]   bmg160Info :structure holding the bmg160 specific configurations
 *
 * @param [in]   callback   :callback that will be called  in deferred context when an registered interrupt occurs
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BMG160 initialized successfully
 * @retval       RETCODE_FAILURE        BMG160 initialization failed
 * @retval       RETCODE_INVALID_PARAM  BMG160 initialization failed because of invalid input parameter
 *
 * @note
 * 1) The support of multiple instances is not possible due to the limitation in the BST library.
 * 2) Calling Bmi160 initialization API present in sensorUtils (Bmg160Utils_initialize) and BST library (bmg160_init)
 *    concurrently will lead to unexpected behaviour.
 */
Retcode_T Bmg160Utils_initialize(Bmg160Utils_InfoPtr_T bmg160Info);

/**
 * @brief           Function to read remapped bmg160 data.
 *
 * @param [in]      bmg160Info :structure holding the bmg160 specific configurations
 *
 * @param [in,out]  bmgData  :structure to hold the remapped bmg160 data
 *
 * @retval          Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval          RETCODE_OK             BMG160 initialized successfully
 * @retval          RETCODE_FAILURE        BMG160 initialization failed
 * @retval          RETCODE_INVALID_PARAM  BMG160 initialization failed because of invalid input parameter
 */
Retcode_T Bmg160Utils_remapAxis(Bmg160Utils_InfoPtr_T bmg160Info,
        AxisRemap_Data_T *bmgData);

/**
 * @brief          Function to uninitialize bmg160.After calling this API sensor is no more usable without initializing it again by calling initialize API
 *
 * @param [in]     bmg160Info :structure holding the bmg160 specific configurations
 *
 * @retval         Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval         RETCODE_OK             BMG160 initialized successfully
 * @retval         RETCODE_FAILURE        BMG160 initialization failed
 * @retval         RETCODE_INVALID_PARAM  BMG160 initialization failed because of invalid input parameter
 */
Retcode_T Bmg160Utils_uninitialize(Bmg160Utils_InfoPtr_T bmg160Info);


#endif /* BCDS_BMG160UTILS_H_ */

/**@} */

