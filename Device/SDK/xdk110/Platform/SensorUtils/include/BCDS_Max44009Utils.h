/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup MAX44009_SensorUtils MAX44009_SensorUtils
 *  @{
 *  @brief SensorUtils Interfaces of MAX44009
 *  @details
 *  The interface header exports the following features:
 *                                              - Max44009Utils_initialize()
 *                                              - Max44009Utils_configInterrupt()
 *                                              - Max44009Utils_disableInterrupt()
 *                                              - Max44009Utils_uninitialize()
 * ****************************************************************************/
#ifndef BCDS_MAX44009UTILS_H_
#define BCDS_MAX44009UTILS_H_

#include "BCDS_Retcode.h"
#include "gpio.h"
#include "SPI_ih.h"
#include "I2C_ih.h"

/* public interface declaration */

/* public type and macro definitions */
/** @brief Enumeration to represent the communication bus used by Max44009*/
enum Max44009Utils_Bus_E
{
    MAX44009UTILS_I2C_BUS,
    MAX44009UTILS_SPI_BUS
};
typedef enum Max44009Utils_Bus_E Max44009Utils_Bus_T;

/** @brief union holding the information regarding the interface configurations*/
union Max44009Utils__InterfaceConfig_U
   {
       I2C_BusInfo_T i2cBus;
       SPI_device_t spiBus;
   };
typedef union Max44009Utils__InterfaceConfig_U Max44009Utils__InterfaceConfig_T;

/** @brief structure to hold information about the communication channel used by Max44009*/
struct Max44009Utils_Interface_S
{
    Max44009Utils_Bus_T bus;
    Max44009Utils__InterfaceConfig_T type;
};
typedef struct Max44009Utils_Interface_S Max44009Utils_Interface_T;

/** @brief structure to hold the Max44009 specific configurations like GPIO ,I2c and interrupt pin details.
 */
struct Max44009Utils_Info_S
{
    Max44009Utils_Interface_T interface;
    Gpio_T sensorPower;
    Gpio_PinInterrupt_T interruptPin;
};
typedef struct Max44009Utils_Info_S Max44009Utils_Info_T, *Max44009Utils_InfoPtr_T;

/** @brief structure representing the configuration of Max44009 Threshold interrupt */
struct Max44009Utils_ThresholdIntrConfig_S
{
    uint8_t upperThreshold; /**< value to be programmed in upper threshold register for Max44009 interrupt */
    uint8_t lowerThreshold; /**< value to be programmed in lower threshold register for Max44009 interrupt */
    uint8_t thresholdTimer; /**< value to be programmed in threshold timer register for Max44009 interrupt */
};
typedef struct Max44009Utils_ThresholdIntrConfig_S Max44009Utils_ThresholdIntrConfig_T, *Max44009Utils_ThresholdIntrConfigPtr_T;

/* public function prototype declarations */
/**
 * @brief        Function to initialize Max44009.  This needs to be called before calling any other API.
 *
 * @param [in]   max44009Info :structure holding the Max44009 specific configurations
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             Max44009 initialized successfully
 * @retval       RETCODE_FAILURE        Max44009 initialization failed
 * @retval       RETCODE_INVALID_PARAM  Max44009 initialization failed because of invalid input parameter
 *
 * @note
 * 1) The support of multiple instances is not possible due to the limitation in the library.
 * 2) Calling Max44009 initialization API present in sensorUtils (Max44009Utils_initialize) and sensorDriver (MAX09_init)
 *    concurrently will lead to unexpected behaviour.
 */
Retcode_T Max44009Utils_initialize(Max44009Utils_InfoPtr_T max44009Info);

/**
 * @brief        Function to Configure interrupts of Max44009.  Before calling this API Update
 *               the below information in the appropriate structure
 *                      1) Update callback function in Max44009Utils_InfoPtr_T.
 *                      2) Update the type of callback(deferred or real time) in  Max44009Utils_InfoPtr_T.
 *               After calling this API the given interrupt will be enabled and the callback will be
 *               associated with the interrupt.
 *
 * @param [in]   max44009Info            :Structure holding the Max44009 specific configurations.Callback for the interrupt and its type
 *                                        should be given in this handle
 *
 * @param [in]   interruptConfig         :Structure holding the information about interrupt related configuration should be given here
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             Max44009 interrupt configuration success
 * @retval       RETCODE_FAILURE        Max44009 interrupt configuration failure
 * @retval       RETCODE_INVALID_PARAM  Max44009 interrupt configuration failed because of invalid input parameter
 */
Retcode_T Max44009Utils_configInterrupt(Max44009Utils_InfoPtr_T max44009Info,
        Max44009Utils_ThresholdIntrConfigPtr_T interruptConfig);

/**
 * @brief        Function to Disable the Interrupt.Max44009 sensor supports only Threshold Interrupt.
 *                  So, calling this API will disable Max44009 Threshold interrupt explicitly.
 *
 * @param [in]   max44009Info     :structure holding the Max44009 specific configurations
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             Max44009 disable interrupt success
 * @retval       RETCODE_FAILURE        Max44009 disable interrupt failure
 * @retval       RETCODE_INVALID_PARAM  Max44009 disable interrupt failed because of invalid input parameter
 */
Retcode_T Max44009Utils_disableInterrupt(Max44009Utils_InfoPtr_T max44009Info);

/**
 * @brief          Function to uninitialize Max44009.After calling this API sensor is no more usable without initializing
 *                 it again by calling Max44009Utils_initialize() API
 *
 * @param [in]     max44009Info :structure holding the Max44009 specific configurations
 *
 * @retval         Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval         RETCODE_OK             Max44009 un-initialization success
 * @retval         RETCODE_FAILURE        Max44009 un-initialization failure
 * @retval         RETCODE_INVALID_PARAM  Max44009 un-initialization failed because of invalid input parameter
 */
Retcode_T Max44009Utils_uninitialize(Max44009Utils_InfoPtr_T max44009Info);

#endif /* BCDS_MAX44009UTILS_H_ */

