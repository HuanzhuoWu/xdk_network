/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup BME280_SensorUtils BME280_SensorUtils
 *  @{
 *  @brief SensorUtils Interfaces
 *  @details
 *  The interface header exports the following features:
 *  										-  Bme280Utils_initialize()
 *  										   Bme280Utils_uninitialize()
 *
 * ****************************************************************************/

#ifndef BCDS_BME280UTILS_H_
#define BCDS_BME280UTILS_H_


#include "BCDS_Retcode.h"
#if defined(Hydrogen)
#include "SPI.h"
#include "PIN.h"
#else
#include "gpio.h"
#include "SPI_ih.h"
#include "I2C_ih.h"
#endif


/* public interface declaration */

/* public type and macro definitions */
/** Enumeration to represent the communication bus supported by bme280*/
enum Bme280Utils_Bus_E
{
    BME280UTILS_I2C_BUS,
    BME280UTILS_SPI_BUS
};
typedef enum Bme280Utils_Bus_E Bme280Utils_Bus_T;

#if defined(Hydrogen)
/** @brief struct holding the information regarding the interface configurations*/
struct Bme280Utils_InterfaceConfig_S
{
    SPI_Handle spiHandle;
    PIN_Handle pinHandle;
};
typedef struct Bme280Utils_InterfaceConfig_S Bme280Utils_InterfaceConfig_T;
#else
/** @brief union holding the information regarding the interface configurations*/
union Bme280Utils_InterfaceConfig_U
   {
       I2C_BusInfo_T i2cBus;
       SPI_device_t spiBus;
   };
typedef union Bme280Utils_InterfaceConfig_U Bme280Utils_InterfaceConfig_T;
#endif

/** structure to hold information regarding the communication channel */
struct Bme280Utils_Interface_S
{
    Bme280Utils_InterfaceConfig_T type;
    Bme280Utils_Bus_T bus;
};
typedef struct Bme280Utils_Interface_S Bme280Utils_Interface_T;

/** @brief structure to hold the Environmental specific GPIO ,I2c configurations to initialize
  configure and get the data from the sensor */
struct Bme280Utils_Info_S
{
    Bme280Utils_Interface_T interface; /**< communication Bus configuration to be used for this sensor*/
#if defined(Hydrogen)
#else
    Gpio_T sensorPower; /**< Power enable Pin for sensor*/
#endif
};
typedef struct Bme280Utils_Info_S Bme280Utils_Info_T,*Bme280Utils_InfoPtr_T;

/* public function prototype declarations */
/**
 * @brief        Function to initialize Environmental.This needs to be called before calling any other API
 *
 * @param [in]   bme280Info :structure holding the bme280 specific configurations
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BME280 initialized successfully
 * @retval       RETCODE_FAILURE        BME280 initialization failed
 * @retval       RETCODE_INVALID_PARAM  BME280 initialization failed because of invalid input parameter
 *
 * @note
 * 1) The support of multiple instances is not possible due to the limitation in the BST library.
 * 2) Calling Bme280 initialization API present in sensorUtils (Bme280Utils_initialize) and BST library (bme280_init)
 *    concurrently will lead to unexpected behaviour.
 */
Retcode_T Bme280Utils_initialize(Bme280Utils_InfoPtr_T bme280Info);

/**
 * @brief          Function to uninitialize Environmental.After calling this API sensor is no more usable without initializing it again by calling Bme280Utils_initialize() API
 *
 * @param [in]     bme280Info :structure holding the bme280 specific configurations
 *
 * @retval         Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval         RETCODE_OK             BME280 un-initialization success
 * @retval         RETCODE_FAILURE        BME280 un-initialization failure
 * @retval         RETCODE_INVALID_PARAM  BME280 un-initialization failed because of invalid input parameter
 */
Retcode_T Bme280Utils_uninitialize(Bme280Utils_InfoPtr_T bme280Info);


#endif /* BCDS_BME280UTILS_H_ */

/**@} */
