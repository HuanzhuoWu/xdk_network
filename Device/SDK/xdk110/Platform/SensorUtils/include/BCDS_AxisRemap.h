/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 /**
 *  @defgroup Axis_Remap Axis Remap
 *  @{
 *  @brief Axis Remap for Sensors
 *  @details
 * 				Axis Remap for the Sensors.
 *
 * ****************************************************************************/


/* header definition ******************************************************** */
#ifndef BCDS_AXISREMAP_H_
#define BCDS_AXISREMAP_H_

/* public interface declaration ********************************************* */
#include "BCDS_Retcode.h"

/* public type and macro definitions */
/* structure for XYZ axis data of sensors*/
struct AxisRemap_Data_S
{
    int32_t x; /* x-axis data */
    int32_t y; /* y-axis data */
    int32_t z; /* Z-axis data */
};

typedef struct AxisRemap_Data_S AxisRemap_Data_T;

/****************************************************************************/
/*
 * Axis Remap Config bit fields
 *          Z        Y      X
 *  b7 b6 (b5 b4) (b3 b2) (b1 b0)
 *  0  0    0  0    0  0    0  0  (0x00)    --- X --> X , Y --> Y , Z --> Z
 *
 *  0  0    0  0    0  0    0  1  (0x01)    --- Y to--> X
 *  0  0    0  0    0  0    1  0  (0x02)    --- Z to--> X
 *
 *  0  0    0  0    0  1    0  0  (0x04)    --- X to--> Y
 *  0  0    0  0    1  0    0  0  (0x08)    --- Z to--> Y
 *
 *  0  0    0  1    0  0    0  0  (0x10)    --- X to--> Z
 *  0  0    1  0    0  0    0  0  (0x20)    --- Y to--> Z
 *
 ****************************************************************************/

/****************************************************************************/
/**
 *  Axis Sign Config bit fields
 *                      Z   Y   X
 *  b7 b6   b5 b4  b3 (b2   b1 b0)
 *  0  0    0  0    0  0    0  0  (0x00)    ---  X-->Pos ,Y-->Pos,Z-->Pos
 *
 *  0  0    0  0    0  0    0  1  (0x01)    --- X --> Neg
 *  0  0    0  0    0  0    1  0  (0x02)    --- Y --> Neg
 *  0  0    0  0    0  1    0  0  (0x04)    --- Z --> Neg
 *
 ****************************************************************************/
/* structure for declaring the Axis Configuration according to board */
struct AxisRemap_Config_S
{
    uint8_t axisConfig;
    uint8_t axisSign;
};

typedef struct AxisRemap_Config_S AxisRemap_Config_T;

#endif /* BCDS_AXISREMAP_H_ */

/**@} */

/** ************************************************************************* */
