/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup BMA280_SensorUtils BMA280_SensorUtils
 *  @{
 *  @brief SensorUtils Interfaces of BMA280
 *  @details
 *  The interface header exports the following features:
 *                                              - Bma280Utils_initialize()
 *                                              - Bma280Utils_configInterrupt()
 *                                              - Bma280Utils_disableInterrupt()
 *                                              - Bma280Utils_remapAxis()
 *                                              - Bma280Utils_uninitialize()
 * ****************************************************************************/
#ifndef BCDS_BMA280UTILS_H_
#define BCDS_BMA280UTILS_H_

#include "BCDS_Retcode.h"
#include "BCDS_AxisRemap.h"
#include "gpio.h"
#include "SPI_ih.h"
#include "I2C_ih.h"

/* public type and macro definitions */
/** @brief Enumeration to represent supported interrupt types of Bma280 */
enum Bma280Utils_IntrType_E
{
    BMA280UTILS_NEW_DATA_INTERRUPT,
    BMA280UTILS_SLOPE_INTERRUPT,
    BMA280UTILS_DOUBLE_TAP_INTERRUPT,
    BMA280UTILS_LOW_G_INTERRUPT,
    BMA280UTILS_INVALID_INTERRUPT
};
typedef enum Bma280Utils_IntrType_E Bma280Utils_IntrType_T;

/** @brief Enumeration to represent supported interrupt channels of Bma280 */
enum Bma280Utils_IntrChannel_E
{
    BMA280UTILS_INTERRUPT_CHANNEL1,
    BMA280UTILS_INTERRUPT_CHANNEL2,
    BMA280UTILS_INTERRUPT_CHANNEL_INVALID
};
typedef enum Bma280Utils_IntrChannel_E Bma280Utils_IntrChannel_T;

/** @brief Enumeration to represent the communication bus used by bma280*/
enum Bma280Utils_Bus_E
{
    BMA280UTILS_I2C_BUS,
    BMA280UTILS_SPI_BUS
};
typedef enum Bma280Utils_Bus_E Bma280Utils_Bus_T;

/** @brief union holding the information regarding the interface configurations*/
union Bma280Utils_InterfaceConfig_U
   {
       I2C_BusInfo_T i2cBus;
       SPI_device_t spiBus;
   };
typedef union Bma280Utils_InterfaceConfig_U Bma280Utils_InterfaceConfig_T;

/** @brief structure to hold information about the communication channel used by Bma280*/
struct Bma280Utils_Interface_S
{
    Bma280Utils_Bus_T bus;
    Bma280Utils_InterfaceConfig_T type;
};
typedef struct Bma280Utils_Interface_S Bma280Utils_Interface_T;

/** @brief Structure containing the information about the sensor's power pin and port. It also contains the flag which provides information
 * whether the sensor's power pin can be controlled. */
struct Bma280Utils_SensorPower_S
{
    Gpio_T SensorPower;
    uint8_t PowerControlFlag; /**< Set 1/0 if the sensor's power pin can be controlled. */
};

typedef struct Bma280Utils_SensorPower_S Bma280Utils_SensorPower_T;

/** @brief structure to hold the Bma280 specific configurations like GPIO ,I2c and axisRemap
 */
struct Bma280Utils_Info_S
{
    Bma280Utils_Interface_T interface;
    Bma280Utils_SensorPower_T sensorPowerInfo;
    Gpio_PinInterrupt_T interruptPin1;
    Gpio_PinInterrupt_T interruptPin2;
    AxisRemap_Config_T remap;
};
typedef struct Bma280Utils_Info_S Bma280Utils_Info_T, *Bma280Utils_InfoPtr_T;

/** @brief structure representing the configuration of Bma280 slope interrupt */
struct Bma280Utils_SlopeIntrConfig_S
{
    uint8_t slopeEnableX; /**< set 1/0 to enable/disable slope interrupt for X axis */
    uint8_t slopeEnableY; /**< set 1/0 to enable/disable slope interrupt for Y axis */
    uint8_t slopeEnableZ; /**< set 1/0 to enable/disable slope interrupt for Z axis */
    uint8_t slopeThreshold; /**< Interrupt triggers on enabled axis, whenever it exceeds this threshold value */
    uint8_t slopeDuration; /**< Interrupt triggers on enabled axis, when threshold value exceeds and remains for duration mentioned in this field */
};
typedef struct Bma280Utils_SlopeIntrConfig_S Bma280Utils_SlopeIntrConfig_T, *Bma280Utils_SlopeIntrConfigPtr_T;

/** @brief Pointer to represent interrupt configuration structure */
typedef void* Bma280Utils_IntrConfigPtr_T;

/** @brief structure to represent interrupt configuration of Bma280*/
struct Bma280Utils_ConfigInterrupt_S
{
    Bma280Utils_IntrType_T interruptType;
    Bma280Utils_IntrChannel_T interruptChannel;
    Bma280Utils_IntrConfigPtr_T configInterrupt;
};
typedef struct Bma280Utils_ConfigInterrupt_S Bma280Utils_ConfigInterrupt_T, *Bma280Utils_ConfigInterruptPtr_T;

/* public function prototype declarations */
/**
 * @brief        Function to initialize Bma280.  This needs to be called before calling any other API.
 *
 * @param [in]   bma280Info :structure holding the bma280 specific configurations
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BMA280 initialized successfully
 * @retval       RETCODE_FAILURE        BMA280 initialization failed
 * @retval       RETCODE_INVALID_PARAM  BMA280 initialization failed because of invalid input parameter
 *
 * @note
 * 1) The support of multiple instances is not possible due to the limitation in the BST library.
 * 2) Calling Bma280 initialization API present in sensorUtils (Bma280Utils_initialize) and BST library (bma2x2_init)
 *    concurrently will lead to unexpected behaviour.
 */
Retcode_T Bma280Utils_initialize(Bma280Utils_InfoPtr_T bma280Info);

/**
 * @brief        Function to Configure interrupts of Bma280.  Before calling this API Update
 *               the below information in the appropriate structure
 *                      1) Update callback function in Bma280Utils_InfoPtr_T.
 *                      2) Update the type of callback(deferred or real time) in  Bma280Utils_InfoPtr_T.
 *                      3) Update interrupt type and channel to Which interrupt needs be associated in Bma280Utils_ConfigInterrupt_T.
 *                      4) Update Bma280Utils_IntrConfigPtr_T with appropriate interrupt configuration
 *               After calling this API the given interrupt will be enabled on the given channel and
 *               the callback will be associated with the interrupt.
 *
 * @param [in]   bma280Info            :Structure holding the bma280 specific configurations.Callback for the interrupt and its type
 *                                      should be given in this handle
 *
 * @param [in]   interruptConfig       :Structure holding the information about interrupt type,channel and its related configuration
 *                                      should be given here
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BMA280 interrupt configuration success
 * @retval       RETCODE_FAILURE        BMA280 interrupt configuration failure
 * @retval       RETCODE_INVALID_PARAM  BMA280 interrupt configuration failed because of invalid input parameter
 */
Retcode_T Bma280Utils_configInterrupt(Bma280Utils_InfoPtr_T bma280Info,
        Bma280Utils_ConfigInterruptPtr_T interruptConfig);

/**
 * @brief        Function to Disable the Interrupt.
 *
 * @param [in]   bma280Info     :structure holding the Bma280 specific configurations
 *
 * @param [in]   interruptType  :type of interrupt that needs to be disabled
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BMA280 disable interrupt success
 * @retval       RETCODE_FAILURE        BMA280 disable interrupt failure
 * @retval       RETCODE_INVALID_PARAM  BMA280 disable interrupt failed because of invalid input parameter
 */
Retcode_T Bma280Utils_disableInterrupt(
        Bma280Utils_InfoPtr_T bma280Info,
        Bma280Utils_IntrType_T interruptType,
        Bma280Utils_IntrChannel_T interruptChannel);

/**
 * @brief           Function to read remapped Bma280 data.
 *
 * @param [in]      bma280Info :structure holding the Bma280 specific configurations.  The information about remap configuration should be given
 *                  in this struct
 *
 * @param [in,out]  accelData  : while calling this API structure will hold the Bma280 data that needs to be
 *                               the remapped.  After calling this API the structure will hold remapped data
 *
 * @retval          Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval          RETCODE_OK             BMA280 axis remap success
 * @retval          RETCODE_FAILURE        BMA280 axis remap failure
 * @retval          RETCODE_INVALID_PARAM  BMA280 axis remap failed because of invalid input parameter
 */
Retcode_T Bma280Utils_remapAxis(Bma280Utils_InfoPtr_T bma280Info,
        AxisRemap_Data_T *accelData);

/**
 * @brief          Function to uninitialize Accel.After calling this API sensor is no more usable without initializing it again by calling Bma280Utils_initialize() API
 *
 * @param [in]     bma280Info :structure holding the bma280 specific configurations
 *
 * @retval       Retcode_T    The return value consist of (First MSB Byte represents Package ID,
 *                            Next  Byte represents Severity and Last 2LSB Bytes represents error code).\n
 *                            Below are the possible error codes
 *
 * @retval       RETCODE_OK             BMA280 un-initialization success
 * @retval       RETCODE_FAILURE        BMA280 un-initialization failure
 * @retval       RETCODE_INVALID_PARAM  BMA280 un-initialization failed because of invalid input parameter
 */
Retcode_T Bma280Utils_uninitialize(Bma280Utils_InfoPtr_T bma280Info);

#endif /* BCDS_BMA280UTILS_H_ */

