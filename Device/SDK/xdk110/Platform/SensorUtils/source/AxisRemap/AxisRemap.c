/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* own header files */
#include "BCDS_AxisRemap.h"
#include "SensorUtilsAxisRemap.h"

/* additional interface header files */

/* constant definitions ***************************************************** */
#define MASK_TWO_LEAST_SIGNIFICANT_BITS  UINT32_C(0x03)                  /* Masking condition for all axis config variable */
#define Y_AXIS_CONFIG_SHIFT_VAL          UINT32_C(0x02)                  /* shift config variable for Y axis */
#define Z_AXIS_CONFIG_SHIFT_VAL          UINT32_C(0x04)                  /* shift config variable for Z axis */
#define X_AXIS_SIGN_MASK_VAL             UINT32_C(0x01)                  /* Masking value for sign of X axis */
#define Y_AXIS_SIGN_MASK_VAL             UINT32_C(0x02)                  /* Masking value for sign of Y axis */
#define Z_AXIS_SIGN_MASK_VAL             UINT32_C(0x04)                  /* Masking value for sign of Z axis */
#define MAP_Y_TO_X                       UINT32_C(0x01)                  /* Condition check for mapping Y to X */
#define MAP_Z_TO_X                       UINT32_C(0x02)                  /* Condition check for mapping Z to X */
#define MAP_X_TO_Y                       UINT32_C(0x01)                  /* Condition check for mapping X to Y */
#define MAP_Z_TO_Y                       UINT32_C(0x02)                  /* Condition check for mapping Z to Y */
#define MAP_X_TO_Z                       UINT32_C(0x01)                  /* Condition check for mapping X to Z */
#define MAP_Y_TO_Z                       UINT32_C(0x02)                  /* Condition check for mapping Y to Z */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/* API documentation is in the interface header file */
Retcode_T AxisRemap_remapSensorData(AxisRemap_Data_T actualSensorDataXyz,
        AxisRemap_Config_T remapConfigData, AxisRemap_Data_T *remapSensorDataXyz_p)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == remapSensorDataXyz_p)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_INVALID_PARAM));
    }

    uint8_t allAxisCfgVariable = UINT8_C(0);
    uint8_t allAxisSignVariable = UINT8_C(0);
    uint8_t xAxisCfgVariable = UINT8_C(0);
    uint8_t xAxisSignVariable = UINT8_C(0);
    uint8_t yAxisCfgVariable = UINT8_C(0);
    uint8_t yAxisSignVariable = UINT8_C(0);
    uint8_t zAxisCfgVariable = UINT8_C(0);
    uint8_t zAxisSignVariable = UINT8_C(0);

    allAxisCfgVariable = remapConfigData.axisConfig;
    allAxisSignVariable = remapConfigData.axisSign;

    if ((allAxisCfgVariable < UINT8_C(64)) && (allAxisSignVariable < UINT8_C(8)))
    {
        /* Extract Axis and Sign configurations for X axis*/

        xAxisCfgVariable = allAxisCfgVariable;
        xAxisCfgVariable &= MASK_TWO_LEAST_SIGNIFICANT_BITS;
        xAxisSignVariable = allAxisSignVariable & X_AXIS_SIGN_MASK_VAL;

        /* Extract Axis and Sign configurations for Y axis*/

        yAxisCfgVariable = allAxisCfgVariable >> Y_AXIS_CONFIG_SHIFT_VAL;
        yAxisCfgVariable &= MASK_TWO_LEAST_SIGNIFICANT_BITS;
        yAxisSignVariable = allAxisSignVariable & Y_AXIS_SIGN_MASK_VAL;

        /* Extract Axis and Sign configurations for Z axis*/

        zAxisCfgVariable = allAxisCfgVariable >> Z_AXIS_CONFIG_SHIFT_VAL;
        zAxisCfgVariable &= MASK_TWO_LEAST_SIGNIFICANT_BITS;
        zAxisSignVariable = allAxisSignVariable & Z_AXIS_SIGN_MASK_VAL;

        /*Re-mapping for X axis based on the Configuration data*/
        if (xAxisCfgVariable == MAP_Y_TO_X)
        {
            remapSensorDataXyz_p->x = actualSensorDataXyz.y;
        }
        else if (xAxisCfgVariable == MAP_Z_TO_X)
        {
            remapSensorDataXyz_p->x = actualSensorDataXyz.z;
        }
        else
        {
            remapSensorDataXyz_p->x = actualSensorDataXyz.x;
        }
        if (xAxisSignVariable)
        {
            remapSensorDataXyz_p->x = (-remapSensorDataXyz_p->x);
        }

        /* Re-mapping for Y axis based on the Configuration data*/

        if (yAxisCfgVariable == MAP_X_TO_Y)
        {
            remapSensorDataXyz_p->y = actualSensorDataXyz.x;
        }
        else if (yAxisCfgVariable == MAP_Z_TO_Y)
        {
            remapSensorDataXyz_p->y = actualSensorDataXyz.z;
        }
        else
        {
            remapSensorDataXyz_p->y = actualSensorDataXyz.y;
        }
        if (yAxisSignVariable)
        {
            remapSensorDataXyz_p->y = (-remapSensorDataXyz_p->y);
        }

        /*Re-mapping for Z axis based on the Configuration data*/

        if (zAxisCfgVariable == MAP_X_TO_Z)
        {
            remapSensorDataXyz_p->z = actualSensorDataXyz.x;
        }
        else if (zAxisCfgVariable == MAP_Y_TO_Z)
        {
            remapSensorDataXyz_p->z = actualSensorDataXyz.y;
        }
        else
        {
            remapSensorDataXyz_p->z = actualSensorDataXyz.z;
        }
        if (zAxisSignVariable)
        {
            remapSensorDataXyz_p->z = (-remapSensorDataXyz_p->z);
        }
        returnValue = RETCODE_OK;

    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    if (RETCODE_OK != returnValue)
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }
    return (returnValue);
}
