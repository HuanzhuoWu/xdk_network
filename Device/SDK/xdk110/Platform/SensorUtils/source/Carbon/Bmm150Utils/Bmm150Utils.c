/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */

/* own header files */
#include "BCDS_Bmm150Utils.h"
#include "SensorUtilsMapping.h"
#include "SensorUtilsAxisRemap.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include <bmm050.h>
#include "BCDS_Assert.h"

/** structure to hold the initializations of BMM050 */
static struct bmm050_t bmm150Initialization;

#define DELAY_4MS            UINT8_C(4) /**< macro used to represent 4ms delay */
#define BMM050_SUCCESS       UINT8_C(0) /**< macro used to represent success of library  */
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */

/**< variable to check initialization status */
static bool initializationStatus = false;
/**
 * @brief       This function maps error codes returned from BMM150 library to retcode values
 *
 * @param [in]   BMA2x2_RETURN_FUNCTION_TYPE Return value from BMA2X2 library
 *
 * @retval      RETCODE_SUCCESS         BMM150 sensor API call success
 * @retval      RETCODE_FAILURE         BMM150 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMM150 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(BMM050_RETURN_FUNCTION_TYPE bmmLibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (BMM050_SUCCESS == bmmLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_SUCCESS;
    }
    else if (E_BMM050_NULL_PTR == bmmLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}

/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Bmm150Utils_initialize(Bmm150Utils_InfoPtr_T bmm150Info)
{
    BMM050_RETURN_FUNCTION_TYPE bmmLibReturn = E_BMM050_OUT_OF_RANGE;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == bmm150Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    /* enable the power pin of the BMM150 sensor */
    if ((bmm150Info->sensorPower.port <= PTD_MAXIMUM_PORT)
            && (bmm150Info->sensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(bmm150Info->sensorPower.port,
                bmm150Info->sensorPower.pin);
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_4MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    }

    if (BMM150UTILS_I2C_BUS == bmm150Info->interface.bus)
    {
        if (I2CCHANNEL_0 == bmm150Info->interface.type.i2cBus.I2C_Channel)
        {
            bmm150Initialization.bus_read = BMM150UTILS_I2CBUSREAD_CHANNEL0;
            bmm150Initialization.bus_write = BMM150UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1
                == bmm150Info->interface.type.i2cBus.I2C_Channel)
        {
            bmm150Initialization.bus_read = BMM150UTILS_I2CBUSREAD_CHANNEL1;
            bmm150Initialization.bus_write = BMM150UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM));
        }
        bmm150Initialization.delay_msec = BMM150UTILS_I2C_DELAY;
        bmm150Initialization.dev_addr =
                bmm150Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    /*initialize the sensor */
    bmmLibReturn = bmm050_init(&bmm150Initialization);
    if (BMM050_SUCCESS == bmmLibReturn)
    {
        bmmLibReturn = bmm050_set_functional_state(BMM050_NORMAL_MODE);
    }

    returnValue = libErrorMapping(bmmLibReturn);
    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmm150Utils_remapAxis(Bmm150Utils_InfoPtr_T bmm150Info,
        AxisRemap_Data_T *bmiData)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    AxisRemap_Data_T actualData;

    if ((NULL == bmm150Info) || (NULL == bmiData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_UNINITIALIZED));
    }

    /** copying the actual sensor data into a local variable */
    actualData.x = bmiData->x;
    actualData.y = bmiData->y;
    actualData.z = bmiData->z;

    returnValue = AxisRemap_remapSensorData(actualData, bmm150Info->remap,
            bmiData);

    if ((RETCODE_OK != returnValue)
            && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmm150Utils_uninitialize(Bmm150Utils_InfoPtr_T bmm150Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == bmm150Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    /* disable the Sensor power and interrupt pin */
    if ((bmm150Info->sensorPower.port <= PTD_MAXIMUM_PORT)
            && (bmm150Info->sensorPower.pin <= PTD_MAXIMUM_PIN))

    {
        PTD_pinOutClear(bmm150Info->sensorPower.port,
                bmm150Info->sensorPower.pin);
        initializationStatus = false;
        returnValue = RETCODE_OK;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}
