/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */
#include "BCDS_Basics.h"

/* own header files */
#include "BCDS_Bmi160Utils.h"
#include "SensorUtilsMapping.h"
#include "SensorUtilsAxisRemap.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include <bmi160.h>
#include "BCDS_Assert.h"

/** This macro indicates the delay required for BMI160_ACCEL while switching
 from Suspend/low power mode to normal mode  */
#define ACCEL_DELAY                      UINT32_C(4)
/** This macro indicates the delay required for BMI160_GYRO while switching
 from Suspend mode to normal mode  */
#define GYRO_DELAY                       UINT32_C(55)
#define DELAY_10MS           UINT8_C(10) /**< macro used to represent 10ms delay  */
#define ENABLE_INTERRUPT     UINT8_C(1)  /**< macro used to enable the interrupt */
#define BMI160_SUCCESS           UINT8_C(0) /**< macro used to represent success of library  */
#define BMI_ACCEL_MODE_NORMAL   UINT8_C(0x11)
#define BMI_GYRO_MODE_NORMAL   UINT8_C(0x15)
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */

/**< variable to check initialization status */
static bool initializationStatus = false;

/** structure to hold the initializations of BMI160*/
static struct bmi160_t bmi160Initialization;

/**
 * @brief       This function maps error codes returned from BMI160 library to retcode values
 *
 * @param [in]   BMA2x2_RETURN_FUNCTION_TYPE Return value from BMA2X2 library
 *
 * @retval      RETCODE_OK              BMI160 sensor API call success
 * @retval      RETCODE_FAILURE         BMI160 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMI160 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(BMI160_RETURN_FUNCTION_TYPE bmiLibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (BMI160_SUCCESS == bmiLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_SUCCESS;
    }
    else if (E_BMI160_NULL_PTR == bmiLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}
/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Bmi160Utils_initialize(Bmi160Utils_InfoPtr_T bmi160Info)
{
    BMI160_RETURN_FUNCTION_TYPE bmiLibReturn = E_BMI160_NULL_PTR;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == bmi160Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    /* enable the power pin of the BMI160 sensor */
    if ((bmi160Info->sensorPower.port <= PTD_MAXIMUM_PORT)
            && (bmi160Info->sensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(bmi160Info->sensorPower.port,
                bmi160Info->sensorPower.pin);
        /* Start up time delay required for BMI160 sensor*/
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_10MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    }

    /* configure the interrupt for interrupt channel 1 of BMI160 sensor */
    if (ENABLE_INTERRUPT == bmi160Info->interruptPin1.enable)
    {
        if ((bmi160Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bmi160Info->interruptPin1.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bmi160Info->interruptPin1.gpio.port,
                    bmi160Info->interruptPin1.gpio.pin,
                    bmi160Info->interruptPin1.risingEdge,
                    bmi160Info->interruptPin1.fallingEdge,
                    GPIO_INTERRUPT_DISABLE_FLAG,
                    bmi160Info->interruptPin1.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    /* configure the interrupt for interrupt channel 2 of BMI160 sensor */
    if (ENABLE_INTERRUPT == bmi160Info->interruptPin2.enable)
    {
        if ((bmi160Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bmi160Info->interruptPin2.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bmi160Info->interruptPin2.gpio.port,
                    bmi160Info->interruptPin2.gpio.pin,
                    bmi160Info->interruptPin2.risingEdge,
                    bmi160Info->interruptPin2.fallingEdge,
                    GPIO_INTERRUPT_DISABLE_FLAG,
                    bmi160Info->interruptPin2.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    if (BMI160UTILS_I2C_BUS == bmi160Info->interface.bus)
    {
        if (I2CCHANNEL_0 == bmi160Info->interface.type.i2cBus.I2C_Channel)
        {
            bmi160Initialization.bus_read = BMI160UTILS_I2CBUSREAD_CHANNEL0;
            bmi160Initialization.bus_write = BMI160UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1 == bmi160Info->interface.type.i2cBus.I2C_Channel)
        {
            bmi160Initialization.bus_read = BMI160UTILS_I2CBUSREAD_CHANNEL1;
            bmi160Initialization.bus_write = BMI160UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            isFailed++;
        }
        bmi160Initialization.delay_msec = BMI160UTILS_I2C_DELAY;
        bmi160Initialization.dev_addr =
                bmi160Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    bmiLibReturn = bmi160_init(&bmi160Initialization);

    if (BMI160_SUCCESS == bmiLibReturn)
    {
        bmiLibReturn = bmi160_set_command_register(BMI_ACCEL_MODE_NORMAL);
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) ACCEL_DELAY / portTICK_RATE_MS);
    }

    if (BMI160_SUCCESS == bmiLibReturn)
    {
        returnValue = bmi160_set_command_register(BMI_GYRO_MODE_NORMAL);
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) GYRO_DELAY / portTICK_RATE_MS);
    }

    returnValue = libErrorMapping(bmiLibReturn);

    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue)
            && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmi160Utils_remapAxis(Bmi160Utils_InfoPtr_T bmi160Info,
        AxisRemap_Data_T *bmiData)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == bmi160Info) || (NULL == bmiData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_UNINITIALIZED));
    }

    /** copying the actual sensor data into a local variable */
    AxisRemap_Data_T actualData;
    actualData.x = bmiData->x;
    actualData.y = bmiData->y;
    actualData.z = bmiData->z;

    returnValue = AxisRemap_remapSensorData(actualData, bmi160Info->remap,
            bmiData);

    if ((RETCODE_OK != returnValue)
            && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmi160Utils_uninitialize(Bmi160Utils_InfoPtr_T bmi160Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == bmi160Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_UNINITIALIZED));
    }
    /* disable the Sensor power and interrupt pin */
    if ((bmi160Info->sensorPower.port <= PTD_MAXIMUM_PORT)
            && (bmi160Info->sensorPower.pin <= PTD_MAXIMUM_PIN)
            && (bmi160Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN)
            && (bmi160Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutClear(bmi160Info->sensorPower.port,
                bmi160Info->sensorPower.pin);
        PTD_intClear(bmi160Info->interruptPin1.gpio.pin);
        PTD_intDisable(bmi160Info->interruptPin1.gpio.pin);
        PTD_intClear(bmi160Info->interruptPin2.gpio.pin);
        PTD_intDisable(bmi160Info->interruptPin2.gpio.pin);
        initializationStatus = false;
        returnValue = RETCODE_OK;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }

    if ((RETCODE_OK != returnValue)
            && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

