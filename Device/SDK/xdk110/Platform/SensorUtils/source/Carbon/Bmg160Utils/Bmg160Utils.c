/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */
#include "BCDS_Basics.h"

/* own header files */
#include "BCDS_Bmg160Utils.h"
#include "SensorUtilsAxisRemap.h"
#include "SensorUtilsMapping.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include <bmg160.h>
#include "BCDS_Assert.h"

#define DELAY_30MS           UINT8_C(30) /**< macro used to represent 30ms delay  */
#define ENABLE_INTERRUPT     UINT8_C(1)  /**< macro used to enable the interrupt */
#define BMG160_SUCCESS       UINT8_C(0) /**< macro used to represent success of library  */
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */

/**< variable to check initialization status */
static bool initializationStatus = false;

/** structure to hold the initializations of BMA2X2 */
static struct bmg160_t bmg160initialization;

/**
 * @brief       This function maps error codes returned from BMG160 library to retcode values
 *
 * @param [in]   BMA2x2_RETURN_FUNCTION_TYPE Return value from BMA2X2 library
 *
 * @retval      RETCODE_OK              BMG160 sensor API call success
 * @retval      RETCODE_FAILURE         BMG160 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMG160 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(BMG160_RETURN_FUNCTION_TYPE bmgLibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (BMG160_SUCCESS == bmgLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_SUCCESS;
    }
    else if (E_BMG160_NULL_PTR == bmgLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}
/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Bmg160Utils_initialize(Bmg160Utils_InfoPtr_T bmg160Info)
{
    BMG160_RETURN_FUNCTION_TYPE bmgLibReturn = E_BMG160_NULL_PTR;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == bmg160Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    /* enable the power pin of BMG160 sensor */
    if ((bmg160Info->sensorPower.port <= PTD_MAXIMUM_PORT) && (bmg160Info->sensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(bmg160Info->sensorPower.port, bmg160Info->sensorPower.pin);
        /* Start up time delay required for BMA280 sensor*/
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_30MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    }

    /* configure the interrupt for interrupt channel 1 of BMG160 sensor */
    if ((ENABLE_INTERRUPT == bmg160Info->interruptPin1.enable))
    {
        if ((bmg160Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bmg160Info->interruptPin1.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bmg160Info->interruptPin1.gpio.port, bmg160Info->interruptPin1.gpio.pin,
                    bmg160Info->interruptPin1.risingEdge,
                    bmg160Info->interruptPin1.fallingEdge, GPIO_INTERRUPT_DISABLE_FLAG,
                    bmg160Info->interruptPin1.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    /* configure the interrupt for interrupt channel 2 of BMG160 sensor */
    if ((ENABLE_INTERRUPT == bmg160Info->interruptPin2.enable))
    {
        if ((bmg160Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bmg160Info->interruptPin2.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bmg160Info->interruptPin2.gpio.port, bmg160Info->interruptPin2.gpio.pin,
                    bmg160Info->interruptPin2.risingEdge,
                    bmg160Info->interruptPin2.fallingEdge, GPIO_INTERRUPT_DISABLE_FLAG,
                    bmg160Info->interruptPin2.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    if (BMG160UTILS_I2C_BUS == bmg160Info->interface.bus)
    {
        if (I2CCHANNEL_0 == bmg160Info->interface.type.i2cBus.I2C_Channel)
        {
            bmg160initialization.bus_read = BMG160UTILS_I2CBUSREAD_CHANNEL0;
            bmg160initialization.bus_write = BMG160UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1 == bmg160Info->interface.type.i2cBus.I2C_Channel)
        {
            bmg160initialization.bus_read = BMG160UTILS_I2CBUSREAD_CHANNEL1;
            bmg160initialization.bus_write = BMG160UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            isFailed++;
        }
        bmg160initialization.delay_msec = BMG160UTILS_I2C_DELAY;
        bmg160initialization.dev_addr = bmg160Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    bmgLibReturn = bmg160_init(&bmg160initialization);
    returnValue = libErrorMapping(bmgLibReturn);

    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmg160Utils_uninitialize(Bmg160Utils_InfoPtr_T bmg160Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == bmg160Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    /* disable the Sensor power and interrupt pin */
    if ((bmg160Info->sensorPower.port <= PTD_MAXIMUM_PORT) && (bmg160Info->sensorPower.pin <= PTD_MAXIMUM_PIN)
            && (bmg160Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN) && (bmg160Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN))

    {
        PTD_pinOutClear(bmg160Info->sensorPower.port, bmg160Info->sensorPower.pin);
        PTD_intClear(bmg160Info->interruptPin1.gpio.pin);
        PTD_intDisable(bmg160Info->interruptPin1.gpio.pin);
        PTD_intClear(bmg160Info->interruptPin2.gpio.pin);
        PTD_intDisable(bmg160Info->interruptPin2.gpio.pin);
        initializationStatus = false;
        returnValue = RETCODE_OK;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bmg160Utils_remapAxis(Bmg160Utils_InfoPtr_T bmg160Info,
        AxisRemap_Data_T *bmgData)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    if ((NULL == bmg160Info) || (NULL == bmgData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    /** copying the actual sensor data into a local variable */
    AxisRemap_Data_T actualData;
    actualData.x = bmgData->x;
    actualData.y = bmgData->y;
    actualData.z = bmgData->z;

    returnValue = AxisRemap_remapSensorData(actualData, bmg160Info->remap, bmgData);

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }
    return (returnValue);
}
