/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */
#include "BCDS_Basics.h"

/* own header files */
#include "BCDS_Bme280Utils.h"
#include "SensorUtilsMapping.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include <bme280.h>
#include "BCDS_Assert.h"

#define DELAY_10MS           UINT32_C(10) /**< macro used to represent 10ms delay  */
#define DELAY_100MS          UINT32_C(100) /**< macro used to represent 100ms delay  */
#define BME280_SUCCESS       UINT8_C(0) /**< macro used to represent success of library  */
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */

/** structure to hold the initializations of BME280*/
static struct bme280_t bme280Initialization;

/**< variable to check initialization status */
static bool initializationStatus = false;

/**
 * @brief       This function maps error codes returned from BME280 library to retcode values
 *
 * @param [in]   BME280_RETURN_FUNCTION_TYPE Return value from BME280 library
 *
 * @retval      RETCODE_SUCCESS         BME280 sensor API call success
 * @retval      RETCODE_FAILURE         BME280 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BME280 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(BME280_RETURN_FUNCTION_TYPE BME280_libReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (BME280_libReturn == BME280_SUCCESS)
    {
        returnValue = (Retcode_T) RETCODE_SUCCESS;
    }
    else if (BME280_libReturn == E_BME280_NULL_PTR)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}

/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Bme280Utils_initialize(Bme280Utils_InfoPtr_T bme280Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    BME280_RETURN_FUNCTION_TYPE bmeLibReturn = E_BME280_OUT_OF_RANGE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == bme280Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t ) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    /* enable the power pin for bme280 sensor */
    if ((bme280Info->sensorPower.port <= PTD_MAXIMUM_PORT) && (bme280Info->sensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(bme280Info->sensorPower.port,
                bme280Info->sensorPower.pin);
        /* Start up time delay required for BME280 sensor*/
        static_assert((portTICK_RATE_MS != 0), "Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_10MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    }

    if (BME280UTILS_I2C_BUS == bme280Info->interface.bus)
    {
        if (I2CCHANNEL_0 == bme280Info->interface.type.i2cBus.I2C_Channel)
        {
            bme280Initialization.bus_read = BME280UTILS_I2CBUSREAD_CHANNEL0;
            bme280Initialization.bus_write = BME280UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1 == bme280Info->interface.type.i2cBus.I2C_Channel)
        {
            bme280Initialization.bus_read = BME280UTILS_I2CBUSREAD_CHANNEL1;
            bme280Initialization.bus_write = BME280UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            isFailed++;
        }

        bme280Initialization.delay_msec = BME280UTILS_I2C_DELAY;
        bme280Initialization.dev_addr =
                bme280Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t ) RETCODE_INVALID_PARAM));
    }

    /*initialize the sensor */
    bmeLibReturn = bme280_init(&bme280Initialization);
    /* Set  BME280 default settings only if initialization success */
    if (BME280_SUCCESS == bmeLibReturn)
    {
        bmeLibReturn = bme280_set_oversamp_temperature(BME280_OVERSAMP_1X);
        if (BME280_SUCCESS == bmeLibReturn)
        {
            bmeLibReturn = bme280_set_oversamp_pressure(BME280_OVERSAMP_4X);
        }
        if (BME280_SUCCESS == bmeLibReturn)
        {
            bmeLibReturn = bme280_set_oversamp_humidity(BME280_OVERSAMP_2X);
        }
        if (BME280_SUCCESS == bmeLibReturn)
        {
            bmeLibReturn = bme280_set_standby_durn(BME280_STANDBY_TIME_125_MS);
        }
        if (BME280_SUCCESS == bmeLibReturn)
        {
            bmeLibReturn = bme280_set_power_mode(BME280_NORMAL_MODE);
        }
        vTaskDelay((portTickType) DELAY_100MS / portTICK_RATE_MS);
    }

    returnValue = libErrorMapping(bmeLibReturn);

    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bme280Utils_uninitialize(Bme280Utils_InfoPtr_T bme280Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == bme280Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL,
                (uint32_t ) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t ) RETCODE_UNINITIALIZED));
    }

    /* disable the Sensor power and interrupt pin */
    if ((bme280Info->sensorPower.port <= PTD_MAXIMUM_PORT)
            && (bme280Info->sensorPower.pin <= PTD_MAXIMUM_PIN))

    {
        PTD_pinOutClear(bme280Info->sensorPower.port,
                bme280Info->sensorPower.pin);
        initializationStatus = false;
        returnValue = RETCODE_OK;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }

    if ((RETCODE_OK != returnValue)
            && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) returnValue);
    }

    return (returnValue);
}
