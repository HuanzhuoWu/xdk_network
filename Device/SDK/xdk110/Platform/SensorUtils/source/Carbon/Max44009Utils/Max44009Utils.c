/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */

/* own header files */
#include "BCDS_Max44009Utils.h"
#include "SensorUtilsMapping.h"
#include "BCDS_Max44009.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include "BCDS_Assert.h"

#define DELAY_10MS           UINT8_C(10) /**< macro used to represent 10ms delay  */
#define DISABLE_INTERRUPT    UINT8_C(0X00) /**< macro used to disable the interrupt */
#define ENABLE_INTERRUPT     UINT8_C(0X01)  /**< macro used to enable the interrupt */
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */

/**< variable to check initialization status */
static bool initializationStatus = false;

/** structure to hold the initializations of Max44009 */
static MAX44009_Init_T max44009Initialization;

/**
 * @brief       This function maps error codes returned from max44009 library to retcode values
 *
 * @param [in]   max44009LibReturn Return value from max44009 library
 *
 * @retval      RETCODE_OK              max44009 sensor API call success
 * @retval      RETCODE_FAILURE         max44009 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   max44009 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(Retcode_T max44009LibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (RETCODE_OK == max44009LibReturn)
    {
        returnValue = RETCODE_OK;
    }
    else if ((Retcode_T)RETCODE_INVALID_PARAM == max44009LibReturn)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}

/**
 * @brief        Function to control the interrupt Pin.
 *
 * @param [in]   max44009Info :structure holding the MAX44009 specific configurations
 *
 * @param [in]   enable   indicates whether to disable or disable the interrupt pin
 *
 * @retval      RETCODE_OK              Returned when successfully changed gpio pin state.
 * @retval      RETCODE_FAILURE         Returned when gpio state is not modified.
 * @retval      RETCODE_INVALID_PARAM   Returned when there is an invalid input argument.
 */
static Retcode_T controlIntrPin(Max44009Utils_InfoPtr_T max44009Info, uint8_t enable)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (ENABLE_INTERRUPT == enable)
    {
        PTD_intEnable(max44009Info->interruptPin.gpio.pin);
        returnValue = RETCODE_OK;
    }
    else if (DISABLE_INTERRUPT == enable)
    {
        PTD_intClear(max44009Info->interruptPin.gpio.pin);
        PTD_intDisable(max44009Info->interruptPin.gpio.pin);
        returnValue = RETCODE_OK;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/**
 * @brief Function responsible to configure Threshold interrupt conditions in the support layer.
 *
 * @param [in]   interruptConfig structure representing to Threshold interrupt configuration
 *
 * @retval      RETCODE_OK                MAX44009 sensor configure Threshold interrupt success.
 * @retval      RETCODE_FAILURE           MAX44009 sensor configure Threshold interrupt failed.
 * @retval      RETCODE_INVALID_PARAM     MAX44009 sensor configure Threshold interrupt failed because of invalid input parameter.
 */
static Retcode_T configureThresholdInterrupt(Max44009Utils_ThresholdIntrConfigPtr_T interruptConfig)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    Retcode_T maxLibReturn = (Retcode_T) RETCODE_FAILURE;

    maxLibReturn = MAX44009_setUpperThreshold(interruptConfig->upperThreshold);
    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setLowerThreshold(interruptConfig->lowerThreshold);
    }

    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setThresholdTimer(interruptConfig->thresholdTimer);
    }

    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setIntrptReg(MAX44009_ENABLE_INTERRUPT);
    }

    returnValue = libErrorMapping(maxLibReturn);

    return (RETCODE(RETCODE_SEVERITY_ERROR, returnValue));
}

/**
 * @brief        Function to register callback for interrupt.
 *
 * @param [in]   max44009Info :structure holding the MAX44009 specific configurations
 *
 * @retval      RETCODE_OK              Returned when successfully registered callback function.
 * @retval      RETCODE_FAILURE         Returned when there is a problem in registering callback function.
 * @retval      RETCODE_INVALID_PARAM   Returned when there is an invalid input argument.
 */
static Retcode_T registerCallback(Max44009Utils_InfoPtr_T max44009Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    int32_t ptdReturnValue = INT32_C(-1);

    if (ENABLE_INTERRUPT == max44009Info->interruptPin.enable)
    {
        if (max44009Info->interruptPin.callBack != NULL)
        {
            ptdReturnValue = PTD_interruptSwitchCallback(max44009Info->interruptPin.gpio.pin, max44009Info->interruptPin.callBack);
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
        }
    }
    else
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /** Value returned by PTD (GPIO) driver is being overwritten here.
     * This has to be taken care when PTD or GPIO driver is implemented to return Retcode_T.
     * Otherwise, this function will not populate the failures of PTD (GPIO) driver */
    if (INT32_C(0) == ptdReturnValue)
        returnValue = RETCODE_OK;

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Max44009Utils_initialize(Max44009Utils_InfoPtr_T max44009Info)
{
    Retcode_T maxLibReturn = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == max44009Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    /* enable the power pin of the MAX44009 sensor */
    if ((max44009Info->sensorPower.port <= PTD_MAXIMUM_PORT) && (max44009Info->sensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(max44009Info->sensorPower.port, max44009Info->sensorPower.pin);
        /* Start up time delay required for MAX44009 sensor*/
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_10MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    }

    /* configure the interrupt for interrupt channel of MAX44009 sensor */
    if (ENABLE_INTERRUPT == max44009Info->interruptPin.enable)
    {
        if ((max44009Info->interruptPin.gpio.pin <= PTD_MAXIMUM_PIN)
                && (max44009Info->interruptPin.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(max44009Info->interruptPin.gpio.port, max44009Info->interruptPin.gpio.pin,
                    max44009Info->interruptPin.risingEdge,
                    max44009Info->interruptPin.fallingEdge, GPIO_INTERRUPT_DISABLE_FLAG,
                    max44009Info->interruptPin.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    if (MAX44009UTILS_I2C_BUS == max44009Info->interface.bus)
    {
        if (I2CCHANNEL_0 == max44009Info->interface.type.i2cBus.I2C_Channel)
        {
            max44009Initialization.readFunc = MAX44009UTILS_I2CBUSREAD_CHANNEL0;
            max44009Initialization.writeFunc = MAX44009UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1 == max44009Info->interface.type.i2cBus.I2C_Channel)
        {
            max44009Initialization.readFunc = MAX44009UTILS_I2CBUSREAD_CHANNEL1;
            max44009Initialization.writeFunc = MAX44009UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            isFailed++;
        }
        max44009Initialization.delayFunc = MAX44009UTILS_I2C_DELAY;
        max44009Initialization.dev_add = max44009Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    maxLibReturn = MAX44009_init(&max44009Initialization);
    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setManualModeReg(MAX44009_ENABLE_MANUAL_MODE);
    }

    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setIntegrationTime(MAX44009_100MS);
    }

    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setCdr(MAX44009_HIGH_BRIGHTNESS);
    }

    if (RETCODE_OK == maxLibReturn)
    {
        maxLibReturn = MAX44009_setContinousModeReg(MAX44009_ENABLE_CONTINOUS_MODE);
    }

    returnValue = libErrorMapping(maxLibReturn);
    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Max44009Utils_configInterrupt(Max44009Utils_InfoPtr_T max44009Info, Max44009Utils_ThresholdIntrConfigPtr_T interruptConfig)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == max44009Info) || (NULL == interruptConfig))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    returnValue = controlIntrPin(max44009Info, DISABLE_INTERRUPT);
    if (RETCODE_OK == returnValue)
    {
        returnValue = registerCallback(max44009Info);
    }
    if (RETCODE_OK == returnValue)
    {
        returnValue = configureThresholdInterrupt(interruptConfig);
    }
    if (RETCODE_OK == returnValue)
    {
        returnValue = controlIntrPin(max44009Info, ENABLE_INTERRUPT);
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Max44009Utils_disableInterrupt(Max44009Utils_InfoPtr_T max44009Info)
{
    Retcode_T maxLibReturn = (Retcode_T) RETCODE_FAILURE;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == max44009Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    maxLibReturn = MAX44009_setIntrptReg(MAX44009_DISABLE_INTERRUPT);
    returnValue = libErrorMapping(maxLibReturn);

    if (((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT)))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Max44009Utils_uninitialize(Max44009Utils_InfoPtr_T max44009Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == max44009Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    /* disable the Sensor power and interrupt pin */
    if ((max44009Info->sensorPower.port <= PTD_MAXIMUM_PORT) && (max44009Info->sensorPower.pin <= PTD_MAXIMUM_PIN)
            && (max44009Info->interruptPin.gpio.pin <= PTD_MAXIMUM_PIN))

    {
        PTD_pinOutClear(max44009Info->sensorPower.port, max44009Info->sensorPower.pin);
        PTD_intClear(max44009Info->interruptPin.gpio.pin);
        PTD_intDisable(max44009Info->interruptPin.gpio.pin);
        initializationStatus = false;
        returnValue = RETCODE_OK;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}
