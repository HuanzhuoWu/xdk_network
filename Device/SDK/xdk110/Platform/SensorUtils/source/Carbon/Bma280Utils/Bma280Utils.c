/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* system header files */

/* own header files */
#include "BCDS_Bma280Utils.h"
#include "SensorUtilsAxisRemap.h"
#include "SensorUtilsMapping.h"

/* additional interface header files */
#include <FreeRTOS.h>
#include <task.h>
#include "PTD_portDriver_ph.h"
#include <bma2x2.h>
#include "BCDS_Assert.h"

#define DISABLE_INTERRUPT    UINT8_C(0X00) /**< macro used to disable the interrupt */
#define ENABLE_INTERRUPT     UINT8_C(1)  /**< macro used to enable the interrupt */
#define DELAY_10MS           UINT32_C(10) /**< macro used to represent 10ms delay  */
#define DELAY_3MS            UINT32_C(3) /**< macro used to represent 3ms delay  */
#define BMA2X2_SUCCESS       UINT8_C(0) /**< macro used to represent success of library  */
#define PACKAGE_ID_DEFAULT   UINT8_C(0) /**< macro used to represent the return code package Id */
#define SET_POWER_CONTROL_FLAG   UINT8_C(1) /**< Macro to set the power control flag */

/**< macro to set maximum allowed slope duration */
#define BMA280_MAX_ALLOWED_SLOPE_DURATION UINT8_C(3)

/**< variable to check initialization status */
static bool initializationStatus = false;

/** structure to hold the initializations of BMA2X2 */
static struct bma2x2_t bma280Initialization;

/**
 * @brief       This function maps error codes returned from BMA280 library to retcode values
 *
 * @param [in]   BMA2x2_RETURN_FUNCTION_TYPE Return value from BMA2X2 library
 *
 * @retval      RETCODE_OK              BMA2X2 sensor API call success
 * @retval      RETCODE_FAILURE         BMA2X2 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   BMA2X2 sensor API call failed because of invalid input parameter
 */
static Retcode_T libErrorMapping(BMA2x2_RETURN_FUNCTION_TYPE bmaLibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (BMA2X2_SUCCESS == bmaLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_SUCCESS;
    }
    else if (E_BMA2x2_NULL_PTR == bmaLibReturn)
    {
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    }
    else
    {
        returnValue = (Retcode_T) RETCODE_FAILURE;
    }

    return (returnValue);
}

/**
 * @brief Function responsible to configure interrupt for new sensor data.
 *
 * @param [in]   interruptChannel Channel to which interrupt needs to be configured
 *
 * @retval      RETCODE_OK                BMA280 sensor configure slope interrupt success.
 * @retval      RETCODE_FAILURE           BMA280 sensor configure slope interrupt failed.
 * @retval      RETCODE_INVALID_PARAM     BMA280 sensor configure slope interrupt failed because of invalid input parameter.
 */
static Retcode_T configureNewDataInterrupt(Bma280Utils_IntrChannel_T interruptChannel)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    BMA2x2_RETURN_FUNCTION_TYPE bmaLibReturn = E_OUT_OF_RANGE;

    bmaLibReturn = bma2x2_set_new_data((int) interruptChannel, ENABLE_INTERRUPT);

    if (BMA2X2_SUCCESS == bmaLibReturn)
    {
        bmaLibReturn = bma2x2_set_intr_enable(BMA2x2_DATA_ENABLE, INTR_ENABLE);
    }

    returnValue = libErrorMapping(bmaLibReturn);

    if (returnValue != RETCODE_OK)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, returnValue));
    }
    else
    {
        return RETCODE_OK;
    }
}

/**
 * @brief Function responsible to configure interrupt conditions in the support layer.
 *
 * @param [in]   slopeIntrConfig structure representing to slope interrupt configuration
 *
 * @param [in]   interruptChannel Channel to which interrupt needs to be configured
 *
 * @retval      RETCODE_OK                BMA280 sensor configure slope interrupt success.
 * @retval      RETCODE_FAILURE           BMA280 sensor configure slope interrupt failed.
 * @retval      RETCODE_INVALID_PARAM     BMA280 sensor configure slope interrupt failed because of invalid input parameter.
 */
static Retcode_T configureSlopeInterrupt(Bma280Utils_SlopeIntrConfigPtr_T slopeIntrConfig, Bma280Utils_IntrChannel_T interruptChannel)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    BMA2x2_RETURN_FUNCTION_TYPE bmaLibReturn = E_OUT_OF_RANGE;

    if (NULL == slopeIntrConfig)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if ((slopeIntrConfig->slopeEnableX > ENABLE_INTERRUPT)
            || (slopeIntrConfig->slopeEnableY > ENABLE_INTERRUPT)
            || (slopeIntrConfig->slopeEnableZ > ENABLE_INTERRUPT)
            || (slopeIntrConfig->slopeDuration > BMA280_MAX_ALLOWED_SLOPE_DURATION))
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM));
    }


    /* It is recommended in the data sheet that while changing configuration parameter, we should
     * disable the corresponding Interrupt and wait for 10ms before enabling it back.
     */
    bmaLibReturn = bma2x2_set_intr_slope((int) interruptChannel, DISABLE_INTERRUPT);

    if (BMA2X2_SUCCESS == bmaLibReturn)
    {
        bmaLibReturn = bma2x2_set_intr_enable(BMA2x2_SLOPE_X_INTR, slopeIntrConfig->slopeEnableX);
        bmaLibReturn += bma2x2_set_intr_enable(BMA2x2_SLOPE_Y_INTR, slopeIntrConfig->slopeEnableY);
        bmaLibReturn += bma2x2_set_intr_enable(BMA2x2_SLOPE_Z_INTR, slopeIntrConfig->slopeEnableZ);
        bmaLibReturn += bma2x2_set_durn(BMA2x2_SLOPE_DURN, slopeIntrConfig->slopeDuration);
        bmaLibReturn += bma2x2_set_thres(BMA2x2_SLOPE_THRES, slopeIntrConfig->slopeThreshold);

        if (BMA2X2_SUCCESS == bmaLibReturn)
        {
            /* It is recommended in the data sheet that we need 10ms delay between changing the configuration and enabling interrupt */
            static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
            vTaskDelay((portTickType) DELAY_10MS / portTICK_RATE_MS);

            /* enabling the interrupt after modifying the interrupt configuration */
            bmaLibReturn += bma2x2_set_intr_slope((int) interruptChannel, ENABLE_INTERRUPT);
        }
    }


    returnValue = libErrorMapping(bmaLibReturn);

    return (RETCODE(RETCODE_SEVERITY_ERROR, returnValue));
}

/**
 * @brief        Function to control the interrupt Pin.
 *
 * @param [in]   bma280Info :structure holding the bma280 specific configurations
 *
 * @param [in]   interruptChannel Channel to which interrupt needs to be configured
 *
 * @param [in]   enable   indicates whether to disable or disable the interrupt pin
 *
 * @retval      RETCODE_OK              Returned when successfully changed gpio pin state.
 * @retval      RETCODE_FAILURE         Returned when gpio state is not modified.
 * @retval      RETCODE_INVALID_PARAM   Returned when there is an invalid input argument.
 */
static Retcode_T controlIntrPin(Bma280Utils_InfoPtr_T bma280Info,
        Bma280Utils_IntrChannel_T interruptChannel, uint8_t enable)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    switch (interruptChannel)
    {
    case BMA280UTILS_INTERRUPT_CHANNEL1:
        {
        if (ENABLE_INTERRUPT == enable)
        {
            PTD_intEnable(bma280Info->interruptPin1.gpio.pin);
            returnValue = RETCODE_OK;
        }
        else if (DISABLE_INTERRUPT == enable)
        {
            PTD_intClear(bma280Info->interruptPin1.gpio.pin);
            PTD_intDisable(bma280Info->interruptPin1.gpio.pin);
            returnValue = RETCODE_OK;
        }
        break;
    }

    case BMA280UTILS_INTERRUPT_CHANNEL2:
        {
        if (ENABLE_INTERRUPT == enable)
        {
            PTD_intEnable(bma280Info->interruptPin2.gpio.pin);
            returnValue = RETCODE_OK;
        }
        else if (DISABLE_INTERRUPT == enable)
        {
            PTD_intClear(bma280Info->interruptPin2.gpio.pin);
            PTD_intDisable(bma280Info->interruptPin2.gpio.pin);
            returnValue = RETCODE_OK;
        }
        break;
    }

    default:
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/**
 * @brief        Function to register callback for interrupt.
 *
 * @param [in]   bma280Info :structure holding the bma280 specific configurations
 *
 * @param [in]   interruptChannel Channel to which interrupt needs to be configured
 *
 * @retval      RETCODE_OK              Returned when successfully registered callback function for the given channel.
 * @retval      RETCODE_FAILURE         Returned when there is a problem in registering callback function.
 * @retval      RETCODE_INVALID_PARAM   Returned when there is an invalid input argument.
 */
static Retcode_T registerCallback(Bma280Utils_InfoPtr_T bma280Info,
        Bma280Utils_IntrChannel_T interruptChannel)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    int32_t ptdReturnValue = INT32_C(-1);

    switch (interruptChannel)
    {
    case BMA280UTILS_INTERRUPT_CHANNEL1:
        {
        if (ENABLE_INTERRUPT == bma280Info->interruptPin1.enable)
        {
            if (bma280Info->interruptPin1.callBack != NULL)
            {
                ptdReturnValue = PTD_interruptSwitchCallback(bma280Info->interruptPin1.gpio.pin, bma280Info->interruptPin1.callBack);
            }
            else
            {
                return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
            }
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
        }
        break;
    }

    case BMA280UTILS_INTERRUPT_CHANNEL2:
        {
        if (ENABLE_INTERRUPT == bma280Info->interruptPin2.enable)
        {
            if (bma280Info->interruptPin2.callBack != NULL)
            {
                ptdReturnValue = PTD_interruptSwitchCallback(bma280Info->interruptPin2.gpio.pin, bma280Info->interruptPin2.callBack);
            }
            else
            {
                return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
            }
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
        }
        break;
    }
    default:
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    /** Value returned by PTD (GPIO) driver is being overwritten here.
     * This has to be taken care when PTD or GPIO driver is implemented to return Retcode_T.
     * Otherwise, this function will not populate the failures of PTD (GPIO) driver */
    if (INT32_C(0) == ptdReturnValue)
        returnValue = RETCODE_OK;

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}
/* Public Functions */

/* API documentation is in the interface header file */
Retcode_T Bma280Utils_initialize(Bma280Utils_InfoPtr_T bma280Info)
{
    BMA2x2_RETURN_FUNCTION_TYPE bmaLibReturn = E_OUT_OF_RANGE;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t isFailed = UINT8_C(0);

    if (NULL == bma280Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (true == initializationStatus)
    {
        return (RETCODE_OK);
    }

    if(SET_POWER_CONTROL_FLAG == bma280Info->sensorPowerInfo.PowerControlFlag)
    {
    /* enable the power pin of the BMA280 sensor */
    if ((bma280Info->sensorPowerInfo.SensorPower.port <= PTD_MAXIMUM_PORT) && (bma280Info->sensorPowerInfo.SensorPower.pin <= PTD_MAXIMUM_PIN))
    {
        PTD_pinOutSet(bma280Info->sensorPowerInfo.SensorPower.port, bma280Info->sensorPowerInfo.SensorPower.pin);
        /* Start up time delay required for BMA280 sensor*/
        static_assert((portTICK_RATE_MS != 0),"Tick rate MS is zero");
        vTaskDelay((portTickType) DELAY_3MS / portTICK_RATE_MS);
    }
    else
    {
        isFailed++;
    	}
    }

    /* configure the interrupt for interrupt channel 1 of BMA280 sensor */
    if (ENABLE_INTERRUPT == bma280Info->interruptPin1.enable)
    {
        if ((bma280Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bma280Info->interruptPin1.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bma280Info->interruptPin1.gpio.port, bma280Info->interruptPin1.gpio.pin,
                    bma280Info->interruptPin1.risingEdge,
                    bma280Info->interruptPin1.fallingEdge, GPIO_INTERRUPT_DISABLE_FLAG,
                    bma280Info->interruptPin1.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    /* configure the interrupt for interrupt channel 2 of BMA280 sensor */
    if (ENABLE_INTERRUPT == bma280Info->interruptPin2.enable)
    {
        if ((bma280Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN)
                && (bma280Info->interruptPin2.gpio.port <= PTD_MAXIMUM_PORT))
        {
            PTD_intConfig(bma280Info->interruptPin2.gpio.port, bma280Info->interruptPin2.gpio.pin,
                    bma280Info->interruptPin2.risingEdge,
                    bma280Info->interruptPin2.fallingEdge, GPIO_INTERRUPT_DISABLE_FLAG,
                    bma280Info->interruptPin2.callBack);
        }
        else
        {
            isFailed++;
        }
    }

    if (BMA280UTILS_I2C_BUS == bma280Info->interface.bus)
    {
        if (I2CCHANNEL_0 == bma280Info->interface.type.i2cBus.I2C_Channel)
        {
            bma280Initialization.bus_read = BMA280UTILS_I2CBUSREAD_CHANNEL0;
            bma280Initialization.bus_write = BMA280UTILS_I2CBUSWRITE_CHANNEL0;
        }
        else if (I2CCHANNEL_1 == bma280Info->interface.type.i2cBus.I2C_Channel)
        {
            bma280Initialization.bus_read = BMA280UTILS_I2CBUSREAD_CHANNEL1;
            bma280Initialization.bus_write = BMA280UTILS_I2CBUSWRITE_CHANNEL1;
        }
        else
        {
            isFailed++;
        }
        bma280Initialization.delay_msec = BMA280UTILS_I2C_DELAY;
        bma280Initialization.dev_addr = bma280Info->interface.type.i2cBus.I2C_Address;
    }
    else
    {
        isFailed++;
    }

    if (UINT8_C(0) != isFailed)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM));
    }

    bmaLibReturn = bma2x2_init(&bma280Initialization);

    if (BMA2X2_SUCCESS == bmaLibReturn)
    {

        bmaLibReturn = bma2x2_set_bw(BMA2x2_BW_500HZ);
    }

    returnValue = libErrorMapping(bmaLibReturn);
    if (RETCODE_OK == returnValue)
    {
        initializationStatus = true;
    }
    else
    {
        initializationStatus = false;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bma280Utils_remapAxis(Bma280Utils_InfoPtr_T bma280Info, AxisRemap_Data_T *accelData)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;
    AxisRemap_Data_T actualData;

    if ((NULL == bma280Info) || (NULL == accelData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    /** copying the actual sensor data into a local variable */
    actualData.x = accelData->x;
    actualData.y = accelData->y;
    actualData.z = accelData->z;

    returnValue = AxisRemap_remapSensorData(actualData, bma280Info->remap, accelData);

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bma280Utils_configInterrupt(Bma280Utils_InfoPtr_T bma280Info, Bma280Utils_ConfigInterruptPtr_T interruptConfig)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == bma280Info) || (NULL == interruptConfig))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    if (interruptConfig->interruptChannel > BMA280UTILS_INTERRUPT_CHANNEL2)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM));
    }

    switch (interruptConfig->interruptType)
    {
    case BMA280UTILS_SLOPE_INTERRUPT:
        returnValue = controlIntrPin(bma280Info, interruptConfig->interruptChannel, DISABLE_INTERRUPT);
        if (RETCODE_OK == returnValue)
        {
            returnValue = registerCallback(bma280Info, interruptConfig->interruptChannel);
        }
        if (RETCODE_OK == returnValue)
        {
            returnValue = configureSlopeInterrupt((Bma280Utils_SlopeIntrConfigPtr_T) interruptConfig->configInterrupt, interruptConfig->interruptChannel);
        }
        if (RETCODE_OK == returnValue)
        {
            returnValue = controlIntrPin(bma280Info, interruptConfig->interruptChannel, ENABLE_INTERRUPT);
        }
        break;
    case BMA280UTILS_NEW_DATA_INTERRUPT:
        returnValue = controlIntrPin(bma280Info, interruptConfig->interruptChannel, DISABLE_INTERRUPT);
        if (RETCODE_OK == returnValue)
        {
            returnValue = registerCallback(bma280Info, interruptConfig->interruptChannel);
        }
        if (RETCODE_OK == returnValue)
        {
            returnValue = configureNewDataInterrupt(interruptConfig->interruptChannel);
        }
        if (RETCODE_OK == returnValue)
        {
            returnValue = controlIntrPin(bma280Info, interruptConfig->interruptChannel, ENABLE_INTERRUPT);
        }
        break;
    default:
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bma280Utils_disableInterrupt(Bma280Utils_InfoPtr_T bma280Info, Bma280Utils_IntrType_T interruptType,
        Bma280Utils_IntrChannel_T interruptChannel)
{
    BMA2x2_RETURN_FUNCTION_TYPE bmaLibReturn = E_OUT_OF_RANGE;
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if ((NULL == bma280Info)
            || (interruptChannel >= BMA280UTILS_INTERRUPT_CHANNEL_INVALID)
            || (interruptType >= BMA280UTILS_INVALID_INTERRUPT))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    switch (interruptType)
    {
    case BMA280UTILS_SLOPE_INTERRUPT:
        {
        bmaLibReturn = bma2x2_set_intr_slope((int) interruptChannel, DISABLE_INTERRUPT);
        returnValue = libErrorMapping(bmaLibReturn);
        break;
    }
    default:
        returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        break;
    }

    if (((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT)))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}

/* API documentation is in the interface header file */
Retcode_T Bma280Utils_uninitialize(Bma280Utils_InfoPtr_T bma280Info)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (NULL == bma280Info)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if (false == initializationStatus)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_UNINITIALIZED));
    }

    if(SET_POWER_CONTROL_FLAG == bma280Info->sensorPowerInfo.PowerControlFlag)
    {
    	if ((bma280Info->sensorPowerInfo.SensorPower.port <= PTD_MAXIMUM_PORT) && (bma280Info->sensorPowerInfo.SensorPower.pin <= PTD_MAXIMUM_PIN))
    	{
            PTD_pinOutClear(bma280Info->sensorPowerInfo.SensorPower.port, bma280Info->sensorPowerInfo.SensorPower.pin);
            returnValue = RETCODE_OK;
    	}
        else
        {
            returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
        }
    }
    else
    {
    	returnValue = RETCODE_OK;
    }

    if ((ENABLE_INTERRUPT == bma280Info->interruptPin1.enable) && (RETCODE_OK == returnValue))
    {
    	/* disable the Sensor power and interrupt pin */
    	if(bma280Info->interruptPin1.gpio.pin <= PTD_MAXIMUM_PIN)
    	{
    		PTD_intClear(bma280Info->interruptPin1.gpio.pin);
    		PTD_intDisable(bma280Info->interruptPin1.gpio.pin);
    		initializationStatus = false;
    		returnValue = RETCODE_OK;
    	}
    	else
    	{
    		returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    	}
    }

    if ((ENABLE_INTERRUPT == bma280Info->interruptPin2.enable) && (RETCODE_OK == returnValue))
    {
    	/* disable the Sensor power and interrupt pin */
    	if(bma280Info->interruptPin2.gpio.pin <= PTD_MAXIMUM_PIN)
    	{
    		PTD_intClear(bma280Info->interruptPin2.gpio.pin);
    		PTD_intDisable(bma280Info->interruptPin2.gpio.pin);
    		initializationStatus = false;
    		returnValue = RETCODE_OK;
    	}
    	else
    	{
    		returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    	}
    }

    if ((RETCODE_OK != returnValue) && (Retcode_getPackage(returnValue) == PACKAGE_ID_DEFAULT))
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) returnValue);
    }

    return (returnValue);
}
