/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SENSORUTILS_MAPPING_H_
#define SENSORUTILS_MAPPING_H_

/* public interface declaration */

/* public type and macro definitions */

/* Macros for Mapping the Write, Read and Delay Functions of the BMA sensor */
#define BMA280UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define BMA280UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define BMA280UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define BMA280UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define BMA280UTILS_I2C_DELAY             (void*)I2C_delayFunction

/* Macros for Mapping the Write, Read and Delay Functions of the BMG sensor */

#define BMG160UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define BMG160UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define BMG160UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define BMG160UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define BMG160UTILS_I2C_DELAY             (void*)I2C_delayFunction


/* Macros for Mapping the Write, Read and Delay Functions of the BMI sensor */
#define BMI160UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define BMI160UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define BMI160UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define BMI160UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define BMI160UTILS_I2C_DELAY             (void*)I2C_delayFunction

/* Macros for Mapping the Write, Read and Delay Functions of the BMM sensor */
#define BMM150UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define BMM150UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define BMM150UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define BMM150UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define BMM150UTILS_I2C_DELAY             (void*)I2C_delayFunction

/* Macros for Mapping the Write, Read and Delay Functions of the BME sensor */
#define BME280UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define BME280UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define BME280UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define BME280UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define BME280UTILS_I2C_DELAY             (void*)I2C_delayFunction

/* Macros for Mapping the Write, Read and Delay Functions of the MAX sensor */
#define MAX44009UTILS_I2CBUSWRITE_CHANNEL0  I2C_busWriteChannel0
#define MAX44009UTILS_I2CBUSREAD_CHANNEL0   I2C_busReadChannel0
#define MAX44009UTILS_I2CBUSWRITE_CHANNEL1  I2C_busWriteChannel1
#define MAX44009UTILS_I2CBUSREAD_CHANNEL1   I2C_busReadChannel1
#define MAX44009UTILS_I2C_DELAY             (void*)I2C_delayFunction

/* public function prototype declarations */



#endif /* SENSORUTILS_MAPPING_H_ */

