#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# The library name is derived from this package name
BCDS_PACKAGE_NAME = SensorUtils
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 7
BCDS_PFM ?= Carbon

BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_DEVICE_ID ?= EFM32GG230F1024

# Internal Path Settings
BCDS_SENSORUTILS_SOURCE_DIR := source
BCDS_SENSORUTILS_INCLUDE_DIR := include
BCDS_SENSORUTILS_CONFIG_DIR := config
BCDS_SENSORUTILS_PROTECTED_DIR := source/protected
BCDS_SENSORUTILS_OBJECT_DIR := object
BCDS_SENSORUTILS_LINT_DIR := lint

#Path of other libraries and platform
BCDS_PLATFORM_DIR ?= $(BCDS_PACKAGE_HOME)/../../Platform
BCDS_LIBRARIES_DIR ?= $(BCDS_PACKAGE_HOME)/../../Libraries
BCDS_TOOLS_DIR ?= $(BCDS_PACKAGE_HOME)/../../Tools

ifeq ($(BCDS_TARGET_PLATFORM),ti_cc26xx)
#Path info for taking the configuration files
BCDS_TI_COMPILER_PATH ?= C:/PROGRA~2/IARSYS~1/EMBEDD~1.2/arm/inc/c
BCDS_HYDROGEN_PATH ?= $(BCDS_PLATFORM_DIR)/Hydrogen
BCDS_BST_LIB_DIR = $(BCDS_LIBRARIES_DIR)/BSTLib

#External Path information
TIBLE_STACK_VERSION ?= ble_cc26xx_2_01_01_44627
TIRTOS_STACK_VERSION?= tirtos_simplelink_2_13_00_06
TIXDC_TOOLS_VERSION?=xdctools_3_31_01_33_core
TIRTOS_BIOS_VERSION?=bios_6_42_00_08
TIRTOS_CC26XXWARE_VERSION?=cc26xxware_2_21_01_15600

BCDS_TI_RTOS_PATH ?= $(BCDS_LIBRARIES_DIR)/Ti_simplelink_tirtos/tirtos_simplelink/$(TIRTOS_STACK_VERSION)
BCDS_TI_XDC_PATH ?= $(BCDS_TOOLS_DIR)/ti_xdctools/$(TIXDC_TOOLS_VERSION)
BCDS_TI_BLE_PATH ?= $(BCDS_LIBRARIES_DIR)/Ti_simplelink_ble_cc26xx/ble_cc26xx/$(TIBLE_STACK_VERSION)
BCDS_TI_RTOS_BIOS_PATH ?= $(BCDS_TI_RTOS_PATH)/products/$(TIRTOS_BIOS_VERSION)
BCDS_TI_RTOS_CC26XXWARE_PATH ?= $(BCDS_TI_RTOS_PATH)/products/$(TIRTOS_CC26XXWARE_VERSION)

#Setting to verify the IAR Compiler version in the PC(required for ti_cc26xx platfrom)
IAR_VERSION = $(shell iccarm)
IAR_VERSION_STRING = $(word 5, $(IAR_VERSION))
#TI Recommanded IAR version
IAR_VERSION_REQUIRED ?= V7.40.2.8542/W32
endif

ifneq ($(BCDS_TARGET_PLATFORM),ti_cc26xx)
BCDS_EMLIBS_PATH ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FREE_RTOS_PATH ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS
BCDS_PERIPHERALS_PATH ?= $(BCDS_PLATFORM_DIR)/Peripherals
endif

#Tool Chain Definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOLS_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt
LINT_EXE = $(BCDS_TOOLS_DIR)/PCLint/exe/lint-nt.launch

ifneq ($(BCDS_TARGET_PLATFORM),ti_cc26xx)
#External Library Package Dependencies
BCDS_BST_LIB_DIR = $(BCDS_LIBRARIES_DIR)/BSTLib
BCDS_EMLIBS_PATH = $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FREE_RTOS_PATH = $(BCDS_LIBRARIES_DIR)/FreeRTOS
endif

#External Platform Package Dependencies
BCDS_PHERIPHERAL_PATH = $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_UTILS_PATH =  $(BCDS_PLATFORM_DIR)/Utils
BCDS_SENSORS_PATH =  $(BCDS_PLATFORM_DIR)/Sensors
BCDS_BASICS_PATH =  $(BCDS_PLATFORM_DIR)/Basics
BCDS_SENSORDRIVER_PATH =  $(BCDS_PLATFORM_DIR)/SensorDrivers


#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)
BCDS_SENSORUTILS_HARDWARE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)/$(BCDS_HW_VERSION)
BCDS_SENSORUTILS_DEBUG_PATH ?= $(BCDS_PACKAGE_HOME)/debug
BCDS_SENSORUTILS_RELEASE_PATH ?= $(BCDS_PACKAGE_HOME)/release

BCDS_SENSORUTILS_DEBUG_OBJECT_PATH = $(BCDS_SENSORUTILS_DEBUG_PATH)/$(BCDS_SENSORUTILS_OBJECT_DIR)
BCDS_SENSORUTILS_RELEASE_OBJECT_PATH = $(BCDS_SENSORUTILS_RELEASE_PATH)/$(BCDS_SENSORUTILS_OBJECT_DIR)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Lint Path
BCDS_SENSORUTILS_DEBUG_LINT_PATH = $(BCDS_SENSORUTILS_DEBUG_PATH)/$(BCDS_SENSORUTILS_LINT_DIR)
	
# Archive File Settings
BCDS_SENSORUTILS_LIB_BASE_NAME = lib$(BCDS_PACKAGE_NAME)
BCDS_SENSORUTILS_DEBUG_LIB_NAME = $(BCDS_SENSORUTILS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_SENSORUTILS_RELEASE_LIB_NAME = $(BCDS_SENSORUTILS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a

BCDS_SENSORUTILS_DEBUG_LIB   = $(BCDS_SENSORUTILS_DEBUG_PATH)/$(BCDS_SENSORUTILS_DEBUG_LIB_NAME)
BCDS_SENSORUTILS_RELEASE_LIB = $(BCDS_SENSORUTILS_RELEASE_PATH)/$(BCDS_SENSORUTILS_RELEASE_LIB_NAME)

# Define the path for the include directories.
ifeq ($(BCDS_TARGET_PLATFORM),ti_cc26xx) 
BCDS_PACKAGE_INCLUDES = \
	-I$(BCDS_TI_COMPILER_PATH)\
	-I$(BCDS_TI_RTOS_PATH)/packages/ti/drivers \
	-I$(BCDS_TI_XDC_PATH)/packages \
	-I$(BCDS_TI_RTOS_CC26XXWARE_PATH) \
	-I$(BCDS_TI_RTOS_CC26XXWARE_PATH)/driverlib \
	-I$(BCDS_TI_RTOS_PATH)/packages	\
	-I$(BCDS_TI_RTOS_BIOS_PATH)/packages \
	-I$(BCDS_TI_BLE_PATH)/Components/ble/include \
	-I$(BCDS_TI_BLE_PATH)/Components/osal/include \
	-I$(BCDS_TI_XDC_PATH) \
	-I$(BCDS_TI_BLE_PATH)/Components/hal/target/CC2650TIRTOS \
	-I$(BCDS_TI_BLE_PATH)/Components/hal/include \
	-I$(BCDS_TI_RTOS_BIOS_PATH)/packages/ti/sysbios \
	-I$(BCDS_TI_RTOS_BIOS_PATH)/packages/ti/sysbios/knl \
	-I$(BCDS_TI_BLE_PATH)/Projects/ble/common/cc26xx \
	-I$(BCDS_HYDROGEN_PATH)/include
endif

# Define the path for the include directories.
ifneq ($(BCDS_TARGET_PLATFORM),ti_cc26xx)
BCDS_PACKAGE_INCLUDES = \
	-I$(BCDS_INCLUDE_DIR) \
	-I$(BCDS_PACKAGE_CONFIG_PATH) \
	-I$(BCDS_FREE_RTOS_PATH)/source/include \
	-I$(BCDS_FREE_RTOS_PATH)/source/portable/ARM_CM3 \
	-I$(BCDS_PERIPHERALS_PATH)/include \
	-I$(BCDS_BASICS_PATH)/include \
	
endif	

# Define the path for the include directories.
BCDS_EXTERNAL_INCLUDES = \
	-I$(BCDS_EMLIBS_PATH)/CMSIS/Include \
	-I$(BCDS_EMLIBS_PATH)/emlib/inc \
	-I$(BCDS_EMLIBS_PATH)/usb/inc \
	-I$(BCDS_EMLIBS_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
	-I$(BCDS_FREE_RTOS_PATH)/FreeRTOS/source/include \
	-I$(BCDS_FREE_RTOS_PATH)/FreeRTOS/source/portable/ARM_CM3 \
	-I$(BCDS_PHERIPHERAL_PATH)/include \
	-I$(BCDS_UTILS_PATH)/include \
	-I$(BCDS_BASICS_PATH)/include \
	-I$(BCDS_SENSORDRIVER_PATH)/include \
	-I$(BCDS_SENSORUTILS_INCLUDE_DIR) \
	-I$(BCDS_PACKAGE_CONFIG_PATH) \
	-I$(BCDS_SENSORUTILS_HARDWARE_CONFIG_PATH) \
	-I$(BCDS_SENSORUTILS_PROTECTED_DIR) \
	-I$(BCDS_CONFIG_PATH) \
	-I$(BCDS_CONFIG_PATH)/$(BCDS_PFM) \
	-I$(BCDS_CONFIG_PATH)/Peripherals
	
BCDS_SENSORUTILS_INCLUDES += \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMA2x2_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BME280_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMG160_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMI160_driver \
	-I$(BCDS_BST_LIB_DIR)/BST_Github/BMM050_driver
	

#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -I,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_EXTERNAL_INCLUDES_LINT += -i$(BCDS_LINT_CONFIG_PATH)
BCDS_SENSORUTILS_INCLUDES_LINT := $(subst -I,-i,$(BCDS_SENSORUTILS_INCLUDES))
BCDS_PACKAGE_INCLUDES_LINT := $(subst -I,-i,$(BCDS_PACKAGE_INCLUDES))
#-------------------------------------------------------------------------------------------------
