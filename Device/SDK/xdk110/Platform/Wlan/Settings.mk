#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# Package Information
# Package name is used for generating the library. lib<PackageName>_<BCDS_DEVICE_TYPE>.a
BCDS_PACKAGE_NAME = Wlan
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 10

BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_DEVICE_ID ?= EFM32GG395F1024
BCDS_HW_VERSION ?= HW_XDK_v1

# Internal Path Settings
BCDS_WIFI_SOURCE_DIR := source
BCDS_WIFI_INCLUDE_DIR := include
BCDS_WIFI_PROTECTED_DIR := source/protected
BCDS_WIFI_OBJECT_DIR := object
BCDS_WIFI_LINT_DIR := lint

#Path of Platform, Libraries and Tools
BCDS_TOOLS_DIR ?= $(BCDS_PACKAGE_HOME)/../../Tools
BCDS_LIBRARIES_DIR = $(BCDS_PACKAGE_HOME)/../../Libraries
BCDS_PLATFORM_DIR = $(BCDS_PACKAGE_HOME)/../../Platform

#Tool chain definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOLS_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt
LINT_EXE = $(BCDS_TOOLS_DIR)/PCLint/exe/lint-nt.launch

#External Library Package Dependencies
BCDS_EMLIBS_PATH ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FREE_RTOS_PATH ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS
BCDS_SIMPLELINK_PATH = $(BCDS_LIBRARIES_DIR)/WiFi/TI/simplelink

#External Platform Package Dependencies
BCDS_POWER_PATH ?= $(BCDS_PLATFORM_DIR)/Power
BCDS_PHERIPHERAL_PATH ?= $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_BASICS_PATH ?= $(BCDS_PLATFORM_DIR)/Basics
WIFI_SFP_DIR = $(BCDS_PLATFORM_DIR)/Wlan
WIFI_SPF_HOME = $(WIFI_SFP_DIR)
BCDS_PFM_UTILS = $(BCDS_PLATFORM_DIR)/Utils/include

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)
BCDS_WIFI_HARDWARE_CONFIG_PATH := $(BCDS_PACKAGE_CONFIG_PATH)/$(BCDS_HW_VERSION)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Source Paths
BCDS_WIFI_SOURCE_PATH = $(BCDS_WIFI_SOURCE_DIR)

#Path of the generated static library and its object files
BCDS_WIFI_DEBUG_PATH ?= $(BCDS_PACKAGE_HOME)/debug
BCDS_WIFI_RELEASE_PATH ?= $(BCDS_PACKAGE_HOME)/release

BCDS_WIFI_DEBUG_OBJECT_PATH = $(BCDS_WIFI_DEBUG_PATH)/$(BCDS_WIFI_OBJECT_DIR)
BCDS_WIFI_RELEASE_OBJECT_PATH = $(BCDS_WIFI_RELEASE_PATH)/$(BCDS_WIFI_OBJECT_DIR)

#Lint Path
BCDS_WIFI_DEBUG_LINT_PATH = $(BCDS_WIFI_DEBUG_PATH)/$(BCDS_WIFI_LINT_DIR)

# Archive File Settings
BCDS_WIFI_LIB_BASE_NAME = lib$(BCDS_PACKAGE_NAME)
BCDS_WIFI_DEBUG_LIB_NAME = $(BCDS_WIFI_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_WIFI_RELEASE_LIB_NAME = $(BCDS_WIFI_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a

BCDS_WIFI_DEBUG_LIB   = $(BCDS_WIFI_DEBUG_PATH)/$(BCDS_WIFI_DEBUG_LIB_NAME)
BCDS_WIFI_RELEASE_LIB = $(BCDS_WIFI_RELEASE_PATH)/$(BCDS_WIFI_RELEASE_LIB_NAME)

# Define the path for the include directories.
BCDS_EXTERNAL_INCLUDES = \
	-I$(BCDS_SIMPLELINK_PATH)/include \
	-I$(BCDS_POWER_PATH)/include \
	-I$(BCDS_POWER_PATH)/config \
	-I$(BCDS_BASICS_PATH)/include \
	-I$(BCDS_BASICS_PATH)/config \
	-I$(WIFI_SPF_HOME)/include \
	-I$(BCDS_PFM_UTILS) \
	-I$(BCDS_CONFIG_PATH) \
	-I$(BCDS_FREE_RTOS_PATH)/FreeRTOS/source/include \
	-I$(BCDS_FREE_RTOS_PATH)/FreeRTOS/source/portable/ARM_CM3 \
	-I$(BCDS_PHERIPHERAL_PATH)/include \
	-I$(BCDS_EMLIBS_PATH)/emlib/inc \
    -I$(BCDS_EMLIBS_PATH)/usb/inc \
    -I$(BCDS_EMLIBS_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
    -I$(BCDS_EMLIBS_PATH)/CMSIS/Include \
	-I$(WIFI_SPF_HOME)/config

BCDS_WIFI_INCLUDES = \
	-I$(BCDS_WIFI_INCLUDE_DIR) \
	-I$(BCDS_PACKAGE_CONFIG_PATH) \
	-I$(BCDS_WIFI_PROTECTED_DIR) \
	-I$(BCDS_WIFI_SOURCE_DIR)/NetworkConfig \
	-I$(BCDS_WIFI_SOURCE_DIR)/WlanConnect \
	-I$(BCDS_WIFI_SOURCE_DIR)/WlanDriver \
	-I$(BCDS_WIFI_HARDWARE_CONFIG_PATH)
	
#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_EXCLUDES_LINT := $(foreach DIR, $(subst -I,,$(BCDS_EXTERNAL_INCLUDES)), +libdir\($(DIR)\))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -I,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_WIFI_INCLUDES_LINT := $(subst -I,-i,$(BCDS_WIFI_INCLUDES))
BCDS_LINT_CONFIG = -i$(BCDS_LINT_CONFIG_PATH)
#-------------------------------------------------------------------------------------------------
	            		