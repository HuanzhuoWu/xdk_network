/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_WlanDriver.h"
#include "WlanDriver.h"

/* additional interface header files */
#include "simplelink.h"
#include "PTD_portDriver_ih.h"
#include "PTD_portDriver_ph.h"
#include "em_device.h"
#include "em_cmu.h"
#include "SPI_ih.h"
#include "WIFI_ph.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "timers.h"
#include "queue.h"
#include "task.h"
#include "BCDS_PowerMgt.h"
#include "BCDS_Assert.h"

/* constant definitions ***************************************************** */

/* local variables ***********************************************************/
//lint -e956 Suppressing Non const, non volatile static or external variable lint warning
/** Queue handle for the Queue created for Spawn task */
static xQueueHandle SpawnQueueHandle = NULL;
/** Task handle for Spawn task */
static xTaskHandle SpawnTaskHandle = NULL;

/** Event Handler declaration required by wifi stack */
static WLANDRIVER_EVENT_HANDLER WlanIrqEventHandler = NULL;
/* global variables ********************************************************* */

/** Ring Buffer for low level SPI interface */
uint8_t TransmitRingBuffer[WID_RING_BUFFER_SIZE] = { UINT8_C(0) };
uint8_t ReceiveRingBuffer[WID_RING_BUFFER_SIZE] = { UINT8_C(0) };

/** SPI handle for hardware SPI instance created for wifi */
SPI_device_t SpiHandle;
//lint +e956 enabling Non const, non volatile static or external variable lint warning
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

static inline TickType_t OsTime2Tick(WlanDriver_OsTime TimeOut)
{
    if (0 == TimeOut)
    {
        return (TickType_t)0;
    }
    else if (WLANDRIVER_OS_WAIT_FOREVER == TimeOut)
    {
        return portMAX_DELAY;
    }
    TickType_t ticks = (TickType_t)(TimeOut/portTICK_RATE_MS);
    if (0 == ticks)
    {
        ++ticks;
    }
    return ticks;
}

/**
 *  @brief This function is used to create delay in milli seconds
 *  @param[in]  millisecondsDelay - Delay Value in milli seconds
 *  @warning    Do not use this API before starting the RTOS scheduler
 */
static void DelayAPI(uint32_t milliSecondsDelay)
{
    uint64_t oldTicks;
    uint64_t newTicks;
    oldTicks = PowerMgt_GetSystemTimeMs();
    newTicks = PowerMgt_GetSystemTimeMs();
    while ((newTicks - oldTicks) < milliSecondsDelay)
    {
        newTicks = PowerMgt_GetSystemTimeMs();
    }
}

/** @brief This function is to create spawn Task and queue needed by simpleLink driver
 *  @param[in] spawnTaskPriority
 *             priority of the spawn task
 *  @return
 *         OS_ERR_NO_ERROR - on success
 *         OS_ERR_NOT_ENOUGH_MEMORY - on error in creating spawn task
 */
static int32_t CreateSpawnQAndTask(uint32_t spawnTaskPriority)
{
    int8_t retVal = WLANDRIVER_NOT_ENOUGH_MEMORY;
    SpawnQueueHandle = xQueueCreate(WID_SPAWN_QUEUE_SIZE, sizeof(WlanDriver_SlSpawnMsg_T));

    if (SpawnQueueHandle != NULL)
    {
        if (xTaskCreate(WlanDriver_SimpleLinkSpawnTask, (const char * const) "SLSPAWN", WID_SPAWN_TASK_STACK_SIZE,NULL, spawnTaskPriority, &SpawnTaskHandle) != pdPASS)
        {
            /* The task was not created as there was insufficient heap memory remaining.*/
            retVal = WLANDRIVER_NOT_ENOUGH_MEMORY;
        }
        else
        {
            retVal = WLANDRIVER_SUCCESS;
        }
    }
    else
    {
        retVal = WLANDRIVER_NOT_ENOUGH_MEMORY;
    }
    return (retVal);
}

/**
 *  @brief
 *      This is the callback function that got triggered by WiFi device interrupt pin.
 *  @usage
 *      This function will used by the PTD module interrupt config function PTD_intConfig()
 */
static void WifiInterruptCallback(void)
{
    if (WlanIrqEventHandler != NULL)
    {
        WlanIrqEventHandler(0);
    }
}

/* global functions ********************************************************* */
/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
Retcode_T WlanDriver_Init(void)
{
    int32_t returnValue = WLANDRIVER_FAILURE;
    uint32_t spawnTaskPriority = WID_SPAWN_TASK_PRIORITY;
    /* Enable clock to USART0/SPI module */
    CMU_ClockEnable((CMU_Clock_TypeDef) WIFI_SPI_CLOCK_NUMBER, true);
    /* Enable clock to GPIO  */
    CMU_ClockEnable(cmuClock_GPIO, true);
    /* Enable the 3V3 vcc power supply to WiFi Chip */
    PTD_pinOutSet((GPIO_Port_TypeDef)PTD_PORT_WIFI_SD_VDD_PORT, PTD_PIN_WIFI_SD_VDD_PORT);
    /* Enables the 2V5 vcc power supply to WiFi Chip, this is required to make wifi function properly  */
    PTD_pinOutClear((GPIO_Port_TypeDef)PTD_PORT_WIFI_VDD_2V5_ENABLE, PTD_PIN_WIFI_VDD_2V5_ENABLE);
    /* This reset sequence is recommended to power up wifi properly */
    PTD_pinOutClear((GPIO_Port_TypeDef)PTD_PORT_WIFI_RESET, PTD_PIN_WIFI_RESET);
    DelayAPI(WID_RESET_LOW_DELAY);
    PTD_pinOutSet((GPIO_Port_TypeDef)PTD_PORT_WIFI_RESET, PTD_PIN_WIFI_RESET);
    DelayAPI(WID_RESET_HIGH_DELAY);
    PTD_intConfig((GPIO_Port_TypeDef) PTD_PORT_WIFI_IRQ, PTD_PIN_WIFI_IRQ,
    WID_INTERRUPT_EDGE_RISING, WID_INTERRUPT_EDGE_FALLING, WID_INTERRUPT_ENABLE,
            (PTD_intrCallback) WifiInterruptCallback);
    /* Create spawn task and Queue  */
    returnValue = CreateSpawnQAndTask(spawnTaskPriority);
    if (WLANDRIVER_SUCCESS != returnValue)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_RTOS_QUEUE_ERROR));
    }
    else
    {
        return (RETCODE_OK);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
int WlanDriver_RegInterruptHdlr(WLANDRIVER_EVENT_HANDLER interruptHandle, void* pValue)
{
    BCDS_UNUSED(pValue);
    if (interruptHandle == NULL)
    {
        return ((int) WLANDRIVER_FAILURE);
    }
    WlanIrqEventHandler = interruptHandle;
    return ((int) WLANDRIVER_SUCCESS);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_DeviceDisable(void)
{
    PTD_pinOutClear(PTD_PORT_WIFI_ENABLE_DISABLE, PTD_PIN_WIFI_ENABLE_DISABLE);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_DeviceEnable(void)
{
    PTD_pinOutSet(PTD_PORT_WIFI_ENABLE_DISABLE, PTD_PIN_WIFI_ENABLE_DISABLE);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_InterruptEnable(void)
{
    PTD_intEnable(PTD_PIN_WIFI_IRQ);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_InterruptDisable(void)
{

    PTD_intDisable(PTD_PIN_WIFI_IRQ);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_SlHttpServerCb(SlHttpServerEvent_t *pHttpEvent,
        SlHttpServerResponse_t *pHttpResponse)
{
    BCDS_UNUSED(pHttpEvent);
    BCDS_UNUSED(pHttpResponse);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
int WlanDriver_SpiClose(Fd_t fd)
{
    BCDS_UNUSED(fd);
    SPI_return_t spiRetVal = SPI_FAILURE;
    spiRetVal = SPI_deinit(&SpiHandle);
    if (spiRetVal != SPI_SUCCESS)
    {
        return (WLANDRIVER_SPI_ERROR);
    }
    /* Disable WLAN Interrupt*/
    WlanDriver_InterruptDisable();
    return (WLANDRIVER_SUCCESS);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
Fd_t WlanDriver_SpiOpen(char *ifName, unsigned long flags)
{
    BCDS_UNUSED(ifName);
    BCDS_UNUSED(flags);
    SPI_initParams_t initParams;
    SPI_return_t spiRetVal = SPI_FAILURE;
    initParams.portNumber = (SPI_portNumber_t) WIFI_SPI_PORT_NUMBER;
    initParams.clockMode = (SPI_clockMode_t) WIFI_SPI_CLOCK_MODE;
    initParams.routeLocation = (SPI_routeLocation_t) WIFI_SPI_ROUTE_LOCATION;
    initParams.txBuf_p = TransmitRingBuffer;
    initParams.rxBuf_p = ReceiveRingBuffer;
    initParams.txBufSize = WID_RING_BUFFER_SIZE;
    initParams.rxBufSize = WID_RING_BUFFER_SIZE;
    initParams.baudrate = WID_SPI_BAUDRATE;
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_MOSI));
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_MISO));
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_CS));
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_CLK));
    spiRetVal = SPI_init(&SpiHandle, &initParams);
    if (spiRetVal != SPI_SUCCESS)
    {
        return (WLANDRIVER_SPI_ERROR);
    }
    /*Enable WLAN interrupt*/
    PTD_intClear(PTD_PIN_WIFI_IRQ);
    WlanDriver_InterruptEnable();
    return (WLANDRIVER_SUCCESS);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
int WlanDriver_SpiWrite(Fd_t fd, unsigned char *pBuff, int len)
{
    BCDS_UNUSED(fd);
    int lenToReturn = len;
    SPI_return_t spiRetVal = SPI_FAILURE;
    WID_ASSERT_CS();
    spiRetVal = SPI_write(&SpiHandle, (uint8_t*) pBuff, (uint32_t) len, SPI_WRITE_ONLY);
    WID_DEASSERT_CS();
    if (spiRetVal != SPI_SUCCESS)
    {
        return (WLANDRIVER_SPI_RW_ERROR);
    }
    return (lenToReturn);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
int WlanDriver_SpiRead(Fd_t fd, unsigned char *pBuff, int len)
{
    BCDS_UNUSED(fd);
    SPI_return_t spiRetVal = SPI_FAILURE;
    WID_ASSERT_CS();
    spiRetVal = SPI_read(&SpiHandle, (uint8_t*) pBuff, (uint32_t) len);
    WID_DEASSERT_CS();
    if (spiRetVal != SPI_SUCCESS)
    {
        return (WLANDRIVER_SPI_RW_ERROR);
    }
    return (len);
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncObjectCreate(WlanDriver_OsSyncObject* pSyncObj)
{
    *pSyncObj = xSemaphoreCreateBinary();
    if ((SemaphoreHandle_t) (*pSyncObj) != NULL)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncObjectDelete(WlanDriver_OsSyncObject* pSyncObj)
{
    if ((SemaphoreHandle_t) (*pSyncObj) != NULL)
    {
        vSemaphoreDelete(*pSyncObj);
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncObjectSignal(WlanDriver_OsSyncObject* pSyncObj)
{
    if ((SemaphoreHandle_t) (*pSyncObj) != NULL)
    {
        BCDS_UNUSED(xSemaphoreGive(*pSyncObj));
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncSignalFromISR(WlanDriver_OsSyncObject* pSyncObj)
{
    if ((SemaphoreHandle_t) (*pSyncObj) != NULL)
    {
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        if (xSemaphoreGiveFromISR (*pSyncObj,&xHigherPriorityTaskWoken) == pdTRUE)
        {
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncObjectWait(WlanDriver_OsSyncObject* pSyncObj, WlanDriver_OsTime TimeOut)
{
    if (NULL == pSyncObj)
    {
        return WLANDRIVER_FAILURE;
    }
    if (xSemaphoreTake((SemaphoreHandle_t)*pSyncObj, OsTime2Tick(TimeOut)) == pdTRUE)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsSyncObjectClear(WlanDriver_OsSyncObject* pSyncObj)
{
    if (NULL == pSyncObj)
    {
        return WLANDRIVER_FAILURE;
    }
    if (WlanDriver_OsSyncObjectWait(pSyncObj, WLANDRIVER_OS_NO_WAIT) == WLANDRIVER_SUCCESS)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsLockObjectCreate(WlanDriver_OsLockObject* pLockObj)
{
    *pLockObj = xSemaphoreCreateMutex();
    if ((*pLockObj) != NULL)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsLockObjectDelete(WlanDriver_OsLockObject* pLockObj)
{
    if ((pLockObj) != NULL)
    {
        vSemaphoreDelete((SemaphoreHandle_t )*pLockObj);
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }

}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsLockObjectWait(WlanDriver_OsLockObject* pLockObj, WlanDriver_OsTime TimeOut)
{
    if (xSemaphoreTake(*pLockObj, OsTime2Tick(TimeOut)) == pdTRUE)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h */
int32_t WlanDriver_OsLockUnlock(WlanDriver_OsLockObject* pLockObj)
{
    if (NULL == pLockObj)
    {
        return (WLANDRIVER_FAILURE);
    }
    if (xSemaphoreGive(*pLockObj) == pdTRUE)
    {
        return (WLANDRIVER_SUCCESS);
    }
    else
    {
        return (WLANDRIVER_FAILURE);
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
void WlanDriver_SimpleLinkSpawnTask(void *pvParameters)
{
    BCDS_UNUSED(pvParameters);
    WlanDriver_SlSpawnMsg_T message;
    Retcode_T retVal = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_RTOS_QUEUE_ERROR);
    for (;;)
    {
        if (xQueueReceive(SpawnQueueHandle, &message, (UINT32_MAX)) != pdPASS)
        {
            Retcode_raiseError(retVal);
        }
        else
        {
            short RetunValue;
            RetunValue = message.pEntry(message.pValue);
            if (RetunValue != 0)
            {
                Retcode_raiseError(retVal);
            }
        }
    }
}

/* For details refer API documentation is in the interface header file BCDS_WlanDriver.h  */
int32_t WlanDriver_OsiSpawn(WLANDRIVER_OSI_SPAWN_ENTRY pEntry, void* pValue, unsigned long flags)
{
    WlanDriver_SlSpawnMsg_T message;
    message.pEntry = pEntry;
    message.pValue = pValue;
    if (SL_SPAWN_FLAG_FROM_SL_IRQ_HANDLER == flags)
    {
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        if (xQueueSendFromISR(SpawnQueueHandle,&message,&xHigherPriorityTaskWoken) == pdTRUE)
        {
            portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
            return (WLANDRIVER_SUCCESS);
        }
        else
        {
            return (WLANDRIVER_FAILURE);
        }
    }
    else
    {
        if (xQueueSend(SpawnQueueHandle,&message, portMAX_DELAY) == pdTRUE)
        {
            portYIELD();
            return (WLANDRIVER_SUCCESS);
        }
        else
        {
            return (WLANDRIVER_FAILURE);
        }
    }
}

void WlanDriver_DeInit(void)
{
    /* Disable clock to USART0/SPI module */
    CMU_ClockEnable((CMU_Clock_TypeDef) WIFI_SPI_CLOCK_NUMBER, false);
    /* Disable the 3V3 vcc power supply to WiFi Chip */
    PTD_pinOutClear((GPIO_Port_TypeDef)PTD_PORT_WIFI_SD_VDD_PORT, PTD_PIN_WIFI_SD_VDD_PORT);
    /* Disable the 2V5 vcc power supply to WiFi Chip, this is required to make wifi function properly  */
    PTD_pinOutSet((GPIO_Port_TypeDef)PTD_PORT_WIFI_VDD_2V5_ENABLE, PTD_PIN_WIFI_VDD_2V5_ENABLE);
    PTD_intDisable(PTD_PIN_WIFI_IRQ);
    /* Delete spawn task and Queue  */
    vTaskDelete(SpawnTaskHandle);
    vQueueDelete(SpawnQueueHandle);
}

/** ************************************************************************* */
