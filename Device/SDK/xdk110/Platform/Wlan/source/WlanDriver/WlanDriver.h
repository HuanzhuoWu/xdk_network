/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 * @file
 */

/* header definition ******************************************************** */
#ifndef WLANDRIVER_H_
#define WLANDRIVER_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define WID_SPAWN_TASK_STACK_SIZE         UINT16_C(512)         /**< Stack Size for spawn Task */
#define WID_SPAWN_TASK_PRIORITY           UINT32_C(4)           /**< Task priority for spawn Task */
#define WID_SPAWN_QUEUE_SIZE              UINT32_C(3)           /**< Queue Size for spawn Task */
#define WID_INTERRUPT_EDGE_RISING         UINT8_C(0x01)         /**< Rising edge is configured as TRUE for interrupt */
#define WID_INTERRUPT_EDGE_FALLING        UINT8_C(0x00)         /**< Falling edge is configured as FALSE for interrupt */
#define WID_INTERRUPT_ENABLE              UINT8_C(0x01)         /**< Macro to configure Interrupt as TRUE */
#define WID_INTERRUPT_DISABLE             UINT8_C(0x00)         /**< Macro to configure Interrupt as FALSE */
#define WID_RING_BUFFER_SIZE              UINT32_C(2000)        /**< Macro to represent ring buffer size */
#define WID_SPI_BAUDRATE                  UINT32_C(12000000)     /**< Macro to represent SPI baudrate value */
#define WID_RESET_LOW_DELAY               UINT32_C(3)           /**< Macro to have 3msec Delay after making WiFi Reset Pin Low recomended by datasheet */
#define WID_RESET_HIGH_DELAY              UINT32_C(1400)        /**< Macro to have 1400msec Delay after making WiFi Reset Pin High recommended in datasheet*/

/* Macro to activate CS signal for SPI interface*/
#define WID_ASSERT_CS()      PTD_pinOutClear(PTD_PORT_WIFI_CS, PTD_PIN_WIFI_CS )
/* Macro to inactivate CS signal for SPI interface*/
#define WID_DEASSERT_CS()    PTD_pinOutSet(PTD_PORT_WIFI_CS, PTD_PIN_WIFI_CS)

/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */

#endif /* WLANDRIVER_H_ */

/** ************************************************************************* */
