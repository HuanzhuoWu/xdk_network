/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_NetworkConfig.h"
#include "NetworkConfig.h"

/* additional interface header files */
#include "simplelink.h"
#include "BCDS_WlanConnect.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
static volatile NetworkConfig_IpCallback_T IpConfigCallback; /**< Variable for storing the DHCP IP callback*/
static volatile NetworkConfig_IpStatus_T NetworkIpStatus; /**< Flag variable for IP obtained status */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
void NetworkConfig_SlNetAppEvtHdlr(SlNetAppEvent_t *pNetAppEvent)
{
    SlNetAppEvent_t *pNetApp = (SlNetAppEvent_t *) pNetAppEvent;
    switch (pNetApp->Event)
    {
    case SL_NETAPP_IPV4_IPACQUIRED_EVENT:
        /* IP v4 was obtained*/
        NetworkIpStatus = NETWORKCONFIG_IPV4_ACQUIRED;
        /* Call function for non-blocking function with callback*/
        if (NULL != IpConfigCallback)
        {
            IpConfigCallback(NETWORKCONFIG_IPV4_ACQUIRED);
        }
        break;
    case SL_NETAPP_IPV6_IPACQUIRED_EVENT:
        /* IP v6 was obtained*/
        /*IPV6 not supported */
        break;
    case SL_NETAPP_IP_LEASED_EVENT:
        /* Ip was leased*/
        /* relevant in AP or P2P GO mode */
        break;
    case SL_NETAPP_IP_RELEASED_EVENT:
        /* Ip was released. Set status for blocking function */
        NetworkIpStatus = NETWORKCONFIG_IP_NOT_ACQUIRED;
        /* Call function for non-blocking function with callback*/
        if (NULL != IpConfigCallback)
        {
            IpConfigCallback(NETWORKCONFIG_IP_NOT_ACQUIRED);
        }
        break;
    default:
        break;
    }
}

/* global functions ********************************************************* */
NetworkConfig_IpStatus_T NetworkConfig_GetIpStatus(void)
{
    return (NetworkIpStatus);
}

void NetworkConfig_SetIpStatus(NetworkConfig_IpStatus_T ipStatus)
{
    NetworkIpStatus = ipStatus;
}

Retcode_T NetworkConfig_GetIpSettings(NetworkConfig_IpSettings_T* myIpSettings)
{
    if (NULL == myIpSettings)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t ) RETCODE_NULL_POINTER));
    }
    if (WLANCONNECT_NOT_INITIALZED == WlanConnect_GetInitStatus())
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_UNINITIALIZED));
    }
    Retcode_T returnValue = RETCODE_OK;
    int32_t slStatus = NETWORKCONFIG_SUCCESS;
    SlNetCfgIpV4Args_t ipV4;
    uint32_t len = UINT8_C(0);

    memset(&ipV4, 0, sizeof(ipV4));
    len = sizeof(SlNetCfgIpV4Args_t);

    /* Read the IP parameter */
    slStatus = sl_NetCfgGet((uint8_t) SL_IPV4_STA_P2P_CL_GET_INFO,
            (uint8_t*) &myIpSettings->isDHCP, (uint8_t*) &len,
            (uint8_t*) &ipV4);

    /* determine return value*/
    if (NETWORKCONFIG_FAILURE == slStatus)
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_FAILURE);
    }
    else
    {
        myIpSettings->ipV4 = ipV4.ipV4;
        myIpSettings->ipV4DnsServer = ipV4.ipV4DnsServer;
        myIpSettings->ipV4Gateway = ipV4.ipV4Gateway;
        myIpSettings->ipV4Mask = ipV4.ipV4Mask;
    }

    /* return API status*/
    return (returnValue);
}

Retcode_T NetworkConfig_SetIpStatic(NetworkConfig_IpSettings_T myIpSettings)
{
    if (WLANCONNECT_NOT_INITIALZED == WlanConnect_GetInitStatus())
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_UNINITIALIZED));
    }
    /* local variables */
    Retcode_T returnValue = RETCODE_OK;
    int32_t slStatus = NETWORKCONFIG_SUCCESS;
    SlNetCfgIpV4Args_t ipV4;
    uint32_t len = UINT8_C(0);

    /* set parameters */
    len = sizeof(SlNetCfgIpV4Args_t);

    ipV4.ipV4 = (unsigned long) (myIpSettings.ipV4);
    ipV4.ipV4DnsServer = (unsigned long) (myIpSettings.ipV4DnsServer);
    ipV4.ipV4Gateway = (unsigned long) (myIpSettings.ipV4Gateway);
    ipV4.ipV4Mask = (unsigned long) (myIpSettings.ipV4Mask);

    /* set static ip*/
    slStatus = sl_NetCfgSet((uint8_t) SL_IPV4_STA_P2P_CL_STATIC_ENABLE, NETWORKCONFIG_ONE, (uint8_t) len, (uint8_t*) &ipV4);
    /* restart the device*/
    if (NETWORKCONFIG_FAILURE != slStatus)
    {
        slStatus = sl_Stop((_u16) NETWORKCONFIG_ZERO);
    }
    if (NETWORKCONFIG_SUCCESS == slStatus)
    {
        slStatus = sl_Start(NULL, NULL, NULL);

        if (ROLE_STA != (SlWlanMode_e) slStatus)
        {
            returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_FAILURE);
        }
        else
        {
            returnValue = RETCODE_OK;
        }
    }
    else
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_FAILURE);

    }
    /* return API status*/
    return (returnValue);
}

Retcode_T NetworkConfig_SetIpDhcp(NetworkConfig_IpCallback_T myIpCallback)
{
    if (WLANCONNECT_NOT_INITIALZED == WlanConnect_GetInitStatus())
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_UNINITIALIZED));
    }
    Retcode_T returnValue = RETCODE_OK;
    int32_t slStatus = NETWORKCONFIG_SUCCESS;
    uint8_t val = UINT8_C(0);

    val = NETWORKCONFIG_ONE;
    IpConfigCallback = myIpCallback;

    slStatus = sl_NetCfgSet((uint8_t) SL_IPV4_STA_P2P_CL_DHCP_ENABLE, NETWORKCONFIG_ONE, NETWORKCONFIG_ONE, &val);

    /* restart the device*/
    if (NETWORKCONFIG_FAILURE != slStatus)
    {
        slStatus = sl_Stop(NETWORKCONFIG_TIMEOUT);
    }
    if (NETWORKCONFIG_SUCCESS == slStatus)
    {
        slStatus = sl_Start(NULL, NULL, NULL);
    }
    if (ROLE_STA != (SlWlanMode_e) slStatus)
    {
        returnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t ) RETCODE_FAILURE);
    }

    /* return API status*/
    return (returnValue);
}

uint32_t NetworkConfig_Ipv4Value(uint8_t add3, uint8_t add2, uint8_t add1, uint8_t add0)
{
    uint32_t returnValue;

    returnValue = SL_IPV4_VAL(add3, add2, add1, add0);

    return (returnValue);
}

uint8_t NetworkConfig_Ipv4Byte(uint32_t ipValue, uint8_t index)
{
    uint8_t returnValue;

    returnValue = SL_IPV4_BYTE(ipValue, index);

    return (returnValue);
}
/** ************************************************************************* */
