/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 * @file
 */
/* header definition ******************************************************** */
#ifndef WLANCONNECT_H_
#define WLANCONNECT_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define WLANCONNECT_SCAN_TABLE_SIZE         UINT8_C(20)
#define WLANCONNECT_SCAN_ENABLE          	UINT8_C(1)
#define WLANCONNECT_SCAN_DISABLE         	UINT8_C(0)
#define WLANCONNECT_FAILURE                 INT16_C(-1) /**< Macro for defining Failure*/
#define WLANCONNECT_SUCCESS                 UINT16_C(0) /**< Macro for defining SUCCESS*/
#define WLANCONNECT_ZERO                    UINT8_C(0) /**< Macro for defining 0*/
#define WLANCONNECT_ONE                     UINT8_C(1) /**< Macro for defining 1*/
#define WLANCONNECT_TIMEOUT_VAL             UINT8_C(0xFF) /**< Macro for timeout value*/
#define WLANCONNECT_ALL_PROFILES            UINT8_C(0xFF) /**< Macro for deleting all profiles */
#define WLANCONNECT_MAX_BUFFER              UINT8_C(10) /**< Macro for Simple Link status buffer */
#define WLANCONNECT_NO_OF_ENTRIES           UINT8_C(5) /**< Macro for scan function number of entries*/
#define WLANCONNECT_ONE_SEC_DELAY           (portTickType)(1000) /**< Macro for 1 second delay*/
#define WLANCONNECT_MAX_TRIES               UINT8_C(15) /**< Macro for Simple Link status buffer */
#define WLANCONNECT_TIMER_TICKS             UINT8_C(1000) /**< Macro for Simple Link status buffer */
#define TIMER_AUTORELOAD_ON                 UINT8_C(1)  /**< Auto reload of timer is disabled*/
#define TIMERBLOCKTIME                      UINT32_MAX     /**< Macro used to define blocktime of a timer*/

/* local function prototype declarations */

/* local module global variable declarations */

/* public global variable declarations */

/* local inline function definitions */

#endif /* WLANCONNECT_H_ */

/** ************************************************************************* */
