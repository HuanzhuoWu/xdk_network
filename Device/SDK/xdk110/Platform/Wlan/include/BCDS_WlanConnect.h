/*----------------------------------------------------------------------------*/
/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*
 * Copyright (C) Bosch Connected Devices and Solutions GmbH.
 * All Rights Reserved. Confidential.
 *
 * Distribution only to people who need to know this information in
 * order to do their job.(Need-to-know principle).
 * Distribution to persons outside the company, only if these persons
 * signed a non-disclosure agreement.
 * Electronic transmission, e.g. via electronic mail, must be made in
 * encrypted form.
 */
/*----------------------------------------------------------------------------*/

/**
 *  @file        
 *
 *  Interface header for the WLI module.
 *  The interface header exports the following features:
 *  - APIs for the WLI module :
 *                                            -# WlanConnect_Open()
 *                                            -# WlanConnect_WEP_Open()
 *                                            -# WlanConnect_WPA()
 *                                            -# WlanConnect_WPS_PBC()
 *                                            -# WlanConnect_WPS_PIN()
 *                                            -# WlanConnect_DeleteAllProfiles()
 *                                            -# WlanConnect_ScanNetworks()
 *                                            -# WlanConnect_GetStatus()
 *                                            -# WlanConnect_GetCurrentNwStatus()
 *                                            -# WlanConnect_Disconnect()
 *                                            -# WlanConnect_GetInitStatus()
 *
 *  - APIs to Initialize/Disable WLI module   :
 *                                            -# WlanConnect_Init()
 *                                            -# WlanConnect_DeInit()
 *
 */
/**
 * @defgroup            wlan_interface Wlan Connection Interfaces
 * @ingroup             xdk_networking
 *
 * @{
 * @brief               Module Describes the usage Of Wlan Interface
 */

/* header definition ******************************************************** */
#ifndef BCDS_WLANCONNECT_H_
#define BCDS_WLANCONNECT_H_

/* public interface declaration ********************************************* */
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"

/* public type and macro definitions **************************************** */
#define WLANCONNECT_MAX_SCAN_INFO_BUF              UINT8_C(20) /**< The max buffer for storing the scan data */
#define WLANCONNECT_MAX_SSID_LENGTH                UINT8_C(32) /**< The max buffer for storing the SSID length */
#define WLANCONNECT_MAX_MAC_ADDR_LEN               UINT8_C(6) /**< The max buffer for storing the Mac address length */
#define WLANCONNECT_ENROLLEE_PIN                   "35072317" /**< The Enrollee PIN that should be used for WPS PIN Connect function */
#define WLANCONNECT_DUMMY_SSID                     "XDK110" /**< macro to represent the ssid of the device in WPS_PBC and WPS_PIN Security mode*/
#define WLANCONNECT_INITIALZED                     UINT8_C(1) /**< Macro to represent Initialized state of Wlan*/
#define WLANCONNECT_NOT_INITIALZED                 UINT8_C(0) /**< Macro to represent un-Initialized state of Wlan*/

/**
 * @brief Enumeration to represent the return codes of WLAN module.
 */
enum WlanConnect_Retcode_E
{
    RETCODE_NO_NW_AVAILABLE = RETCODE_FIRST_CUSTOM_CODE,
    RETCODE_ALREADY_DISCONNECTED,
    RETCODE_CONNECTION_ERROR,
    RETCODE_ERROR_WRONG_PASSWORD,
    RETCODE_ERROR_IP_NOT_ACQ,
    RETCODE_DISCONNECT_ERROR,
    RETCODE_SIMPLELINK_ERROR
};

/**  Type defining callback status.
 *   This type is used for different connection status for the WLAN Connect
 *   function callback.
 ******************************************************************************/
enum WlanConnect_Status_E
{
    WLAN_CONNECTED,
    WLAN_DISCONNECTED,
    WLAN_CONNECTION_ERROR,
    WLAN_CONNECTION_PWD_ERROR,
    WLAN_DISCONNECT_ERROR,
};
typedef enum WlanConnect_Status_E WlanConnect_Status_T;

/** Different status for the Current Network Configuration function callback */
enum WlanConnect_CurrentStatus_E
{
    CONNECTED_AND_IPV4_ACQUIRED = 0x00,
    CONNECTED_AND_IP_NOT_ACQUIRED = 0x01,
    DISCONNECTED_AND_IPV4_ACQUIRED = 0x02,
    DISCONNECTED_IP_NOT_ACQUIRED = 0x03,
};
typedef enum WlanConnect_CurrentStatus_E WlanConnect_CurrentNwStatus_T;

/******************************************************************************/
/**  Type defining a pointer to char.
 *   This type is used in order to pass the network SSID
 ******************************************************************************/
typedef signed char (*WlanConnect_SSID_T);

/******************************************************************************/
/**  Type defining a pointer to char
 *   This type is used in order to pass the network Pass Phrase
 ******************************************************************************/
typedef signed char (*WlanConnect_PassPhrase_T);

/******************************************************************************/
/**  Type defining scan interval.
 *   This type is used for WlanConnect_scanNetworks function
 ******************************************************************************/
typedef unsigned long (WlanConnect_ScanInterval_T);

/******************************************************************************/
/** Type defining scan information.
 *  Structure to store single network data after scanning
 ******************************************************************************/
struct WlanConnect_ScanInfo_S
{
    uint8_t Ssid[WLANCONNECT_MAX_SSID_LENGTH]; /**< Name of the access point*/
    uint8_t SsidLength; /**< Name of the access point*/
    uint8_t SecurityType; /**< Security type of the access point */
    uint8_t Bssid[WLANCONNECT_MAX_MAC_ADDR_LEN]; /**< MAC address of the access point*/
    int8_t Rssi; /**< RSSI value of the access point*/
};
typedef struct WlanConnect_ScanInfo_S WlanConnect_ScanInfo_T;

/******************************************************************************/
/** Type defining scan list
 *  Structure to store the scan list of all networks, which has the scan
 *  info data structure  as well as other scan information
 ******************************************************************************/
struct WlanConnect_ScanList_S
{
    int8_t NumOfScanEntries; /**< Number of access points after scan */
    uint32_t TimeStamp; /**< The last time stamp (seconds from last reset) when the scan was done*/
    WlanConnect_ScanInfo_T ScanData[WLANCONNECT_MAX_SCAN_INFO_BUF]; /**< The structure containing the scan data */
};
typedef struct WlanConnect_ScanList_S WlanConnect_ScanList_T;

/******************************************************************************/
/** Type defining the prototype which connection call back must conform to.
 *  This call back is called to notify the user when connection is established
 *  Returns the connection status as a parameter
 ******************************************************************************/
typedef void (*WlanConnect_Callback_T)(WlanConnect_Status_T connectStatus);

/******************************************************************************/
/** Type defining the prototype which disconnection call back must conform to.
 *  This call back is called to notify the user when connection is terminated
 *  following the call to WlanConnect_disconnect().
 *  Returns the connection status as a parameter
 ******************************************************************************/
typedef void (*WlanConnect_DisconnectCallback_T)(
        WlanConnect_Status_T discconnectStatus);

/* public function prototype declarations */

/******************************************************************************/
/**
 * @brief        WLAN driver initialization function.\n
 *               This function initializes WLAN driver context and
 *               Hardware interfaces; Also initializes the WLAN Stack.
 *               This API should be called first before calling any other API.
 *
 *  @note        By default device is configured to STA mode if user want to change the mode,
 *               they need to use the Simple link API directly
 *
 * @param  [in]  none
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      - Successfully WLAN driver initialized.
 * @retval       RETCODE_SIMPLELINK_ERROR - Not initialized WLAN driver.
 *
 * @par          Example 1
 *               Initialize the WLAN Stack and the WLAN Driver
 * @code
 *               // Initialize WLAN driver and WLAN stack
 *               WlanConnect_Init();
 * @endcode
 *
 * @par          Example 2
 *               Restart Device
 * @code
 *               // Local API return status
 *               Retcode_T retStatus[2];
 *
 *               retStatus[0] = WlanConnect_DeInit();
 *               retStatus[1] = WlanConnect_Init();
 *
 *               // Check if device restart has finished with error
 *               if ((RETCODE_OK != retStatus[0]) || (RETCODE_OK != retStatus[1]))
 *               {
 *                  printf("Restart device failed");
 *               }
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_Init(void);

/******************************************************************************/
/**
 * @brief        WLAN driver de-initialization function.\n
 *               Performs memory deallocation, closes WLAN driver context and
 *               shuts down the hardware interfaces.

 * @param  [in]  none
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      - Successfully WLAN driver de-initialized.
 * @retval       RETCODE_SIMPLELINK_ERROR - De-initializing WLAN driver failed
 *
 * @par          Example 1
 *               Deinitialize the WLAN Stack
 * @code
 *               // Deinitialize WLAN stack
 *               WlanConnect_DeInit();
 * @endcode
 *
 * @par          Example 2
 *               Restart Device
 * @code
 *               // Local API return status
 *               Retcode_T retStatus[2];
 *
 *               retStatus[0] = WlanConnect_DeInit();
 *               retStatus[1] = WlanConnect_Init();
 *
 *               // Check if device restart has finished with error
 *               if ((RETCODE_OK != retStatus[0]) || (RETCODE_OK != retStatus[1]))
 *               {
 *                  printf("Restart device failed");
 *               }
 * @endcode
 ******************************************************************************/
Retcode_T WlanConnect_DeInit(void);

/******************************************************************************/
/**
 * @brief        WLI Connect to Open Network interface.\n
 *               This function connects to an open network.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address of
 *               the user function
 *
 * @param  [in]  connectSSID
 *               Pointer to char containing the network SSID to connect to.
 *
 * @param  [in]  connectCallback
 *               Pointer to user function callback.\n
 *               Returns the WLI connection status as a parameter.
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      - Successfully connected to the AP.In case of API
 *                              success return value after calling a blocking
 *                              function, the WLI_SUCCESS implicitly confirms
 *                              to the user that the WLI connection is done
 * @retval       RETCODE_SIMPLELINK_ERROR - Failure returned by simplelink API.
 * @retval       WLAN_CONNECTION_ERROR - Failure because of incorrect SSID.
 * @retval       RETCODE_ERROR_IP_NOT_ACQ     - Connected but ip not acquired
 *
 * @par          Example 1
 *               Connect to an open WLAN network with callback (non-blocking)
 * @code
 *               // Callback function
 *               void myConnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLI_CONNECTED == returnStatus)
 *                   {
 *                       // Network connection successfully
 *                       printf("Callback Function : Connect OK!\n\r");
 *                       // User may add logic once the connection is done
 *                       // E.g launch another OS Timer
 *                   }
 *                   else
 *                   {
 *                         // user may retry to connect
 *                   }
 *               }
 *
 *               // local variables
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *               WlanConnect_Callback_T connectCallback;
 *
 *               // Open wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"Name-Of-Open-Network";
 *
 *               // Set callback functions
 *               connectCallback = myConnectCallbackFunc;
 *
 *               // Connect to network --> non blocking function with callback
 *               retStatus = WlanConnect_Open(connectSSID, connectCallback);
 *
 * @endcode
 *
 * @par          Example 2
 *               Connect to an open WLAN network in blocking mode
 * @code
 *               // local variables
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *
 *               // Open wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"Name-Of-Open-Network";
 *
 *               // Connect to network --> blocking function without callback
 *               // Task will be blocked here until connection is done and IP acquired
 *               retStatus = WlanConnect_Open(connectSSID, NULL);
 *
 * @endcode
 ******************************************************************************/
Retcode_T WlanConnect_Open(WlanConnect_SSID_T connectSSID,
        WlanConnect_Callback_T connectCallback);

/******************************************************************************/
/**
 * @brief        WLI Connect to WEP Open Network interface.\n
 *               This function connects to an WEP Open network.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address
 *               of the user function
 *
 * @param  [in]  connectionSSID
 *               Pointer to char containing the network SSID to connect to.
 * @param  [in]  connectionPass
 *               Pointer to char containing the network Pass Phrase.
 * @param  [in]  passPhraseLength
 *               Length of the Pass Phrase
 * @param  [in]  connectionCallback
 *               Pointer to char containing the connection callback.
 *
 * @retval       Retcode_T         The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      -  API return code for success. In case of API
 *                              success return value after calling a blocking
 *                              function, the WLI_SUCCESS implicitly confirms
 *                              to the user that the WLI connection is done
 * @retval       RETCODE_SIMPLELINK_ERROR - Failure returned by simplelink API.
 * @retval       RETCODE_ERROR_SSID_NOT_FOUND - Failure because of incorrect SSID.
 * @retval       RETCODE_ERROR_WRONG_PASSWORD -  Failure because of incorrect Pass phrase.
 * @retval       RETCODE_ERROR_IP_NOT_ACQ     - Connected but ip not acquired
 * @retval       RETCODE_INVALID_PARAM        - passPhraseLength is zero
 *
 * @par          Example 1
 *               Connect to an WEP Open network with callback (non-blocking)
 * @code
 *               // Callback function
 *               void myConnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLI_CONNECTED == returnStatus)
 *                   {
 *                       // Network connection successfully
 *                       printf("Callback Function : Connect OK!\n\r");
 *                       // User may add logic once the connection is done
 *                       // E.g launch another OS Timer
 *                   }
 *                    else
 *                   {
 *                         // user may retry to connect
 *                   }
 *               }
 *
 *               // local variables
 *               //For example if AP is configured to WEP with 40bit Hex password of: "FFFFFFFFFF"
 *               char passPhrase[5] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *               WlanConnect_Callback_T connectCallback;
 *               WlanConnect_PassPhrase_T connectPassPhrase;
 *               uint8_t passPhraseLength;[Don't use strlen() in order not to bump on zero/NULL in the passPhrase]
 *
 *               // WEP encrypted wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"WEP-OPEN-NETWORK-NAME";
 *               connectPassPhrase = (WlanConnect_PassPhrase_T)passPhrase;
 *               passPhraseLength = UINT8_C(5);
 *               // Set callback functions
 *               connectCallback = myConnectCallbackFunc;
 *
 *               // Connect to network --> non blocking function with callback
 *               retStatus = WlanConnect_WEP_Open(connectSSID, connectPassPhrase,passPhraseLength, connectCallback);
 * @endcode
 *
 * @par          Example 2
 *               Connect to an WEP Open network in blocking mode
 * @code
 *               // local variables
 *               //For example if AP is configured to WEP with 40bit Hex password of: "FFFFFFFFFF"
 *               char passPhrase[5] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *               WlanConnect_PassPhrase_T connectPassPhrase;
 *               uint8_t passPhraseLength;[Don't use strlen() in order not to bump on zero/NULL in the passPhrase]
 *
 *               // WEP encrypted wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"WEP-OPEN-NETWORK-NAME";
 *               connectPassPhrase = (WlanConnect_PassPhrase_T)passPhrase;
 *               passPhraseLength = UINT8_C(5);
 *
 *               // Connect to network --> blocking function without callback
 *               retStatus = WlanConnect_WEP_Open(connectSSID, connectPassPhrase,passPhraseLength, NULL);
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_WEP_Open(WlanConnect_SSID_T connectSSID,
        WlanConnect_PassPhrase_T connectPass,uint8_t passPhraseLength,
        WlanConnect_Callback_T connectCallback);

/******************************************************************************/
/**
 * @brief        WLI Connect to WPA Network interface.\n
 *               This function connects to an WPA2-PSK or WPA-PSK network.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address of
 *               the user function
 *
 * @param  [in]  connectionSSID
 *               Pointer to char containing the network SSID to connect to.
 * @param  [in]  connectionPass
 *               Pointer to char containing the network Pass Phrase.
 * @param  [in]  connectionCallback
 *               Pointer to char containing the connection callback.
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      - API return code for success. In case of API
 *                              success return value after calling a blocking
 *                              function, the WLI_SUCCESS implicitly confirms
 *                              to the user that the WLI connection is done
 * @retval       RETCODE_SIMPLELINK_ERROR - Failure returned by simplelink API.
 * @retval       RETCODE_ERROR_SSID_NOT_FOUND - Failure because of incorrect SSID.
 * @retval       RETCODE_ERROR_WRONG_PASSWORD -  Failure because of incorrect Pass phrase.
 * @retval       RETCODE_ERROR_IP_NOT_ACQ     - Connected but ip not acquired
 *
 * @par          Example 1
 *               Connect to WPA/WPA2 network with callback (non-blocking)
 * @code
 *               // Callback function
 *               void myConnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLI_CONNECTED == returnStatus)
 *                   {
 *                       // Network connection successfully
 *                       printf("Callback Function : Connect OK!\n\r");
 *                       // User may add logic once the connection is done
 *                       // E.g launch another OS Timer
 *                   }
 *                   else
 *                   {
 *                         // user may retry to connect
 *                   }
 *               }
 *
 *               // local variables
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *               WlanConnect_Callback_T connectCallback;
 *               WlanConnect_PassPhrase_T connectPassPhrase;
 *
 *               // WPA/WPA2 encrypted wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"WPA-NETWORK-NAME";
 *               connectPassPhrase = (WlanConnect_PassPhrase_T)"WPA-PASSWORD";
 *
 *               // Set callback functions
 *               connectCallback = myConnectCallbackFunc;
 *
 *               // Connect to network --> non blocking function with callback
 *               retStatus = WlanConnect_WPA(connectSSID, connectPassPhrase, connectCallback);
 * @endcode
 *
 * @par          Example 2
 *               Connect to WPA/WPA2 network in blocking mode
 * @code
 *               // local variables
 *               WlanConnect_SSID_T connectSSID;
 *               Retcode_T retStatus;
 *               WlanConnect_PassPhrase_T connectPassPhrase;
 *
 *               // WPA/WPA2 encrypted wireless network settings
 *               connectSSID = (WlanConnect_SSID_T)"WPA-NETWORK-NAME";
 *               connectPassPhrase = (WlanConnect_PassPhrase_T)"WPA-PASSWORD";
 *
 *               // Connect to network --> blocking function without callback
 *               retStatus = WlanConnect_WPA(connectSSID, connectPassPhrase, NULL);
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_WPA(WlanConnect_SSID_T connectSSID,
        WlanConnect_PassPhrase_T connectPass,
        WlanConnect_Callback_T connectCallback);

/******************************************************************************/
/**
 * @brief        WLI Connect using WPS PBC interface.\n
 *               This function connects to a network by using the  Wi-Fi
 *               Protected Setup. After calling the function the user must
 *               push the connect button of the Router for around 5 seconds.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address of
 *               the user function

 * @param  [in]  connectionCallback
 *               Pointer to char containing the connection callback.
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      -  API return code for success. In case of API
 *                              success return value after calling a blocking
 *                              function, the WLI_SUCCESS implicitly confirms
 *                              to the user that the WLI connection is done.
 * @retval       RETCODE_SIMPLELINK_ERROR - Failure returned by simplelink API.
 * @retval       RETCODE_CONNECTION_ERROR -  conection Failure .
 * @retval       RETCODE_ERROR_IP_NOT_ACQ     - Connected but ip not acquired
 *
 * @par          Example 1
 *               Connect to a wireless network using WPS (WiFi Protected Setup)
 *               PBC (Push Button Connect) with callback (non-blocking)
 * @code
 *               // Callback function
 *               void myConnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLI_CONNECTED == returnStatus)
 *                   {
 *                       // Network connection successfully
 *                       printf("Callback Function : Connect OK!\n\r");
 *                       // User may add logic once the connection is done
 *                       // E.g launch another OS Timer
 *                   }
 *                    else
 *                   {
 *                         // user may retry to connect
 *                   }
 *               }
 *
 *               // local variables
 *               Retcode_T retStatus[3];
 *               WlanConnect_Callback_T connectCallback;
 *
 *               // Set callback functions
 *               connectCallback = myConnectCallbackFunc;
 *
 *              // Because the board connects to network very fast, the user
 *               // must disconnect and delete all stored profiles before
 *               // retrying to connect with WPS PBC
 *               retStatus[0] = WlanConnect_Disconnect(NULL);
 *
 *               // Delete all stored profiles in order to connect WPS by pushing
 *               // the button all the time. Remove this line if you want to
 *               // keep profile after running once the function.
 *               retStatus[1] = WlanConnect_DeleteAllProfiles();
 *
 *               // Connect to network --> non blocking function with callback
 *               retStatus[2] = WlanConnect_WPS_PBC(connectCallback);
 * @endcode
 *
 * @par          Example 2
 *               Connect to a wireless network using WPS (WiFi Protected Setup)
 *               PBC (Push Button Connect) in blocking mode (without callback)
 * @code
 *               // local variables
 *               Retcode_T retStatus[3];
 *
 *               // Because the board connects to network very fast, the user
 *               // must disconnect and delete all stored profiles before
 *               // retrying to connect with WPS PBC
 *               retStatus[0] = WlanConnect_Disconnect(NULL);
 *
 *               // Delete all stored profiles in order to connect WPS by pushing
 *               // the button all the time. Remove this line if you want to
 *               // keep profile after running once the function.
 *               retStatus[1] = WlanConnect_DeleteAllProfiles();
 *
 *               // Connect to network --> blocking function without callback
 *               retStatus[2] = WlanConnect_WPS_PBC(NULL);
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_WPS_PBC(WlanConnect_Callback_T connectCallback);

/******************************************************************************/
/**
 * @brief        WLI Connect using WPS PIN interface.\n
 *               This function connects to a network by using the  Wi-Fi
 *               Protected Setup. The function is configured with the following
 *               enrollee PIN code: 35072317. This PIN must be entered in the
 *               router interface after calling the function.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address of
 *               the user function
 *
 * @param  [in]  connectionCallback
 *               Pointer to char containing the connection callback.
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK      - API return code for success. In case of API
 *                              success return value after calling a blocking
 *                              function, the WLI_SUCCESS implicitly confirms
 *                              to the user that the WLI connection is done
 * @retval       RETCODE_SIMPLELINK_ERROR - Failure returned by simplelink API.
 * @retval       RETCODE_CONNECTION_ERROR - Connection Failure because of incorrect Pin.
 * @retval       RETCODE_ERROR_IP_NOT_ACQ     - Connected but ip not acquired
 *
 * @par          Example 1
 *               Connect to a wireless network using WPS (WiFi Protected Setup)
 *               PIN (Personal Identif. Number) with callback (non-blocking)
 * @code
 *               // Callback function
 *               void myConnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLI_CONNECTED == returnStatus)
 *                   {
 *                       // Network connection successfully
 *                       printf("Callback Function : Connect OK!\n\r");
 *                       // User may add logic once the connection is done
 *                       // E.g launch another OS Timer
 *                   }
 *                    else
 *                   {
 *                         // user may retry to connect
 *                   }
 *               }
 *
 *               // local variables
 *               Retcode_T retStatus[3];
 *               WlanConnect_Callback_T connectCallback;
 *
 *               // Set callback functions
 *               connectCallback = myConnectCallbackFunc;
 *
 *              // Because the board connects to network very fast, the user
 *               // must disconnect and delete all stored profiles before
 *               // retrying to connect with WPS PIN
 *               retStatus[0] = WlanConnect_Disconnect(NULL);
 *
 *               // Delete all stored profiles in order to connect WPS using
 *               // enrollee PIN all the time. Remove this line if you want to
 *               // keep profile after running once the function.
 *               retStatus[1] = WlanConnect_DeleteAllProfiles();
 *
 *               // Connect to network --> non blocking function with callback
 *               retStatus[2] = WlanConnect_WPS_PIN(connectCallback);
 * @endcode
 *
 * @par          Example 2
 *               Connect to a wireless network using WPS (WiFi Protected Setup)
 *               PIN (Personal Identification Number) in blocking mode
 * @code
 *               // local variables
 *               Retcode_T retStatus[3];
 *
 *               // Because the board connects to network very fast, the user
 *               // must disconnect and delete all stored profiles before
 *               // retrying to connect with WPS PIN
 *               retStatus[0] = WlanConnect_Disconnect(NULL);
 *
 *               // Delete all stored profiles in order to connect WPS using
 *               // enrollee PIN all the time. Remove this line if you want to
 *               // keep profile after running once the function.
 *               retStatus[1] = WlanConnect_DeleteAllProfiles();
 *
 *               // Connect to network --> blocking function without callback
 *               retStatus[2] = WlanConnect_WPS_PIN(NULL);
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_WPS_PIN(WlanConnect_Callback_T connectCallback);

/******************************************************************************/
/**
 * @brief        WLI Delete All Profiles interface.\n
 *               This function erases from memory all the stored profiles from
 *               WIFI Protected Setup functions.
 *
 * @param  [in]  none
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK - successfully erased all profiles.
 * @retval       RETCODE_SIMPLELINK_ERROR - error during erase
 *
 * @par          Example
 *               Delete all stored WLI profiles
 * @code
 *               Retcode_T retStatus;
 *
 *               retStatus = WlanConnect_DeleteAllProfiles();
 *
 *               if (RETCODE_OK != retStatus)
 *               {
 *                  printf("Profiles were not deleted correctly");
 *               }
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_DeleteAllProfiles(void);

/******************************************************************************/
/**
 * @brief        WLI disconnect interface.\n
 *               This function disconnects from the WLAN network.\n
 *               Function is configurable in two ways:
 *               - blocking : callback parameter is set to WLI_NULL(maximum blocking in error case will be 15seconds)
 *               - non-blocking : callback parameter is set to the address
 *               of the user function
 *
 * @param  [in]  disconnectCallback
 *               Pointer to user function callback\n
 *               Returns the WLI connection status as a parameter.
 *
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 * @retval       RETCODE_OK       successfully disconnected from Access point.\n
 *                         In case of API success return value after calling a
 *                         blocking function, the WLI_SUCCESS implicitly
 *                         confirms to the user that WLAN disconnection is done.
 * @retval       RETCODE_DISCONNECT_ERROR  Error in disconnecting
 * @par          Example 1
 *               Disconnect with callback (non-blocking)
 * @code
 *               void myDisconnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *               {
 *                   if (WLAN_DISCONNECTED == returnStatus)
 *                   {
 *                       // Network disconnection successfully from WlanConnect_Disconnect function
 *                       printf("Callback Function : Disconnect OK!\n\r");
 *                   }
 *                   else
 *                   {
 *                   //user can retry
 *                   }
 *               }
 *
 *               // local variables
 *               Retcode_T retStatus;
 *               WlanConnect_DisconnectCallback_T disconnectCallback;
 *
 *               // Set disconnect callback function
 *               disconnectCallback = myDisconnectCallbackFunc;
 *
 *               // disconnect from network with callback --> non-blocking
 *               retStatus = WlanConnect_Disconnect(disconnectCallback);
 * @endcode
 *
 * @par          Example 2
 *               Disconnect without callback
 * @code
 *               // local variables
 *               Retcode_T retStatus;
 *
 *               // disconnect from network in blocking mode
 *               retStatus = WlanConnect_Disconnect(NULL);
 *
 *               if (RETCODE_OK == retStatus)
 *               {
 *                   // Disconnect has completed successfully
 *                   printf("Disconnected successfully!\n\r");
 *               }
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_Disconnect(WlanConnect_DisconnectCallback_T disconnectCallback);

/******************************************************************************/
/**
 * @brief        Function used to scan the available networks.\n
 *               Get the available scan information.\n
 *               List of scan results will be available via the call
 *               back function.
 *
 * @param [in]   scanInterval
 *               Time interval between network scan.
 *
 * @param [out]  scanList
 *               Structure pointer hold the WLI scan information data.
 * @retval       Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *                              Below shows the error code(last 2 Bytes) meaning
 *
 * @retval       RETCODE_OK                        - scan done successful.
 * @retval       RETCODE_SIMPLELINK_ERROR          - scan was not successful.
 * @retval       RETCODE_NO_NW_AVAILABLE           - No network availble to scan.
 *
 * @par          Example
 *               Scan networks and print SSIDs
 * @code
 *               // local variables
 *               WLI_scanReturnCode_t retScanStatus;
 *               WlanConnect_ScanInterval_T scanInterval;
 *               WlanConnect_ScanList_T scanList;
 *
 *               // Set scan interval
 *               scanInterval = 5;
 *
 *               // Fill out the scan list by calling the scan network function
 *               retScanStatus = WlanConnect_ScanNetworks(scanInterval, &scanList);
 *
 *               // Set the number of entries
 *               int nbEntries = scanList.numOfScanEntries;
 *
 *               // Print all the non-empty SSIDs
 *               for (int i = 0; i < nbEntries; i++)
 *               {
 *                   if (0 != scanList.scanData[i].ssidLength)
 *                   {
 *                       printf(" - found SSID number %d is : %s\n\r", i, scanList.scanData[i].ssid);
 *
 *                       // Delay each printf with 0,5 seconds
 *                       vTaskDelay(DELAY_500_MSEC);
 *                   }
 *               }
 * @endcode
 *
 ******************************************************************************/
Retcode_T WlanConnect_ScanNetworks(WlanConnect_ScanInterval_T scanInterval,
        WlanConnect_ScanList_T* scanList);
/******************************************************************************/
/**
 * @brief         Function to get the current network status
 *                This function sets a disconnect callback which is used to
 *                notify the user that there has been a disconnection that was
 *                not triggered by the user.ed on connection status.
 *
 * @Warning       This API must be called only after initialization
 *
 * @param [ in ]  allDisconnectCallback
 *                Pointer to user function callback.
 *                Returns the WLI connection status as a parameter.
 *
 * @retval        CONNECTED_AND_IPV4_ACQUIRED    - currently WLAN connected
 *                                                 and IP acquired.
 * @retval        CONNECTED_AND_IP_NOT_ACQUIRED  - currently WLAN connected
 *                                                 and IP not acquired.
 * @retval        DISCONNECTED_AND_IPV4_ACQUIRED - currently WLAN disconnected
 *                                                 and IP acquired
 * @retval        DISCONNECTED_IP_NOT_ACQUIRED   - currently WLAN disconnected
 *                                                 and IP not acquired.
 * @par           Example
 *                Get current status of a network and set a callback for any
 *                disconnect events
 * @code
 *                void myAllDisconnectCallbackFunc(WlanConnect_ConnectionStatus_T returnStatus)
 *                {
 *                    if (WLI_DISCONNECTED == returnStatus)
 *                    {
 *                        // Network disconnection successfully from WlanConnect_Disconnect function
 *                        printf("Callback Function : A disconnect event has happened!\n\r");
 *                    }
 *                }
 *
 *                // local variables
 *                Retcode_T retStatus;
 *                WlanConnect_DisconnectCallback_T myAllDisconnectCallback;
 *
 *                // Set callback function
 *                myAllDisconnectCallback = myAllDisconnectCallbackFunc;
 *
 *                if (DISCONNECTED_IP_NOT_ACQUIRED
 *                     != WlanConnect_GetCurrentNwStatus()
 *                   && (DISCONNECTED_AND_IPV4_ACQUIRED
 *                     != WlanConnect_GetCurrentNwStatus()))
 *                {
 *                    retStatus = WlanConnect_Disconnect(myAllDisconnectCallback);
 *                    printf("Finished disconnection successfully \n\r");
 *                }
 * @endcode
 ******************************************************************************/
WlanConnect_CurrentNwStatus_T WlanConnect_GetCurrentNwStatus(void);

/******************************************************************************/
/**
 * @brief         Function to get the connection status.\n
 *                This function return the connection status.
 *
 * @param [ in ]  none
 *
 * @retval        WLI_CONNECTED                 - WLI connected obtained after
 *                                               calling connect function.
 * @retval        WLI_DISCONNECTED              - WLI disconnected obtained
 *                                               after calling disconnect
 *                                               function.
 * @par           Example
 *                Block until IP status is acquired and Wlan is connected
 * @code
 *                while ((WLI_DISCONNECTED == WlanConnect_GetStatus())
 *                 || (NETWORKCONFIG_IP_NOT_ACQUIRED == NetworkConfig_GetIpStatus()))
 *                {
 *                    // Stay here until connected and ip acquired.
 *                    // Timeout logic can be added here.
 *                }
 * @endcode
 * @Warning       This API must be called only after initialization
 *
 ******************************************************************************/
WlanConnect_Status_T WlanConnect_GetStatus(void);

/**
 * @brief         Function to get the connection status.\n
 *                This function return the connection status.
 *
 * @param [ in ]  none
 *
 * @retval        WLANCONNECT_INITIALZED       - Wlan module is initialzed.
 * @retval        WLANCONNECT_NOT_INITIALZED   - Wlan module is not initialzed.
 ******************************************************************************/
uint8_t WlanConnect_GetInitStatus(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_WLANCONNECT_H_ */
/*@}*/

/******************************************************************************/
