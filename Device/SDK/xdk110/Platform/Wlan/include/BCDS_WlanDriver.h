/*----------------------------------------------------------------------------*/
/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*
 * Copyright (C) Bosch Connected Devices and Solutions GmbH.
 * All Rights Reserved. Confidential.
 *
 * Distribution only to people who need to know this information in
 * order to do their job.(Need-to-know principle).
 * Distribution to persons outside the company, only if these persons
 * signed a non-disclosure agreement.
 * Electronic transmission, e.g. via electronic mail, must be made in
 * encrypted form.
 */
/*----------------------------------------------------------------------------*/

/**
 *  @file        
 *
 *  Interface header for the WlanDriver module.
 *
 *  The interface header exports the following features:
 *  										WlanDriver_Init(),
 *  										WlanDriver_DeviceDisable(),
 *  										WlanDriver_DeviceEnable(),
 *  										WlanDriver_RegInterruptHdlr()
 *  										WlanDriver_InterruptEnable(),
 *  										WlanDriver_InterruptDisable(),
 *  										WlanDriver_SpiClose(),
 *  										WlanDriver_SpiOpen(),
 *											WlanDriver_SpiWrite(),
 *											WlanDriver_SpiRead(),
 *											WlanDriver_OsSyncObjectCreate(),
 *											WlanDriver_OsSyncObjectDelete(),
 *											WlanDriver_OsSyncObjectSignal(),
 *											WlanDriver_OsSyncSignalFromISR(),
 *											WlanDriver_OsSyncObjectWait(),
 *											WlanDriver_OsSyncObjectClear(),
 *											WlanDriver_OsLockObjectCreate(),
 *											WlanDriver_OsLockObjectDelete(),
 *											WlanDriver_OsLockObjectWait(),
 *											WlanDriver_OsLockUnlock(),
 *											WlanDriver_OsiSpawn()
 *	@note
 *  	Data type 'Fd_t' is custom for third party wifi driver and hence for
 *  	porting we cannot change this to BCDS standard datatype
 */
/**
 * @defgroup WlanDriver_networking Wlan Driver Interfaces
 * @ingroup xdk_networking
 *
 * @{
 * @brief  Module contains the necessary interface for integrating wifi stack
 */

/* header definition ******************************************************** */
#ifndef BCDS_WLANDRIVER_H_
#define BCDS_WLANDRIVER_H_

/* public interface declaration ********************************************* */
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"

/* public type and macro definitions */
#define WLANDRIVER_OS_WAIT_FOREVER        (UINT32_MAX)  /**< Definition representing wait for ever time value for OS API TimeOut */
#define WLANDRIVER_OS_NO_WAIT              UINT32_C(0)      /**< Definition representing wait for ever time  value for OS API TimeOut */
#define WLANDRIVER_MAX_DELAY              (UINT32_MAX)      /**< Definition representing  Maximum delay*/
#define WLANDRIVER_SUCCESS                 INT32_C(0)       /**< Definition representing  Success*/
#define WLANDRIVER_FAILURE                 INT32_C(-1)      /**< Definition representing failure*/
#define WLANDRIVER_NOT_ENOUGH_MEMORY       (-1L)
#define WLANDRIVER_SPI_RW_ERROR     UINT32_C(0)
#define WLANDRIVER_SPI_ERROR     INT32_C(-1)

/** fileDescriptor for SPI port */
typedef signed int Fd_t;

/** type definition of wifi event handler*/
typedef void (*WLANDRIVER_EVENT_HANDLER)(void* pValue);
/**
 * @brief
 *      type definition for a spawn entry callback
 
 * @return 
 *       The function which gets invoked inside stack returns the type short, 
 *       hence in V1.2.0 simplelink stack this change is required 
 * @info
 *      the spawn mechanism enable to run a function on different context.
 * This mechanism allow to transfer the execution context from interrupt context to thread context
 * or changing the context from an unknown user thread to general context.
 * The implementation of the spawn mechanism depends on the user's system requirements and could varies
 * from implementation of serialized execution using single thread to creating thread per call
 *
 * @note
 *      The stack size of the execution thread must be at least of TBD bytes!
 */
typedef short (*WLANDRIVER_OSI_SPAWN_ENTRY)(void* pValue);


/**
 @brief  structure definition for spawn message
 */
struct WlanDriver_SlSpawnMsg_S
{
    WLANDRIVER_OSI_SPAWN_ENTRY pEntry;
    void* pValue;
};
typedef struct WlanDriver_SlSpawnMsg_S WlanDriver_SlSpawnMsg_T;

/*
 @brief     type definition for a sync object container

 @info  Sync object is object used to synchronize between two threads or thread and interrupt handler.One
 thread is waiting on the object and the other thread send a signal, which then release the waiting thread.
 The signal must be able to be sent from interrupt context. This object is generally implemented by binary
 semaphore or events.

 @note  On each porting or platform the type could be whatever is needed - integer, structure etc.
 */
typedef void * WlanDriver_OsSyncObject;

/*
 @brief     type definition for a locking object container

 @info      Locking object are used to protect a resource from mutual accesses of two or more threads.
 The locking object should support re-entrant locks by a signal thread.
 This object is generally implemented by mutex semaphore

 @note  On each porting or platform the type could be whatever is needed - integer, structure etc.
 */
typedef void * WlanDriver_OsLockObject;

/* @brief  Type definition for a time value
 @note  On each porting or platform the type could be whatever is needed - integer, pointer to structure etc.
 */
typedef uint32_t WlanDriver_OsTime;

/* public function prototype declarations */

/* public global variable declarations */
/**
 *  @brief
 *      Function to Initialize the WIFI Driver Layer
 */
Retcode_T WlanDriver_Init(void);

/**
 *  @brief
 *      Function to register the Interrupt handler
 *  @note
 *  	This function will be called by the WiFi driver API mapped in <user.h> header file
 */
int WlanDriver_RegInterruptHdlr(WLANDRIVER_EVENT_HANDLER InterruptHdl, void* pValue);

/**
 *  @brief
 *      Function to disable the WIFI device
 *  @note
 *      This function will be called by the WiFi driver API mapped in <user.h> header file
 */
void WlanDriver_DeviceDisable(void);

/**
 *  @brief
 *      Function to enable the WIFI device
 *  @note
 *  	This function will be called by the WiFi driver API mapped in <user.h> header file
 */
void WlanDriver_DeviceEnable(void);

/**
 *  @brief
 *      Function to enable the WIFI device Interrupt
 *  @note
 *  	This function will be called by the WiFi driver API mapped in <user.h> header file
 */
void WlanDriver_InterruptEnable(void);

/**
 *  @brief
 *      Function to disable the WIFI device Interrupt
 *  @note
 *  	This function will be called by the WiFi driver API mapped in <user.h> header file
 */
void WlanDriver_InterruptDisable(void);

/**
 *  @brief
 *      Function to close the SPI port
 *
 *  @retval  int
 *              WLANDRIVER_SUCCESS - Successfull close of spi port
 *              WLANDRIVER_SPI_ERROR - Not Successfull in closing spi port
 *  @note
 *      The return type 'int' and input parameter 'Fd_t' cannot be replaced with BCDS datatype since prototype has
 *      to match one on one with third party function data type
 */
int WlanDriver_SpiClose(Fd_t fd);

/**
 *  @brief
 *      Function to Initialize the SPI port
 *
 *  @retval  Fd_t
 *      WLANDRIVER_SUCCESS - Successfully opened the SPI port
 *      WLANDRIVER_SPI_ERROR -Failed to open SPI port
 *
 *  @note
 *      The return type 'Fd_t' and input parameters 'char*'and 'unsigned long' cannot be
 *      replaced with BCDS datatype since prototype has to match one on one with third
 *      party function data type
 */
Fd_t WlanDriver_SpiOpen(char *ifName, unsigned long flags);

/**
 *  @brief
 *      Function to write given no of  bytes over SPI
 *
 *  @param [in]  fd -file descriptor for spi port, which is not used in current implementation
 *
 *  @param [in]  pBuff - pointer to the first location of a buffer that contains the data to send
 *                        over the communication channel.
 *
 *  @param [in]  len - number of bytes to write to the communication channel
 *
 *   @retval  int
 *              WLANDRIVER_SPI_RW_ERROR - spi write failed
 *              lenToReturn - on success returns number of bytes sent
 *  @note
 *      The return type 'int' and input parameters 'Fd_t','unsigned char*'and 'int' cannot be
 *      replaced with BCDS datatype since prototype has to match one on one with third
 *      party function data type
 */
int WlanDriver_SpiWrite(Fd_t fd, unsigned char *pBuff, int len);

/**
 *  @brief
 *      Function to read given number of bytes over SPI
 *
 *  @param [in]  fd -file descriptor for spi port, which is not used in current implementation
 *
 *  @param [in]  pBuff - pointer to the first location of a buffer that contains enough
 *                       space for all expected data
 *
 *  @param [in]  len -  number of bytes to read from the communication channel
 *
 *   @retval  int
 *              WLANDRIVER_SPI_RW_ERROR - spi read failed
 *              len - on success returns number of bytes received
 *  @note
 *      The return type 'int' and input parameters 'Fd_t','unsigned char*'and 'int' cannot be
 *      replaced with BCDS datatype since prototype has to match one on one with third
 *      party function data type
 */
int WlanDriver_SpiRead(Fd_t fd, unsigned char *pBuff, int len);

/** @brief This function creates a sync object (binary semaphore)
 *          The sync object is used for synchronization between diffrent thread or ISR and
 *          a thread.
 *  @param [in] pSyncObj
 *          pointer to the sync object control block
 * @retval  int32_t
 *              WLANDRIVER_SUCCESS - successfully created synchronous object
 *              WLANDRIVER_FAILURE - error in creating synchronous object
 */
int32_t WlanDriver_OsSyncObjectCreate(WlanDriver_OsSyncObject* pSyncObj);

/** @brief This function deletes a sync object (binary semaphore) if the input parameter is valid
 *  @param [in] pSyncObj
 *          pointer to the sync object control block
 * @retval  int32_t
 *              WLANDRIVER_SUCCESS - successfully deleted synchronous object
 *              WLANDRIVER_FAILURE - error in deleting synchronous object
 */
int32_t WlanDriver_OsSyncObjectDelete(WlanDriver_OsSyncObject* pSyncObj);

/** @brief This function generates a sync signal for the object (binary semaphore)
 *         All suspended threads waiting on this sync object are resumed
 *  @param [in] pSyncObj
 *          pointer to the sync object control block
 *  @retval  int32_t
 *          WLANDRIVER_SUCCESS - successfully released the synchronous object
 *          WLANDRIVER_FAILURE - the object value is NULL
 *  @note
 *          This API gives the synchronous object successfully if it was already taken by some one. If the sync object given as input was not
 *          taken and this API call simply returns since sync object shall not be given if it is not taken
 */
int32_t WlanDriver_OsSyncObjectSignal(WlanDriver_OsSyncObject* pSyncObj);

/** @brief This function generates a sync signal for the object from Interrupt
 *  @param [in] pSyncObj
 *         pointer to the sync object control block
 *  @retval  int32_t
 *          WLANDRIVER_SUCCESS - successfully released the synchronous object from interrupt
 *          WLANDRIVER_FAILURE - Error in giving the object from interrupt
 */
int32_t WlanDriver_OsSyncSignalFromISR(WlanDriver_OsSyncObject* pSyncObj);

/** @brief This function waits for a sync signal of the specific sync object
 *  @param [in] pSyncObj
 *         pointer to the sync object control block
 *  @param [in] Timeout
 *         numeric value specifies the maximum number of mSec to stay suspended while waiting for the sync signal
 *  @retval int32_t
 *          WLANDRIVER_SUCCESS -  reception of the signal within the TimeOut window
 *          WLANDRIVER_FAILURE - Error not able to take sync object within specified time window
 */
int32_t WlanDriver_OsSyncObjectWait(WlanDriver_OsSyncObject* pSyncObj, WlanDriver_OsTime TimeOut);

/** @brief This API tries to take the sync object without no wait time
 *  @param [in] pSyncObj
 *          pointer to the sync object control block
 *  @retval int32_t
 *          WLANDRIVER_SUCCESS -  successful reception of the signal
 *          WLANDRIVER_FAILURE - Error not able to take sync object
 */
int32_t WlanDriver_OsSyncObjectClear(WlanDriver_OsSyncObject* pSyncObj);

/** @brief This function creates a locking object (mutex semaphore)
 *         Locking object are used to protect a resource from mutual accesses of two or more threads.
 *  @param [in] pLockObj
 *         pointer to the locking object control block
 * @retval  int32_t
 *              WLANDRIVER_SUCCESS - successfully created lock object
 *              WLANDRIVER_FAILURE - error in creating lock object
 */
int32_t WlanDriver_OsLockObjectCreate(WlanDriver_OsLockObject* pLockObj);

/** @brief This function deletes a locking object
 *  @param [in] pLockObj
 *         pointer to the locking object control block
 * @retval  int32_t
 *              WLANDRIVER_SUCCESS - successfully deleted lock object
 *              WLANDRIVER_FAILURE - error in deleting lock object
 */
int32_t WlanDriver_OsLockObjectDelete(WlanDriver_OsLockObject* pLockObj);

/** @brief
 *  For details refer API documentation is in the interface header file  WlanDriverInterface.h
 */
/** @brief This function locks a locking object
 *         All other threads that call this function before this thread calls the WlanDriver_osLockUnlock would be suspended
 *  @param [in] pLockObj
 *         pointer to the lock object control block
 *  @param [in] Timeout
 *         numeric value specifies the maximum number of mSec to  stay suspended while waiting for the locking object
 *  @retval int32_t
 *          WLANDRIVER_SUCCESS -  reception of the locking object within the TimeOut window
 *          WLANDRIVER_FAILURE - Error not able to take lock object within specified time window
 */
int32_t WlanDriver_OsLockObjectWait(WlanDriver_OsLockObject* pLockObj, WlanDriver_OsTime TimeOut);

/** @brief This function unlock a locking object
 *  @param [in] pLockObj
 *         pointer to the lock object control block
 *  @retval  int32_t
 *          WLANDRIVER_SUCCESS - successfully released the lock object
 *          WLANDRIVER_FAILURE - Error in releasing the lock  object
 */
int32_t WlanDriver_OsLockUnlock(WlanDriver_OsLockObject* pLockObj);

/**
 * @brief    This is the simplelink spawn task required to call SimpleLink Driver's callback from a different context
 * @param[in] pvParameters
 *            pointer to the task parameter
 */
void WlanDriver_SimpleLinkSpawnTask(void *pvParameters);

/** @brief This function call the pEntry callback from a different context
 *  @param[in] pEntry - pointer to the entry callback
 *  @param[in] pValue - pointer to any type of memory structure that would be passed to pEntry callback from the execution thread.
 *  @param[in] flags  - execution flags - reserved for future usage
 *  @retval int32_t
 *          WLANDRIVER_SUCCESS -  successful registration of the spawn function
 *          WLANDRIVER_FAILURE - Error in registration of spawn function
 */
int32_t WlanDriver_OsiSpawn(WLANDRIVER_OSI_SPAWN_ENTRY pEntry, void* pValue, unsigned long flags);
/**
 *  @brief
 *      Function to De-Initialize the WIFI Driver Layer.
 *      disabling WiFi clock , Powering off the WiFi module,Deleting the task and queue created
 */
void WlanDriver_DeInit(void);

/* public global variable declarations */

/* inline function definitions */

/**@}*/
#endif /* BCDS_WLANDRIVER_H_ */


