/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module will calculate & validate the firmware CRC, Hash calculation and Firmware Signature and updated the results.
 */
#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_VALIDATIONAGENT

#include "BCDS_Fota.h"

#if BCDS_FEATURE_VALIDATION_AGENT
#include "BCDS_ValidationAgent.h"

#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "BCDS_FotaStorageAgent.h"
#include "BCDS_FWContainer.h"
#include "FirmwareValidation.h"
#include "BCDS_FotaConfig.h"
#include "sha_256.h"
#include "crc_32.h"


/* Put the type and macro definitions here */
#define HASH_KEY_LEN  UINT8_C(32)
#define MAX_BLOCK_SIZE UINT16_C(512)

/* The description is in interface header */
Retcode_T ValidationAgent_VerifyFWCRC32(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T SelectedPartition)
{
    Retcode_T retVal = (Retcode_T) RETCODE_FAILURE;
    Esc_UINT32 CalculatedCRC = 0;
    uint32_t firmwareSize = 0;
    uint16_t blocks = 0;
    uint16_t remainingBytes = 0;
    uint32_t offset = FWCONTAINER_HEADER_SIZE;
    uint8_t firmware[MAX_BLOCK_SIZE] = { 0 };

    if ((NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= SelectedPartition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }

    retVal = FWContainer_ReadFWSize(StorageAgentHandle, SelectedPartition, (uint8_t *) &firmwareSize);
    if (RETCODE_OK != retVal)
    {
        return retVal;
    }
    blocks = firmwareSize / MAX_BLOCK_SIZE;
    remainingBytes = firmwareSize % MAX_BLOCK_SIZE;
    if (EscCrc32_Init(&CalculatedCRC) == Esc_NO_ERROR)
    {
        while (blocks--)
        {
            retVal = StorageAgentHandle->Read(SelectedPartition, firmware, offset, MAX_BLOCK_SIZE);
            if (RETCODE_OK != retVal)
            {
                return retVal;
            }
            offset = offset + MAX_BLOCK_SIZE;
            if (EscCrc32_Compute(firmware, &CalculatedCRC, MAX_BLOCK_SIZE) != Esc_NO_ERROR)
            {
                return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) FOTA_RETCODE_CRC_ERROR);
            }
        }
        if (0 != remainingBytes)
        {
            retVal = StorageAgentHandle->Read(SelectedPartition, firmware, offset, remainingBytes);
            if (RETCODE_OK != retVal)
            {
                return retVal;
            }
            if (EscCrc32_Compute(firmware, &CalculatedCRC, remainingBytes) != Esc_NO_ERROR)
            {
                return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) FOTA_RETCODE_CRC_ERROR);
            }
        }
    }
    else
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) FOTA_RETCODE_CRC_ERROR);
    }
    if (EscCrc32_Finalize(&CalculatedCRC) == Esc_NO_ERROR)
    {
        uint32_t firmwareCRC = UINT32_C(0);
        retVal = FWContainer_ReadFWCRC(StorageAgentHandle, SelectedPartition, (uint8_t *) &firmwareCRC);
        if (RETCODE_OK != retVal)
        {
            return retVal;
        }
        if (CalculatedCRC == firmwareCRC)
        {
            return RETCODE_OK;
        }
        else
        {
            return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) FOTA_RETCODE_CRC_ERROR);
        }
    }
    return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) FOTA_RETCODE_INTERNAL_ERROR));
}

/* The description is in interface header */
Retcode_T ValidationAgent_VerifyHeaderCRC32(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T SelectedPartition)
{
    Retcode_T retVal = (Retcode_T) RETCODE_FAILURE;
    FWContainer_Information_T containerInfo;
    Esc_UINT32 CalculatedCRC = 0;

    if ((NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= SelectedPartition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    retVal = StorageAgentHandle->Read(SelectedPartition, (uint8_t *) &containerInfo, FWCONTAINER_HDR_START_ADR, FWCONTAINER_HEADER_SIZE);
    if (RETCODE_OK != retVal)
    {
        return retVal;
    }
    if (EscCrc32_Init(&CalculatedCRC) == Esc_NO_ERROR)
    {
        if (EscCrc32_Compute((uint8_t *) &containerInfo, &CalculatedCRC,
                containerInfo.HeaderSize - 4) == Esc_NO_ERROR)
        {
            if (EscCrc32_Finalize(&CalculatedCRC) == Esc_NO_ERROR)
            {
                if (CalculatedCRC == containerInfo.HeaderCRC)
                {
                    return RETCODE_OK;
                }
                else
                {
                    return RETCODE(RETCODE_SEVERITY_ERROR,
                            (uint32_t) FOTA_RETCODE_CRC_ERROR);
                }
            }
        }
    }
    return RETCODE(RETCODE_SEVERITY_ERROR,
            (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
}

/* The description is in interface header */
Retcode_T ValidationAgent_VerifyPKCS1V1_5(
        FotaStorageAgentPtr_T StorageAgentHandle,
        Fota_Partition_T SelectedPartition,
        const EscRsa_PubKeyT * PublicKey)
{
    Retcode_T RetVal;
    FWContainer_Information_T containerInfo;

    Esc_UINT8 CalculatedSha256Hash[32];

    if ((NULL == StorageAgentHandle) || (NULL == PublicKey) || (FOTA_PARTITION_INVALID <= SelectedPartition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    RetVal = FWContainer_ParseInformation(StorageAgentHandle, SelectedPartition, &containerInfo);
    if (RETCODE_OK != RetVal)
    {
        return RetVal;
    }
    RetVal = ValidationAgent_HashCalculation(StorageAgentHandle, SelectedPartition, &containerInfo,
            CalculatedSha256Hash);
    if (RetVal == RETCODE_OK)
    {
        return ValidationAgent_SignatureVerification(PublicKey, &containerInfo,
                CalculatedSha256Hash);
    }
    else
    {
        return RetVal;
    }
}

/* The description is in interface header */
Retcode_T ValidationAgent_HashCalculation(FotaStorageAgentPtr_T StorageAgentHandle,
        Fota_Partition_T SelectedPartition,
        FWContainer_Information_T * containerInfo,
        Esc_UINT8 * OutputHashBuffer)
{
    /* Calculate the hash for the firmware block by block */
    EscSha256_ContextT Sha256CalcuationContext;
    Retcode_T retval;

    if ((NULL == StorageAgentHandle) || (NULL == containerInfo))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else if (EscSha256_Init(false, &Sha256CalcuationContext) == Esc_NO_ERROR)
    {
        /* Calculate the number of blocks with full READ_BLOCK_SIZE */
        uint32_t FullBlocksToRead = (containerInfo->HeaderSize + containerInfo->FirmwareSize) / READ_BLOCK_SIZE;
        uint16_t Rest = (containerInfo->HeaderSize + containerInfo->FirmwareSize) % READ_BLOCK_SIZE;
        Esc_UINT8 CurrentBlock[READ_BLOCK_SIZE];
        for (uint32_t PackNr = 0; PackNr < FullBlocksToRead; PackNr++)
        {
            retval = StorageAgentHandle->Read((Fota_Partition_T) SelectedPartition, CurrentBlock, (PackNr * READ_BLOCK_SIZE), READ_BLOCK_SIZE);
            if (RETCODE_OK == retval)
            {
                if (EscSha256_Update(&Sha256CalcuationContext, CurrentBlock,
                READ_BLOCK_SIZE) != Esc_NO_ERROR)
                {
                    return RETCODE(RETCODE_SEVERITY_ERROR,
                            (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
                }
            }
            else
            {
                return retval;
            }
        }
        /*Calculate the hash for the last package < READ_BLOCK_SIZE if needed
         * (most likely)
         */
        if (Rest)
        {
            retval = StorageAgentHandle->Read((Fota_Partition_T) SelectedPartition, CurrentBlock, (FullBlocksToRead * READ_BLOCK_SIZE), Rest);

            if (RETCODE_OK == retval)
            {
                if (EscSha256_Update(&Sha256CalcuationContext, CurrentBlock,
                        Rest) != Esc_NO_ERROR)
                {
                    return RETCODE(RETCODE_SEVERITY_ERROR,
                            (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
                }
            }
            else
            {
                return retval;
            }
        }
        if (EscSha256_Finish(&Sha256CalcuationContext, OutputHashBuffer,HASH_KEY_LEN) ==
        Esc_NO_ERROR)
        {
            return RETCODE_OK;
        }
    }
    return RETCODE(RETCODE_SEVERITY_ERROR,
            (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
}

/* The description is in interface header */
Retcode_T ValidationAgent_SignatureVerification(
        const EscRsa_PubKeyT * PublicKey, FWContainer_Information_T * containerInfo,
        Esc_UINT8 * Sha256Hash)
{
    Esc_UINT8 Signature[256];
    memcpy(Signature, containerInfo->FirmwareSignature, 256);
    Esc_ERROR retSignatureVerify = EscPkcs1RsaSsaV15_Verify(Signature,sizeof(Signature),
            PublicKey, Sha256Hash, EscPkcs1_DIGEST_TYPE_SHA256);

    if (retSignatureVerify == Esc_NO_ERROR)
    {
        return RETCODE_OK;
    }
    else if (retSignatureVerify == Esc_INVALID_SIGNATURE)
    {
        return RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) FOTA_RETCODE_INVALID_SIGNATURE);
    }
    else
    {
        return RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
    }
}

/* The description is in interface header */
Retcode_T ValidationAgent_VerifyHeader(FotaStorageAgentPtr_T storageAgent, Fota_Partition_T partition)
{
    BCDS_UNUSED(partition);
    Retcode_T retVal = RETCODE_OK;
    uint8_t hdrValidationStatus = 0;
    retVal = FirmwareValidation_VerifyHeader(storageAgent, &hdrValidationStatus);
    if (RETCODE_OK != retVal)
    {
        switch (hdrValidationStatus)
        {
        case CONTAINER_CRC_FAIL:
            retVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_CRC_ERROR);
            break;

        case VERSION_FAIL:
            case CONTAINER_HEADER_VERSION_FAIL:
            retVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_FIRMWARE_VERSION_FAIL);
            break;

        case FIRMWARE_SIZE_FAIL:
            retVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_FIRMWARE_SIZE_FAIL);
            break;
        case CONTAINER_READ_FAIL:
            retVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_CONTAINER_READ_FAIL);
            break;

        default:
            retVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_CONTAINER_READ_FAIL);
            break;
        }
    }
    return retVal;
}

/* The description is in interface header */
Retcode_T ValidationAgent_VerifyFirmware(FotaStorageAgentPtr_T storageAgent, Fota_Partition_T partition)
{
#if FOTA_WITH_SIGNATURE
    Retcode_T RetVal = RETCODE_OK;
   EscRsa_PubKeyT pubKey;
    RetVal = (Retcode_T)EscRsa_PubKeyFromBytes(&pubKey,keySizeBits, CurrentMod, pubExp);
   if(RetVal != RETCODE_OK)
   {       
       return RETCODE(RETCODE_SEVERITY_ERROR,
               (uint32_t) FOTA_RETCODE_INTERNAL_ERROR);
   }
    return ValidationAgent_VerifyPKCS1V1_5(storageAgent,partition,&pubKey);
#else
    return ValidationAgent_VerifyFWCRC32(storageAgent, partition);
#endif
}
#endif /*BCDS_FEATURE_VALIDATION_AGENT*/
