/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module provide the control of firmware downloading,status notify to the application
 *  and provide the common interfaces between CoAP and application Layer.
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_DOWNLOADCLIENT

/* system header files */
//lint -e49 error in standard libraries suppressed
//lint -e309 error in third party library suppressed
//lint -e547 error in platform suppressed
//lint -esym(956,*) /* Suppressing Non const, non volatile static or external variable */
#include "BCDS_Fota.h"

#if BCDS_FEATURE_DOWNLOAD_CLIENT

/* own header files */
#include "BCDS_DownloadClient.h"
#include "DownloadClient.h"

/* additional interface header files */
#include <Serval_Log.h>
#include <Serval_Timer.h>
#include <Serval_Network.h>
#include <Serval_Coap.h>
#include "BCDS_CoapBlockwise.h"

#define LOG_MODULE "DlC"

/* static variable for reference of Download context */
static DownloadClient_Context_T downloadContext;

/* functions */
static retcode_t SentCallback(Callable_T *callablePntr, retcode_t status)
{
    if (NULL == callablePntr)
        return RC_APP_ERROR;

    LOG_TRACE_ENTER_ARGS("%p, %i", callablePntr, status);

    //lint -e{413} hear only allocating memory for this structure . it will not be a NULL So this warning has been suppressed.
    DownloadClient_Context_T* dlCtx = CALLABLE_GET_CONTEXT(DownloadClient_Context_T, onSent, callablePntr);
    if (NULL == dlCtx)
        return RC_APP_ERROR;

    switch (status)
    {
    case RC_OK:
        dlCtx->onDownloadStatusChanged(DL_REQUEST_SENT, NULL, 0, dlCtx->blockOptionValue);
        break;

    case RC_COAP_REQUEST_TIMEOUT:
        dlCtx->onDownloadStatusChanged(DL_TIMEOUT, NULL, 0, dlCtx->blockOptionValue);
        break;

    case RC_MSG_UDP_SEND_ERROR:
    case RC_COAP_SENDING_ERROR:
        dlCtx->onDownloadStatusChanged(DL_SEND_ERROR, NULL, 0, dlCtx->blockOptionValue);
        break;

    default:
        dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, dlCtx->blockOptionValue);
        break;
    }

    LOG_TRACE_EXIT();

    return RC_OK;
}

#if SERVAL_POLICY_LARGE_PAYLOAD
static bool ValidBlockOption(uint32_t blockOptionValue, uint32_t blockValue, uint16_t payloadLen)
#else
static bool ValidBlockOption(uint32_t blockOptionValue, uint32_t blockValue, uint8_t payloadLen)
#endif
{
    if (blockValue == COAPBLOCKWISE_INVALID)
    {
        LOG_ERROR("Invalid block option received\n");
        return false;
    }

    if (!CoapBlockwise_IsExpectedBlock(blockOptionValue, blockValue))
    {
        LOG_ERROR("Received unexpected block. Expected block %u, received %d\n", CoapBlockwise_GetBlockNumber(blockOptionValue), CoapBlockwise_GetBlockNumber(blockValue));
        return false;
    }

    if (!CoapBlockwise_IsCorrectPacketSize(blockValue, payloadLen))
    {
        LOG_ERROR("Incomplete block of length %d received. Expected %d bytes payload\n", payloadLen, CoapBlockwise_GetBlockSize(blockValue));
        return false;
    }

    if (!CoapBlockwise_IsExpectedSize(blockOptionValue, blockValue))
    {
        /* we could handle smaller block sizes */
        LOG_INFO("Falling back to blocksize of %d bytes due to server reply\n", CoapBlockwise_GetBlockSize(blockValue));
    }

    return true;
}

static retcode_t ResponseCallback(CoapSession_T *coapSession, Msg_T *msgPntr, retcode_t status)
{

    BCDS_UNUSED(coapSession);

    /** @TODO  improvements are required,we cannot really get to the DownloadContext easily */
    DownloadClient_Context_T* dlCtx = &downloadContext;
    retcode_t rc = RC_OK;

    if (status == RC_COAP_REQUEST_TIMEOUT)
    {
        dlCtx->onDownloadStatusChanged(DL_TIMEOUT, NULL, 0, dlCtx->blockOptionValue);
        return RC_OK;
    }
    if (status != RC_OK)
    {
        dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, dlCtx->blockOptionValue);
        return RC_OK;
    }

    if (msgPntr == NULL)
    {
        LOG_ERROR("ResponseCallback Error: msgPntr == null\n");
        dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, dlCtx->blockOptionValue);
        return RC_OK;
    }

    CoapParser_T parser;
    CoapParser_setup(&parser, msgPntr);

    uint8_t code = CoapParser_getCode(msgPntr);
    if (code != Coap_Codes[COAP_CONTENT])
    {
        LOG_ERROR("Response Code %u.%02u received\n", (code & 0xC0) >> 5, code & ~0xC0);
        DownloadClient_Status_T dlStatus = (code == Coap_Codes[COAP_NOT_FOUND] ? DL_NOTFOUND : DL_ERROR);
        dlCtx->onDownloadStatusChanged(dlStatus, NULL, 0, dlCtx->blockOptionValue);
        return RC_OK;
    }

    uint8_t const *payload_ptr;
#if SERVAL_POLICY_LARGE_PAYLOAD
    uint16_t payloadLen;
#else
    uint8_t payloadLen;
#endif

    CoapOption_T option;
    rc = CoapParser_getOption(&parser, msgPntr, &option, Coap_Options[COAP_BLOCK_2]);
    if (rc != RC_OK)
    {
        /** @TODO
         1. block option might be absent in which case we could get the entire payload
         2. check if block option is critical
         */
        LOG_ERROR("Expected block option to be present\n");
        dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, dlCtx->blockOptionValue);
        return RC_OK;
    }

    rc = CoapParser_getPayload(&parser, &payload_ptr, &payloadLen);

    if (rc == RC_OK)
    {
        uint32_t blockValue = CoapBlockwise_GetBlockOptionValue(&option);
        if (!ValidBlockOption(dlCtx->blockOptionValue, blockValue, payloadLen))
        {
            dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, dlCtx->blockOptionValue);
            return RC_OK;
        }

        uint32_t blockOptionValue = dlCtx->blockOptionValue;

        dlCtx->blockOptionValue = CoapBlockwise_GetNextBlock(blockValue);
        switch (dlCtx->blockOptionValue)
        {
        case COAPBLOCKWISE_DONE:
            dlCtx->onDownloadStatusChanged(DL_DONE, payload_ptr, payloadLen, blockOptionValue);
            break;

        case COAPBLOCKWISE_INVALID:
            dlCtx->onDownloadStatusChanged(DL_ERROR, NULL, 0, blockOptionValue);
            break;

        default:
            dlCtx->onDownloadStatusChanged(DL_DATA, payload_ptr, payloadLen, blockOptionValue);
        }
    }
    return rc;
}

static uint16_t LengthUntildelimiter(const uint8_t* str, const char delimiter)
{
    uint16_t index = 0;
    if (NULL != str)
    {
        while (str[index] != delimiter && str[index] != '\0' && index < COAP_MSG_MAX_LEN)
        {
            ++index;
        }
    }
    return index;
}

static retcode_t SerializeUriPath(CoapSerializer_T* serializer, Msg_T* msgPntr, const char* targetLocation)
{
    if (NULL == targetLocation)
        return RC_APP_ERROR;

    retcode_t rc = RC_OK;
    CoapOption_T opt;

    if (targetLocation[0] == '\0')
    {
        return RC_OK;
    }

    opt.OptionNumber = Coap_Options[COAP_URI_PATH];
    opt.value = (const uint8_t*) targetLocation;

    while (rc == RC_OK)
    {
        opt.length = LengthUntildelimiter(opt.value, '/');
        rc = CoapSerializer_serializeOption(serializer, msgPntr, &opt);

        if (opt.value[opt.length] == '\0')
        {
            break;
        }

        opt.value = &opt.value[opt.length + 1];
    }

    return rc;
}

static retcode_t SerializeGetRequest(DownloadClient_Context_T* dlCtx, Msg_T* msgPntr)
{
    retcode_t rc = RC_OK;
    CoapSerializer_T serializer;

    rc = CoapSerializer_setup(&serializer, msgPntr, REQUEST);
    if (rc != RC_OK)
    {
        return rc;
    }
    rc = CoapSerializer_setCode(&serializer, msgPntr, Coap_Codes[COAP_GET]);
    if (rc != RC_OK)
    {
        return rc;
    }
    CoapSerializer_setConfirmable(msgPntr, dlCtx->confirmableRequests);

    rc = SerializeUriPath(&serializer, msgPntr, dlCtx->targetLocation);
    if (rc != RC_OK)
    {
        return rc;
    }

    rc = CoapBlockwise_AddBlockOption(&serializer, msgPntr, dlCtx->blockOptionValue);
    if (rc != RC_OK)
    {
        return rc;
    }

    rc = CoapSerializer_setEndOfOptions(&serializer, msgPntr);
    if (rc != RC_OK)
    {
        return rc;
    }
    return rc;
}

static retcode_t SendGetRequest(DownloadClient_Context_T* dlCtx)
{
    retcode_t rc = RC_OK;
    Msg_T* msgPntr;

    rc = DownloadClient_InitMsg(dlCtx, &msgPntr);

    if (rc == RC_OK)
    {
        rc = SerializeGetRequest(dlCtx, msgPntr);
    }

    if (rc == RC_OK)
    {
        rc = CoapClient_request(msgPntr, &dlCtx->onSent, ResponseCallback);
    }

    return rc;
}

static uint8_t StringToIpv4Addr(char const *str, Ip_Address_T *ipAddress)
{

    uint8_t ip[4] = { 0, 0, 0, 0 };
    uint8_t position = 0;
    uint32_t groupValue = 0;
    uint8_t count = 0;
    uint8_t index;

    if (NULL != str)
    {
        if (NULL != ipAddress)
        {

            for (index = 0; index < 4; ++index)
            {

                count = DownloadClient_StrToUnsignedDec(&str[position], &groupValue);
                if (!DownloadClient_IsValidGroup(count, groupValue))
                {
                    return 0;
                }

                ip[index] = groupValue & 0xFF;
                position += count;

                if (index < 3)
                {
                    if (str[position] != '.')
                    {
                        return 0;
                    }

                    position += 1;
                }
            }

            Ip_convertOctetsToAddr(ip[0], ip[1], ip[2], ip[3], ipAddress);
        }
    }
    return (position);
}

static uint8_t ParseIpAndPort(const char* ipAndPort, Ip_Address_T *ipAddress, Ip_Port_T *port)
{
    uint8_t index = 0;
    uint8_t count = 0;
    *port = 0; /* clearing the buffer */

    index = StringToIpv4Addr(&ipAndPort[index], ipAddress);
    if (0 == index)
    {
        return 0;
    }

    if (ipAndPort[index] == ':')
    {
        index++;
        count = DownloadClient_StringToPort(&ipAndPort[index], port);
        if (0 == count)
        {
            return 0;
        }

        index += count;
    }

    return (index);
}

static uint8_t ParseScheme(const char *url, const char* scheme, bool* secured)
{
    uint8_t schemeLength = strlen(scheme);
    uint8_t index;

    if (strncmp(url, scheme, schemeLength) != 0)
    {
        return 0;
    }

    index = schemeLength;

    if (url[schemeLength] == 's')
    {
        *secured = true;
        index++;
    }
    else
    {
        *secured = false;
    }

    if (strncmp(&url[index], "://", 3) != 0)
        return 0;

    return (index + 3);
}

static uint8_t ParseUrl(const char *url, const char* scheme, Ip_Address_T *address, Ip_Port_T *port, bool* secured)
{
    uint8_t position = 0;
    uint8_t count;

    position = ParseScheme(url, scheme, secured);
    if (position == 0)
    {
        return 0;
    }

    count = ParseIpAndPort(&url[position], address, port);
    if (count == 0)
    {
        return 0;
    }

    return (position + count);
}

static retcode_t ParseDestinationUri(DownloadClient_Context_T* dlCtx, const char* uri)
{
    uint8_t index;

    index = ParseUrl(uri, "coap", &dlCtx->targetAddress, &dlCtx->targetPort, &dlCtx->targetRequiresDTLS);

    if ((index == 0) || (uri[index] != '/') || (uri[index + 1] == '\0'))
    {
        return RC_APP_ERROR;
    }

    dlCtx->targetLocation = &uri[index + 1];

    if (dlCtx->targetPort == 0)
    {
        dlCtx->targetPort = Ip_convertIntToPort(dlCtx->targetRequiresDTLS ? 5684 : 5683);
    }

#if ! SERVAL_ENABLE_DTLS_CLIENT
    if (dlCtx->targetRequiresDTLS)
    {
        LOG_ERROR("DTLS support not compiled in\n");
        return RC_APP_ERROR;
    }
#endif

    return RC_OK;
}

retcode_t DownloadClient_SetupNewDownload(const char* uri, uint8_t log2BlockSize, bool confirmableRequests, DownloadClient_StatusCallback onDownloadStatusChanged)
{
    if (NULL == onDownloadStatusChanged)
        return RC_APP_ERROR;
    if (NULL == uri)
        return RC_APP_ERROR;

    if (log2BlockSize < 4 || log2BlockSize > 10)
    {
        return RC_APP_ERROR;
    }
    if (uri[0] == '\0')
    {
        return RC_APP_ERROR;
    }
    if (ParseDestinationUri(&downloadContext, uri) != RC_OK)
    {
        return RC_APP_ERROR;
    }

    downloadContext.blockOptionValue = CoapBlockwise_NewTransfer(log2BlockSize);
    downloadContext.onDownloadStatusChanged = onDownloadStatusChanged;
    downloadContext.confirmableRequests = confirmableRequests;

    (void) Callable_assign(&downloadContext.onSent, SentCallback);

    return RC_OK;
}

void DownloadClient_ResumeFromBlock(uint32_t blockNumber)
{
    CoapBlockwise_SetBlockNumber(&downloadContext.blockOptionValue, blockNumber);
}

retcode_t DownloadClient_DownloadBlock(void)
{
    return SendGetRequest(&downloadContext);
}

const char* DownloadClient_GetTargetLocation(void)
{
    return (downloadContext.targetLocation);
}
#endif/*BCDS_FEATURE_DOWNLOAD_CLIENT*/
