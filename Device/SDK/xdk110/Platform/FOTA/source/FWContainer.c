/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module will parse the information into various partitions(Download,Primary,Backup) read the Firmware Signature  and update the results.
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_FWCONTAINER

#include "BCDS_Fota.h"

#if BCDS_FEATURE_FW_CONTAINER
#include "BCDS_FWContainer.h"
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "BCDS_FotaStorageAgent.h"

/* Put the type and macro definitions here */

/* The API description is available in interface header */
Retcode_T FWContainer_ParseInformation(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, FWContainer_Information_T * containerInfo)
{
    if ((NULL == containerInfo) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR,
                (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        if (RETCODE_OK == StorageAgentHandle->Read(Partition, (uint8_t *) containerInfo, FWCONTAINER_HDR_START_ADR, FWCONTAINER_HEADER_SIZE))
        {
            return (StorageAgentHandle->Read(Partition, (uint8_t *) containerInfo->FirmwareSignature,
            FWCONTAINER_HEADER_SIZE + containerInfo->FirmwareSize, FWCONTAINER_FIRMWARE_SIG_LEN));
        }
        else
        {
            return RETCODE(RETCODE_SEVERITY_ERROR,
                    (uint32_t) FOTA_RETCODE_INVALID_HEADER_VERSION);
        }
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadHeaderVersion(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerVersion)
{
    if ((NULL == headerVersion) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, headerVersion, FWCONTAINER_HDR_VER, FWCONTAINER_HDR_VER_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadHeaderSize(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerSize)
{
    if ((NULL == headerSize) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, headerSize, FWCONTAINER_HDR_SIZE, FWCONTAINER_HDR_SIZE_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadProductClass(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * productClass)
{
    if ((NULL == productClass) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, productClass, FWCONTAINER_PRODUCT_CLASS, FWCONTAINER_PRODUCT_CLASS_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadProductVar(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * productVariant)
{
    if ((NULL == productVariant) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, productVariant, FWCONTAINER_PRODUCT_VARIANT, FWCONTAINER_PRODUCT_VARIANT_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadFWVersion(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firewareVersion)
{
    if ((NULL == firewareVersion) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, firewareVersion, FWCONTAINER_FIRMWARE_VER, FWCONTAINER_FIRMWARE_VER_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadFWSize(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firewareSize)
{
    if ((NULL == firewareSize) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, firewareSize, FWCONTAINER_FIRMWARE_SIZE, FWCONTAINER_FIRMWARE_SIZE_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadFWCRC(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firmwareCrc)
{
    if ((NULL == firmwareCrc) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, firmwareCrc, FWCONTAINER_FIRMWARE_CRC, FWCONTAINER_FIRMWARE_CRC_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadHeaderCRC(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerCrc)
{
    if ((NULL == headerCrc) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    else
    {
        return (StorageAgentHandle->Read(Partition, headerCrc, FWCONTAINER_HDR_CRC, FWCONTAINER_HDR_CRC_LEN));
    }
}

/* The API description is available in interface header */
Retcode_T FWContainer_ReadFWSignature(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * fwSignature)
{
    if ((NULL == fwSignature) || (NULL == StorageAgentHandle) || (FOTA_PARTITION_INVALID <= Partition))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM);
    }
    Retcode_T returnValue = (Retcode_T) RETCODE_INVALID_PARAM;
    uint32_t firewareSize = UINT8_C(0);
    returnValue = FWContainer_ReadFWSize(StorageAgentHandle, Partition, (uint8_t *) &firewareSize);
    if (RETCODE_OK == returnValue)
    {
        returnValue = StorageAgentHandle->Read(Partition, fwSignature, FWCONTAINER_HEADER_SIZE + firewareSize, FWCONTAINER_FIRMWARE_SIG_LEN);
    }
    return (returnValue);
}
#endif /*BCDS_FEATURE_FW_CONTAINER*/
