/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module will validate the received firmware from server and update the results.
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_FIRMWAREVALIDATION

/* module includes ********************************************************** */


/* system header files */
/* own header files */
#include "FirmwareValidation.h"

#if BCDS_FEATURE_VALIDATION_AGENT

/* additional interface header files */
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "BCDS_FotaConfig.h"
#include "BCDS_FWContainer.h"
#include "BCDS_ValidationAgent.h"
#include "crc_32.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The function description is available in the interface header */

/* global functions ********************************************************* */

/* The function description is available in the Private header */
Retcode_T FirmwareValidation_VerifyHeader(FotaStorageAgentPtr_T StorageAgentHandle, uint8_t* hdrValditationStatus)
{
#ifndef CONFIG_EXTERNAL_NVM
    uint32_t currentFwVer = 0;
    uint32_t newFwVer = 0;
#endif /* CONFIG_EXTERNAL_NVM */
    uint32_t firmwareSize = 0;
    /* Local variables declaration */
    Retcode_T RetValue = RETCODE_OK;
    *hdrValditationStatus = (uint8_t) CONTAINER_VALIDATION_SUCCESS;

    /* Verify header CRC */
    RetValue = ValidationAgent_VerifyHeaderCRC32(StorageAgentHandle, FOTA_PARTITION_DOWNLOAD);
    if (RETCODE_OK != RetValue)
    {
        *hdrValditationStatus = (uint8_t) CONTAINER_CRC_FAIL;
        return (RetValue);
    }

    RetValue = FWContainer_ReadFWSize(StorageAgentHandle, FOTA_PARTITION_DOWNLOAD, (uint8_t *) &firmwareSize);
    if (RETCODE_OK != RetValue)
    {
        *hdrValditationStatus = (uint8_t) CONTAINER_READ_FAIL;
        return (RetValue);
    }
    /* Verify if firmware is the right size */
    if (firmwareSize > FOTA_FIRMWARE_MAX_SIZE)
    {
        *hdrValditationStatus = (uint8_t) FIRMWARE_SIZE_FAIL;
        return RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_FIRMWARE_SIZE_FAIL);
    }

#ifndef CONFIG_EXTERNAL_NVM
    /* Verify if we don't downgrade the firmware */
    RetValue = FWContainer_ReadFWVersion(StorageAgentHandle, FOTA_PARTITION_PRIMARY, (uint8_t *) &currentFwVer);
    if (RETCODE_OK == RetValue)
    {
        RetValue = FWContainer_ReadFWVersion(StorageAgentHandle, FOTA_PARTITION_DOWNLOAD, (uint8_t *) &newFwVer);
    }
    if (RETCODE_OK == RetValue)
    {
        if (currentFwVer > newFwVer)
        {
            *hdrValditationStatus = (uint8_t) VERSION_FAIL;
            return RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_RETCODE_FIRMWARE_VERSION_FAIL);
        }
    }
    else
    {
        *hdrValditationStatus = (uint8_t) CONTAINER_READ_FAIL;
        return RetValue;
    }
#endif /* CONFIG_EXTERNAL_NVM */

    return RetValue;

}

#endif /* BCDS_FEATURE_VALIDATION_AGENT */

/** ************************************************************************* */
