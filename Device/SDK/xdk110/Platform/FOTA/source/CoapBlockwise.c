/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module has CoAp Block allocation and serializes for FOTA download.
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_COAPBLOCKWISE

/* system header files */
//lint -e49 error in standard libraries suppressed
//lint -e309 error in platform suppressed
//lint -e547 error in platform suppressed
#include "BCDS_Fota.h"

#if BCDS_FEATURE_COAP_BLOCKWISE
/* additional interface header files */
#include <Serval_Defines.h>
#include <Serval_Policy.h>
#include <Serval_Types.h>
#include <Serval_Coap.h>

/* own header files */
#include "BCDS_CoapBlockwise.h"

/* functions */
/* The API description is in interface header */
retcode_t CoapBlockwise_AddBlockOption(CoapSerializer_T* serializer, Msg_T* msgPntr, uint32_t blockOptionValue)
{
    assert(NULL != serializer);
    assert(NULL != msgPntr);

    uint8_t formatValue[3] = { 0, 0, 0 };

    CoapOption_T opt = {
            .OptionNumber = Coap_Options[COAP_BLOCK_2],
            .length = 0,
            .value = NULL
    };

    if (blockOptionValue > COAPBLOCKWISE_MAX_BLOCK_OPTION)
    {
        return RC_COAP_SERIALIZE_ERROR;
    }

    formatValue[2] = (blockOptionValue & 0xFF);
    formatValue[1] = ((blockOptionValue >> 8) & 0xFF);
    formatValue[0] = ((blockOptionValue >> 16) & 0xFF);

    if (blockOptionValue >= COAPBLOCKWISE_BLOCK_OPTION_3)
    {
        opt.length = 3;
    }
    else if (blockOptionValue >= COAPBLOCKWISE_BLOCK_OPTION_2)
    {
        opt.length = 2;
    }
    else if (blockOptionValue >= COAPBLOCKWISE_BLOCK_OPTION_1)
    {
        opt.length = 1;
    }

    opt.value = (uint8_t const*) &formatValue[3 - opt.length];

    return CoapSerializer_serializeOption(serializer, msgPntr, &opt);
}

/* The API description is in interface header */
uint32_t CoapBlockwise_GetBlockOptionValue(CoapOption_T* blockOption)
{

    if (NULL == blockOption)
    {
        return COAPBLOCKWISE_INVALID;
    }

    if ((blockOption->length) > 3)
    {
        return COAPBLOCKWISE_INVALID;
    }

    uint32_t blockOptionValue = 0;
    for (uint8_t i = 0; i < blockOption->length; ++i)
    {
        blockOptionValue = (blockOptionValue << 8) + blockOption->value[i];
    }
    return (blockOptionValue);
}

#endif /*BCDS_FEATURE_COAP_BLOCKWISE*/
