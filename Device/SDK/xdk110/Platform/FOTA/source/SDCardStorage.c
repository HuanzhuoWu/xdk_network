/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This file provides the interfaces to access different partitions of the SDCard
 *  storage medium trough FATFs filesystem API's
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_SDCARDSTORAGE

/* own header files */
#include "BCDS_Fota.h"
#if BCDS_FEATURE_MEDIUM_SDCARD
#include "BCDS_FotaStorageAgent.h"
#include "SDCardStorage.h"

/* additional interface header files */
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "ff.h"
#include "BCDS_SDCardDriver.h"
#include "BCDS_FWContainer.h"
#include "FSH_flash_ih.h"

/* Local Function declarations ***************************************************** */
static Retcode_T SDCardStorage_Read(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes);
static Retcode_T SDCardStorage_Write(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes);
static Retcode_T SDCardStorage_Erase(Fota_Partition_T partition);
static Retcode_T SDCardStorage_Copy(Fota_Partition_T dest, Fota_Partition_T src);

/* global variables ********************************************************* */
FotaStorageAgent_T SDCardStorage =
        {
                .Read = SDCardStorage_Read,
                .Write = SDCardStorage_Write,
                .Erase = SDCardStorage_Erase,
                .Copy = SDCardStorage_Copy,
        };
/* inline functions ********************************************************* */
//lint -e514 warning Unusual use of a Boolean expression in platform suppressed
static_assert((FOTA_PARTITION_MAX_SIZE%(UINT32_C(4096)) == 0), "Primary Partition size is not 4K Flash Page aligned ")

//lint -e514 warning Unusual use of a Boolean expression in platform suppressed
static_assert((FOTA_PRIMARY_PARTTITION_START_ADDRESS%(UINT32_C(4096)) == 0), "Primary Partition start address is not 4K Flash Page aligned ")

/* local functions ********************************************************** */

static void SDCardStorage_GetPartitionName(Fota_Partition_T Partition, uint8_t* Filename)
{
    if (FOTA_PARTITION_BACKUP == Partition)
    {
        strncpy((char*) Filename, FIRMWARE_FILE_BACKUP, FIRMWARE_FILE_NAME_MAX_SIZE);
    }
    else if (FOTA_PARTITION_DOWNLOAD == Partition)
    {
        strncpy((char*) Filename, FIRMWARE_FILE_DOWNLOAD, FIRMWARE_FILE_NAME_MAX_SIZE);
    }
}

static Retcode_T SDCardStorage_checkCardInserted(void)
{
    Retcode_T retval = RETCODE_OK;
    SDCardDriver_Status_T status = SDCardDriver_GetDetectStatus();
    if (SDCARD_NOT_INSERTED == status)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_NOT_DETECTED));
    }
    return retval;
}

static Retcode_T SDCardStorage_deleteFile(const char* path)
{
    FRESULT _fileSystemResult;
    Retcode_T retval = RETCODE_OK;

    _fileSystemResult = f_unlink(path);
    if ((FR_OK != _fileSystemResult) && (FR_NO_FILE != _fileSystemResult))
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_ERASE_ERROR));
    }
    return retval;
}

static Retcode_T SDCardStorage_getFileSize(const char* path, int32_t* size, uint8_t fileOperation)
{
    Retcode_T retval = RETCODE_OK;
    FRESULT _fileSystemResult;
    FILINFO _fileInfo;

    memset(&_fileInfo, 0, sizeof(_fileInfo));
    _fileSystemResult = f_stat(path, &_fileInfo);
    if (FR_OK == _fileSystemResult)
    {
        *size = _fileInfo.fsize;
    }
    else if (FR_NO_FILE == _fileSystemResult)
    {
        *size = -1;
        if (fileOperation == FILE_OPERATION_READ)
        {
            retval = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_NOT_FOUND);
        }
    }
    else
    {
        *size = -1;
        retval = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR);
    }
    return retval;
}

/**
 * @brief   This function reads the data from the specified partition of the storage medium. This intern uses FATFs File system API's
 *          to read the data from the storage before calling this function ensure SDcard has been initialized properly.
 *			Also the caller of this function has to ensure the valid position & no of bytes to read from the medium. Also
 *			partitions are mapped to a specific file names.
 *
 * @note    Please make sure to initialize the storage medium before calling SDCard Storage_Read. Data is read from the SDCard when
 *			the Partition is FOTA_PARTITION_BACKUP or FOTA_PARTITION_DOWNLOAD. Data read from Internal Flash when the partition is
 *			FOTA_PARTITION_PRIMARY. Also FATfs file system is used for FOTA_PARTITION_BACKUP/FOTA_PARTITION_DOWNLOAD. So File names has been 
 *			fixed for the "firmware.bin"(FOTA_PARTITION_DOWNLOAD) and "firmware.bkp" for the FOTA_PARTITION_BACKUP partitions. while the primary partition 
 *			FOTA_PARTITION_PRIMARY access is based on address, which can be configured by the application through "BCDS_Fotaconfig.h". 
 * 			Also make sure the primary partition address is page aligned to the internal flash.
 *
 * @param[in] partition: Partition of the storage medium to read.
 *
 * @param[out] buff: pointer to destination buff to hold the data.  This pointer should have enough room to place no of bytes that is
 *                      mentioned in argument numOfBytes.
 *
 * @param[in] position: offset from the start of the partition.
 *
 * @param[in] numOfBytes: Number of bytes to read from the storage medium.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
static Retcode_T SDCardStorage_Read(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes)
{
    Retcode_T _result = RETCODE_OK;
    FRESULT _fileSystemResult;
    FIL _fileReadHandle;
    UINT _bytesRead;
    int32_t fileSize;
    uint32_t Addr = 0;
    uint8_t filename[FIRMWARE_FILE_NAME_MAX_SIZE] = { 0 };

    if (NULL == buff)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NULL_POINTER));
    }
    if (FOTA_PARTITION_INVALID <= partition)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) FOTA_STORAGE_MEDIUM_INVALID));
    }
    if (FOTA_PARTITION_PRIMARY == partition)
    {
        /*Flash reading required here*/
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS + position;
        if (FSH_SUCCESS != FSH_ReadByteStream((uint8_t*) Addr, buff, numOfBytes))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR));
        }
    }
    else
    {
        _result = SDCardStorage_checkCardInserted();
        if (RETCODE_OK != _result)
        {
            return (_result);
        }
        SDCardStorage_GetPartitionName(partition, filename);
        _result = SDCardStorage_getFileSize((const char*) filename, &fileSize, FILE_OPERATION_READ);
        if (RETCODE_OK != _result)
        {
            return (_result);
        }
        if (0 > fileSize)
        {
            fileSize = 0;
        }
        if ((uint32_t) fileSize < position)
        {
            /* missing blocks => error */
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR));
        }
        /* Open the file */
        _fileSystemResult = f_open(&_fileReadHandle, (const char *) filename, FA_OPEN_EXISTING | FA_READ);
        if (FR_OK != _fileSystemResult)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_OPEN_ERROR));
        }
        /* append after position */
        _fileSystemResult = f_lseek(&_fileReadHandle, position);
        if (FR_OK != _fileSystemResult)
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR);
            goto read_close;
        }
        _fileSystemResult = f_read(&_fileReadHandle, buff, numOfBytes, &_bytesRead);
        if ((FR_OK != _fileSystemResult) || (_bytesRead != numOfBytes))
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR);
        }
        read_close:
        _fileSystemResult = f_close(&_fileReadHandle);
        if (FR_OK != _fileSystemResult)
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_CLOSE_ERROR);
        }
    }
    return _result;
}

/**
 * @brief   This function writes the data from the buffer to the specified partition of the storage medium. This intern uses FATFs 
 * 			File system API's to write the data to the storage, before calling this funtion ensure SDcard has been initialized properly.
 *			Also the caller of this has to ensure the valid position & no of bytes to write to the medium. Also
 *			partitions are mapped to a specific file names.
 * 
 * @note    Please make sure to initialize the storage medium before calling SDCard Storage_Write.  Also data is written to the SDCard when
 *			the Partition is FOTA_PARTITION_BACKUP or FOTA_PARTITION_DOWNLOAD. Data written to Internal Flash when the partition is
 *			FOTA_PARTITION_PRIMARY. Also FATfs file system is used for FOTA_PARTITION_BACKUP/FOTA_PARTITION_DOWNLOAD. So File names has been 
 *			fixed for the "firmware.bin"(FOTA_PARTITION_DOWNLOAD) and "firmware.bkp" for the FOTA_PARTITION_BACKUP partitions. while the primary 
 *			partition FOTA_PARTITION_PRIMARY access is based on address, which can be configured by the application through "BCDS_Fotaconfig.h".
 *			Also make sure Partition is erased before writing on to it & the primary partiton address is page aligned to the internal flash.
 *
 * @param[in] partition: Partition of the storage medium to write.
 *
 * @param[in] buff: Pointer to buffer containing valid data to be written to storage medium.
 *
 * @param[in] position: offset from the start of the partition.
 *
 * @param[in] numOfBytes: Number of bytes to write.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
static Retcode_T SDCardStorage_Write(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes)
{
    Retcode_T _result = RETCODE_OK;
    FRESULT _fileSystemResult;
    FIL _fileWriteHandle;
    UINT _bytesWritten;
    int32_t fileSize;
    uint32_t Addr = 0;
    uint8_t filename[FIRMWARE_FILE_NAME_MAX_SIZE] = { 0 };

    if (NULL == buff)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_NULL_POINTER));
    }
    if (FOTA_PARTITION_INVALID <= partition)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) FOTA_STORAGE_MEDIUM_INVALID));
    }
    if (FOTA_PARTITION_PRIMARY == partition)
    {
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS + position;

        if (FSH_SUCCESS != FSH_WriteByteStream((uint8_t*) Addr, buff, numOfBytes))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_WRITE_ERROR));
        }
    }
    else
    {
        _result = SDCardStorage_checkCardInserted();
        if (RETCODE_OK != _result)
        {
            return (_result);
        }
        SDCardStorage_GetPartitionName(partition, filename);
        _result = SDCardStorage_getFileSize((const char*) filename, &fileSize, FILE_OPERATION_WRITE);
        if (RETCODE_OK != _result)
        {
            return (_result);
        }
        if (0 > fileSize)
        {
            fileSize = 0;
        }
        if ((uint32_t) fileSize < position)
        {
            /* missing blocks => error */
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR));
        }
        /* Open the file */
        _fileSystemResult = f_open(&_fileWriteHandle, (const char *) filename, FA_OPEN_ALWAYS | FA_WRITE);
        if (FR_OK != _fileSystemResult)
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_OPEN_ERROR));
        }
        /* append after position */
        _fileSystemResult = f_lseek(&_fileWriteHandle, position);
        if (FR_OK != _fileSystemResult)
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR);
            goto download_close;
        }
        if (position < (uint32_t) fileSize)
        {
            /* truncate file to position */
            _fileSystemResult = f_truncate(&_fileWriteHandle);
            if (FR_OK != _fileSystemResult)
            {
                _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR);
                goto download_close;
            }
        }
        _fileSystemResult = f_write(&_fileWriteHandle, buff, numOfBytes, &_bytesWritten);
        if ((FR_OK != _fileSystemResult) || (numOfBytes != _bytesWritten))
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_OUT_OF_STORAGE);
        }
        download_close:
        _fileSystemResult = f_close(&_fileWriteHandle);
        if (FR_OK != _fileSystemResult)
        {
            _result = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_CLOSE_ERROR);
        }
    }
    return _result;
}

/**
 * @brief   This function Erases the specified partition data of the storage medium. This intern uses FATFs File system API's
 *			to erase the data form the the storage, before calling this funtion ensure SDcard has been initialized properly. Also
 *			partitions are mapped to a specific file names.
 * 
 * @note    Please make sure to initialize the storage medium before calling SDCardStorage_Erase. Data is Erased on the SDCard when
 *			the Partition is FOTA_PARTITION_BACKUP or FOTA_PARTITION_DOWNLOAD. Data is Erased on the Internal Flash when the partition is
 *			FOTA_PARTITION_PRIMARY. Also FATfs file system is used for FOTA_PARTITION_BACKUP/FOTA_PARTITION_DOWNLOAD. So File names has been 
 *			fixed for the "firmware.bin"(FOTA_PARTITION_DOWNLOAD) and "firmware.bkp" for the FOTA_PARTITION_BACKUP partitions. while the primary 
 *			partition FOTA_PARTITION_PRIMARY access is based on address, which can be configured by the application through "BCDS_Fotaconfig.h". Also 
 * 			Also make sure the primary partiton address is page aligned to the internal flash.
 *
 * @param[in] partition: partition to erase.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
static Retcode_T SDCardStorage_Erase(Fota_Partition_T partition)
{
    Retcode_T _result = RETCODE_OK;
    uint8_t filename[FIRMWARE_FILE_NAME_MAX_SIZE] = { 0 };
    uint32_t size = 0;
    uint32_t pages = 0;
    uint32_t Addr = 0;

    if (FOTA_PARTITION_INVALID <= partition)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) FOTA_STORAGE_MEDIUM_INVALID));
    }
    if (FOTA_PARTITION_PRIMARY == partition)
    {
        /*Flash erase required here*/
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS;

        size = FOTA_PARTITION_MAX_SIZE;
        pages = size / FLASH_PAGE_SIZE;
        if (0 != pages)
        {
            if (FSH_SUCCESS != FSH_Erase((uint32_t*) Addr, pages))
            {
                return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_ERASE_ERROR));
            }
        }
    }
    else
    {
        _result = SDCardStorage_checkCardInserted();
        if (RETCODE_OK != _result)
        {
            return (_result);
        }
        SDCardStorage_GetPartitionName(partition, filename);
        _result = SDCardStorage_deleteFile((const char*) filename);
    }
    return _result;
}

/**
 * @brief   This function copies the data from the source partition to the destination partition of the storage medium. This intern uses FATFs 
 *			File system API's to read & write data. before calling this funtion ensure SDcard has been initialized properly.
 *			The data copy happens only in the same storage medium of different partitions. Also partitions are mapped to a specific file names. 
 * 
 * @note    Please make sure to initialize the storage medium before calling SDCardStorage_Erase.  Also data is accessed(read/write)from the SDCard when
 *			the Partition is FOTA_PARTITION_BACKUP or FOTA_PARTITION_DOWNLOAD. Data is accessed(read/Wrie) from the Internal Flash when the partition is
 *			FOTA_PARTITION_PRIMARY. Also FATfs file system is used for FOTA_PARTITION_BACKUP/FOTA_PARTITION_DOWNLOAD. So File names has been 
 *			fixed for the "firmware.bin"(FOTA_PARTITION_DOWNLOAD) and "firmware.bkp" for the FOTA_PARTITION_BACKUP partitions. while the primary partition
 *			"FOTA_PARTITION_PRIMARY" access is based on address which can be configured by the application through "BCDS_Fotaconfig.h". 
 * 			Also make sure dest partition is erased before coping the data & the primary partiton address is page aligned to the internal flash.
 *
 * @param[in] dest: Destination Partition of the storage medium.
 *
 * @param[in] src: Source Partition of the storage medium.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
static Retcode_T SDCardStorage_Copy(Fota_Partition_T dest, Fota_Partition_T src)
{
    Retcode_T retval = RETCODE_OK;
    uint8_t srcfilename[FIRMWARE_FILE_NAME_MAX_SIZE] = { 0 };
    uint8_t data[FILE_PAGE_SIZE] = { 0 };
    uint32_t Addr = 0;
    uint32_t size = 0;
    uint32_t blocks = 0;
    uint16_t rem = 0;
    uint32_t offset = 0;
    int32_t fileSize = 0;

    if ((FOTA_PARTITION_INVALID <= dest) || (FOTA_PARTITION_INVALID <= src) || (src == dest))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) FOTA_STORAGE_MEDIUM_INVALID));
    }
    retval = SDCardStorage_checkCardInserted();
    if (RETCODE_OK != retval)
    {
        return (retval);
    }
    if ((FOTA_PARTITION_PRIMARY != src) && (FOTA_PARTITION_PRIMARY != dest))
    {
        SDCardStorage_GetPartitionName(src, srcfilename);
        retval = SDCardStorage_getFileSize((const char*) srcfilename, &fileSize, FILE_OPERATION_READ);
        if (RETCODE_OK != retval)
        {
            return (retval);
        }
        if (0 > fileSize)
        {
            fileSize = 0;
        }
        offset = 0;
        blocks = fileSize / FILE_PAGE_SIZE;
        rem = fileSize % FILE_PAGE_SIZE;
        while (blocks--)
        {
            retval = SDCardStorage_Read(src, data, offset, FILE_PAGE_SIZE);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            retval = SDCardStorage_Write(dest, data, offset, FILE_PAGE_SIZE);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            offset = offset + FILE_PAGE_SIZE;
            Addr = Addr + FILE_PAGE_SIZE;
        }
        if (rem)
        {
            retval = SDCardStorage_Read(src, data, offset, rem);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            retval = SDCardStorage_Write(dest, data, offset, rem);
        }
    }
    else if (FOTA_PARTITION_PRIMARY == src)
    {
        /* Get the content from Flash active memory*/
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS + FWCONTAINER_FIRMWARE_SIZE;

        if (FSH_SUCCESS != FSH_ReadByteStream((uint8_t*) Addr, (uint8_t*) &size, FWCONTAINER_FIRMWARE_SIZE_LEN))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR));
        }
        size += FWCONTAINER_HEADER_SIZE + FWCONTAINER_FOOTER_SIZE;
        blocks = size / FILE_PAGE_SIZE;
        rem = size % FILE_PAGE_SIZE;
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS;

        while (blocks--)
        {
            if (FSH_SUCCESS != FSH_ReadByteStream((uint8_t*) Addr, data, FILE_PAGE_SIZE))
            {
                return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_FILE_READ_ERROR));
            }
            retval = SDCardStorage_Write(dest, data, offset, FILE_PAGE_SIZE);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            offset = offset + FILE_PAGE_SIZE;
            Addr = Addr + FILE_PAGE_SIZE;
        }
        if (rem)
        {
            if (FSH_SUCCESS != FSH_ReadByteStream((uint8_t*) Addr, data, rem))
            {
                return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_FILE_READ_ERROR));
            }
            retval = SDCardStorage_Write(dest, data, offset, rem);
        }
    }
    else if (FOTA_PARTITION_PRIMARY == dest)
    {
        Addr = FOTA_PRIMARY_PARTTITION_START_ADDRESS;
        SDCardStorage_GetPartitionName(src, srcfilename);
        retval = SDCardStorage_getFileSize((const char*) srcfilename, &fileSize, FILE_OPERATION_READ);
        if (RETCODE_OK != retval)
        {
            return (retval);
        }
        if (0 > fileSize)
        {
            fileSize = 0;
        }
        offset = 0;
        blocks = fileSize / FILE_PAGE_SIZE;
        rem = fileSize % FILE_PAGE_SIZE;
        while (blocks--)
        {
            retval = SDCardStorage_Read(src, data, offset, FILE_PAGE_SIZE);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            if (FSH_SUCCESS != FSH_WriteByteStream((uint8_t*) Addr, data, FILE_PAGE_SIZE))
            {
                return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_FILE_WRITE_ERROR));
            }
            offset = offset + FILE_PAGE_SIZE;
            Addr = Addr + FILE_PAGE_SIZE;
        }
        if (rem)
        {
            retval = SDCardStorage_Read(src, data, offset, rem);
            if (RETCODE_OK != retval)
            {
                return retval;
            }
            if (FSH_SUCCESS != FSH_WriteByteStream((uint8_t*) Addr, data, rem))
            {
                return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_FILE_WRITE_ERROR));
            }
        }
    }
    return retval;
}

#endif /* BCDS_FEATURE_MEDIUM_SDCARD */
/* global functions ********************************************************* */

/** ************************************************************************* */
