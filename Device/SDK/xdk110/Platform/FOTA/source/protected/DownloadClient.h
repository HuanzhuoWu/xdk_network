/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @brief
 *  This module provide the control of firmware downloading,status notify to the application
 *   and provide the common interfaces between CoAP and application Layer
 */


#ifndef DOWNLOADCLIENT_H_
#define DOWNLOADCLIENT_H_

/* public interface declaration ********************************************* */
#include "BCDS_Retcode.h"

/* public function prototype declarations */

/**
 * @brief     This is application callback to be called for informing send message
 *            status.
 *
 * @param[in] callablePntr
 *               The parameter callablePntr generally refers to the Callable_T element
 *               carrying this functions. This pointer is passed into the function in order to
 *               allow the retrieval of the context structure using the macro CALLABLE_GET_CONTEXT().
 *
 * @param[in] status
 *               status of sending message.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
static retcode_t SentCallback(Callable_T *callablePntr, retcode_t status);

/**
 * @brief        This function is called to push a request from a CoAP client. This function
 *               prepares the message for further processing before passing it to the lower
 *               layers.
 *
 * @param[in] dlCtx
 *               Pointer to a DownloadClient_Context_T structure that holds the necessary download information.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_COAP_CLIENT_INIT_REQ_ERROR, if message initialize is failed
 *               RC_COAP_SENDING_ERROR, if CoAP client request is failed for sending
 *               RC_COAP_SERIALIZE_ERROR otherwise.
 */
static retcode_t SendGetRequest(DownloadClient_Context_T* dlCtx);

/**
 * @brief   This function is called to serialize outgoing message and sent the application
 *          request.
 *
 * @param[in] msgPntr
 *              Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] dlCtx
 *               Pointer to a DownloadClient_Context_T structure that holds the necessary download information.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_COAP_SERIALIZE_ERROR otherwise.
 */
static retcode_t SerializeGetRequest(DownloadClient_Context_T* dlCtx, Msg_T* msgPntr);

/**
 * @brief   This function is called to serialize a option in the incoming message. It
 *          calculates the delta, length and the position of option value from passed
 *          object of type CoapOption_T and the serializing context and populate the
 *          buffer with these values.
 *
 * @param[in] serializer_ptr
 *                  Reference to a CoapSerializer_T object which identifies the current
 *                  instance of serializing context which should be used for serializing. It has
 *                  to be a valid pointer.
 *
 * @param[in] msgPntr
 *                  Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] targetLocation
 *                   buffer contain the target path of downloading,.
 *
 * @retval  retcode_t
 *              RC_OK, if successful
 *              RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
static retcode_t SerializeUriPath(CoapSerializer_T* serializer, Msg_T* msgPntr, const char* targetLocation);

/**
 * @brief     This is the application callback to be called for a received response
 *            message for this request. If the application does not expect a response for
 *            this request it should be set to NULL.
 *
 * @param[in] coapSession
 *                 Reference to a CoapSerializer_T object which identifies the current
 *                 instance of serializing context which should be used for serializing. It has
 *                 to be a valid pointer.
 *
 * @param[in] msgPntr
 *              Reference to a Msg_T object which represents current incoming message.
 *
 * @param[in] status
 *              status of send request or outgoing message
 *
 * @retval retcode_t
 *              RC_OK, if successful
 *              RC_COAP_END_OF_OPTIONS, if it detects end of option
 *              RC_COAP_ENCODING_ERROR, if it detects Payload error.
 *              RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
static retcode_t ResponseCallback(CoapSession_T *coapSession, Msg_T *msgPntr, retcode_t status);

/**
 * @brief   This function called to validated the download Block.
 *
 * @param[in] blockOptionValue
 *                   sent request block option.
 *
 * @param[in] blockValue
 *                   received block option .
 *
 * @param[in] payloadLen
 *                   length of the payload of the Download message.
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static bool ValidBlockOption(uint32_t blockOptionValue, uint32_t blockValue, uint8_t payloadLen);

/**
 * @brief   This function called to calculate length of string or URI, until
 *          specified delimiter.
 *
 * @param[in] str
 *              data pointer to calculate length of the character.
 *
 * @retval  uint16_t
 *               The length of the string.
 */
static uint16_t LengthUntildelimiter(const uint8_t* str, const char delimiter);

/**
 * @brief   This function converts a given string to an IP address, and
 *          stores the result in the given buffer.
 *
 * @param[in] str
 *              A string that contains an IP address in dotted-quad notation for IPv4.
 *
 * @param[out] ipAddress
 *               Reference to an IPv4 address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @retval  uint8_t
 *            return zero, if failure
 *            otherwise,Total number of characters written,.
 */
static uint8_t StringToIpv4Addr(char const *str, Ip_Address_T *ipAddress);

/**
 * @brief   This function parsing the a given string to an IP address, and
 *          port address.result stores in the given buffer.
 *
 * @param[in] ipAndPort
 *              A string that contains an IP,Port address in dotted-quad notation.
 *
 * @param[out] ipAddress
 *               Reference to an IPv4 address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @param[out] port
 *               Reference to an port address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @retval  uint8_t
 *            return zero, if failure
 *            otherwise,Total number of characters written,.
 */
static uint8_t ParseIpAndPort(const char* ipAndPort, Ip_Address_T *ipAddress, Ip_Port_T *port);

/**
 * @brief   This function compare the parameter1 with parameter2 until second string length and then
 *          validate the CoAP message is secured or non-secured.
 *
 * @param[in] url
 *              A string that contains an IP,Port,URi path address in dotted-quad notation.
 *
 * @param[in] scheme
 *              A string contains fixed characters.
 *
 * @param[out] secured
 *             Pointer to secured/non-secured variable passed by COAP
 *
 * @retval  uint8_t
 *            return zero, if failure
 *            otherwise,total number of characters read,.
 */
static uint8_t ParseScheme(const char *url, const char* scheme, bool* secured);

/**
 * @brief   This function validate the CoAp message and parsing IP,Port address from input parameter and store it to particular
 *          output parameter end. its validate the CoAP message is secured or non-secured.
 *
 * @param[in] url
 *              Pointer to a buffer, it contains IP ,port address and URI path
 *
 * @param[in] scheme
 *              Pointer to a data with fixed characters
 *
 * @param[out] ipAddress
 *               Reference to an IPv4 address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @param[out] port
 *               Reference to an port address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @param[out] secured
 *             Pointer to secured/non-secured variable passed by COAP
 *
 * @retval  uint8_t
 *            return zero, if failure
 *            otherwise,total number of characters read,.
 */
static uint8_t ParseUrl(const char *url, const char* scheme, Ip_Address_T *address, Ip_Port_T *port, bool* secured);

/**
 * @brief   This function is called to parsing or serialize the IP,Port Address from DownloadClient_Context_T
 *          structure and validating.
 *
 * @param[in] dlCtx
 *              Pointer to a DownloadClient_Context_T structure that holds the necessary download information.
 *
 * @param[in] url
 *              Pointer to a buffer, it contains IP ,port address and URI path.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_APP_ERROR otherwise.
 */
static retcode_t ParseDestinationUri(DownloadClient_Context_T* dlCtx, const char* uri);

#endif /* DOWNLOADCLIENT_H_ */

