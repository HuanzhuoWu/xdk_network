/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @brief
 *  This module will handle, all FOTA related activities like Firmware Download, Fota resources State
 *  change, firmware storage and etc.
 */

#ifndef FOTASTATEMACHINE_H_
#define FOTASTATEMACHINE_H_

#include <Serval_Coap.h>
#include "BCDS_DownloadClient.h"
/*  local interface declaration declaration */

/* local type and macro definitions */
#define FOTA_LWM2M_STATUS_MASK                  (UINT8_C(0x0F))      /* lwm2m status mask */
#define FOTA_LWM2M_RESULT_MASK                  (UINT32_C(0x0F00))   /* lwm2m result mask */
#define FOTA_BLOCK_MAX_RETRY                    (UINT8_C(4))       /* maximum Retry Count for  FOTA Block request */
#define FOTA_SHIFT_BY_8                         (UINT8_C(8))

/* URI path objects details */
#define FOTA_URIPATH_STATE_RESOURCE_INDEX       (UINT8_C(3))
#define FOTA_URIPATH_UPDATE_RESOURCE_INDEX      (UINT8_C(5))

/* Resource ID Definition for firmware update Object */
#define FOTA_RESOURCE_PACKAGE                    (UINT8_C(0))
#define FOTA_RESOURCE_PACKAGE_URI                (UINT8_C(1))
#define FOTA_RESOURCE_UPDATE                     (UINT8_C(2))
#define FOTA_RESOURCE_UPDATE_STATE               (UINT8_C(3))
#define FOTA_RESOURCE_UPDATE_SUPPORT_OBJ         (UINT8_C(4))
#define FOTA_RESOURCE_UPDATE_RESULT              (UINT8_C(5))

#define FOTA_TIMEOUT_NOTIFICATION_ENABLE        (UINT8_C(1))
#define FOTA_TIMEOUT_NOTIFICATION_DISABLE       (UINT8_C(0))


/**
 * @brief   Fota Resource package definition which will check the parser pointer.
 *
 * @param[in] serializer_ptr
 *                  Reference to a CoapSerializer_T object which identifies the current
 *                  instance of serializing context which should be used for serializing. It has
 *                  to be a valid pointer.
 *
 * @param[in] parser_ptr
 *                  Reference to a parser_ptr object which represents current outgoing message.
 *
 * @retval  retcode_t
 *              RC_OK, if successful
 *              RC_LWM2M_BAD_REQUEST otherwise.
 *
 */
static retcode_t FotaPackageResource(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr);

/**
 * @brief   Notification callback for firmware upgrade to LightWeight M2M or application.
 *
 * @param[in] serializer_ptr
 *                  Reference to a CoapSerializer_T object which identifies the current
 *                  instance of serializing context which should be used for serializing. It has
 *                  to be a valid pointer.
 *
 * @param[in] parser_ptr
 *                  Reference to a parser_ptr object which represents current outgoing message.
 *
 * @retval  retcode_t
 *              RC_OK, if successful
 *              RC_LWM2M_BAD_REQUEST otherwise.
 *
 */
static retcode_t FotaStateMachine_Update(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr);

/**
 * @brief   This function is used to get URI path or new download location from server and
 *          parsing it to Download the firmware.
 *
 * @param[in] serializer_ptr
 *                  Reference to a CoapSerializer_T object which identifies the current
 *                  instance of serializing context which should be used for serializing. It has
 *                  to be a valid pointer.
 *
 * @param[in] parser_ptr
 *                  Reference to a parser_ptr object which represents current outgoing message.
 *
 * @retval  retcode_t
 *              RC_OK, if successful
 *              RC_LWM2M_PARSING_ERROR, if data could not be parsed properly.
 *              RC_LWM2M_METHOD_NOT_ALLOWED, if invalid pointer passed.
 *
 */
static retcode_t FotaStateMachine_UriDownload(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr);

/**
 * @brief stores the firmware update coap blocks in the DOWNLOAD partition of the storage medium & increments the block counter .
 *
 * @param[in]  status is the current CoAP download status
 *
 * @param[out] data buffer pointer to the coap data to be stored in the storage medium.
 *
 * @param[in] size of the coap data to be stored in the storage medium.
 *
 * @param[in] blockOptionValue is the CoAP block offset value from the block header
 *
 * @retval  RETCODE_SUCCESS if the store was successful, else an error code is returned.
 */
static Retcode_T StoreBlock(DownloadClient_Status_T status, const uint8_t* data, uint16_t size, uint32_t offset);

/**
 * @brief This function is used to notify the stack that a FOTA State and Result has changed.
 *
 * @return Retcode_T
 *          RETCODE_OK, if successful.
 *          RETCODE_FOTA_FAILED_RESULT_AND_STATE_UPDATE on failure to update to serval stack
 */
static Retcode_T FotaStateAndResultNotify(void);

/**
 * @brief   This function is used to reset the FOTA to default state with all default parameters.
 *
 * @retval  retcode_t
 *              RETCODE_OK, if successful
 *              RETCODE_FAILURE, if cannot continue the download.
 */
static Retcode_T FotaStateMachine_ResetCredentials(void);

#endif /* FOTASTATEMACHINE_H_ */

