/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *
 *  This module will handle, all FOTA related activities like Firmware Download, Fota resources State
 *  change, firmware storage and etc.
 */

#include "BCDS_FotaInfo.h"
#undef BCDS_MODULE_ID
#define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_FOTASTATEMACHINE

#include "BCDS_Fota.h"

#if BCDS_FEATURE_STATEMACHINE
/* system header files */
//lint -e49 error in standard libraries suppressed#include <stdint.h>
//lint -e309 error in third party libraries suppressed BCDS_Assert.h
//lint -esym(956,*) /* Suppressing Non const, non volatile static or external variable */
/* own header files */
#include "BCDS_FotaStateMachine.h"
#include "FotaStateMachine.h"

/* additional interface header files */
#ifndef CONFIG_EXTERNAL_NVM
#include "BCDS_NVMConfig.h"
#endif
#include "BCDS_FWContainer.h"
#include "Serval_CoapClient.h"
#include "BCDS_CoapBlockwise.h"
#include "BCDS_Retcode.h"
#include "BCDS_FotaStorageAgent.h"
#include "BCDS_ValidationAgent.h"
#include "FirmwareValidation.h"
#include "BCDS_FotaConfig.h"

#ifdef CONFIG_EXTERNAL_NVM
#define WriteFotaContext(id, parm, size) FotaStateMachine_WriteFotaContext(id, parm, size)
#define FlushFotaContext() RETCODE_OK
#define ReadFotaContext(id, parm, size) FotaStateMachine_ReadFotaContext(id, parm, size)
#define FOTA_FIRMWARE_PACKAGE_URL_SIZE FOTA_MAX_PACKAGE_URL

#else

static struct NVM_Item_S NvmItemCurrentState;
static struct NVM_Item_S NvmItemNewState;
static struct NVM_Item_S NvmItemNextBlock;
static struct NVM_Item_S NvmItemDownloadInProgress;
static struct NVM_Item_S NvmItemFirmwarePackageUrl;
static struct NVM_Item_S NvmItemCurrentResult;
static const struct NVM_Item_S *NvmItems[] = {
        &NvmItemCurrentState,
        &NvmItemNewState,
        &NvmItemNextBlock,
        &NvmItemDownloadInProgress,
        &NvmItemFirmwarePackageUrl,
        &NvmItemCurrentResult
};
#define WriteFotaContext(id, parm, size) NVM_Write(&NVMUser, *(NvmItems[id]), parm, size)
#define FlushFotaContext() NVM_Flush(&NVMUser)
#define ReadFotaContext(id, parm, size) NVM_Read(&NVMUser,*(NvmItems[id]),parm, size)
#define FOTA_FIRMWARE_PACKAGE_URL_SIZE NVM_ITEM_ID_FIRMWARE_PACKAGE_URL_SIZE
#endif

/* StorageMedium Handle to be updated in FotaStateMachine_Init */
static FotaStorageAgentPtr_T FotaStorageAgentHandle = NULL;
/* This Notification is used for the Application to do the tasks before the soft reset*/
static FotaStateMachine_Notification_T FotaNotificationCall = NULL;
static uint8_t TimeoutBlock; /* Flag to identify the status of FOTA Downloading or Notification Timeout Enabled or Disabled */
static char PackageUrl[FOTA_MAX_PACKAGE_URL];
/* Local variable declaration */
static volatile uint8_t FotaAsynchronousCallFlag;
static volatile FotaStateMachine_State_T FOTA_State = FOTA_INVALID; /* instance created for FOTA_state and assigned default state */
static uint8_t CoapBlock[FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY * FOTA_COAP_BLOCK_SIZE];
static uint8_t HeaderValidationCheck = FALSE;
static bool SendFailureFlag = false; /* Flag to intimate application about run time send failure */

/**
 * @brief   States the URI path.
 *
 * @param[in] instance of URI path structure for FOTA state
 *
 */
static Lwm2m_URI_Path_T stateUriPath =
        {
        FOTA_URIPATH_OBJECT_INSTANCE,
        FOTA_URIPATH_OBJECT_INDEX,
                -1 }; /* instance of URI path structure for FOTA state */

/**
 * LWM2M Firmware Object resourcesinfo are packed in this array, this called by Registration/Firmware updation
 */
FotaStateMachine_FW_UpdateObj_T FotaStateMachine_FotaResource =
        {
                { FOTA_RESOURCE_PACKAGE, LWM2M_DYNAMIC(FotaPackageResource) }, /* This is non-readable! */
                { FOTA_RESOURCE_UPDATE, LWM2M_FUNCTION(FotaStateMachine_Update) },
                { FOTA_RESOURCE_PACKAGE_URI, LWM2M_DYNAMIC(FotaStateMachine_UriDownload) | LWM2M_WRITE_ONLY },
                { FOTA_RESOURCE_UPDATE_STATE, LWM2M_INTEGER((int) FOTA_IDLE) },
                { FOTA_RESOURCE_UPDATE_SUPPORT_OBJ, LWM2M_BOOL(true) },
                { FOTA_RESOURCE_UPDATE_RESULT, LWM2M_INTEGER(FOTASTATEMACHINE_FW_DEFAULT) },
        };
/**
 * @brief This function is will trigger the FOTA update process and reboot the Node.
 *
 * @return Retcode_T
 *           RETCODE_OK, if successful.
 *           RETCODE_FAILURE otherwise.
 */
static Retcode_T PrepareAndNofityUpdate(void)
{
    Retcode_T returnCode = RETCODE_OK;

    returnCode = FotaStateMachine_ResetCredentials();
    if (RETCODE_OK == returnCode)
    {
        /* Signal to bootloader that a new firmware package has been received */
        uint8_t isNewFirmware = 1;
        returnCode = WriteFotaContext(FOTA_CONTEXT_NEW_FIRMWARE, (uint32_t* ) &isNewFirmware, sizeof(isNewFirmware));
    }
    if (RETCODE_OK == returnCode)
    {
        /* Set FOTA State in updating before reboot */
        FOTA_State = FOTA_UPDATING;
        /* Set the current FOTA State in user page */
        returnCode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
    }
    if (RETCODE_OK == returnCode)
    {
        /* Save changes to flash */
        returnCode = FlushFotaContext();
    }
    if (RETCODE_OK == returnCode)
    {
        returnCode = FotaNotificationCall(FOTA_State, FOTA_STATE_NOTFY);
    }
    return returnCode;
}

/* local function prototype declarations */
/**
 * @brief This API handling retry block activities.
 *
 */
static inline void FotaDownloadRetry(void)
{
    /*
     1.increment some counter to abort eventually
     2.Need to set connection lost once we give up by calling setUpdateResult(LWM2M_FW_CONNECTION_LOST);
     3.check if we need to validate original state first
     */

    FOTA_State = FOTA_DOWNLOADING_READY_FOR_REQUEST;
}

/**
 * @brief Initial idle state of firmware download.
 *
 */
static inline void SetFotaStateIdle(void)
{
    FOTA_State = FOTA_IDLE;
}

/**
 * @brief notification for start firmware downloading.
 *
 */
static inline void StartFotaDownload(void)
{
    FOTA_State = FOTA_DOWNLOADING_NOTIFY;
}

/**
 * @brief notification to abort FOTA activity.
 *
 */
static inline void AbortFotaDownload(void)
{
    FOTA_State = FOTA_FAILED_NOTIFY;
}

/**
 * @brief notification for FOTA activity done.
 *
 */
static inline void FotaDownloadFinished(void)
{
    FOTA_State = FOTA_DOWNLOADED_NOTIFY;
}

/**
 * @brief notification for wait for the previous request responds.
 *
 */
static inline void FotaDownloadRequestSent(void)
{
    FOTA_State = FOTA_DOWNLOADING_WAITING_FOR_RESPONSE;
}

/**
 * @brief notification for next block downloading.
 *
 */
static inline void FotaDownloadResponseReceived(void)
{
    FOTA_State = FOTA_DOWNLOADING_READY_FOR_REQUEST;
}

/**
 * @brief notification to notify updating.
 *
 */
static inline void FotaStartUpdating(void)
{
    FOTA_State = FOTA_UPDATING_NOTIFY;
}

/**
 * @brief get the current state of Firmware Downloading.
 *
 * @return int32_t
 *         current state of firmware downloading.
 *
 */
static inline int32_t GetFirmwareDownloadState(void)
{
    return ((int32_t) FOTA_State & FOTA_LWM2M_STATUS_MASK);
}

/**
 * @brief set the current state of Firmware Downloading in the Fota object state resource.
 *
 *@param[in] updateResult setting the current state of firmware downloading.
 *
 */
static void UpdateFirmwareDownloadStatus(int32_t DownloadState)
{
    FotaStateMachine_FotaResource.state.data.i = DownloadState;
}

/**
 * @brief set the current state of Firmware update result in the Fota object result resource.
 *
 * @param[in] updateResult setting the updated result.
 *
 *
 */
static void UpdateFotaResultResource(uint8_t updateResult)
{
    FotaStateMachine_FotaResource.updateResult.data.i = (int32_t) updateResult;
}

/**
 * @brief get the current state of Firmware update result in the Fota object result resource.
 *
 * @return int32_t
 *         current state of firmware update result.
 *
 */
static int32_t GetFotaResultResource(void)
{
    return FotaStateMachine_FotaResource.updateResult.data.i;
}

/* The function description is available in the Private header */
static retcode_t FotaPackageResource(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr)
{
    BCDS_UNUSED(serializerPntr);

    if (parserPntr == NULL)
    {
        /* ignore this, it is write-only */
        return RC_OK;
    }
    else
    {
        return RC_LWM2M_METHOD_NOT_ALLOWED;
    }
}

/**
 * @brief   Parses a string from an incoming lwm2m message.
 *          The function takes care to handle TLV decoding if necessary
 *
 * @param[in] parserPntr
 *          Pointer to the parser structure (typically obtained in the callback)
 *
 * @param[in] packageUrl
 *          Pointer to the buffer for the storing the extracted package url
 *
 * @retval RC_OK on success
 *         RC_LWM2M_PARSING_ERROR if the data could not be parsed properly
 */
static retcode_t ExtractLocation(Lwm2mParser_T *parserPntr, char *Url)
{
    StringDescr_T descr;
    retcode_t rc = Lwm2mParser_getString(parserPntr, &descr);

    if (RC_OK != rc)
    {
        return rc;
    }
    else if ((0 == descr.length) && (FOTA_UPDATING != (FotaStateMachine_State_T) GetFirmwareDownloadState())) /* Empty URI Triggered from Server For Aborting the FOTA Activity */
    {
        AbortFotaDownload();
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_DEFAULT);

        /* Notifying application that an empty URI is received.
         * In-case of any failure from callback,
         * there is no need to do anything from the FOTA package.
         */
        (void) FotaNotificationCall(FOTA_State, FOTA_EMPTY_URI_RECEIVED);

        return RC_OK; /* if not return, its get debug assert while parsing */
    }
    else if (descr.length >= (int) FOTA_MAX_PACKAGE_URL)
    {
        return RC_APP_ERROR;
    }
    else if (FOTA_IDLE == (FotaStateMachine_State_T) GetFirmwareDownloadState())
    {
        memset(Url, '\0', FOTA_MAX_PACKAGE_URL);
        StringDescr_copySegment(&descr, Url, 0, FOTA_MAX_PACKAGE_URL);
    }
    return RC_OK;
}

/**
 * @brief   This function is used to read Fota State, Fota Resut and Fota FW acceptance data from NVM.
 *
 * @param[in] FotaResData
 *           Reading the FotaResData information from the NVM at power-on
 *
 * @param[in] FWacceptData
 *           Reading the FWacceptData information from the NVM
 * @return Retcode_T
 *           RETCODE_OK, if successful.
 *           RETCODE_FAILURE otherwise.
 *
 */
static Retcode_T GetFotaNVMData(uint8_t* FotaResData, uint8_t* FWacceptData)
{
    Retcode_T returnCode = RETCODE_OK;
    /* Retrieve the FOTA State from UserPage after POR */
    returnCode = ReadFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, 4U);
    if ((RETCODE_OK == returnCode) && (FOTA_INVALID != FOTA_State) && (FOTA_STATE_NOT_IN_USERPAGE != FOTA_State))
    {
        /* Retrieve the FOTA Result from UserPage after POR */
        returnCode = ReadFotaContext(FOTA_CONTEXT_CURRENT_RESULT, (uint32_t* )FotaResData, sizeof(uint8_t));
        if (RETCODE_OK == returnCode)
        {
            /* Retrieve the Software quality flag from UserPage after New Firmware Upgrade */
            returnCode = ReadFotaContext(FOTA_CONTEXT_NEW_FIRMWARE, (uint32_t* )FWacceptData, sizeof(uint8_t));
        }
    }
    else
    {
        if (RETCODE_OK == returnCode)
        {
            returnCode = RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_FAILED_NVM_READ_CREDENTIALS);
        }
    }
    return returnCode;
}

/**
 * @brief   This function is used to update the Fota result resource based on the container header validation.
 * 
 * @param[in] HdrValidationStatus header validation status 
 * @return void
 *
 */
static void HdrValidationFotaResultUpdate(uint32_t HdrValidationStatus)
{
    switch (HdrValidationStatus)
    {
    case FOTA_RETCODE_CRC_ERROR:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_CRC_FAILED);
        break;

    case FOTA_RETCODE_FIRMWARE_VERSION_FAIL:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UNSUPPORTED_TYPE);
        break;

    case FOTA_RETCODE_FIRMWARE_SIZE_FAIL:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_OUT_OF_STORAGE);
        break;

    case FOTA_RETCODE_CONTAINER_READ_FAIL:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        break;

    default:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        break;
    }
}

/* The function description is available in the Private header */
static Retcode_T StoreBlock(DownloadClient_Status_T status, const uint8_t* data, uint16_t size, uint32_t blockOptionValue)
{
    uint32_t currentBlockNumber = 0;
    Retcode_T retVal = RETCODE_OK;
    Retcode_T nvmStatus = RETCODE_OK;
    uint32_t AddressOffsetToFlash = 0;
    uint32_t runningBlockOffset = 0;
    uint16_t indx = 0;
    uint32_t blocksInRam = 0;

    if ((NULL == data) || (NULL == FotaStorageAgentHandle))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_NULL_POINTER));
    }
    if (0 == size)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_INVALID_PARAM));
    }
    currentBlockNumber = CoapBlockwise_GetBlockNumber(blockOptionValue);

    blocksInRam = (currentBlockNumber % FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY);

    if (blocksInRam < FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY)
    {
        indx = ((blocksInRam) * FOTA_COAP_BLOCK_SIZE);
        memcpy(&CoapBlock[indx], data, size);
    }
    if ((blocksInRam == (FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY - 1)) || (status == DL_DONE))
    {
        uint32_t dataToCopy = 0;
        runningBlockOffset = CoapBlockwise_GetBlockOffset(blockOptionValue);
        if (blocksInRam == (FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY - 1))
        {
            dataToCopy = ((blocksInRam + 1) * FOTA_COAP_BLOCK_SIZE);
        }
        else
        {
            dataToCopy = ((blocksInRam * FOTA_COAP_BLOCK_SIZE) + size);
        }
        /*To calculate the actual offset to write in the storage. CoapBlockwise_GetBlockOffset gives offset based on the FOTA_COAP_BLOCK_SIZE so if
         If we store the blocks(FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY) in ram then actual offset is calculated by the beelow calculation*/
        AddressOffsetToFlash = (runningBlockOffset - (dataToCopy - size));
        retVal = FotaStorageAgentHandle->Write(FOTA_PARTITION_DOWNLOAD, (uint8_t *) CoapBlock, AddressOffsetToFlash, dataToCopy);
        //write next block only on DL_DATA not on DL_DONE
        if (RETCODE_OK == retVal)
        {
            uint32_t NextBlockNumber = currentBlockNumber + 1;
            nvmStatus = WriteFotaContext(FOTA_CONTEXT_NEXT_BLOCK_NO, &NextBlockNumber, sizeof(NextBlockNumber));
            if (RETCODE_OK == nvmStatus)
            {
                nvmStatus = FlushFotaContext();
            }
        }
        if (RETCODE_OK != nvmStatus)
        {
            retVal = RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_UNEXPECTED_BEHAVIOR);
        }
    }
    return retVal;
}
/**
 * @brief stores the firmware update coap blocks in the DOWNLOAD partition of the storage medium & increments the block counter .
 *
 * @param[out] data buffer pointer to the coap data to be stored in the storage medium.
 *
 * @param[in] size of the coap data to be stored in the storage medium.
 *
 * @param[in] blockOptionValue is the CoAP block option value from the block header
 */
static void StoreData(DownloadClient_Status_T status, const uint8_t* data, uint16_t size, uint32_t blockOptionValue)
{
    Retcode_T retVal = RETCODE_OK;
    Retcode_T nvmStatus = RETCODE_OK;
    uint32_t currentBlockNumber = 0;
    uint32_t BlockOffset = CoapBlockwise_GetBlockOffset(blockOptionValue);

    retVal = StoreBlock(status, data, size, blockOptionValue);
    currentBlockNumber = CoapBlockwise_GetBlockNumber(blockOptionValue);
    /* FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY - no of received coap blocks that is kept in RAM buffer & updated once in storage medium, which is configured by the application.
     * This check ensures that Firmware header Validation is done only when the header data is written into storage medium.*/
    if ((retVal == RETCODE_OK) && (FALSE == HeaderValidationCheck) && ((BlockOffset + size) >= FWCONTAINER_HEADER_SIZE) && (currentBlockNumber >= (FOTA_BLOCK_STORAGE_UPDATE_FREQUENCY - 1)))
    {
        retVal = ValidationAgent_VerifyHeader(FotaStorageAgentHandle, FOTA_PARTITION_DOWNLOAD);
        HeaderValidationCheck = TRUE;
    }
    if (RETCODE_OK != retVal)
    {
        retVal = Retcode_getCode(retVal);
        /* getting the error code to update server */
        HdrValidationFotaResultUpdate(retVal);
        AbortFotaDownload();
        UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
        return;
    }
    if (DL_DATA == status)
    {
        FotaDownloadResponseReceived();
    }
    else /* DL_DONE */
    {
        /* Perform the state change */
        FotaDownloadFinished();

        uint8_t DownloadInProgress = UINT8_C(0);
        /* Reset the download in progress flag in user page to zero */
        nvmStatus = WriteFotaContext(FOTA_CONTEXT_DOWNLOAD_IN_PROGRESS, (uint32_t* )&DownloadInProgress, sizeof(DownloadInProgress));
        if (RETCODE_OK != nvmStatus)
        {
            Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_FAILED_NVM_WRITE_DOWNLOAD_IN_PROGRESS));
        }
        /* Set the current FOTA State in user page */
        nvmStatus = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
        if (RETCODE_OK != nvmStatus)
        {
            Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_FAILED_NVM_WRITE_FOTA_CURRENT_STATE));
        }
        /* Save changes to flash */
        nvmStatus = FlushFotaContext();
        if (RETCODE_OK != nvmStatus)
        {
            Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_FAILED_NVM_FLUSH));
        }
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_DEFAULT);
    }
}

/**
 * @brief Call back function used by the Constrained Application Protocol (CoAP)
 * download client.
 * @details This function gets called by the CoAP download client whenever
 * the download status changes.
 * @param status is the current CoAP download status
 * @param data is a pointer on the receive buffer containing received data
 * @param size is the size of the data block that was received
 * @param blockOptionValue is the CoAP block option value from the block header
 * of the received CoAP block.
 */
static void OnFileDownloadStatusChanged(DownloadClient_Status_T status, const uint8_t* data, uint16_t size, uint32_t blockOptionValue)
{
    /* To receive any response, the current state should be WAITING_FOR_RESPONSE */
    if (FOTA_DOWNLOADING_WAITING_FOR_RESPONSE != FOTA_State)
    {
        /** @TODO:This functionality will enable once, ServalStack has fix, their multi-thread issue  */
        /*
         AbortFotaDownload();
         updateFirmwareDownloadStatus(GetFirmwareDownloadState());
         */
        FotaAsynchronousCallFlag = 1;
        return;
    }

    switch (status)
    {
    case DL_NOTFOUND:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_INVALID_URI);
        AbortFotaDownload();
        break;

    case DL_TIMEOUT:
        TimeoutBlock = FOTA_TIMEOUT_NOTIFICATION_ENABLE;
        FotaDownloadRetry();
        Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_DL_TIMEOUT));
        break;

    case DL_SEND_ERROR:
        /*  Send has failed and should be notified to application
         * to handle the radio related errors like signal loss
         */
        SendFailureFlag = true;
        FotaDownloadRetry();
        Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_DL_SEND_ERROR));
        break;

    case DL_ERROR:
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        AbortFotaDownload();
        break;

    case DL_DATA:
        case DL_DONE:
        StoreData(status, data, size, blockOptionValue);
        break;

    case DL_REQUEST_SENT:
        break;

    default:
        break;
    }
    UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
}

/* The function description is available in the Private header */
static Retcode_T FotaStateAndResultNotify(void)
{
    if (RC_OK != Lwm2mReporting_resourceChanged(&stateUriPath))
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) FOTA_FAILED_RESULT_AND_STATE_UPDATE);
    }
    return RETCODE_OK;
}

/**
 * @brief This function is used to set the fota update result resource and notify the stack that a FOTA Result has changed.
 *
 * @return Retcode_T
 *          RETCODE_OK, if successful.
 *          FOTA_RETCODE_RESOURCE_CHANGED_ERROR on failure to update to serval stack
 */
static Retcode_T SetFotaUpdateResult(uint8_t NewUpdateResult)
{
    uint8_t OldUpdateResult;
    Retcode_T ReturnValue = RETCODE_OK;

    OldUpdateResult = FotaStateMachine_FotaResource.updateResult.data.i;

    if (OldUpdateResult != NewUpdateResult)
    {
        UpdateFotaResultResource(NewUpdateResult);
        ReturnValue = FotaStateAndResultNotify();
    }
    return ReturnValue;
}

/* The function description is available in the Private header */
static retcode_t FotaStateMachine_Update(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr)
{
    BCDS_UNUSED(parserPntr);
    BCDS_UNUSED(serializerPntr);
    Retcode_T nvmStatus = RETCODE_OK;

    if (FOTA_DOWNLOADED != FOTA_State)
    {
        return RC_LWM2M_BAD_REQUEST;
    }

    FotaStartUpdating();
    /* Set the current FOTA State in user page */
    nvmStatus = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
    if (RETCODE_OK == nvmStatus)
    {
        /* Save changes */
        nvmStatus = FlushFotaContext();
    }
    if (RETCODE_OK != nvmStatus)
    {
        return RC_PLATFORM_ERROR;
    }
    UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
    return (RC_OK);
}

static Retcode_T PrepareFotaDownload(void)
{
    Retcode_T retcode = RETCODE_OK;
    int32_t Result = 0;
    HeaderValidationCheck = 0;

    retcode = FotaStorageAgentHandle->Erase(FOTA_PARTITION_DOWNLOAD);
    if (RETCODE_OK != retcode)
    {
		AbortFotaDownload();
        UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
        if (RETCODE_OK != SetFotaUpdateResult(FOTASTATEMACHINE_FW_UPDATE_FAILED))
        {
            retcode = RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_FAILED_UPDATE_GENERIC_ERROR);
        }
    }
    if (RETCODE_OK == retcode)
    {
        /* Set the current FOTA State in user page */
        retcode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
    }
    if (RETCODE_OK == retcode)
    {
        /* Store the backup of url details */
        retcode = WriteFotaContext(FOTA_CONTEXT_URL, (void * ) PackageUrl, FOTA_FIRMWARE_PACKAGE_URL_SIZE);
        if (RETCODE_OK == retcode)
        {
            /* Notifying application that a valid URI is received.
             * In-case of any failure from callback,
             * there is no need to do anything from the FOTA package.
             */
            (void) FotaNotificationCall(FOTA_State, FOTA_VALID_URI_RECEIVED);
        }
    }
    if (RETCODE_OK == retcode)
    {
        /* Getting the FOTA result */
        Result = GetFotaResultResource();
        /* Updating the current FOTA Result in user page */
        retcode = WriteFotaContext(FOTA_CONTEXT_CURRENT_RESULT, (uint32_t* ) &Result, sizeof(uint8_t));
    }
    if (RETCODE_OK == retcode)
    {
        /* Store changes to flash */
        retcode = FlushFotaContext();
    }
    return retcode;
}

/* The function description is available in the Private header */
static retcode_t FotaStateMachine_UriDownload(Lwm2mSerializer_T *serializerPntr, Lwm2mParser_T *parserPntr)
{
    BCDS_UNUSED(serializerPntr);

    if (NULL == parserPntr)
    {
        return RC_LWM2M_METHOD_NOT_ALLOWED;
    }
    else
    {
        /* Extract the firmware location details */
        retcode_t rc = ExtractLocation(parserPntr, PackageUrl);
        if (RC_OK != rc)
        {
            if (RETCODE_OK != SetFotaUpdateResult(FOTASTATEMACHINE_FW_INVALID_URI))
            {
                Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_UPDATE_INVALID_URI_FAILED));
            }
            return RC_LWM2M_PARSING_ERROR;
        }
        else if (FOTA_IDLE == FOTA_State) /* Initiate a firmware download only if the current state is IDLE state */
        {
            /* configuring the firmware download */
            rc = DownloadClient_SetupNewDownload(PackageUrl, CoapBlockwise_Log2(FOTA_COAP_BLOCK_SIZE), true, OnFileDownloadStatusChanged);
            if (RC_OK != rc)
            {
                if (RETCODE_OK != SetFotaUpdateResult(FOTASTATEMACHINE_FW_INVALID_URI))
                {
                    Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_UPDATE_INVALID_URI_FAILED));
                }
                return rc;
            }
            UpdateFotaResultResource(FOTASTATEMACHINE_FW_DEFAULT);

            /* status updated to downloading */
            StartFotaDownload();
        }
        UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());

        return RC_OK;
    }
}

/**
 * @brief   This function is used continue the fota after the reset or connection loss.
 *          This function uses the details from the user page and controls the state machine to
 *          continue collecting the block form last collected block number.
 *
 * @retval  retcode_t
 *              RETCODE_OK, if successful
 *              RETCODE_FAILURE, if cannot continue the download.
 */
static Retcode_T FotaStateMachine_ResumeFota(void)
{
    uint32_t NextBlockNumber = 0;
    Retcode_T ReturnVal = RETCODE_OK;

    /* Read the next block to be download before reset */
    ReturnVal = ReadFotaContext(FOTA_CONTEXT_NEXT_BLOCK_NO, &NextBlockNumber, sizeof(NextBlockNumber));

    if (RETCODE_OK == ReturnVal)
    {
        /* Read the package URI which was used for downloading before reset */
        ReturnVal = ReadFotaContext(FOTA_CONTEXT_URL, (void * ) PackageUrl, FOTA_FIRMWARE_PACKAGE_URL_SIZE);
    }
    if (RETCODE_OK == ReturnVal)
    {
        /* setup the firmware download to fill all the required details */
        if (RC_OK != DownloadClient_SetupNewDownload(PackageUrl, CoapBlockwise_Log2(FOTA_COAP_BLOCK_SIZE), true, OnFileDownloadStatusChanged))
        {
            ReturnVal = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) RETCODE_FAILURE);
        }
        else
        {
            /* Resume downloading from that block */
            DownloadClient_ResumeFromBlock(NextBlockNumber);
        }
    }
    return ReturnVal;
}

/**
 * @brief   This function is used to check the new firmware acceptance on power up & update the 
 *			fota result & Fota state Fota object resources.so that device will report it to the server 
 *			after registarion.
 *
 * @param[in] acceptNewFW
 *                  acceptNewFW flag read from the NVM is passed This is set by the bootloader after the update.
 * @retval  retcode_t
 *              RETCODE_OK, if successful
 */
static Retcode_T CheckFotaNewFirmwareAcceptance(uint8_t acceptNewFW)
{
    uint8_t result;
    Retcode_T returnCode = RETCODE_OK;

    if (acceptNewFW == FALSE)
    {
        /*  Updating the FOTA Updated status is success in Result resource,
         *  only if NVM_ITEM_ID_IS_NEW_FW is Set and FOTA state is in Updating
         */
        result = FOTASTATEMACHINE_FW_UPDATE_SUCCESS;
        UpdateFotaResultResource(result); /* Updating the download result in FOTA Object for registration*/
        FOTA_State = FOTA_IDLE;
    }
    else
    {
        /*  Updating the FOTA Updated status is failed in Result resource,
         *  Because firmware swapping has failed.
         */
        result = FOTASTATEMACHINE_FW_UPDATE_FAILED;
        UpdateFotaResultResource(result); /* Updating the download result in FOTA Object for registration*/
        FOTA_State = FOTA_DOWNLOADED;
    }
    /* Updating the current FOTA Result in user page */
    returnCode = WriteFotaContext(FOTA_CONTEXT_CURRENT_RESULT, (uint32_t* )&result, sizeof(uint8_t));

    if (RETCODE_OK == returnCode)
    {
        /*  Updating the current FOTA State in user page */
        returnCode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));

        if (RETCODE_OK == returnCode)
        {
            /* Save changes */
            returnCode = FlushFotaContext();
        }
    }
    return returnCode;
}

/* The function description is available in the Private header */
static Retcode_T FotaStateMachine_ResetCredentials(void)
{
    Retcode_T RetValue = RETCODE_OK;
    uint32_t NextBlockNumber = 0;
    uint8_t DownloadInProgress = FALSE;
    int32_t Result = 0;

    /* Reset the Next block to download in user page to zero */
    RetValue = WriteFotaContext(FOTA_CONTEXT_NEXT_BLOCK_NO, &NextBlockNumber, sizeof(NextBlockNumber));
    if (RETCODE_OK == RetValue)
    {
        /* Reset the download in progress flag in user page to zero */
        RetValue = WriteFotaContext(FOTA_CONTEXT_DOWNLOAD_IN_PROGRESS, (uint32_t* )&DownloadInProgress, sizeof(DownloadInProgress));
    }
    if (RETCODE_OK == RetValue)
    {
        /* Make the FOTA state machine to be in IDLE state */
        SetFotaStateIdle();
        /*  Updating the current FOTA State in user page */
        RetValue = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
    }
    if (RETCODE_OK == RetValue)
    {
        /* Get the firmware update result */
        Result = GetFotaResultResource();
        /* Updating the current FOTA Result in user page */
        RetValue = WriteFotaContext(FOTA_CONTEXT_CURRENT_RESULT, (uint32_t* ) &Result, sizeof(uint8_t));
    }
    if (RETCODE_OK == RetValue)
    {
        /* Store the reset of url details */
        uint8_t packageUrlTemp[FOTA_FIRMWARE_PACKAGE_URL_SIZE];
        memset(packageUrlTemp, 0, FOTA_FIRMWARE_PACKAGE_URL_SIZE);
        /* Updating the URI in user page */
        RetValue = WriteFotaContext(FOTA_CONTEXT_URL, (void* ) packageUrlTemp, FOTA_FIRMWARE_PACKAGE_URL_SIZE);
    }
    if (RETCODE_OK == RetValue)
    {
        /* Save changed data to flash */
        RetValue = FlushFotaContext();
    }
    return RetValue;
}

/* The description is in interface header */
Retcode_T FotaStateMachine_Handler(void)
{
    /* Assumes that the Serval Scheduler runs in the same task context as this function!! */
    Retcode_T returnCode = RETCODE_OK;
    Retcode_T nvmStatus = RETCODE_OK;
    if ((NULL == FotaStorageAgentHandle) || (NULL == FotaNotificationCall))
    {
        returnCode = SetFotaUpdateResult(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        BCDS_UNUSED(returnCode);
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_NULL_POINTER));
    }
    /* If FOTA Download timeout is happen due to latency or communication issue then,
     Notification "FOTA_DELAY_REQUEST" send to application. So application has to delay
     FotaStateMachine_Handler() pooling a minimum of 2 sec for proper communication with the server.
     */
    if (FOTA_TIMEOUT_NOTIFICATION_ENABLE == TimeoutBlock)
    {
        returnCode = FotaNotificationCall(FOTA_State, FOTA_DELAY_REQUEST);
        if (RETCODE_OK != returnCode)
        {
            Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_DELAY_REQUEST_NOTIFY_FAIL));
        }
        TimeoutBlock = FOTA_TIMEOUT_NOTIFICATION_DISABLE;
        return RETCODE_OK;
    }
    /*
     * Send has failed and notify to application to handle the communication channel related errors like GSM signal loss.
     * So that application handle will do the necessary actions and call the FotaStateMachine_Handler() accordingly
     */
    if (true == SendFailureFlag)
    {
        returnCode = FotaNotificationCall(FOTA_State, FOTA_SEND_FAILED);
        if (RETCODE_OK != returnCode)
        {
            Retcode_raiseError(RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) FOTA_SEND_FAILED_NOTIFY_FAIL));
        }
        SendFailureFlag = false;
        return RETCODE_OK;
    }

    switch (FOTA_State)
    {
    case FOTA_DOWNLOADING_NOTIFY:
        returnCode = PrepareFotaDownload();
        if (RETCODE_OK != returnCode)
        {
            returnCode = FotaStateMachine_ResetCredentials();
            return returnCode;
        }
        returnCode = FotaStateAndResultNotify();
        if (RETCODE_OK == returnCode)
        {
            uint8_t DownloadInProgress = TRUE;
            FOTA_State = FOTA_DOWNLOADING_READY_FOR_REQUEST;
            /* Set the current FOTA State in user page */
            //lint -e485 error from NVM suppressed. NVM_ITEM is duplicate initialed once in config and in the function stack.
            returnCode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
            if (RETCODE_OK == returnCode)
            {
                /* The download has started now. From now on, if any reset happens before all blocks download,
                 * then the state will be set to READY_FOR_REQUEST in FOTA_Initialize to resume block download */
                returnCode = WriteFotaContext(FOTA_CONTEXT_DOWNLOAD_IN_PROGRESS, (uint32_t* )&DownloadInProgress, sizeof(DownloadInProgress));
            }
            if (RETCODE_OK == returnCode)
            {
                /* Store changes to flash */
                returnCode = FlushFotaContext();
            }
        }
        else
        {
            /* Remain in same state. This state will get executed again in the next call cycle.
             * The number of retries need to be handled */
        }
        break;

    case FOTA_DOWNLOADING_READY_FOR_REQUEST:

        FotaAsynchronousCallFlag = 0;

        returnCode = (Retcode_T) DownloadClient_DownloadBlock();
        if ((Retcode_T) RC_COAP_CLIENT_SESSION_ALREADY_ACTIVE == returnCode)
        {
            TimeoutBlock = FOTA_TIMEOUT_NOTIFICATION_ENABLE;
            break;
        }
        /*
         * Ensure that the state was not changed in a callback
         */
        if ((FOTA_DOWNLOADING_READY_FOR_REQUEST == FOTA_State) && (FotaAsynchronousCallFlag == 0))
        {
            if (returnCode == (Retcode_T) RC_OK)
            {
                FotaDownloadRequestSent();
            }
            else
            {
                UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
                AbortFotaDownload();
                UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
                /* Set the current FOTA State in user page */
                nvmStatus = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
                if (RETCODE_OK != nvmStatus)
                {
                    return nvmStatus;
                }
                nvmStatus = FlushFotaContext();
                if (RETCODE_OK != nvmStatus)
                {
                    return RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) RETCODE_FAILURE);
                }
                returnCode = RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) RETCODE_NOT_SUPPORTED);
            }
        }
        else
        {
            /* Asynchronous call triggered from Serval stack */
            FotaAsynchronousCallFlag = 0;
        }
        break;

    case FOTA_DOWNLOADING_WAITING_FOR_RESPONSE:
        /* FOTA_State is changed in onFileDownloadStatusChanged() API to any of the below mentioned states based on the response received.
         (FOTA_DOWNLOADING_READY_FOR_REQUEST) or (FOTA_DOWNLOADED_NOTIFY) or  (FOTA_FAILED_NOTIFY)  so just return RETCODE_OK on this state */
        break;

    case FOTA_DOWNLOADED_NOTIFY:
        returnCode = ValidationAgent_VerifyFirmware(FotaStorageAgentHandle, FOTA_PARTITION_DOWNLOAD);
        if (RETCODE_OK == returnCode)
        {
            returnCode = FotaStateAndResultNotify();

            if (RETCODE_OK == returnCode)
            {
                FOTA_State = FOTA_DOWNLOADED;

                /* Set the current FOTA State in user page */
                returnCode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));

                if (RETCODE_OK == returnCode)
                {
                    returnCode = FlushFotaContext();
                }
            }
            else
            {
                /* Remain in same state. This state will get executed again in the next call cycle.
                 * The number of retries need to be handled */
            }
        }
        else
        {
            UpdateFotaResultResource(FOTASTATEMACHINE_FW_CRC_FAILED);
            AbortFotaDownload();
            UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
        }
        break;

    case FOTA_DOWNLOADED:
        /* Once FOTA image downloading completed then the notification is sent to the server on FOTA_DOWNLOADED_NOTIFY state &
         FOTA_State will be in this state till update comes from server. on FOTASTATEMACHINE_FW_UPDATE_FAILED So just retutn RETCODE_OK on this state */
        break;

    case FOTA_UPDATING_NOTIFY:
        if (Lwm2mReporting_resourceChanged(&stateUriPath) == RC_OK)
        {
            FOTA_State = FOTA_UPDATING;
            /* Set the current FOTA State in user page */
            returnCode = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));
            if (RETCODE_OK == returnCode)
            {
                returnCode = FlushFotaContext();
            }
        }
        else
        {
            /* Remain in same state. This state will get executed again in the next call cycle.
             * The number of retries need to be handled */
        }
        break;

    case FOTA_UPDATING:
        /* To proceed the Fota update application will be doing the necessary tasks befor calling FotaStateMachine_Update() API
         while getting the FOTA_State = FOTA_UPDATING */
        /* Reset all the FOTA credentials before reset */
        returnCode = PrepareAndNofityUpdate();
        if (RETCODE_OK != returnCode)
        {
            /* Here nvmStatus error code is not returned. Only returnCode error is returned */
            UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);

            /*As per spec, after the update failed, the state have to be in DOWNLOADED state */
            FOTA_State = FOTA_DOWNLOADED;

            /* Set the current FOTA State in user page */
            nvmStatus = WriteFotaContext(FOTA_CONTEXT_STATE, (uint32_t* ) &FOTA_State, sizeof(uint32_t));

            if (RETCODE_OK == nvmStatus)
            {
                nvmStatus = FlushFotaContext();
            }
            if (RETCODE_OK == nvmStatus)
            {
                UpdateFirmwareDownloadStatus(GetFirmwareDownloadState());
                returnCode = FotaStateAndResultNotify();
            }
        }
        break;

    case FOTA_FAILED_NOTIFY:
        if (RETCODE_OK == FotaStateAndResultNotify())
        {
            /* Reset all the FOTA related actions */
            returnCode = FotaStateMachine_ResetCredentials();
            if (RETCODE_OK != returnCode)
            {
                /* The state will not get changed. So this state will try to execute until success or always.
                 This needs to be handled to decide the number of retries */
            }
        }
        else
        {
            /* Remain in same state. This state will get executed again in the next call cycle.
             * The number of retries need to be handled */
        }
        break;

    case FOTA_INVALID:
        returnCode = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) RETCODE_NOT_SUPPORTED);
        break;

    case FOTA_IDLE:
        /* After successfull update it will stay here Also on Fota_State data in user page invalid on power up
         stay here till Fota update triggers through URI from server */
        break;

    default:
        returnCode = RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) RETCODE_NOT_SUPPORTED);
        break;
    }

    return (returnCode);
}

/* The description is in interface header */
Retcode_T FotaStateMachine_Init(FotaStorageAgentPtr_T storageAgentHandle, FotaStateMachine_Notification_T notification)
{
    uint8_t DownloadInProgress = FALSE;
    uint8_t isFWWaitingAcceptance = FALSE;
    Retcode_T returnCode = RETCODE_OK;
    uint8_t result = 0;
    uint32_t NextBlockNumber = 0;
    HeaderValidationCheck = 0;
    TimeoutBlock = FOTA_TIMEOUT_NOTIFICATION_DISABLE;
    SendFailureFlag = false;

#ifndef CONFIG_EXTERNAL_NVM
    NvmItemCurrentState = NVM_ITEM_ID_FOTA_CURRENT_STATE;
    NvmItemNewState = NVM_ITEM_ID_IS_NEW_FW;
    NvmItemNextBlock = NVM_ITEM_ID_NEXT_BLOCK_NUMBER;
    NvmItemDownloadInProgress = NVM_ITEM_ID_DOWNLOAD_IN_PROGRESS;
    NvmItemFirmwarePackageUrl = NVM_ITEM_ID_FIRMWARE_PACKAGE_URL;
    NvmItemCurrentResult = NVM_ITEM_ID_FOTA_CURRENT_RESULT;
#endif

    if ((NULL == storageAgentHandle) || (NULL == notification))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_NULL_POINTER));
    }
    FotaStorageAgentHandle = storageAgentHandle;
    FotaNotificationCall = notification;

    /*Reading Fotaresult, acceptance & FotaState information from NVM on power up*/
    returnCode = GetFotaNVMData(&result, &isFWWaitingAcceptance);
    if (RETCODE_OK != returnCode)
    {
        SetFotaStateIdle();
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        return returnCode;
    }
    if (FOTA_UPDATING == FOTA_State)
    {
        returnCode = CheckFotaNewFirmwareAcceptance(isFWWaitingAcceptance);
    }
    else if (FOTA_IDLE != FOTA_State)
    {
        /* The Firmware download result from NVM is success. The download might have been already finished or not yet started  */
        /* check whether FOTA download was in progress before reset */
        returnCode = ReadFotaContext(FOTA_CONTEXT_DOWNLOAD_IN_PROGRESS, (uint32_t* )&DownloadInProgress, sizeof(DownloadInProgress));

        if (RETCODE_OK == returnCode)
        {
            /* Get the FOTA credentials from user page and be in state notify */
            returnCode = FotaStateMachine_ResumeFota();
        }
        if (RETCODE_OK == returnCode)
        {
            returnCode = ReadFotaContext(FOTA_CONTEXT_NEXT_BLOCK_NO, &NextBlockNumber, sizeof(NextBlockNumber));
            if (RETCODE_OK == returnCode)
            {
                if ((NextBlockNumber * FOTA_COAP_BLOCK_SIZE) >= FWCONTAINER_HEADER_SIZE)
                {
                    returnCode = ValidationAgent_VerifyHeader(FotaStorageAgentHandle, FOTA_PARTITION_DOWNLOAD);
                }
            }
        }
    }
    if (RETCODE_OK == returnCode)
    {
        if (TRUE == DownloadInProgress)
        {
            FotaDownloadResponseReceived(); /* Trigger the download without updating the state to server */
        }
        UpdateFirmwareDownloadStatus(GetFirmwareDownloadState()); /* Updating the download state in FOTA Object for registration */
    }
    else
    {
        SetFotaStateIdle();
        UpdateFotaResultResource(FOTASTATEMACHINE_FW_UPDATE_FAILED);
        returnCode = RETCODE(RETCODE_SEVERITY_WARNING, (Retcode_T) RETCODE_FAILURE);
    }
    /* Updating the download result in FOTA Object for registration*/
    if ((result != FOTASTATEMACHINE_FW_DEFAULT) && (result <= FOTASTATEMACHINE_FW_UPDATE_FAILED))
    {
        UpdateFotaResultResource(result);
    }
    return returnCode;
}

/* The description is in interface header */
FotaStateMachine_State_T FotaStateMachine_GetState(void)
{
    return FOTA_State;
}

Retcode_T FotaStateMachine_CancelDownload(void)
{
    Retcode_T returnCode = RETCODE_OK;
    int32_t fotaDownloadState = 0U;

    fotaDownloadState = GetFirmwareDownloadState();
    if (FOTA_DOWNLOADING_READY_FOR_REQUEST == (FotaStateMachine_State_T) fotaDownloadState
            || FOTA_DOWNLOADING_NOTIFY == (FotaStateMachine_State_T) fotaDownloadState
            || FOTA_DOWNLOADING_WAITING_FOR_RESPONSE == (FotaStateMachine_State_T) fotaDownloadState)
    {
        /* Make the FOTA download state to be in FAILURE state */
        AbortFotaDownload();

        returnCode = FotaStateAndResultNotify();
        if (RETCODE_OK == returnCode)
        {
            /* Reset all the FOTA related actions */
            returnCode = FotaStateMachine_ResetCredentials();
        }
    }
    else
    {
        /*
         * Device is not in download mode or has already downloaded.
         * @TODO: Need to address for device already in download mode.
         * - Maybe delete the downloaded firmware as well.
         */
        returnCode = RETCODE_OK;
    }

    return returnCode;
}

#endif /*BCDS_FEATURE_STATEMACHINE*/
