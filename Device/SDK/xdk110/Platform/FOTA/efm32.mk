##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

BCDS_TARGET_PLATFORM ?= efm32
BCDS_DEVICE_ID ?= EFM32GG230F1024
BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_TARGET_BOARD ?= BSP_XDK110_R0
BCDS_DEVICE_PACKAGE_TYPE ?= EFM32GGxxx

ifeq ($(BCDS_TARGET_BOARD),BSP_XDK110_R0)
BCDS_DEVICE_ID ?=EFM32GG390F1024
endif

BCDS_LIB_BASE_NAME ?= lib$(BCDS_PACKAGE_NAME)
BCDS_DEBUG_LIB_NAME = $(BCDS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_RELEASE_LIB_NAME = $(BCDS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a
BCDS_DEBUG_LIB   = $(BCDS_DEBUG_PATH)/$(BCDS_DEBUG_LIB_NAME)
BCDS_RELEASE_LIB = $(BCDS_RELEASE_PATH)/$(BCDS_RELEASE_LIB_NAME)

# Define the path for the include directories
BCDS_PACKAGE_INCLUDES = \
    -I $(BCDS_PACKAGE_CONFIG_PATH) \
    -I $(BCDS_INCLUDE_DIR)\
    -I $(BCDS_SOURCE_DIR)/$(BCDS_PROTECTED_DIR) \
    -I $(BCDS_CONFIG_PATH) \
    -I $(BCDS_CONFIG_PATH)/HAL \
    -I $(BCDS_CONFIG_PATH)/Utils \
    -I $(BCDS_CONFIG_PATH)/MiscDrivers \
    -I $(BCDS_MISCDRIVERS_DIR)/include \
    -I $(BCDS_BASICS_DIR)/include \
    -I $(BCDS_HAL_DIR)/include \
    -I $(BCDS_HAL_DIR)/include/bsp \
    -I $(BCDS_HAL_DIR)/include/mcu \
    -I $(BCDS_BOARD_PATH)/include \
    -I $(BCDS_UTILS_DIR)/include

# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	-DASSERT_FILENAME=\"$*.c\"

# Build chain settings
ifndef BCDS_BUILD_LINUX
CC =  $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR =  $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
BCDS_LINT_EXE = $(BCDS_LINT_PATH)/lint-nt.launch
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

RMDIRS := rm -rf

###############################################################################
# Compiler settings
###############################################################################
BCDS_ARCH_FLAGS = \
	-mcpu=cortex-m3 -mthumb

ARFLAGS = -cr

# Lint config
LINT_CONFIG = \
	+d$(BCDS_DEVICE_TYPE) +d$(BCDS_DEVICE_ID) +d$(BCDS_TARGET_PLATFORM) +dBCDS_TARGET_EFM32 +dBCDS_PACKAGE_ID \
    $(BCDS_EXTERNAL_INCLUDES_LINT) $(BCDS_LINT_CONFIG) $(BCDS_FOTA_INCLUDES_LINT) $(BCDS_LINT_CONFIG_FILE) \
    +libh\(core_cm3.h\) +libh\(em_gpio.h\) +libh\(device.h\) +libh\(em_msc.h\) +libh\(Serval_Lwm2m.h\)\
    

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Werror -Wextra -Wmissing-prototypes -Wimplicit-function-declaration -Wstrict-prototypes \
	$(BCDS_ARCH_FLAGS) -ffunction-sections -fdata-sections \
	-D $(BCDS_DEVICE_ID) $(SERVAL_FEATURES_CONFIG) -D $(BCDS_DEVICE_PACKAGE_TYPE) -D $(BCDS_TARGET_PLATFORM) -DBCDS_MODULE_ID=26
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(BCDS_DEPEDENCY_FLAGS) -D BCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) \
-D BCDS_TARGET_EFM32 -D $(BCDS_TARGET_PLATFORM) -D BCDS_TARGET_PLATFORM=$(BCDS_TARGET_PLATFORM) -D BCDS_FEATURE_MEDIUM_EXTFLASH=1 \
-D$(BCDS_DEVICE_PACKAGE_TYPE)
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

#If there is any additional target specific include path then mention here.
BCDS_STM32CUBE_DIR ?= $(BCDS_LIBRARIES_DIR)/STM32Cube/stm32cube

BCDS_EXTERNAL_INCLUDES = \
    -isystem $(BCDS_FREE_RTOS_DIR)/source/include \
    -isystem $(BCDS_PLATFORM_DIR)/ServalPAL_WiFi/include \
    -isystem $(BCDS_CYCURL_LIB_DIR)/lib/inc \
    -isystem $(BCDS_LIBRARIES_DIR)/ServalStack/ServalStack/api \
    -isystem $(BCDS_LIBRARIES_DIR)/ServalStack/ServalStack/pal \
    -isystem $(BCDS_FREE_RTOS_DIR)/source/portable/ARM_CM3 \
    -isystem $(BCDS_LIBRARIES_DIR)/ServalStack \
    -isystem $(BCDS_LIBRARIES_DIR)/EMlib/EMLib/emlib/inc \
    -isystem $(BCDS_LIBRARIES_DIR)/EMlib/EMLib/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
    -isystem $(BCDS_LIBRARIES_DIR)/EMlib/EMLib/CMSIS/Include \
    -isystem $(BCDS_PLATFORM_DIR)/Peripherals/include \
    -isystem $(BCDS_LIBRARIES_DIR)/FATfs/src
    

#LINT variables
BCDS_EXTERNAL_EXCLUDES_LINT := $(foreach DIR, $(subst -isystem , ,$(BCDS_EXTERNAL_INCLUDES)), +libdir\($(DIR)\))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -isystem ,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_EXTERNAL_INCLUDES_LINT += -i$(BCDS_LINT_CONFIG_FILE_PATH)
BCDS_INCLUDES_LINT := $(subst -I ,-i,$(BCDS_PACKAGE_INCLUDES))
BCDS_EXTERNAL_INCLUDES_LINT += +libh\(AttBase.h\) +libh\(portmacro.h\) +libh\(BleGap.h\) +libh\(stm32l4xx_hal_dac.h\) +libh\(BleTypes.h\)

# Source and object files
BCDS_PACKAGE_SOURCE_FILES = \
	    $(BCDS_SOURCE_DIR)/FWContainer.c \
	    $(BCDS_SOURCE_DIR)/ValidationAgent.c \
	    $(BCDS_SOURCE_DIR)/FirmwareValidation.c \
	    $(BCDS_SOURCE_DIR)/FotaStateMachine.c \
	    $(BCDS_SOURCE_DIR)/CoapBlockwise.c \
	    $(BCDS_SOURCE_DIR)/DownloadClient.c \
	    $(BCDS_SOURCE_DIR)/SDCardStorage.c

BCDS_PACKAGE_OBJECT_FILES = \
	$(patsubst $(BCDS_SOURCE_DIR)/%.c, %.o, $(BCDS_PACKAGE_SOURCE_FILES))

BCDS_PACKAGE_LINT_FILES = \
	$(patsubst $(BCDS_SOURCE_DIR)/%.c, %.lob, $(BCDS_PACKAGE_SOURCE_FILES))

BCDS_PACKAGE_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_DEBUG_OBJECT_PATH)/,$(BCDS_PACKAGE_OBJECT_FILES))

BCDS_PACKAGE_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_DEBUG_OBJECT_PATH)/, $(BCDS_PACKAGE_OBJECT_FILES:.o=.d))

BCDS_PACKAGE_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_RELEASE_OBJECT_PATH)/,$(BCDS_PACKAGE_OBJECT_FILES))

BCDS_PACKAGE_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_RELEASE_OBJECT_PATH)/, $(BCDS_PACKAGE_OBJECT_FILES:.o=.d))
	
BCDS_PACKAGE_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_DEBUG_LINT_PATH)/, $(BCDS_PACKAGE_LINT_FILES))

######################## Build Targets #######################################

.PHONY: debug release clean lint lint_clean diagnosis

debug: $(BCDS_DEBUG_LIB)

release: $(BCDS_RELEASE_LIB)

############# Generate Platform Library for BCDS ###################
$(BCDS_DEBUG_LIB): $(BCDS_PACKAGE_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build $(BCDS_PACKAGE_NAME) for $(BCDS_TARGET_PLATFORM) (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_PACKAGE_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_RELEASE_LIB): $(BCDS_PACKAGE_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build $(BCDS_PACKAGE_NAME) for $(BCDS_TARGET_PLATFORM) (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_PACKAGE_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"

# compile C files
$(BCDS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_SOURCE_DIR)/%.c
	@echo $(BCDS_PACKAGE_INCLUDES)
	@echo "Build $<"
	@echo "mkdir -p "$(@D)
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
	       -I . $(BCDS_PACKAGE_INCLUDES) $(BCDS_EXTERNAL_INCLUDES)  $< -o $@

$(BCDS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_PACKAGE_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) $< -o $@

.PHONY: clean
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_DEBUG_PATH) $(BCDS_RELEASE_PATH)

lint: $(BCDS_PACKAGE_DEBUG_LINT_FILES)
	@echo "Lint End"

$(BCDS_DEBUG_LINT_PATH)/%.lob: $(BCDS_SOURCE_DIR)/%.c
	@mkdir -p $(@D)
	@echo $@
	@$(BCDS_LINT_EXE) $(BCDS_EXTERNAL_EXCLUDES_LINT) $(BCDS_EXTERNAL_INCLUDES_LINT) $(BCDS_INCLUDES_LINT) $(LINT_CONFIG) $(BCDS_LINT_CONFIG_FILE_PATH)/bcds.lnt $< -oo[$@]
	@echo "===================================="

-include $(BCDS_PACKAGE_DEBUG_DEPENDENCY_FILES) $(BCDS_PACKAGE_RELEASE_DEPENDENCY_FILES)

lint_clean :
	@echo Cleaning Lint files
	@$(RMDIRS) $(BCDS_DEBUG_LINT_PATH)
	@echo Lint files cleaned

.PHONY: diagnosis
diagnosis:
	@echo "--- diagnosis ---"
	@echo "BCDS_TARGET_PLATFORM: "$(BCDS_TARGET_PLATFORM)
	@echo "BCDS_PACKAGE_NAME: " $(BCDS_PACKAGE_NAME)
	@echo "BCDS_PACKAGE_HOME: " $(BCDS_PACKAGE_HOME)
	@echo "BCDS_DEBUG_OBJECT_PATH: " $(BCDS_DEBUG_OBJECT_PATH)
	@echo "BCDS_SOURCE_DIR: " $(BCDS_SOURCE_DIR)
	@echo "BCDS_DEVICE_TYPE: "$(BCDS_DEVICE_TYPE)
	@echo "BCDS_DEVICE_PACKAGE_TYPE: "$(BCDS_DEVICE_PACKAGE_TYPE)
	@echo "BCDS_DEVICE_ID: "$(BCDS_DEVICE_ID)
	@echo "BCDS_TARGET_BOARD: "$(BCDS_TARGET_BOARD)
	@echo "BCDS_CONFIG_PATH: "$(BCDS_CONFIG_PATH)
	@echo "BCDS_HAL_DIR: "$(BCDS_HAL_DIR)
	@echo " "
	@echo "BCDS_DEBUG_PATH: "$(BCDS_DEBUG_PATH)
	@echo "BCDS_RELEASE_PATH: "$(BCDS_RELEASE_PATH)
	@echo " "
	@echo "BCDS_PACKAGE_INCLUDES: " $(BCDS_PACKAGE_INCLUDES)
	@echo " "
	@echo "CFLAGS_DEBUG: " $(CFLAGS_DEBUG)
	@echo " "
	@echo "CFLAGS_COMMON: " $(CFLAGS_COMMON)
	@echo " "
	@echo "BCDS_CFLAGS_DEBUG_COMMON: " $(BCDS_CFLAGS_DEBUG_COMMON)
	@echo " "
	@echo "BCDS_PACKAGE_INCLUDES: " $(BCDS_PACKAGE_INCLUDES)
	@echo " "
	@echo "BCDS_EXTERNAL_INCLUDES: " $(BCDS_EXTERNAL_INCLUDES)
	@echo " "
	@echo "Objects"
	@echo $(BCDS_PACKAGE_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_DEBUG_PATH)
	@echo $(BCDS_PACKAGE_DEBUG_OBJECT_FILES)
	@echo ""
	@echo "BCDS_DEBUG_LIB: "$(BCDS_DEBUG_LIB)
	@echo "BCDS_RELEASE_LIB: "$(BCDS_RELEASE_LIB)
	@echo "---"
