/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @file
 *  @defgroup    FotaStorageAgent FotaStorageAgent
 *
 *  @{
 *  @brief      This file provides the interfaces to access different partitions of the SDCard
 *              storage medium trough FATFs filesystem API's.
 * ****************************************************************************/

#ifndef BCDS_FOTASTORAGEAGENT_H_
#define BCDS_FOTASTORAGEAGENT_H_

/* Include all headers which are needed by this file. */

#include "BCDS_Fota.h"
#include "BCDS_FotaConfig.h"

/* Put the type and macro definitions here */
/**
 * @brief  enums to Identify the different Storage Mediums
 */
enum FotaStorageMedium_E
{
	FOTA_STORAGE_MEDIUM_SDCARD,
	FOTA_STORAGE_MEDIUM_INVALID
};
typedef enum FotaStorageMedium_E FotaStorageMedium_T, *FotaStorageMediumPtr_T;

/* Put the function declarations here */

/**
 * @brief   This function reads the data from the specified partition of the storage medium. 
			Before calling this funtion ensure FotaStorageAgent_Init has been done for the corresponding storage medium.
			Also the caller of this function has to ensure the valid position & no of bytes to read from the medium. 
			
 * @param[in] partition: Partition of the storage medium to read.
 *
 * @param[out] buff: pointer to destination buff to hold the data.  This pointer should have enough room to place no of bytes that is
 *                      mentioned in argument numOfBytes.
 *
 * @param[in] position: offset from the start of the partition.
 *
 * @param[in] numOfBytes: Number of bytes to read from the storage medium.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
typedef Retcode_T (*FotaStorageAgent_ReadFunc_T)(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes);
/**
 * @brief   This function writes the data from the buffer to the specified partition of the storage medium.
			Before calling this funtion ensure FotaStorageAgent_Init has been done for the corresponding storage medium.
			Also the caller of this has to ensure the valid position & no of bytes to write to the medium. 

 * @param[in] partition: Partition of the storage medium to write.
 *
 * @param[in] buff: Pointer to buffer containing valid data to be written to storage medium.
 *
 * @param[in] position: offset from the start of the partition.
 *
 * @param[in] numOfBytes: Number of bytes to write.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
typedef Retcode_T (*FotaStorageAgent_WriteFunc_T)(Fota_Partition_T partition, uint8_t * buff, uint32_t position, uint32_t numOfBytes);
/**
 * @brief   This function Erases the specified partition data of the storage medium.
			Before calling this funtion ensure FotaStorageAgent_Init has been done for the corresponding storage medium.

 * @param[in] partition: partition to erase.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
typedef Retcode_T (*FotaStorageAgent_EraseFunc_T)(Fota_Partition_T partition);
/**
 * @brief   This function copies the data from the soucrce partition to the destination partition of the storage medium.
			Before calling this funtion ensure FotaStorageAgent_Init has been done for the corresponding storage medium.
			The data copy happens only in the same storage medium of different partitions.  

 * @param[in] dest: Destination Partition of the storage medium.
 *
 * @param[in] src: Source Partition of the storage medium.
 *
 * @retval  RETCODE_SUCCESS if the read was successful, else an error code is returned.
 *
 */
typedef Retcode_T (*FotaStorageAgent_CopyFunc_T)(Fota_Partition_T dest, Fota_Partition_T src);

/** Structure contains function pointers as it members to access the storage medium easily.*/
struct FotaStorageAgent_S
{
	FotaStorageAgent_ReadFunc_T Read;
	FotaStorageAgent_WriteFunc_T Write;
    FotaStorageAgent_EraseFunc_T Erase;
    FotaStorageAgent_CopyFunc_T Copy;
};

typedef struct FotaStorageAgent_S FotaStorageAgent_T, *FotaStorageAgentPtr_T;

/* Application can map the to access a particular storage medium as given in the below example code */

/**
 *
 *  Example usage:
 *  @code
 *  
 *	Application
 *	-----------------
 *	#include "FotaStorageAgent.h"
 *
 *	//Initialization
 *	struct FotaStorageAgent_S const * FotaStorageAgent = &SDCardStorage;
 *	// or
 *	struct FotaStorageAgent_S const * FotaStorageAgent = &ExtFlashFotaStorageAgent;
 *
 *	// Use
 *	Retcode_T retcode = FotaStorageAgent->Read(...);
 *	if(RETCODE_OK == retcode)
 *	{
 *		...
 *  }
 *
 * @endcode
 * ****************************************************************************/
#if BCDS_FEATURE_MEDIUM_SDCARD
extern FotaStorageAgent_T SDCardStorage;
#endif

#endif /* BCDS_FOTASTORAGEAGENT_H_*/

/**@} */
