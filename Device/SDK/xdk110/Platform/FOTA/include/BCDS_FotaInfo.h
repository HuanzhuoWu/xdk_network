/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/


/**
 *
 * @file
 * @breif It defines the package id and the version number.
 *
 *  @defgroup    FotaInfo FotaInfo
 *  @{
 **/


#ifndef BCDS_FotaInfo_H_
#define BCDS_FotaInfo_H_

/* Include all headers which are needed by this file. */

/* Put the type and macro definitions here */

#define BCDS_FOTA_PACKAGE_ID      26
#define BCDS_FOTA_VERSION_MAJOR   0
#define BCDS_FOTA_VERSION_MINOR   2
#define BCDS_FOTA_VERSION_PATCH   0

/**
 * @brief BCDS_MODULE_ID for each FOTA C module
 * @info  usage:
 *      #undef BCDS_MODULE_ID
 *      #define BCDS_MODULE_ID BCDS_FOTA_MODULE_ID_xxx
 */
enum BCDS_FOTA_ModuleID_E
{
    BCDS_FOTA_MODULE_ID_COAPBLOCKWISE = 1,
    BCDS_FOTA_MODULE_ID_DOWNLOADCLIENT = 2,
    BCDS_FOTA_MODULE_ID_EXTFLASHSTORAGE = 3,
    BCDS_FOTA_MODULE_ID_FIRMWAREVALIDATION = 4,
    BCDS_FOTA_MODULE_ID_FOTASTATEMACHINE = 5,
    BCDS_FOTA_MODULE_ID_FWCONTAINER = 6,
    BCDS_FOTA_MODULE_ID_SDCARDSTORAGE = 7,
    BCDS_FOTA_MODULE_ID_VALIDATIONAGENT = 8,
/* Define next module ID here and assign a value to it! */
};

/* Put the function declarations here */


#endif /* BCDS_FotaInfo_H_ */
/**@} */
