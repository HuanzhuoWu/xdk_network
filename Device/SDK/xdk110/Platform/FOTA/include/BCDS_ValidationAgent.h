/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @defgroup    ValidationAgent ValidationAgent
 *
 *  @{
 *  @brief      This module is responsible for validating new firmware that is about to be switched with the current working firmware.
 * ****************************************************************************/

#ifndef BCDS_ValidationAgent_H_
#define BCDS_ValidationAgent_H_

#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"

#if BCDS_FEATURE_VALIDATION_AGENT

#include "BCDS_FWContainer.h"
#include "pkcs1_rsaes_v15.h"
#include "pkcs1_rsassa_v15.h"

/* Public key defined in BCDS_PublicKey.h */
extern const Esc_UINT8 CurrentMod[];
extern const Esc_UINT16 keySizeBits;
extern const Esc_UINT32 pubExp;

/**
 * @brief   This function is called to verify the Firmware CRC of the given partition.
 *          This function internally reads the Firmware CRC and Firmware size from header data and
 *          read the firmware data from given partition and calculates the CRC and
 *          compare it with the firmware CRC in the header.
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 * 
 * @return  RETCODE_SUCCESS: 					If the Calculation was successful and the
 * 							 			     calculated CRC checksum matches the expected
 * 			RETCODE_INVALID_PARAM: 			 If a null pointer is handed as parameter
 *
 * 			FOTA_RETCODE_INTERNAL_ERROR: If an unpredicted error occurs in
 * 												the calculations of the underlying library
 * 			FOTA_RETCODE_CRC_ERROR:			 If the calculation is successful but the
 * 			calculated checksum does not match the expected checksum
 *
 */
Retcode_T ValidationAgent_VerifyFWCRC32(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T SelectedPartition);

/**
 * @brief   This function is called to verify the Header CRC of the given partition.
 *          This function internally reads the header data and calculates the CRC and compare it
 *          with the CRC in the header.
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota header.
 * 
 * @return  RETCODE_SUCCESS: If the Calculation was successful and the
 * 			calculated CRC checksum matches the expected
 * 			RETCODE_INVALID_PARAM: If a null pointer is handed as parameter
 * 			FOTA_RETCODE_INTERNAL_ERROR: If an unpredicted error occurs in
 * 			the calculations of the underlying library
 * 			FOTA_RETCODE_CRC_ERROR: If the calculation is successful but the
 * 			calculated checksum does not match the expected checksum
 *
 */
Retcode_T ValidationAgent_VerifyHeaderCRC32(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T SelectedPartition);

/**
 * @brief   This function is called after the integrity of the containerInfo is
 *  		verified. It calculates the sha256 hash of the firmware and performs
 *  		a PKCS1V1.5 digital signature verification against the signature
 *  		contained in the containerInfo.

 * @param[in] StorageAgentHandle: Handle to access the storage medium
 *
 * @param[in] SelectedPartition: Partition from where to validate the signature data.
 *
 * @param[in] PublicKey this is the public key in the format of the CycurLIB
 * 			  containing modulus and public exponent
 *
 * @return  RETCODE_SUCCESS: If the hash calculation was successful and the
 * 			signature matches the expected value
 * 			RETCODE_INVALID_PARAM: If a null pointer is handed as parameter
 * 			FOTA_RETCODE_INTERNAL_ERROR: If an unpredicted error occurs in
 * 			the calculations of the underlying library
 * 			FOTA_RETCODE_INVALID_SIGNATURE: If the calculation is successful
 * 			but the signature happens to be invalid
 *
 */
Retcode_T ValidationAgent_VerifyPKCS1V1_5(FotaStorageAgentPtr_T StorageAgentHandle,
		Fota_Partition_T SelectedPartition,
		const EscRsa_PubKeyT * PublicKey );

/**
 * @brief   This is a helper function to split the PKCS1 verification into
 * 			multiple parts to improve testability. This function should not be
 * 			called by the user. It reads the data from flash in a block wise
 * 			manner and calculates the hash over the whole data
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium
 *
 * @param[in] SelectedPartition: Partition from where to calculate the hash algorithm. It
 * 			  needs to be chosen from the enum in BCDS_Fota.h
 *
 * @param[in] containerInfo already should contain the parsed information from
 * 			  the raw data of the container. To parse it use the functions of
 * 			  the FWContainer.h
 *
 * @param[in] OutputHashBuffer: is the output buffer for the calculated hash

 * @return  RETCODE_SUCCESS: If the hash calculation was successful and the
 * 			signature matches the expected value
 * 			RETCODE_INVALID_PARAM: If a null pointer is handed as parameter
 * 			FOTA_RETCODE_INTERNAL_ERROR: If an unpredicted error occurs in
 * 			the calculations of the underlying library
 * 			FOTA_RETCODE_INVALID_SIGNATURE: If the calculation is successful
 * 			but the signature happens to be invalid
 */
Retcode_T ValidationAgent_HashCalculation(FotaStorageAgentPtr_T StorageAgentHandle,
        Fota_Partition_T SelectedPartition,
        FWContainer_Information_T * containerInfo,
        Esc_UINT8 * OutputHashBuffer);

/**
 * @brief   This function calculates the signature from the provided public key and hash and compares it
 *     		with the signature
 *
 * @param[in] PublicKey: Public key for the signature calculation.
 *
 * @param[in] containerInfo already should contain the parsed information from
 * 			  the raw data of the container. To parse it use the functions of
 * 			  the FWContainer.h
 *
 * @param[in] Sha256Hash: calculated hash for the firmware + header
 *
 * @return  RETCODE_OK: If the signature matches the expected value
 *
 * 			FOTA_RETCODE_INTERNAL_ERROR: If an unpredicted error occurs in
 * 			the calculations of the underlying library
 *
 * 			FOTA_RETCODE_INVALID_SIGNATURE: If the calculation is successful
 * 			but the signature happens to be invalid
 */
Retcode_T ValidationAgent_SignatureVerification(
		const EscRsa_PubKeyT * PublicKey, FWContainer_Information_T * containerInfo,
        Esc_UINT8 * Sha256Hash);

#endif /* BCDS_FEATURE_VALIDATION_AGENT */

/**
 * @brief   This function will verify the Header of the firmware.
 *          The implementation should be done as part of the application
 *          based on BCDS_FEATURE_VALIDATION_AGENT value.
 *
 * @param[in] storageAgent
 * Handle to access the storage medium.
 *
 * @param[in] Partition
 * Partition of the FOTA storage medium.
 *
 * @return  RETCODE_OK
 * Header validation is successful
 * @return   RETCODE_FAILURE
 * Header validation failure.
 *
 */
Retcode_T ValidationAgent_VerifyHeader(FotaStorageAgentPtr_T storageAgent, Fota_Partition_T partition);

/**
 * @brief   This function will verify the Firmware CRC or Signature
 *          of the downloaded file. The implementation should be as
 *          part application based on BCDS_FEATURE_VALIDATION_AGENT value.
 *
 * @param[in] storageAgent
 * Handle to access the storage medium.
 *
 * @param[in] Partition
 * Partition of the FOTA storage medium.
 *
 * @return  RETCODE_OK
 * If the Calculation was successful and the calculated CRC checksum matches the expected
 * @return  RETCODE_INVALID_PARAM
 * If a null pointer is handed as parameter
 * @return  FOTA_RETCODE_INTERNAL_ERROR
 * If an unpredicted error occurs in the calculations of the underlying library
 * @return  FOTA_RETCODE_CRC_ERROR
 * If the calculation is successful but the calculated checksum does not match the expected checksum
 *
 */
Retcode_T ValidationAgent_VerifyFirmware(FotaStorageAgentPtr_T storageAgent, Fota_Partition_T partition);

#endif /* BCDS_ValidationAgent_H_ */
/**@} */
