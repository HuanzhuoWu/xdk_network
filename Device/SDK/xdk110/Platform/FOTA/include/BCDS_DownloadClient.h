/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @defgroup    DownloadClient DownloadClient
 *
 *  @{
 *  @details
 *                                           - DownloadClient_SetupNewDownload()
 *                                           - DownloadClient_ResumeFromBlock()
 *                                           - DownloadClient_DownloadBlock()
 *                                           - DownloadClient_GetTargetLocation()
 *
 * ****************************************************************************/

#ifndef BCDS_DOWNLOADCLIENT_H_
#define BCDS_DOWNLOADCLIENT_H_

#include <Serval_Defines.h>
#include <Serval_Types.h>
#include <Serval_CoapClient.h>
#include <Serval_Scheduler.h>
#include <Serval_Ip.h>

/* local declaration */
/* Firmware download states.it's advertised via CoAP in the state resource
 */
enum DownloadClient_Status_E
{
    DL_DONE,
    DL_NOTFOUND,
    DL_TIMEOUT,
    DL_SEND_ERROR,
    DL_ERROR,
    DL_DATA,
    DL_REQUEST_SENT
};

/**
 * Type definition for the download state.
 */
typedef enum DownloadClient_Status_E DownloadClient_Status_T;
/**
 * @brief   A data type representing a callback function pointer for CoAP application.
 *          The application uses such callback functions in order to be notified about
 *          the download status and messages.
 */
typedef void (*DownloadClient_StatusCallback)(DownloadClient_Status_T status, const uint8_t* data, uint16_t size, uint32_t blockOptionValue);

/*
 * Should  be integrated into the stack, we acquire an endpoint and use that one to send
 * then address, port and dtls are encapsulated in that endpoint
 * Of course we need to make sure to keep that endpoint locked.
 */
struct DownloadClient_Context_S
{
    Ip_Address_T targetAddress;
    Ip_Port_T targetPort;
    const char* targetLocation;
    bool targetRequiresDTLS;
    bool confirmableRequests;

    uint32_t blockOptionValue;

    DownloadClient_StatusCallback onDownloadStatusChanged;
    Callable_T onSent;
};

/* local type and macro definitions */

/**
 * Type definition for the download context.
 */
typedef struct DownloadClient_Context_S DownloadClient_Context_T;

/* local function prototype declarations */

/**
 * @brief   This function is called to initiate a request from application. This function
 *          checks for any previous active transaction with the given server and if not
 *          provides the message to be filled for sending the request.
 *
 * @param[in] dlCtx
 *              Pointer to a DownloadContext_T structure that holds the necessary download information.
 *
 * @param[out] msgPntr
 *              This holds message to be filled by application for request.
 *
 * @retval  retcode_t
 *              RC_OK, if successful.
 *              RC_COAP_SECURE_CONNECTION_ERROR,if error in secure connect
 *              RC_COAP_CLIENT_INIT_REQ_ERROR, otherwise.
 */
static inline retcode_t DownloadClient_InitMsg(DownloadClient_Context_T* dlCtx, Msg_T** msgPntr)
{
#if SERVAL_ENABLE_DTLS_CLIENT
    if (dlCtx->targetRequiresDTLS)
    {
        retcode_t rc;
        rc = CoapClient_connectSecure(&dlCtx->targetAddress, dlCtx->targetPort);
        if ( rc != RC_OK && rc != RC_DTLS_ALREADY_CONNECTED )
        {
            return rc;
        }

        return CoapClient_initSecureReqMsg(&dlCtx->targetAddress, dlCtx->targetPort, msgPntr);
    }
    else
#endif
    {
        return CoapClient_initReqMsg(&dlCtx->targetAddress, dlCtx->targetPort, msgPntr);
    }
}

/**
 * @brief   This function is called to valid the given char is
 *           decimal or not.
 *
 * @param[in] digit
 *              character.
 *
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool DownloadClient_IsDecimalDigit(char digit)
{
    return (digit >= '0' && digit <= '9');
}

/**
 * @brief   This function is called to convert given char into decimal.
 *
 * @param[out] number
 *              Parameter for storing the converted value.
 *
 * @param[in] digit
 *              character for convert.
 */
static inline void DownloadClient_AddDecimalDigit(uint32_t* number, char digit)
{
    *number = (*number * 10) + (digit - '0');
}

/**
 * @brief   This function converts a given string to decimal address and returns the
 *          index or position.
 *
 * @param[in] str
 *              A string that contains a number to convert
 *
 * @param[out] number
 *              parameter to store converted value
 *
 * @retval  uint8_t
 *            returns total number of characters converted, if success
 *            zero, otherwise.
 */
static inline uint8_t DownloadClient_StrToUnsignedDec(const char* str, uint32_t* number)
{
    uint8_t index = 0;
    *number = 0;

    while (DownloadClient_IsDecimalDigit(str[index]))
    {
        DownloadClient_AddDecimalDigit(number, str[index]);
        ++index; /* incrementing the index */
        /* 2^32 = 4294967295 ==> 10 digits */
        if (10 == index)
        {
            return 0;
        }
    }
    return index;
}

/**
 * @brief   This function converts a given string to port address and returns the
 *          index or position.
 *
 * @param[in] str
 *              A valid pointer reference to the string.
 *
 * @param[out] port
 *               Reference to an port address for the result of the conversion. It has to be
 *               a valid pointer.
 *
 * @retval  uint8_t
 *            return total number of characters converted, if success
 *            zero, otherwise.
 */
static inline uint8_t DownloadClient_StringToPort(const char* str, Ip_Port_T* port)
{
    uint32_t portNumber = 0;
    uint8_t position = DownloadClient_StrToUnsignedDec(str, &portNumber);
    if (position == 0 || position > 5 || portNumber >= (1 << 16))
    {
        return 0;
    }
    *port = Ip_convertIntToPort(portNumber);

    return position;
}

/**
 * @brief   This function is called to valid the Block option and value for
 *          downloading,.
 *
 * @param[in] length
 *                 block options (1 to 3).
 *
 * @param[in] value
 *                 Block group value less than 256.
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool DownloadClient_IsValidGroup(uint8_t length, uint32_t value)
{
    return (length > 0 && length <= 3 && value <= 255);
}

/**
 * @brief   This function is called to parsing or serialize the IP,Port Address from DownloadContext_T
 *          structure and validating.
 *
 * @param[in] url
 *               Pointer to a buffer, it contains IP ,port address and URI path.
 *
 * @param[in] log2BlockSize
 *                Block size is in log2 value.
 *
 * @param[in] confirmableRequests
 *                downloading message is confirmed or unconfirmed.
 *
 * @param[in] DownloadStatusCallback
 *              This is the reference to the download status callback for received message. It
 *              has to be a valid pointer.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_APP_ERROR otherwise.
 */
retcode_t DownloadClient_SetupNewDownload(const char* uri, uint8_t log2BlockSize, bool confirmableRequests, DownloadClient_StatusCallback onDownloadStatusChanged);

/**
 * @brief   This function is called to resume the downloading from given
 *          block number.
 *
 * @param[in] blockNumber   Block number for downloading.
 *
 */
void DownloadClient_ResumeFromBlock(uint32_t blockNumber);

/**
 * @brief   This function is called to get the status of previous sending and make the
 *          CoAP should be ready for downloading.
 *
 * @retval  retcode_t
 *               RC_OK, if successful
 *               RC_APP_ERROR,RC_COAP_SENDING_ERROR otherwise.
 */
retcode_t DownloadClient_DownloadBlock(void);

/**
 * @brief   This function called to get destination path of downloading .
 *
 * @retval  const char*
 *             returns the Destination or URI path
 */
const char* DownloadClient_GetTargetLocation(void);

#endif /* BCDS_DOWNLOADCLIENT_H_ */

/**@} */
