/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @defgroup    FotaStateMachine FotaStateMachine
 *
 *  @{
 *  @brief      This module will handle, all FOTA related activities like Firmware Download, Fota resources State
 *              change, firmware storage and etc
 * ****************************************************************************/

#ifndef BCDS_FOTASTATEMACHINE_H_
#define BCDS_FOTASTATEMACHINE_H_

#include "Serval_Lwm2m.h"
#include "BCDS_FotaStorageAgent.h"

/* public type and macro definitions */

/** @brief These below macros represents  LWM2M standard state
 *         for Result resource of firmware update
 */
#define FOTASTATEMACHINE_FW_DOWNLOADED             (UINT8_C(3))
#define FOTASTATEMACHINE_FW_DEFAULT                (UINT8_C(0))
#define FOTASTATEMACHINE_FW_UPDATE_SUCCESS         (UINT8_C(1))
#define FOTASTATEMACHINE_FW_UPDATE_FAILED          (UINT8_C(8))
#define FOTASTATEMACHINE_FW_OUT_OF_STORAGE         (UINT8_C(2))
#define FOTASTATEMACHINE_FW_OUT_OF_MEMORY          (UINT8_C(3))
#define FOTASTATEMACHINE_FW_CONNECTION_LOST        (UINT8_C(4))
#define FOTASTATEMACHINE_FW_CRC_FAILED             (UINT8_C(5))
#define FOTASTATEMACHINE_FW_UNSUPPORTED_TYPE       (UINT8_C(6))
#define FOTASTATEMACHINE_FW_INVALID_URI            (UINT8_C(7))
#define LOG_MODULE                      "LWF"

/** Firmware download states */
enum FotaStateMachine_FW_State_E
{
    LWM2M_FW_STATE_IDLE = 0x00,
    LWM2M_FW_STATE_DOWNLOADING = 0x01,
    LWM2M_FW_STATE_DOWNLOADED = 0x02,
    LWM2M_FW_STATE_UPDATING = 0x03
};

/** Structure that contains information about an firmware resource */
struct FotaStateMachine_FW_UpdateObj_S
{
    Lwm2mResource_T firmwarePackage;
    Lwm2mResource_T uriFromWhereToDownload;
    Lwm2mResource_T update;
    Lwm2mResource_T state;
    Lwm2mResource_T updateSupportedObjects;
    Lwm2mResource_T updateResult;
};
/* typedef prototype for firmware resources */
typedef struct FotaStateMachine_FW_UpdateObj_S FotaStateMachine_FW_UpdateObj_T;

extern FotaStateMachine_FW_UpdateObj_T FotaStateMachine_FotaResource;

/** @brief  This enum represents the Firmware download states.
 *          The low nibble encodes the status that is advertised via LwM2M in the
 *          state resource ( 1: Idle, 2: Downloading, 3: Firmware Ready )
 */
enum FotaStateMachine_State_E
{
    FOTA_IDLE = (uint32_t) LWM2M_FW_STATE_IDLE | 0x00,
    /* Firmware download error notify state. After this sate it goes back to Idle state. */
    FOTA_FAILED_NOTIFY = (uint32_t) LWM2M_FW_STATE_IDLE | 0x10,
    /* Firmware coap block request state. */
    FOTA_DOWNLOADING_READY_FOR_REQUEST = (uint32_t) LWM2M_FW_STATE_DOWNLOADING | 0x00,
    /* Once Valid URI received on Idle state then moves to FOTA_DOWNLOADING_NOTIFY to do
     initial firmware download setup and goes to  FOTA_DOWNLOADING_READY_FOR_REQUEST state */
    FOTA_DOWNLOADING_NOTIFY = (uint32_t) LWM2M_FW_STATE_DOWNLOADING | 0x10,
    /* Waiting in this state for coap block response. once received the coap block then
     goes back to FOTA_DOWNLOADING_READY_FOR_REQUEST state */
    FOTA_DOWNLOADING_WAITING_FOR_RESPONSE = (uint32_t) LWM2M_FW_STATE_DOWNLOADING | 0x20,
    /* Stays in FOTA_DOWNLOADED after notifying till receives update trigger from the server */
    FOTA_DOWNLOADED = (uint32_t) LWM2M_FW_STATE_DOWNLOADED | 0x00,
    /* Notifies Download success to the server & goes to FOTA_DOWNLOADED */
    FOTA_DOWNLOADED_NOTIFY = (uint32_t) LWM2M_FW_STATE_DOWNLOADED | 0x10,
    /* Notifies the appication about the new firmware donladed and to do the firmware update */
    FOTA_UPDATING = (uint32_t) LWM2M_FW_STATE_UPDATING | 0x00,
    /* Notifies the server for fota update */
    FOTA_UPDATING_NOTIFY = (uint32_t) LWM2M_FW_STATE_UPDATING | 0x10,
    /* State to detect if the state machine is run before being initialized */
    FOTA_INVALID = 0x99,
    FOTA_STATE_NOT_IN_USERPAGE = 0xFF,
};
/* Type definition for the firmware download states */
typedef enum FotaStateMachine_State_E FotaStateMachine_State_T;

/**
 * @brief
 * FOTA Notification to application on various occasions. Notification can either be a FOTA State or
 * some other information to decide the application activity
 *
 * FOTA_STATE_NOTFY: If Notification message is "FOTA_STATE_NOTFY" then application can use the FotaStateMachine_State_T to decides
 * its activity based on the State.
 * When the notification is "FOTA_STATE_NOTFY" & State is "FOTA_UPDATING" then it is a New Firmware notification to application,
 * so application has to reboot to give control to the Bootloader. Before reboot it can do other necessary activities
 *
 * FOTA_VALID_URI_RECEIVED: After receiving "FOTA_VALID_URI_RECEIVED" notification only, application can poll the FotaStateMachine_Handler()
 * to proceed firmware download.
 *
 * FOTA_EMPTY_URI_RECEIVED: "FOTA_EMPTY_URI_RECEIVED" is notified to the application, upon receiving empty package URI from the Serval stack task context
 * Application should not block for this notification.
 *
 * FOTA_DELAY_REQUEST: On receiving "FOTA_DELAY_REQUEST" notification, application has to delay the FotaStateMachine_Handler polling
 * a minimum of 2 seconds for proper firmware download
 *
 * FOTA_SEND_FAILED: If "FOTA_SEND_FAILED" is received, the FotaStateMachine_Handler shall wait until the communicating interface
 * is ready to further communicate
 */
enum FotaStateMachine_MsgNotify_E
{
    FOTA_STATE_NOTFY = 0,
    FOTA_VALID_URI_RECEIVED,
    FOTA_EMPTY_URI_RECEIVED,
    FOTA_DELAY_REQUEST,
    FOTA_SEND_FAILED,
};

typedef enum FotaStateMachine_MsgNotify_E FotaStateMachine_MsgNotify_T; /* Type definition for the firmware download Notification */

/*
 * Enum that contains id for Fota state machine environment variables to read/write map the Persistent memory (ex. NVM,EEPROM,etc
 */

enum FotaStateMachine_ContextInfo_E
{
    FOTA_CONTEXT_STATE = 0,
    FOTA_CONTEXT_NEW_FIRMWARE,
    FOTA_CONTEXT_NEXT_BLOCK_NO,
    FOTA_CONTEXT_DOWNLOAD_IN_PROGRESS,
    FOTA_CONTEXT_URL,
    FOTA_CONTEXT_CURRENT_RESULT,
    FOTA_CONTEXT_MAX
};

/* typedef prototype for Fota state machine environment variable IDs */
typedef enum FotaStateMachine_ContextInfo_E FotaStateMachine_ContextInfo_T;

/**
 * @brief   The general purpose of this is to provide notification to the application to do the necessary
 * 			things on a particular state when it receives a notification.
 *
 * @param[out] State: Current state of the Fota state handler when the notification triggers.
 *
 * @return   RETCODE_OK, if successful.
 *           RETCODE_FAILURE otherwise.
 *
 */
typedef Retcode_T (*FotaStateMachine_Notification_T)(FotaStateMachine_State_T State, FotaStateMachine_MsgNotify_T notify);

/**
 * @brief 	    This function will handle fota download and the various notification into LWM2M depends on the FOTA state changes.
 *				application should continuously call this function after the fota initiate trigger from the server trough URI 
 *				to make further coap block download request till receiving the update notification. 
 * 
 * @Usage       @code
 *              //Application code
 *				void FotaTask(void * pvParameters)
 *				{
 *					BCDS_UNUSED(pvParameters);
 *					uint32_t TimeToFotaTask = UINT32_C(0); 
 *					TimeToFotaTask = xTaskGetTickCount();
 *   				for (;;)
 *   				{
 *						FotaStateMachine_Handler();
 *       				vTaskDelayUntil(&TimeToFotaTask, PowerMgt_GetMsDelayTimeInSystemTicks(FOTA_POLLING_TIME_MS));
 *					}
 *				}
 *
 * @retval Retcode_T
 *           RETCODE_OK, if successful.
 *           RETCODE_FAILURE otherwise.
 */
Retcode_T FotaStateMachine_Handler(void);

/**
 * @brief    	This function sets the Fota downloading State to known state by checking various Fota NVM variables on every Power
 *			 	and also update the result resource of the fota object. Also Initialise the application notification & Storage medium access
 * 				function pointers provided by the application for the callback. 
 *
 * @Usage       @code
 *              //Application code
 *				Retcode_T FotaAppNotification(Fota_State, Fota_NotifyMsg)
 *				{
 *				 	Retcode_T retval;
 *				 	if(Fota_State == FOTA_UPDATING)&&(Fota_NotifyMsg == FOTA_STATE_NOTFY)
 *				 	{
 *						retval = Do the necessary things before the the reboot
 *						if(RETCODE_OK == retval )
 *						{
 *						  reboot();
 *						}
 *					}
 *					if(FOTA_DELAY_REQUEST == Fota_NotifyMsg)
 *					{
 *						//delay calling FotaStateMachine_Handler() a minimum of 2 sec.
 *					}
 *					if(FOTA_VALID_URI_RECEIVED == Fota_NotifyMsg)
 *					{
 *						//start pooling FotaStateMachine_Handler() after this condition.
 *					}
 *					return retval;
 *				}
 *
 *				struct FotaStorageAgent_S const * FotaStorageAgent_handle = &SDCardStorage; 	
 *              FotaStateMachine_Init(FotaStorageAgent_handle, updatenotification)
 *              @endcode
 *
 * @note     	Please make sure to initialize the storage medium before calling FotaStateMachine_Init.  Application has to provide 
 *           	the proper Storage handle for accessing the storage medium and appropriate notification for the Fota update. This callback 
 *           	has to do the reboot so that bootloader get triggered after the update.
 *
 * @param [in]  StorageAgentHandle:  contains the storage medium handle
 *
 * @param [in]  notification:  contains the callback for the notification on fota update
 *
 * @retval RETCODE_OK - Initialization is  success
 *         RETCODE_FAILURE -Initialization is  failed
 */
Retcode_T FotaStateMachine_Init(FotaStorageAgentPtr_T StorageAgentHandle, FotaStateMachine_Notification_T notification);

/**
 * @brief   	The function is to get the current fota state.
 *
 * @param[out]  state: Current state of the Fota state handler.
 *
 */
FotaStateMachine_State_T FotaStateMachine_GetState(void);

/**
 * @brief       To Cancel an ongoing download
 *
 * @return RETCODE_OK - Cancellation is success
 *         RETCODE_FAILURE -Cancellation is failed
 */
Retcode_T FotaStateMachine_CancelDownload(void);

#ifdef CONFIG_EXTERNAL_NVM

/**
 * @brief   Fota state machine environment variables are stored in persistent memory which may vary for each project.
 *          State machine need to store/change these information while downloading the binary file.
 *
 * @param[In] id: Fota state machine environment variable IDs
 *
 * @param[In] data: Fota state machine environment variable to be stored
 *
 * @param[In] size: size of Fota state machine environment variable to be stored
 *
 */
Retcode_T FotaStateMachine_WriteFotaContext(FotaStateMachine_ContextInfo_T id, void * param, uint32_t size);

/**
 * @brief   Fota state machine environment variables are stored in persistent memory which may vary for each project.
 *          State machine need to store/change these information while downloading the binary file.
 *
 * @param[In] id: Fota state machine environment variable IDs
 *
 * @param[In] data: Fota state machine environment variable to be stored
 *
 * @param[In] size: size of Fota state machine environment variable to be stored
 *
 */
Retcode_T FotaStateMachine_ReadFotaContext(FotaStateMachine_ContextInfo_T id, void * param, uint32_t size);

#endif /* CONFIG_EXTERNAL_NVM */

#endif /* BCDS_FOTASTATEMACHINE_H_ */

/**@} */
