/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @defgroup    FWContainer FWContainer
 *
 *  @{
 *  @brief      This file contains types and functions to store and parse the bytes of the container header into an easily usable structure.
 *
 * ****************************************************************************/

#ifndef BCDS_FWContainer_H_
#define BCDS_FWContainer_H_

/* Include all headers which are needed by this file. */
#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"
#include "BCDS_Assert.h"
#include "BCDS_Fota.h"
#include "BCDS_FotaStorageAgent.h"

/* Put the type and macro definitions here */
#define FWCONTAINER_HEADER_SIZE	UINT16_C(512)
#define FWCONTAINER_FOOTER_SIZE	UINT16_C(256)

#define FWCONTAINER_HDR_START_ADR   UINT8_C(0)
#define FWCONTAINER_HDR_VER		 	FWCONTAINER_HDR_START_ADR
#define FWCONTAINER_HDR_VER_LEN		2
#define FWCONTAINER_HDR_SIZE		        (FWCONTAINER_HDR_VER + FWCONTAINER_HDR_VER_LEN)
#define FWCONTAINER_HDR_SIZE_LEN			2
#define FWCONTAINER_PRODUCT_CLASS			FWCONTAINER_HDR_SIZE + FWCONTAINER_HDR_SIZE_LEN
#define	FWCONTAINER_PRODUCT_CLASS_LEN		2
#define FWCONTAINER_PRODUCT_VARIANT			FWCONTAINER_PRODUCT_CLASS + FWCONTAINER_PRODUCT_CLASS_LEN
#define FWCONTAINER_PRODUCT_VARIANT_LEN		2
#define FWCONTAINER_RESERVED1_LEN			232
#define FWCONTAINER_FIRMWARE_VER			FWCONTAINER_PRODUCT_VARIANT + FWCONTAINER_PRODUCT_VARIANT_LEN + FWCONTAINER_RESERVED1_LEN
#define FWCONTAINER_FIRMWARE_VER_LEN		4
#define FWCONTAINER_FIRMWARE_SIZE			FWCONTAINER_FIRMWARE_VER + FWCONTAINER_FIRMWARE_VER_LEN
#define FWCONTAINER_FIRMWARE_SIZE_LEN		4
#define FWCONTAINER_RESERVED2_LEN			256
#define FWCONTAINER_FIRMWARE_CRC			FWCONTAINER_FIRMWARE_SIZE + FWCONTAINER_FIRMWARE_SIZE_LEN + FWCONTAINER_RESERVED2_LEN
#define FWCONTAINER_FIRMWARE_CRC_LEN		4
#define FWCONTAINER_HDR_CRC					FWCONTAINER_FIRMWARE_CRC + FWCONTAINER_FIRMWARE_CRC_LEN
#define FWCONTAINER_HDR_CRC_LEN				4
#define FWCONTAINER_FIRMWARE_SIG_LEN		256

/**
 * @brief   struct and type to comfortably store and access the single
 * 			parameters of the container header
 */
#pragma pack (push,1)
struct FWContainer_Information_S
{
    /* Header of container */
    uint16_t ContainerVersion; /* Default 1 */
    uint16_t HeaderSize; /* Size of the Header in bytes*/
    uint16_t ProductClass; /* Product + Minimum HW Version */
    uint16_t ProductVariant; /* Region */
    uint8_t ReservedBytes[232]; /* Reserved for future use */
    uint32_t FirmwareVersion; /* major, minor, patch */
    uint32_t FirmwareSize; /* Size of the binary in bytes*/
    uint8_t ReservedBytes2[256]; /* Reserved for the firmware */
    uint32_t FirmwareCRC; /* CRC of the firmware image */
    uint32_t HeaderCRC; /* CRC of the ContainerHeader */

    /* Footer of container */
    uint8_t FirmwareSignature[256]; /* Reserved for the firmware signature */
};

#pragma pack(pop)
typedef struct FWContainer_Information_S FWContainer_Information_T;

/* Put the function declarations here */

/**
 * @brief   This function parses the granted data buffer containing the raw
 * 			container header data into the submitted FWContainer_Header_T

 * @param[in] StorageAgentHandle: Handle to access the storage medium
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] Header: Pointer to the FWContainer_Header_T the parsed data
 * 			   should be stored to
 *
 * @return  RETCODE_SUCCESS: If the parsing process was successful
 * 			RETCODE_INVALID_PARAM: If a null pointer is handed as parameter
 */
Retcode_T FWContainer_ParseInformation(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, FWContainer_Information_T * Header);

/**
 * @brief   This function reads the header version of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] headerVersion: Pointer to the which header version should be stored.
 *
 * @return  RETCODE_SUCCESS : Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadHeaderVersion(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerVersion);

/**
 * @brief   This function reads the header size of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] headerSize: Pointer to the which header size should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadHeaderSize(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerSize);

/**
 * @brief   This function reads the product class of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] productClass: Pointer to the which product class should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadProductClass(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * productClass);

/**
 * @brief   This function reads the product Variant of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] productVariant: Pointer to the which product Variant should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadProductVar(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * productVariant);

/**
 * @brief   This function reads the Firmware Version of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] firewareVersion: Pointer to the which Firmware Version should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadFWVersion(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firewareVersion);

/**
 * @brief   This function reads the Firmware size of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] firewareSize: Pointer to the which Firmware size should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadFWSize(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firewareSize);

/**
 * @brief   This function reads the Firmware CRC of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] firmwareCrc: Pointer to the which Firmware CRC should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadFWCRC(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * firmwareCrc);

/**
 * @brief   This function reads the Header CRC of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] headerCrc: Pointer to the which Header CRC should be stored.
 *
 * @return  RETCODE_SUCCESS - Read process was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadHeaderCRC(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * headerCrc);

/**
 * @brief   This function reads the firmware signature of the given partition
 *
 * @param[in] StorageAgentHandle: Handle to access the storage medium.
 *
 * @param[in] Partition: Partition of the fota storage medium.
 *
 * @param[out] fwSignature: Pointer to the which firmware signature should be stored.
 *
 * @return  RETCODE_SUCCESS - Read Firmware Signature was successful,else an error code is returned.
 *
 */
Retcode_T FWContainer_ReadFWSignature(FotaStorageAgentPtr_T StorageAgentHandle, Fota_Partition_T Partition, uint8_t * fwSignature);

#endif /* BCDS_FWContainer_H_ */
/**@} */
