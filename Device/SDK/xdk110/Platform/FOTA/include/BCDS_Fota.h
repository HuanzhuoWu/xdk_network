/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 *  @file
 *  @defgroup    Fota Fota
 *
 *  @{
 *  @brief      This module provides  declarations which are commonly used by Fota
 * ****************************************************************************/
/**
 * @file
 * @brief
 *  This file contains types used commonly across the FOTA shared package
 */
#ifndef BCDS_FOTA_H_
#define BCDS_FOTA_H_

/* Include all headers which are needed by this file. */
#include "BCDS_Retcode.h"
#include "BCDS_FotaConfig.h"
#include "BCDS_FotaInfo.h"

/* public type and macro definitions */
#ifndef BCDS_FEATURE_MEDIUM_SDCARD
/**
 * This macro enables the build of the SDCard feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_MEDIUM_SDCARD 1
#endif

#ifndef BCDS_FEATURE_VALIDATION_AGENT
/**
 * This macro enables the build of the Fota Validation Agent feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_VALIDATION_AGENT 1
#endif

#ifndef BCDS_FEATURE_FW_CONTAINER
/**
 * This macro enables the build of the Firmware Container feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_FW_CONTAINER 1
#endif

#ifndef BCDS_FEATURE_STATEMACHINE
/**
 * This macro enables the build of the Fota StateMachine feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_STATEMACHINE 1
#endif

#ifndef BCDS_FEATURE_COAP_BLOCKWISE
/**
 * This macro enables the build of the Coap Blockwise feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_COAP_BLOCKWISE 1
#endif

#ifndef BCDS_FEATURE_DOWNLOAD_CLIENT
/**
 * This macro enables the build of the DownloadClient feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_DOWNLOAD_CLIENT 1
#endif

/**
 * @brief This enum represents to Identify the different FOTA Partitions
 */
enum Fota_Partition_E
{
    FOTA_PARTITION_PRIMARY,
    FOTA_PARTITION_BACKUP,
    FOTA_PARTITION_DOWNLOAD,
    FOTA_PARTITION_INVALID
};
typedef enum Fota_Partition_E Fota_Partition_T, *Fota_PartitionPtr_T;

/**
 * @brief module specific extension of the basic retcode
 */
enum Fota_Retcode_E
{
    /*Fota validation related failures*/
    FOTA_RETCODE_INTERNAL_ERROR = RETCODE_FIRST_CUSTOM_CODE,
    FOTA_RETCODE_CRC_ERROR,
    FOTA_RETCODE_INVALID_SIGNATURE,
    FOTA_RETCODE_INVALID_HEADER_VERSION,
    FOTA_RETCODE_RESOURCE_CHANGED_ERROR,
    FOTA_RETCODE_CONTAINER_READ_FAIL,
    FOTA_RETCODE_FIRMWARE_SIZE_FAIL,
    FOTA_RETCODE_FIRMWARE_VERSION_FAIL,
    /*Fota download related failures*/
    FOTA_DL_TIMEOUT,
    FOTA_DL_SEND_ERROR,
    /*NVM  related failures*/
    FOTA_FAILED_NVM_WRITE_DOWNLOAD_IN_PROGRESS,
    FOTA_FAILED_NVM_WRITE_FOTA_CURRENT_STATE,
    FOTA_FAILED_NVM_FLUSH,
    FOTA_FAILED_ENV_READ_CREDENTIALS,
    FOTA_FAILED_NVM_READ_CREDENTIALS,
    /*Fota status related failures*/
    FOTA_FAILED_RESULT_AND_STATE_UPDATE,
    FOTA_UPDATE_INVALID_URI_FAILED,
    FOTA_FAILED_UPDATE_GENERIC_ERROR,
    /*SDCard storage related failures*/
    SDCARD_NOT_DETECTED,
    SDCARD_FILE_OPEN_ERROR,
    SDCARD_FILE_WRITE_ERROR,
    SDCARD_FILE_READ_ERROR,
    SDCARD_FILE_ERASE_ERROR,
    SDCARD_FILE_CLOSE_ERROR,
    SDCARD_FILE_NOT_FOUND,
    SDCARD_OUT_OF_STORAGE,
    /*External Flash storage related failures*/
    EXTFLASH_FILE_OPEN_ERROR,
    EXTFLASH_FILE_WRITE_ERROR,
    EXTFLASH_FILE_READ_ERROR,
    EXTFLASH_FILE_ERASE_ERROR,
    EXTFLASH_FILE_CLOSE_ERROR,
    EXTFLASH_FILE_NOT_FOUND,
    EXTFLASH_OUT_OF_STORAGE,
    /*Fota call back api Error*/
    FOTA_DELAY_REQUEST_NOTIFY_FAIL,
    FOTA_SEND_FAILED_NOTIFY_FAIL,
    FOTA_RETCODE_MAX_ERROR
};

#endif /* BCDS_FOTA_H_ */

/**@} */
