/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 *  @file
 *  @defgroup    CoapBlockwise CoapBlockwise
 *
 *  @{
 *  @brief      This module provides declarations which are commonly used by CoapBlockwise
 *  @details
 *                                           - CoapBlockwise_AddBlockOption()
 *                                           - CoapBlockwise_GetBlockOptionValue()
 *
 * ****************************************************************************/

#ifndef BCDS_COAPBLOCKWISE_H_
#define BCDS_COAPBLOCKWISE_H_

/* local interface declaration */

/* local type and macro definitions */

/* Defines for COAP BlockWise Firmware Downloads */
#define COAPBLOCKWISE_INVALID                    (UINT32_C(0x0FFFFFFF))
#define COAPBLOCKWISE_DONE                       (UINT32_C(0xFFFFFFFF))
#define COAPBLOCKWISE_NUMBER_INVALID             (UINT32_C(0x000FFFFF))
#define COAPBLOCKWISE_MAX_BLOCK_OPTION           (UINT32_C(0x00ffffff))
#define COAPBLOCKWISE_VALID_BLOCK_OPTION         (UINT32_C(0x00FFFFF0))
#define COAPBLOCKWISE_VALIDATE_BLOCK_OPTION      (UINT32_C(0x00FFFFF8))
#define COAPBLOCKWISE_BLOCK_OPTION_1             (UINT32_C(0x00000001))
#define COAPBLOCKWISE_BLOCK_OPTION_2             (UINT32_C(0x00000100))
#define COAPBLOCKWISE_BLOCK_OPTION_3             (UINT32_C(0x00010000))
#define COAPBLOCKWISE_MAX_BLOCK_SIZE             (UINT16_C(1024))
#define COAPBLOCKWISE_MIN_BLOCK_SIZE             (UINT8_C(16))

/* local function prototype declarations */
/**
 * @brief   This function returns inverse function of the power of two
 *
 * @param[in] blockSize  size of the block
 *
 * @retval  uint8_t
 *            return power value of 2
 *
 */
static inline uint8_t CoapBlockwise_Log2(uint16_t blockSize)
{
    assert(blockSize != 0);

    uint8_t log2BlockSize = 0;
    while (blockSize >>= 1)
        ++log2BlockSize;
    return (log2BlockSize);
}

/**
 * @brief   This function returns the current block number
 *
 * @param[in] blockOptionValue
 *                   COAP Block option value for getting the block number
 *
 * @retval  uint8_t
 *               Block number
 *
 */
static inline uint32_t CoapBlockwise_GetBlockNumber(uint32_t blockOptionValue)
{
    return ((blockOptionValue & COAPBLOCKWISE_VALID_BLOCK_OPTION) >> 4);
}

/**
 * @brief   This function will set the block number.
 *
 * @param[out] blockOptionValue
 *                       setting the block number to COAP Block option for serialize.
 *
 * @param[in] blockNumber   Block number.
 *
 */
static inline void CoapBlockwise_SetBlockNumber(uint32_t* blockOptionValue, uint32_t blockNumber)
{
    assert(blockOptionValue != NULL);
    assert(blockNumber < (0x01 << 20));

    if( NULL != blockOptionValue)
    {
        *blockOptionValue = ((blockNumber << 4) | (*blockOptionValue & 0x0F));
    }
}

/**
 * @brief   Validating the final block
 *
 * @param[in] blockOptionValue
 *                   COAP Block option value for validating the final block.
 *
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool CoapBlockwise_IsFinalBlock(uint32_t blockOptionValue)
{
    return ((blockOptionValue & 0x08) ? false : true);
}

/**
 * @brief   This function set the final block of the download.
 *
 * @param[out] blockOptionValue   setting the COAP Block option value
 *
 * @param[in] isFinalBlock   true or false.
 *
 */
static inline void CoapBlockwise_SetFinalBlock(uint32_t* blockOptionValue, bool isFinalBlock)
{
    assert(blockOptionValue != NULL);
    if( NULL != blockOptionValue)
    {
        *blockOptionValue = (isFinalBlock ? 0x00 : 0x08) | (*blockOptionValue & ~0x08);
    }
}

/**
 * @brief   This function returns the Block size
 *
 * @param[in] blockOptionValue
 *                   Block option value received from COAP
 *
 * @retval  uint16_t
 *               Size of the block
 */
static inline uint16_t CoapBlockwise_GetBlockSize(uint32_t blockOptionValue)
{
    return (0x10 << (blockOptionValue & 0x07));
}

/**
 * @brief   This function set size of the block for downloading.
 *
 * @param[out] blockOptionValue
 *                        set the block size into COAP Block option(CoapOption_T)
 *
 * @param[in] blockSize   size to set the block option for serialize.
 *
 */
static inline void CoapBlockwise_SetBlockSize(uint32_t* blockOptionValue, uint16_t blockSize)
{
    assert(blockOptionValue != NULL);
    assert(blockSize <= COAPBLOCKWISE_MIN_BLOCK_SIZE);
    assert(blockSize >= COAPBLOCKWISE_MAX_BLOCK_SIZE);

    if( NULL != blockOptionValue)
    {
        *blockOptionValue = (*blockOptionValue & COAPBLOCKWISE_VALIDATE_BLOCK_OPTION) | (CoapBlockwise_Log2(blockSize) - 4);
    }
}

/**
 * @brief   Validating the serialize block option for downloading.
 *
 * @param[in] blockOptionValue
 *                   Block option value received from COAP.
 *
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool CoapBlockwise_ValidBlockOptionValue(uint32_t blockOptionValue)
{
    return ((blockOptionValue < COAPBLOCKWISE_MAX_BLOCK_OPTION) && ((blockOptionValue & 0x07) < 7));
}

/**
 * @brief   Validating the received Block number by using Block Option.
 *
 * @param[in] sentBlockOptionValue
 *                   Block option value send to COAP.
 *
 * @param[in] receivedBlockOptionValue
 *                   Block option received from COAP.
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool CoapBlockwise_IsExpectedBlock(uint32_t sentBlockOptionValue, uint32_t receivedBlockOptionValue)
{
    return ((sentBlockOptionValue & COAPBLOCKWISE_VALID_BLOCK_OPTION) == (receivedBlockOptionValue & COAPBLOCKWISE_VALID_BLOCK_OPTION));
}

/**
 * @brief   Validating the received Block size by using Block Option.
 *
 * @param[in] sentBlockOptionValue
 *                   Block option value send to COAP.
 *
 * @param[in] receivedBlockOptionValue
 *                   Block option received from COAP.
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool CoapBlockwise_IsExpectedSize(uint32_t sentBlockOptionValue, uint32_t receivedBlockOptionValue)
{
    return ((sentBlockOptionValue & 0x07) == (receivedBlockOptionValue & 0x07));
}

/**
 * @brief   Validating payload size by using received Block Option from serval.
 *
 * @param[in] blockOptionValue
 *                   Block option received from COAP.
 *
 * @param[in] payloadSize
 *                   received block size.
 * @retval  bool
 *               True, if success
 *               false otherwise
 */
static inline bool CoapBlockwise_IsCorrectPacketSize(uint32_t blockOptionValue, uint16_t payloadSize)
{
    return (CoapBlockwise_IsFinalBlock(blockOptionValue) || CoapBlockwise_GetBlockSize(blockOptionValue) == payloadSize);
}

/**
 * @brief   This function used to set the new block to receive or indicate to
 *          the COAP to start downloading from particular block
 *
 * @param[in] log2BlockSize  block number to start downloading
 *
 * @retval uint32_t
 *                   returns block option to set CoapOption_T object .
 */
static inline uint32_t CoapBlockwise_NewTransfer(uint8_t log2BlockSize)
{
    if (log2BlockSize >= 4 && log2BlockSize <= 10)
    {
        return (log2BlockSize - 4);
    }
    else
    {
        return (COAPBLOCKWISE_INVALID);
    }
}

/**
 * @brief   This function used to get next block .
 *
 * @param[in] blockOptionValue Block number
 *
 * @retval uint32_t
 *                   returns next block number to receive.
 */
static inline uint32_t CoapBlockwise_GetNextBlock(uint32_t blockOptionValue)
{
    if (CoapBlockwise_IsFinalBlock(blockOptionValue))
    {
        return COAPBLOCKWISE_DONE;
    }
    else if (CoapBlockwise_GetBlockNumber(blockOptionValue) == COAPBLOCKWISE_NUMBER_INVALID)
    {
        return COAPBLOCKWISE_INVALID;
    }
    else
    {
        return (blockOptionValue + 0x10) & ~0x08;
    }
}

/**
 * @brief   This function returns block offset from the Block Option.
 *
 * @param[in] blockOption
 *                      Reference to a CoapOption_T object where the option value should be stored.
 *                      It has to be a valid pointer.
 *
 * @retval uint32_t
 *                   return particular Block offset.
 */
static inline uint32_t CoapBlockwise_GetBlockOffset(uint32_t blockOptionValue)
{
    return ((blockOptionValue & COAPBLOCKWISE_VALID_BLOCK_OPTION) << (blockOptionValue & 0x07));
}

/**
 * @brief   This function validate the maximum block size to avoid unexpected issue .
 *
 * @param[in] blockOptionValue
 *                      Reference to a CoapOption_T object where the option value should be stored.
 *                      It has to be a valid pointer.
 *
 * @param[in] maxBlockSize
 *                      Size of the Block or number of the bytes to receive from server.
 */
static inline void CoapBlockwise_EnforceBlockSizeCeiling(uint32_t* blockOptionValue, uint16_t maxBlockSize)
{
    assert(blockOptionValue != NULL);

    if(NULL != blockOptionValue)
    {
        if (CoapBlockwise_GetBlockSize(*blockOptionValue) > maxBlockSize)
        {
            CoapBlockwise_SetBlockSize(blockOptionValue, maxBlockSize);
        }
    }
}

/**
 * @brief   This function is called to serialize a option in the outgoing message. It calculates
 *          the length and the position of option value from passed blockOptionValue and the serializing
 *          context and populate the buffer with these values.
 *
 * @param[in] serializer
 *                  Reference to a CoapSerializer_T object which identifies the current
 *                  instance of serializing context which should be used for serializing. It has
 *                  to be a valid pointer.
 *
 * @param[in] msg_ptr
 *                  Reference to a Msg_T object which represents current outgoing message.
 *
 * @param[in] blockOptionValue
 *                 Reference to a CoapOption_T object which should hold the value of option to
 *                 be serialized.
 *
 * @retval  retcode_t
 *          RC_OK, if successful
 *          RC_COAP_SERIALIZE_ERROR otherwise.
 *
 */
retcode_t CoapBlockwise_AddBlockOption(CoapSerializer_T* serializer, Msg_T* msg_ptr, uint32_t blockOptionValue);

/**
 * @brief   This function returns the particular Block for a given option number.
 *
 * @param[in] blockOption
 *                      Reference to a CoapOption_T object where the option value should be stored.
 *                      It has to be a valid pointer.
 *
 * @retval uint32_t
 *                   return particular Block Value.
 *                   COAPBLOCKWISE_INVALID, if the option was not found.
 */
uint32_t CoapBlockwise_GetBlockOptionValue(CoapOption_T* blockOption);

#endif /* BCDS_COAPBLOCKWISE_H_ */

/**@} */
