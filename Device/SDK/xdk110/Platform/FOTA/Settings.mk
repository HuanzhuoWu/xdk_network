#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# Package Information
# Package name is used for generating the library. lib<PackageName>_<BCDS_DEVICE_TYPE>.a
BCDS_PACKAGE_NAME = FOTA
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 26

# Internal Path Settings
BCDS_INCLUDE_DIR := include
BCDS_SOURCE_DIR := source
BCDS_PROTECTED_DIR := protected
BCDS_CONFIG_DIR := config
BCDS_OBJECT_DIR := objects
BCDS_LINT_DIR := lint

#Path of Platform, Libraries and Tools
BCDS_TOOLS_DIR ?= ./../../Tools
BCDS_LIBRARIES_DIR = ./../../Libraries
BCDS_PLATFORM_DIR = ./../../Platform

#Tool chain definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
BCDS_LINT_CONFIG_FILE_PATH := $(BCDS_TOOLS_DIR)/pclint/config
BCDS_LINT_CONFIG_FILE ?= $(BCDS_TOOLS_DIR)/pclint/config/bcds.lnt
BCDS_LINT_EXE = $(BCDS_TOOLS_DIR)/pclint/exe/lint-nt.launch
BCDS_LINT_PATH ?= $(BCDS_TOOLS_DIR)/PCLint/exe

# Library Paths
BCDS_CYCURL_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/Escrypt_CycurTLS/CycurTLS/src/cycurlib

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)

#This flag is used to generate dependency files
BCDS_DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#External Platform Package Dependencies
BCDS_UTILS_DIR ?= $(BCDS_PLATFORM_DIR)/Utils
BCDS_MISCDRIVERS_DIR ?= $(BCDS_PLATFORM_DIR)/MiscDrivers
BCDS_BASICS_DIR ?= $(BCDS_PLATFORM_DIR)/Basics
BCDS_HAL_DIR ?= $(BCDS_PLATFORM_DIR)/HAL
BCDS_FREE_RTOS_DIR ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS
BCDS_REFBOARDS_DIR ?= $(BCDS_PLATFORM_DIR)/RefBoards

#Path of the generated static library and its object files
BCDS_DEBUG_PATH ?= ./debug
BCDS_RELEASE_PATH ?= ./release
BCDS_DEBUG_OBJECT_PATH ?= $(BCDS_DEBUG_PATH)/$(BCDS_OBJECT_DIR)
BCDS_RELEASE_OBJECT_PATH ?= $(BCDS_RELEASE_PATH)/$(BCDS_OBJECT_DIR)

#Lint Path
BCDS_DEBUG_LINT_PATH = $(BCDS_DEBUG_PATH)/$(BCDS_LINT_DIR)
# This variable should fully specify the build configuration of the Serval 
# Stack library with regards the enabled and disabled features as well as 
# the configuration of each enabled feature.
export SERVAL_FEATURES_CONFIG ?= \
	-D SERVAL_LOG_LEVEL=SERVAL_LOG_LEVEL_ERROR\
	-D SERVAL_ENABLE_HTTP_CLIENT=1\
	-D SERVAL_ENABLE_HTTP_SERVER=1\
	-D SERVAL_ENABLE_WEBSERVER=1\
	-D SERVAL_ENABLE_COAP_OBSERVE=1\
	-D SERVAL_ENABLE_COAP_CLIENT=1\
	-D SERVAL_ENABLE_COAP_SERVER=1\
	-D SERVAL_ENABLE_REST_CLIENT=1\
	-D SERVAL_ENABLE_REST_SERVER=1\
	-D SERVAL_ENABLE_REST_HTTP_BINDING=1\
	-D SERVAL_ENABLE_REST_COAP_BINDING=1\
	-D SERVAL_ENABLE_XUDP=1\
	-D SERVAL_ENABLE_DPWS=0\
	-D SERVAL_ENABLE_TLS_CLIENT=1\
	-D SERVAL_ENABLE_TLS_SERVER=1\
	-D SERVAL_ENABLE_TLS_ECC=1\
	-D SERVAL_ENABLE_DTLS=1\
	-D SERVAL_ENABLE_DTLS_CLIENT=1\
	-D SERVAL_ENABLE_DTLS_SERVER=1\
	-D SERVAL_ENABLE_DTLS_PSK=1\
	-D SERVAL_ENABLE_HTTP_AUTH=1\
	-D SERVAL_ENABLE_HTTP_AUTH_DIGEST=1\
	-D SERVAL_ENABLE_DUTY_CYCLING=1\
	-D SERVAL_ENABLE_APP_DATA_ACCESS=0\
	-D SERVAL_ENABLE_COAP_COMBINED_SERVER_AND_CLIENT=1\
	-D SERVAL_ENABLE_COAP_OBSERVE=1\
	-D SERVAL_ENABLE_LWM2M=1\
	-D SERVAL_ENABLE_XTCP=1\
	-D SERVAL_ENABLE_XTCP_SERVER=1\
	-D SERVAL_ENABLE_XTCP_CLIENT=1\
	-D SERVAL_HTTP_MAX_NUM_SESSIONS=3\
	-D SERVAL_HTTP_SESSION_MONITOR_TIMEOUT=4000\
	-D SERVAL_MAX_NUM_MESSAGES=3\
	-D SERVAL_MAX_SIZE_APP_PACKET=600\
	-D SERVAL_MAX_SECURE_SOCKETS=5\
	-D SERVAL_MAX_SECURE_CONNECTIONS=5\
	-D SERVAL_DOWNGRADE_TLS=1\
	-D SERVAL_TLS_CYASSL=1 \
	-D COAP_MAX_NUM_OBSERVATIONS=50 \
	-D LWM2M_MAX_NUM_OBSERVATIONS=50 \
	-D SERVAL_HTTP_MAX_LENGTH_URL=256

export CYCURTLS_FEATURES_CONFIG = \
	-DEscTls_CIPHER_SUITE=TLS_PSK_WITH_AES_128_CCM_8 \
	-DEscHashDrbg_SHA_TYPE=256 \
	-DEscDtls_MAX_COOKIE_SIZE=64 \
	-DEscTls_PSK_IDENTITY_LENGTH_MAX=64 \
	-DEscTls_MAXIMUM_FRAGMENT_LENGTH=256 \
	-DEscTls_KEEP_SESSION_ID=0 \
	-DSERVAL_ENABLE_DTLS_HEADER_LOGGING=1 \
	-DCYCURTLS_HAVE_GET_CURRENT_TASKNAME=1	
	
# Mind the space behinde '-I' in the following declaration. It is assumed here
# that the includes are defined in the form "-I one/path -I second/path ..."
BCDS_SERVAL_CONFIG_LINT := $(subst -D , +d,$(SERVAL_FEATURES_CONFIG))
BCDS_CYCURTLS_CONFIG_LINT := $(subst -D , +d,$(CYCURTLS_FEATURES_CONFIG))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -I,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_FOTA_INCLUDES_LINT := $(subst -I,-i,$(BCDS_FOTA_INCLUDES))
BCDS_LINT_CONFIG = -i$(BCDS_LINT_CONFIG_PATH)

