##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################

include Settings.mk

# Build chain settings
ifndef BCDS_BUILD_LINUX
CC =  $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR =  $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
LINT_EXE = $(BCDS_LINT_PATH)/lint-nt.launch
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

RMDIRS := rm -rf

BCDS_ARCH_FLAGS = \
	-mcpu=cortex-m3 -mthumb

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Werror -Wextra -Wstrict-prototypes \
	$(BCDS_ARCH_FLAGS) -ffunction-sections -fdata-sections \
	-D $(BCDS_DEVICE_ID)  -D $(BCDS_DEVICE_PACKAGE_TYPE) 
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -D BCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID)
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

# Source and object files
BCDS_BASICS_SOURCE_FILES = \
	$(BCDS_BASICS_SOURCE_DIR)/Assert.c \
	$(BCDS_BASICS_SOURCE_DIR)/Retcode.c

BCDS_BASICS_OBJECT_FILES = \
	$(patsubst $(BCDS_BASICS_SOURCE_DIR)/%.c, %.o, $(BCDS_BASICS_SOURCE_FILES))

BCDS_BASICS_LINT_FILES = \
	$(BCDS_BASICS_SOURCE_FILES:.c=.lob)

BCDS_BASICS_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_BASICS_DEBUG_OBJECT_PATH)/,$(BCDS_BASICS_OBJECT_FILES))

BCDS_BASICS_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_BASICS_DEBUG_OBJECT_PATH)/, $(BCDS_BASICS_OBJECT_FILES:.o=.d))
	
BCDS_BASICS_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_BASICS_RELEASE_OBJECT_PATH)/,$(BCDS_BASICS_OBJECT_FILES))

BCDS_BASICS_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_BASICS_RELEASE_OBJECT_PATH)/, $(BCDS_BASICS_OBJECT_FILES:.o=.d))
	
BCDS_BASICS_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_BASICS_DEBUG_LINT_PATH)/, $(BCDS_BASICS_LINT_FILES))

######################## Build Targets #######################################
.PHONY: debug
debug: $(BCDS_BASICS_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_BASICS_RELEASE_LIB)

############# Generate BASICS Library for BCDS ###################
$(BCDS_BASICS_DEBUG_LIB): $(BCDS_BASICS_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BASICS for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BASICS_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_BASICS_RELEASE_LIB): $(BCDS_BASICS_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BASICS for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BASICS_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"
	
# compile C files
$(BCDS_BASICS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_BASICS_SOURCE_DIR)/%.c
	@echo $(BCDS_BASICS_INCLUDES)
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
	       -I . $(BCDS_BASICS_INCLUDES)  $< -o $@


$(BCDS_BASICS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_BASICS_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_BASICS_INCLUDES) $< -o $@

.PHONY: lint
lint: $(BCDS_BASICS_DEBUG_LINT_FILES)
	@echo "Lint End"

# Lint flags
LINT_CONFIG = \
	+d$(DEVICE)=1 \
	+d$(BCDS_DEVICE_PACKAGE_TYPE)

$(BCDS_BASICS_DEBUG_LINT_PATH)/%.lob: %.c
	@mkdir -p $(@D)
	@echo $@
	@$(LINT_EXE) $(BCDS_BASICS_INCLUDES_LINT) $(LINT_CONFIG) $(BCDS_LINT_CONFIG_FILE_PATH)/bcds.lnt $< -oo[$@]
	@echo "===================================="

.PHONY: lint_clean
lint_clean :
	@echo Cleaning Lint files
	@$(RMDIRS) $(BCDS_BASICS_DEBUG_LINT_PATH)
	@echo Lint files cleaned

.PHONY: clean	
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_BASICS_DEBUG_PATH) $(BCDS_BASICS_RELEASE_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_BASICS_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_BASICS_DEBUG_PATH)
	@echo $(BCDS_BASICS_DEBUG_OBJECT_FILES)