/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_RETCODE_H_
#define BCDS_RETCODE_H_

/* additional interface header files */
#include "BCDS_Basics.h"

#ifndef BCDS_PACKAGE_ID
#warning "BCDS_PACKAGE_ID not defined. The BCDS_PACKAGE_ID is set to the default value 0"
#define BCDS_PACKAGE_ID 0
#endif

/**
 * @brief This is the return code data type of the BCDS platform.
 * Each retcode is composed of three parts: package, severity and code.
 *
 * The package (most left byte - 8-bit value) indicates the package where the retcode
 * has been raised.
 *
 * The severity  (2nd left byte - 8-bit value) indicates the harshness of
 * the retcode, i.e. warning, error, fatal error or just an info.
 *
 * The code (2 right bytes - 16 bit value) indicates the error code.
 * The first 255 errors are reserved as general error code. Packages and projects
 * may define their own codes starting from the code 256 (up to the code 65535).
 *
 *
 * @see  Retcode_Severity_T
 */
typedef uint32_t Retcode_T;

/**
 * @brief	Enumeration to represent the General return codes.
 * The first 255 errors are reserved as general error code.
 * Packages and projects may define their own codes starting
 * from the code 256 (up to the code 65535).
 */
enum Retcode_General_E
{
    RETCODE_SUCCESS = 0,
    RETCODE_FAILURE,
    RETCODE_OUT_OF_RESOURCES,
    RETCODE_INVALID_PARAM,
    RETCODE_NOT_SUPPORTED,
    RETCODE_INCONSITENT_STATE,
    RETCODE_UNINITIALIZED,
	RETCODE_NULL_POINTER,
	RETCODE_RTOS_QUEUE_ERROR,
	RETCODE_UNEXPECTED_BEHAVIOR,
	RETCODE_SEMAPHORE_ERROR,
    //...
    RETCODE_FIRST_CUSTOM_CODE = 65,
};

/**
 * @brief	Enumeration to represent the Severity of errors.
 */
enum Retcode_Severity_E
{
    RETCODE_SEVERITY_NONE = 0,
    RETCODE_SEVERITY_INFO,
    RETCODE_SEVERITY_WARNING,
    RETCODE_SEVERITY_ERROR,
    RETCODE_SEVERITY_FATAL,
};

/**
 * @brief	Typedef to represent the Severity of error.
 */
typedef enum Retcode_Severity_E Retcode_Severity_T;

/**
 * @brief	Typedef to represent the Callback function to be called
 * for handling the Error.
 */
typedef void (* Retcode_ErrorHandlingFunc_T)(Retcode_T error);

/**
 * @brief 	This function initializes the Retcode module. It registers an
 * 			error handling callback function. When an error is raised,
 * 			registered callback function of the corresponding component is called.
 *
 * @param [in]	func : A callback function which is executed when an error is raised.
 */
void Retcode_initialize(Retcode_ErrorHandlingFunc_T func);

/**
 * @brief	Each component may use this function to raise an error to the application.
 * This call results in the invocation of the error handling function which has
 * been given while the initialization of Retcode using the function
 * Retcode_initialize().
 *
 * @param [in]	error : The error to be raised to the application
 *
 * @see Retcode_initialize
 * @see Retcode_ErrorHandlingFunc_T
 */
void Retcode_raiseError(Retcode_T error);

/**
 * @brief	This function composes a return code based on input parameters.
 *
 * @param [in]	package : Package ID of the corresponding module
 * @param [in]	severity : Severity of the error
 * @param [in]	code : Error code
 */
Retcode_T Retcode_compose(uint32_t package, Retcode_Severity_T severity, uint32_t code);

/**
 * @brief	A macro which composes a retcode from its three parts package, severity
 * and code. While severity and code are passed as parameters, the package is
 * taken from the macro BCDS_PACKAGE_ID.
 */
#define RETCODE(severity, code)  (Retcode_compose(BCDS_PACKAGE_ID, severity, code))

/**
 * @brief	A helper macro to return success
 */
#define RETCODE_OK				((Retcode_T) 0)

/**
 * @brief	This function provides the Package of a return code.
 *
 * @param [in]	retcode : return code
 *
 * @retval	uint32_t : Package ID of the corresponding module
 */
static BCDS_INLINE uint32_t Retcode_getPackage(Retcode_T retcode)
{
	return (((uint32_t) retcode >> 24) & 0x000000FFUL);
}

/**
 * @brief	This function provides the Severity of a return code.
 *
 * @param [in]	retcode : return code
 *
 * @retval	uint32_t : Severity of the error
 */
static BCDS_INLINE Retcode_Severity_T Retcode_getSeverity(Retcode_T retcode)
{
	return ((Retcode_Severity_T) (((uint32_t) retcode >> 16) & 0x000000FFUL));
}

/**
 * @brief	This function provides the Error code of a return code.
 *
 * @param [in]	retcode : return code
 *
 * @retval	uint32_t : Error code
 */
static BCDS_INLINE uint32_t Retcode_getCode(Retcode_T retcode)
{
	return ((uint32_t) retcode & 0x0000FFFFUL);
}

#endif /* BCDS_RETCODE_H_ */

/** ************************************************************************* */
