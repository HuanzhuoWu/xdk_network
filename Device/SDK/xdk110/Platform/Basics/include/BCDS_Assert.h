/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_ASSERT_H_
#define BCDS_ASSERT_H_

/* additional interface header files */
#include "BCDS_Retcode.h"

#if (__STDC_VERSION__ <= 199901L) /* check to C99 mode */

#ifndef __FILE__
#define __FILE_ 	"Filename not defined"
#endif

#ifndef __LINE__
#define __LINE__ 	0
#endif

/* Helper macro - direct use is forbidden! */
#define ASSERT_CONCAT_(a, b) a##b

/* Helper macro - direct use is forbidden! */
#define ASSERT_CONCAT(a, b) ASSERT_CONCAT_(a, b)

/**
 * @brief 	Static (compilation time) assertion macro.
 * 			The C99 standard does not support static assertions, nor the sizeof() keyword
 * 			in preprocessor macros. To be able to perform compilation time variable size
 * 			related checks, this macro is provided as a workaround. Note that the macro
 * 			has some limitations.
 *
 * @warning This macro can't be used twice on the same __LINE__ number.
 *          The user must ensure that even in header files the __LINE__
 *          number is always unique. Protect header files against multiple
 *          time inclusions and if possible put static assertions into C
 *          files instead of headers.
 *
 * @example To check if the int integer type is 4 bytes long, the following
 *          static assertion can be used:
 *
 *          static_assert(sizeof(int) == 4, "The int type shall be 32 bits long.");
 *
 * @param [in]	expression : A logical statement which can be true or false.
 * @param [in]	message : A string about the test case - note that it is not compiled into the code.
 */
#define static_assert(expression, message) \
	enum { ASSERT_CONCAT(ASSERT_LINE_, __LINE__) = 1/(!!(expression)) };

#else
#warning "The compiler shall build the code in C99 mode to make use of static_assert(). \
	Currently static_assert() implementation is restricted to C99 mode only"
#endif /* __STDC_VERSION__ */

#ifndef NDEBUG /* valid only for debug builds */

#undef assert /* removing the default compiler assert function */

/**
 * @brief 	Program assertion macro.
 * 			The program assertion macro implements an assertion, which can be used to
 * 			verify assumptions made by the program and get notified of this information
 * 			through a callback if this assumption is false and stop program execution.
 * 			Program assertion is behaving differently in debug and release builds.
 *
 * @note 	Release builds must be built with the NDEBUG symbol defined.
 *
 * @param [in]	expression : A logical statement which can be true or false.
 */
#define assert(expression) \
	do{ if (!(expression)) { Assert_dynamic((unsigned long)__LINE__,(unsigned char*)__FILE__); } } while (0)

/**
 * @brief   This data type represents a function pointer which would be executed
 * 			when an assertion is triggered.
 *
 * @note    Ideally the first operation that is expected from the API implementing
 * 			this functionality is to disable all the Interrupts of the system
 * 			(based on the Hardware) and if required, it can trace / LOG the line
 * 			number and filename from where the assertion came.
 *
 * @param [in]	line : The line number of the source code from where the assert() is called.
 *
 * @param [in]	file : The file name of the source code from where the assert() is called.
 */
typedef void (*Assert_Callback_T)(const unsigned long line, const unsigned char * const file);

/**
 * @brief 	This function initializes the Assert module. It registers an
 * 			assertion callback function. When an assertion is hit, registered
 * 			callback function is called.
 *
 * 			Ideally, the first thing to be done in the callback function it to
 * 			disable the interrupts of the system (hardware dependent). For trouble shooting,
 * 			it can also trace / log the line number and filename from where the assertion
 * 			came.
 *
 * @note 	Release builds must be built with the NDEBUG symbol defined.
 * 			This API should be called before making use of any one of the
 * 			assertion API's except for the static_assert().
 *
 * @param [in]	callback : A callback function which is executed when an assertion is triggered.
 *
 * @retval	Status of the initialization
 *
 * @retval	RETCODE_OK
 * @retval	RETCODE_INVALID_PARAM
 */
Retcode_T Assert_initialize(Assert_Callback_T callback);

/**
 * @brief 	Dynamic assert function.
 * 			The dynamic assert function calls a user mapped callback function
 * 			(where atleast all the interrupts disabling is expected) and would
 * 			freeze the system by entering an endless loop.
 *
 * @note 	Release builds must be built with the NDEBUG symbol defined.
 * 			User shall not use this API and rather call the assert() macro with
 * 			required assertion expression. Defining the ASSERT_USE_STD_EXIT macro
 * 			in CFLAGS_DEBUG make variable would use the standard C exit() API.
 *
 * @param [in]	line : The line number of the source code from where the assert() is called.
 *
 * @param [in]	file : The file name of the source code from where the assert() is called.
 */
void Assert_dynamic(const unsigned long line, const unsigned char * const file);

#else /* valid only for release builds */

/**
 * @note 	For release builds where NDEBUG flag is defined
 * 			dynamic assertion and Assert initialize function
 * 			call are disabled. The Callback function is made void
 * 			to suppress unused function warning.
 */
#define assert(expression)								((void)0)
#define Assert_initialize(callback)						((void)callback)

#endif /* NDEBUG */

#endif /* BCDS_ASSERT_H_ */

/** ************************************************************************* */
