/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 
/**
 * @defgroup PAL_scheduler PAL Scheduler
 * @brief PAL Scheduler
 * @ingroup Serval_pal
 *
 *  @{
 *  @brief Interface header for the PAL_scheduler module.
 *
 *  The interface header exports the following features:
 *  								-PAL_schedulerInitialize()
 *  								-PAL_schedulerDequeue()
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef PAL_SCHEDULER_IH_H_
#define PAL_SCHEDULER_IH_H_

/* public interface declaration ********************************************* */
#include "Serval_Callable.h"

/* public type and macro definitions */
#define PAL_SCHEDULAR_QUEUE_SIZE 		 UINT32_C(100) /* Maximum number of queues which can be registered */
#if SERVAL_ENABLE_DTLS||SERVAL_ENABLE_TLS
#define PAL_DEENQUE_TASK_SIZE            UINT16_C(1000)           /**< Macro to represent Task size for RTOS task */
#else
#define PAL_DEENQUE_TASK_SIZE            UINT16_C(500)           /**< Macro to represent Task size for RTOS task */
#endif
#define PAL_DEENQUE_PRIORITY             UINT32_C(3)             /**< Macro to represent task priority */

/**
 *@brief Structure saving the data of the function handles enqueued
*/
typedef struct PAL_event_s
{
	Callable_T *callable_ptr;
	retcode_t status;
}PAL_event_t;

/* public function prototype declarations */

/* public global variable declarations */

/**
 *@brief This routine completes the Scheduler Initialization
 *@retval retcode_t RC_OK                : If queue has been created.
 *                  RC_SCHED_INVALID_JOB : If queue has not been created successfully
 */
extern retcode_t PAL_schedulerInitialize(void);

/**
 *@brief This routine will dequeue the registered callback function form the scheduler queue
 *@param[in] callablePptr
 *         Callback function pointer
 *@param[in] statusPtr
 *         The status of the registered callback function.
 *@retval retcode_t RC_OK                : If queue has been created.
 *                  RC_SCHED_INVALID_JOB : If function hadnle assigned is NULL
 *                  RC_SCHED_EMPTY       : If queue is empty
 */
extern retcode_t PAL_schedulerDequeue(Callable_T **callablePptr, retcode_t *statusPtr);


/* inline function definitions */

#endif /* PAL_SCHEDULER_IH_H_ */
/**@} */
/** ************************************************************************* */
