/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/


/**
 * @file
 *
 * @brief The interface header implements Serval stack's header file dependency called PIp.h.
 *
 * The interface header exports the following features below.
 * -Ip_Address_T
 * -Ip_Port_T
 * -Tcp_Socket_T
 * -Udp_Socket_T
 * -Tcp_Listener_T
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef PIP_H_
#define PIP_H_

/* additional interface header files **************************************** */
#include <stdint.h>
#include "Serval_Callable.h"
/* public type and macro definitions */

#define PAL_TCP_BACKLOG					INT32_C(3)
#define PAL_IP_ADDRESS_SIZE UINT8_C(15) /*< Maximum size of the ip address */

/* PAL communication buffer which can be defined for the project specific needs */
typedef struct PAL_commBuffer_s
{
	// Total allocation
	uint8_t *begin;
	uint32_t size;

	// Used by the current layer
    uint8_t *payload;
    uint32_t length;

} PAL_commBuffer_t;

/* typedef is for the Serval stack, since it requires CommBuff_T defined.  */
typedef struct PAL_commBuffer_s* CommBuff_T;
/* IP address required for communicating to the server */
typedef uint32_t Ip_Address_T;
/* IP port required for communicating to server, UDP or TCP/IP */
typedef uint16_t Ip_Port_T;

extern volatile Ip_Address_T PAL_deviceIPAddr; /* hold the current local ip address */

/**
 * @brief : Current status of the socket.
 * This definition of status allows us
 * to reuse it as the tcp socket status.
 *
 */
typedef enum PAL_socketStatus_e{
        STATUS_CLOSED 			= 0,
        STATUS_CONNECTING 		= 1,
        STATUS_OPEN 			= 2,
        STATUS_HALF_CLOSED 		= 3,
        STATUS_HALF_OPEN 		= 4,
        STATUS_CLOSING 			= 5,
} PAL_socketStatus_t;

/**
 * @brief : Socket type
 *
 */
typedef enum PAL_socketType_e{
		TYPE_NDF 		= 0,
        TYPE_UDP 		= 1,
        TYPE_TCP 		= 2,
        TYPE_LISTENER	= 3,
} PAL_socketType_t;

/**
 * @brief : structure that hold the socket information
 *
 */
typedef struct PAL_Udp_Socket_s
{
	volatile int8_t 		Handler;				/* socket handler */
	volatile Ip_Port_T 		Port;					/* binded port */
	volatile Ip_Address_T 	IP_Address;				/* binded address, not used in current implementation */
	volatile Ip_Address_T	PeerAddress;			/* TCP: Peer Address, UDP: default send address - in Network bit order */
	volatile Ip_Port_T		PeerPort;				/* TCP: Peer port, UDP: default send port - in host bit order */
	volatile PAL_socketStatus_t status; 			/* current status */
	volatile int8_t			Deleted;				/* if TRUE this socket handler can be reused */
	volatile int8_t			SendLaterFlag;			/* TRUE: when the socket is ready to send the socket monitor will call the sending function */
	Callable_T* 			callback_SendingLAterFunc; /* sending function */
	Callable_T* 			callback_rx;			/* TCP/UDP: callback on receiving data, TCP_Listener: callback of listener */
	volatile int8_t			Recv_en;				/* 1: enable receive callback, 0: ignore the receive*/
	volatile PAL_socketType_t type;					/* TCP,UDP, LISTENER */
} PAL_Socket_t;


typedef PAL_Socket_t* Tcp_Socket_T;  	/* Tcp communication Socket required for for the Serval stack */
typedef PAL_Socket_t* Udp_Socket_T;  	/* UDP communication Socket Required for the Serval stack */
typedef PAL_Socket_t* Tcp_Listener_T; 	/* TCP listener Socket required for the Serval stack */


/* public function prototype declarations */

/**
 *  @brief
 *     initialize the ip, need to be called before using tcp and udp api
 *  @return The status of the comparison:
 *	RC_OK successful
 */
retcode_t PAL_Ip_initialize(void);

/**
 *  @brief
 *     initialize the PAL
 */
void PAL_Pal_initialize(void);

#endif /* PIP_H_*/
