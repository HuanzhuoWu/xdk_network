/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @defgroup PAL_socketMonitor PAL SocketMonitor
 * @brief PAL SocketMonitor
 * @ingroup Serval_pal
 *
 *  @{
 *
 * @brief Interface header for the PAL_socketMonitor module.
 *
 *  The interface header exports the following features:
 *                                              - PAL_socketMonitorInit()
 *                                              - PAL_socketMonitorStart()
 *                                              - PAL_socketMonitorMapping()
 *                                              - PAL_socketMonitorUnmapping()
 *                                              - PAL_getSocketHandle()
 * 												- PAL_getfreePort()
 *												- PAL_Udp_isValidSocket()
 *												- PAL_Tcp_isValidSocket()
 *												- PAL_Tcp_isSocketInPool()
 *												- PAL_Tcp_isValidListener()
 *												- PAL_SocketMonitorEnqueue()
 *												- PAL_socketMonitorDelete()
 *												- PAL_getInvalidUDPSocket()
 *												- PAL_getInvalidTCPSocket()
 *												- PAL_getInvalidListener()
 *												- PAL_queueCallBackEnQueue()
 *												- PAL_socketMonitorFreeSoc()
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef PAL_SOCKETMONITOR_IH_H_
#define PAL_SOCKETMONITOR_IH_H_

/* public interface declaration ********************************************* */
#include "Serval_Callable.h"
#include <stdint.h>
#include "PIp.h"

typedef struct PAL_SocketMonitor_QData_s
{
    Callable_T* callable_ptr;
    retcode_t status;
//	int8_t		TcpCloseFlag;
} PAL_SocketMonitor_QData_t;

extern xTaskHandle PAL_socketMonitorTaskHandler_gdt;

/* Socket Monitor Error Code*/
#define PAL_SOCKET_HANDLE_SUCCESS 		INT8_C(0) 	/* value returned when an API sucess */
#define PAL_SOCKET_HANDLE_ERROR  		INT8_C(-1) 	/* Error value returned when an API fails */
#define PAL_SOCKET_HANDLE_QUEUE_FULL	INT8_C(-2)
#define PAL_SOCKET_MONITOR_ERR_NO_ERROR           (0L)

/* public global variable declarations */

/* inline function definitions */

/* public function prototype declarations */
/**
 * @brief    : Initializes the structure holding socket related parameters.Presently it also
 *              creates the task which monitors the sockets.
 * @see PAL_socketMonitoring()
 *
 */
void PAL_socketMonitorInit(void);

/**
 * @brief    : Binds the socket to the port and keeps it in listen mode.
 *             Calls the Udp_listen function.
 *
 * @warning : Must be called after the Socket is opened. ( ex. after calling CoapMessaging_startClient)
 *
 *
 */
void PAL_socketMonitorStart(void);

/**
 * @brief    : Adds the socket related information , callback function and socket handle to the socket structure.
 *
 * @param[out]: socket*
 *  On success handle to UDP socket is given back.
 *  On failure, an invalid socket handle is given back.
 *
 *
 * @param[in]: socketHandle
 *  The socket handle created by the driver.
 *
 * @param[in]:callback
 *  A reference to a callback function which will be invoked on incoming data or
 *  reporting errors and any socket issues. It can be NULL, if the upper-layer
 *  is not interested in any incoming data. *
 */

void PAL_socketMonitorMapping(Udp_Socket_T* socket, int8_t socketHandle, uint32_t localAddr, Ip_Port_T Port, uint32_t PeerAddress, uint16_t PeerPort, PAL_socketType_t type, Callable_T* callback, int8_t recv_en);

/**
 * @brief    : deletes the socket related information , callback function and socket handle to the socket structure.
 *
 * @param[in]: socket
 * handle to UDP socket.
 *
 */
void PAL_socketMonitorUnmapping(Udp_Socket_T *socket);

/**
 * @brief    : Gives the socket handle when given the input of a socket pointer.
 *
 * @param[in] : socket
 * handle to UDP socket.
 *
 * @retval   :int8_t
 *  The socket handle which is mapped to the handle to the UDP socket.
 */
int8_t PAL_getSocketHandle(Udp_Socket_T* socket);

/**
 * @brief   : 	Gives a local port number that is not currently used.
 * 			   	the port number is between PAL_GENPORTMIN and PAL_GENPORTMAX. PAL_SOCKET_HANDLE_ERROR
 * @retval 	: 	int32_t
 *  			- number within PAL_GENPORTMIN and PAL_GENPORTMAX 	: free port number
 *  			- PAL_SOCKET_HANDLE_ERROR							: all port are used
 */
int32_t PAL_getfreePort(void);

/**
 * @brief    : check if the socket is valid udp socket
 * @param[in] : socket
 * 		handle to UDP socket.
 * @retval  :int8_t
 * 			 TRUE	: valid
 * 			 FALSE 	: non valid
 * @warning this function have to be called only after the PAL_mutexTcpUdp_gdt is obtained
 */
int8_t PAL_Udp_isValidSocket(Udp_Socket_T socket);

/**
 * @brief    : check if the socket is valid tcp socket
 * @param[in] : socket
 * 		handle to tcp socket.
 * @retval  :int8_t
 * 			 TRUE	: valid
 * 			 FALSE 	: non valid
 * @warning this function have to be called only after the PAL_mutexTcpUdp_gdt is obtained
 */
int8_t PAL_Tcp_isValidSocket(Tcp_Socket_T socket);

/**
 * @brief    	: check if the socket is in the pool of memory PAL_socketInformation[].
 * @param[in] 	: socket
 * 				  handle to UDP socket.
 * @retval  	:int8_t
 * 			 	 TRUE	: found
 * 			 	 FALSE 	: not found
 * @warning this function have to be called only after the PAL_mutexTcpUdp_gdt is obtained
 */
int8_t PAL_Tcp_isSocketInPool(Tcp_Socket_T const socket); // LLD1LR : the parameter should be Tcp_Socket_T

/**
 * @brief    	: check if the socket is a valid listener.
 * @param[in] 	: socket
 * 				  handle to UDP socket.
 * @retval  	: int8_t
 * 			 	  - TRUE	: valid
 * 			 	  - FALSE 	: not valid
 * @warning this function have to be called only after the PAL_mutexTcpUdp_gdt is obtained
 */
int8_t PAL_Tcp_isValidListener(Tcp_Socket_T socket);

/**
 * @brief    	: delete the socket from PAL_socketInformation[].
 * @param[in] 	: socket
 * 				  	handle to UDP/TCP/Listener socket.
 * @warning this function have to be called only after the PAL_mutexTcpUdp_gdt is obtained
 */
void PAL_socketMonitorDelete(Udp_Socket_T* socket);

/**
 * @brief    	: return invalid udp socket.
 * @retval  	: Udp_Socket_T
 * 			 	 invalid udp socket
 */
Udp_Socket_T PAL_getInvalidUDPSocket(void);

/**
 * @brief    	: return invalid tcp socket.
 * @retval  	: Udp_Socket_T
 * 			 	 invalid tcp socket
 */
Tcp_Socket_T PAL_getInvalidTCPSocket(void);

/**
 * @brief    	: return invalid Listener socket.
 * @retval  	: Udp_Socket_T
 * 			 	  invalid Listener socket
 */
Tcp_Listener_T PAL_getInvalidListener(void);

/**
 * @brief    	: push a callback function in a queue. this function will be executed by the socket monitor.
 * 				  queue size is defined in PAL_SOCKETMONITOR_QUEUE_SIZE
 * @param[in] 	: callback_ptr
 * 				  pointer callable object
 * @param[in] 	: status
 * @retval  	: int8_t
 * 			 	  - PAL_SOCKET_HANDLE_SUCCESS		: success
 * 			 	  - PAL_SOCKET_HANDLE_QUEUE_FULL 	: queue is full
 */
int8_t PAL_SocketMonitorEnqueue(Callable_T* callback_ptr, retcode_t status);

/**
 * @brief    	: push a callback function in a queue. this function will be executed by the socket monitor.
 * 				  It is used by Serval to delegate stack heavy CallBack.
 * 				  queue size is defined in PAL_SOCKETMONITOR_QUEUE_SIZE
 * @param[in] 	: callback_ptr
 * 				  pointer callable object
 * @param[in] 	: status
 * @retval  	: int8_t
 * 			 	  - PAL_SOCKET_HANDLE_SUCCESS		: success
 * 			 	  - PAL_SOCKET_HANDLE_QUEUE_FULL 	: queue is full
 */
int8_t PAL_queueCallBackEnQueue(Callable_T* callback_ptr, retcode_t status);

/**
 * @brief    	: force socket monitor to close and delete all the socket,
 * 				  it alse reinitialize PAL_socketInformation[].
 * @warning 	: it used by only the pal test.
 */
void PAL_socketMonitorFreeSoc(void);

/**
 * @brief    	: queues a callback function in a queue.
 * @param[in] 	: callback_ptr
 * 				  pointer callable object
 * @param[in] 	: status
 * @retval  	: int8_t
 * 			 	  - PAL_SOCKET_HANDLE_SUCCESS		: success
 * 			 	  - PAL_SOCKET_HANDLE_QUEUE_FULL 	: queue is full
 */
int8_t PAL_queueCallBackEnQueue(Callable_T* callable_ptr, retcode_t status);

#endif /* PAL_SOCKETMONITOR_IH_H_ */
/**@} */
/** ************************************************************************* */
