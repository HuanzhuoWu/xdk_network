#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# The package name. The library name is derived from this package name
BCDS_POWER_PACKAGE_NAME = POWER
BCDS_PACKAGE_NAME = Power
BCDS_POWER_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 16
BCDS_DEVICE_TYPE ?=EFM32LG
BCDS_DEVICE_ID ?=EFM32LG990F256
BCDS_TOOLS_DIR ?= $(BCDS_POWER_HOME)/../../Tools
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin

# Internal Path Settings
BCDS_POWER_INCLUDE_DIR := include
BCDS_POWER_SOURCE_DIR := source
BCDS_POWER_PROTECTED_DIR := protected
BCDS_POWER_OBJECT_DIR:= objects
BCDS_POWER_LINT_DIR := lint
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOLS_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt

LINT_EXE = $(BCDS_TOOLS_DIR)/PCLint/exe/lint-nt.launch

BCDS_POWER_COMPONENT ?= RTC

#The base name of the library that is generated from this package
ifeq ($(BCDS_POWER_COMPONENT),RTC)
BCDS_POWER_SOURCE_COMPONENT = RTC
else
BCDS_POWER_SOURCE_COMPONENT = BURTC
endif

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Path of other libraries and platform
BCDS_LIBRARIES_DIR =$(BCDS_POWER_HOME)/../../Libraries
BCDS_PLATFORM_DIR = $(BCDS_POWER_HOME)/../../Platform

BCDS_EM_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_OS_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS

BCDS_PERIPHERALS_DIR ?= $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_UTILS_DIR ?= $(BCDS_PLATFORM_DIR)/Utils

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig

#Source Paths
BCDS_POWER_SOURCE_PATH = $(BCDS_POWER_SOURCE_DIR)

#Path of the generated static library and its object files
BCDS_POWER_DEBUG_PATH ?= $(CURDIR)/debug
BCDS_POWER_RELEASE_PATH ?= $(CURDIR)/release
BCDS_POWER_DEBUG_OBJECT_PATH ?= $(BCDS_POWER_DEBUG_PATH)/$(BCDS_POWER_OBJECT_DIR)
BCDS_POWER_RELEASE_OBJECT_PATH ?= $(BCDS_POWER_RELEASE_PATH)/$(BCDS_POWER_OBJECT_DIR)

#Lint Path
BCDS_POWER_DEBUG_LINT_PATH = $(BCDS_POWER_DEBUG_PATH)/$(BCDS_POWER_LINT_DIR)

BCDS_LIB_POWER_BASE_NAME ?= lib$(BCDS_POWER_PACKAGE_NAME)
BCDS_POWER_DEBUG_LIB_NAME = $(BCDS_LIB_POWER_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_POWER_RELEASE_LIB_NAME = $(BCDS_LIB_POWER_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a
BCDS_POWER_DEBUG_LIB   = $(BCDS_POWER_DEBUG_PATH)/$(BCDS_POWER_DEBUG_LIB_NAME)
BCDS_POWER_RELEASE_LIB = $(BCDS_POWER_RELEASE_PATH)/$(BCDS_POWER_RELEASE_LIB_NAME)

# Define the path for the include directories
BCDS_POWER_INCLUDES = \
 				  -I$(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
                  -I$(BCDS_POWER_INCLUDE_DIR)\
                  -I$(BCDS_POWER_PROTECTED_DIR) \
                  -I$(BCDS_POWER_SOURCE_PATH)/PowerMgt/$(BCDS_POWER_SOURCE_COMPONENT) \
                  -I$(BCDS_CONFIG_PATH)/Power \
                  -I$(BCDS_CONFIG_PATH) \
                  -I$(BCDS_PERIPHERALS_DIR)/include \
                  -I$(BCDS_PERIPHERALS_DIR)/config \
                  -I$(BCDS_OS_LIB_DIR)/source/include \
                  -I$(BCDS_OS_LIB_DIR)/source/portable/GCC/ARM_CM4F \
                  -I$(BCDS_OS_LIB_DIR)/source/portable/ARM_CM3 \
                  -I$(BCDS_PLATFORM_DIR)/Basics/include \
                  -I$(BCDS_EM_LIB_DIR)/emlib/inc \
                  -I$(BCDS_EM_LIB_DIR)/usb/inc \
                  -I$(BCDS_EM_LIB_DIR)/CMSIS/Include \
                  -I$(BCDS_UTILS_DIR)/include \
                  -I$(BCDS_UTILS_DIR)/config \
                                   
# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	-DASSERT_FILENAME=\"$*.c\"
	
BCDS_EXTERNAL_INCLUDES = \
	-isystem $(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include
#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_EXCLUDES_LINT := $(foreach DIR, $(subst -isystem , ,$(BCDS_EXTERNAL_INCLUDES)), +libdir\($(DIR)\))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -isystem ,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_POWER_INCLUDES_LINT := $(subst -I,-i,$(BCDS_POWER_INCLUDES))
#-------------------------------------------------------------------------------------------------