/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 * This module is an RTOS wrapper and feature extender library
 * for FreeRTOS and OpenRTOS.
 *
 * There is no public API which would allow the application writers to query the
 * deepest sleep mode which could be entered as application writers should simply
 * define what is the deepest sleep mode their RTOS tasks could enter in a given
 * scenario and they should not be bothered by what sleep state the actual
 * application is allowed to enter at a particular time. This should be
 * transparent for the applications.
 *
 * The PowerMgt_SystemTimerIrqHandler() ISR must be registered as the system ticks
 * timer ISR. In the Energy Micro MCU family specific port the system timer
 * is the RTC module which means that PowerMgt_SystemTimerIrqHandler() is mapped to
 * RTC_IRQHandler().
 * There is no application hook available for the PowerMgt_SystemTimerIrqHandler()
 * specific events and the ISR's prototype is abstracted from the application
 * writers.
 *
 * This module implementation only supports energy mode 0 - 2.
 * Energy mode 3 on the Energy Micro MCUs is nothing more than EM2 with
 * the low frequency, low energy peripherals disabled which should be
 * application driven.
 */

#ifndef POWERMGT_H
#define POWERMGT_H

/* Include all headers which are needed by this file. */

#include "BCDS_Basics.h"

/* Put the type and macro definitions here */

struct PowerMgt_SleepModeList_S
{
    uint8_t sleepModeNestCounter[Max_Mode_Limit];
};

typedef struct PowerMgt_SleepModeList_S PowerMgt_SleepModeList_T;

typedef struct PowerMgt_SleepModeList_S *PowerMgt_SleepModeListPtr_T;


#define POWER_MGT_SYSTEM_TIMER_RESOLUTION       (1U << 24U)     /* System timer's count register has 24 bits resolution */

#define POWER_MGT_USE_LOW_FREQ_EXTERNAL_CLK     1               /* External crystal oscillator is selected as clock source for the system timer  */

#define POWER_MGT_HF_CLK                        0               /* The high frequency external crystal oscillator is NOT in use. */

#define POWER_MGT_RTC_PRESCALE_FACTOR           cmuClkDiv_1     /* The prescale factor configured for the RTC module . Clock divisors*/

#define POWER_MGT_ROUNDUP_DIVISION(numerator, denominator) (((numerator) + (denominator) - 1) / (denominator))

#define POWER_MGT_ALWAYS_INLINE_API __attribute__((always_inline)) inline

#define POWER_MGT_SYSTEM_TIMER_MAX ((POWER_MGT_SYSTEM_TIMER_RESOLUTION) - 1ULL)

#define POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL (POWER_MGT_SYSTEM_CLOCK_FREQUENCY / POWER_MGT_SYSTEM_TICK_RATE_HZ)

#define POWER_MGT_SYSTEM_TIMER_FULL_ROUND POWER_MGT_ROUNDUP_DIVISION(POWER_MGT_SYSTEM_TIMER_RESOLUTION, POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL)
#define elapsedWaitTime expectedWaitTime

#define POWER_MGT_ALLOWED_TO_BLOCK (!UINT32_C(0))
#define POWER_MGT_NOT_ALLOWED_TO_BLOCK (UINT32_C(0))

#if defined(_EFM32_GECKO_FAMILY)
#define POWER_MGT_SYSTEM_TIMER_SYNCH_TIME (4UL)
#else /**< defined(_EFM32_GECKO_FAMILY) */
#define POWER_MGT_SYSTEM_TIMER_SYNCH_TIME (0UL) /* Synch time added according to RTC write requires 3 RTC cycle */
#endif /**< defined(_EFM32_GECKO_FAMILY) */

/** The function like macro implements a conditional test to check for active RTC compare channel 0 events. */
#define PowerMgt_GetSystemTimerIrqFlags() ((RTC->IF) & (RTC->IEN))
#define POWER_MGT_SYSTEM_TIMER_IRQ_MASK (RTC_IF_COMP0)

/** The function like macro implements an event clearing mechanism for active RTC compare channel 0 events. */
#define PowerMgt_ClearSysTimerInterrupt() RTC->IFC = (RTC_IFC_COMP0)

/** The function like macro implements an event clearing mechanism for active RTC IRQs. */
#define PowerMgt_AckPendingSysTimerInterrupt() NVIC_ClearPendingIRQ(RTC_IRQn)

/** The function like macro pends or activates an RTC compare channel 0 event. */
#define PowerMgt_SetSysTimerInterrupt() RTC->IFS = (RTC_IFS_COMP0)

/** The function like macro pends or activates an RTC IRQ. */
#define PowerMgt_PendSysTimerInterrupt() NVIC_SetPendingIRQ(RTC_IRQn)

/** The function like macro implements a read access provider for the RTC module's compare channel 0 register. */
#define PowerMgt_GetSystemTimerCompareValue() \
    (RTC->COMP0)

/** The function like macro implements a read access provider for the RTC module's count register. */
#define PowerMgt_GetSystemTimerCountValue() \
    (RTC->CNT)

#if defined(_EFM32_GECKO_FAMILY)
/** The function like macro implements a write access provider for the RTC module's compare channel 0 register. */
#define PowerMgt_SetSystemTimerCompareValue(compareValue, allowedToBlock) \
    { \
        if ((allowedToBlock)) \
        { \
            for (; ((RTC->SYNCBUSY & RTC_SYNCBUSY_COMP0) != 0);) \
            { \
                ; /* Wait until synchronization finishes. */ \
            } \
            RTC->COMP0 = (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue)); \
        } \
        else \
        { \
            if ((RTC->COMP0) != (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue))) \
            { \
                /* Synchronization issues must be prevented by design. */ \
                assert(((RTC->SYNCBUSY & RTC_SYNCBUSY_COMP0) == 0)); \
                RTC->COMP0 = (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue)); \
            } \
        } \
    }
#else /* defined(_EFM32_GECKO_FAMILY) */
/** The function like macro implements a write access provider for the RTC module's compare channel 0 register. */
#define PowerMgt_SetSystemTimerCompareValue(compareValue, allowedToBlock) \
    if ((allowedToBlock)) \
    { \
        for (; ((RTC->SYNCBUSY & RTC_SYNCBUSY_COMP0) != 0);) \
        { \
            ; /* Wait until synchronization finishes. */ \
        } \
        RTC->COMP0 = (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue)); \
    } \
    else \
    { \
        if ((RTC->COMP0) != (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue))) \
        { \
            /* Synchronization issues must be prevented by design. */ \
            assert(((RTC->SYNCBUSY & RTC_SYNCBUSY_COMP0) == 0)); \
            RTC->COMP0 = (POWER_MGT_SYSTEM_TIMER_MAX & (compareValue)); \
        } \
    }
#endif /* defined(_EFM32_GECKO_FAMILY) */ \

/* Map PowerMgt_SystemTimerIrqHandler() to RTC_IRQHandler() */
#define PowerMgt_SystemTimerIrqHandler RTC_IRQHandler

/** The macro abstracts from the low power entry sequence of the given CPU core.
 *
 * Utilization of the data and instruction barrier instructions, DSB and ISB,
 * are a recommendation from ARM. Low power mode is entered using the Wait For
 * Interrupt or WFI instruction.
 */
#define PowerMgt_GoToSleepCommand() \
    __DSB(); \
    __WFI(); \
    __ISB()

#define POWER_MGT_CMU_LOCKED UINT8_C(1)   /** Indicates CMU register locked */
#define POWER_MGT_CMU_UNLOCKED UINT8_C(0) /** Indicates CMU register not locked */

/* Put the function declarations here */

/* Map PowerMgt_SetupSystemTickInterrupt() to vPortSetupTimerInterrupt() */
void PowerMgt_SetupSystemTickInterrupt(void) __asm("vPortSetupTimerInterrupt");

#endif /* POWERMGT_H */

