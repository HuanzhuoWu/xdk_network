/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#include "BCDS_PowerMgt.h"
#include "PowerMgt.h"
#include "BCDS_Assert.h"
#include <FreeRTOS.h>
#include <task.h>
#include "em_cmu.h"
#include "em_emu.h"
#include "em_rmu.h"
#include "em_int.h"
#include "em_burtc.h"

/* Put the type and macro definitions here */

static_assert(POWER_MGT_SYSTEM_CLOCK_FREQUENCY > 0U,
    "Parameter OS_SYSTEM_CLOCK_FREQUENCY must be greater than 0.");

static_assert(POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL > 0U,
    "The implementation requires OS_SYSTEM_TIMER_TICK_INTERNAL to be greater than 0.");

static_assert((POWER_MGT_SYSTEM_TIMER_RESOLUTION % POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL) == 0U,
    "The implementation requires OS_SYSTEM_TIMER_RESOLUTION to be a multiple of"
    "OS_SYSTEM_TIMER_TICK_INTERNAL which ensures that the OS timer compare value"
    "always falls to well defined positions or slots within the compare value"
    "register.");

/* Put constant and variable definitions here */

/**
 * The variable keeps track of the last or previously set OS timer compare channel 0
 * value as reference point for internal calculations.
 *
 * The software design guarantees that concurrent accesses to the variable cannot
 * occur so the variable does not need to be qualified as volatile.
 */
static uint64_t PreviousBurtcComp;

/**
 * The enum variable is used to track the selected operation mode.
 *
 * The software design guarantees that concurrent accesses to the variable cannot
 * occur so the variable does not need to be qualified as volatile.
 */
static PowerMgt_SleepMode_T SleepMode;

/**
 * The variable tracks the elapsed OS timer increments since the last system start-up.
 * With a 32.768 kHz OS timer frequency the 64 bit resolution makes it possible to
 * track the system time for thousands of years.
 *
 * The software design guarantees that concurrent accesses to the variable cannot
 * occur so the variable does not need to be qualified as volatile.
 */
static uint64_t InternalSystemTime;

/**
 * The variable is used as a sleep mode block request nest counter.
 *
 * The software design guarantees that concurrent accesses to the variable cannot
 * occur so the variable does not need to be qualified as volatile.
 */
static PowerMgt_SleepModeList_T SleepModeControl;


/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/** The function updates the operating system timer's internal state.
 *
 * The forced static inline function is used in two different contexts. In ISR context,
 * the function is not allowed to block on low frequency domain synchronization event.
 *
 * To eliminate as much overhead as possible, still allowing a formal implementation
 * which is valid in both interrupt and task contexts, in-lining is enforced and the
 * input argument is defined to be read only to allow the compiler to skip conditional
 * statements depending only on the input argument during the in-lining process.
 *
 * The function determines how much time have passed since the last operating system timer compare
 * match event. This is only required to be calculated using the operating system timer value register's
 * count value in specific scenarios like when the tick interrupt is blocked for more
 * then a full tick time by the application software or when the system is in sleep
 * mode and an external event wakes the system before the scheduled, expected wake time.
 *
 * The function also sets the new operating system timer compare value for the next system tick
 * and saves the previous compare value for later use.
 *
 * @param[in] POWER_MGT_NOT_ALLOWED_TO_BLOCK if the function is allowed to block until the operating system
 *            timer synchronizes into the low frequency domain. POWER_MGT_ALLOWED_TO_BLOCK otherwise.
 *
 * @return The number of full system tick periods elapsed.
 */
static POWER_MGT_ALWAYS_INLINE_API uint32_t UpdateSystemTimerState(const uint32_t allowedToBlock)
{
    uint64_t waitTimeElapsed, tickPeriodsElapsed, counterValue, newCompareValue;

    assert(PreviousBurtcComp < POWER_MGT_SYSTEM_TIMER_RESOLUTION);

    /* cache the system timer count value */
    counterValue = PowerMgt_GetSystemTimerCountValue() + POWER_MGT_SYSTEM_TIMER_SYNCH_TIME;

    /* Time elapsed since last system timer compare match */
    if (PreviousBurtcComp < counterValue)
    {
        waitTimeElapsed = (counterValue - PreviousBurtcComp);
    }
    else
    {
        waitTimeElapsed = ((POWER_MGT_SYSTEM_TIMER_RESOLUTION) - PreviousBurtcComp + counterValue);
    }

    /* Elapsed system tick periods since last system timer compare match event */
    tickPeriodsElapsed = waitTimeElapsed / (uint32_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL;

    /* Update previous system timer compare match value */
    PreviousBurtcComp = (POWER_MGT_SYSTEM_TIMER_MAX & (PreviousBurtcComp + (tickPeriodsElapsed * ((uint32_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL))));

    /* Calculate next system timer compare match value */
    newCompareValue = (POWER_MGT_SYSTEM_TIMER_MAX & (PreviousBurtcComp + (uint32_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL));

    /* Set next system timer compare match value */
    PowerMgt_SetSystemTimerCompareValue(newCompareValue, allowedToBlock);

    return (tickPeriodsElapsed);
}

/**
 * @brief    This function provides CMU register lock status
 *
 * @retval   POWER_MGT_CMU_LOCKED     CMU registers locked, it must be unlocked to proceed further operations
 * @retval   POWER_MGT_CMU_UNLOCKED   CMU registers unlocked
 */
static POWER_MGT_ALWAYS_INLINE_API uint8_t CMU_Locked(void)
{
    if (CMU->LOCK & CMU_LOCK_LOCKKEY_LOCKED)
    {
        return (POWER_MGT_CMU_LOCKED);
    }
    return (POWER_MGT_CMU_UNLOCKED);
}

/**
 * @brief    This function enters MCU into EM3 mode.
 *
 */
static void EnterEM3mode(void)
{
    uint8_t cmuStatus = POWER_MGT_CMU_UNLOCKED;

    /* Enable stop mode - EM3 at system start-up. */
    cmuStatus = CMU_Locked();

    /* CMU registers may be locked hence get the locking status and unlock it  */
    if ( cmuStatus == POWER_MGT_CMU_LOCKED )
    {
        CMU_Unlock();
    }

    /* Disable LFXO oscillator clock domain */
    CMU_ClockEnable(cmuClock_LFA, false);
    CMU_ClockEnable(cmuClock_LFB, false);

    /* Restore CMU lock status if it was locked already */
    if ( cmuStatus == POWER_MGT_CMU_LOCKED )
    {
        CMU_Lock();
    }
    SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
    SleepMode = Stop_Mode;
}

/** The function initializes the OS sleep mode handler system's state.
 *
 * The forced static inline function is embedded into the OS_setupSystemTickInterrupt()
 * function.
 *
 * The initial system state is configured to allow all available sleep modes.
 */
static POWER_MGT_ALWAYS_INLINE_API void SleepModeInit(void)
{
    /* optimization - C runtime 0 initialization performs the following init sequence implicitly.
     SleepModeControl array is a statically allocated entity, it does not need to be initialized.
     the SleepModeInit() can only be called once per MCU reset. If it is called multiple times, the below code is required

     SleepModeControl.sleepModeNestCounter[Normal_Mode] = UINT8_C(0);
     SleepModeControl.sleepModeNestCounter[Sleep_Mode] = UINT8_C(0);
     SleepModeControl.sleepModeNestCounter[DeepSleep_Mode] = UINT8_C(0);
     SleepModeControl.sleepModeNestCounter[Stop_Mode] = UINT8_C(0);
     */

    /* By default... enter into EM3 mode*/
    EnterEM3mode();
}

/** The function returns the deepest available sleep mode for the system to enter.
 *
 * The forced static inline function is currently not made available as a public API.
 */
static POWER_MGT_ALWAYS_INLINE_API PowerMgt_SleepMode_T GetLowestSleepMode(void)
{
    return (SleepMode);
}


/** The function updates the system configuration settings depending on the
 * allowed sleep modes.
 *
 * The function is called within the PowerMgt_SleepBlockBegin() and PowerMgt_SleepBlockEnd()
 * APIs.
 */
static void UpdateLowestSleepMode(void)
{
    if (SleepModeControl.sleepModeNestCounter[Sleep_Mode])
    {
        /* EM0 - only normal mode is allowed */
        SleepMode = Normal_Mode;
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
    }
    else if (SleepModeControl.sleepModeNestCounter[DeepSleep_Mode])
    {
        /* EM1 - sleep mode is allowed */
        SleepMode = Sleep_Mode;
        SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;
    }
    else if (SleepModeControl.sleepModeNestCounter[Stop_Mode])
    {
        /* EM2 - deep sleep mode is allowed */
        SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
        SleepMode = DeepSleep_Mode;
    }
    else
    {
        /* EM3 - stop mode is allowed */
        EnterEM3mode();
    }
}

/* Put function implementations here */

/* API documentation is in the Public header */
uint32_t PowerMgt_GetSystemTime(void)
{
    uint64_t systemTime;

    portENTER_CRITICAL();
    systemTime = (((InternalSystemTime + PowerMgt_GetSystemTimerCountValue()) - PreviousBurtcComp) / POWER_MGT_SYSTEM_CLOCK_FREQUENCY);
    portEXIT_CRITICAL();

    return (systemTime);
}

/* API documentation is in the Public header */
uint64_t PowerMgt_GetSystemTimeMs(void)
{
    uint64_t systemTime;

    portENTER_CRITICAL();
    systemTime = ((((InternalSystemTime + PowerMgt_GetSystemTimerCountValue()) - PreviousBurtcComp) * 1000U) / POWER_MGT_SYSTEM_CLOCK_FREQUENCY);
    portEXIT_CRITICAL();

    return (systemTime);
}

/* API documentation is in the Public header */
void PowerMgt_SleepBlockBegin(PowerMgt_SleepMode_T operationModeToBlock)
{

    assert(Max_Mode_Limit > operationModeToBlock);

    assert(SleepModeControl.sleepModeNestCounter[operationModeToBlock] != (UINT8_MAX));

    portENTER_CRITICAL();

    (SleepModeControl.sleepModeNestCounter[operationModeToBlock])++;

    UpdateLowestSleepMode();
    portEXIT_CRITICAL();
}

/* API documentation is in the Public header */
void PowerMgt_SleepBlockEnd(PowerMgt_SleepMode_T operationModeToUnblock)
{
    assert(Max_Mode_Limit > operationModeToUnblock);

   /* It is assumed that calls to the OS_sleepBlockBegin() and OS_sleepBlockEnd() APIs are aligned to each other."
        This is a harder requirement than the one defined by the vendor's implementation */
    assert(SleepModeControl.sleepModeNestCounter[operationModeToUnblock] != (UINT8_C(0)));

    portENTER_CRITICAL();
    (SleepModeControl.sleepModeNestCounter[operationModeToUnblock])--;

    UpdateLowestSleepMode();
    portEXIT_CRITICAL();
}

/** ISR to update the RTOS tick count and the operating system timer increments in full system
 * tick periods.
 *
 * The interrupt service routine is mapped to the BURTC module's IRQs.
 */
void PowerMgt_SystemTimerIrqHandler(void)
{
    uint32_t sysTimerIntFlags = PowerMgt_GetSystemTimerIrqFlags();

    if (sysTimerIntFlags & POWER_MGT_SYSTEM_TIMER_IRQ_MASK)
    /* operating system system timer compare channel 0 match event */
    {
        /* Mark the event as handled. */
        PowerMgt_ClearSysTimerInterrupt();

        /* Update the internal system timer with the full system tick periods elapsed.
         *
         * Note that if the application masks the operating system system timer interrupt for longer
         * periods it is possible that more than one full system tick period elapses
         * between calls to this ISR which must be handled properly.
         */
        {
            uint32_t elapsedWaitTime;

            elapsedWaitTime = UpdateSystemTimerState(POWER_MGT_NOT_ALLOWED_TO_BLOCK);

            /* TODO The value of elapsed time is sometimes 1, whcih is not correct. After
             * this issue is fixed, the below commented assertion will be uncommented. */
#if 0
            /* The current implementation assumes that critical sections and
                forced interrupt disable API calls will never block interrupt
                service routines from being executed for longer than one RTOS
                tick timer interval. */
            assert(elapsedWaitTime <= UINT32_C(1));
              
#endif
            if(elapsedWaitTime == UINT32_C(1))
            {
                InternalSystemTime += elapsedWaitTime * ((uint32_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL);

                /* The SysTick runs at the lowest interrupt priority, so when this interrupt
                 executes all interrupts must be unmasked.  There is therefore no need to
                 save and then restore the interrupt mask value as its value is already
                 known. */
                (void) portSET_INTERRUPT_MASK_FROM_ISR();
                {
                    /* Increment the RTOS tick and pend a context switch if necessary. */
                    portYIELD_FROM_ISR(xTaskIncrementTick());
                }
                portCLEAR_INTERRUPT_MASK_FROM_ISR(0);
            }
        }
    }
}

/* API documentation is in the interface header */
void PowerMgt_SuppressTicksAndSleep64(uint64_t expectedWaitTime)
{
    if (GetLowestSleepMode() == Normal_Mode)
        /* If no low power modes are available simply return to the idle task. */
        return;

    {
        eSleepModeStatus rtosSleepModeStatus;

		/* Requested wait time must be at least 2 system ticks */
        assert(expectedWaitTime >= UINT64_C(2));

        /* Mask all maskable interrupts. Note that IRQs are registered still,
         * but the related ISRs cannot be entered. The CPU is not supposed to
         * loose IRQ requests when the ISRs are disabled.
         */
        INT_Disable();

        /* Test whether the RTOS kernel allows entering a low power mode. */
        rtosSleepModeStatus = eTaskConfirmSleepModeStatus();

        if (rtosSleepModeStatus != eAbortSleep)
        /* Enter an available low power mode. */
        {
            uint64_t waitTimeRemaining, waitTimeRemainingPlanned;

#if !defined(NDEBUG)
            if (rtosSleepModeStatus == eStandardSleep)
            {
                /* eStandardSleep */
                /* expectedWaitTime = expectedWaitTime; */
            }
            else if (rtosSleepModeStatus == eNoTasksWaitingTimeout)
            {
                /* eNoTasksWaitingTimeout */
                expectedWaitTime = UINT64_MAX;
            }
            else
            {
                /* Unsupported sleep mode status is returned by eTaskConfirmSleepModeStatus. */
                assert(UINT8_C(0));
            }
#else /* !defined(NDEBUG) */
            if (rtosSleepModeStatus == eNoTasksWaitingTimeout)
            {
                /* eNoTasksWaitingTimeout */
                expectedWaitTime = UINT64_MAX;
            }
#endif /* !defined(NDEBUG) */

            waitTimeRemaining = expectedWaitTime;
            waitTimeRemainingPlanned = expectedWaitTime - UINT64_C(1);

#if (POWER_MGT_HF_CLK == 1)
            /* Save the oscillator configuration so that it could be restored later. */
            EMU_UpdateOscConfig();
#endif /* (POWER_MGT_HF_CLK == 1) */

            for (; waitTimeRemaining;)
            {
                /* Enter a low power mode of the MCU. */
                PowerMgt_GoToSleepCommand();

                /* A wake-up event occurred which needs to be handled. There are two possible
                 * scenarios. A scheduled, planned wake-up event occurred, or an external event
                 * woke up the MCU.
                 */
                {
                    if (PowerMgt_GetSystemTimerIrqFlags() & POWER_MGT_SYSTEM_TIMER_IRQ_MASK)
                    /* The system was woken up by a scheduled wake event. */
                    {
                        uint64_t waitTimeElapsed = waitTimeRemaining - waitTimeRemainingPlanned;

                        PowerMgt_ClearSysTimerInterrupt();

                        if (!PowerMgt_GetSystemTimerIrqFlags())
                        {
                            PowerMgt_AckPendingSysTimerInterrupt();
                        }

                        PreviousBurtcComp = PowerMgt_GetSystemTimerCompareValue();

                        static_assert((((uint64_t)POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL * (uint64_t)POWER_MGT_SYSTEM_TIMER_FULL_ROUND) < (uint64_t)UINT64_MAX),
                            "The elapsed wait time calculation result may not fit into 32 bits.");

                        waitTimeRemaining -= waitTimeElapsed;

                        if (waitTimeRemaining >= (uint64_t) POWER_MGT_SYSTEM_TIMER_FULL_ROUND)
                        {
                            /* PowerMgt_SetSystemTimerCompareValue(PowerMgt_GetSystemTimerCompareValue(), 0UL); */
                            waitTimeRemainingPlanned = waitTimeRemaining - (uint64_t) POWER_MGT_SYSTEM_TIMER_FULL_ROUND;
                        }
                        else if (waitTimeRemaining > UINT64_C(0))
                        {
                            PowerMgt_SetSystemTimerCompareValue((PreviousBurtcComp + (waitTimeRemainingPlanned * (uint32_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL)), !0UL);
                            waitTimeRemainingPlanned -= waitTimeRemaining;
                        }
                        else
                        {
                            /* Exit sleep mode and restore normal system tick period */
                            PowerMgt_SetSystemTimerCompareValue((PreviousBurtcComp + POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL), 0UL);
                        }
                    }
                    else
                    /* The system was woken up by an external event, by an IRQ. */
                    {
                        /* Overall system ticks elapsed during the tickless idle period */
                        elapsedWaitTime = UpdateSystemTimerState(POWER_MGT_ALLOWED_TO_BLOCK) + expectedWaitTime - waitTimeRemaining;

                        /* exit the sleep mode loop */
                        waitTimeRemaining = 0ULL;
                    }
                }
            }

            {
                if (elapsedWaitTime > UINT32_C(0))
                {
                    InternalSystemTime += ((uint64_t) elapsedWaitTime) * ((uint64_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL);
                    vTaskStepTick(--elapsedWaitTime);

                    /* The scheduler is in suspended state while this code snippet executes.
                     * Only unmasked exceptions could request a yield.
                     */
                    xTaskIncrementTick();
                }
            }

#if (POWER_MGT_HF_CLK == 1)
            /* Restore the oscillator configuration. Note that HFRCO is autonomously
             * restored by the energy Micro MCU family of controllers.
             */
            extern void EMU_Restore(void);
            EMU_Restore();
#endif /* (POWER_MGT_HF_CLK == 1) */

        }
        else /* if (rtosSleepModeStatus != eAbortSleep) */
        /* The RTOS kernel aborted the low power sleep request. */
        {
            ; /* nothing to do */
        }

        /* Enable all previously masked ISRs if allowed. Note that INT_Enable()
         * uses a nest counter so based on the application context it could
         * happen that ISRs would still be masked after calling the API.
         */
        INT_Enable();
    }
}

/** The function overrides the default implementation of vPortSetupTimerInterrupt()
 * defined and required by FreeRTOS and OpenRTOS.
 *
 * The function is not a public API, application writers should not call to this
 * function directly, but as external linkage is required for the RTOS implementation,
 * the function cannot be restricted to module scope using the static function qualifier.
 *
 * The function configures the OS system timer module to use an external high accuracy
 * low PPM crystal oscillator with a frequency of POWER_MGT_SYSTEM_CLOCK_FREQUENCY.
 *
 * The operating system system timer's period time is configured to be POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL.
 */
void PowerMgt_SetupSystemTickInterrupt(void)
{
    /* initialize low power system */
    SleepModeInit();

    /* Configure and enable system timer clock source */

#if defined(POWER_MGT_USE_LOW_FREQ_EXTERNAL_CLK)
    CMU_OscillatorEnable(cmuOsc_LFXO, true, true);
#else
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);
#endif /* defined(POWER_MGT_USE_LOW_FREQ_EXTERNAL_CLK) */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Enable access to BURTC registers */
    RMU_ResetControl(rmuResetBU, false);

    /* Configure system timer interrupt priority */
    BURTC_IntDisable(BURTC_IFC_COMP0);
    NVIC_SetPriority(BURTC_IRQn, 255);

    {
        BURTC_Init_TypeDef burtcInit = BURTC_INIT_DEFAULT;

        burtcInit.enable       = false;                /* Enable BURTC after initialization  */
        burtcInit.mode         = burtcModeEM3;         /* BURTC is enabled in EM0-EM4 */
        burtcInit.debugRun     = false;                /* Counter shall keep running during debug halt. */
        burtcInit.clkSel       = burtcClkSelLFXO;      /* Select LFRCO as clock source */
        burtcInit.clkDiv       = burtcClkDiv_1;        /* Clock pre-scaler             */
        burtcInit.lowPowerComp = 0;                    /* Number of least significant clock bits to ignore in low power mode */
        burtcInit.timeStamp    = false;                /* Enable time stamp on entering backup power domain */
        burtcInit.compare0Top  = false;                /* Clear counter on compare match */
        burtcInit.lowPowerMode = burtcLPDisable;       /* Low power operation mode, requires LFXO or LFRCO */

        /* Start the system timer counter from a value of zero */
        BURTC_Enable(false);

        PowerMgt_SetSystemTimerCompareValue((uint64_t) POWER_MGT_SYSTEM_TIMER_TICK_INTERNAL, 0UL);
        PreviousBurtcComp = 0ULL;
        BURTC_IntClear(BURTC_IFC_COMP0);
        BURTC_IntEnable(BURTC_IF_COMP0);
        NVIC_EnableIRQ(BURTC_IRQn);

        /* Initialize BURTC */
        BURTC_Init(&burtcInit);
        BURTC_Enable(true);
    }
}

/** ************************************************************************* */
