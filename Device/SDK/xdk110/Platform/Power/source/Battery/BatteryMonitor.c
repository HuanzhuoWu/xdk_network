/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#include "BCDS_Power.h"

#if (BCDS_FEATURE_BATTERYMONITOR == 1)
/* system header files */

#include <stddef.h>

/* additional interface header files */
#include "BCDS_Assert.h"
#include "BCDS_Retcode.h"
#include "ADC_ih.h"
#include "PTD_portDriver_ih.h"

#include <FreeRTOS.h>
#include <timers.h>

/* own header files */
#include "BCDS_BatteryMonitorConfig.h"
#include "BCDS_BatteryMonitor.h"
#include "BatteryMonitor.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static Battery_State_T currentBatteryState;
static Battery_Level_T previousBatteryLevel;
static TimerHandle_t batteryRoutineHandle;
static uint8_t stablityCounter[BATTERY_LEVEL_MAX];
static Battery_callback batteryStatusCallback;
static ADC_node_t adcNode;
static ADC_singleAcq_t adcSingleAcq;
static uint16_t batteryVoltageInMilliVolt;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the private header */
static void enableBatteryCharging(void) {
	PTD_pinOutClear(POWER_BATTERY_PORT, POWER_BATTERY_PIN);
}

/* The description is in the private header */
static Battery_Level_T classifyBatteryLevel(uint16_t batteryVoltage,
		Battery_Level_T previousLevel) {
	Battery_Level_T batteryLevel = previousBatteryLevel;

	/* Checking whether battery voltage is in normal level ie less than 3.9V
	 * under the previous level is battery full */
	if (BATTERY_LEVEL_FULL == previousLevel) {

		if (BATTERY_HIGH_THRESHOLD_LL > batteryVoltage) {
			batteryLevel = BATTERY_LEVEL_NORMAL;
		}
	}

	/* Checking whether battery voltage is in normal level ie greater than 3.6V or critical level
	 * if the battery level is less than or equal to 3.5V
	 * under the previous level is battery low */
	else if (BATTERY_LEVEL_LOW == previousLevel) {
		if (BATTERY_LOW_THRESHOLD_HL < batteryVoltage) {
			batteryLevel = BATTERY_LEVEL_NORMAL;
		}
		else if(BATTERY_CRITICAL_THRESHOLD_LL >= batteryVoltage){
		    batteryLevel = BATTERY_LEVEL_CRITICAL;
		}
	}

	/* Checking whether battery voltage is greater than 4.0V or less than 3.5V
	 * under the previous level is battery normal */
	else if (BATTERY_LEVEL_NORMAL == previousLevel) {
		if (BATTERY_HIGH_THRESHOLD_HL < batteryVoltage) {
			batteryLevel = BATTERY_LEVEL_FULL;
		} else if ((batteryVoltage > BATTERY_CRITICAL_THRESHOLD_LL) && (batteryVoltage < BATTERY_LOW_THRESHOLD_LL)){
			batteryLevel = BATTERY_LEVEL_LOW;
		}
	}


	else if (BATTERY_LEVEL_CRITICAL == previousLevel){
	    if(BATTERY_CRITICAL_THRESHOLD_HL < batteryVoltage){
	        batteryLevel = BATTERY_LEVEL_LOW;
	    }
	}

	else {
		; /* Do nothing */
	}

	return (batteryLevel);
}

/* The description is in the private header */
static Battery_Level_T stabilizeBatteryLevel(Battery_Level_T batteryLevel) {
	Battery_Level_T returnBatteryLevel = BATTERY_LEVEL_MAX;

	switch (batteryLevel) {

	case BATTERY_LEVEL_LOW:

		stablityCounter[BATTERY_LEVEL_NORMAL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_FULL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_CRITICAL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_LOW]++;
		if (stablityCounter[BATTERY_LEVEL_LOW] > BATTERY_STABLITY_COUNT_LIMIT) {
			returnBatteryLevel = BATTERY_LEVEL_LOW;
		}
		break;

	case BATTERY_LEVEL_NORMAL:

		stablityCounter[BATTERY_LEVEL_LOW] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_FULL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_CRITICAL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_NORMAL]++;
		if (stablityCounter[BATTERY_LEVEL_NORMAL] > BATTERY_STABLITY_COUNT_LIMIT) {
			returnBatteryLevel = BATTERY_LEVEL_NORMAL;
		}
		break;

	case BATTERY_LEVEL_FULL:

		stablityCounter[BATTERY_LEVEL_LOW] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_NORMAL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_CRITICAL] = UINT8_C(0);
		stablityCounter[BATTERY_LEVEL_FULL]++;
		if (stablityCounter[BATTERY_LEVEL_FULL] > BATTERY_STABLITY_COUNT_LIMIT) {
			returnBatteryLevel = BATTERY_LEVEL_FULL;
		}
		break;

	case BATTERY_LEVEL_CRITICAL:

        stablityCounter[BATTERY_LEVEL_LOW] = UINT8_C(0);
        stablityCounter[BATTERY_LEVEL_NORMAL] = UINT8_C(0);
        stablityCounter[BATTERY_LEVEL_FULL] = UINT8_C(0);
        stablityCounter[BATTERY_LEVEL_CRITICAL]++;
        if (stablityCounter[BATTERY_LEVEL_CRITICAL] > BATTERY_STABLITY_COUNT_LIMIT) {
            returnBatteryLevel = BATTERY_LEVEL_CRITICAL;
        }
        break;

	default:

		assert(0);
		break;

	}
	return (returnBatteryLevel);
}

/* The description is in the private header */
static void analyseBatteryState(Battery_State_T presentBatteryState) {
	if (previousBatteryLevel != presentBatteryState.batteryLevel) {
		batteryStatusCallback(presentBatteryState);
		previousBatteryLevel = presentBatteryState.batteryLevel;
	}
}

/* The description is in the private header */
static void monitorBattery(TimerHandle_t pxTimer) {
	(void) pxTimer;

	uint8_t adcChannel = (uint8_t)adcSingleInpCh4;
	uint32_t adcData = UINT32_C(0);
	uint32_t adcScaleFactor = BATTERY_VBAT_CONV_FACTOR;
	Battery_Level_T batteryLevel;

	/* Set channel */
	adcSingleAcq.adcChannel = (ADC_SingleInput_TypeDef) adcChannel;

	/* Acquire single data */
	ADC_pollSingleData(&adcSingleAcq);

	/* Scaled data */
	adcData = ADC_scaleAdcValue(&adcSingleAcq);

	/* Scale the result according to other hardware factors such as voltage divider. */
	if (BATTERY_ADC_SCALE < adcScaleFactor) {
		adcData = (uint32_t) ((adcData * adcScaleFactor) / BATTERY_ADC_RESOLUTION);
	}

	batteryVoltageInMilliVolt = adcData;
    if (currentBatteryState.batteryLevel == BATTERY_LEVEL_MAX)
    {
        if (BATTERY_CRITICAL_THRESHOLD_LL >= adcData)
        {
            currentBatteryState.batteryLevel = BATTERY_LEVEL_CRITICAL;
        }
        else if (adcData < BATTERY_LOW_THRESHOLD_LL)
        {
            currentBatteryState.batteryLevel = BATTERY_LEVEL_LOW;

        }
        else if (adcData > BATTERY_HIGH_THRESHOLD_HL)
        {
            currentBatteryState.batteryLevel = BATTERY_LEVEL_FULL;
        }
        else
        {
            currentBatteryState.batteryLevel = BATTERY_LEVEL_NORMAL;
        }

    }
    else
    {
        /* Calculate and update current battery status */
        batteryLevel = classifyBatteryLevel(adcData, previousBatteryLevel);

        batteryLevel = stabilizeBatteryLevel(batteryLevel);
        if (BATTERY_LEVEL_MAX != batteryLevel)
        {
            currentBatteryState.batteryLevel = batteryLevel;
        }
    }

    /* Update system based on charging or battery */
    analyseBatteryState(currentBatteryState);
}

/* The description is in the private header */
static void checkChargerLine(void *callbackParameter, uint32_t data) {
	(void) callbackParameter;
	(void) data;

	/* Based on the status of charging pin update the charge status of battery */
	currentBatteryState.chargeStatus = getChargingStatus();

	batteryStatusCallback(currentBatteryState);
}

/* The description is in the private header */
static void ptdCallbackIsr(void)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    if(pdPASS == xTimerPendFunctionCallFromISR(checkChargerLine, NULL, UINT32_C(0), &xHigherPriorityTaskWoken))
    {
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }

}

/* The description is in the private header */
static void configureCharingPinForInt(void) {
	/* The mode is set for the pin configured for charge indication */
	PTD_pinModeSet(BATTERY_CHARGE_PORT,
	BATTERY_CHARGE_PIN,
	BATTERY_CHARGE_MODE_ENABLE,
	BATTERY_CHARGE_DOUT_ENABLE);

	/* The interrupt pin is configured in the PTD module for the Push button. */
	PTD_intConfig( BATTERY_CHARGE_PORT,
	BATTERY_CHARGE_PIN,
	BATTERY_ENABLE_INT_RISING_EDGE,
	BATTERY_ENABLE_INT_FALLING_EDGE,
	BATTERY_ENABLE_INT_AFTER_CONFIG, (PTD_intrCallback) ptdCallbackIsr);
}

/* The description is in the private header */
static Battery_Status_T getChargingStatus(void) {
	uint8_t pinStatus = UINT8_C(0);
	Battery_Status_T chargeStatus;

	pinStatus = PTD_pinInGet(BATTERY_CHARGE_PORT, BATTERY_CHARGE_PIN);

	if (UINT8_C(0) == pinStatus) {
		chargeStatus = BATTERY_STATUS_CHARGING;
	}

	else {
		chargeStatus = BATTERY_STATUS_NOT_CHARGING;
	}
	return (chargeStatus);
}

/* The description is in the private header */
static void enableCharging(void) {
    if( pdPASS != xTimerStart(batteryRoutineHandle, UINT32_C(0)))
    {
        Retcode_raiseError(RETCODE(RETCODE_SEVERITY_FATAL,(uint32_t)RETCODE_UNEXPECTED_BEHAVIOR));
    }

}

/* global functions ********************************************************* */

/* The description is in the public header */
void Battery_init(Battery_callback callbackFunction) {
    assert(callbackFunction != NULL);

    batteryStatusCallback = callbackFunction;

    adcNode = (ADC_node_t )BATTERY_ADC_INIT;
    adcSingleAcq = (ADC_singleAcq_t )ADC_INITSINGLE;

    configureCharingPinForInt();

    enableBatteryCharging();

    ADC_init(&adcNode);

    /* Based on the status of charging pin update the charge status of battery */
    currentBatteryState.chargeStatus = getChargingStatus();
    previousBatteryLevel = BATTERY_LEVEL_MAX;

    currentBatteryState.batteryLevel = BATTERY_LEVEL_MAX;

    if (NULL == batteryRoutineHandle)
    {
        batteryRoutineHandle = xTimerCreate((const char * const ) "monitorBattery", BATTERY_MONITORING_RATE_MS / portTICK_RATE_MS,
                UINT16_C(1), NULL, monitorBattery);
        assert(batteryRoutineHandle != NULL);
    }
    monitorBattery(NULL);
    enableCharging();
}

/* The description is in the public header */
uint16_t Battery_readBatteryLevel(void) {

	uint16_t batteryVolt = BATTERY_HIGHEST_THRESHOLD / BATTERY_CENT_PERCENT;

	batteryVolt = batteryVoltageInMilliVolt / batteryVolt;

	if (batteryVolt > BATTERY_CENT_PERCENT) {
		batteryVolt = BATTERY_CENT_PERCENT;
	}
	return (batteryVolt);
}

uint16_t Battery_readBatteryInMilliVolt(void)
{
    return (batteryVoltageInMilliVolt);
}

/* The description is in the public header */
void Battery_deInit(void) {
	if (NULL != batteryRoutineHandle) {

	    if ( pdPASS  != xTimerStop(batteryRoutineHandle, UINT32_C(0)))
	    {
	        Retcode_raiseError(RETCODE(RETCODE_SEVERITY_FATAL,(uint32_t)RETCODE_UNEXPECTED_BEHAVIOR));
	    }

	}
	previousBatteryLevel = BATTERY_LEVEL_NORMAL;
	currentBatteryState.batteryLevel = BATTERY_LEVEL_NORMAL;
	stablityCounter[BATTERY_LEVEL_LOW] = UINT8_C(0);
	stablityCounter[BATTERY_LEVEL_FULL] = UINT8_C(0);
	stablityCounter[BATTERY_LEVEL_NORMAL] = UINT8_C(0);
	stablityCounter[BATTERY_LEVEL_CRITICAL] = UINT8_C(0);
}

#endif /*if (BCDS_FEATURE_BATTERYMONITOR == 1) */
/** ************************************************************************* */
