/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BATTERY_MONITOR_H_
#define BATTERY_MONITOR_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
/** ADC instance. */
#define BATTERY_ADC_INSTANCE            (ADC0)

/** Associate CMUc clock */
#define BATTERY_ADC_CMU_CLOCK           (cmuClock_ADC0)

/** ADC frequency */
#define BATTERY_ADC_FREQUENCY           UINT32_C(7000000)

#define BATTERY_ADC_SCALE 				UINT32_C(1)

#define BATTERY_CENT_PERCENT 			UINT16_C(100)

/** Default config for ADC init structure with (It can be reconfigured by the driver) :
 * - ADC instance definition.
 * - adcOvsRateSel2 :               2x oversampling (if enabled).
 * - adcLPFilterBypass :            No input filter selected.
 * - adcWarmupNormal :              ADC shutdown after each conversion.
 * - _ADC_CTRL_TIMEBASE_DEFAULT :   Use HW default value.
 * - _ADC_CTRL_PRESC_DEFAULT :      Use HW default value.
 * - false :                        Do not use tailgate.
 */
#define BATTERY_ADC_INIT                        \
    {                                           \
    BATTERY_ADC_INSTANCE,                       \
    (CMU_Clock_TypeDef)BATTERY_ADC_CMU_CLOCK,   \
    BATTERY_ADC_FREQUENCY,                      \
    ADC_INIT_DEFAULT                        \
    }

/** Default config for ADC single conversion init structure. Overitten by driver.
 * - ADC instance definition.
 * - PRS ch0 (if enabled).
 * - 32 ADC_CLK cycle acquisition time.
 * - Buffered VDD reference.
 * - 12 bit resolution.
 * - Used channels, overriten by driver.
 * - Single ended input.
 * - PRS disabled.
 * - Right adjust.
 * - Deactivate conversion after one scan sequence.
 */
#define ADC_INITSINGLE                  \
    {                                           \
    BATTERY_ADC_INSTANCE,                       \
    {                                       \
        adcPRSSELCh0,                       \
        adcAcqTime32,                       \
        adcRef2V5,                          \
        adcRes12Bit,                        \
        adcSingleInpCh4,                    \
        false,                              \
        false,                              \
        false,                              \
        false                               \
    },                                      \
    (ADC_SingleInput_TypeDef)UINT32_C(0),           \
    UINT32_C(0),                                    \
    }

#define BATTERY_ADC_RESOLUTION                      UINT32_C(1000)

#define BATTERY_VBAT_CONV_FACTOR                    (UINT32_C(2) * BATTERY_ADC_RESOLUTION)

/* local function prototype declarations */

/** @brief	enableBatteryCharging() is used to clear the battery charging GPIO.
 */
static void enableBatteryCharging(void);

/**
 * @brief	classifyBatteryLevel() can be used to classify the battery according to the battery voltage.
 *
 * @param[in] - batteryVoltage      - ADC value of battery voltage
 * @param[in] - previousLevel       - previous level of battery
 * @retval    - Battery_Level_T  - current battery level after initial analysis
 */
static Battery_Level_T classifyBatteryLevel(uint16_t batteryVoltage,
		Battery_Level_T previousLevel);

/**
 * @brief	stabilizeBatteryLevel() can be used to stabilize from battery voltage fluctuations.
 *
 * @param[in] - Battery_Level_T   - current battery level after initial analysis
 * @retval    - Battery_Level_T   - very recent battery level based on the stabilized voltage range
 */
static Battery_Level_T stabilizeBatteryLevel(Battery_Level_T batteryLevel);

/**
 * @brief	 analyseBatteryState() is used to analyze the current battery state and to take decisions.
 *
 * @param[in] - Battery_State_T  - current battery state including charge level and charge state
 */
static void analyseBatteryState(Battery_State_T currentBatteryState);

/**
 * @brief   monitorBattery() is timer task initiated at regular interval to monitor the entire battery state.
 *
 * @param[in] - TimerHandle_t     - timer handler
 */
static void monitorBattery(TimerHandle_t pxTimer);

/**
 * @brief   To check the charger line and to update the status
 *
 * @param[in] callbackParameter
 * @param[in] uint32_t data
 *
 * @note    Input parameters are unused,it is specified to match with
 * 			one of the input type of OS_timerPend function ,where it
 * 			is used.
 */
static void checkChargerLine(void *callbackParameter, uint32_t data);

/**
 * @brief   This gives the status of charging or not
 *
 * @retval - BATTERY_STATUS_CHARGING - If the battery is charging
 * @retval - BATTERY_STATUS_NOT_CHARGING  - if the battery is not charging
 */
static Battery_Status_T getChargingStatus(void);

/**
 * @brief   This will enable the Battery Charging functionalities
 */
static void enableCharging(void);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BATTERY_MONITOR_H_ */

