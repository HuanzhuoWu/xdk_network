/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/


#ifndef BCDS_BATTERY_MONITOR_H_
#define BCDS_BATTERY_MONITOR_H_

#include "BCDS_Power.h"

#if (BCDS_FEATURE_BATTERYMONITOR == 1)

/* public interface declaration */

/* public type and macro definitions */

/** Enum for the charge status */
enum Battery_Status_E
{
    BATTERY_STATUS_NOT_CHARGING, /**< The battery is in not charging state */
    BATTERY_STATUS_CHARGING, /**< The battery is in charging state */
    BATTERY_STATUS_MAX
};

typedef enum Battery_Status_E Battery_Status_T;

/** Enum for the battery level */
enum Battery_Level_E
{
    BATTERY_LEVEL_LOW, /**< The battery is low */
    BATTERY_LEVEL_NORMAL, /**< The battery is normal */
    BATTERY_LEVEL_FULL, /**< The battery is high */
    BATTERY_LEVEL_CRITICAL, /**< The battery level is critically low */
    BATTERY_LEVEL_MAX
};

typedef enum Battery_Level_E Battery_Level_T;

/** structure containing battery information */
struct Battery_State_S
{
    Battery_Status_T chargeStatus; /**< Information about battery charge status */
    Battery_Level_T batteryLevel; /**< Information about battery charge level */
};

typedef struct Battery_State_S Battery_State_T, *Battery_StatePtr_T;

/** Call back function to intimate state change of battery to power manager module */
typedef void (*Battery_callback)(Battery_State_T batteryState);

/* public function prototype declarations */

/**
 * @brief   Initialization of battery module
 *
 * @param[in]  callbackFunction - a callback for registration
 * 				which will be called upon battery status change
 */
void Battery_init(Battery_callback callbackFunction);

/**
 * @brief   This function will get data from adc and calculate
 * 			battery level in percentage
 *
 * @note    The return value is percentage based
 * @retval  uint16_t -level in percentage.
 */
uint16_t Battery_readBatteryLevel(void);

/**
 *  @brief This function returns the battery voltage in millivolts.
 *
 *  @retval The level of battery in millivolts
 */
uint16_t Battery_readBatteryInMilliVolt(void);


/**
 * @brief   This function is to deinitialize battery monitoring module
 *
 */
void Battery_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* if (BCDS_FEATURE_BATTERYMONITOR == 1) */

#endif /*BCDS_BATTERY_MONITOR_H_ */

