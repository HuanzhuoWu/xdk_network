#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# The package name. The library name is derived from this package name
BCDS_PACKAGE_NAME = MiscDrivers
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 28

BCDS_DEVICE_PACKAGE_TYPE ?= STM32L476xx
BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_DEVICE_ID ?= EFM32GG390F1024

# Internal Path Settings
BCDS_MISCDRIVERS_INCLUDE_DIR := include
BCDS_MISCDRIVERS_SOURCE_DIR := source
BCDS_MISCDRIVERS_PROTECTED_DIR := protected
BCDS_MISCDRIVERS_OBJECT_DIR:= objects
BCDS_MISCDRIVERS_LINT_DIR := lint

#Path of Platform, Libraries and Tools
BCDS_TOOLS_DIR ?= $(BCDS_PACKAGE_HOME)/../../Tools
BCDS_LIBRARIES_DIR = $(BCDS_PACKAGE_HOME)/../../Libraries
BCDS_PLATFORM_DIR = $(BCDS_PACKAGE_HOME)/../../Platform

#Tool chain definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
#Lint configuration and exe file paths
#Path where the LINT config files are present(compiler and environment configurations)
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOLS_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt

LINT_EXE = $(BCDS_TOOLS_DIR)/PCLint/exe/lint-nt.launch

#External Library Package Dependencies
BCDS_EM_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_STM32CUBE_DIR ?= $(BCDS_LIBRARIES_DIR)/STM32Cube/stm32cube
BCDS_OS_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS
BCDS_FATFS_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/FATfs

#External Platform Package Dependencies
BCDS_BASICS_DIR = $(BCDS_PLATFORM_DIR)/Basics
BCDS_PERIPHERALS_DIR ?= $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_UTILS_DIR ?= $(BCDS_PLATFORM_DIR)/Utils
BCDS_POWER_DIR ?= $(BCDS_PLATFORM_DIR)/Power

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Path of the generated static library and its object files
BCDS_MISCDRIVERS_DEBUG_PATH ?= $(BCDS_PACKAGE_HOME)/debug
BCDS_MISCDRIVERS_RELEASE_PATH ?= $(BCDS_PACKAGE_HOME)/release
BCDS_MISCDRIVERS_DEBUG_OBJECT_PATH ?= $(BCDS_MISCDRIVERS_DEBUG_PATH)/$(BCDS_MISCDRIVERS_OBJECT_DIR)
BCDS_MISCDRIVERS_RELEASE_OBJECT_PATH ?= $(BCDS_MISCDRIVERS_RELEASE_PATH)/$(BCDS_MISCDRIVERS_OBJECT_DIR)

#Lint Path
BCDS_MISCDRIVERS_DEBUG_LINT_PATH = $(BCDS_MISCDRIVERS_DEBUG_PATH)/$(BCDS_MISCDRIVERS_LINT_DIR)

BCDS_LIB_MISCDRIVERS_BASE_NAME ?= lib$(BCDS_PACKAGE_NAME)
BCDS_MISCDRIVERS_DEBUG_LIB_NAME = $(BCDS_LIB_MISCDRIVERS_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_MISCDRIVERS_RELEASE_LIB_NAME = $(BCDS_LIB_MISCDRIVERS_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a
BCDS_MISCDRIVERS_DEBUG_LIB   = $(BCDS_MISCDRIVERS_DEBUG_PATH)/$(BCDS_MISCDRIVERS_DEBUG_LIB_NAME)
BCDS_MISCDRIVERS_RELEASE_LIB = $(BCDS_MISCDRIVERS_RELEASE_PATH)/$(BCDS_MISCDRIVERS_RELEASE_LIB_NAME)

# Define the path for the include directories
ifeq ($(BCDS_TARGET_PLATFORM),efm32)
BCDS_MISCDRIVERS_INCLUDES = \
 				  -I$(BCDS_MISCDRIVERS_INCLUDE_DIR) \
                  -I$(BCDS_MISCDRIVERS_PROTECTED_DIR) \
 				  -I$(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
 				  -I$(BCDS_OS_LIB_DIR)/source/include \
 				  -I$(BCDS_OS_LIB_DIR)/source/portable/GCC/ARM_CM3 \
 				  -I$(BCDS_OS_LIB_DIR)/source/portable/ARM_CM3 \
                  -I$(BCDS_PACKAGE_CONFIG_PATH) \
                  -I$(BCDS_PERIPHERALS_DIR)/include \
                  -I$(BCDS_POWER_DIR)/include \
                  -I$(BCDS_BASICS_DIR)/include \
                  -I$(BCDS_EM_LIB_DIR)/emlib/inc \
                  -I$(BCDS_EM_LIB_DIR)/usb/inc \
                  -I$(BCDS_EM_LIB_DIR)/CMSIS/Include \
                  -I$(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
                  -I$(BCDS_UTILS_DIR)/include \
                  -I$(BCDS_FATFS_LIB_DIR)/src \
                  -I$(BCDS_PACKAGE_CONFIG_PATH) \
                  -I$(BCDS_CONFIG_PATH)/Power \
                  -I$(BCDS_CONFIG_PATH)/$(BCDS_HW_VERSION) \
                  -I$(BCDS_CONFIG_PATH)
endif 
       
ifeq ($(BCDS_TARGET_PLATFORM),stm32)
BCDS_MISCDRIVERS_INCLUDES += \
	-I$(BCDS_OS_LIB_DIR)/Source/include \
	-I$(BCDS_OS_LIB_DIR)/Source/portable/GCC/ARM_CM4F \
    -I$(BCDS_MISCDRIVERS_INCLUDE_DIR)\
    -I$(BCDS_PACKAGE_CONFIG_PATH) \
    -I$(BCDS_CONFIG_PATH)/Power \
    -I$(BCDS_CONFIG_PATH)/Utils \
    -I$(BCDS_CONFIG_PATH) \
    -I$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc \
    -I$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc/Legacy \
    -I$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Include \
    -I$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Device/ST/$(BCDS_DEVICE_TYPE)/Include \
    -I$(BCDS_STM32CUBE_DIR)/../BCDS_Customization/config \
    -I$(BCDS_PERIPHERALS_DIR)/include \
    -I$(BCDS_BASICS_DIR)/include \
    -I$(BCDS_UTILS_DIR)/include
endif   

ifeq ($(BCDS_TARGET_PLATFORM),stm32)
BCDS_EXTERNAL_INCLUDES += \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc/Legacy \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Include \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Device/ST/$(BCDS_DEVICE_TYPE)/Include \
    -isystem$(BCDS_STM32CUBE_DIR)/../BCDS_Customization/config
endif
        
# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	-DDEBUG -D DBG_ASSERT_FILENAME=\"$*.c\" -DASSERT_FILENAME=\"$*.c\"

#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_INCLUDES_LINT += -i$(BCDS_LINT_CONFIG_PATH)
BCDS_MISCDRIVERS_INCLUDES_LINT := $(subst -I,-i,$(BCDS_MISCDRIVERS_INCLUDES))

