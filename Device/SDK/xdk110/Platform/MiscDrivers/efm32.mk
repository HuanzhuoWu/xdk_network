##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

# Build chain settings
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif

#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra -Wstrict-prototypes\
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections -D$(BCDS_DEVICE_ID)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -Werror -D BCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -DBCDS_TARGET_EFM32
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

# Source and object files
BCDS_MISCDRIVERS_SOURCE_FILES += \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/Buzzer/Buzzer.c \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/LED/LED.c \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/PushButton/PushButton.c \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/FlashS25FL116K/FlashS25FL116K.c \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/NVM/NVM.c \
	$(BCDS_MISCDRIVERS_SOURCE_DIR)/SDCard/SDCardDriver.c \

BCDS_MISCDRIVERS_OBJECT_FILES = \
	$(patsubst $(BCDS_MISCDRIVERS_SOURCE_DIR)/%.c, %.o, $(BCDS_MISCDRIVERS_SOURCE_FILES))

BCDS_MISCDRIVERS_LINT_FILES = \
	$(patsubst $(BCDS_MISCDRIVERS_SOURCE_DIR)/%.c, %.lob, $(BCDS_MISCDRIVERS_SOURCE_FILES))
	
BCDS_MISCDRIVERS_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_MISCDRIVERS_DEBUG_OBJECT_PATH)/,$(BCDS_MISCDRIVERS_OBJECT_FILES))

BCDS_MISCDRIVERS_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_MISCDRIVERS_DEBUG_OBJECT_PATH)/, $(BCDS_MISCDRIVERS_OBJECT_FILES:.o=.d))	
	
BCDS_MISCDRIVERS_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_MISCDRIVERS_RELEASE_OBJECT_PATH)/,$(BCDS_MISCDRIVERS_OBJECT_FILES))

BCDS_MISCDRIVERS_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_MISCDRIVERS_RELEASE_OBJECT_PATH)/, $(BCDS_MISCDRIVERS_OBJECT_FILES:.o=.d))
		
BCDS_MISCDRIVERS_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_MISCDRIVERS_DEBUG_LINT_PATH)/, $(BCDS_MISCDRIVERS_LINT_FILES))

######################## Build Targets #######################################
.PHONY: debug
debug: $(BCDS_MISCDRIVERS_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_MISCDRIVERS_RELEASE_LIB)

all: debug release 

############# Generate MISCDRIVERS Library for BCDS ###################
$(BCDS_MISCDRIVERS_DEBUG_LIB): $(BCDS_MISCDRIVERS_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build MISCDRIVERS for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_MISCDRIVERS_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_MISCDRIVERS_RELEASE_LIB): $(BCDS_MISCDRIVERS_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build MISCDRIVERS for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_MISCDRIVERS_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"
	
# compile C files
$(BCDS_MISCDRIVERS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_MISCDRIVERS_SOURCE_DIR)/%.c
	@echo "Build $?"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
	       -I . $(BCDS_MISCDRIVERS_INCLUDES) $< -o $@

$(BCDS_MISCDRIVERS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_MISCDRIVERS_SOURCE_DIR)/%.c
	@echo "Build $?"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_MISCDRIVERS_INCLUDES) $< -o $@
     
     
.PHONY: lint
lint: $(BCDS_MISCDRIVERS_DEBUG_LINT_FILES)
	@echo "Lint End"

$(BCDS_MISCDRIVERS_DEBUG_LINT_PATH)/%.lob: $(BCDS_MISCDRIVERS_SOURCE_DIR)/%.c
	@echo "===================================="
	@mkdir -p $(@D)
	@echo $@
	@$(LINT_EXE) +d$(BCDS_DEVICE_TYPE) +d$(BCDS_DEVICE_ID) -DBCDS_TARGET_EFM32 +dBCDS_PACKAGE_ID $(LINTFLAGS) $(BCDS_EXTERNAL_INCLUDES_LINT) $(BCDS_MISCDRIVERS_INCLUDES_LINT) $(BCDS_LINT_CONFIG_FILE) $< -oo[$@]
	@echo "===================================="

-include $(BCDS_MISCDRIVERS_DEBUG_DEPENDENCY_FILES) $(BCDS_MISCDRIVERS_RELEASE_DEPENDENCY_FILES)
	
.PHONY: lint_clean
lint_clean :
	@echo Cleaning Lint files
	@rm -rf $(BCDS_MISCDRIVERS_DEBUG_LINT_PATH)
	@echo Lint files cleaned

.PHONY: clean	
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_MISCDRIVERS_DEBUG_PATH) $(BCDS_MISCDRIVERS_RELEASE_PATH)

.PHONY: diagnosis
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_MISCDRIVERS_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_MISCDRIVERS_DEBUG_PATH)
	@echo $(BCDS_MISCDRIVERS_DEBUG_OBJECT_FILES)

cdt:
	@echo "cdt"
	$(CC) $(BCDS_CFLAGS_DEBUG_COMMON) $(BCDS_MISCDRIVERS_INCLUDES) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -E -P -v -dD -c ${CDT_INPUT_FILE}
.PHONY: cdt
	