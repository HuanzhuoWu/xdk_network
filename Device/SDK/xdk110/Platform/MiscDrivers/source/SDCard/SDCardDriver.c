/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_SDCardDriver.h"
#include "SDCardDriver.h"

/* system header files */
#include "BCDS_Retcode.h"
#include "BCDS_Basics.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_SDCARD

#include <FreeRTOS.h>
#include <timers.h>
#include "em_cmu.h"
#include "em_usart.h"
#include "em_gpio.h"
#include "SPI_ih.h"
#include "BCDS_Assert.h"
#include "PTD_portDriver_ih.h"
#include "SER_serialDriver_ih.h"
#include "diskio.h"

/* constant definitions ***************************************************** */
#define SDCARD_DETECT_RISING_EDGE			UINT8_C(1)
#define SDCARD_DETECT_FALLING_EDGE			UINT8_C(1)
/* local variables ***********************************************************/

static Retcode_T SDCard_DiskStatus = (Retcode_T) SDCARD_NOT_INITIALIZED; /**< Disk status is Initialized with NOINIT state */
static uint8_t SDCard_SDCardType; /**< default type of SD-card is Unknown  */
static volatile SDCardDriver_Status_T SDCardDetectStatus = SDCARD_NOT_INSERTED; /**< Status of SD card slot as SD card inserted or not */
static bool InitStatus = false;
static SPI_device_t SDCard_spiHandle; /**< SPI handle for hardware SPI instance created for SD card */
static uint8_t SDCard_TxRingBuffer[SDC_RING_BUFFER_SIZE]; /**< Ring Buffer for low level SPI interface */
static uint8_t SDCard_RxRingBuffer[SDC_RING_BUFFER_SIZE]; /**< Ring Buffer for low level SPI interface */
static uint8_t SDCard_DummyTxData = SDC_CRC_DUMMY_DATAWRITE; /**< variable is using for dummy write */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* API documentation is in the configuration header */
static void SDCardInterruptHandler(void *callBackParam1, uint32_t callBackParam2)
{
    /* To suppress warnings */
    BCDS_UNUSED(callBackParam1);
    BCDS_UNUSED(callBackParam2);

    /* Check whether SD card is inserted in XDK */
    if (PTD_pinInGet(PTD_PORT_SDC_PRESENT, PTD_PIN_SDC_PRESENT) == SDC_CARD_DETECTED)
    {
        /* SD Card is inserted, activate LevelShifter and fill global variable */
        PTD_pinModeSet((GPIO_Port_TypeDef) PTD_PORT_SDC_LEVEL_SHIFT, (uint16_t) PTD_PIN_SDC_LEVEL_SHIFT,
                (GPIO_Mode_TypeDef) PTD_MODE_SDC_LEVEL_SHIFT, (uint16_t) SDC_LEVEL_SHIFT_LOW);
        SDCardDetectStatus = SDCARD_INSERTED;
    }
    else
    {
        /* SD Card is removed, de-activate LevelShifter and fill global variable */
        PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_LEVEL_SHIFT));
        SDCardDetectStatus = SDCARD_NOT_INSERTED;
    }
}

/* API documentation is in the private header */
static void SDCardDetectCallBack(void)
{
    /* To reduce the functionality in ISR, the SDC_interruptHandler has been added as
     a function handler which will be executed */
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    if (xTimerPendFunctionCallFromISR(SDCardInterruptHandler, NULL, UINT32_C(0), &xHigherPriorityTaskWoken) == pdPASS)
    {
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
    else
    {
        Retcode_raiseError((Retcode_T) RETCODE_RTOS_QUEUE_ERROR);
    }
}

/* API documentation is in the configuration header */
static void SDCardPinSetting(void)
{
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(WIFI_SD_VDD_PORT));

    /* pin setting for detection of SD card */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_PRESENT));

    /* pin setting for chip select of SD card */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_CS));

    /* Check whether the card is inserted, then activate LevelShifter */
    if (SDC_CARD_DETECTED == PTD_pinInGet(PTD_PORT_SDC_PRESENT, PTD_PIN_SDC_PRESENT))
    {
        SDCardDetectStatus = SDCARD_INSERTED;
        /* Level shifter On */
        PTD_pinModeSet((GPIO_Port_TypeDef) PTD_PORT_SDC_LEVEL_SHIFT, (uint16_t) PTD_PIN_SDC_LEVEL_SHIFT,
                (GPIO_Mode_TypeDef) PTD_MODE_SDC_LEVEL_SHIFT,
                SDCARD_LEVEL_SHIFT_ON);
    }
    else
    {
        SDCardDetectStatus = SDCARD_NOT_INSERTED;
        /* Level shifter Off */
        PTD_pinModeSet((GPIO_Port_TypeDef) PTD_PORT_SDC_LEVEL_SHIFT, (uint16_t) PTD_PIN_SDC_LEVEL_SHIFT,
                (GPIO_Mode_TypeDef) PTD_MODE_SDC_LEVEL_SHIFT,
                SDCARD_LEVEL_SHIFT_OFF);
    }
    /* Config GPIO */
    uint8_t Usart_Tx_port = AF_USART1_TX_PORT(SDCARD_SPI_LOCATION); //To suppress the lint warnings
    uint16_t Usart_Tx_pin = AF_USART1_TX_PIN(SDCARD_SPI_LOCATION);
    uint8_t Usart_Rx_port = AF_USART1_RX_PORT(SDCARD_SPI_LOCATION);
    uint16_t Usart_Rx_pin = AF_USART1_RX_PIN(SDCARD_SPI_LOCATION);
    uint16_t SpiMode = SDCARD_SPI_MODE;
    SpiMode = SpiMode >> 1;
    PTD_pinModeSet((GPIO_Port_TypeDef)(Usart_Tx_port), (Usart_Tx_pin), (GPIO_Mode_TypeDef) gpioModePushPull, (uint16_t) 1);
    PTD_pinModeSet((GPIO_Port_TypeDef)(Usart_Rx_port), (Usart_Rx_pin), (GPIO_Mode_TypeDef) gpioModeInput, (uint16_t) 1);

    /* Clock Idle high for SPI Mode 2, 3; Clock Idle low for SPI Mode 0, 1 */
    uint8_t Usart_clk_port = AF_USART1_CLK_PORT(SDCARD_SPI_LOCATION); //To suppress the lint warnings
    uint16_t Usart_clk_pin = AF_USART1_CLK_PIN(SDCARD_SPI_LOCATION);
    PTD_pinModeSet((GPIO_Port_TypeDef)(Usart_clk_port), (Usart_clk_pin), (GPIO_Mode_TypeDef) gpioModePushPull,
            (uint16_t) (SpiMode));

    /* MOSI configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_MOSI));

    /* MISO configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_MISO));

    /* Clock configuration */
    PTD_pinModeSet(PTD_GET_PORT_PIN_MODE_DOUT(SDC_CLK));
}

/* API documentation is in the configuration header */
static void SDCardEnableClocks(void)
{
    /* Enable the clock and select */
    CMU_OscillatorEnable(cmuOsc_HFXO, true, true); /* HFXO as high frequency clock, HFCLK */
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
    CMU_ClockEnable(cmuClock_CORELE, true);
    CMU_ClockEnable(cmuClock_GPIO, true);

    /* Update SystemCoreClock */
    SystemCoreClockUpdate();

    /* Enable USART clock */
    CMU_ClockEnable(cmuClock_USART1, true);
}

/* API documentation is in the configuration header */
static void SDCardDetectRegInterrupt(void)
{
    PTD_intConfig((GPIO_Port_TypeDef) PTD_PORT_SDC_PRESENT, (uint32_t) PTD_PIN_SDC_PRESENT, (bool) SDCARD_DETECT_RISING_EDGE, (bool) SDCARD_DETECT_FALLING_EDGE, (bool) SDCARD_DETECT_ENABLE, (PTD_intrCallback) SDCardDetectCallBack);
}

/* API documentation is in the configuration header */
static Retcode_T SDCardSpiInitialize(void)
{
    SPI_initParams_t initParams;
    SPI_return_t spiRetVal;
    Retcode_T sdcSpiInitReturnValue;

    initParams.portNumber = SPI1;
    initParams.clockMode = SPI_CLOCK_MODE_CPOL0_CPHA0;
    initParams.routeLocation = SPI_ROUTE_LOCATION1;
    initParams.txBuf_p = SDCard_TxRingBuffer;
    initParams.rxBuf_p = SDCard_RxRingBuffer;
    initParams.txBufSize = SDC_RING_BUFFER_SIZE;
    initParams.rxBufSize = SDC_RING_BUFFER_SIZE;
    initParams.baudrate = SDCARD_SPI_LOW_BITRATE;

    spiRetVal = SPI_init(&SDCard_spiHandle, &initParams);
    if (spiRetVal != SPI_SUCCESS)
    {
        sdcSpiInitReturnValue = (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_SPI_INIT_FAILED));
    }
    else
    {

        sdcSpiInitReturnValue = RETCODE_OK;
    }
    return (sdcSpiInitReturnValue);
}

/* API documentation is in the configuration header */
static Retcode_T SDCardWaitReady(uint8_t* receiveByte)
{
    uint32_t retryCount;

    /* Wait for ready in timeout of 500ms */
    retryCount = ((uint8_t) SDC_VALUE_FIVEHUNDRED * SDCARD_MILLI_SECOND_VALUE);
    do
    {
        if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, receiveByte, SDC_ONE_BYTE))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
    } while ((*receiveByte != SDC_SPI_DEFAULT_VALUE) && (--retryCount));

    return (RETCODE_OK);
}

/* API documentation is in the private header */
static Retcode_T SDCardSendCmd(uint8_t command, uint32_t argument, uint8_t* preceiveByte)
{
    uint32_t retryCount;
    uint8_t transmitByte[SDC_SIX_BYTE];
    uint8_t receiveByte;

    if (command & SDC_VALIDATE_ACMD_SET) /* validating the ACMDx command sequence of CMD55-Commands */
    {
        command &= SDC_VALIDATE_ACMD_CLEAR;
        if (RETCODE_OK == SDCardSendCmd(SDC_COMMAND55, (uint8_t) SDC_VALUE_ZERO, preceiveByte))
        {
            if (*preceiveByte > (uint8_t) SDC_VALUE_ONE)
                return RETCODE_OK;

        }
    }
    /* Select the card and wait for ready */
    SDCardDriver_Disconnect();
    if (SDCardDriver_Connect() != RETCODE_OK)
    {
        *preceiveByte = SDC_SPI_DEFAULT_VALUE;
        return (RETCODE(RETCODE_SEVERITY_NONE, (Retcode_T) SDCARD_SPI_BUSY));
    }
    transmitByte[0] = (SDC_START_COMMAND | command); /* Start + Command index */
    transmitByte[1] = (uint8_t) (argument >> (uint8_t) SDC_VALUE_TWENTYFOUR); /* Argument[31..24] */
    transmitByte[2] = (uint8_t) (argument >> (uint8_t) SDC_VALUE_SIXTEEN); /* Argument[23..16] */
    transmitByte[3] = (uint8_t) (argument >> (uint8_t) SDC_VALUE_EIGHT); /* Argument[15..8] */
    transmitByte[4] = (uint8_t) (argument); /* Argument[7..0] */
    transmitByte[5] = (uint8_t) SDC_VALUE_ONE; /* Dummy CRC + Stop */
    if (command == SDC_COMMAND0)
    {
        transmitByte[5] = SDC_CRC_VALUE_COMMAND0; /* Valid CRC for CMD0(0) */
    }
    if (command == SDC_COMMAND8)
    {
        transmitByte[5] = SDC_CRC_VALUE_COMMAND8; /* Valid CRC for CMD8(0x1AA) */
    }
    if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, transmitByte, SDC_SIX_BYTE, SPI_WRITE_ONLY))
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
    }
    /* Receive command response */
    if (command == SDC_COMMAND12)
    {
        if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, &SDCard_DummyTxData, SDC_ONE_BYTE, SPI_WRITE_ONLY))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
    }
#ifdef ENABLE_DMA
    vTaskDelay(DELAY_ONE_MS);
#endif
    retryCount = (uint8_t) SDC_VALUE_TEN; /* Wait for a valid response in timeout of 10 attempts */
    do
    {
        if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, &receiveByte, SDC_ONE_BYTE))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
    } while ((receiveByte & SDC_VALIDATE_ACMD_SET) && (--retryCount));

    *preceiveByte = receiveByte; /* Return with the response value */
    return RETCODE_OK;
}

/* API documentation is in the configuration header */
static Retcode_T SDCardBlockSend(const uint8_t *sendBuffer, uint8_t token)
{
    uint8_t responseValue;
    uint8_t retryCount;
    Retcode_T sdCardWriteStatus;
    const uint8_t DummyCRC[SDC_TWO_BYTE] = { SDCard_DummyTxData, SDCard_DummyTxData };
    /* NULL case checking for data pointer */
    if (sendBuffer == NULL)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_NULL_POINTER));
    }
    if ((RETCODE_OK != SDCardWaitReady(&responseValue)) && (responseValue != SDC_SPI_DEFAULT_VALUE))
    { /* SD-card status check */
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
    }
    if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, &token, SDC_ONE_BYTE, SPI_WRITE_ONLY))/* Transmit a token to SD-Card */
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
    }
    if (token != SDC_STOP_TOKEN)
    {/* Not Stop Transmission token is received from SD-Card*/
        if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, sendBuffer, SDC_SINGLE_BLOCK_LENGTH, SPI_WRITE_ONLY))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }

        /* Next two bytes is the CRC (Dummy) which we discard */
        if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, DummyCRC, SDC_TWO_BYTE, SPI_WRITE_ONLY))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }

        retryCount = (uint8_t) SDC_VALUE_TEN; /*Timeout value to get proper responds*/
        do
        {
            if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, &responseValue, SDC_ONE_BYTE))
            {
                return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
            }

        } while (((responseValue & SDC_RESPONSE_CHECK) != (uint8_t) SDC_VALUE_FIVE)
                && (--retryCount));

        if ((responseValue & SDC_RESPONSE_CHECK) != (uint8_t) SDC_VALUE_FIVE)
        { /* If not accepted, return with error */
            sdCardWriteStatus = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t) SDCARD_RW_FAIL);
        }
        else
        {
            sdCardWriteStatus = RETCODE_OK;
        }
    }
    else
    {
        /* STOP TOKEN VALUE RECEIVED FROM SD-CARD */
        sdCardWriteStatus = RETCODE_OK;
    }
    return (sdCardWriteStatus);
}

/* API documentation is in the configuration header */
static Retcode_T SDCardBlockReceive(uint8_t *receiveBuffer, uint32_t receiveLength)
{
    uint8_t token;
    uint32_t retryCount;
    Retcode_T sdCardReceiveStatus;
    const uint8_t DummyCRC[SDC_TWO_BYTE] = { SDC_CRC_DUMMY_DATAWRITE, SDC_CRC_DUMMY_DATAWRITE };

    /* NULL case checking for data pointer */
    if (receiveBuffer == NULL)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_NULL_POINTER));
    }

    /* Wait for data packet in timeout of 100ms */
    retryCount = ((uint8_t) SDC_VALUE_HUNDRED * SDCARD_MILLI_SECOND_VALUE);

    do
    {
        if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, &token, SDC_ONE_BYTE))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }

    } while ((token == SDC_SPI_DEFAULT_VALUE) && (--retryCount));

    if (token != SDC_VALID_TOKEN_RESP)
    { /* Invalid data token */
        sdCardReceiveStatus = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
    }
    else
    {
        if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, receiveBuffer, receiveLength))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
        /* Next two bytes is the CRC (Dummy) which we discard */
        if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, DummyCRC, SDC_TWO_BYTE, SPI_WRITE_ONLY))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
        sdCardReceiveStatus = RETCODE_OK; /* Return with success */
    }
    return (sdCardReceiveStatus);
}

static Retcode_T SdcardTypeDetect(uint8_t responseVal, uint8_t * diskResult)
{
    uint16_t retryCount;
    uint8_t diskType = (uint8_t) SDC_VALUE_ZERO;
    uint8_t command = SDC_COMMAND0;
    uint8_t validateBuffer[SDC_VALUE_FOUR];
    uint8_t respVal;
    Retcode_T sdCardInitRetVal;

    if (responseVal == (uint8_t) SDC_VALUE_ONE) /* SDv2?  or card is HC */
    {
        if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, validateBuffer, (uint8_t) SDC_VALUE_FOUR))
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
        if ((validateBuffer[SDC_HS_COMMAND_RSP] == (uint8_t) SDC_VALUE_ONE) && (validateBuffer[SDC_VOLTAGE_STATUS] == SDC_VDD_VALIDATE)) /* The card can work at vdd range of 2.7-3.6V */
        {
            retryCount = SDC_SEND_RETRY_COUNT; /* Retry count set for timeout  */
            do
            {
                retryCount--;
                sdCardInitRetVal = SDCardSendCmd(SDC_A_COMMAND41, SDC_IDLE_VALUE, &respVal);
                if (RETCODE_OK != sdCardInitRetVal)
                {
                    return (sdCardInitRetVal); /*  */
                }
            } while ((retryCount) && (respVal));

            sdCardInitRetVal = SDCardSendCmd(SDC_COMMAND58, (uint8_t) SDC_VALUE_ZERO, &respVal);
            if (RETCODE_OK != sdCardInitRetVal)
            {
                return (sdCardInitRetVal); /*  */
            }
            if ((retryCount) && (respVal == (uint8_t) SDC_VALUE_ZERO)) /* Check CCS bit in the OCR */
            {
                if (SPI_SUCCESS != SPI_read(&SDCard_spiHandle, validateBuffer, (uint8_t) SDC_VALUE_FOUR))
                {
                    return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
                }
                diskType = (validateBuffer[SDC_VALUE_ZERO] & SDC_DISK_TYPE_VALIDATE) ? SDC_CT_SD2 | SDC_CT_BLOCK : SDC_CT_SD2; /* SDv2 */
            }
        }
    }
    else
    { /* SDv1 or MMCv3 *//* card is not HC */
        sdCardInitRetVal = SDCardSendCmd(SDC_A_COMMAND41, (uint8_t) SDC_VALUE_ZERO, &respVal);
        if (RETCODE_OK != sdCardInitRetVal)
        {
            return (sdCardInitRetVal); /*  */
        }
        if (respVal <= (uint8_t) SDC_VALUE_ONE)
        {
            diskType = SDC_CT_SD1;
            command = SDC_A_COMMAND41; /* SDv1 */
        }
        else
        {
            diskType = SDC_CT_MMC;
            command = SDC_COMMAND1; /* MMCv3 */
        }
        retryCount = SDC_SEND_RETRY_COUNT; /* Retry count set for timeout */
        do
        {
            retryCount--;
            sdCardInitRetVal = SDCardSendCmd(command, (uint8_t) SDC_VALUE_ZERO, &respVal);
            if (RETCODE_OK != sdCardInitRetVal)
            {
                return (sdCardInitRetVal); /*  */
            }
        } while ((retryCount) && (respVal)); /* Wait for leaving idle state */

        sdCardInitRetVal = SDCardSendCmd(SDC_COMMAND16, SDC_SINGLE_BLOCK_LENGTH, &respVal); /*Select R/w block length */
        if (RETCODE_OK != sdCardInitRetVal)
        {
            return (sdCardInitRetVal);
        }
        if ((retryCount) || (respVal != (uint8_t) SDC_VALUE_ZERO))
        {
            /* Set read/write block length to 512 */
            diskType = (uint8_t) SDC_VALUE_ZERO;
        }
    }
    *diskResult = diskType;
    return (RETCODE_OK);
}
/* global functions ********************************************************* */

/* API documentation is in the interface header */
Retcode_T SDCardDriver_Init(void)
{
    int8_t initReturn;
    Retcode_T SDCardInitStatus;
    if (false == InitStatus)
    {
        /* Pin settings for SD card */
        SDCardPinSetting();

        /* Clock settings for SD card */
        SDCardEnableClocks();

        /* Interrupt settings for SD card detection */
        SDCardDetectRegInterrupt();

        /* SPI driver setting for SD card */
        initReturn = SDCardSpiInitialize();
        if (initReturn != RETCODE_OK)
        {
            SDCardInitStatus = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t ) RETCODE_FAILURE);
        }
        else
        {
            /* After power up we need a initialization delay of 1ms or 74 clock cycles supply ramp up time*/
            for (uint8_t writeCounter = (uint8_t) SDC_VALUE_TEN; writeCounter; writeCounter--)
            {
                if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, &SDCard_DummyTxData, SDC_ONE_BYTE, SPI_WRITE_ONLY))
                {

                    /* For Power up sequence of SDcard we are only providing clocks
                     * to the sdcard with chip select high so here return value decision shouldn't be considered.
                     * In this case SPI write will fail because of chip select is high while SPIwrite, */
                }
            }
            SDCardInitStatus = RETCODE_OK;
            InitStatus = true;
        }
    }
    else
    {
        SDCardInitStatus = RETCODE_OK;
    }
    return (SDCardInitStatus);
}

/* API documentation is in the interface header */
SDCardDriver_Status_T SDCardDriver_GetDetectStatus(void)
{
    /* return the SD card inserted or not */
    return (SDCardDetectStatus);
}

/* API documentation is in the interface header */
void SDCardDriver_Disconnect(void)
{
    PTD_pinOutSet((GPIO_Port_TypeDef) PTD_PORT_SDC_CS, PTD_PIN_SDC_CS); /* CS pin high. */
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_Connect(void)
{
    uint8_t resVal;
    Retcode_T sdCardConnectStatus;

    PTD_pinOutClear((GPIO_Port_TypeDef) PTD_PORT_SDC_CS, PTD_PIN_SDC_CS); /* CS pin low. */
    if ((RETCODE_OK != SDCardWaitReady(&resVal)) && (resVal != SDC_SPI_DEFAULT_VALUE))
    {
        SDCardDriver_Disconnect();
        sdCardConnectStatus = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
    }
    else
    {
        sdCardConnectStatus = RETCODE_OK;
    }
    return (sdCardConnectStatus);
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_DiskInitialize(uint8_t sdDrive)
{
    uint8_t diskType = (uint8_t) SDC_VALUE_ZERO;
    uint8_t respVal;
    Retcode_T sdCardInitRetVal;

    if (sdDrive)
    {
        return (RETCODE(RETCODE_SEVERITY_NONE, (uint32_t) RETCODE_FAILURE)); /* Supports only single drive */
    }
    sdCardInitRetVal = SDCardSendCmd(SDC_COMMAND0, (uint8_t) SDC_VALUE_ZERO, &respVal);
    if (RETCODE_OK != sdCardInitRetVal)
    {
        return sdCardInitRetVal; /* No card in the socket */
    }
    if (respVal == (uint8_t) SDC_VALUE_ONE)
    {
        sdCardInitRetVal = SDCardSendCmd(SDC_COMMAND8, SDC_CT_HS, &respVal);
        if (RETCODE_OK != sdCardInitRetVal)
        {
            return sdCardInitRetVal; /*  */
        }
        sdCardInitRetVal = SdcardTypeDetect(respVal, &diskType);
        if (RETCODE_OK != sdCardInitRetVal)
        {
            return sdCardInitRetVal; /*  */
        }
    }

    SDCard_SDCardType = diskType;
    SDCardDriver_Disconnect();

    if (diskType) /* Initialization succeed */
    {
        SDCard_DiskStatus = (Retcode_T) SDCARD_INITIALIZED; /* Updating the Disk status */
        /* Speed up SPI clock. */
        USART_BaudrateSyncSet(USART1, SDC_REF_FRQ, SDCARD_SPI_HIGH_BITRATE);
    }
    else /* Initialization failed */
    {
        SDCard_DiskStatus = RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_NOT_INITIALIZED); /* Set SDCARD_NOT_INITIALIZED */
    }

    return (SDCard_DiskStatus);
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_DiskRead(uint8_t drive, uint8_t *readBuffer, uint32_t sector, uint32_t readCount)
{
    Retcode_T sdCardDiskReadStatus;
    uint8_t respVal;

    if (readBuffer == NULL)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_NULL_POINTER));
    }

    if (drive || (!readCount))
    { /* Validating the read count and drive */
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_NO_DISK));
    }
    if (SDCard_DiskStatus != (Retcode_T) SDCARD_INITIALIZED)
    {
        /*Validating the Disk state */
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_DISK_NOT_READY));
    }

    if (!(SDCard_SDCardType & SDC_CT_BLOCK))
    {/*Validating the SD-Card Type */
        sector *= SDC_SINGLE_BLOCK_LENGTH; /* Convert to byte address if needed */
    }
    if (readCount == (uint8_t) SDC_VALUE_ONE)
    { /* READ_SINGLE_BLOCK */
        sdCardDiskReadStatus = SDCardSendCmd(SDC_COMMAND17, sector, &respVal);
        if (RETCODE_OK != sdCardDiskReadStatus)
        {
            return sdCardDiskReadStatus;
        }
        if ((respVal == (uint8_t) SDC_VALUE_ZERO) && (SDCardBlockReceive(readBuffer, SDC_SINGLE_BLOCK_LENGTH) == RETCODE_OK))
        {
            readCount = (uint8_t) SDC_VALUE_ZERO;
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
    }
    else
    { /* READ_MULTIPLE_BLOCK */
        sdCardDiskReadStatus = SDCardSendCmd(SDC_COMMAND18, sector, &respVal);
        if (sdCardDiskReadStatus != RETCODE_OK)
        {
            return sdCardDiskReadStatus;
        }
        if (respVal == (uint8_t) SDC_VALUE_ZERO)
        {
            do
            {
                sdCardDiskReadStatus = SDCardBlockReceive(readBuffer, SDC_SINGLE_BLOCK_LENGTH);
                if (sdCardDiskReadStatus != RETCODE_OK)
                {
                    return sdCardDiskReadStatus;
                }
                readBuffer += SDC_SINGLE_BLOCK_LENGTH;
            } while (--readCount);

            sdCardDiskReadStatus = SDCardSendCmd(SDC_COMMAND12, (uint8_t) SDC_VALUE_ZERO, &respVal);
            if (sdCardDiskReadStatus != RETCODE_OK)
            {
                return sdCardDiskReadStatus;
            }
        }
    }
    SDCardDriver_Disconnect(); /* Communication is disconnected from SD-card */
    if (readCount)
    {
        sdCardDiskReadStatus = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
    }
    else
    {
        sdCardDiskReadStatus = RETCODE_OK;
    }
    return (sdCardDiskReadStatus);
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_DiskWrite(uint8_t drive, const uint8_t *writeBuffer, uint32_t sector, uint32_t writeCount)
{
    Retcode_T sdCardDiskWriteStatus;
    uint8_t respVal;

    if (writeBuffer == NULL)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) RETCODE_NULL_POINTER));
    }

    if (drive || (!writeCount))
    { /* Validating the Write count and drive */
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_NO_DISK));
    }

    if (SDCard_DiskStatus & (Retcode_T) SDCARD_DISK_WRITE_PROTECTED)
    { /* SD-card checking either configured or Protected state */
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) SDCARD_DISK_WRITE_PROTECTED));
    }

    if (SDCard_DiskStatus != (Retcode_T) SDCARD_INITIALIZED)
    {
        /*Validating the Disk state */
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_DISK_NOT_READY));
    }

    /*Validating the SD-card type */
    if (!(SDCard_SDCardType & SDC_CT_BLOCK))
    {
        sector *= SDC_SINGLE_BLOCK_LENGTH; /* Convert to byte address if needed */
    }
    if (writeCount == (uint8_t) SDC_VALUE_ONE)
    { /* Single block write */
        sdCardDiskWriteStatus = SDCardSendCmd(SDC_COMMAND24, sector, &respVal);
        if (RETCODE_OK != sdCardDiskWriteStatus)
        {
            return sdCardDiskWriteStatus;
        }
        if ((respVal == (uint8_t) SDC_VALUE_ZERO) && (SDCardBlockSend(writeBuffer, SDC_VALID_TOKEN_RESP) == RETCODE_OK))
        {
            writeCount = (uint8_t) SDC_VALUE_ZERO;
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
        }
    }
    else
    { /* Multiple block write */
        if (SDCard_SDCardType & SDC_CT_SDC)
        {
            sdCardDiskWriteStatus = SDCardSendCmd(SDC_A_COMMAND23, writeCount, &respVal);
            if (sdCardDiskWriteStatus != RETCODE_OK)
            {
                return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
            }
        }
        sdCardDiskWriteStatus = SDCardSendCmd(SDC_COMMAND25, sector, &respVal);
        if (respVal == (uint8_t) SDC_VALUE_ZERO)
        {/* WRITE_MULTIPLE_BLOCK */
            do
            {
                if (SDCardBlockSend(writeBuffer, SDC_BLOCK_SEND_TOKEN) != RETCODE_OK)
                    break;
                writeBuffer += SDC_SINGLE_BLOCK_LENGTH;
            } while (--writeCount);
            sdCardDiskWriteStatus = SDCardBlockSend(writeBuffer, SDC_STOP_TOKEN); /* STOP_TRAN token */
            if ((sdCardDiskWriteStatus != RETCODE_OK) || (RETCODE_OK != SDCardDriver_Connect()))
                writeCount = (uint8_t) SDC_VALUE_ONE;
        }
    }
    SDCardDriver_Disconnect();
    if (writeCount)
    {
        sdCardDiskWriteStatus = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t) SDCARD_RW_FAIL);
    }
    else
    {
        sdCardDiskWriteStatus = RETCODE_OK;
    }

    return (sdCardDiskWriteStatus);
}

/* API documentation is in the interface header */
unsigned long get_fattime(void)
{
    return (28 << 25) | (2 << 21) | (1 << 16);
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_GetDiskStatus(uint8_t drive)
{
    if (drive)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (Retcode_T) SDCARD_NOT_INITIALIZED)); /* Supports only single drive */
    }
    return (SDCard_DiskStatus);
}

/* API documentation is in the interface header */
Retcode_T SDCardDriver_DiskIoctl(uint8_t drive, uint8_t controlCode, void *buffer)
{
    Retcode_T RetVal = RETCODE(RETCODE_SEVERITY_NONE, (uint32_t) RETCODE_FAILURE);
    uint8_t dataLength;
    uint8_t csd[SDC_CSD_SIZE];
    uint32_t csize;
    uint8_t transmitByte;
    uint8_t respVal;

    if (drive)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_NO_DISK));
    }
    if (SDCard_DiskStatus != (Retcode_T) SDCARD_INITIALIZED)
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_DISK_NOT_READY));
    }
    switch (controlCode)
    {
    case CTRL_SYNC: /* Flush dirty buffer if present */
        if (SDCardDriver_Connect() == RETCODE_OK)
        {
            SDCardDriver_Disconnect();
            RetVal = RETCODE_OK;
        }
        else
        {
            return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_DISK_NOT_READY));
        }
        break;
    case GET_SECTOR_COUNT: /* Get number of sectors on the disk (uint16_t) */
        RetVal = SDCardSendCmd(SDC_COMMAND9, 0, &respVal);

        if ((RetVal == RETCODE_OK) && (respVal == 0) && (SDCardBlockReceive(csd, SDC_CSD_SIZE) == RETCODE_OK))
        {
            if ((csd[SDC_CSD_CRC] >> SDC_CSD_CRC_OFFSET) == SDC_CSD_CRC_SET)
            { /* SDv2? */
                csize = csd[SDC_CSD_BLOCK_LENGTH] + ((uint16_t) csd[SDC_CSD_BLOCK_PARTIAL] << SDC_CSD_BLOCK_OFFSET) + (uint8_t) SDC_VALUE_ONE;
                *(uint32_t*) buffer = (uint32_t) csize << SDC_CSD_SIZE_OFFSET;
            }
            else
            { /* SDv1 or MMCv2 */
                dataLength = (csd[SDC_CSD_C_SIZE_MULT] & SDC_CSD_C_SIZE_MASK) + ((csd[SDC_CSD_BLOCK_PARTIAL] & SDC_CSD_BLOCK_MASK) >> (uint8_t) SDC_VALUE_SEVEN) + ((csd[SDC_CSD_BLOCK_LENGTH] & (uint8_t) SDC_VALUE_THREE) << (uint8_t) SDC_VALUE_ONE) + (uint8_t) SDC_VALUE_TWO;
                csize = (csd[SDC_CSD_BLOCK_OFFSET] >> SDC_CSD_CRC_OFFSET) + ((uint16_t) csd[SDC_CSD_VDD_W_R_CURR_MAX] << SDC_CSD_VDD_W_R_CURR_OFFSET)
                        + ((uint16_t) (csd[SDC_CSD_CRC_OFFSET] & (uint8_t) SDC_VALUE_THREE) << SDC_CSD_SIZE_OFFSET) + (uint8_t) SDC_VALUE_ONE;
                *(uint32_t*) buffer = (uint32_t) csize << (dataLength - SDC_CSD_BLOCK_LENGTH);
            }
        }
        else
        {
            RetVal = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
        }
        break;

    case GET_SECTOR_SIZE: /* Get sectors on the disk (uint16_t) */
        *(uint16_t*) buffer = SDC_SINGLE_BLOCK_LENGTH;
        RetVal = RETCODE_OK;
        break;

    case GET_BLOCK_SIZE: /* Get erase block size in unit of sectors (uint32_t) */
        if (SDCard_SDCardType & SDC_CT_SD2)
        { /* SDv2? */
            RetVal = SDCardSendCmd(SDC_A_COMMAND13, 0, &respVal);
            if ((respVal == 0) && (RetVal == RETCODE_OK))
            { /* Read SD status */
                transmitByte = 0xff;
                if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, &transmitByte, SDC_ONE_BYTE, SPI_WRITE_ONLY))
                {
                    return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
                }

                if ((SDCardBlockReceive(csd, SDC_CSD_SIZE)) == RETCODE_OK)
                { /* Read partial block */
                    dataLength = SDC_CSD_BLOCK_PARTIAL_LENGTH - SDC_CSD_SIZE;
                    for (; dataLength; dataLength--)
                        if (SPI_SUCCESS != SPI_write(&SDCard_spiHandle, &transmitByte, SDC_ONE_BYTE, SPI_WRITE_ONLY)) /* Purge trailing data */
                        {
                            return (RETCODE(RETCODE_SEVERITY_ERROR, (Retcode_T) SDCARD_RW_FAIL));
                        }

                    *(uint32_t*) buffer = SDC_CSD_BLOCK_PARTIAL_SIZE << (csd[SDC_CSD_SIZE_OFFSET] >> (uint8_t) SDC_VALUE_FOUR);
                }
                else
                {
                    RetVal = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
                }
            }
            else
            {
                RetVal = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
            }
        }
        else
        { /* SDv1 or MMCv3 */
            RetVal = SDCardSendCmd(SDC_COMMAND9, 0, &respVal);
            if ((RetVal == RETCODE_OK) && (respVal == 0) && (SDCardBlockReceive(csd, SDC_CSD_SIZE) == RETCODE_OK))
            { /* Read CSD */
                if (SDCard_SDCardType & SDC_CT_SD1)
                { /* SDv1 */
                    *(uint32_t*) buffer = (((csd[SDC_CSD_BLOCK_PARTIAL] & 63) << (uint8_t) SDC_VALUE_ONE)
                            + ((uint16_t) (csd[SDC_CSD_TRAN_SPEED] & SDC_CSD_BLOCK_MASK) >> (uint8_t) SDC_VALUE_SEVEN) + (uint8_t) SDC_VALUE_ONE) << ((csd[SDC_CSD_TAAC] >> SDC_CSD_TAAC_OFFSET) - (uint8_t) SDC_VALUE_ONE);
                }
                else
                { /* MMCv3 */
                    *(uint32_t*) buffer = ((uint16_t) ((csd[SDC_CSD_BLOCK_PARTIAL] & SDC_CSD_BLOCK_PARTIAL_MASK) >> (uint8_t) SDC_VALUE_TWO) + (uint8_t) SDC_VALUE_ONE)
                            * (((csd[SDC_CSD_TRAN_SPEED] & (uint8_t) SDC_VALUE_THREE) << (uint8_t) SDC_VALUE_THREE) + ((csd[SDC_CSD_TRAN_SPEED] & SDC_CSD_TRAN_SPEED_MASK) >> SDC_CSD_TRAN_SPEED_OFFSET) + (uint8_t) SDC_VALUE_ONE);
                }
            }
            else
            {
                RetVal = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) SDCARD_RW_FAIL);
            }
        }
        break;

    default:
        RetVal = RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) SDCARD_NO_DISK);
        break;
    }

    SDCardDriver_Disconnect();
    return (RetVal);
}

#endif /* #if BCDS_FEATURE_SDCARD */

/**************************************************************************** */
