/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef SDCARDDRIVER_H_
#define SDCARDDRIVER_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_SDCARD

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#ifdef ENABLE_DMA
#define DELAY_ONE_MS          (portTickType) (1)
#endif

/* SD Card type definitions (CardType) */
#define SDC_CT_MMC			UINT8_C(0x01)
#define SDC_CT_SD1			UINT8_C(0x02)
#define SDC_CT_SD2			UINT8_C(0x04)
#define SDC_CT_SDC			UINT8_C((SDC_CT_SD1|SDC_CT_SD2))
#define SDC_CT_BLOCK		UINT8_C(0x08)
#define SDC_CT_HS			UINT32_C(0x1AA)
#define SDC_VOLTAGE_STATUS  UINT8_C(3)     /* SD-Card Voltage status */
#define SDC_HS_COMMAND_RSP  UINT8_C(2)      /* HS Command responds */

/* SD Card various commands definitions */
#define SDC_COMMAND0      UINT8_C(0)     		/* GO_IDLE_STATE */
#define SDC_COMMAND1      UINT8_C(1)      		/* SEND_OP_COND */
#define SDC_A_COMMAND41    UINT8_C(SDC_COMMAND41 | 0x80) /* SEND_OP_COND (SDC) */
#define SDC_COMMAND8      UINT8_C(8)         	/* SEND_IF_COND */
#define SDC_COMMAND9      UINT8_C(9)		    /**< SEND_CSD */
#define SDC_COMMAND12     UINT8_C(12)        	/* STOP_TRANSMISSION */
#define SDC_A_COMMAND13   UINT8_C(13 | 0x80)    /**< SD_STATUS (SDC) */
#define SDC_COMMAND16     UINT8_C(16)        	/* SET_BLOCKLEN */
#define SDC_COMMAND17     UINT8_C(17)	        /* READ_SINGLE_BLOCK */
#define SDC_COMMAND18     UINT8_C(18)   	    /* READ_MULTIPLE_BLOCK */
#define SDC_COMMAND23     UINT8_C(23)       	/* SET_BLOCK_COUNT */
#define SDC_A_COMMAND23   UINT8_C(SDC_COMMAND23| 0x80) /* SET_WR_BLK_ERASE_COUNT (SDC) */
#define SDC_COMMAND24     UINT8_C(24)       	/* WRITE_BLOCK */
#define SDC_COMMAND25     UINT8_C(25)      		/* WRITE_MULTIPLE_BLOCK */
#define SDC_COMMAND41     UINT8_C(41)        	/* SEND_OP_COND (ACMD) */
#define SDC_COMMAND55     UINT8_C(55)        	/* APP_CMD */
#define SDC_COMMAND58     UINT8_C(58)        	/* READ_OCR */

#define SDC_CSD_CRC						UINT8_C(0)
#define SDC_CSD_SIZE					UINT8_C(16)
#define SDC_CSD_CRC_OFFSET				UINT8_C(6)
#define SDC_CSD_CRC_SET					UINT8_C(1)
#define SDC_CSD_BLOCK_LENGTH			UINT8_C(9)
#define SDC_CSD_BLOCK_PARTIAL			UINT8_C(10)
#define SDC_CSD_BLOCK_PARTIAL_SIZE		UINT8_C(16)
#define SDC_CSD_BLOCK_PARTIAL_MASK		UINT8_C(124)
#define SDC_CSD_BLOCK_PARTIAL_LENGTH	UINT8_C(64)
#define SDC_CSD_BLOCK_OFFSET			UINT8_C(8)
#define SDC_CSD_SIZE_OFFSET				UINT8_C(10)
#define SDC_CSD_C_SIZE_MULT				UINT8_C(5)
#define SDC_CSD_C_SIZE_MASK				UINT8_C(15)
#define SDC_CSD_BLOCK_MASK				UINT8_C(128)
#define SDC_CSD_VDD_W_R_CURR_MAX		UINT8_C(7)
#define SDC_CSD_VDD_W_R_CURR_OFFSET		UINT8_C(2)
#define SDC_CSD_TRAN_SPEED 				UINT8_C(11)
#define SDC_CSD_TRAN_SPEED_OFFSET		UINT8_C(5)
#define SDC_CSD_TRAN_SPEED_MASK			UINT8_C(224)
#define SDC_CSD_TAAC	 				UINT8_C(13)
#define SDC_CSD_TAAC_OFFSET	 			UINT8_C(6)
/*Since the maximum number of bytes that can be written is 512, ring buffer size is also been increased to 512*/
#define SDC_RING_BUFFER_SIZE    	 UINT8_C(512)
#define SDC_LEVEL_SHIFT_LOW			 UINT8_C(0)
#define SDC_LEVEL_SHIFT_HIGH 		 UINT8_C(1)
#define SDC_EVEN_PIN				 UINT8_C(1)
#define SDC_ODD_PIN                  UINT8_C(0)
#define SDC_CARD_DETECTED			 UINT8_C(0)
#define SDC_CRC_DUMMY_DATAWRITE      UINT8_C(0xFF)
#define SDC_DISK_TYPE_VALIDATE		 UINT8_C(0x40)
#define SDC_SPI_DEFAULT_VALUE		 UINT8_C(0xFF)
#define SDC_ONE_BYTE			     UINT32_C(1)
#define SDC_TWO_BYTE                 UINT32_C(2)
#define SDC_SIX_BYTE                 UINT32_C(6)
#define SDC_REF_FRQ					 UINT32_C(0)   			/* Reference frequency for USART communication */
#define SDC_SINGLE_BLOCK_LENGTH		 UINT32_C(512) 			/* Number of byte in SDC card sector */
#define SDC_IDLE_VALUE				 UINT32_C(0x40000000) 	/* Register value setting for IDLE state */
#define SDC_VDD_VALIDATE			 UINT8_C(0xAA)          /* Voltage level checking command */
#define SDC_SEND_RETRY_COUNT		 UINT16_C(500)  		/* Value for timeout approximately 50ms */
#define SDC_VALIDATE_ACMD_SET        UINT8_C(0x80)
#define SDC_CRC_VALUE_COMMAND0		 UINT8_C(0x95)
#define SDC_CRC_VALUE_COMMAND8		 UINT8_C(0x87)
#define SDC_VALIDATE_ACMD_CLEAR      UINT8_C(0x7F)
#define SDC_START_COMMAND			 UINT8_C(0x40)
#define SDC_STOP_TOKEN				 UINT8_C(0xFD)
#define SDC_RESPONSE_CHECK           UINT8_C(0x1F)
#define SDC_VALID_TOKEN_RESP		 UINT8_C(0xFE)
#define SDC_BLOCK_SEND_TOKEN		 UINT8_C(0xFC)
#undef NULL
#define NULL 						 UINT8_C(0x00)

/* Enum is using to define constant numerical value in SD card modules */
enum SDC_Number_E
{
    SDC_VALUE_ZERO = UINT8_C(0), /**< sets SD-card value to zero */
    SDC_VALUE_ONE = UINT8_C(1), /**< sets SD-card value to one */
    SDC_VALUE_TWO = UINT8_C(2), /**< sets SD-card value to two */
    SDC_VALUE_THREE = UINT8_C(3), /**< sets SD-card value to three */
    SDC_VALUE_FOUR = UINT8_C(4), /**< sets SD-card value to four */
    SDC_VALUE_FIVE = UINT8_C(5), /**< sets SD-card value to five */
    SDC_VALUE_SEVEN = UINT8_C(7), /**< sets SD-card value to seven */
    SDC_VALUE_EIGHT = UINT8_C(8), /**< sets SD-card value to eight */
    SDC_VALUE_TEN = UINT8_C(10), /**< sets SD-card value to ten */
    SDC_VALUE_SIXTEEN = UINT8_C(16), /**< sets SD-card value to sixteen */
    SDC_VALUE_TWENTYFOUR = UINT8_C(24),/**< sets SD-card value to twenty four */
    SDC_VALUE_HUNDRED = UINT8_C(100), /**< sets SD-card value to hundred */
    SDC_VALUE_FIVEHUNDRED = UINT16_C(500) /**< sets SD-card value to five hundred */
};
typedef enum SDC_Number_E SDC_Number_T;


/* local function prototype declarations */
/**
 * @brief
 *      The SDCardInterruptHandler API checks the Port and pin of SD card pin detection.
 *      Based on that, the static global variable will be updated as SD card inserted
 *      or not inserted and also it will activate or de-activate the level shifter.
 *
 * @param[in] callBackParam1
 * 		Not used in this implementation.
 *
 * @param[in] callBackParam1
 * 		Not used in this implementation.
 */
static void SDCardInterruptHandler(void *callBackParam1, uint32_t callBackParam2);

/**
 * @brief
 *      The SDCardDetectCallBack API will submit the interrupt handler
 *      to RTOS daemon task which will executed after the ISR.
 *      The main work has been handled in the interrupt handler. To make the
 *      ISR very less work to do, this implementation has been followed.
 *
 * @note
 * 		This is a call back function triggered whenever the SD card is inserted or removed.
 */
static void SDCardDetectCallBack(void);

/**
 * @brief
 *      The SDCardPinSetting API is used to set the Port, pin, mode and dout
 *      of the SD card module related pins.
 */
static void SDCardPinSetting(void);

/**
 * @brief
 *      The SDCardEnableClocks API is used to enable the clock for high frequency
 *      crystal oscillator, GPIO, USART1 and core.
 */
static void SDCardEnableClocks(void);

/**
 * @brief
 *      The SDCardDetectRegInterrupt API is used to initialize the PTD module
 *      and register the call back function which have to be triggered when the
 *      SD card is getting inserted or removed.
 */
static void SDCardDetectRegInterrupt(void);

/**
 * @brief
 *      The SDCardSpiInitialize API is used to initialize the SPI with the
 *      required parameters of SD card communication.
 *
 * @retval
 * 		SDC_ERR_SPI_INIT_FAILED - SPI initialization failed.
 *
 * @retval
 * 		SDC_ERR_NO_ERROR - Initialization success.
 */
static Retcode_T SDCardSpiInitialize(void);

/**
 * @brief
 * 		The SDCardWaitReady API is used to Wait for SD card Ready.
 *
 * @retval
 *       0xff:  SD card ready.
 *
 * @retval
 *       other value: SD card not ready.
 */
static Retcode_T SDCardWaitReady(uint8_t* receiveByte);

/**
 * @brief
 *		  The SDCardSendCmd API is used to Send a command packet to SD card.
 *
 * @param[in] command
 *                  Command byte to send SD-Card.
 *
 * @param[in] argument
 *                  Data to transmit with command.
 *
 * @retval  uint8_t
 *  		       Response value.
 */
static Retcode_T SDCardSendCmd(uint8_t command, uint32_t argument,uint8_t* preceiveByte);

/**
 * @brief
 *         Send a data block to micro SD card.
 *
 * @param[in] sendBuffer
 *            data pointer to be Transmit.
 *
 * @note      Maximum Data byte for Block is 512.
 *
 * @param[in] token
 *            Data token for Stop or Not-Stop transmit.
 *
 * @retval
 * 		SDC_FAILURE - SD disk initialization failed
 * @retval
 * 		SDC_SUCCESS - Initialization success.
 */
static Retcode_T SDCardBlockSend(const uint8_t *sendBuffer, uint8_t token);

/**
 * @brief
 *         Send a data block to micro SD card.
 *
 * @param[in] receiveBuffer
 *            data pointer to store received data.
 *
 * @param[in] receiveLength
 *            length of data to read from  SD-card.
 *
 * @note      receiveLength (must be multiple of 4).
 *
 * @retval
 * 		SDC_FAILURE - SD disk initialization failed.
 *
 * @retval
 * 		SDC_SUCCESS - Initialization success.
 */
static Retcode_T SDCardBlockReceive(uint8_t *receiveBuffer,
    uint32_t receiveLength);
/* local module global variable declarations */

/* local inline function definitions */

#endif /* #if BCDS_FEATURE_SDCARD */

#endif /* SDCARDDRIVER_H_ */

/** ************************************************************************* */
