/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef LED_H_
#define LED_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_LED

/* local interface declaration ********************************************** */

/**< Call back function type to call call a function when OS timer expired */
typedef void (*LED_blinkFunc)(void*);

/** maximum allowed intensity for LED */
#define LED_INTENSITY_HIGH  (UINT8_C(100))

/** maximum allowed intensity for LED */
#define LED_FREQUENCY_LOW   (UINT8_C(583))

/** maximum allowed frequency for LED */
#define LED_FREQUENCY_HIGH  (UINT8_C(875))

/** Macro to enable the frequency/intensity of LED */
#define LED_ENABLED         (0xAA)

/** Macro to disable the frequency/intensity of LED */
#define LED_DISABLED        (0x99)

/** macro repersent the led minimum on time */
#define LED_ONTIME_MIN      (UINT32_C(0))

/** macro repersent the led maximum on time */
#define LED_ONTIME_MAX      (UINT32_MAX)

/** macro repersent the led minimum off time */
#define LED_OFFTIME_MIN     (UINT32_C(0))

/** macro represent the led maximum off time */
#define LED_OFFTIME_MAX     (UINT32_MAX)

/* local function prototype declarations */

/**
 *  @brief      Logic for LED blink with different modes.
 *
 * 	@param[in]  void* timerHandler - to match with the type of OS timer call back function
 */
static void ledBlink(void*);

/**
 *  @brief      Logic for LED breath with different modes.
 *
 *  @param[in]  void* timerHandler - to match with the type of OS timer call back function
 */
static void ledBreath(void*);

/**
 *  @brief      Turn On the LED.
 *
 *  @param[in]  LED_Types_T led - LED number
 */
static void ledOn(LED_Types_T led);

/**
 *  @brief      Turn Off the LED.
 *
 *  @param[in]  LED_Types_T led - LED number
 */
static void ledOff(LED_Types_T led);

/**
 *  @brief      Callback from ISR.
 */
static void ledCallbackISR(void);

/**
 * @brief      ISR interrupt handling
 *
 * @param[in]  callBackParam1
 *
 * @param[in]  callBackParam2
 */
static void ledInterruptHandling(void *callBackParam1, uint32_t callBackParam2);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* #if BCDS_FEATURE_LED */

#endif /* LED_H_ */

/** ************************************************************************* */
