/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_LED.h"
#include "LED.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_LED

#include <FreeRTOS.h>
#include <timers.h>
#include "PTD_portDriver_ih.h"
#include "TIM_timer_ih.h"
#include "BCDS_Assert.h"
#include "BCDS_PowerMgt.h"

/* constant definitions ***************************************************** */

/* local type and macro definitions */

#define LED_MACRO_TEMP(M)  { M##_GPIO_PORT,\
                             M##_GPIO_PIN,\
                             M##_GPIO_MODE_ENABLE,\
                             M##_GPIO_MODE_DISABLE,\
                             M##_GPIO_DOUT_ENABLE,\
                             M##_GPIO_DOUT_DISABLE }

/* LED GPIO Configuration table*/
const PTD_configInit_t LED_configTable[LED_MAX] = { LED_TYPE_MACRO };

#undef LED_MACRO_TEMP

#define LED_MACRO_TEMP(M)  M##_TIM_CC_PIN

/* LED Timer-3 Configuration table*/
const TIM_port_t LED_portTable[LED_MAX] = { LED_TYPE_MACRO };

#undef LED_MACRO_TEMP

#define LED_MACRO_TEMP(M)  M##_DEFAUTL_INTENSITY_PERCENT

/* LED Default intensity table*/
const uint8_t LED_defaultIntensity[LED_MAX] = { LED_TYPE_MACRO };

#undef LED_MACRO_TEMP

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* Common variable */
static LED_ParamConfig_T ledParamConfiguration[LED_MAX];
static LED_OperationModes_T ledOperationalmode[LED_MAX];
static uint16_t ledRepeatCount[LED_MAX];
static LED_Operations_T ledOperationState[LED_MAX];

/* Blink specific variable */
static xTimerHandle ledBlinkTimerHandle = NULL;
static uint8_t ledBlinkTimerEnabled;
static uint8_t ledBlinkEnable[LED_MAX];
static uint32_t ledBlinkTickInMs;

static uint32_t ledBlinkTimeCount[LED_MAX];
static uint8_t ledDoubleBlinkEnable[LED_MAX];
static uint32_t ledDoubleBlinkOffTimeInTick;
static uint8_t ledDoubleBlinkTriggered[LED_MAX];

/* Breath specific variable */
static xTimerHandle ledBreathTimerHandle = NULL;
static uint8_t ledBreathTimerEnabled;
static uint8_t ledBreathEnable[LED_MAX];
static uint32_t ledBreathTickInMs;
static TIM_breathConfig_t ledBreathConfig[LED_MAX];

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the private header */
static void ledInterruptHandling(void *callBackParam1, uint32_t callBackParam2)
{

    (void) callBackParam1;
    (void) callBackParam2;
    PowerMgt_SleepBlockEnd(DeepSleep_Mode);
}

/* The description is in the private header */
static void ledCallbackISR(void)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    if(pdPASS == xTimerPendFunctionCallFromISR(ledInterruptHandling, NULL, UINT32_C(0), &xHigherPriorityTaskWoken))
    {
    	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/* The description is in the private header */
static void ledOn(LED_Types_T led)
{

    TIM_pwmAlterDutyCycle(LED_TIMER_PORT, LED_portTable[led], LED_defaultIntensity[led]);
    ledOperationState[led] = LED_ON;
}

/* The description is in the private header */
static void ledOff(LED_Types_T led)
{

    TIM_pwmAlterDutyCycle(LED_TIMER_PORT, LED_portTable[led], UINT8_C(0));
    ledOperationState[led] = LED_OFF;
}

/* The description is in the private header */
static void ledBlink(void* timerHandler)
{
    (void) timerHandler;
    static uint8_t led;
    static uint8_t isBlinkEnabled = UINT8_C(0);
    for (led = UINT8_C(0); led < LED_MAX; led++)
    {
        if (ledBlinkEnable[led] == UINT8_C(1))
        {
            isBlinkEnabled = UINT8_C(1);
            if (ledRepeatCount[led] < (((uint16_t) ledParamConfiguration[led].repeatCount) * UINT16_C(2))) /* Blink LED with repeat count */
            {
                if (ledOperationState[led] == LED_OFF)
                {
                    ledBlinkTimeCount[led]++;
                    if (ledDoubleBlinkEnable[led] == UINT8_C(1)) /* Check for double blink enable */
                    {
                        if (ledBlinkTimeCount[led] >= ledDoubleBlinkOffTimeInTick) /* Check whether double blink off time is elapsed or not */
                        {
                            ledOn((LED_Types_T) led);
                            ledBlinkTimeCount[led] = UINT32_C(0);
                            ledDoubleBlinkEnable[led] = UINT8_C(2);
                        }
                    }
                    else if (ledBlinkTimeCount[led] >= ledParamConfiguration[led].offTimeInTick) /* Check whether normal blink off time is elapsed or not */
                    {
                        ledRepeatCount[led]++;
                        if (ledRepeatCount[led] < (((uint16_t) ledParamConfiguration[led].repeatCount) * UINT16_C(2)))
                        {
                            ledOn((LED_Types_T) led);
                            ledBlinkTimeCount[led] = UINT32_C(0);
                        }
                    }
                }
                else
                {
                    ledBlinkTimeCount[led]++;

                    if (ledBlinkTimeCount[led] >= ledParamConfiguration[led].onTimeInTick) /* Check whether normal blink on time is elapsed or not */
                    {
                        ledBlinkTimeCount[led] = UINT32_C(0);
                        if (ledDoubleBlinkEnable[led] == UINT8_C(2)) /* if ledDoubleBlinkEnable = 2 then off the LED and disable the double blink function */
                        {
                            ledOff((LED_Types_T) led);
                            ledRepeatCount[led]++;
                            ledDoubleBlinkEnable[led] = UINT8_C(0);
                        }
                        else
                        {
                            ledOff((LED_Types_T) led);
                            if (ledDoubleBlinkTriggered[led] == UINT8_C(1))
                            {
                                ledDoubleBlinkEnable[led] = UINT8_C(1);
                            }
                            else
                            {
                                ledRepeatCount[led]++;
                            }

                        }

                    }
                }
            }
            else if (ledParamConfiguration[led].repeatCount == UINT8_C(0)) /* if repeatCount = 0 which means non stop blink, the same logic of above case is followed without using repeat count*/
            {
                if (ledOperationState[led] == LED_OFF)
                {
                    ledBlinkTimeCount[led]++;
                    if (ledDoubleBlinkEnable[led] == UINT8_C(1)) /* Check for double blink enable */
                    {
                        if (ledBlinkTimeCount[led] >= ledDoubleBlinkOffTimeInTick) /* Check whether double blink off time is elapsed or not */
                        {
                            ledOn((LED_Types_T) led);
                            ledBlinkTimeCount[led] = UINT32_C(0);
                            ledDoubleBlinkEnable[led] = UINT8_C(2);
                        }
                    }
                    else if (ledBlinkTimeCount[led] >= ledParamConfiguration[led].offTimeInTick) /* Check whether normal blink off time is elapsed or not */
                    {
                        ledOn((LED_Types_T) led);
                        ledBlinkTimeCount[led] = UINT32_C(0);
                    }
                }
                else
                {
                    ledBlinkTimeCount[led]++;

                    if (ledBlinkTimeCount[led] >= ledParamConfiguration[led].onTimeInTick) /* Check whether normal blink on time is elapsed or not */
                    {
                        ledBlinkTimeCount[led] = UINT32_C(0);
                        if (ledDoubleBlinkEnable[led] == UINT8_C(2)) /* if ledDoubleBlinkEnable = 2 then off the LED and disable the double blink function */
                        {
                            ledOff((LED_Types_T) led);
                            ledDoubleBlinkEnable[led] = UINT8_C(0);
                        }
                        else
                        {
                            ledOff((LED_Types_T) led);
                            if (ledDoubleBlinkTriggered[led] == UINT8_C(1))
                            {
                                ledDoubleBlinkEnable[led] = UINT8_C(1);
                            }

                        }

                    }
                }
            }
            else if (ledRepeatCount[led] >= (((uint16_t) ledParamConfiguration[led].repeatCount) * UINT16_C(2)))/* when repeatCount is finished */
            {
                LED_deactivate((LED_Types_T) led);
                if (ledParamConfiguration[led].callback != NULL)
                {
                    ledParamConfiguration[led].callback(); /* Call back notification for the end of repeat count */
                }

            }
        }
    }
    if (isBlinkEnabled == UINT8_C(0)) /* When no LED is activated for any type of blink */
    {
        /* Checking if the timer handler is valid (not NULL)*/
        assert(ledBlinkTimerHandle != NULL);

        xTimerStop(ledBlinkTimerHandle, portMAX_DELAY);
        ledBlinkTimerEnabled = UINT8_C(0);
    }
}

/* The description is in the private header */
static void ledBreath(void* timerHandler)
{
    (void) timerHandler;
    static uint8_t led;
    uint8_t isBreathEnabled = UINT8_C(0);

    for (led = UINT8_C(0); led < LED_MAX; led++)
    {
        if (ledBreathEnable[led] == UINT8_C(1)) /* When breath is enabled */
        {
            if (ledParamConfiguration[led].repeatCount == UINT8_C(0))
            {
                isBreathEnabled = UINT8_C(1);
                ledBlinkTimeCount[led]++;
                if (ledBlinkTimeCount[led] >= ledParamConfiguration[led].offTimeInTick)
                {
                    ledBlinkTimeCount[led] = UINT8_C(0);
                    /*TIMER3 will not run in EM2, So, System shall not goes to EM2 untill breath mode is completed. */
                    PowerMgt_SleepBlockEnd(DeepSleep_Mode);
                    TIM_pwmBreathMode(LED_TIMER_PORT, LED_portTable[(LED_Types_T) led], &ledBreathConfig[(LED_Types_T) led]); /* activate the timer breath functionality */
                }
            }
        }
    }
    if (isBreathEnabled == UINT8_C(0)) /* When no LED is activated for any type of breath */
    {
        /* Checking if the timer handler is valid (not NULL)*/
        assert(ledBreathTimerHandle != NULL);

        xTimerStop(ledBreathTimerHandle, portMAX_DELAY);
        ledBreathTimerEnabled = UINT8_C(0);
    }
}

/* global functions ********************************************************* */

/* The description is in the public header */
extern void LED_init(uint32_t blinkTickInMs, uint32_t doubleBlinkOffTimeInTick, uint32_t breathTickInMs)
{
    static uint8_t led;

    ledBlinkTickInMs = blinkTickInMs; /* Basic tick time for OS timer of blink function  */
    ledBreathTickInMs = breathTickInMs;/* Basic tick time for OS timer of breath function  */
    ledDoubleBlinkOffTimeInTick = doubleBlinkOffTimeInTick;/* Time required in between blinks of a double blink */
    ledBlinkTimerHandle = xTimerCreate((const char * const ) "LED_blinkTimer", blinkTickInMs, pdTRUE, NULL, &ledBlink);
    ledBreathTimerHandle = xTimerCreate((const char * const ) "LED_breathTimer", breathTickInMs, pdTRUE, NULL, &ledBreath);

    /* Checking if the timer handler is valid (not NULL)*/
    assert(ledBlinkTimerHandle != NULL);

    /* Checking if the timer handler is valid (not NULL)*/
    assert(ledBreathTimerHandle != NULL);

    for (led = UINT8_C(0); led < LED_MAX; led++)
    {
        PTD_pinModeSet(LED_configTable[led].port, LED_configTable[led].pin, LED_configTable[led].modeEnable, LED_configTable[led].doutDisable);

        LED_deactivate((LED_Types_T) led); /* Initially deactivate all the LEDs */

    }
}

/* The description is in the public header */
extern void LED_deactivate(LED_Types_T led)
{
    /* De-initializing the general flags*/
    ledOperationalmode[led] = LED_MODE_OFF;
    ledRepeatCount[led] = UINT16_C(0);
    ledOff(led);

    /* De-initializing the blink specific flags*/
    ledBlinkEnable[led] = UINT8_C(0);
    ledBlinkTimeCount[led] = UINT32_C(0);
    ledDoubleBlinkEnable[led] = UINT8_C(0);
    ledDoubleBlinkTriggered[led] = UINT8_C(0);
    ledBlinkTimerEnabled = UINT8_C(0);

    /* De-initializing the breath specific flags*/
    ledBreathEnable[led] = UINT8_C(0);
    ledBreathTimerEnabled = UINT8_C(0);

}

/* The description is in the public header */
extern void LED_setParameters(LED_Types_T led, LED_ParamConfig_T *ledHandler)
{
    assert(ledHandler != NULL);

    /* Copy the valid parameters to LED_data */
    ledParamConfiguration[led] = *ledHandler;

    if (LED_INTENSITY_CHANGABLE == LED_ENABLED)
    {
        /* Check the  high limits of intensity percentage */
        assert((ledHandler[led].intensityPercentage <= LED_INTENSITY_HIGH));

        /* Set Intensity */
        TIM_pwmAlterDutyCycle(LED_TIMER_PORT, LED_portTable[led], ledParamConfiguration[led].intensityPercentage);
    }
    if (LED_FREQUENCY_CHANGABLE == LED_ENABLED)
    {
        /* Check the low and high limits of frequency */
        if ((ledHandler[led].frequency < LED_FREQUENCY_LOW) || (ledHandler[led].frequency > LED_FREQUENCY_HIGH))
        {
            assert(0);
        }

        /* Set Frequency */
        TIM_pwmAlterFrequency(LED_TIMER_PORT, ledParamConfiguration[led].frequency);
    }
}

/* The description is in the public header */
extern LED_ErrorTypes_T LED_activate(LED_Types_T led, LED_OperationModes_T mode)
{
    static LED_ErrorTypes_T ret = LED_INVALID_CONFIG;

    if (ledOperationalmode[led] == LED_MODE_OFF)
    {
        ret = LED_SUCCESS;
        ledOperationalmode[led] = mode;

        switch (mode)
        {
        case LED_MODE_ON:
            ledOn(led);
            break;

        case LED_MODE_DOUBLE_BLINK:

            ledDoubleBlinkTriggered[led] = UINT8_C(1); /* Trigger flag for double blink functionality */
            ledBlinkEnable[led] = UINT8_C(1); /* Set flag to activate LED blink */
            ledOn((LED_Types_T) led);
            ledOperationState[led] = LED_ON;
            if (ledBlinkTimerEnabled == UINT8_C(0))
            {
                /* Checking if the timer handler is valid (not NULL)*/
                assert(ledBlinkTimerHandle != NULL);
                ledBlinkTimerEnabled = UINT8_C(1);
                xTimerStart(ledBlinkTimerHandle, portMAX_DELAY);
            }
            break;

        case LED_MODE_BLINK:
            ledBlinkEnable[led] = UINT8_C(1); /* Set flag to activate LED blink */
            ledOn((LED_Types_T) led);
            ledOperationState[led] = LED_ON;
            if (ledBlinkTimerEnabled == UINT8_C(0))
            {
                /* Checking if the timer handler is valid (not NULL)*/
                assert(ledBlinkTimerHandle != NULL);
                ledBlinkTimerEnabled = UINT8_C(1);
                xTimerStart(ledBlinkTimerHandle, portMAX_DELAY);
            }
            break;

        case LED_MODE_MULTIPLE_BREATH:

            /* No break is required. It is fall through.*/

        case LED_MODE_BREATH:
            ledBreathEnable[led] = UINT8_C(1);
            if (mode == LED_MODE_BREATH)
            {
                ledBreathConfig[led].breathCount = UINT32_C(1); /* set breath count as 1 for LED_MODE_BREATH */
            }
            else
            {
                ledBreathConfig[led].breathCount = LED_MULTIPLE_BREATH_COUNT; /* set multiple breath count for LED_MODE_MULTIPLE_BREATH */
            }
            ledBreathConfig[led].timerMode = TIM_SET; /* set timer mode */
            ledBreathConfig[led].breathTimeInSeconds = LED_BREATH_TIME_IN_SEC;/* set breath time:default value is 1 second */
            ledBreathConfig[led].callbackFunction = &ledCallbackISR;
            if (ledBreathTimerEnabled == UINT8_C(0))
            {
                /* Checking if the timer handler is valid (not NULL)*/
                assert(ledBreathTimerHandle != NULL);

                ledBreathTimerEnabled = UINT8_C(1);
                ledBreath(NULL); /* Start the breath */
                xTimerStart(ledBreathTimerHandle, portMAX_DELAY);
            }
            break;

        case LED_MODE_OFF:
            /* Nothing to be done*/
            break;

        default:
            /* Should not reach here */
            assert(0);
            break;
        }
    }
    return ret;
}

/* The description is in the public header */
extern void LED_getParameters(LED_Types_T led, LED_ParamConfig_T *ledHandler)
{

    *ledHandler = ledParamConfiguration[led]; /* Copy the present working parameters of particular LED to the handler which
     * in turn points to a global structure  */
}

#endif /* #if BCDS_FEATURE_LED */

/** @} ******** */
