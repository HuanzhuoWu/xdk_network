/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef FLASHS25FL116K_H_
#define FLASHS25FL116K_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_FLASHS25FL116K

/* local interface declaration ********************************************** */

/* local type and macro definitions */

/* macros used for SPI communication */
#define SPI_CS_PIN_LOW();             GPIO_PinOutClear(PTD_PORT_MEM_CSN,PTD_PIN_MEM_CSN);
#define SPI_CS_PIN_HIGH();            GPIO_PinOutSet(PTD_PORT_MEM_CSN,PTD_PIN_MEM_CSN);
#define SFL_SPI_PORT_NUMBER           SPI1
#define SFL_SPI_CLK_MODE              SPI_CLOCK_MODE_CPOL0_CPHA0
#define SFL_SPI_ROUTE_LOCATION        SPI_ROUTE_LOCATION1
#define RING_BUFFER_SIZE              (UINT32_C(50))
#define SPI_LOW_BITRATE               (UINT32_C(7000000))

/* Macros used to enable CMU */
#define S25FL116K_CMU_USART           cmuClock_USART1
#define S25FL116K_CMU_GPIO            cmuClock_GPIO

/* macros used to verify flash device */
#define S25FL116K_MANUFACTURER_ID     (UINT8_C(0x01))
#define S25FL116K_DEVICE_TYPE         (UINT8_C(0x40))
#define S25FL116K_CAPACITY            (UINT8_C(0x15))
#define S25FL116K_STARTUP_DELAY       (UINT8_C(0x15))
#define S25FL116K_SECTOR_ERASE_DELAY  (UINT16_C(450))
#define S25FL116K_BLOCK_ERASE_DELAY   (UINT16_C(2000))
#define MASK_SR1_BUSY                 (UINT8_C(0x01))
#define S25FL116K_BUSY                (UINT8_C(1))
#define S25FL116K_PAGE_SIZE           (UINT8_C(256))
#define S25FL116K_BLOCK_SIZE          (UINT32_C(0x10000))
#define S25FL116K_SECTOR_SIZE         (UINT32_C(0x1000))

/* Flash S25FL116K instructions */
#define S25FL116K_READ_STATUS_REG_1    (UINT8_C(0x05))
#define S25FL116K_CHIP_ERASE           (UINT8_C(0xC7))
#define S25FL116K_READ_DEVICE_ID_2     (UINT8_C(0x9F))
#define S25FL116K_READ                 (UINT8_C(0x03))
#define S25FL116K_WRITE_ENABLE         (UINT8_C(0x06))
#define S25FL116K_PAGE_PROGRAM         (UINT8_C(0x02))
#define S25FL116K_SECTOR_ERASE         (UINT8_C(0x20))
#define S25FL116K_RELEASE_POWER_DOWN   (UINT8_C(0xAB))
#define S25FL116K_DEEP_POWER_DOWN      (UINT8_C(0xB9))
#define S25FL116K_BLOCK_ERASE          (UINT8_C(0xD8))

/* Macros to avoid magic numbers */
#define S25FL116K_DEFAULT_ZERO         (UINT8_C(0))
#define S25FL116K_DEFAULT_ONE          (UINT8_C(1))
#define S25FL116K_DEFAULT_TWO          (UINT8_C(2))
#define S25FL116K_DEVICEID_INDEX       (UINT8_C(3))
#define S25FL116K_DEVICEID_INDEX_CHECK (UINT8_C(2))
#define S25FL116K_MSEC_DELAY           (UINT32_C(600))

/* Macros to mask 24 bit address */
#define S25FL116K_FIRST_BYTE_MASK      (UINT32_C(0x0000FF))
#define S25FL116K_SECOND_BYTE_MASK     (UINT32_C(0x00FF00))
#define S25FL116K_THIRD_BYTE_MASK      (UINT32_C(0xFF0000))

/* Macros to use shift operations */
#define S25FL116K_SHIFT_ZERO           (UINT8_C(0))
#define S25FL116K_SHIFT_EIGHT          (UINT8_C(8))
#define S25FL116K_SHIFT_SIXTEEN        (UINT8_C(16))

/* local variables  */

/* local function prototype declarations */
/**
 * @brief        function responsible to write a data byte to flash module.
 *
 * @param [in]   writeData              Data to write.
 * @retval       FLASH_WRITE_FAIL
 * @retval       FLASH_WRITE_SUCCESS
 *
 */
static S25FL116K_flashReturnCode_T S25FL116K_writeByte(uint8_t writeData);

/**
 * @brief     function responsible to read a data byte from flash module.
 *
 * @param [in] readData            reference to hold the received data
 * @retval     FLASH_INVALID_PARAMETER  given data store location is invalid.
 * @retval     FLASH_READ_FAIL
 * @retval     FLASH_READ_SUCCESS
 *
 */
static S25FL116K_flashReturnCode_T S25FL116K_readByte(uint8_t *readData);

/**
 * @brief     function responsible to enable GPIO pins
 *
 */
static void S25FL116K_gpioActiveState(void);

/**
 * @brief     function responsible to enable clock sources
 *
 */
static void S25FL116K_spiClockInit(void);

/**
 * @brief     function responsible to initialize SPI for flash module.
 *
 * @retval     FLASH_FAIL     SPI initialization failed no further operations can be performed.
 * @retval     FLASH_SUCCESS  SPI initialization success.
 */
static S25FL116K_flashReturnCode_T S25FL116K_InitSpi(void);

/**
 * @brief     function responsible to disable SPI for flash module.
 *
 * @retval     FLASH_FAIL     SPI disable operation failed.
 * @retval     FLASH_SUCCESS  SPI disabled for flash module.
 */
static S25FL116K_flashReturnCode_T S25FL116K_deInitSpi(void);

/**
 * @brief     function responsible to check the device is in busy or not. If it is busy then no operations can be performed
 *
 * @retval     uint8_t  If return value is 0 then device is not busy, if it is 1 then device is busy.
 */
static uint8_t S25FL116K_isBusy(void);

/**
 * @brief      function responsible to enable write operations.
 *
 * @retval     FLASH_WRITE_FAIL        write operations could not be performed
 * @retval     FLASH_WRITE_SUCCESS     Write operations can be performed.
 */
static S25FL116K_flashReturnCode_T S25FL116K_writeEnable(void);

/**
 * @brief Tell the CPU to wait for a given amount of time.
 *
 * @param[in] :  requiredDelay The time of the delay
 */
static void S25FL116K_msDelay(uint32_t requiredDelay);

/**
 * @brief function responsible to drive 24 bit address into the SI pin
 *
 * @param[in] :  address The 32 bit address that is passed for operations
 * @retval       FLASH_SUCCESS   If operation is success.
 */
static S25FL116K_flashReturnCode_T S25FL116K_driveAddress(uint32_t address);

/**
 * @brief        function responsible to validate the input like address, buffer size and buffer pointer.
 *
 * @param [in]   address              Starting address for the read operation.
 * @param [out]  receiveBuffer        Buffer to store the data
 * @param [in]   receiveBufferSize    Buffer size, it is used to calculate end address for operation.
 *
 * @retval     FLASH_INVALID_PARAMETER  If any of the input in not a valid one
 * @retval     FLASH_SUCCESS            all inputs are valid
 */
static S25FL116K_flashReturnCode_T S25FL116K_inputValidation(uint32_t address, uint8_t* buffer, uint32_t bufferSize);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* #if BCDS_FEATURE_FLASHS25FL116K */

#endif /* FLASHS25FL116K_H_ */

/** ************************************************************************* */
