/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_FlashS25FL116K.h"
#include "FlashS25FL116K.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_FLASHS25FL116K

#include "em_cmu.h"
#include "SPI_ih.h"
#include "PTD_portDriver_ih.h"


/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static SPI_device_t externalFlashSpiHandle; /**< Global SPI driver handle */
static uint8_t transmitRingBuffer_gmd[RING_BUFFER_SIZE]; /**< Ring Buffer for low level SPI interface */
static uint8_t receiveRingBuffer_gmd[RING_BUFFER_SIZE]; /**< Ring Buffer for low level SPI interface */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_writeByte(uint8_t writeData)
{
    SPI_return_t returnStatus = SPI_FAILURE;
    uint8_t readData = S25FL116K_DEFAULT_ZERO;

    returnStatus = SPI_writeReadInOut(&externalFlashSpiHandle, &writeData, &readData);

    if (returnStatus != SPI_SUCCESS)
    {
        return (FLASH_WRITE_FAIL);
    }
    return (FLASH_WRITE_SUCCESS);
}

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_readByte(uint8_t *readData)
{
    SPI_return_t returnStatus = SPI_FAILURE;
    uint8_t data = S25FL116K_DEFAULT_ZERO;
    uint8_t readSpiData = S25FL116K_DEFAULT_ZERO;

    returnStatus = SPI_writeReadInOut(&externalFlashSpiHandle, &data, &readSpiData);

    if (returnStatus != SPI_SUCCESS)
    {
        return (FLASH_READ_FAIL);
    }

    *readData = readSpiData;

    return (FLASH_READ_SUCCESS);
}

/* Function description is in private header */
static void S25FL116K_msDelay(uint32_t requiredDelay)
{
    uint32_t startCount;
    uint32_t delayCount;
    static volatile uint32_t msecDelay = S25FL116K_DEFAULT_ZERO;

    for (startCount = 0UL; startCount < requiredDelay; startCount++)
        for (delayCount = 0UL; delayCount < S25FL116K_MSEC_DELAY; delayCount++)
        {
            msecDelay = msecDelay + 1 ;
        }
}

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_driveAddress(uint32_t address)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    returnStatus = S25FL116K_writeByte((address >> S25FL116K_SHIFT_SIXTEEN) & S25FL116K_FIRST_BYTE_MASK);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_FAIL);
    }
    returnStatus = S25FL116K_writeByte((address >> S25FL116K_SHIFT_EIGHT) & S25FL116K_FIRST_BYTE_MASK);
    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_FAIL);
    }
    returnStatus = S25FL116K_writeByte((address >> S25FL116K_SHIFT_ZERO) & S25FL116K_FIRST_BYTE_MASK);
    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_FAIL);
    }

    return (FLASH_SUCCESS);
}

/* Function description is in private header */
void S25FL116K_gpioSleepState(void)
{
    PTD_pinModeSet(PTD_PORT_MEM_DISABLE, PTD_PIN_MEM_DISABLE,PTD_MODE_MEM_DISABLE, PTD_DOUT_MEM_DISABLE);
}

/* Function description is in private header */
static void S25FL116K_gpioActiveState(void)
{

    PTD_pinModeSet(PTD_PORT_MEM_EN, PTD_PIN_MEM_EN, PTD_MODE_MEM_EN,
        PTD_DOUT_MEM_EN);

    PTD_pinModeSet(PTD_PORT_MEM_W_N, PTD_PIN_MEM_W_N, PTD_MODE_MEM_W_N,
        PTD_DOUT_MEM_W_N);

    PTD_pinModeSet(PTD_PORT_MEM_MOSI, PTD_PIN_MEM_MOSI, PTD_MODE_MEM_MOSI,
        PTD_DOUT_MEM_MOSI);

    PTD_pinModeSet(PTD_PORT_MEM_MISO, PTD_PIN_MEM_MISO, PTD_MODE_MEM_MISO,
        PTD_DOUT_MEM_MISO);

    PTD_pinModeSet(PTD_PORT_MEM_SCK, PTD_PIN_MEM_SCK, PTD_MODE_MEM_SCK,
        PTD_DOUT_MEM_SCK);

    PTD_pinModeSet(PTD_PORT_MEM_CSN, PTD_PIN_MEM_CSN, PTD_MODE_MEM_CSN,
        PTD_DOUT_MEM_CSN);

    PTD_driveModeSet(PTD_PORT_PORT_D, PTD_DRIVE_MODE_PORT_D);
}

/* Function description is in private header */
static void S25FL116K_spiClockInit(void)
{
    /* Enable clock to USART1/SPI module */
    CMU_ClockEnable(S25FL116K_CMU_USART, true);

    /* Enable clock to GPIO  */
    CMU_ClockEnable(S25FL116K_CMU_GPIO, true);
}

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_InitSpi(void)
{
    SPI_return_t spiReturnStatus = SPI_FAILURE;
    SPI_initParams_t initParams;

    initParams.portNumber = SFL_SPI_PORT_NUMBER;
    initParams.clockMode = SFL_SPI_CLK_MODE;
    initParams.routeLocation = SFL_SPI_ROUTE_LOCATION;
    initParams.txBuf_p = transmitRingBuffer_gmd;
    initParams.rxBuf_p = receiveRingBuffer_gmd;
    initParams.txBufSize = RING_BUFFER_SIZE;
    initParams.rxBufSize = RING_BUFFER_SIZE;
    initParams.baudrate = SPI_LOW_BITRATE;

    (void)S25FL116K_spiClockInit();

    (void)S25FL116K_gpioActiveState();

    spiReturnStatus = SPI_init(&externalFlashSpiHandle, &initParams);

    if (spiReturnStatus != SPI_SUCCESS)
    {
        return (FLASH_FAIL);
    }
    return (FLASH_SUCCESS);
}

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_deInitSpi(void)
{
    PTD_pinModeSet(PTD_PORT_MEM_MOSI, PTD_PIN_MEM_MOSI, PTD_MODE_MEM_MOSI_DISABLE,
        PTD_DOUT_MEM_MOSI);

    PTD_pinModeSet(PTD_PORT_MEM_MISO, PTD_PIN_MEM_MISO, PTD_DOUT_MEM_MISO_DISABLE,
        PTD_DOUT_MEM_MISO);

    PTD_pinModeSet(PTD_PORT_MEM_SCK, PTD_PIN_MEM_SCK, PTD_DOUT_MEM_SCK_DISABLE,
        PTD_DOUT_MEM_SCK);

    PTD_pinModeSet(PTD_PORT_MEM_CSN, PTD_PIN_MEM_CSN, PTD_DOUT_MEM_CSN_DISABLE,
        PTD_DOUT_MEM_CSN);

    return (FLASH_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_verifyDeviceId(void)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;
    uint8_t deviceInfo[S25FL116K_DEVICEID_INDEX] = { S25FL116K_DEFAULT_ZERO };
    uint8_t deviceIdIndex = S25FL116K_DEFAULT_ZERO;

    while (S25FL116K_isBusy());

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_READ_DEVICE_ID_2);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_FAIL);
    }

    for (deviceIdIndex = S25FL116K_DEFAULT_ZERO; deviceIdIndex <= S25FL116K_DEVICEID_INDEX_CHECK; deviceIdIndex++)
    {
        returnStatus = S25FL116K_readByte(&deviceInfo[deviceIdIndex]);

        if (returnStatus != FLASH_READ_SUCCESS)
        {
        	SPI_CS_PIN_HIGH();
            return (FLASH_FAIL);
        }
    }

    SPI_CS_PIN_HIGH();

    if ((deviceInfo[S25FL116K_DEFAULT_ZERO] != S25FL116K_MANUFACTURER_ID) ||
        (deviceInfo[S25FL116K_DEFAULT_ONE] != S25FL116K_DEVICE_TYPE) ||
        (deviceInfo[S25FL116K_DEFAULT_TWO] != S25FL116K_CAPACITY))
    {
        return (FLASH_FAIL);
    }

    return (FLASH_SUCCESS);
}

/* Function description is in private header */
static uint8_t S25FL116K_isBusy(void)
{
    uint8_t busy = S25FL116K_DEFAULT_ZERO;
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_READ_STATUS_REG_1);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (S25FL116K_BUSY);
    }
    returnStatus = S25FL116K_readByte(&busy);

    if (returnStatus != FLASH_READ_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (S25FL116K_BUSY);
    }

    SPI_CS_PIN_HIGH();

    return (busy & MASK_SR1_BUSY);
}

/* Function description is in private header */
static S25FL116K_flashReturnCode_T S25FL116K_writeEnable(void)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    while (S25FL116K_isBusy());

    SPI_CS_PIN_LOW();
    returnStatus = S25FL116K_writeByte(S25FL116K_WRITE_ENABLE);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_WRITE_FAIL);
    }
    SPI_CS_PIN_HIGH();

    return (FLASH_WRITE_SUCCESS);
}

/* global functions ********************************************************* */

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_flashPowerDown(void)
{
    (void)S25FL116K_msDelay(S25FL116K_DEFAULT_ONE);

    if (FLASH_SUCCESS != S25FL116K_deInitSpi())
    {
        return (FLASH_FAIL);
    }

    S25FL116K_gpioSleepState();

    return (FLASH_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_flashPowerUp(void)
{
    S25FL116K_gpioActiveState();

    if (FLASH_SUCCESS == S25FL116K_InitFlash())
    {
        SPI_CS_PIN_LOW();

        if (FLASH_WRITE_SUCCESS != S25FL116K_writeByte(S25FL116K_RELEASE_POWER_DOWN))
        {
        	SPI_CS_PIN_HIGH();
            return (FLASH_FAIL);
        }

        SPI_CS_PIN_HIGH();

        (void)S25FL116K_msDelay(S25FL116K_DEFAULT_ONE);
    }
    else
    {
        return (FLASH_FAIL);
    }
    return (FLASH_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_chipErase(void)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    while (S25FL116K_isBusy());

    returnStatus = S25FL116K_writeEnable();

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_CHIP_ERASE_FAIL);
    }

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_CHIP_ERASE);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_CHIP_ERASE_FAIL);
    }

    SPI_CS_PIN_HIGH();

    return (FLASH_CHIP_ERASE_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_blockErase(uint32_t address)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;
    while(S25FL116K_isBusy());

    /* the flash is divided into blocks, so the address should be a valid starting address */
    if((address % S25FL116K_BLOCK_SIZE) != S25FL116K_DEFAULT_ZERO)
    {
        return (FLASH_INVALID_PARAMETER);
    }

    returnStatus = S25FL116K_writeEnable();

    if (returnStatus != FLASH_WRITE_SUCCESS)
      {
          return (FLASH_BLOCK_ERASE_FAIL);
      }

    SPI_CS_PIN_LOW();
    returnStatus = S25FL116K_writeByte(S25FL116K_BLOCK_ERASE);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_BLOCK_ERASE_FAIL);
    }
    returnStatus = S25FL116K_driveAddress(address);
    if (returnStatus != FLASH_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_BLOCK_ERASE_FAIL);
    }
    SPI_CS_PIN_HIGH();

    /* sector erase time delay is required before initiating any command */
    (void) S25FL116K_msDelay(S25FL116K_BLOCK_ERASE_DELAY);

    return (FLASH_BLOCK_ERASE_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_sectorErase(uint32_t address)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    /* the flash is divided into sector, so the address should be a valid starting address */
    if((address % S25FL116K_SECTOR_SIZE) != S25FL116K_DEFAULT_ZERO)
    {
        return (FLASH_INVALID_PARAMETER);
    }

    while (S25FL116K_isBusy());

    returnStatus = S25FL116K_writeEnable();

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_SECTOR_ERASE_FAIL);
    }

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_SECTOR_ERASE);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_SECTOR_ERASE_FAIL);
    }
    returnStatus = S25FL116K_driveAddress(address);

    if (returnStatus != FLASH_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_SECTOR_ERASE_FAIL);
    }
    SPI_CS_PIN_HIGH();

    /* sector erase time delay is required before initiating any command */
    (void) S25FL116K_msDelay(S25FL116K_SECTOR_ERASE_DELAY);

    return (FLASH_SECTOR_ERASE_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_readData(uint32_t address, uint8_t* receiveBuffer, uint32_t receiveBufferSize)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;
    uint32_t receiveBufferindex = S25FL116K_DEFAULT_ZERO;

    /* the buffer to store data, cant be NULL */
    if( NULL == receiveBuffer )
    {
        return (FLASH_INVALID_PARAMETER);
    }

    while (S25FL116K_isBusy());

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_READ);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_FAIL);
    }

    returnStatus = S25FL116K_driveAddress(address);

    if (returnStatus != FLASH_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_FAIL);
    }

    for (receiveBufferindex = S25FL116K_DEFAULT_ZERO; receiveBufferindex < receiveBufferSize; receiveBufferindex++)
    {
        returnStatus = S25FL116K_readByte(&receiveBuffer[receiveBufferindex]);

        if (returnStatus != FLASH_READ_SUCCESS)
        {
        	SPI_CS_PIN_HIGH();
            return (FLASH_FAIL);
        }
    }

    SPI_CS_PIN_HIGH();

    return (FLASH_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_page_program(uint32_t address, uint8_t* transmitBuffer, uint32_t transmitBufferSize)
{
    uint32_t transmitBufferindex = S25FL116K_DEFAULT_ZERO;
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    /* validate the inputs */
    if( FLASH_SUCCESS != S25FL116K_inputValidation((uint32_t)address,(uint8_t*)transmitBuffer,(uint32_t)transmitBufferSize))
    {
        return (FLASH_INVALID_PARAMETER);
    }

    while (S25FL116K_isBusy());

    returnStatus = S25FL116K_writeEnable();

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
        return (FLASH_FAIL);
    }

    SPI_CS_PIN_LOW();

    returnStatus = S25FL116K_writeByte(S25FL116K_PAGE_PROGRAM);

    if (returnStatus != FLASH_WRITE_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_FAIL);
    }

    returnStatus = S25FL116K_driveAddress(address);
    if (returnStatus != FLASH_SUCCESS)
    {
    	SPI_CS_PIN_HIGH();
        return (FLASH_FAIL);
    }

    for (transmitBufferindex = S25FL116K_DEFAULT_ZERO; transmitBufferindex < transmitBufferSize; transmitBufferindex++)
    {
        returnStatus = S25FL116K_writeByte(transmitBuffer[transmitBufferindex]);

        if (returnStatus != FLASH_WRITE_SUCCESS)
        {
        	SPI_CS_PIN_HIGH();
            return (FLASH_FAIL);
        }
    }
    SPI_CS_PIN_HIGH();

    return (FLASH_SUCCESS);
}

/* Function description is in interface header */
S25FL116K_flashReturnCode_T S25FL116K_InitFlash(void)
{
    S25FL116K_flashReturnCode_T returnStatus = FLASH_FAIL;

    returnStatus = S25FL116K_InitSpi();

    if (returnStatus != FLASH_SUCCESS)
    {
        return (FLASH_FAIL);
    }

    /* Power-Up to Write time delay is required before initiating a write command */
    (void)S25FL116K_msDelay(S25FL116K_STARTUP_DELAY);

    returnStatus = S25FL116K_verifyDeviceId();

    if (returnStatus != FLASH_SUCCESS)
    {
        return (FLASH_FAIL);
    }

    return (FLASH_SUCCESS);
}

/* Function description is in configuration header */
static S25FL116K_flashReturnCode_T S25FL116K_inputValidation(uint32_t address, uint8_t* buffer, uint32_t bufferSize)
{
    /* the buffer to store data, cant be NULL */
    if( NULL == buffer )
    {
        return (FLASH_INVALID_PARAMETER);
    }

    /* since the page size is 256 bytes, we cant read more than that */
    if(bufferSize > S25FL116K_PAGE_SIZE)
    {
        return (FLASH_INVALID_PARAMETER);
    }

    /* since we are reading by page, the starting address should be the page's start address */
    if((address % S25FL116K_PAGE_SIZE) != 0)
    {
        return (FLASH_INVALID_PARAMETER);
    }

    return(FLASH_SUCCESS);
}

#endif /* #if BCDS_FEATURE_FLASHS25FL116K */

/** ************************************************************************* */
