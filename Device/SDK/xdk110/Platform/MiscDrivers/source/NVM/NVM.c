/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_NVM.h"

/* system header files */
#include "BCDS_Basics.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_NVM

#include "FSH_flash_ih.h"

/* constant definitions ***************************************************** */
#define WORD_SIZE 2
#define DWORD_SIZE 4
#define LWORD_SIZE 8

enum Section_Update_E
{
    SECTION_UPDATE_REQUIRED = 0,
    SECTION_UPDATE_NOT_REQUIRED,
    SECTION_UPDATE_FAIL_TO_CHECK
};

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @return true if current architecture is little endian, otherwise false
 */
static bool isLittleEndianPlatform(void);

/**
 * @brief Check if the endianness of the given item differs from the
 *        endianness of the underlying platform.
 *
 * @param item with specified endianness
 * @return true (if item and platform have different endianness) or false
 *         (if item and platform have same endianess)
 */
static bool NVM_IsEndiannessDifferent(struct NVM_Item_S item);

/**
 * @brief Swap the order of bytes of the given value and length.
 *
 * The function does not do a safety check if swapping is necessary
 * for the given data.
 *
 * @param[in] value that need to be swapped
 * @param length amount of bytes that need to be swapped
 * @return nothing
 * @example If bytes was in little endian the result is a byte order
 *          of big endian and vice versa.
 */
static void NVM_SwapBytes(void *value, uint32_t length);

/**
 * @brief Compare the current flash memory content of the section
 *        section buffer are same.
 *
 * @retval UPDATE_NOT_REQUIRED if the buffer and memory contains the same
 *                             data, indicating update of section not required
 * @retval UPDATE_REQUIRED if the buffer contains different data,
 *                         indicating update of section is required
 */
static enum Section_Update_E sectionUpdateRequired(const struct NVM_S *nvm_obj);

/**
 * @brief Check consistency of the given item
 * @retval RETCODE_OK if the given item is okay
 * @return NVM-return codes if the item has inconsistencies in size,
 *         addresses etc.
 */
static Retcode_T itemConsistencyCheck(const struct NVM_S *nvm_obj,
        struct NVM_Item_S item, const uint32_t length);

/**
 * @brief Check consistency of the given object
 * @retval RETCODE_OK if the given object is okay
 * @return NVM-return codes if the given object doesn't meet the
 *         expectations and includes inconsistencies e.g. in size, or has
 *         uninitialized buffers
 */
static Retcode_T objectConsistencyCheck(const struct NVM_S *nvm_obj);

/* global functions ********************************************************* */

/* The function description is available in the interface header */
Retcode_T NVM_Init(struct NVM_S *nvm_obj)
{
    Retcode_T retcode = objectConsistencyCheck(nvm_obj);
    if (RETCODE_OK != retcode)
        return retcode;

    /* Get the section data and fill the buffer for future reference */
    FSH_errorCode_t fshRetCode;
    fshRetCode = FSH_ReadByteStream((uint8_t*) (nvm_obj->section.start_address),
            (uint8_t*) nvm_obj->page_buffer, nvm_obj->page_buffer_size_byte);
    if (FSH_SUCCESS != fshRetCode)
    {
        retcode = (Retcode_T)RETCODE_FAILURE;
    }
    return retcode;
}

static Retcode_T objectConsistencyCheck(const struct NVM_S *nvm_obj)
{
    if (0 != (nvm_obj->page_buffer_size_byte % 4)
            || (nvm_obj->section.length_byte % 4 != 0)
            || (nvm_obj->section.page_length_byte % 4 != 0)
            ) /* size of buffer or section is not 4-bytes-aligned */
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    if (NULL == nvm_obj->page_buffer)
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INTEGRITY_FAILED);

    return RETCODE_OK;
}

/* The function description is available in the interface header */
Retcode_T NVM_DeInit(const struct NVM_S *nvm_obj)
{
    Retcode_T retcode = objectConsistencyCheck(nvm_obj);
    if (RETCODE_OK != retcode)
        return retcode;

    return NVM_Flush(nvm_obj);
}

/* The function description is available in the interface header */
Retcode_T NVM_Read(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        void *value, uint32_t length)
{
    Retcode_T retcode = objectConsistencyCheck(nvm_obj);
    if (RETCODE_OK != retcode)
        return retcode;

    retcode = itemConsistencyCheck(nvm_obj, item, length);
    if (RETCODE_OK != retcode)
        return retcode;

    uint32_t relative_item_address = item.start_address - nvm_obj->section.start_address;

    memcpy(value, (void*) ((nvm_obj->page_buffer) + relative_item_address), length);

    if (TRUE == NVM_IsEndiannessDifferent(item))
    {
        NVM_SwapBytes((void *) value, length);
    }

    return RETCODE_OK;
}

static Retcode_T itemConsistencyCheck(const struct NVM_S *nvm_obj,
        struct NVM_Item_S item, const uint32_t length)
{
    if ((item.length_byte != length)
            || (0 == length)
            || (0 == item.length_byte)
            )
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);
    }
    else if (item.start_address < nvm_obj->section.start_address)
    {
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INTEGRITY_FAILED);
    }

    return RETCODE_OK;
}

/* The function description is available in the interface header */
Retcode_T NVM_Write(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const void *value, uint32_t length)
{
    Retcode_T retcode = objectConsistencyCheck(nvm_obj);
    if (RETCODE_OK != retcode)
        return retcode;

    retcode = itemConsistencyCheck(nvm_obj, item, length);
    if (RETCODE_OK != retcode)
        return retcode;

    if (NVM_MODE_R == item.mode)
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_WRITE_FORBIDDEN);

    uint32_t relative_item_address = item.start_address - nvm_obj->section.start_address;

    memcpy(((nvm_obj->page_buffer) + relative_item_address), value, length);

    if (TRUE == NVM_IsEndiannessDifferent(item))
    {
        NVM_SwapBytes((void *) ((nvm_obj->page_buffer) + relative_item_address), length);
    }

    return RETCODE_OK;
}

Retcode_T NVM_Flush(const struct NVM_S *nvm_obj)
{
    enum Section_Update_E sectionUpdateStatus = SECTION_UPDATE_REQUIRED;

    Retcode_T retcode = objectConsistencyCheck(nvm_obj);
    if (RETCODE_OK != retcode)
        return retcode;

    sectionUpdateStatus = sectionUpdateRequired(nvm_obj);

    if (SECTION_UPDATE_REQUIRED == sectionUpdateStatus)
    {
        FSH_errorCode_t fshRetCode;
        fshRetCode = FSH_ErasePage((uint32_t*) nvm_obj->section.start_address);
        if (FSH_SUCCESS != fshRetCode)
        {
            return (Retcode_T)RETCODE_FAILURE;
        }
        else
        {
            fshRetCode = FSH_WriteByteStream((uint8_t*) nvm_obj->section.start_address,
                    (uint8_t*) nvm_obj->page_buffer, nvm_obj->section.length_byte);
            if (FSH_SUCCESS != fshRetCode)
            {
                return (Retcode_T)RETCODE_FAILURE;
            }
            else
            {
            	return (Retcode_T)RETCODE_OK;
            }
        }
    }
    else if (SECTION_UPDATE_FAIL_TO_CHECK == sectionUpdateStatus)
    {
        return (Retcode_T)RETCODE_FAILURE;
    }
    else if (SECTION_UPDATE_NOT_REQUIRED == sectionUpdateStatus)
    {
        return (Retcode_T)RETCODE_OK;
    }

    return (Retcode_T)RETCODE_FAILURE;
}

static enum Section_Update_E sectionUpdateRequired(const struct NVM_S *nvm_obj)
{
    uint32_t length_word = nvm_obj->section.length_byte / sizeof(uint32_t);
    char read_word[sizeof(uint32_t)];
    FSH_errorCode_t fshRetCode = FSH_WRITEFAIL;

    for (uint32_t i = UINT32_C(0); i < length_word; i++)
    {
        fshRetCode = FSH_ReadWordStream((uint32_t*)(nvm_obj->section.start_address) + i,
                (uint32_t*) read_word, UINT32_C(1));

        if (FSH_SUCCESS != fshRetCode)
        {
            return SECTION_UPDATE_FAIL_TO_CHECK;
        }
        else if (0 != memcmp(&nvm_obj->page_buffer[i], read_word, sizeof(uint32_t)))
        {
            return SECTION_UPDATE_REQUIRED;
        }
        else
        {
            /* do nothing */
        }
    } /* End of for loop */
    return SECTION_UPDATE_NOT_REQUIRED;
}

/* *******Functions for typed access to NVM items ************************* */

/* ********* Typed READ Access Functions **************** */

Retcode_T NVM_ReadInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	int8_t *value)
{
    if (item.length_byte != sizeof(int8_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(int8_t));
}

Retcode_T NVM_ReadUInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	uint8_t *value)
{
    if (item.length_byte != sizeof(uint8_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(uint8_t));
}

Retcode_T NVM_ReadInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	int16_t *value)
{
    if (item.length_byte != sizeof(int16_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(int16_t));
}

Retcode_T NVM_ReadUInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	uint16_t *value)
{
    if (item.length_byte != sizeof(uint16_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(uint16_t));
}

Retcode_T NVM_ReadInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	int32_t *value)
{
    if (item.length_byte != sizeof(int32_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(int32_t));
}

Retcode_T NVM_ReadUInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	uint32_t *value)
{
    if (item.length_byte != sizeof(uint32_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(uint32_t));
}

Retcode_T NVM_ReadInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	int64_t *value)
{
    if (item.length_byte != sizeof(int64_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(int64_t));
}

Retcode_T NVM_ReadUInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	uint64_t *value)
{
    if (item.length_byte != sizeof(uint64_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Read(nvm_obj, item, value, sizeof(uint64_t));
}

/* ********* Typed WRITE Access Functions **************** */

Retcode_T NVM_WriteInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const int8_t *value)
{
    if (item.length_byte != sizeof(int8_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(int8_t));
}

Retcode_T NVM_WriteUInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const uint8_t *value)
{
    if (item.length_byte != sizeof(uint8_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(uint8_t));
}

Retcode_T NVM_WriteInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const int16_t *value)
{
    if (item.length_byte != sizeof(int16_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(int16_t));
}

Retcode_T NVM_WriteUInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const uint16_t *value)
{
    if (item.length_byte != sizeof(uint16_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(uint16_t));
}

Retcode_T NVM_WriteInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const int32_t *value)
{
    if (item.length_byte != sizeof(int32_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(int32_t));
}

Retcode_T NVM_WriteUInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const uint32_t *value)
{
    if (item.length_byte != sizeof(uint32_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(uint32_t));
}

Retcode_T NVM_WriteInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
	const int64_t *value)
{
    if (item.length_byte != sizeof(int64_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(int64_t));
}

Retcode_T NVM_WriteUInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const uint64_t *value)
{
    if (item.length_byte != sizeof(uint64_t))
        return RETCODE(RETCODE_SEVERITY_ERROR, NVM_RETCODE_INVALID_SIZE);

    return NVM_Write(nvm_obj, item, value, sizeof(uint64_t));
}

/* The function description is available in the prototype declaration */
static bool isLittleEndianPlatform(void)
{
    uint16_t x = 1;
    uint8_t *c = (uint8_t*) &x;
    if (*c)
    { /* current architecture is little-endian (efm32 is little-endian) */
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/* The function description is available in the prototype declaration */
static bool NVM_IsEndiannessDifferent(struct NVM_Item_S item)
{
    if (NVM_ENDIANNESS_NONE == item.endianness)
        return FALSE;

    bool littleEndPlatform = isLittleEndianPlatform();
    if ((TRUE == littleEndPlatform && NVM_ENDIANNESS_BIG == item.endianness)
            || (FALSE == littleEndPlatform
                    && NVM_ENDIANNESS_LITTLE == item.endianness))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/* The function description is available in the prototype declaration */
static void NVM_SwapBytes(void *value, uint32_t length)
{
    typedef unsigned char Word[WORD_SIZE];
    typedef unsigned char DWord[DWORD_SIZE];
    typedef unsigned char LWord[LWORD_SIZE];

    unsigned char temp;

    if (WORD_SIZE == length)
    {
        Word *src = (Word *) value;
        temp = (*src)[0];
        (*src)[0] = (*src)[1];
        (*src)[1] = temp;

        return;
    }

    if (DWORD_SIZE == length)
    {
        DWord *src = (DWord *) value;
        temp = (*src)[0];
        (*src)[0] = (*src)[3];
        (*src)[3] = temp;

        temp = (*src)[1];
        (*src)[1] = (*src)[2];
        (*src)[2] = temp;

        return;
    }

    if (LWORD_SIZE == length)
    {
        LWord *src = (LWord *) value;
        temp = (*src)[0];
        (*src)[0] = (*src)[7];
        (*src)[7] = temp;

        temp = (*src)[1];
        (*src)[1] = (*src)[6];
        (*src)[6] = temp;

        temp = (*src)[2];
        (*src)[2] = (*src)[5];
        (*src)[5] = temp;

        temp = (*src)[3];
        (*src)[3] = (*src)[4];
        (*src)[4] = temp;

        return;
    }
}

#endif /* #if BCDS_FEATURE_NVM */
