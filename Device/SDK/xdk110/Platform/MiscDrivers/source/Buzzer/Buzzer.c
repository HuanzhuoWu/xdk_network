/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_Buzzer.h"
#include "Buzzer.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_BUZZER

#include <FreeRTOS.h>
#include <timers.h>
#include "PTD_portDriver_ih.h"
#include "TIM_timer_ih.h"
#include "BCDS_Assert.h"


/* local variables ********************************************************** */
/* Variable used for storing the present state ie(on/off) of buzzer */
static Buzzer_State_T currentBuzzerState;

/* Variable used for storing configuration parameter of buzzer */
static Buzzer_Param_T buzzerParamConfiguration;

/* Used to count the no of repetitions */
static uint16_t buzzerTimeCounter;

/* Time handler for OS_timer for each buzzer */
static xTimerHandle buzzerTimerHandler;

/* Variable used to indicate the present status of buzzer */
static uint8_t buzzerPresentState = UINT8_C(0);

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the private header */
static void buzzerOn(void)
{

	/* Enable the GPIO for PWM output to buzzer */
	PTD_pinModeSet(BUZZER_PTD_PORT, BUZZER_PTD_PIN, BUZZER_PTD_MODE_ENABLE, BUZZER_PTD_DOUT_ENABLE);
	currentBuzzerState = BUZZER_ON;
}

/* The description is in the private header */
static void buzzerOff(void)
{

	/* Disable the GPIO from PWM output */
	PTD_pinModeSet(BUZZER_PTD_PORT, BUZZER_PTD_PIN, BUZZER_PTD_MODE_DISABLE, BUZZER_PTD_DOUT_DISABLE);
	currentBuzzerState = BUZZER_OFF;
}

/* The description is in the private header */
static void buzzerActivate(void *timerHandle)
{

	BCDS_UNUSED(timerHandle);

	/* Checking if the timer handler is valid (not NULL)*/
	assert(NULL != buzzerTimerHandler);

    if (buzzerTimeCounter < (((uint16_t) buzzerParamConfiguration.repeatCount) * UINT16_C(2)))
    {

    	if (UINT32_C(0) == buzzerParamConfiguration.onTimeInMilliSec) /* Validated for ZERO and assign with ONE,since this validation is not available in freeRTOS */
		{
			buzzerParamConfiguration.onTimeInMilliSec = UINT32_C(1);
		}

        if (portMAX_DELAY != buzzerParamConfiguration.onTimeInMilliSec) /* Validated for portMAX_DELAY to assist the task to wait Infinitely (without timing out) */
        {
        	buzzerParamConfiguration.onTimeInMilliSec /= portTICK_RATE_MS;
        }

        /* BUZ_timeCount is incremented till it equals with repeat count */
        buzzerTimeCounter++;
        if (currentBuzzerState == BUZZER_OFF)
        {
            /* Currently buzzer is in off State, now load on time and switch on the buzzer */
            xTimerChangePeriod(buzzerTimerHandler, buzzerParamConfiguration.onTimeInMilliSec, portMAX_DELAY);
            buzzerOn();
        }
        else
        {
            /* Currently buzzer is in on State ,now load Off time and switch off the buzzer */
            xTimerChangePeriod(buzzerTimerHandler, buzzerParamConfiguration.onTimeInMilliSec, portMAX_DELAY);
            buzzerOff();
        }
    }
    else
    {

        /* Currently buzzerTimeCounter is equal to the buzzerParamConfiguration.repeatCount*2, so turn off buzzer and stop OS_timer */
        Buzzer_deactivate();
        if (buzzerParamConfiguration.callback != NULL)
        {

            buzzerParamConfiguration.callback();
        }
    }
}

/* global functions ********************************************************* */

/* The description is in the public header */
void Buzzer_init(void)
{
    /* Initially  disable the GPIO from PWM output and create an OS_timer with on time and with auto reload function */
    PTD_pinModeSet(BUZZER_PTD_PORT, BUZZER_PTD_PIN, BUZZER_PTD_MODE_DISABLE, BUZZER_PTD_DOUT_DISABLE);
    buzzerTimerHandler = xTimerCreate((const char * const ) "BUZ_buzzerTimer", BUZZER_DEFAULT_INIT_TIME, pdTRUE, NULL, &buzzerActivate);

	/* Checking if the timer has been created successfully*/
	assert(NULL != buzzerTimerHandler);

	buzzerOff();
}

/* The description is in the public header */
uint8_t Buzzer_activate(void)
{

	/* Set the buzzer status as 1 ie.ON */
	if (buzzerPresentState == UINT8_C(0))
	{
		buzzerPresentState = UINT8_C(1);
		buzzerActivate(NULL);
	}

	return (buzzerPresentState);
}

/* The description is in the public header */
void Buzzer_deactivate(void)
{
	/* Checking if the timer handler is valid (not NULL)*/
	assert(NULL != buzzerTimerHandler);
	buzzerOff();
	buzzerTimeCounter = UINT16_C(0);
    xTimerStop(buzzerTimerHandler, portMAX_DELAY);
    buzzerPresentState = UINT8_C(0);
}

/* The description is in the public header */
void Buzzer_setParameters(Buzzer_Param_T *buzzerParamConfig)
{

	/* Check the low limit of volume */
	assert(NULL != buzzerParamConfig);

	/* Check the low and high limits of volume */
	assert(buzzerParamConfig->volume <= BUZZER_VOLUME_HIGH);

	/* Check the low and high limits of pitch */
	if ((buzzerParamConfig->pitch < BUZZER_PITCH_LOW) || (buzzerParamConfig->pitch > BUZZER_PITCH_HIGH))
	{
		assert(0);
	}

	/* Copy the valid parameters to buzzerParamConfiguration */
	buzzerParamConfiguration = *buzzerParamConfig;

	/* Set volume */
	TIM_pwmAlterDutyCycle(BUZZER_TIMER_PORT, BUZZER_TIMER_CC_PIN, buzzerParamConfig->volume);

	/* Set pitch */
	TIM_pwmAlterFrequency(BUZZER_TIMER_PORT, buzzerParamConfig->pitch);

}

/* The description is in the public header */
void Buzzer_getParameters(Buzzer_Param_T *buzzerHandle)
{

	/* Copy the valid working parameters of buzzer back to global structure */
	*buzzerHandle = buzzerParamConfiguration;
}

/* The description is in the public header */
void Buzzer_setVolume(uint8_t volume)
{

	/* Check the low and high limits of volume */
	assert((volume <= BUZZER_VOLUME_HIGH));

	/* update the working parameter structure */
	buzzerParamConfiguration.volume = volume;

	/* Set volume */
	TIM_pwmAlterDutyCycle(BUZZER_TIMER_PORT, BUZZER_TIMER_CC_PIN, volume);
}

/* The description is in the public header */
void Buzzer_setPitch(uint16_t pitch)
{

	/* Check the low and high limits of pitch */
	if ((pitch < BUZZER_PITCH_LOW) || (pitch > BUZZER_PITCH_HIGH))
	{
		assert(0);
	}

	/* update the working parameter structure */
	buzzerParamConfiguration.pitch = pitch;

	/* Set pitch */
	TIM_pwmAlterFrequency(BUZZER_TIMER_PORT, pitch);
}

/* The description is in the public header */
void Buzzer_setRepeatCount(uint8_t repeatCount)
{

	/* update the working parameter structure */
	buzzerParamConfiguration.repeatCount = repeatCount;
}

/* The description is in the public header */
void Buzzer_activePeriodTime(uint32_t milliseconds)
{

	/* update the working parameter structure */
	buzzerParamConfiguration.onTimeInMilliSec = milliseconds;
}

/* The description is in the public header */
void Buzzer_deactivePeriodTime(uint32_t milliseconds)
{
	/* update the working parameter structure */
	buzzerParamConfiguration.offTimeInMilliSec = milliseconds;
}

#endif /* #if BCDS_FEATURE_BUZZER */

/****************************************************************************/
