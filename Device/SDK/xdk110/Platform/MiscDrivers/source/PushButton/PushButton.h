/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef PUSHBUTTON_H_
#define PUSHBUTTON_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_PUSHBUTTON

/* local interface declaration ********************************************** */

/* local type and macro definitions */

/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */

/**
 * @brief
 * This is the function to execute from the timer service/ daemon task (from OS_timerPendFunctionCallFromISR()).
 * The event type and duration of key press is calculated in this API. From this function,
 * the call-back is provided to the application layer with the corresponding event and time(in ms).
 *
 * @note
 * As this is a deferred handler, if a higher priority task is running or time slicing interferes, even more than 1 millisecond delay could be
 * introduced to the measurement results.
 *
 * param[in] callBackParam1 : The value of the call-back function's first parameter.
 *                            The parameter has a void * type to allow it to be used to pass any type.
 *
 * param[in] callBackParam2 : The value of the call-back function's second parameter.
 *
 */
static void pushbuttonInterruptHandling(void *callBackParam1, uint32_t callBackParam2);

/**
 * @brief
 *   This is the callback function, which is assigned to an Interrupt Service Routine to service the push/release events.
 *   When the push button is pressed or released, the interrupt arises and this function is invoked, as it is
 *   mapped to the callback of PTD_intConfig api.
 */
static void pushbuttonIsrCallback(void);

#endif /* #if BCDS_FEATURE_PUSHBUTTON */

#endif /* PUSHBUTTON_H_ */

/** ************************************************************************* */
