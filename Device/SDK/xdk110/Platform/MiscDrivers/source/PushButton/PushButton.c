/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* Own Header Files   */
#include "BCDS_PushButton.h"
#include "PushButton.h"

/* additional interface header files */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_PUSHBUTTON

#include <FreeRTOS.h>
#include <timers.h>
#include "BCDS_Assert.h"
#include "BCDS_PowerMgt.h"

/* local prototypes ********************************************************* */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* A static global variable to store the time at which the last event occurred */
static uint64_t lastEventTimeInMs = UINT64_C(0);

/* Enumeration to store the event type */
static PushButton_EventType_T pushbuttonEventType;

/* Instance of the callback function is created to use it for invoking the function */
static PushButton_CallbackEvent_T pushbuttonCallBackEvent;

/* To indicate whether the initialization of Push button is done or not */
static uint8_t pushbuttonIntializedStatus = UINT8_C(0);

/* To avoid the interrupts raised due to bouncing, this variable is made use of */
volatile uint32_t pushbuttonInterruptHandled = UINT32_C(1);

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the private header */
static void pushbuttonInterruptHandling(void *callBackParam1, uint32_t callBackParam2)
{
    /* To suppress warnings */
    (void) callBackParam1;
    (void) callBackParam2;

    uint64_t lastEventTimeDiffInMs = UINT64_C(0);
    uint64_t currentTimeInMs = UINT64_C(0);

    /* Read the pad value for a single pin in the GPIO port. */
    pushbuttonEventType = (PushButton_EventType_T) PTD_pinInGet(PUSH_BUTTON_GPIO_PORT, PUSH_BUTTON_GPIO_PIN);

	currentTimeInMs = PowerMgt_GetSystemTimeMs();

    lastEventTimeDiffInMs = currentTimeInMs - lastEventTimeInMs;

    lastEventTimeInMs = currentTimeInMs;

    /* To avoid the events created by bouncing - If the time difference between consecutive events is less than the
     * debounce time, then the call back to the application is not send.
     */
    if (lastEventTimeDiffInMs >= PUSH_BUTTON_DEBOUNCE_TIME_DELAY_MS)
    {
        pushbuttonCallBackEvent(pushbuttonEventType, lastEventTimeDiffInMs);
    }

    /* After the delay, the flag is set for interrupt */
    pushbuttonInterruptHandled = UINT32_C(1);

}

/* The description is in the private header */
static void pushbuttonIsrCallback(void)
{
    if (UINT32_C(1) == pushbuttonInterruptHandled)
    {
        pushbuttonInterruptHandled = UINT32_C(0);
        portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
        if(pdPASS == xTimerPendFunctionCallFromISR(pushbuttonInterruptHandling, NULL, UINT32_C(0), &xHigherPriorityTaskWoken))
        {
        	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
        }
    }
}

/* global functions ********************************************************* */

/* The description is in the public header */
void PushButton_init(PushButton_Config_T *config)
{
    /* Check is made for Null pointer */
    assert(config != NULL);

    pushbuttonCallBackEvent = config->callBackEvent;

    /* The mode is set for the pin configured for push button */
    PTD_pinModeSet(PUSH_BUTTON_GPIO_PORT,
    PUSH_BUTTON_GPIO_PIN,
    PUSH_BUTTON_GPIO_MODE,
    PUSH_BUTTON_GPIO_DOUT);

    /* The interrupt pin is configured in the PTD module for the Push button. */
    PTD_intConfig( PUSH_BUTTON_GPIO_PORT,
    PUSH_BUTTON_GPIO_PIN,
    PUSH_BUTTON_RISING_EDGE_INT_ENABLE,
    PUSH_BUTTON_FALLING_EDGE_INT_ENABLE,
    PUSH_BUTTON_CONFIG_AFTER_ENABLE_INT,
        (PTD_intrCallback) &pushbuttonIsrCallback);

    /* The flag is updated, as the interrupt config is done */
    pushbuttonIntializedStatus = UINT8_C(1);
}

/* The description is in the public header */
void PushButton_resetTime(void)
{
    lastEventTimeInMs = UINT64_C(0);
}

/* The description is in the public header */
void PushButton_enable(void)
{
    PTD_intClear(PUSH_BUTTON_GPIO_PIN);
    PTD_intEnable(PUSH_BUTTON_GPIO_PIN);
}

/* The description is in the public header */
void PushButton_disable(void)
{
    PTD_intDisable(PUSH_BUTTON_GPIO_PIN);
}

/* The description is in the public header */
uint8_t PushButton_isInitialized(void)
{
    return (pushbuttonIntializedStatus);
}

#endif /* #if BCDS_FEATURE_PUSHBUTTON */
