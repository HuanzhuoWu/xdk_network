/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_LED_H_
#define BCDS_LED_H_

/* public interface declaration ********************************************* */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_LED

/* public type and macro definitions */

/** Function pointer type for repeat completed
 for LED blink*/
typedef void (*LED_completeCallBack)(void);

/** Function pointer type for repeat completed
 LED breath*/
typedef void (*LED_breathCallBack)(void);

#define LED_MACRO_TEMP(M) M
/** LED enum have the number of LEDs to be used.
 */
enum LED_Types_E
{
    LED_TYPE_MACRO,
    LED_MAX
};

typedef enum LED_Types_E LED_Types_T;

#undef LED_MACRO_TEMP
/** LED return enum have the return state of LED including success,invalid configuration,
 */
enum LED_ErrorTypes_E
{
    LED_SUCCESS,
    LED_INVALID_CONFIG,
};

typedef enum LED_ErrorTypes_E LED_ErrorTypes_T;

/** LED state enum have the states of LED.
 */
enum LED_Operations_E
{
    LED_OFF,
    LED_ON,
};

typedef enum LED_Operations_E LED_Operations_T;

/** @brief     LED param structure have the members that are used to do the
 *             configuration of each LEDs.
 *  @note      To make the LED glow continuously(non stop blink) make Repeat_Count as 0
 */
struct LED_ParamConfig_S
{
    uint8_t intensityPercentage; /**< The intensity percentage - 0 to 100% */
    uint8_t repeatCount; /**< The number of times the LED have to be On and Off */
    uint16_t frequency; /**< Frequency */
    uint32_t onTimeInTick; /**< The LED will glow for this time duration */
    uint32_t offTimeInTick; /**< The LED will be off for this time duration */
    LED_completeCallBack callback; /**< Call back function for LED glow complete */
};

typedef struct LED_ParamConfig_S LED_ParamConfig_T;

/** @brief     LED mode enum have the different modes in which a LED can be be operated
 */
enum LED_OperationModes_E
{
    LED_MODE_OFF,
    LED_MODE_ON,
    LED_MODE_BLINK,
    LED_MODE_DOUBLE_BLINK,
    LED_MODE_BREATH,
    LED_MODE_MULTIPLE_BREATH,
    LED_MODE_MAX,
};

typedef enum LED_OperationModes_E LED_OperationModes_T;

/* public global variable declarations */

/* public function prototype declarations */
/**
 * @brief       Initialize the LED module
 *
 * @param[in]   blinkTickInMs - tick for the OS_timer in milliseconds
 *
 * @param[in]   doubleBlinkOffTimeInTick - intermediate time required in between two blinks of a double blink
 *
 * @param[in]   breathTickInMs -  Time required for a breath
 *
 * @warning     onTimeInTick,offTimeInTick should be multiple of blinkTickInMs,since it is the basic time of OS timer
 *              eg: if user need Led ON time as 1sec(1000ms),and if configured blinkTickInMs = 10ms,then onTimeInTick = 10ms x 100ms
 *              Also make sure that doubleBlinkOffTimeInTick is less than offTimeInTick.
 */
extern void LED_init(uint32_t blinkTickInMs, uint32_t doubleBlinkOffTimeInTick, uint32_t breathTickInMs);

/**
 *  @brief      Set LED parameters required or its functioning.
 *
 *  @param[in]  LED_Types_T led - The Specific LED to be used
 *
 *  @param[in]  LED_ParamConfig_T ledHandler - The pointer to the configuration structure
 */
extern void LED_setParameters(LED_Types_T led, LED_ParamConfig_T *ledHandler);

/**
 *  @brief      Activate LED for its different modes
 *
 *  @note       - Activate for LED_MODE_ON,LED_MODE_BLINK,LED_MODE_DOUBLE_BLINK,
 *                LED_MODE_BREATH and LED_MODE_MULTIPLE_BREATH.
 *
 *  @param[in]  LED_Types_T led - The Specific LED to be used
 *
 *  @param[in]  LED_OperationModes_T mode - Mode of LED ie. LED_MODE_ON,LED_MODE_BLINK,LED_MODE_DOUBLE_BLINK etc
 *
 *  @return     LED_ErrorTypes_T - return the success or the error.
 */
extern LED_ErrorTypes_T LED_activate(LED_Types_T led, LED_OperationModes_T mode);

/**
 *  @brief      Deactivate the LED from its current mode.
 *
 *  @param[in]  LED_Types_T led - The Specific LED to be used to get switched off
 */
extern void LED_deactivate(LED_Types_T led);

/**
 *  @brief      Get the current status of the LED
 *
 *  @param[in]  LED_Types_T led - The Specific LED to be used
 *
 *  @param[out] LED_ParamConfig_T ledHandler - The handler to a structure
 */
extern void LED_getParameters(LED_Types_T led, LED_ParamConfig_T *ledHandler);

/* inline function definitions */

#endif /* #if BCDS_FEATURE_LED */

#endif /* BCDS_LED_H_ */
