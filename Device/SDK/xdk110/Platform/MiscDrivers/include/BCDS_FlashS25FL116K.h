/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_FLASHS25FL116K_H_
#define BCDS_FLASHS25FL116K_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_FLASHS25FL116K

/* public type and macro definitions */
typedef enum S25FL116K_flashReturnCode_E
{
    FLASH_SUCCESS,
    FLASH_FAIL,
    FLASH_WRITE_SUCCESS,
    FLASH_READ_SUCCESS,
    FLASH_WRITE_FAIL,
    FLASH_READ_FAIL,
    FLASH_CHIP_ERASE_FAIL,
    FLASH_CHIP_ERASE_SUCCESS,
    FLASH_SECTOR_ERASE_FAIL,
    FLASH_SECTOR_ERASE_SUCCESS,
    FLASH_BLOCK_ERASE_FAIL,
    FLASH_BLOCK_ERASE_SUCCESS,
    FLASH_INVALID_PARAMETER
} S25FL116K_flashReturnCode_T;

/* public interface declaration ********************************************* */

/**
 * @brief      function responsible to enable external flash S25FL116K.
 *
 * @retval     FLASH_FAIL              Initialization of S25FL116K failed,read/write operations could not be performed.
 * @retval     FLASH_SUCCESS           S25FL116K initialized successfully.
 */
S25FL116K_flashReturnCode_T S25FL116K_InitFlash(void);

/**
 * @brief        function responsible to read data from S25FL116K from the given address
 *
 * @param [in]   address              Starting address for the read operation.
 * @param [out]  receiveBuffer        Buffer to store the data
 * @param [in]   receiveBufferSize    Buffer size, it is used to calculate end address for operation.
 *
 * @retval     FLASH_FAIL              Read operation failed
 * @retval     FLASH_SUCCESS           Read operation success
 */
S25FL116K_flashReturnCode_T S25FL116K_readData(uint32_t address, uint8_t* receiveBuffer, uint32_t receiveBufferSize);

/**
 * @brief        function responsible to write one page data(256 bytes) in S25FL116K.
 *
 * @param [in]   address              Starting address for the read operation.
 * @param [out]  transmitBuffer        Buffer to store the data
 * @param [in]   transmitBufferSize    Buffer size, it is used to calculate end address for operation.
 *
 * @retval     FLASH_FAIL              write operation failed
 * @retval     FLASH_SUCCESS           write operation success
 */
S25FL116K_flashReturnCode_T S25FL116K_page_program(uint32_t address, uint8_t* transmitBuffer, uint32_t transmitBufferSize);

/**
 * @brief      function responsible to execute chip erase command. The Chip Erase command sets
 *             all memory within the device to the erased state of all 1 (FFh).
 *
 * @retval     FLASH_CHIP_ERASE_FAIL
 * @retval     FLASH_CHIP_ERASE_SUCCESS
 */
S25FL116K_flashReturnCode_T S25FL116K_chipErase(void);

/**
 * @brief      function responsible to execute block erase command. The Block Erase command sets all
 *             memory within a specified sector (64 kbytes) to the erased state of all 1 (FFh).
 *
 * @param [in]   address              Starting address for the block erase operation.
 *
 * @retval     FLASH_SECTOR_ERASE_FAIL
 * @retval     FLASH_SECTOR_ERASE_SUCCESS
 */
S25FL116K_flashReturnCode_T S25FL116K_blockErase(uint32_t address);

/**
 * @brief      function responsible to execute sector erase command. The Sector Erase command sets all
 *             memory within a specified sector (4 kbytes) to the erased state of all 1 (FFh).
 *
 * @param [in]   address              Starting address for the sector erase operation.
 *
 * @retval     FLASH_SECTOR_ERASE_FAIL
 * @retval     FLASH_SECTOR_ERASE_SUCCESS
 */
S25FL116K_flashReturnCode_T S25FL116K_sectorErase(uint32_t address);

/**
 * @brief      function responsible to put flash memory in lowest power consumption state(Deep-Power Down : 0.002 mA)
 *
 * @retval     FLASH_FAIL              Flash memory is not in deep power down mode
 * @retval     FLASH_SUCCESS           Flash memory is entered in deep power down mode
 */
S25FL116K_flashReturnCode_T S25FL116K_flashPowerDown(void);

/**
 * @brief      function responsible to put flash memory back to normal read/program operations.
 * Flash memory power consumption :
 * Serial Read 50 MHz  - 7 mA
 * Program/Erase       - 20 mA
 *
 * @retval     FLASH_FAIL              Flash memory is not in normal mode
 * @retval     FLASH_SUCCESS           Flash memory is in normal mode (Ready for read/program/erase operations)
 */
S25FL116K_flashReturnCode_T S25FL116K_flashPowerUp(void);

/**
 * @brief     function responsible to clear GPIO pins
 *
 */
void S25FL116K_gpioSleepState(void);


/**
 * @brief     function responsible to check the device ID.
 *
 * @retval     FLASH_FAIL     Device ID verification failed no further operation can be performed.
 * @retval     FLASH_SUCCESS  Device ID verification success.
 */
S25FL116K_flashReturnCode_T S25FL116K_verifyDeviceId(void);
/* public global variable declarations */

/* inline function definitions */

#endif /* #if BCDS_FEATURE_FLASHS25FL116K */

#endif /* BCDS_FLASHS25FL116K_H_ */

/** ************************************************************************* */
