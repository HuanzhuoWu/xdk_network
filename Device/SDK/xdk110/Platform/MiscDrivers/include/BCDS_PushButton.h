/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */

#ifndef BCDS_PUSHBUTTON_IH_H_
#define BCDS_PUSHBUTTON_IH_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_PUSHBUTTON

/* public interface declaration ********************************************* */
#include "PTD_portDriver_ih.h"

/* public type and macro definitions */

/* Events that occur during push button operation */

enum PushButton_EventType_E
{
    PUSH_BUTTON_HIGH_TO_LOW, /* Falling Edge */
    PUSH_BUTTON_LOW_TO_HIGH, /* Rising Edge */
    PUSH_BUTTON_MAX_EVENT
};

typedef enum PushButton_EventType_E PushButton_EventType_T;

/* Callback for the button operation. */
typedef void (*PushButton_CallbackEvent_T)(PushButton_EventType_T pushbuttonEventType, uint64_t lastEventTimeDiffInMs);

/** structure that holds the push button configurations */
struct PushButton_Config_S
{
    /* Debounce time in ms */
    uint8_t deBounceInMs;

    /* Callback of the button event */
    PushButton_CallbackEvent_T callBackEvent;
};

typedef struct PushButton_Config_S PushButton_Config_T;

/* public global variable declarations */

/* public function prototype declarations */
/**
 * @brief
 *   This function initializes the Push button module, by configuring it to the GPIO pin.
 *   This is achieved using the PTD driver module. The ISR for push button is mapped to the
 *   callback of PTD module. So, if the PTD module senses a change in event, then the ISR gets
 *   executed.
 *
 * @param[in]   : PushButton_Config_T - A pointer to the PushButton_Config_T structure
 *
 */
extern void PushButton_init(PushButton_Config_T *initailisations);

/**
 * @brief
 * 	This API is used to reset the time of the last event occurrence. This can be called in the
 * 	application after the button is released.
 */
extern void PushButton_resetTime(void);

/**
 * @brief
 * 	This API enables the Push Button feature when it is disabled.
 */
extern void PushButton_enable(void);

/**
 * @brief
 * 	This API disables the Push Button feature. This can be called when the system does not
 * 	need to respond to the pushes of the button.
 */
extern void PushButton_disable(void);

/**
 * @brief
 * 	This API tells the status of the push button, i.e. whether it is initialized or not.
 *
 * 	@retval  1 - Push button is initialized
 * 	@retval  0 - Push button is not initialized
 */
extern uint8_t PushButton_isInitialized(void);

#endif /* #if BCDS_FEATURE_PUSHBUTTON */

#endif /* BCDS_PUSHBUTTON_IH_H_ */
