/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BCDS_BUTTON_H_
#define BCDS_BUTTON_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_BUTTON

/* public interface declaration */

/* public type and macro definitions */

/**
 * @brief   Enumeration to represent the Button return codes.
 */
enum Button_ResultCode_E
{
    BUTTON_FAILURE,
    BUTTON_SUCCESS,
	BUTTON_GPIO_INIT_RECURSIVE,
	BUTTON_RESOURCE_BUSY,
	BUTTON_INVALID_REQUEST
};

/**
 * @brief   Typedef to represent the Button return code.
 */
typedef enum Button_ResultCode_E Button_ResultCode_T;

/**
 * @brief Enumeration to represent the Events that occur during push button operation.
 */
enum Button_EventType_E
{
	BUTTON_PRESS_EVENT, /* Rising Edge */
	BUTTON_RELEASE_EVENT, /* Falling Edge */
    BUTTON_MAX_EVENT
};

/**
 * @brief Typedef to represent the Events that occur during push button operation.
 */
typedef enum Button_EventType_E Button_EventType_T;


/**
 * @brief Enumeration to represent the Button configurations .
 */
enum Button_Config_E
{
	BUTTON_CONFIG_FALLING_EDGE,
	BUTTON_CONFIG_RISING_EDGE,
	BUTTON_CONFIG_FALLING_RISING_EDGE
};

/**
 * @brief Typedef to represent the  Button configurations.
 */
typedef enum Button_Config_E Button_Config_T;

/**
 * @brief   This data type represents a function pointer which would be executed
 *          when any event (error/status/...) is to be notified.
 *
 * @param [in]  eventType : The argument containing relevant event information.
 * @param [in]  lastEventTimeDiffInMs : The argument containing the time in msec of last event time
 * difference
 */
typedef void (*Button_EventCB_T)(Button_EventType_T eventType, uint32_t lastEventTimeDiffInMs);

/* public function prototype declarations */

/**
 * @brief   Structure to represent the Button Driver functions.
 */
struct Button_Module_S
{

    /**
     * @brief   Pointer to initialize Button Interface.
     *          This would enable the clock configurations.
     *
     * @param [in]  eventCB     : Callback to be called in case of any event notification.
     *
     * @retval  This would return the status of Button initialization.
     */
    Button_ResultCode_T (*initialize) (Button_EventCB_T eventCB);

    /**
     * @brief   Pointer to uninitialize PushButton Interface.
     *
     * @retval  This would return the status of Button uninitialization.
     */
    Button_ResultCode_T (*uninitialize) (void);

    /**
     * @brief   Pointer to control Button Interface power.
     *
     * @param [in]  state       : state of Power that the GPIO peripheral is to be set.
     *
     * @retval  This would return the status of Button Interface power control.
     */
    Button_ResultCode_T (*powerControl) (MiscDrivers_PowerState_T state);

    /**
     * @brief   Pointer to control a Button Interface.
     *
     * @param [in]  config      : Button configuration based on the interrupt type.
     *
     * @note : single as well as multiple control can be performed in single call.
     *
     *         For example :
     *
     *         Button_Driver.Control \
     *         BUTTON_CONFIG_FALLING_RISING_EDGE);
     *
     *
     * @retval  This would return the status of Button interfcae control operation.
     */
    Button_ResultCode_T (*control) (Button_Config_T config);
};

/**
 * @brief   Typedef to represent the Button Driver functions.
 */
typedef struct Button_Module_S const Button_Module_T;

/**
 * @brief   Button driver object through which all related member
 * functions can be accessed.
 */
extern Button_Module_T Button_Module;


/* public global variable declarations */

/* inline function definitions */

#endif /* #if BCDS_FEATURE_BUTTON */

#endif /* BCDS_BUTTON_H_ */

/** ************************************************************************* */



