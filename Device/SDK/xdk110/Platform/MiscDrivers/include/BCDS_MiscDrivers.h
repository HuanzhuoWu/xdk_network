/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BCDS_MISCDRIVERS_H_
#define BCDS_MISCDRIVERS_H_

/* public interface declaration */
#include "BCDS_MiscDriversInfo.h"
#include "BCDS_MiscDriversConfig.h"
#include "BCDS_Basics.h"
/* public type and macro definitions */

#ifndef BCDS_FEATURE_BUTTON
/**
 * This macro enables the build of the Button feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_BUTTON 1
#endif

#ifndef BCDS_FEATURE_PUSHBUTTON
/**
 * This macro enables the build of the PushButton feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_PUSHBUTTON 1
#endif

#ifndef BCDS_FEATURE_LED
/**
 * This macro enables the build of the LED feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_LED 1
#endif

#ifndef BCDS_FEATURE_BUZZER
/**
 * This macro enables the build of the Buzzer feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_BUZZER 1
#endif

#ifndef BCDS_FEATURE_FLASHS25FL116K
/**
 * This macro enables the build of the FLASHS25FL116K feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_FLASHS25FL116K 1
#endif

#ifndef BCDS_FEATURE_FLASHW25Q256FV
/**
 * This macro enables the build of the FLASHW25Q256FV feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_FLASHW25Q256FV 1
#endif

#ifndef BCDS_FEATURE_NVM
/**
 * This macro enables the build of the NVM feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_NVM 1
#endif

#ifndef BCDS_FEATURE_SDCARD
/**
 * This macro enables the build of the SDCard feature.
 * By default the feature is enabled.
 */
#define BCDS_FEATURE_SDCARD 1
#endif

/**
 * @brief   Structure to represent the BCDS miscellaneous driver version.
 */
struct MiscDrivers_DriverVersion_S {
  uint16_t api;
  uint16_t drv;
};

/**
 * @brief   Typedef to represent the BCDS miscellaneous driver version.
 */
typedef struct MiscDrivers_DriverVersion_S MiscDrivers_DriverVersion_T;

/**
 * @brief   Enumeration to represent the BCDS miscellaneous device power states.
 */
enum MiscDrivers_PowerState_E {
  MISCDRIVERS_POWER_STATE_OFF,
  MISCDRIVERS_POWER_STATE_LOW,
  MISCDRIVERS_POWER_STATE_FULL
};

/**
 * @brief   Typedef to represent the BCDS miscellaneous device power states.
 */
typedef enum MiscDrivers_PowerState_E MiscDrivers_PowerState_T;

/* public function prototype declarations */

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_MISCDRIVERS_H_    */

/** ************************************************************************* */
