/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
/**
 *
 *  @file
 *
 *  Interface header for the non-volatile memory (NVM)
 *
 */

#ifndef BCDS_NVM_H_
#define BCDS_NVM_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_NVM

#include "BCDS_Basics.h"
#include "BCDS_Retcode.h"

/**
 * @brief return values for Nvm_read* and Nvm_write*
 * @see BCDS_Retcode.h for generic return codes
 */
enum NVM_Retcode_E
{
    NVM_RETCODE_INVALID_ITEM = RETCODE_FIRST_CUSTOM_CODE,
    NVM_RETCODE_INVALID_BUFFER,
    NVM_RETCODE_INVALID_SECTION,
    NVM_RETCODE_INVALID_SIZE,
    NVM_RETCODE_WRITE_FORBIDDEN,
    NVM_RETCODE_INTEGRITY_FAILED /**< reserved */
};

enum NVM_Endianness_E
{
    NVM_ENDIANNESS_NONE = 0,
    NVM_ENDIANNESS_BIG,
    NVM_ENDIANNESS_LITTLE
};

enum NVM_Mode_E
{
    NVM_MODE_R,
    NVM_MODE_RW
};

/**
 * @brief meta data description of items
 */
struct NVM_Item_S
{
    enum NVM_Endianness_E endianness;
    enum NVM_Mode_E mode;
    uint8_t section_id;
    uint32_t length_byte;
    uint32_t start_address; /**< absolute start address of item */
};

/**
 * @brief meta data description of sections
 */
struct NVM_Section_S
{
    uint8_t section_id; /**< unique section identifier */
    uint32_t length_byte; /**< length of section in bytes */
    uint32_t start_address; /**< absolute start address of section */
    uint32_t page_length_byte; /**< length of page the section is located in */
};

/**
 * @brief Represents an NVM object
 * @warning any manual modification to an instance of that struct leads to
 * undefined behavior of the NVM
 */
struct NVM_S
{
    struct NVM_Section_S section;
    uint8_t *page_buffer;
    uint32_t page_buffer_size_byte;
};

/**
 * @brief Initializes the NVM for a certain section specified by the
 * user and the given function parameter nvm_obj. NVM_Init() reads the
 * section content of the flash memory and stores it in the NVM
 * internal buffer for later read and write operations.
 *
 * @param[in,out] nvm_obj object instance
 * @return outcome of initialization, see NVM return code
 */
Retcode_T NVM_Init(struct NVM_S *nvm_obj);

/**
 * @brief De-initializes the NVM interface and makes sure all items
 * are written back to flash
 *
 * @param[in] nvm_obj object instance
 * @return outcome of deinit operation
 * @retval RETCODE_NULL_POINTER nvm_obj is a null pointer
 *
 * @warning any further usage of nvm_obj after that function call leads to
 * undefined behavior of the NVM
 */
Retcode_T NVM_DeInit(const struct NVM_S *nvm_obj);

/**
 * @brief Reads an item from NVM. This function only operates on the
 * NVM internal buffer.  In order to store the buffers content to the
 * flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj object instance
 * @param item identifies an item
 * @param[out] value must point to a properly sized buffer
 * @param length length of provided value buffer
 * @return outcome of read operation
 * @retval RETCODE_SUCCESS success
 * @retval RETCODE_NULL_POINTER nvm_obj is a null pointer
 * @retval NVM_RETCODE_INVALID_BUFFER value is a null pointer
 * @retval NVM_RETCODE_INVALID_ITEM item does not exist
 * @retval NVM_RETCODE_INVALID_SECTION item does not belong to section
 * @retval NVM_RETCODE_INVALID_SIZE provided buffer length does not match item
 *                                  length
 * @retval NVM_RETCODE_INTEGRITY_FAILED the item's content is corrupt
 */
Retcode_T NVM_Read(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        void *value, uint32_t length);

/**
 * @brief Writes an item to NVM. This function only operates on the
 * NVM internal buffer. In order to store the buffers content to the
 * flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj object instance
 * @param item identifies an item
 * @param[in] value must point to a properly sized buffer which
 *                  includes data to be written
 * @param[in] length of provided value buffer, amount of bytes to be written
 * @return outcome of write operation
 * @retval RETCODE_SUCCESS success
 * @retval RETCODE_NULL_POINTER nvm_obj is a null pointer
 * @retval NVM_RETCODE_INVALID_BUFFER value is a null pointer
 * @retval NVM_RETCODE_INVALID_ITEM item does not exist
 * @retval NVM_RETCODE_INVALID_SECTION item does not belong to section
 * @retval NVM_RETCODE_INVALID_SIZE provided buffer length does not match item
 *                                  length
 * @retval NVM_RETCODE_WRITE_FORBIDDEN item is read-only
 */
Retcode_T NVM_Write(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const void *value, uint32_t length);

/**
 * @brief Reads an signed int8 item. This function only operates on
 * the NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *            specification
 *
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        int8_t *value);

/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadUInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        uint8_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        int16_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadUInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        uint16_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        int32_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadUInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        uint32_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        int64_t *value);
/**
 * @brief Reads an signed int8 item. This function only operates on
 * an NVM internal buffer. In order to store the buffers content to
 * the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item to be read
 * @param[out] value read value
 *
 * @retval RETCODE_SUCCESS if the item has been read successfully
 */
Retcode_T NVM_ReadUInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        uint64_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const int8_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteUInt8(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const uint8_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const int16_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteUInt16(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const uint16_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const int32_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteUInt32(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const uint32_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const int64_t *value);

/**
 * @brief Writes an unsigned uint8 item to NVM. This function only
 * operates on an NVM internal buffer. In order to store the buffers
 * content to the flash memory use NVM_Flush().
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 * @param[in] item item specification
 * @param[in] value desired value to write
 *
 * @retval RETCODE_SUCCESS if the item has been successfully written
 */
Retcode_T NVM_WriteUInt64(const struct NVM_S *nvm_obj, struct NVM_Item_S item,
        const uint64_t *value);

/**
 * @brief Flush all items from an NVM internal buffer to the flash
 * memory.
 *
 * @param[in] nvm_obj nvm object holding the used page and section
 *                    specification
 */
Retcode_T NVM_Flush(const struct NVM_S *nvm_obj);

#endif /* #if BCDS_FEATURE_NVM */

#endif /* BCDS_NVM_H_ */
