/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BUZZER_H_
#define BCDS_BUZZER_H_

#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_BUZZER

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/** Call back function to intimate the end of repetition of buzzer*/
typedef void (*Buzzer_completeCallBack)(void);

/**
 *  brief Buzzer parameter structure have the members that are used to do the initial configuration and also to specify the
 *  configuration at run time.
 */
struct Buzzer_Param_S
{

    uint8_t volume; /**<The buzzer volume*/
    uint8_t repeatCount; /**<The number of times the buzzer have to be On and Off*/
    uint16_t pitch; /**<The buzzer pitch*/
    uint32_t onTimeInMilliSec; /**<The buzzer ON time - to be specified in millisecond*/
    uint32_t offTimeInMilliSec; /**<The buzzer OFF time - to be specified in millisecond*/
    Buzzer_completeCallBack callback; /**<Call back to intimate the end of repetition*/
};

typedef struct Buzzer_Param_S Buzzer_Param_T;

/**
 * Buzzer state enum to state on/off condition of buzzer.
 */
enum Buzzer_State_E
{
    BUZZER_OFF,
    BUZZER_ON,
};

typedef enum Buzzer_State_E Buzzer_State_T;

/* public global variable declarations */

/* public function prototype declarations */

/**
 *  @brief      Initialize the buzzer module
 *
 *  @param[in]  uint32_t onTimeInMilliSec - time to which buzzer should be On
 *
 *  @warning	Timer is not initialized here, rather it shall be initialized before this API call.
 */
extern void Buzzer_init(void);

/**
 * @brief       Switch off/deactivate a specified buzzer
 */
extern void Buzzer_deactivate(void);

/**
 *  @brief      Switch on the buzzer with specified frequency and mode
 *
 *  @note		Use the configuration set by the Buzzer_setConfig API.
 *
 *  retval		uint8_t - Buzzer active status
 */
extern uint8_t Buzzer_activate(void);

/**
 *  @brief      Configure the buzzer with its required parameters.
 *
 *  @note       To change the configuration the same API have to be called again
 *
 *  @param[in]  Buzzer_Param_T *buzzerParamConfig - Pointer to the buzzer configuration structure
 */
extern void Buzzer_setParameters(Buzzer_Param_T *buzzerParamConfig);

/**
 *  @brief      Get the current working parameter of the buzzer
 *
 *  @param[out] Buzzer_Param_T *buzzerHandle  - Pointer to the buzzer configuration structure
 */
extern void Buzzer_getParameters(Buzzer_Param_T *buzzerHandle);

/**
 * @brief		Set the buzzer volume.
 *
 * @param[in] 	uint8_t volume - duty cycle value
 *
 * @note        100 means - maximum value for volume
 *              0   means - minimum value for volume
 *
 */
extern void Buzzer_setVolume(uint8_t volume);

/**
 * @brief		Set the buzzer pitch.
 *
 * @param[in] 	uint16_t pitch - frequency value
 */
extern void Buzzer_setPitch(uint16_t pitch);

/**
 * @brief		Set the repeat count.
 *
 * @param[in] 	uint8_t repeatCount - count for repetition
 */
extern void Buzzer_setRepeatCount(uint8_t repeatCount);

/**
 * @brief		Set the on time for buzzer.
 *
 * @param[in] 	uint32_t milliseconds - duration for buzzer to be on
 */
extern void Buzzer_activePeriodTime(uint32_t milliseconds);

/**
 * @brief		Set the off time for buzzer.
 *
 * @param[in] 	uint32_t milliseconds - duration for buzzer to be off
 */
extern void Buzzer_deactivePeriodTime(uint32_t milliseconds);

#endif /* #if BCDS_FEATURE_BUZZER */

#endif /* BCDS_BUZZER_H_ */
