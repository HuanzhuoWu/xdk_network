/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/

/**
 * @file Driver for flash W25Q256FV using Spi and Gpio containing initialize,
 * read, write and power methods.
 *
 */

#ifndef BCDS_FLASHW25Q256FV_H_
#define BCDS_FLASHW25Q256FV_H_

/* Include all headers which are needed by this file. */
#include "BCDS_MiscDrivers.h"
#if BCDS_FEATURE_FLASHW25Q256FV

#include "BCDS_Spi.h"
#include "BCDS_Gpio.h"
#include "BCDS_Retcode.h"

/* Put the type and macro definitions here */

/**
 * @brief   Enumeration to represent the Flash retcodes
 */
enum FLASH_W25W256FV_Retcode {
  FLASH_WRITEFAIL = RETCODE_FIRST_CUSTOM_CODE,
  FLASH_READFAIL,
  FLASH_ERASEFAIL,
  FLASH_POWERFAIL
};


#define W25Q256FV_TIMEOUT				    2000
#define FLASH_SPI							BCDS_Spi1
#define FLASH_CS_PORT						GPIO_PORT_G
#define FLASH_CS_PIN						GPIO_PIN_NO_5
#define FLASH_HOLD_PORT                     GPIO_PORT_G
#define FLASH_HOLD_PIN                      GPIO_PIN_NO_1
#define FLASH_WRITE_PROTECT_PORT            GPIO_PORT_G
#define FLASH_WRITE_PROTECT_PIN             GPIO_PIN_NO_9

/**
 * Flash memory characteristics
 */

#define W25Q256FV_DEVICE_ID			    0x18
#define W25Q256FV_MANUFACTURER_ID		0xEF
#define W25Q256FV_DEVICE_TYPE			0x40
#define W25Q256FV_CAPACITY			    0x19

#define W25Q256FV_SECTOR_COUNT		    0x2000
#define W25Q256FV_SECTOR_SIZE			0x1000
#define W25Q256FV_PAGES_PER_SECTOR      16
#define W25Q256FV_ADDRESS_MAX			0x01FFFFFF

/*SPI*/
#define SPI_CS_PIN_LOW()		 			Gpio_write(FLASH_CS_PORT, FLASH_CS_PIN, GPIO_PIN_STATE_LOW)
#define SPI_CS_PIN_HIGH()		 			Gpio_write(FLASH_CS_PORT, FLASH_CS_PIN, GPIO_PIN_STATE_HIGH)

#define SPI_HOLD_PIN_LOW()                  Gpio_write(FLASH_HOLD_PORT, FLASH_HOLD_PIN, GPIO_PIN_STATE_LOW)
#define SPI_HOLD_PIN_HIGH()                 Gpio_write(FLASH_HOLD_PORT, FLASH_HOLD_PIN, GPIO_PIN_STATE_HIGH)

#define SPI_WRITE_PROTECT_PIN_LOW()         Gpio_write(FLASH_WRITE_PROTECT_PORT, FLASH_WRITE_PROTECT_PIN, GPIO_PIN_STATE_LOW)
#define SPI_WRITE_PROTECT_PIN_HIGH()        Gpio_write(FLASH_WRITE_PROTECT_PORT, FLASH_WRITE_PROTECT_PIN, GPIO_PIN_STATE_HIGH)

#define SPI_BIT_STUFFING					0xAB //was 12

/** *******************************************************************************************************************************/
/**
 * SPI_TRANSFERT abstraction
 */
#define SPI_TRANSFERT(output, input)					Spi_Transfer(FLASH_SPI, (const void *)output, input, 1)
#define SPI_SENDT(output)								Spi_Send((Spi_T *)FLASH_SPI, (const void *)output, (uint32_t) 1)

/** *******************************************************************************************************************************/
/**
 * SPI Instructions
 */

#define W25Q256FV_INSTRUCTION_READ_STATUS_REG_1		    0x05
#define W25Q256FV_INSTRUCTION_READ_STATUS_REG_2		    0x35
#define W25Q256FV_INSTRUCTION_READ_STATUS_REG_3		    0x15

#define W25Q256FV_INSTRUCTION_WRITE_STATUS_REG_1		0x01
#define W25Q256FV_INSTRUCTION_WRITE_STATUS_REG_2		0x31
#define W25Q256FV_INSTRUCTION_WRITE_STATUS_REG_3		0x11

#define W25Q256FV_INSTRUCTION_WRITE_ENABLE_VOLATILE	    0x50
#define W25Q256FV_INSTRUCTION_WRITE_DISABLE			    0x04

#define W25Q256FV_INSTRUCTION_SET_BURST_W_WRAP		    0x77

#define W25Q256FV_INSTRUCTION_SECTOR_ERASE			    0x20
#define W25Q256FV_INSTRUCTION_64KB_BLOCK_ERASE		    0x52
#define W25Q256FV_INSTRUCTION_32KB_BLOCK_ERASE		    0xD8
#define W25Q256FV_INSTRUCTION_CHIP_ERASE				0xC7 //or 0x60

#define W25Q256FV_INSTRUCTION_SUSPEND					0x75
#define W25Q256FV_INSTRUCTION_RESUME					0x7A

#define W25Q256FV_INSTRUCTION_READ_DEVICE_ID_1		    0xAB
#define W25Q256FV_INSTRUCTION_READ_DEVICE_ID_2		    0x9F
#define W25Q256FV_INSTRUCTION_READ_SFDP_REG			    0x5A

#define W25Q256FV_INSTRUCTION_READ					    0x13

#define W25Q256FV_INSTRUCTION_WRITE_ENABLE			    0x06
#define W25Q256FV_INSTRUCTION_PAGE_PROGRAM			    0x02

#define W25Q256FV_INSTRUCTION_DEEP_POWER_DOWN			0xB9
#define W25Q256FV_INSTRUCTION_RELEASE_POWER_DOWN		0xAB

#define W25Q256FV_INSTRUCTION_READ_SEC_REG			    0x48
#define W25Q256FV_INSTRUCTION_ERASE_SEC_REG			    0x44
#define W25Q256FV_INSTRUCTION_PROGRAM_SEC_REG			0x42

#define W25Q256FV_INSTRUCTION_SWITCH_TO_4BYTE			0xB7

/** *******************************************************************************************************************************/
/**
 * FLASH Mask Register
 */

/* Status register 1 */
#define MASK_SR1_BUSY										0x01
#define MASK_SR1_WEL										0x02
#define MASK_SR1_BP0										0x04
#define MASK_SR1_BP1										0x08
#define MASK_SR1_BP2										0x10
#define MASK_SR1_BP3										0x20
#define MASK_SR1_TB											0x40
#define MASK_SR1_SRP0										0x80

/* Status register 2 */
#define MASK_SR2_SRP1										0x01
#define MASK_SR2_QE											0x02
#define MASK_SR2_LB1										0x08
#define MASK_SR2_LB2										0x10
#define MASK_SR2_LB3										0x20
#define MASK_SR2_CMP										0x40
#define MASK_SR2_SUS										0x80

/* Status register 3 */
#define MASK_SR3_ADS										0x01
#define MASK_SR3_ADP										0x02
#define MASK_SR3_WPS										0x04
#define MASK_SR3_DRV0										0x20
#define MASK_SR3_DRV1										0x40
#define MASK_SR3_HOLD_RST									0x80

/** *******************************************************************************************************************************/

/* Put the function declarations here */

/**
 *	@brief
 *		Initialize Flash Memory
 */
Retcode_T W25Q256FV_Init(void);

/**
 *	@brief
 *		Return the Flash Memory Busy Status
 * @retval !=0 means busy
 */
uint8_t W25Q256FV_IsBusy(void);

/**
 *	@brief
 *		Read the Flash Memory ID and characteristics and return them.
 *
 *	@param [out] Id
 *	@param [out] Manufacturer
 *	@param [out] MemoryType
 *	@param [out] Capacity
 */
Retcode_T W25Q256FV_ReadDeviceId(uint8_t* Id, uint8_t* Manufacturer, uint8_t* MemoryType, uint8_t* Capacity);

/**
 *	@brief
 *		Read the Flash Memory at the specified Address.
 *
 *	@param [in] Address
 *	@param [out] Buffer
 *	@param [in] Size
 */
Retcode_T W25Q256FV_Read(uint32_t Address, uint8_t* Buffer, uint32_t Size);

/**
 *	@brief
 *		Enable Writing (Necessary before each Write Command).
 */
Retcode_T W25Q256FV_WriteEnable(void);

/**f
 *	@brief
 *		Write the Flash Memory at the specified Address (The address wraps around at the end of a page).
 *
 *	@param [in] Address
 *	@param [in] Buffer
 *	@param [in] Size
 */
Retcode_T W25Q256FV_PageProgram(uint32_t Address, uint8_t* Buffer, uint32_t Size);

/**
 *	@brief
 *		Erase the specified sector.
 *
 *	@param [in] address
 */
Retcode_T W25Q256FV_SectorErase(uint32_t Address);

/**
 *	@brief
 *		Erase the specified block.
 *
 *	@param [in] address
 */
Retcode_T W25Q256FV_BlockErase(uint32_t Address);

/**
 *	@brief
 *		Erase the whole chip.
 */
Retcode_T W25Q256FV_ChipErase(void);

/**
 *	@brief
 *		Command the Flash Memory to enter deep power down state.
 */
Retcode_T W25Q256FV_InitiateDeepPowerDown(void);

/**
 *	@brief
 *		Command the Flash Memory to release the deep power down state.
 */
Retcode_T W25Q256FV_ReleaseDeepPowerDown(void);

/** *******************************************************************************************************************************/

#endif /* #if BCDS_FEATURE_FLASHW25Q256FV */

#endif /* BCDS_FLASHW25Q256FV_H_ */

/**
 * @}
 */
/** ************************************************************************* */
