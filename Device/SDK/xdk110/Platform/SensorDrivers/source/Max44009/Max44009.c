/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* own header files */
#include "BCDS_Max44009.h"
#include "Max44009.h"

/* additional interface header files */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/** structure variable to store the device address,Write, read and Delay functions related information */
static MAX44009_Init_T *MAX44009_ConfigParams_s = (MAX44009_Init_T *) NULL;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* API documentation is in private header file 'Max44009.h' */
static Retcode_T MappingI2cLibError(int8_t i2cLibReturn)
{
    Retcode_T returnValue = (Retcode_T) RETCODE_FAILURE;

    if (I2C_RETURN_SUCCESS == i2cLibReturn)
    {
        returnValue = RETCODE_OK;
    }
    else if (I2C_RETURN_FAIL == i2cLibReturn)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    else
    {
        return (RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) RETCODE_FAILURE));
    }

    return (returnValue);
}

/* API documentation is in private header file 'Max44009.h' */
static uint16_t MAX44009_combineLuxLowHighReg(uint8_t luxHighRegister,
        uint8_t luxLowRegister)
{
    uint16_t sensordata = UINT16_C(0);
    uint16_t temp = UINT16_C(0);
    temp = (luxLowRegister & UINT16_C(0x0F));
    temp |= (luxHighRegister) << MAX44009_SHIFT_BITS_BY_FOUR;
    sensordata = temp;
    return (sensordata);
}

/* global functions ********************************************************* */

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_init(MAX44009_InitPtr_T configParams)
{
    if (NULL == configParams)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* initialize the structure*/
    MAX44009_ConfigParams_s = configParams;
    return (RETCODE_OK);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setCdr(MAX44009_ConfigBrightness_T cdr)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)||(cdr >= MAX44009_OUT_OF_BRIGHTNESS) )
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    apiReturnValue = MAX44009_getConfigurationReg(&configVal);
    /* get the configured value of configuration register */
    if (RETCODE_OK == apiReturnValue)
    {
        /* Masking of the CDR bit of the configuration register */
        configVal = MAX44009_SET_BITSLICE(configVal, MAX44009_CDR,(uint8_t) cdr);

        /* write the configured register value */
        i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
                MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR,
                &configVal, UINT8_C(1));
        apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getCdr(uint8_t * cdr)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s) || (NULL == cdr))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR, cdr,
            UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if(RETCODE_OK == apiReturnValue)
    {
    /* bit slicing the configuration register read value to get the cdr bit value in readable format
     * i.e masking and shifting the bit value to LSB */
    *cdr = (MAX44009_GET_BITSLICE(*cdr, MAX44009_CDR));
    }
    else
    {
        /*I2c Read Failure Case */
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getConfigurationReg(uint8_t *configuredVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s) || (NULL == configuredVal))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* Read the  configuration register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR,
            configuredVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setIntegrationTime(MAX44009_IntegrationTime_T time)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)|| (time >= MAX4409_OUT_OF_TIME))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    apiReturnValue = MAX44009_getConfigurationReg(&configuredVal);
    /* read the previous configured register value*/
    if (RETCODE_OK == apiReturnValue)
    {
        /* replace the TIM[2:0] bit without affecting the remaining bits */
        configuredVal = MAX44009_SET_BITSLICE(configuredVal, MAX44009_TIM, (uint8_t)time);

        /* write the register with the new value */
        i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
                MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR,
                &configuredVal, UINT8_C(1));
        apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    }

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getIntegrationTime(uint8_t* time)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;

    if ((NULL == MAX44009_ConfigParams_s)||(NULL == time))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* get the configured register value */
    apiReturnValue = MAX44009_getConfigurationReg(&configuredVal);

    if (RETCODE_OK == apiReturnValue)
    {
    /* perform bit operations , TIM[2:0] to readable format */
    configuredVal = MAX44009_GET_BITSLICE(configuredVal, MAX44009_TIM);

    *time = (uint8_t) configuredVal;
    }

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getSensorData(uint16_t *sensorData)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t luxData[2] = { UINT8_C(0) };
    uint16_t sensorOutput;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == sensorData))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* read high byte of the sensor data */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_LUX_HIGHBYT_REG_ADR,
            &luxData[0], UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if (RETCODE_OK == apiReturnValue)
    {
        /* read low byte of the sensor data */
        i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
                MAX44009_ConfigParams_s->dev_add,
                MAX44009_LUX_LOWBYT_REG_ADR, &luxData[1], UINT8_C(1));
        apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    }
    if (RETCODE_OK == apiReturnValue)
    {
    sensorOutput = MAX44009_combineLuxLowHighReg(luxData[0], luxData[1]);
    *sensorData = sensorOutput;
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
uint32_t MAX44009_getDataInMilliLux(uint16_t sensorData)
{
    uint32_t milliLux = UINT32_C(0);
    uint8_t exponent = UINT8_C(0);
    uint16_t mantissa = UINT16_C(0);
    uint8_t temp = UINT8_C(0);
    exponent = (sensorData & UINT16_C(0x0F00)) >> MAX44009_SHIFT_BITS_BY_EIGHT; /** Extracts the exponent value */
    mantissa = (sensorData & UINT16_C(0x00F0)); /** higher four bits of mantissa */
    temp = (sensorData & UINT16_C(0x000F)); /** lower four bits of mantissa */
    mantissa += temp; /** calculates the total mantissa value */
    milliLux = mantissa * (UINT8_C(1) << exponent) * MAX44009_LUX_PER_COUNT;
    return (milliLux);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setUpperThreshold(uint8_t thresholdVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;
    if (NULL == MAX44009_ConfigParams_s)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* read the Upper threshold register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_UPPERTHRESHOLD_REG_ADR,
            &thresholdVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setLowerThreshold(uint8_t thresholdVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;
    if (NULL == MAX44009_ConfigParams_s)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* write the desired lower threshold value in to the register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_LOWERTHRESHOLD_REG_ADR,
            &thresholdVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getUpperThreshold(uint8_t *thresholdVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == thresholdVal))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* read the register value */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_UPPERTHRESHOLD_REG_ADR,
            thresholdVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getLowerThreshold(uint8_t *thresholdVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;
    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == thresholdVal))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_LOWERTHRESHOLD_REG_ADR,
            thresholdVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setThresholdTimer(uint8_t thresholdTimerVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;
    if (NULL == MAX44009_ConfigParams_s)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* write to the register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_THRESHOLDTIMER_REG_ADR,
            &thresholdTimerVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getThresholdTimer(uint8_t * thresholdTimerVal)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == thresholdTimerVal))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* read the register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_THRESHOLDTIMER_REG_ADR,
            thresholdTimerVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setContinousModeReg(MAX44009_ConfigMode_T configParam)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if ((configParam != MAX44009_ENABLE_CONTINOUS_MODE) && (configParam != MAX44009_DISABLE_CONTINOUS_MODE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    apiReturnValue = MAX44009_getConfigurationReg(&configuredVal);
    /* read the configuration registers configured value */
    if (RETCODE_OK == apiReturnValue)
    {
        /* modify the Continous mode bit [7] value without changing the remaining bits */
        configuredVal = MAX44009_SET_BITSLICE(configuredVal, MAX44009_CONT_MODE,
                (uint8_t)configParam);

        /* write the register with the new value */
        i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
                MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR,
                &configuredVal, UINT8_C(1));
        apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);

}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getContinousModeReg(uint8_t* configParams)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == configParams))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    apiReturnValue = MAX44009_getConfigurationReg(&configuredVal);
    /* read the configuration register's configured value*/
    if (RETCODE_OK == apiReturnValue)
    {
        /* shift the CONT[7] bit to zeroth position for readability */
        configuredVal = MAX44009_GET_BITSLICE(configuredVal, MAX44009_CONT_MODE);

        *configParams = (uint8_t) configuredVal;
    }

    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setManualModeReg(MAX44009_ConfigMode_T configParam)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t prevConfigVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if((configParam != MAX44009_ENABLE_MANUAL_MODE) && (configParam != MAX44009_DISABLE_MANUAL_MODE))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    apiReturnValue = MAX44009_getConfigurationReg(&prevConfigVal);
    /* read the configuration register's configured value*/
    if (RETCODE_OK == apiReturnValue)
    {
        /* modify the MANUAL[6] bit without changing the remaining bits of the register */
        prevConfigVal = MAX44009_SET_BITSLICE(prevConfigVal, MAX44009_MANUAL_MODE,
                (uint8_t)configParam);

        /* update the new value to the  register*/
        i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
                MAX44009_ConfigParams_s->dev_add, MAX44009_CONFIGURATION_REG_ADDR,
                &prevConfigVal, UINT8_C(1));
        apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);

}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getManualModeReg(uint8_t* configParams)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == configParams))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    apiReturnValue = MAX44009_getConfigurationReg(&configuredVal);
    /* read the register */
    if (RETCODE_OK == apiReturnValue)
    {

        /* shift the MANUAL[6] bit to the zeroth position for readability*/
        configuredVal = MAX44009_GET_BITSLICE(configuredVal, MAX44009_MANUAL_MODE);

        *configParams = configuredVal;
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getInterptStatus(uint8_t* status)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s)|| (NULL == status))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* read the interrupt status register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_INTRPTSTATUS_REG_ADDR,
            &configuredVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if (RETCODE_OK == apiReturnValue)
    {
        *status = configuredVal;
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setIntrptReg(MAX44009_ConfigMode_T configParam)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if((configParam != MAX44009_ENABLE_INTERRUPT) && (configParam != MAX44009_DISABLE_INTERRUPT))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* write to the register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_INTRPTENABLE_REG_ADDR,
            (uint8_t *) &configParam, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getIntrptReg(MAX44009_ConfigModePtr_T configParam)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    uint8_t configuredVal = MAX44009_DEFAULT_VAL;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if (NULL == MAX44009_ConfigParams_s)
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }
    /* read the interrupt register value */
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, MAX44009_INTRPTENABLE_REG_ADDR,
            &configuredVal, UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if (RETCODE_OK == apiReturnValue)
    {
        *configParam = (MAX44009_ConfigMode_T) configuredVal;
    }
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }

    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_getRegister(MAX44009_Registers_T regAddress, uint8_t * value)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s) || (NULL == value))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    if ((regAddress >= MAX44009_THRESHOLD_UNSUPPORTED))
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* read the interrupt register value*/
    i2cLibReturnValue = MAX44009_ConfigParams_s->readFunc(
            MAX44009_ConfigParams_s->dev_add, (uint8_t) regAddress, (uint8_t *) value,
            UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
Retcode_T MAX44009_setRegister(MAX44009_Registers_T regAddress, uint8_t value)
{
    Retcode_T apiReturnValue = (Retcode_T) RETCODE_FAILURE;
    int8_t i2cLibReturnValue = I2C_RETURN_FAIL;

    if ((NULL == MAX44009_ConfigParams_s) || (regAddress > MAX44009_THRESHOLD_TIMER_REG) )
    {
        return (RETCODE(RETCODE_SEVERITY_FATAL, (uint32_t) RETCODE_INVALID_PARAM));
    }

    /* write to the register */
    i2cLibReturnValue = MAX44009_ConfigParams_s->writeFunc(
            MAX44009_ConfigParams_s->dev_add, (uint8_t) regAddress, (uint8_t *) &value,
            UINT8_C(1));
    apiReturnValue = MappingI2cLibError(i2cLibReturnValue);
    if ((RETCODE_OK != apiReturnValue) && (Retcode_getPackage(apiReturnValue) == PACKAGE_ID_DEFAULT))
    {
        apiReturnValue = RETCODE(RETCODE_SEVERITY_ERROR, (uint32_t) apiReturnValue);
    }
    return (apiReturnValue);
}

/* API documentation is in public interface header file 'BCDS_Max44009.h' */
void MAX44009_deInit(void)
{
    MAX44009_ConfigParams_s = NULL;
}

/** ************************************************************************* */
