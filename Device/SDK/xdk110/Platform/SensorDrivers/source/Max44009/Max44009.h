/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef MAX44009_H_
#define MAX44009_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

#define MAX44009_INTRPTSTATUS_REG_ADDR                UINT8_C(0x00)               /**< Interrupt status register */
#define MAX44009_INTRPTENABLE_REG_ADDR                UINT8_C(0x01)               /**< Interrupt enable register address */
#define MAX44009_CONFIGURATION_REG_ADDR               UINT8_C(0X02)               /**< Configuration register address */
#define MAX44009_LUX_HIGHBYT_REG_ADR                  UINT8_C(0x03)               /**< LUX high byte register address */
#define MAX44009_LUX_LOWBYT_REG_ADR                   UINT8_C(0x04)               /**< LUX low byte register address */
#define MAX44009_UPPERTHRESHOLD_REG_ADR               UINT8_C(0x05)               /**< Upper threshold register address */
#define MAX44009_LOWERTHRESHOLD_REG_ADR               UINT8_C(0x06)               /**< Lower threshold register register address */
#define MAX44009_THRESHOLDTIMER_REG_ADR               UINT8_C(0x07)               /**< Threshold timer register address */
#define MAX44009_SHIFT_BITS_BY_FOUR                   UINT8_C(4)                  /**< macro used to shift the bits */
#define MAX44009_SHIFT_BITS_BY_EIGHT                  UINT8_C(8)                  /**< macro used to shift the bits */
#define MAX44009_LUX_PER_COUNT                        UINT8_C(45)                 /**< LUX value per count*/
#define PACKAGE_ID_DEFAULT                UINT32_C(0)/**< default package ID*/

/** INT_ENABLE */
#define MAX44009_INTENABLE_POS                       UINT8_C(0)                  /**< macro represents the CONT bit number of configuration register */
#define MAX44009_INTENABLE_MSK                       UINT8_C(0x01)               /**< macro represents the mask value of Interrupt */
#define MAX44009_INTENABLE_LEN                       UINT8_C(1)                  /**< macro represents the no of bits related to Interrupt in configuration register */
#define MAX44009_INTENABLE_REG                       MAX44009_INTRPTENABLE_REG_ADDR   /**< register address */

/** CONT MODE */
#define MAX44009_CONT_MODE_POS                       UINT8_C(7)                  /**< macro represents the CONT bit number of configuration register */
#define MAX44009_CONT_MODE_MSK                       UINT8_C(0x80)               /**< macro represents the mask value for CONT */
#define MAX44009_CONT_MODE_LEN                       UINT8_C(1)                  /**< macro represents the no of bits related to CONT in configuration register */
#define MAX44009_CONT_MODE_REG                       MAX44009_CONFIGURATION_REG_ADDR  /**< register address */

/** MANUAL_MODE */
#define MAX44009_MANUAL_MODE_POS                     UINT8_C(6)                  /**< macro represents the MANUAL bit number of configuration register */
#define MAX44009_MANUAL_MODE_MSK                     UINT8_C(0x40)               /**< macro represents the mask value for MANUAL */
#define MAX44009_MANUAL_MODE_LEN                     UINT8_C(1)                  /**< macro represents the no of bits related to MANUAL in configuration register */
#define MAX44009_MANUAL_MODE_REG                     MAX44009_CONFIGURATION_REG_ADDR  /**< register address */

/** CDR */
#define MAX44009_CDR_POS                             UINT8_C(3)                  /**< macro represents the CDR bit number of configuration register */
#define MAX44009_CDR_MSK                             UINT8_C(0x08)               /**< macro represents the mask value for CDR */
#define MAX44009_CDR_LEN                             UINT8_C(1)                  /**< macro represents the no of bits related to CDR in configuration register */
#define MAX44009_CDR_REG                             MAX44009_CONFIGURATION_REG_ADDR  /**< register address */

/** TIM */
#define MAX44009_TIM_POS                             UINT8_C(0)                  /**< macro represents the TIM bit number of configuration register */
#define MAX44009_TIM_MSK                             UINT8_C(0x07)               /**< macro represents the mask value for TIM */
#define MAX44009_TIM_LEN                             UINT8_C(3)                  /**< macro represents the no of bits related to Timerintegration in configuration register */
#define MAX44009_TIM_REG                             MAX44009_CONFIGURATION_REG_ADDR  /**< register address */

#define MAX44009_DEFAULT_VAL                          UINT8_C(0)                  /**< This macro represents default zero value for variable to get data from MAX44009 Sensor*/

/** I2C Lib Error Mapping Conditions**/
#define I2C_RETURN_SUCCESS              INT8_C(0)               /**< I2C API call success */
#define I2C_RETURN_FAIL                 INT8_C(-1)              /**< Max09 API call failed */
/** MACROS DEFINTION for register bit slicing, read access */
#define MAX44009_GET_BITSLICE(regvar, bitname)\
                        (regvar & bitname##_MSK) >> bitname##_POS

/** MACROS DEFINTION for register bit slicing, write access */
#define MAX44009_SET_BITSLICE(regvar, bitname, val)\
                  (regvar & ~bitname##_MSK) | ((val<<bitname##_POS)&bitname##_MSK)

/* local function prototype declarations */

/**
 * @brief       This function maps error codes returned from i2c library to retcode values.
 *
 * @param [in]   i2cLibReturn Return value from i2c library
 *
 * @retval      RETCODE_OK              max44009 sensor API call success
 * @retval      RETCODE_FAILURE         max44009 sensor API call failed
 * @retval      RETCODE_INVALID_PARAM   max44009 sensor API call failed because of invalid input parameter
 */

static Retcode_T MappingI2cLibError(int8_t i2cLibReturn);

/**
 * @brief  The function to give the raw lux data in a 16 bit format
 *
 * @param [in] luxHighRegister High Register value read from MAX44009 Sensor
 *
 * @param [in] luxLowRegister Low Register value read from MAX44009 Sensor
 *
 * @returns uint16_t   raw lux data in a 16 bit format
 */
static uint16_t MAX44009_combineLuxLowHighReg(uint8_t luxHighRegister, uint8_t luxLowRegister);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* MAX44009_H_ */

/** ************************************************************************* */
