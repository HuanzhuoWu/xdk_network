#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# The package name. The library name is derived from this package name
BCDS_BLE_PACKAGE_NAME = BLE
BCDS_BLE_HOME = $(CURDIR)
BCDS_DEVICE_TYPE ?=EFM32LG
BCDS_DEVICE_ID ?=EFM32LG990F256
BCDS_PACKAGE_ID = 13

BCDS_TOOLS_DIR ?= $(BCDS_BLE_HOME)/../../Tools
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin

#Path where the LINT config files are present(compiler and environment configurations)
BCDS_LINT_CONFIG_PATH = $(BCDS_TOOLS_DIR)/PClint/config
BCDS_LINT_CONFIG_FILE := $(BCDS_LINT_CONFIG_PATH)/bcds.lnt
LINT_EXE = $(BCDS_TOOLS_DIR)/pclint/exe/lint-nt.launch

BCDS_UNIQUE_MAC ?= 0xFCD6BD090000


# Internal Path Settings
BCDS_BLE_INCLUDE_DIR := include
BCDS_BLE_SOURCE_DIR := source
BCDS_BLE_PROTECTED_DIR := source/protected
BCDS_BLE_CONFIG_DIR := config
BCDS_BLE_OBJECT_DIR:= objects
BCDS_BLE_LINT_DIR := lint

#Path info for taking the configuration files
ifdef BCDS_PROJECT_SPECIFIC_CONFIG
BCDS_BLE_DEFAULT_CONFIG_PATH := $(BCDS_PROJECT_SPECIFIC_CONFIG)
BCDS_BLE_HARDWARE_CONFIG_PATH := $(BCDS_PROJECT_SPECIFIC_CONFIG)/$(BCDS_HW_VERSION)
else
BCDS_BLE_DEFAULT_CONFIG_PATH := $(BCDS_BLE_CONFIG_DIR)
endif

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Path of other libraries and platform
BCDS_LIBRARIES_DIR = $(BCDS_BLE_HOME)/../../Libraries
BCDS_PLATFORM_DIR =  $(BCDS_BLE_HOME)/../../Platform

BCDS_EM_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_OS_LIB_DIR ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS

BCDS_PERIPHERALS_DIR ?= $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_UTILS_DIR ?= $(BCDS_PLATFORM_DIR)/Utils
BCDS_BASICS_DIR ?= $(BCDS_PLATFORM_DIR)/Basics
BCDS_BLE_DIR ?= $(BCDS_LIBRARIES_DIR)/BLEStack

#Source Paths
BCDS_BLE_SOURCE_PATH = $(BCDS_BLE_SOURCE_DIR)

#Path of the generated static library and its object files
BCDS_BLE_DEBUG_PATH ?= $(BCDS_BLE_HOME)/debug
BCDS_BLE_RELEASE_PATH ?= $(BCDS_BLE_HOME)/release
BCDS_BLE_DEBUG_OBJECT_PATH ?= $(BCDS_BLE_DEBUG_PATH)/$(BCDS_BLE_OBJECT_DIR)
BCDS_BLE_RELEASE_OBJECT_PATH ?= $(BCDS_BLE_RELEASE_PATH)/$(BCDS_BLE_OBJECT_DIR)

#Lint Path
BCDS_BLE_DEBUG_LINT_PATH = $(BCDS_BLE_DEBUG_PATH)/$(BCDS_BLE_LINT_DIR)

BCDS_LIB_BLE_BASE_NAME ?= lib$(BCDS_BLE_PACKAGE_NAME)
BCDS_BLE_DEBUG_LIB_NAME = $(BCDS_LIB_BLE_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_BLE_RELEASE_LIB_NAME = $(BCDS_LIB_BLE_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a
BCDS_BLE_DEBUG_LIB   = $(BCDS_BLE_DEBUG_PATH)/$(BCDS_BLE_DEBUG_LIB_NAME)
BCDS_BLE_RELEASE_LIB = $(BCDS_BLE_RELEASE_PATH)/$(BCDS_BLE_RELEASE_LIB_NAME)

# Define the path for the include directories
BCDS_BLE_INCLUDES = \
 				  -I$(BCDS_EM_LIB_DIR)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
                  -I$(BCDS_BLE_INCLUDE_DIR)\
                  -I$(BCDS_BLE_PROTECTED_DIR) \
                  -I$(BCDS_BLE_DEFAULT_CONFIG_PATH) \
                  -I$(BCDS_PERIPHERALS_DIR)/include \
                  -I$(BCDS_PERIPHERALS_DIR)/config \
                  -I$(BCDS_OS_DIR)/config \
                  -I$(BCDS_OS_LIB_DIR)/source/include \
                  -I$(BCDS_OS_LIB_DIR)/source/portable/ARM_CM3 \
                  -I$(BCDS_EM_LIB_DIR)/emlib/inc \
                  -I$(BCDS_EM_LIB_DIR)/usb/inc \
                  -I$(BCDS_EM_LIB_DIR)/CMSIS/Include \
                  -I$(BCDS_UTILS_DIR)/include \
                  -I$(BCDS_BASICS_DIR)/include \
                  -I$(BCDS_UTILS_DIR)/config \
                  -I$(BCDS_BST_DIR)/include \
                  -I$(BCDS_BLE_DIR)/Alpwise/Core_Stack/ATT \
                  -I$(BCDS_BLE_DIR)/Alpwise/Core_Stack/Interfaces \
                  -I$(BCDS_BLE_DIR)/Alpwise/Core_Stack/Interfaces/Services \
                  -I$(BCDS_BLE_DIR)/Alpwise/Service/Data_Exchange_Service/Interfaces \
                  -I$(BCDS_BLE_DIR)/Alpwise/Core_Stack
                  
#The inclusion of the hardware specific config files is required only when the
#compilation is done for project specific configuration
ifdef BCDS_PROJECT_SPECIFIC_CONFIG
BCDS_BLE_INCLUDES += \
	-I $(BCDS_BLE_HARDWARE_CONFIG_PATH)
endif

# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	-DDEBUG -D DBG_ASSERT_FILENAME=\"$*.c\"
	

#LINT variables



BCDS_EXTERNAL_EXCLUDES_LINT := $(foreach DIR, $(subst -isystem , ,$(BCDS_EXTERNAL_INCLUDES)), +libdir\($(DIR)\))
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -isystem ,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_EXTERNAL_INCLUDES_LINT += -i$(BCDS_LINT_CONFIG_PATH) 

BCDS_GSM_INCLUDES_LINT := $(subst -I ,-i,$(BCDS_GSM_INCLUDES))	