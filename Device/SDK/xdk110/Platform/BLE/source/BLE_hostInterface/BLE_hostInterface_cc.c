/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>

/* additional interface header files */
#include "BleTypes.h"
#include "BleTransport.h"
#include "BLE_serialDriver_ih.h"

/* own header files */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */
/**
 * @brief   Initialize the UART or physical layer.
 *			This function is called by the BLE stack (TRANSPORT layer) to initialise
 *			the UART layer.The user should uses this function to open and setup parameters for the
 *			UART line, eventually create Read Thread or setup RX and TX interrupts
 *	        When this function succeeds, the UART layer shall be fully functional
 *@note     This function is called during the BLESTCK_Init() process, failure here
 *	        will issue a failure in BLESTCK_Init()
 *
 *@retval  The status of the operation:
 *			- BLESTATUS_SUCCESS indicates to the BLE stack that the UART have been
 *		      successfully initialized
 *			- BLESTATUS_FAILED indicates to the BLE stack that the UART could not be
 *			  initialized
 */
extern BleStatus BLEUART_Init(void) {
	BleStatus BLE_hciInitStatus;
	/* Initialize the HCI-driver or physical layer */
	BLE_hciInitStatus = BLE_hciDriverInit();
	return (BLE_hciInitStatus);

}

/**
 * @brief	 De-Initialize the UART or physical layer.
 *	   	 	 This function is called by the BLE stack (TRANSPORT layer) to de-initialise
 *			 the UART layer. Eventual thread shall be terminated here.
 *		 	 When this function succeed, the UART layer shall be fully de-initialised
 * @note	 This function is called during the BLESTCK_Deinit() process, failure here
 *			 will issue a failure in BLESTCK_Deinit()
 *
 * @retval   The status of the operation:
 *				- BLESTATUS_SUCCESS indicates to the BLE stack that the UART have been
 *				  successfully initialized
 *				- BLESTATUS_FAILED indicates to the BLE stack that the UART could not be
 *				  initialized
 */
extern BleStatus BLEUART_Deinit(void) {
	/* De-Initialize the HCI-driver or physical layer */
	BLE_hciDriverDeinit();

	return (BLESTATUS_SUCCESS);
}

/**
 * @brief	Send data through Uart or physical layer.
 *			This function is called by the BLE stack (TRANSPORT layer) to send data
 *			through the UART layer.
 *			When the function succeed, the UART layer shall have send all the data
 *			It is to the uart layer to retry to sent the data, if the function fails
 *  		it result to a transport error and stack de-initialisation.
 *
 * @note    BLEUART_Send is always called after BLEUART_TXStarts() and
 * 			before BLEUART_TXEnds().
 *
 * @param[in] data : pointer to received data
 * @param[in] dataLen : length of the received data contained in the data pointer
 *
 * @retval 	The status of the operation:
 *			- BLESTATUS_SUCCESS indicates to the BLE stack that the data have been
 *			  successfully sent
 *			- BLESTATUS_FAILED indicates to the BLE stack that the data have not
 *		      been sent, it result to a transport error
 */
extern BleStatus BLEUART_Send(uint8_t* uartData, uint8_t uartDataLen) {
	BleStatus BLE_hciSendStatus;
	/* Write data to send on physical layer */
	BLE_hciSendStatus = BLE_hciSendData(uartData, uartDataLen);

	return (BLE_hciSendStatus);
}

/**
 * @brief    This function is called by the BLE stack (TRANSPORT layer) to inform
 *			 the UART layer that a packet will be transmitted.
 *			 As a packet can be transmitted into several BLEUART_Send() calls, the
 *           stack  inform the UART layer that a transmittion starts. Then the stack
 *           will call one or several BLEUART_Send() and then  BLEUART_TXEnds API.
 *           it is usefull for UARt line that are not full duplex or with a specific
 *           flow control and requiere to know when transmition starts.
 *        	 Otherwise the function should be empty.
 * @note
 *           BLEUART_Send is always called after BLEUART_TXStarts() and
 *           before BLEUART_TXEnds().
 */
extern void BLEUART_TXStarts(void) {
}

/**
 * @brief   This function is called by the BLE stack (TRANSPORT layer) to inform
 *	        the UART layer that a packet has been transmitted.
 *	        As a packet can be transmitted into several BLEUART_Send() calls, the
 *          stack  inform the UART layer that a transmittion ends.
 *          it is useful for UART line that are not full duplex or with a specific
 *          flow control and require to know when transmition ends.
 * 	        Otherwise the function should be empty.
 *
 * @note    BLEUART_Send is always called after BLEUART_TXStarts() and
 *          before BLEUART_TXEnds().
 */
extern void BLEUART_TXEnds(void) {
}

/** ************************************************************************* */
