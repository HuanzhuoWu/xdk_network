/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* additional interface header files */
#include "BleTypes.h"
#include "TLV_dataHandler_ih.h"

/* own header files */
#include "BleMemory.h"

/* constant definitions ***************************************************** */

#define DEVICE_LIST_SIZE 6
#define member_size(type, member) sizeof(((type *)0)->member)

typedef struct BLE_memoryDescriptor_s
{
    uint8_t buffer[72];
    TLV_groupHandle_t handle;
    BD_ADDR owner;
} BLE_memoryDescriptor_t;

BLE_memoryDescriptor_t BLE_deviceData[DEVICE_LIST_SIZE];

typedef struct BLE_memoryTableDescriptor_s
{
    BLE_memoryDescriptor_tp buffer;
    uint8_t limit;
    uint8_t elements;
    uint8_t oldestElement;
} BLE_memoryTableDescriptor_t;

BLE_memoryTableDescriptor_t BLE_deviceTable = { (BLE_memoryDescriptor_tp) &BLE_deviceData, DEVICE_LIST_SIZE, 0, 0 };

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

static inline uint8_t BLE_getDeviceSlot(BLE_memoryTableDescriptor_tp handle)
{
    return (handle->oldestElement);
}

/* local functions ********************************************************** */

/* global functions ********************************************************* */

extern uint32_t BLE_isBdAddressValid(BD_ADDR *addr)
{
    return (!((addr->addr[0] == 0) &&
        (addr->addr[1] == 0) &&
        (addr->addr[2] == 0) &&
        (addr->addr[3] == 0) &&
        (addr->addr[4] == 0) &&
        (addr->addr[5] == 0)));
}

extern BLE_memoryTableDescriptor_tp BLE_memoryInit(void)
{
    return ((BLE_memoryTableDescriptor_tp) &BLE_deviceTable);
}

extern BLE_memoryDescriptor_tp BLE_getDeviceHandleByAddress(BLE_memoryTableDescriptor_tp handle, BD_ADDR *addr)
{
    BLE_memoryDescriptor_tp device = (BLE_memoryDescriptor_tp) NULL;
    uint8_t index;

    for (index = 0; index < handle->elements; index++)
    {
        if (!memcmp(handle->buffer[index].owner.addr, addr->addr, member_size(BD_ADDR, addr)))
        {
            device = (BLE_memoryDescriptor_tp) &handle->buffer[index];
            break;
        }
    }

    return (device);
}

extern BLE_memoryDescriptor_tp BLE_getDeviceHandleByIndex(BLE_memoryTableDescriptor_tp handle, uint8_t index)
{
    BLE_memoryDescriptor_tp device;

    if (handle->elements > index)
    {
        device = (BLE_memoryDescriptor_tp) &handle->buffer[index];
    }
    else
    {
        device = (BLE_memoryDescriptor_tp) NULL;
    }

    return (device);
}

extern BLE_memoryDescriptor_tp BLE_addDevice(BLE_memoryTableDescriptor_tp handle, BD_ADDR *addr)
{
    BLE_memoryDescriptor_tp device;

    device = BLE_getDeviceHandleByAddress(handle, addr);

    if (device)
    {
        /* device already exists in database, nothing to do */
    }
    else
    {
        /* add new device - potentially overwriting an older device entry */
        device = (BLE_memoryDescriptor_tp) &handle->buffer[BLE_getDeviceSlot(handle)];

        /* clean up previous TLV instance */
        if (device->handle)
        {
            TLV_removeGroup(device->handle);
        }

        /* set new device address */
        memcpy(device->owner.addr, addr->addr, member_size(BD_ADDR, addr));

        /* initialize TLV handle */
        device->handle = (TLV_groupHandle_t) device->buffer;

        /* create new TLV database */
        device->handle = TLV_addGroup(device->buffer, member_size(BLE_memoryDescriptor_t, buffer));

        if (!device->handle)
        {
            device = (BLE_memoryDescriptor_tp) NULL;
        }
        else
        {
            handle->elements < handle->limit ? handle->elements++ : handle->elements;
            handle->oldestElement = (handle->oldestElement + 1) % handle->limit;
        }
    }

    return (device);
}

extern TLV_groupHandle_t BLE_getDeviceTlvDatabase(BLE_memoryDescriptor_tp handle)
{
    return (handle->handle);
}

extern uint8_t BLE_getDeviceDatabaseSize(BLE_memoryTableDescriptor_tp handle)
{
    return (handle ? handle->elements : 0);
}

/** ************************************************************************* */
