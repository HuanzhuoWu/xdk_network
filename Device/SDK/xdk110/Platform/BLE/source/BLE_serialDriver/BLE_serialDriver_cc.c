/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/* module includes ********************************************************** */

/* system header files */
#include <string.h>

/* additional interface header files */
#include "em_cmu.h"
#include "BleTransport.h"
#include "BLE_ph.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "DBG_assert_ih.h"
#include "SER_serialDriver_ih.h"

/* own header files */
#include "BLE_serialDriver_ih.h"
#include "BLE_serialDriver_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/** flag for BLE data transmission check */
uint8_t BLE_dataSentFlag_gdu = BLE_CLEAR;

/** creating the serial Instance for future reference */
static SER_device_t BLE_uartHandle;

/** Serial transmit buffer for ringbuffer */
static uint8_t BLE_ser2TxBuf_ma[BLE_TX_BUFFER_SIZE];

/** Serial receive buffer for ringbuffer */
static uint8_t BLE_ser2RxBuf_ma[BLE_RX_BUFFER_SIZE];

/** Initailising serial init parameters */
static SER_init_t BLE_uartInit =
    {
        .hwType = BLE_SERIAL_HWTYPE,
        .hwDevicePort = BLE_UARTPORT,
        .protocol = SER_UART_PROTOCOL,
        .baudRate = BLE_UARTBAUDRATE,
        .hasHwFlowControl = false,
        .txBuf_p = BLE_ser2TxBuf_ma,
        .txBufSize = BLE_TX_BUFFER_SIZE,
        .rxBuf_p = BLE_ser2RxBuf_ma,
        .rxBufSize = BLE_RX_BUFFER_SIZE,
        .parity = SER_PARITY_NONE,
        .dataBits = SER_DATABITS_8,
        .stopBits = SER_STOPBITS_ONE,
        .routeLocation = BLE_UART_ROUTE_LOCATION,
        .txCallback = NULL,
        .rxCallback = NULL,
        .rtsPort = BLE_CLEAR,
        .rtsPin = BLE_CLEAR,
        .rtspolarity = SER_activeLow,
        .ctsPort = BLE_CLEAR,
        .ctsPin = BLE_CLEAR,
        .ctspolarity = SER_activeLow };
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 *  @brief  This function Initialize the Bluetooth LE port pin configure
 */
static void bleHciPtdConfig(void)
{

    /* configure the Bluetooth Low Energy WAKEUP pin as input */
    PTD_driveModeSet(PTD_PORT_BTLE_WAKEUP, PTD_DRIVE_MODE_BTLE_WAKEUP);

    /* configure the Bluetooth Low Energy WAKEUP pin as input */
    PTD_pinModeSet(PTD_PORT_BTLE_WAKEUP, PTD_PIN_BTLE_WAKEUP,
        (GPIO_Mode_TypeDef) PTD_MODE_BTLE_WAKEUP, PTD_DOUT_BTLE_WAKEUP);

    /* configure the Bluetooth Low Energy RX pin as input */
    PTD_driveModeSet(PTD_PORT_BTLE_RX, PTD_DRIVE_MODE_BTLE_RX);

    /* configure the Bluetooth Low Energy RX pin as input */
    PTD_pinModeSet(PTD_PORT_BTLE_RX, PTD_PIN_BTLE_RX,
        (GPIO_Mode_TypeDef) PTD_MODE_BTLE_RX, PTD_DOUT_BTLE_RX);

    /* configure the Bluetooth Low Energy TX pin as input */
    PTD_driveModeSet(PTD_PORT_BTLE_TX, PTD_DRIVE_MODE_BTLE_TX);

    /* configure the Bluetooth Low Energy TX pin as input */
    PTD_pinModeSet(PTD_PORT_BTLE_TX, PTD_PIN_BTLE_TX,
        (GPIO_Mode_TypeDef) PTD_MODE_BTLE_TX, PTD_DOUT_BTLE_TX);

    /* configure the Bluetooth Low Energy WAKEUP pin as input */
    PTD_driveModeSet(PTD_PORT_BTLE_RESET, PTD_DRIVE_MODE_BTLE_RESET);

    /* configure the Bluetooth Low Energy WAKEUP pin as input */
    PTD_pinModeSet(PTD_PORT_BTLE_RESET, PTD_PIN_BTLE_RESET,
        (GPIO_Mode_TypeDef) PTD_MODE_BTLE_RESET, PTD_DOUT_BTLE_RESET);
}

/* global functions ********************************************************** */

/**
 * @brief   This function is called by the BLE stack to Initialize the HCI layer and This function is called
 *           internally during the BLEMGMT_init() process, failure here will issue a failure in BLEMGMT_Init().
 * @retval   BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciDriverInit(void)
{
    /* Enable the USART clock */
    CMU_ClockEnable(BLE_SERIAL_CLOCK, true);

    /* BLE pin configure initialize */
    bleHciPtdConfig();

    /* Serial driver initialize */
    if (SER_serialInit((SER_device_t*) &BLE_uartHandle,
        (SER_init_t*) &BLE_uartInit) == SER_SUCCESS)
    {
        return (BLE_STATUS_SUCCESS);
    }
    else
    {
        return (BLE_STATUS_FAILED);
    }
}

/**
 * @brief      This function is called by the BLE stack to De-Initialize the HCI layer (TRANSPORT layer).
 *              Eventual thread shall be terminated here.
 */
extern void BLE_hciDriverDeinit(void)
{
    /* Disable the USART clock */
    CMU_ClockEnable(BLE_SERIAL_CLOCK, false);

    /* Disabling the serial driver */
    SER_serialDisable((SER_serialPort_t) BLE_UARTPORT,
        (SER_hwType_t) BLE_SERIAL_HWTYPE);
}

/** This function used to check any Event/message received from BLE centralized device.
 *  @brief    This function used to check any Event/message received from BLE.if it's received then,
 *            read BLE Event/message from serial ring buffer and then send to the CoreStack
 *  @retval   BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciReceiveData(void)
{
    static uint8_t bleReceiveBuffer[BLE_RX_BUFFER_SIZE];
    SER_errorCode_t retVal = SER_SUCCESS;
    uint32_t reminingBytes;
    uint8_t receiveByteLen = BLE_SET;
    BLE_return_t status;

    /* Reads the required number of bytes from internal buffer */
    retVal = SER_serialRead(&BLE_uartHandle, &reminingBytes, bleReceiveBuffer,
        receiveByteLen);

    if (retVal == SER_SUCCESS)
    {
        /* Sending data to the chip */
        status = (BLE_return_t) BLETRANSPORT_UartDataReceived(bleReceiveBuffer,
            receiveByteLen);
        return (status);
    }
    else
    {
        return (BLE_STATUS_FAILED);
    }
}

extern void BLE_hciAssertWakeupPin(void)
{
    /* enable wake-up indication */
    PTD_pinOutSet(PTD_PORT_BTLE_WAKEUP, PTD_PIN_BTLE_WAKEUP);
}

extern void BLE_hciDeassertWakeupPin(void)
{
    /* disable wake-up indication */
    PTD_pinOutClear(PTD_PORT_BTLE_WAKEUP, PTD_PIN_BTLE_WAKEUP);
}

/**
 *  @brief     This function is called by the BLE stack to send data through the HCI layer(TRANSPORT layer).
 *  @param[in] transmitBuffer : pointer to send data
 *  @param[in] transmitLength : length of data to send contained in the data pointer
 *  @retval    BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciSendData(uint8_t* transmitBuffer,
    uint8_t transmitLength)
{

    DBG_ASSERT(transmitBuffer != NULL,
        "NULL pointer had been passed to the function instead of valid dataToSend.");

    DBG_ASSERT(transmitLength != UINT8_C(0), "data send length Cannot be Zero.");

    /* The data has been delivered to the remote device*/
    if (SER_serialWrite(&BLE_uartHandle, NULL, transmitBuffer, transmitLength)
        == SER_SUCCESS)
    {
        return (BLE_STATUS_SUCCESS);
    }
    else
    {
        return (BLE_STATUS_FAILED);
    }
}
/** ************************************************************************* */
