/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <string.h>
#include <stdint.h>

/* additional interface header files */
#include "BLE_ph.h"
#include "BLE_serialDriver_ih.h"
#include "DBG_assert_ih.h"
#include "BleGap.h"
#include "BleHci.h"
#include "BleEngine.h"
#include "BleAlpwDataExchange_Server.h"
#if (BLE_SECURITY == BLE_ENABLE)
#include "BleSm.h"
#endif

/* own header files */
#include "BLE_stateHandler_ih.h"
#include "BLE_stateHandler_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

static uint8_t BLE_isConfigByUser;                      /**< flag used for differentiating the disconnection by the user or by other reason*/
static uint8_t BLE_appRequest;                          /**< current BLE application Request status for BLE */
static uint8_t BLE_appState;                            /**< current BLE application State  */
static BleHandler BLE_handler;                          /**< The handler for BLE stack callback/events variable */
static uint16_t BLE_gapConnectionHandle;                /**< variable for connection handle to send data */
static uint8_t commandParamenter[BLE_commandParamSize]; /**< array for storing HCI command parameters that is to be sent */
static BleServiceRegistryCallBack bleServiceRegistry;   /**< Insistence for BLE service registry */
static BLE_notification_t BLE_notification;
static uint8_t BLE_bloothDeviceName[BLE_MAX_NAME_LENGTH] = "BLE_DEFAULT"; /**< variable to hold the device name , by default name assigned "BLE_DEFAULT" */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief 	This application callback function called by stack and it will receive the GAP events
 *
 * @note    This API called by stack and indicate the initialization response and request to
 *          change states of BLE.
 *          various states:
 *  		- Request to Register the general access protocol service(GAP)
 *  		- Request to Register BLE device name and Initialize the profile or services
 *  		- Request to Generate and Set RandomAddress and paring etc
 *
 * @param[in] event: current device state or status
 *
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 *
 * @param[in] param : This void data pointer has more information of event i.e connection BLE_handler/event,
 *                    status,command etc
 */
static void bleCoreCallback(BleEvent event, BleStatus status, void* param)
{
    /* Variable to store the pairing status */
    BleStatus BLESMP_confirmPairingRetStatus;

    switch (event)
    {
    case BLEEVENT_INITIALIZATION_RSP:
        if (BLESTATUS_SUCCESS != status)
        {
            /* Failed to initialize completely the bluetooth low energy system
             Init the BLE_appState machine*/
            BLE_appState = APPSTATE_IDLE;
            BLE_appRequest = APPREQUEST_NOREQUEST;

        }
        else
        {
            /*  Initialization is successful */
            BLE_appState |= APPSTATE_INITIALIZED;
            /* depends if random addressing is supported, if yes then
             set a random address otherwise go ahead */
#if (BLE_RANDOM_ADDRESSING_SUPPORT == DISABLE)
            BLE_appRequest |= APPREQUEST_REGISTERGAPDEVICE;
#else
#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
            if (BLERADIO_IsPublicAddressValid())
            {
                BLE_appRequest |= APPREQUEST_REGISTERGAPDEVICE;
            }
            else
            {
                BLE_appRequest |= APPREQUEST_RANDOM_ADDRESS;
            }
#else /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */
            BLE_appRequest |= APPREQUEST_RANDOM_ADDRESS;
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */
#endif
        }
        break;

#if (BLE_RANDOM_ADDRESSING_SUPPORT == BLE_ENABLE)
    case BLEEVENT_GENERATE_RANDOM_ADDRESS_RSP:

        BLE_appRequest |= APPREQUEST_REGISTERGAPDEVICE;
        break;
#endif

        /*  Vendor Specific event  */
    case BLEEVENT_VENDOR_COMMAND_COMPLETE:
        {
        BleHciVendorCommandComplete *command = (BleHciVendorCommandComplete*) param;
        if ((SET_POWER_MODE_CMD == command->opcode) && (BLESTATUS_SUCCESS == *(command->parms)))
        {
            /*   The EM9301 is now confirmed to be in SLEEP mode */
            BLE_appState |= APPSTATE_EM9301SLEEP;
        }
    }
        break;

    case BLEEVENT_VENDOR_COMMAND_STATUS:
        {
        BleHciVendorCommandStatus *vendorcommand = (BleHciVendorCommandStatus*) param;
        if (vendorcommand->opcode == SET_POWER_MODE_CMD)
        {
            /* OK Fine, the EM9301 has accepted to go back in IDLE mode
             Wait for the vendor event to be sure that the transition is complete */
        }
    }
        break;

    case BLEEVENT_VENDOR_EVENT:
        {
        BleHciVendorEvent *vendor = (BleHciVendorEvent*) param;

        if ((vendor->parmsLen > UINT8_C(0)) && (vendor->parms[0] == EVENT_CODE_POWER_MODE_IDLE))
        {
            /*   The EM9301 is now confirmed to be in IDLE mode
             * Deasserting wakeup pin and setting device to discoverable mode*/
            BLE_hciDeassertWakeupPin();
            BLE_appState &= ~APPSTATE_EM9301SLEEP;
            /*according to Manual configuration of modes, from sleep mode  device goes to idle mode  and then discoverable mode,
             * to follow this procedure, validating the APPREQUEST_STARTDISCOVERABLE flag whether this event is due to configuration of idle mode(using
             * BLE_setMode API) or not*/
            if (BLE_DISABLE == BLE_isConfigByUser)
            {
                BLE_appRequest |= APPREQUEST_STARTDISCOVERABLE;
            }
        }
    }
        break;

#if (BLE_SECURITY == BLE_ENABLE)
    case BLEEVENT_PAIRING_REQUEST:
        {
        BlePairingInfo *info = ((BlePairingInfo *) param);

        /* Accept the pairing and accept bonding */
        BLESMP_confirmPairingRetStatus = BLESMP_ConfirmPairing(info->connHandle,
            BLE_SET, BLE_SET);
    }
        break;

    case BLEEVENT_PASSKEY_REQUEST:
        /* not suitable in our implementation : SM_IO_CAPABILITIES=SM_IO_NOINPUTNOOUTPUT */
        break;

    case BLEEVENT_PASSKEY_DISPLAY:
        /* not suitable in our implementation : SM_IO_CAPABILITIES=SM_IO_NOINPUTNOOUTPUT */
        break;

    case BLEEVENT_PAIRING_COMPLETE:
        {
        /*	if(status == BLESTATUS_SUCCESS){
         PAIRING SUCCEED:
         }   else {
         PAIRING FAILED:
         }*/
    }
        break;
#endif
    default:
        break;
    }
    UNUSED_PARAMETER(BLESMP_confirmPairingRetStatus);
}
/**
 * @brief 	This application callback function called by general access protocol to indicate device status
 * @note    This API called by GAP and indicate device event and request to
 *          change state of BLE.
 *          various Event:
 *  		- BLEGAP_EVENT_CONNECTEDBLE :host has connected to peripheral device
 *  		- BLEGAP_EVENT_DISCONNECTEDBLE :host has disconnected to peripheral device
 *  		- BLEGAP_EVENT_MODECHANGE: getting the current device mode i.e. discover/profile Init
 * 			      of connection failure etc
 * @param[in] event: current device state or status
 *
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 *
 * @param[in] parms : This void data pointer has more information of event i.e connection BLE_handler/event,
 *                    status,command etc
 */
static void bleGapCallback(BleGapEvent event, BleStatus status, void *parms)
{

    UNUSED_PARAMETER(status);
    U8 newMode;
    uint8_t index;

    /* flag for BLE GAP connection information */
    BleGapConnectionInformation *connInfo = (BleGapConnectionInformation *) parms;
    BLE_connectionDetails_t conectDetails;

    switch (event)
    {

    case BLEGAP_EVENT_CONNECTED:
        {
        /* save the connection handle for future usage */
        BLE_gapConnectionHandle = connInfo->connHandle;
        BLE_appState |= APPSTATE_CONNECTED;

        /* execute the callback function  if notification enabled by the user*/
        if ((BLE_ENABLE_NOTIFICATION == BLE_notification.enableNotification) && (NULL != BLE_notification.callback))
        {
            conectDetails.connectionStatus = BLE_CONNECTED_TO_DEVICE;
            for (index = 0; index < BLE_REMOTE_DEVICE_ADD_LENGTH; index++)
            {
                conectDetails.remoteAddress.addr[index] = connInfo->remoteAddress.addr[index];
            }
            /** pass the connected status and  the device address to which it got connected*/
            BLE_notification.callback(conectDetails);
        }
    }
        break;

    case BLEGAP_EVENT_DISCONNECTED:

    	/* Reset the Handle */
    	BLE_gapConnectionHandle = UINT16_C(0);

    	/* Device are now disconnected */
        BLE_appState &= ~APPSTATE_CONNECTED;

        /* If Disconnect was not due to sleep request and by the user(configuring idle mode using "BLE_setMode" API),
         *  again set to application request discoverable */
        if (((BLE_appRequest & APPREQUEST_EM9301SLEEP) == UINT8_C(0)) && (BLE_DISABLE == BLE_isConfigByUser))
        {
            BLE_appRequest |= APPREQUEST_STARTDISCOVERABLE;
        }

        /* execute the callback function  if notification enabled by the user*/
        if ((BLE_ENABLE_NOTIFICATION == BLE_notification.enableNotification) && (NULL != BLE_notification.callback))
        {

            conectDetails.connectionStatus = BLE_DISCONNECTED_FROM_DEVICE;

            for (index = 0; index < BLE_REMOTE_DEVICE_ADD_LENGTH; index++)
            {
                conectDetails.remoteAddress.addr[index] = connInfo->remoteAddress.addr[index];
            }
            /** pass the disconnected status and  the device address from which it got disconnected*/
            BLE_notification.callback(conectDetails);
        }
        break;

    case BLEGAP_EVENT_MODECHANGE:
        {
        newMode = BLEGAP_GetMode();

        if (((newMode & BLEMODE_DISCOVERABLE_MASK) == BLEMODE_DISCOVERABLE)
            && ((newMode & BLEMODE_CONNECTABLE_MASK) == BLEMODE_CONNECTABLE))
        {
            BLE_appState |= APPSTATE_DISCOVERABLE;

        }
        else
        {
            BLE_appState &= ~APPSTATE_DISCOVERABLE;
        }
    }
        break;
    default:
        break;
    }
}
/**
 *@brief API to configure the device to non discoverable and non connectable mode
 */
static void bleSetNonConnectableState(void)
{
    BleStatus BLE_coreStackInitRetStatus;
    BleStatus status;

    BleGapMode mode;

    /* get the current mode of bluetooth device*/
    mode = BLEGAP_GetMode();

    if ((mode & (BLEMODE_DISCOVERABLE_MASK | BLEMODE_CONNECTABLE_MASK)))
    {

        status = BLEGAP_SetMode(BLEMODE_NOTCONNECTABLE|BLEMODE_NOTDISCOVERABLE);
        if (status == BLESTATUS_FAILED)
        {
            /* Reset the chip due to error */
            BLE_coreStackInitRetStatus = BLE_coreStackInit();
        }
        else if (status == BLESTATUS_SUCCESS)
        {
            /* in requested idle state */
        }
        else
        {
            /* Means pending wait for the BLEGAP_EVENT_MODECHANGE in the GAP callback*/
        }
    }
    UNUSED_PARAMETER(BLE_coreStackInitRetStatus);
}

/* global functions ********************************************************* */
/**
 * @brief 	Initialize the whole BLE stack, including transport, radio ...
 *
 * @note    This function is used to initialize the BLE stack. It shall be the entry
 *  	    point of an application using it and it shall be called before any other
 *  	    stack interfaces.
 * 	    It initializes:
 *  		- Some global context and internal states & transport layer
 *  		- The radio layer & Core layer (BLE-HCI, L2CAP...)
 *
 * @retval   The status of the operation:
 *  		- BLESTATUS_SUCCESS indicates that the operation is successful
 * 		- BLESTATUS_FAILED indicates that the BLE stack could not be initialized
 */
extern BLE_status BLE_coreStackInit(void)
{
    /* Variable to store the initialization status*/
    BleStatus BLEMGMT_initRetstatus;

    /* Init the BLE_appState machine */
    BLE_appState = APPSTATE_IDLE;
    BLE_appRequest = APPREQUEST_NOREQUEST;

    BLEMGMT_initRetstatus = BLEMGMT_Init();
    if (BLEMGMT_initRetstatus == BLESTATUS_PENDING)
    {
        /* The initialization is ongoing,
         Register the stack callback to catch the init success. */
        BLE_handler.callback = bleCoreCallback;
        BLEMGMT_initRetstatus = BLEMGMT_RegisterHandler(&BLE_handler);

    }
    return (BLEMGMT_initRetstatus); /*  BLESTATUS_FAILED or BLESTATUS_SUCCESS */
}
/**
 * @brief 	 This function is used to set BLE in sleep mode
 *   		 allow the system to go into low power mode when the SDK have nothing to do
 *  	   	 especially the APPREQUEST_NOTIFYTORUN
 * @retval   The status of the operation:
 *  			- BLESTATUS_SUCCESS indicates that the operation is successful
 * 				- BLESTATUS_FAILED indicates that the BLE stack could not be
 *      		  initialized
 */
extern BLE_status BLE_readyToGoToSleep(void)
{
    /*    allow the system to go into low power mode when the SDK have nothing to do
     especially the APPREQUEST_NOTIFYTORUN   */
    if (BLE_appRequest == APPREQUEST_NOREQUEST)
    {

        /*	allow the system to go into low power mode when initialized	*/
        if ((BLE_appState & APPSTATE_INITIALIZED) != BLE_CLEAR)
        {
            return (BLESTATUS_SUCCESS);
        }
    }

    return (BLESTATUS_FAILED);
}

/**
 * @brief 	  This function is used to set application request into Stack Run mode
 *			  The BLE stack requests to be executed BLEMGMT_Run().
 */
extern void BLEMGMT_NotifyToRun(void)
{
    BLE_appRequest |= APPREQUEST_NOTIFYTORUN;
}

/**
 *  @brief   This function is used to run the BLE stack and register a BLE device with the specified role(i.e BROADCASTER,OBSERVER,PERIPHERAL,.),BLE Device Name.
 *           It registers,
 *             - GAP/GATT protocols
 *             - BLE profile (services and characteristic's attribute )
 *             - Establish the connection or advertise to the Remote device
 *             - Update the BLE device connection status i.e Connect/Disconnect
 *           If BLE State process failed due to some reason, then BLE Stack will be Initialize automatically.
 *
 *  @note    It could be called by a thread or by a scheduler.
 *
 *  @retval     The status of the operation:
 *  		- BLESTATUS_SUCCESS indicates that the operation is successful
 * 		- BLESTATUS_FAILED indicates that the BLE stack could not be initialized
 */
extern BLE_appStateReturn BLE_coreStateMachine(void)
{
    /* Variable to store the run return status */
    BleStatus BLEMGMT_runRetStatus;

    /* Variable to store the setting of bluetooth device name return status */
    BleStatus BLEGAP_setLocalBluetoothDeviceNameRetStatus;

    /* Variable to store setting of local appearance return status */
    BleStatus BLEGAP_setLocalAppearanceRetStatus;

    /* Variable to store core stack initialization return status */
    BleStatus BLE_coreStackInitRetStatus;

#if (BLE_RANDOM_ADDRESSING_SUPPORT == BLE_ENABLE)
    /* Variable to store the generating of random address return status*/
    BleStatus BLESMP_GenerateAndSetRandomAddressRetStatus;
#endif

    /* Variable to store registeration of device return status */
    BleStatus BLEGAP_registerDeviceRetStatus;

    /* INTERNAL APPLICATION REQUESTS */
    while ((BLE_appRequest & APPREQUEST_NOTIFYTORUN) != BLE_CLEAR)
    {
        /* Clear this BLE_appRequest flags	*/
        BLE_appRequest &= ~APPREQUEST_NOTIFYTORUN;
        /* Do the stuff, note that BLEMGMT_Run(); may lead to a  BLEMGMT_NotifyToRun so purge this request while it
         exist.	*/
        BLEMGMT_runRetStatus = BLEMGMT_Run();
    }

    if ((BLE_appRequest & APPREQUEST_RANDOM_ADDRESS) != BLE_CLEAR)
    {

        /* Clear this BLE_appRequest flags*/
        BLE_appRequest &= ~APPREQUEST_RANDOM_ADDRESS;
	#if (BLE_RANDOM_ADDRESSING_SUPPORT == BLE_ENABLE)
        /*   Register a GAP device*/
        BLESMP_GenerateAndSetRandomAddressRetStatus =
            BLESMP_GenerateAndSetRandomAddress(BLESM_RANDOMADDRESS_STATIC);
        if (BLESMP_GenerateAndSetRandomAddressRetStatus != BLESTATUS_PENDING)
        {
            BLE_coreStackInitRetStatus = BLE_coreStackInit();
        }
        else
        {
            /*    The operation is ongoing, wait for the BLEEVENT_GENERATE_RANDOM_ADDRESS_RSP event indicating
             this operation completion    */
        }
	#endif
    }

    if ((BLE_appRequest & APPREQUEST_REGISTERGAPDEVICE) != BLE_CLEAR)
    {

        /* Clear this BLE_appRequest flags*/
        BLE_appRequest &= ~APPREQUEST_REGISTERGAPDEVICE;
        /* Register a GAP device*/
        BLEGAP_registerDeviceRetStatus = BLEGAP_RegisterDevice(
            BLEGAPROLE_PERIPHERAL, &bleGapCallback);
        if (BLEGAP_registerDeviceRetStatus != BLESTATUS_SUCCESS)
        {
            BLE_coreStackInitRetStatus = BLE_coreStackInit();
        }
        else
        {
            /* init profiles */
            BLE_appRequest |= APPREQUEST_INITPROFILES;
        }
    }

    if ((BLE_appRequest & APPREQUEST_INITPROFILES) != BLE_CLEAR)
    {

        /* Clear this BLE_appRequest flags*/
        BLE_appRequest &= ~APPREQUEST_INITPROFILES;

        /* Set the local device friendly name*/
        BLEGAP_setLocalBluetoothDeviceNameRetStatus = BLEGAP_SetLocalBluetoothDeviceName((U8* ) BLE_bloothDeviceName,(U8 ) sizeof(BLE_bloothDeviceName));

       /* Set the local device appearance*/
        BLEGAP_setLocalAppearanceRetStatus = BLEGAP_SetLocalAppearance(BLEGAPAPPEARANCE_UNKNOWN);

        /* register the  Bluetooth service from application layer callback */
        bleServiceRegistry();

        /* either Set application Request to Sleep mode or Discoverable depending on the requirement*/
        BLE_appRequest |= APPREQUEST_STARTDISCOVERABLE;
    }

    if ((BLE_appRequest & APPREQUEST_ENDCONNECTION) != UINT8_C(0))
    {
        /* Disconnect to be called only if current state is discoverable */
        if ((BLE_appState & APPSTATE_CONNECTED) != UINT8_C(0))
        {
            BLEGAP_Disconnect(BLE_gapConnectionHandle);
        }
        BLE_appRequest &= ~APPREQUEST_ENDCONNECTION;
    }

    if ((BLE_appRequest & APPREQUEST_EM9301SLEEP) != BLE_CLEAR)
    {

        /* Only proceed to set the device to sleep if not in connected state */
        if ((BLE_appState & APPSTATE_CONNECTED) != UINT8_C(0))
        {
            ;
        }
        else
        {

            /* make device non discoverable and non connectable */
            bleSetNonConnectableState();

            /* clear the SLeep State*/
            BLE_appRequest &= ~APPREQUEST_EM9301SLEEP;

            /* de-assert wake-up pin */
            BLE_hciDeassertWakeupPin();

            /* validating the BLE active state from sleep */
            if (BLEHCI_SendVendorSpecificCommand(SET_POWER_MODE_CMD,
                POWER_MODE_CMD_PARAM_LENGTH, commandParamenter) == BLESTATUS_FAILED)
            {

                /* application request set peripheral device to discoverable mode */
                BLE_appRequest |= APPREQUEST_STARTDISCOVERABLE;
            }
        }
        /* The answer of this transition will be notified by a command complete in the
         BleApp_CoreCallback Core stack callback*/

    }

    if ((BLE_appRequest & APPREQUEST_EM9301IDLE) != BLE_CLEAR)
    {
        if ((BLE_appState & APPSTATE_CONNECTED) != BLE_CLEAR)
        {
            ;
        }
        else
        {

            /* make device non discoverable and non connectable */
            bleSetNonConnectableState();

            /* Clear this BLE_appRequest flags*/
            BLE_appRequest &= ~APPREQUEST_EM9301IDLE;
        }

        BLE_hciAssertWakeupPin();

    }

    if ((BLE_appRequest & APPREQUEST_STARTDISCOVERABLE) != BLE_CLEAR)
    {
        /* Clear this BLE_appRequest flags*/
        BLE_appRequest &= ~APPREQUEST_STARTDISCOVERABLE;
        if ((BLE_appState & APPSTATE_INITIALIZED) != BLE_CLEAR)
        {
            BleStatus status;

            status = BLEGAP_SetMode(BLEMODE_DISCOVERABLE | BLEMODE_CONNECTABLE);

            if (status == BLESTATUS_FAILED)
            {
                /*	 We have an error here reset the chip?*/
                BLE_coreStackInitRetStatus = BLE_coreStackInit();
            }
            else if (status == BLESTATUS_SUCCESS)
            {
                /* already in this state*/
                BLE_appState |= APPSTATE_DISCOVERABLE;
            }
            else
            {
                /*	Means pending wait for the BLEGAP_EVENT_MODECHANGE in
                 the GAP callback */
            }
        }
    }
    UNUSED_PARAMETER(BLEMGMT_runRetStatus);
    UNUSED_PARAMETER(BLEGAP_setLocalBluetoothDeviceNameRetStatus);
    UNUSED_PARAMETER(BLEGAP_setLocalAppearanceRetStatus);
    UNUSED_PARAMETER(BLE_coreStackInitRetStatus);

    return (BLE_appRequest); /* Return the Application Request */
}

/**
 * @brief   This API used to wake up the BLE device from Sleep mode.
 */
void BLE_wakeUpDevice(void)
{
    BLE_hciAssertWakeupPin();
}

/**
 * @brief   This API used to set the BLE into the desired sleep mode.
 *
 * @param[in]   BLE_SleepType_t - the target sleep mode
 */
void BLE_setDeviceToSleep(BLE_SleepType_t sleepType)
{
    DBG_ASSERT(sleepType < BLE_ST_MAX, "Invalid Sleep Type");

    /* Setting the command parameter for the HCI command based on required mode */
    if (sleepType == BLE_ST_NORMAL_SLEEP)
    {
        *commandParamenter = POWER_MODE_SLEEP;
    }
    else
    {
        *commandParamenter = POWER_MODE_DEEPSLEEP;
    }

    /* Requesting both sleep and disconnect as a connected device will not move to sleep*/
    BLE_appRequest |= APPREQUEST_EM9301SLEEP;
    BLE_appRequest |= APPREQUEST_ENDCONNECTION;
}

/**
 * @brief       This API used to register application call back
 *              with service profile registry.
 *
 * @param[in]   BleServiceRegistryHandlerCallBack - function pointer
 */
extern BLE_status BLE_customServiceRegistry(BleServiceRegistryCallBack bleCustomService)
{
    BLE_status bleServiceMapReturn = BLESTATUS_FAILED;

    /* NULL case checking for function pointer */
    DBG_ASSERT(bleCustomService != NULL,
        "NULL pointer had been passed to BLE service registry.");

    if (bleCustomService != NULL )
    {
        bleServiceRegistry = bleCustomService;
        bleServiceMapReturn = BLESTATUS_SUCCESS;
    }

    return (bleServiceMapReturn);
}

/**
 * @brief       This function used to set mode of Bluetooth Low Energy Device
 *
 * @param[in]   modeToSet   Mode to configure , refer BLE_mode_t for availbale modes
 *
 * @retval    status of the execution
 *
 * @note   inorder to incorporate these settings the state machine(BLE_coreStateMachine) has to run periodically
 */
extern BLE_returnStatus_t BLE_setMode(BLE_configureMode_t modeToSet)
{
    BLE_returnStatus_t retValue = BLE_FAILURE;

    switch (modeToSet)
    {
    /* idle state refers here is, when there is no link established between them */
    /* idle mode is achieved by making the device disconect and  setting the NON discoverable and non connectable mode*/
    case BLE_IDLE:
        BLE_appRequest = APPREQUEST_ENDCONNECTION;
        BLE_appRequest |= APPREQUEST_EM9301IDLE;
        BLE_isConfigByUser = BLE_ENABLE;
        retValue = BLE_SUCCESS;
        break;

    case BLE_DISCOVERABLE:
        /* make device discoverable only if it is in idle mode*/
        if (APPSTATE_EM9301SLEEP != (BLE_appState & APPSTATE_EM9301SLEEP))
            {
            BLE_appRequest = APPREQUEST_STARTDISCOVERABLE;
            BLE_isConfigByUser = BLE_DISABLE;
            retValue = BLE_SUCCESS;
            }
        break;

    case BLE_NORMAL_SLEEP:
        if (APPSTATE_EM9301SLEEP != (BLE_appState & APPSTATE_EM9301SLEEP))
        {
            BLE_setDeviceToSleep(BLE_ST_NORMAL_SLEEP);
            retValue = BLE_SUCCESS;
        }
        break;

    case BLE_DEEP_SLEEP:
        if (APPSTATE_EM9301SLEEP != (BLE_appState & APPSTATE_EM9301SLEEP))
        {
            BLE_setDeviceToSleep(BLE_ST_DEEP_SLEEP);
            retValue = BLE_SUCCESS;
        }
        break;

    default:
        retValue = BLE_INVALID_PARAMETER;
        break;
    }
    return (retValue);
}

/**
 * @brief       This function used to get the mode of Bluetooth Low Energy Device
 *
 * @param[out]   modeConfigured   the mode of the Bluetooth Device.
 *
 * @retval    status of the execution
 *
 */
extern BLE_returnStatus_t BLE_getMode(BLE_deviceState_t *modeConfigured)
{
    if (modeConfigured == NULL )
    {
        return BLE_INVALID_PARAMETER;
    }

    if (APPSTATE_EM9301SLEEP == (BLE_appState & APPSTATE_EM9301SLEEP))
    {
        *modeConfigured = BLE_DEVICE_IN_SLEEP;
    }
    else if (APPSTATE_DISCOVERABLE != (BLE_appState & APPSTATE_DISCOVERABLE) && (APPSTATE_CONNECTED != (BLE_appState & APPSTATE_CONNECTED)))
    {
        *modeConfigured = BLE_DEVICE_IN_IDLE;
    }
    else if (APPSTATE_DISCOVERABLE == (BLE_appState & APPSTATE_DISCOVERABLE))
    {
        *modeConfigured = BLE_DEVICE_IN_DISCOVERABLE;
    }
    else if (APPSTATE_CONNECTED == (BLE_appState & APPSTATE_CONNECTED))
    {
        *modeConfigured = BLE_DEVICE_IN_CONNECTED;
    }
    else
    {
        DBG_FAIL("Invlaid State of BLE");
    }

    return (BLE_SUCCESS);
}

/**
 * @brief       This function used to set Bluetooth Low Energy Device friendly name
 *
 * @param[in]   deviceName   A valid pointer to the Bluetooth device friendly name.
 *
 * @retval      status of the execution
 *
 * @note        API can be called before profile initializations and the supported length of 
 * 			    the device is 20 bytes
 */
extern BLE_returnStatus_t BLE_setDeviceName(uint8_t *deviceName, uint8_t length)
{

    BLE_returnStatus_t retValue;
    uint8_t index;

    if (BLE_MAX_NAME_LENGTH > length)
    {
        memset(BLE_bloothDeviceName, '\0', BLE_MAX_NAME_LENGTH);

        for (index = BLE_CLEAR; index < length; index++)
        {
            BLE_bloothDeviceName[index] = deviceName[index];
        }
        retValue = BLE_SUCCESS;
    }
    else
    {
        retValue = BLE_INVALID_DEVICE_NAME_LENGTH;
    }

    return (retValue);
}

/**
 * @brief       This function used to register the callback function device connection and disconnection notification
 * 				and to enable/disable the notification feature
 *
 * @param[in]   configParams   A valid pointer to the Bluetooth device friendly name.
 *
 * @retval      status of the execution
 *
 * @note      the callback function should be minimal size
 */
extern BLE_returnStatus_t BLE_enablenotificationForConnect(BLE_notification_t configParams)
{
    BLE_returnStatus_t retValue;

    if (NULL != configParams.callback)
    {
        BLE_notification.enableNotification = configParams.enableNotification;
        BLE_notification.callback = configParams.callback;
        retValue = BLE_SUCCESS;
    }
    else
    {
        retValue = BLE_INVALID_PARAMETER;
    }

    return (retValue);

}

/**
 * @brief   To transmit the BLE data into Target device.
 * @param[in]   dataToSend              The payload to be sent
 * @param[in]   dataToSendLen           The length of the payload to be sent
 * @retval  The status of the operation:
 *              - BLE_SUCCESS indicates that the operation is successful
 *              - BLE_FAILURE indicates that the BLE stack could not be
 *                initialized
 */
extern BLE_returnStatus_t BLE_sendData(uint8_t* dataToSend, uint8_t dataToSendLen)
{
	BLE_returnStatus_t retValue = BLE_FAILURE;
	 DBG_ASSERT(dataToSend != NULL,
	        "Invalid Data");
	 DBG_ASSERT(dataToSendLen != NULL,
	 	        "Invalid Data Length");

	if((UINT16_C(0) != BLE_gapConnectionHandle) && (NULL != dataToSend))
    {
        if(BLESTATUS_FAILED == BLEALPWDATAEXCHANGE_SERVER_SendData(BLE_gapConnectionHandle,dataToSend, dataToSendLen))
        {
        	retValue = BLE_FAILURE;
        }
        else
        {
        	retValue = BLE_SUCCESS;
        }
    }
	   return(retValue);

}

/** ************************************************************************* */
