/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BLE_STATEHANDLER_CH_H_
#define BCDS_BLE_STATEHANDLER_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

/**< This define are using for Core-Stack callback Handler  */
#define APPREQUEST_NOREQUEST			UINT8_C(0x00)
#define APPREQUEST_NOTIFYTORUN			UINT8_C(0x01)
#define APPREQUEST_STARTDISCOVERABLE	UINT8_C(0x02)
#define APPREQUEST_REGISTERGAPDEVICE	UINT8_C(0x04)
#define APPREQUEST_INITPROFILES			UINT8_C(0x08)
#define APPREQUEST_EM9301SLEEP			UINT8_C(0x10)
#define APPREQUEST_EM9301IDLE			UINT8_C(0x20)
#define APPREQUEST_RANDOM_ADDRESS		UINT8_C(0x40)
#define APPREQUEST_ENDCONNECTION        UINT8_C(0x80)

/**< This define are using in General access Profile(GAP) handler
 to indicate to change states of BLE */
#define APPSTATE_IDLE					UINT8_C(0x00)
#define APPSTATE_INITIALIZED			UINT8_C(0x01)
#define APPSTATE_EM9301SLEEP			UINT8_C(0x02)
#define APPSTATE_EM9301IDLE 			UINT8_C(0x04)
#define APPSTATE_DISCOVERABLE			UINT8_C(0x08)
#define	APPSTATE_CONNECTED				UINT8_C(0x10)

/**< These defines are used for sending the HCI command for setting
 *   the power mode (operating state) of the device
 */
#define SET_POWER_MODE_CMD       	    UINT16_C(0xFC03)
#define POWER_MODE_IDLE                 UINT8_C(0x00)
#define POWER_MODE_SLEEP                UINT8_C(0x01)
#define POWER_MODE_DEEPSLEEP            UINT8_C(0x02)
#define POWER_MODE_CMD_PARAM_LENGTH     UINT8_C(0x01)

#define BLE_MAX_NAME_LENGTH             UINT8_C(21) /**< the Maximum length of the bluetooth */
#define BLE_REMOTE_DEVICE_ADD_LENGTH    UINT8_C(6)  /**< the length of the remote device name*/

/* This is used to define the valid parameter received for a
 * Vendor event during a transition to the IDLE Power Mode
 */
#define EVENT_CODE_POWER_MODE_IDLE      UINT8_C(0x01)

/* local function prototype declarations */

/**
 * @brief 	This application callback function called by stack and it will receive the GAP events
 *          various events:
 *  			- Request to Register the general access protocol service(GAP)
 *  			- Request to Register BLE device name and Initialize the profile or services
 *  			- Request to Generate and Set RandomAddress
 * 			    - paring etc
 * @param[in] event: current device state or status
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 * @param[in] param : This void data pointer has more information of event i.e connection handler/event,
 *                    status,command etc
 */
static void bleCoreCallback(BleEvent event, BleStatus status, void* param);

/**
 * @brief 	This application callback function called by general access protocol to indicate device status
 *          various Event:
 *  			- BLEGAP_EVENT_CONNECTEDBLE :host has connected to peripheral device
 *  			- BLEGAP_EVENT_DISCONNECTEDBLE :host has disconnected to peripheral device
 *  			- BLEGAP_EVENT_MODECHANGE: getting the current device mode i.e. discover/profile Init
 * 			      of connection failure etc
 * @param[in] event: current device state or status
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 * @param[in] param : This void data pointer has more information of event i.e connection handler/event,
 *                    status,command etc
 */
static void bleGapCallback(BleGapEvent event, BleStatus status, void *parms);

/**
 *@brief API to configure the device to non discoverable and non connectable mode
 */
static void bleSetNonConnectableState(void);
/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_BLE_STATEHANDLER_CH_H_ */

/** ************************************************************************* */
