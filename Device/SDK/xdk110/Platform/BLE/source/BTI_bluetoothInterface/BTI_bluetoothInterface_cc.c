/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>
#include <stddef.h>

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "queue.h"
#include "BleRadio.h"
#include "BLE_stateHandler_ih.h"
#include "BLE_serialDriver_ih.h"
#include "BleAlpwDataExchange_Server.h"
#include "DBG_assert_ih.h"

/* own header files */
#include "BTI_bluetoothInterface_ih.h"
#include "BTI_bluetoothInterface_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

 /**< task handle for the API BTI_app() */
static xTaskHandle BTI_bluetoothInterface_gdu;

static BTI_callBackOnDataRead BTI_dataReadCallBack;

static BLE_connectionStatus_t BTI_deviceStatus = BLE_DISCONNECTED_FROM_DEVICE; /**< BLE connection status */

/** Handler for the Bluetooth Send/TX Data Queue */
static xQueueHandle BTI_sendQueueHandle;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* The description is in the configuration header */
static void btiConnectionStatusCallback(BLE_connectionDetails_t connectionDetails)
{

    BTI_deviceStatus = connectionDetails.connectionStatus;

}

/* The description is in the configuration header */
static void btiReceiveBleData(BleAlpwDataExchangeEvent event, BleStatus status, void *parameters)
{
    if (parameters == NULL)
    {
        return;
    }

    if (BLEALPWDATAEXCHANGE_EVENT_RXDATA == event)
    {
        if (BLESTATUS_SUCCESS == status)
        {

            BleAlpwDataExchangeServerRxData *bleRxData = (BleAlpwDataExchangeServerRxData *) parameters;

            DBG_ASSERT(bleRxData->rxData != NULL, "NULL data pointer passed from BleDataExchange.");

            /* @todo - make this API call Thread safe. */

            /* Calling the callback to Read and Processes the data */
            BTI_dataReadCallBack(bleRxData->rxData, bleRxData->rxDataLen);
        }
    }
}

/* The description is in the configuration header */
static void btiApp(void *pParameters)
{
    (void) pParameters; /* to quiet warnings */

    for (;;)
    {
        /* Read the BTLE stack Register data from ring buffer send to the chip */
        BLE_hciReceiveData();

        /* This function is used to run the BLE stack and register a BLE device with the specified role */
        BLE_coreStateMachine();

        bluetoothIF_sendDataFromQueue();
    }
}

/**
 * @brief  The BEA_bleServiceRegister is used to register the BLE Alpwise DataExchange
 *         service's into attribute database.
 */
/* The description is in the configuration header */
static void btiBleAppServiceRegister(void)
{
    /* flag for service registry return */
    BleStatus serviceRegistryStatus;

    /* Alpwise data Exchange Service Register*/
    serviceRegistryStatus = BLEALPWDATAEXCHANGE_SERVER_Register(btiReceiveBleData);

    /* Checking data NULL pointer condition */
    if (serviceRegistryStatus == BLESTATUS_FAILED)
    {
        DBG_ASSERT(serviceRegistryStatus != BLESTATUS_FAILED, "BLE Service registry was failure");
    }

}

static void bluetoothIF_sendDataFromQueue(void)
{
    BluetoothIF_queueSendData_T packetData;
    BLE_returnStatus_t retCode = BLE_FAILURE;

    if (pdTRUE == xQueueReceive(BTI_sendQueueHandle, &packetData, UINT32_C(0)))
    {
        if (packetData.packetData != NULL && packetData.dataSize > 0)
        {
            retCode = BLE_sendData(packetData.packetData, packetData.dataSize);
        }
        if (retCode != BLE_SUCCESS)
        {
            DBG_FAIL("BLE Data Send Failed");
        }
    }
}

static void bluetoothIF_copyToQueueStruct(BluetoothIF_queueSendData_T *packetStruct, uint8_t* dataToCopy, uint8_t size)
{
    uint8_t indexData = UINT8_C(0);
    packetStruct->dataSize = size;

    while (indexData < BTI_SEND_MAX_PACKET_SIZE)
    {
        if (indexData < size)
        {
            packetStruct->packetData[indexData] = dataToCopy[indexData];
        }
        else
        {
            packetStruct->packetData[indexData] = UINT8_C(0);
        }
        indexData = indexData + 1;
    }

}

/* global functions ********************************************************* */

/* The description is in the interface header */
extern void BTI_init(BTI_callBackOnDataRead callBackOnDataRead)
{
    DBG_ASSERT(callBackOnDataRead != NULL, "The pointer is not NULL");

    /* return value for BLE stack configuration */
    BleStatus btiStackInitStatus = BLESTATUS_FAILED;
    BLE_notification_t btiConnectionStatus = { .enableNotification = BLE_ENABLE_NOTIFICATION,
            .callback = &btiConnectionStatusCallback };
    BLE_returnStatus_t returnValue;
    uint64_t uid;
    uint8_t uniqueName[] = BLE_DEVICENAME;
    BD_ADDR uniqueAddress = { { UINT8_C(0), UINT8_C(0), UINT8_C(0), UINT8_C(0), UINT8_C(0), UINT8_C(0) } };

    (void) BLE_enablenotificationForConnect(btiConnectionStatus);

    BTI_dataReadCallBack = callBackOnDataRead;

    uid = UNIQUE_MAC;

    uniqueAddress.addr[UINT8_C(0)] = (uint8_t) (uid >> UINT8_C(40));
    uniqueAddress.addr[UINT8_C(1)] = (uint8_t) (uid >> UINT8_C(32));
    uniqueAddress.addr[UINT8_C(2)] = (uint8_t) (uid >> UINT8_C(24));
    uniqueAddress.addr[UINT8_C(3)] = (uint8_t) (uid >> UINT8_C(16));
    uniqueAddress.addr[UINT8_C(4)] = (uint8_t) (uid >> UINT8_C(8));
    uniqueAddress.addr[UINT8_C(5)] = (uint8_t) (uid);

    BLERADIO_SetPublicAddress(&uniqueAddress);

    returnValue = BLE_setDeviceName(&uniqueName[0], sizeof(uniqueName) - UINT8_C(1));

    if (returnValue == BLE_INVALID_DEVICE_NAME_LENGTH)
    {
        DBG_FAIL("Setting BLE device name failed");
    }

    /* Registering the BLE Services  */
    BLE_customServiceRegistry(btiBleAppServiceRegister);

    /* Initialize the whole BLE stack */
    btiStackInitStatus = BLE_coreStackInit();

    if (BLESTATUS_FAILED == btiStackInitStatus)
    {
        DBG_FAIL("BLE Boot up process Failed,.!");
    }
    /* create task for BLE state machine */
    if (pdTRUE != xTaskCreate(btiApp, (const char * const ) "BTI",
    STACK_SIZE_FOR_TASK, NULL, TASK_PRIORITY, &BTI_bluetoothInterface_gdu))
    {
        /* BLE task creation fail case */
        DBG_FAIL("BLE Task was not created, Due to Insufficient heap memory");
    }
    BTI_sendQueueHandle = xQueueCreate(BTI_SEND_QUEUE_SIZE, sizeof(BluetoothIF_queueSendData_T));
    if (BTI_sendQueueHandle == NULL)
    {
        DBG_FAIL("BLE Send Queue could not be created");
    }
}

/* The description is in the interface header */
extern bluetoothIF_retCode_T BTI_sendData(uint8_t* dataToSend, uint8_t dataToSendLen)
{
    DBG_ASSERT(dataToSend != NULL, "invalid Pointer");

    bluetoothIF_retCode_T retCode = BluetoothIF_FAILURE;

    BluetoothIF_queueSendData_T packetData;

    if (BLE_CONNECTED_TO_DEVICE == BTI_deviceStatus)
    {
        bluetoothIF_copyToQueueStruct(&packetData, dataToSend, dataToSendLen);

        if (pdTRUE == xQueueSend(BTI_sendQueueHandle, (void *) &packetData, UINT32_C(0)))
        {
            retCode = BluetoothIF_SUCCESS;
        }
        else
        {
            DBG_FAIL("BLE Send queue is full");
            retCode = BluetoothIF_BUFFER_FAULT;
        }
    }
    return (retCode);
}

/* The description is in the interface header */
extern void BTI_deviceSleep(void)
{
    /* Bluetooth sleep mode */
    BLE_setDeviceToSleep(BLE_ST_DEEP_SLEEP);
}

/* The description is in the interface header */
extern void BTI_deviceWakeUp(void)
{
    /* Bluetooth Wakeup */
    BLE_wakeUpDevice();
}

/** ************************************************************************* */
