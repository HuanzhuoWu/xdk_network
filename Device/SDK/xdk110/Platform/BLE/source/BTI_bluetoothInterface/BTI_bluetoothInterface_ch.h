/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BTI_BLUETOOTHINTERFACE_CH_H_
#define BCDS_BTI_BLUETOOTHINTERFACE_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define STACK_SIZE_FOR_TASK                     (configMINIMAL_STACK_SIZE + 300)
#define TASK_PRIORITY                           (tskIDLE_PRIORITY + 1)
#define BTI_SEND_QUEUE_SIZE 					(10)
#define BTI_SEND_MAX_PACKET_SIZE 				(20)

struct BluetoothIF_queueSendData_S {
	uint8_t dataSize;
	uint8_t packetData[BTI_SEND_MAX_PACKET_SIZE];
};

typedef struct BluetoothIF_queueSendData_S BluetoothIF_queueSendData_T;

/* local function prototype declarations */

/**
 * @brief   This function is used for creating the bluetooth task
 * @param[in] pParameters : This void data pointer
 */
static void btiApp(void *pParameters);

/**
 * @brief  This function is used to register the BLE Alpwise DataExchange
 *         service's into attribute database.
 */
static void btiBleAppServiceRegister(void);

/**
 * @brief   This function callback used in ALPWISE Data Exchange Profile to transfer/receive BLE data
 * @param[in] event: current device state or status
 * @param[in] status: Event status i.e BLESTATUS_SUCCESS or BLESTATUS_FAILURE
 * @param[in] param : This void data pointer has more information of event i.e connection host data/event,
 *                    status,command etc
 */
static void btiReceiveBleData(BleAlpwDataExchangeEvent event, BleStatus status, void *parms);

static void bluetoothIF_sendDataFromQueue(void);

static void bluetoothIF_copyToQueueStruct(BluetoothIF_queueSendData_T *packetStruct, uint8_t* dataToCopy, uint8_t size);

/**
 * @brief   callback function executed when device connected or disconnected to/from a host
 *
 * @param[in] connectionDetails: connection status and remote address
 */
static void btiConnectionStatusCallback(BLE_connectionDetails_t connectionDetails);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_BTI_BLUETOOTHINTERFACE_CH_H_ */

/** ************************************************************************* */
