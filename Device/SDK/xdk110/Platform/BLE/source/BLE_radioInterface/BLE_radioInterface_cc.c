/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stddef.h>
#include <stdint.h>

/* additional interface header files */
#include "BLECustomConfig.h"
#include "BleTypes.h"
#include "BleHci.h"

#ifndef BLE_EM9301_LOADPATCH
#define BLE_EM9301_LOADPATCH                        1
#endif

#if (BLECONTROLLER_NEED_SPECIFIC_INIT == 1)
#include "BleSystem.h"

/** BLE_EM9301_LOADPATCH allows the user to choose if the patch for the
 * EM9301 Bluetooth Low energy Controller version Di07 is applied
 * at startup.
 * @ see u9301_fw_patch_loading.pdf documentation for more information about
 *  EM9301 patch loading
 *
 * Default value is ENABLED, if you want to disable the patch application
 * at startup, define BLE_EM9301_LOADPATCH to 0 in BleCustomConfig.h file.
 */
#if (BLE_EM9301_LOADPATCH == 1)
#include "EM9301_Patch_V0x0A.h"
#endif

/* own header files */
#include "BleRadio.h"

/* constant definitions ***************************************************** */
#if (BLE_EM9301_LOADPATCH == 1)
/* Parameter for the HCI commands sent during initialization */
uint8_t hciCommandParms[50];

/*  Number of ROW in patch file [0-63] */
#define NUMBER_OF_ROWS			64

/* Number of Bytes Per ROW in patch file [0-63] */
#define ROW_SIZE				48

/* Number of Sectors [0-17] in patch file [0-63] */
#define NUMBER_OF_SECTORS		2

signed char currentRow;
signed char currentSector;

uint8_t patchLoadingState;

#define PATCH_LOAD_STATE_NOTSTARTED             0x00u
#define PATCH_LOAD_STATE_ENTERINGISPMODE        0x01u
//#define PATCH_LOAD_STATE_LOADING                0x02u
#define PATCH_LOAD_STATE_EXITINGISPMODE         0x04u
//#define PATCH_LOAD_STATE_DONE                   0x08u

#else
/* Parameter for the HCI commands sent during initialization if the patch loading is disabled save some memory because needed buffer is smaller */
uint8_t hciCommandParms[6];
#endif

/** em9301PublicAddress
 * The Public address is by default the EM-Microelectronics Public address
 * If an application need to use public address(es), the addresses must be
 * issued by the IEEE 802-2001. Several devices cannot share and use the
 * same public address.
 * This address is set during the stack initialization, so it can be
 * Overwritten by using BleRadio_SetPublicAddress(BD_ADDR *addr);
 * before the stack initialization.
 *
 * The public address is stored with MSB first, LSB last.
 */
BD_ADDR em9301PublicAddress = { {0x0C, 0xF3, 0xEE, 0x00, 0x00, 0x00}};

#if (BLE_EM9301_LOADPATCH == 1)

/**
 * @brief
 *          This function is called  during the HCI layer initialization
 *          it will send the HCI command to lower layer and responds.
 * @note    BLE_EM9301_LOADPATCH shall be enabled.
 * @retval  The status of the operation:
 *          - BLESTATUS_SUCCESS indicates that the operation have successfully started.
 *          - BLESTATUS_FAILED indicates that the operation has failed to start
 *            it will result to a failure to the BLEMGMT_Init() function.
 */
static void SendCommand(void);
#endif

#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
/** User registrable callback function to get public BLE address. */
static BLE_getPublicAddressCallback_t BLE_getPublicAddress = (BLE_getPublicAddressCallback_t) NULL;

/**
 * Internal flag variable to track validity status of the public BLE address.
 * See BLERADIO_IsPublicAddressValid() for further details.
 */
static uint8_t BLE_isPublicAddressValid = UINT8_C(0);

/**
 * @brief Plausibility test BLE public addresses.
 *
 * The implementation checks the Company ID (OUI) field of the BD_ADDR stored
 * as type BD_ADDR (MSB first, LSB last from left to right).
 *
 * @param [in] address: pointer to valid BLE address to be configured
 *
 * @retval BLESTATUS_FAILED in case address points to a non-conforming BCDS-XDK public BLE address
 * @retval BLESTATUS_SUCCESS in case address points to a BCDS-XDK conforming public BLE address
 */
static BleStatus BLE_checkAddressValidity(BD_ADDR *address);
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

#if (BLE_EM9301_LOADPATCH == 1)
static void SendCommand() {

	if (currentSector < 0) {

#if (BLE_PRINT_DEBUG_TRACES == 1)
		SYSTEM_Log("\r\n");
#endif

		/* We have now finish to load the patch it is now time to exiting ISP mode
		 try to exit in ISP mode by sending HCI command HCI_EM_Write_Data with parameters (address=0x1FFE, data=0x55AA).*/
		patchLoadingState = PATCH_LOAD_STATE_EXITINGISPMODE;

		hciCommandParms[0] = 0xFE;
		hciCommandParms[1] = 0x1F;
		hciCommandParms[2] = 0xAA;
		hciCommandParms[3] = 0x55;

		if (BLEHCI_SendRadioCommand(0xFC00, 4,
						(U8 *) &hciCommandParms[0]) != BLESTATUS_SUCCESS) {
			BLEHCI_RadioIsInitialised( BLESTATUS_FAILED);
		}
	} else {

		hciCommandParms[0] = currentRow;
		hciCommandParms[1] = currentSector;

		SYSTEM_MemCpy(hciCommandParms + 2,
				&EMPatchArray[(ROW_SIZE * currentRow)
				+ ( NUMBER_OF_ROWS * ROW_SIZE * currentSector)],
				ROW_SIZE);

		/* Once Prepared it will be sent */
		if (BLEHCI_SendRadioCommand(0xFC80, 50,
						(U8*) &hciCommandParms[0]) == BLESTATUS_SUCCESS) {
			if (currentRow == 0) {
				currentRow = NUMBER_OF_ROWS - 1;
#if (BLE_PRINT_DEBUG_TRACES == 1)
				SYSTEM_Log("Sector %d done\r\n", currentSector);
#endif
				currentSector--;
			} else {
				currentRow--;
			}
		}
	}
}
#endif

#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
static BleStatus BLE_checkAddressValidity(BD_ADDR *address)
{
    BleStatus resultCode = BLESTATUS_SUCCESS;

    /* test parameters are hard-coded on purpose to BCDS OUI ID of FC-D6-BD */
    if ((0xFC != address->addr[0]) || (0xD6 != address->addr[1]) || (0xBD != address->addr[2]))
    {
        resultCode = BLESTATUS_FAILED;
    }

    return (resultCode);
}
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */

/* global functions ********************************************************* */

/**
 * @brife	Start the RADIO specific initialization
 *			This function is called  during the HCI layer initialization
 *  		( BLEMGMT_Init() )in order to run some specific radio initialization procedures.
 * @note	BLECONTROLLER_NEED_SPECIFIC_INIT shall be enabled.
 * @retval  The status of the operation:
 *			- BLESTATUS_SUCCESS indicates that the operation have successfully started.
 *          - BLESTATUS_FAILED indicates that the operation has failed to start
 *            it will result to a failure to the BLEMGMT_Init() function.
 */
extern BleStatus BLERADIO_Init() {

#if (BLE_EM9301_LOADPATCH == 1)
	/*     If enabled, init the patch Loading state machine
	 start by uploading the last sector and the last row*/
	currentRow = NUMBER_OF_ROWS - 1;
	currentSector = NUMBER_OF_SECTORS - 1;

	patchLoadingState = PATCH_LOAD_STATE_NOTSTARTED;
#endif

	/*	 it is the responsibility of this layer to check
	 that we have (need answer) to radio command if not
	 radio initialization failed

	 send the HCI RESET command, it does not takes any parameters*/
	return (BLEHCI_SendRadioCommand(0x0C03/*BLEHCI_CMD_RESET*/, 0x00, (U8 *) 0));
}

/**
 * @brife	 Handle all the HCI events received during the Radio Initialization
 *           This function handle all the HCI events received by the STACK during
 *           the radio initialization procedure, between BLERADIO_Init and
 *           BLEHCI_RadioIsInitalised
 * @note     BLECONTROLLER_NEED_SPECIFIC_INIT shall be enabled.
 * @param[in]  eventCode: the HCI event OpCode
 * @param[in]  parmsLen: the HCI event Parameter length
 * @param[in]  parms: a pointer to the HCI event Parameter
 */
extern void BLERADIO_HCIEventHandler(U8 eventCode, U8 parmsLen, U8 *parms) {
	U16 opcode;

	if (((eventCode == 0x0E) || (eventCode == 0x10))) {
		/*		 In case of an answer to the HCI_RESET event
		 (HARDWARE ERROR event or COMMAND COMPLETE EVENT)
		 Cancel the timer*/
	}

	UNUSED_PARAMETER(parmsLen);
	switch (eventCode) {
		case 0x0E:
		opcode = LE_TO_U16(parms + 1);
		if (opcode == 0x0C03) { //RESET
			/*	 means reset has been acknowledged*/

#if (BLE_EM9301_LOADPATCH == 1)
			/* depending of the patch loading state  */
			if (patchLoadingState == PATCH_LOAD_STATE_EXITINGISPMODE) {
				/*  we are exited from ISP mode, it is time to continue the init
				 patchLoadingState = PATCH_LOAD_STATE_DONE*/
#endif

#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
                /*
                 * query for the public BLE address through the user provided
                 * callback interface if a callback is provided. The received
                 * address is than passed to the setter interface which only
                 * sets the provided address if it passes plausibility tests.
                 */
                if (BLE_getPublicAddress)
                {
                    BD_ADDR address;

                    BLERADIO_SetPublicAddress((BD_ADDR*) BLE_getPublicAddress(&address));
                }
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */

				hciCommandParms[5] = em9301PublicAddress.addr[0];
				hciCommandParms[4] = em9301PublicAddress.addr[1];
				hciCommandParms[3] = em9301PublicAddress.addr[2];
				hciCommandParms[2] = em9301PublicAddress.addr[3];
				hciCommandParms[1] = em9301PublicAddress.addr[4];
				hciCommandParms[0] = em9301PublicAddress.addr[5];

				if (BLEHCI_SendRadioCommand(0xFC02, 6,
								(U8 *) &hciCommandParms[0]) != BLESTATUS_SUCCESS) {
					BLEHCI_RadioIsInitialised( BLESTATUS_FAILED);
				}
#if (BLE_EM9301_LOADPATCH == 1)
			} else if (patchLoadingState == PATCH_LOAD_STATE_NOTSTARTED) {
				/* we have not yet loading the patch, start the
				 loading procedure now
				 try to enter in ISP mode by sending  HCI command
				 HCI_EM_Write_Data with parameters (address=0x1FFE, data=0x0000).*/
				patchLoadingState = PATCH_LOAD_STATE_ENTERINGISPMODE;

				hciCommandParms[0] = 0xFE;
				hciCommandParms[1] = 0x1F;
				hciCommandParms[2] = 0x00;
				hciCommandParms[3] = 0x00;

				if (BLEHCI_SendRadioCommand(0xFC00, 4,
								(U8 *) &hciCommandParms[0]) != BLESTATUS_SUCCESS) {
					BLEHCI_RadioIsInitialised( BLESTATUS_FAILED);
				}
			} else {
			}
#endif
		}
#if (BLE_EM9301_LOADPATCH == 1)
		else if (opcode == 0xFC00) {
			/* EM_WRITE_DATA To complete the ISP mode entering send a RESET command expected answer =>
			 hardware error event reason 80 To complete the ISP mode exiting send a RESET command
			 expected answer => reset command complete*/
			BLEHCI_SendRadioCommand(0x0C03/*BLEHCI_CMD_RESET*/, 0x00, (U8 *) 0);

		} else if (opcode == 0xFC80) { /* EM_WRITE_PROGRAM */
			SendCommand();

		}
#endif
		else if (opcode == 0xFC02) {
			/* EM_CHANGE_BD_ADDRESS,Reset has been sent successfully, now
			 activate/enable the Transition to BLE Sleep mode*/
			hciCommandParms[0] = 0x01; //0x00 is disable
			if (BLEHCI_SendRadioCommand(0xFC06, 1,
							(U8*) &hciCommandParms[0]) != BLESTATUS_SUCCESS) {
				BLEHCI_RadioIsInitialised( BLESTATUS_FAILED);
			}
		} else if (opcode == 0xFC06) {
			/* EM_POWER_MODE means transition to BLE SLEEP mode is activated this application does not do everything else,
			 however some other RADIO specific operation can be done there like change the TX power level, ...*/
			BLEHCI_RadioIsInitialised( BLESTATUS_SUCCESS);
		}
		break;

		case 0x10: {
			/* reason = *parms; */
#if (BLE_EM9301_LOADPATCH == 1)
			if (*parms == 0x80) {
				/* we are now in ISP mode Patch can be uploaded
				 patchLoadingState = PATCH_LOAD_STATE_LOADING*/
				SendCommand();
			} else {
				/* Another failure */
				BLEHCI_RadioIsInitialised( BLESTATUS_FAILED);
			}
#else
			/* failure during initialization */
			BLEHCI_RadioIsInitialised( BLESTATUS_FAILED );
#endif

		}
		break;
		default:
		break;
	}
}
#endif

/**
 * @brief Set BLE controller's public address.
 *
 * This function is called externally, before the stack initialization, in order
 * to set the value of the public address that the stack sets during the radio
 * initialization. Note that the BLECONTROLLER_NEED_SPECIFIC_INIT configuration
 * setting needs to be enabled in the build configuration specific BLECustomConfig.h
 * file and the public address is loaded during the radio initialization sequence
 * so the API must be called before the BLE chip initialization.
 *
 * @param [in] addr: pointer to valid BLE address to be configured
 */
extern void BLERADIO_SetPublicAddress(BD_ADDR *address)
{
#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
    if (address && (BLESTATUS_SUCCESS == BLE_checkAddressValidity(address)))
    {
        BLE_isPublicAddressValid = UINT8_C(1);
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */
        (void) SYSTEM_MemCpy(&em9301PublicAddress, address, sizeof(BD_ADDR));
#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)
    }
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */
}

#if defined(BLE_PUBLIC_ADDRESS_GETTER_CALLBACK)

/* public interface specification is in BLECustomConfig.h
 *
 * BLERADIO_ prefix is used for consistency with BleRadio.h interface definitions for the radio layer
 */
extern uint8_t BLERADIO_RegisterGetPublicAddressCallback(BLE_getPublicAddressCallback_t getPublicAddress)
{
    BleStatus resultCode = BLESTATUS_INVALID_PARMS;

    if (getPublicAddress)
    {
        BLE_getPublicAddress = getPublicAddress;
        resultCode = BLESTATUS_SUCCESS;
    }

    return ((uint8_t)resultCode);
}

/* public interface specification is in BLECustomConfig.h
 *
 * BLERADIO_ prefix is used for consistency with BleRadio.h interface definitions for the radio layer
 */
extern uint8_t BLERADIO_IsPublicAddressValid(void)
{
    return (BLE_isPublicAddressValid);
}
#endif /* BLE_PUBLIC_ADDRESS_GETTER_CALLBACK */

/** ************************************************************************* */
