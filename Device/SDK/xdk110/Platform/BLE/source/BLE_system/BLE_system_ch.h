/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BLE_SYSTEM_CH_H_
#define BCDS_BLE_SYSTEM_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
/**
 * @brief Enum for Timer priority level setting.
 */
typedef enum BLE_timerPriority_e {
	BLE_TIMERPRIORITY_0 = UINT8_C(0), /**< Timer priority zero */
	BLE_TIMERPRIORITY_1 = UINT8_C(1), /**< Timer priority one */
	BLE_TIMERPRIORITY_MAX /**< Defines the Boundary of the Timer priority */
} BLE_priority_t;

/* local function prototype declarations */

/**
 * @brief
 *     Type for a timer notification callback. The notification function
 *     is called to indicate that the timer has fired.
 *
 */
typedef void (*BLE_timerFunc)(void *);

/**
 *  @brief
 *       callback function for BLE timer handler
 *
 * 	@param[in]   pointer to application_handler
 */
void (*bleTimerAppHandler)(void);
/**
 *  @brief
 *     The BLE_timerIrqHandler API will handle BLE counter value depending on the BLE_TIMER_GRANULARITY define
 *     and handle the Timer init and deinit
 */
static void bleTimerIrqHandler(void);
/**
 *  @brief      Setting up Timer0
 * 	@note    	Timer0 runs continously, if this not required or should not be the case, the
 *			    Init function should be extended to use a switch for SINGLE or LOOP, see RTC.h
 *
 * 	@param[in]  scheduleMilliSec	- After which time in ms the timer should expire
 *
 */
static void bleInitTimer(uint32_t scheduleMilliSec);
/**
 *  @brief
 *     The TIMER0_IRQHandler API will handle the interrupt request of TIMER0.
 *      This is needed to remove TIMER0_IRQHandler in timer.c
 */
void TIMER0_IRQHandler(void);
/**
 *  @brief
 *      Set pointer to function which should be called when timer expires
 * 	@param[in]   pointer to application_handler
 *
 */
static void bleTimerSetIrqHandler(void (*BLE_timerAppHandler)(void));
/**
 *  @brief
 *      Disables timer (i.e. sets the state after POR), required for going to EM2
 */
static void bleTimerDisable(void);
/**
 * @brief
 *    The enableTimer API will enable the timer and Interrupt Flag corresponding to
 *    BLE_TIMER_TYPE. Also enables the interrupt vector in NVIC
 */
static void bleTimerEnable(void);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_BLE_SYSTEM_CH_H_ */

/** ************************************************************************* */
