/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>
#include <stddef.h>
#include <string.h>

/* additional interface header files */
#include "BLE_ph.h"
#include "BLE_serialDriver_ih.h"
#include "em_timer.h"
#include "em_cmu.h"
#include "BleTypes.h"
#include "BleSystem.h"

#include "DBG_assert_ih.h"
#include "FreeRTOS.h"
#include "semphr.h"

#include "TLV_dataHandler_ih.h"
#include "BleMemory.h"

/* own header files */
#include "BLE_system_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/*Timer Counter*/
static uint32_t BLE_timerCount;

/*Timer Top value*/
static uint16_t BLE_timerTop;

/*TIMER initialization structure. */
static TIMER_Init_TypeDef BLE_timerInit;

/*function callback to call when timer has fired*/
BLE_timerFunc BLE_timerFunction;

/*Parameter of the function callback*/
void *BLE_timerCallbackParam;

/* local mutex handles for the BLE stack */
static xSemaphoreHandle BLE_mutex = (xSemaphoreHandle) NULL;
static xSemaphoreHandle BLE_mutexDb = (xSemaphoreHandle) NULL;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 *  @brief
 *      Setting up Timer0
 * 	@note
 *		Timer0 runs continously, if this not required or should not be the case, the
 *		Init function should be extended to use a switch for SINGLE or LOOP, see RTC.h
 *
 * 	@param[in]   scheduleMilliSec	- After which time in ms the timer should expire
 */
static void bleInitTimer(uint32_t scheduleMilliSec) {

	/* Enable clock for TIMER0 module */
	CMU_ClockEnable(BLE_TIMER_CLOCK, true);

	/* Enable main clock i.e. higher frequency for TIMER module */
	CMU_ClockEnable(cmuClock_HFPER, true);

	/* Select TIMER0 parameters */
	BLE_timerInit.enable = false, /* Do not Start counting when init completed */
	BLE_timerInit.debugRun = true, /* Counter shall keep running during debug halt */
	BLE_timerInit.prescale = BLE_TIMER_PRESCALAR_VALUE, /* Prescaling factor, for HFPER clock */
	BLE_timerInit.clkSel = timerClkSelHFPerClk, /* Clock selection - Prescaled HFPER*/
	BLE_timerInit.fallAction = timerInputActionNone, /* Action on falling input edge disabled */
	BLE_timerInit.riseAction = timerInputActionNone, /* Action on rising input edge disabled */
	BLE_timerInit.mode = timerModeUp, /* Counting mode - Up-counting*/
	BLE_timerInit.dmaClrAct = false, /* DMA request clear on active disabled */
	BLE_timerInit.quadModeX4 = false, /* Select X2 quadrature decode mode */
	BLE_timerInit.oneShot = false, /* Determines as only counting up once */
	BLE_timerInit.sync = false, /* Timer start/stop/reload by other timers disabled */

	/* Configure TIMER */
	TIMER_Init(BLE_TIMER_TYPE, &BLE_timerInit);

	// top value calc: SYSTEM_CLOCK_FREQUENCY / BLE_TIMER_CLOCK_PRESCALAR --> result leads to timer interrupt every second, for 250ms value has to be divided by 4
	BLE_timerTop = (uint16_t) (SystemCoreClock/ (BLE_TIMER_CLOCK_PRESCALAR/ BLE_MILLISECOND_CONVERTION * scheduleMilliSec));

	/* Set Top Value */
	TIMER_TopSet(BLE_TIMER_TYPE, BLE_timerTop);

}

/**
 *  @brief
 *     The TIMER0_IRQHandler API will handle the interrupt request of TIMER0.
 *      This is needed to remove TIMER0_IRQHandler in timer.c
 */
void TIMER0_IRQHandler(void) {

	/* Clear flag for TIMER0 overflow interrupt */
	TIMER_IntClear(BLE_TIMER_TYPE, TIMER_IF_OF);

	/* For calculation of top value see above */
	TIMER_TopSet(BLE_TIMER_TYPE, BLE_timerTop);

	(*bleTimerAppHandler)();
}

/**
 *  @brief
 *      Set pointer to function which should be called when timer expires
 * 	@param[in]   pointer to application_handler
 */
static void bleTimerSetIrqHandler(void (*BLE_applicationHandler)(void)) {
	/* Set pointer to timer handler function */
	bleTimerAppHandler = BLE_applicationHandler;
}

/**
 * @brief
 *    The enableTimer API will enable the Interrupt Flag corresponding to
 *    BLE_TIMER_TYPE. Also enables the interrupt vector in NVIC
 *
 */
static void bleTimerEnable(void) {
	/* Start counting when init completed */
	BLE_timerInit.enable = true;

	/* Enable overflow interrupt */
	TIMER_IntEnable(BLE_TIMER_TYPE, TIMER_IF_OF);

	/* Enable TIMER interrupt vector in NVIC */
	NVIC_EnableIRQ(BLE_TIMER_IRQ);

	/* Set priority group to 7 preemption priorities and 1 sub priority */
	NVIC_SetPriorityGrouping(BLE_TIMERPRIORITY_1);

	/* Decrease preempt priority of TIMER IRQ (higher value = lower priority) so that USART IRQ has higher priority */
	NVIC_SetPriority(BLE_TIMER_IRQ, BLE_TIMERPRIORITY_0);

	/* Configure timer */
	TIMER_Init(BLE_TIMER_TYPE, &BLE_timerInit);
}

/**
 *  @brief
 *      Disables timer (i.e. sets the state after POR), required for going to EM2
 */
static void bleTimerDisable(void) {
	/* Disable TIMER interrupt vector in NVIC */
	NVIC_DisableIRQ(BLE_TIMER_IRQ);
	/* Reset TIMER instance */
	TIMER_Reset(BLE_TIMER_TYPE);
}

/**
 *  @brief
 *     The bleTimerIrqHandler API will handle BLE counter value depending on the BLE_TIMER_GRANULARITY define
 *     and handle the Timer init and deinit
 */
static void bleTimerIrqHandler(void) {

	if (BLE_timerCount > BLE_TIMER_GRANULARITY) {

		/* decrease timer counter */
		BLE_timerCount -= BLE_TIMER_GRANULARITY;

		/* re-launch the timer */
		bleInitTimer(BLE_TIMER_GRANULARITY);

	} else {
		BLE_timerCount = BLE_CLEAR;

		/* call the timer function callback */
		BLE_timerFunction(BLE_timerCallbackParam);

		/* Disable TIMER */
		bleTimerDisable();
	}
}

/**
 * Take mutex defined via handle. If handle is NULL, create the mutex first.
 *
 * @param [in] handle : Pointer to mutex handle to take.
 */
static void bleMutexTake(xSemaphoreHandle *handle)
{
    if ((xSemaphoreHandle) NULL == *handle)
    {
        *handle = xSemaphoreCreateMutex();
    }

    if ((xSemaphoreHandle) NULL != *handle)
    {
        int8_t resultCode;

        if (pdTRUE != (resultCode = xSemaphoreTake(*handle, UINT32_MAX)))
        {
            DBG_FAIL("The result must be valid otherwise the semaphore's command"
                "queue size is not selected properly at system configuration time.")
        }
    }
    else
    {
        DBG_FAIL("The mutex handle must be valid at this point otherwise the RTOS"
            " configuration settings are not selected properly.")
    }
}

/**
 * Give mutex defined via handle.
 *
 * @param [in] handle : Pointer to mutex handle to give.
 */
static void bleMutexGive(xSemaphoreHandle *handle)
{
    if ((*handle) && (pdTRUE != xSemaphoreGive(*handle)))
    {
        DBG_FAIL("The result must be valid otherwise the semaphore's command"
            "queue size is not selected properly at system configuration time.")
    }
}

/* global functions ********************************************************* */

/*  Synchronisation Operation */
#if (BLE_STACK_SKIP_MUTEX_FUNCTIONS == BLE_DISABLE)

/**
 * @brief    Lock the stack mutex
 *			 The BLE stack uses a MUTEX to prevent internal interface reentrency and
 *			 dead lock, the BLE stack calls this function to acquire the MUTEX
 *			 this function SHALL be implemented.
 * @note	 It could be implemented using a mutex or perhaps a semaphore
 */
void SYSTEM_StackMutexPend(void)
{
    bleMutexTake(&BLE_mutex);
}

/**
 * @brief   Unlock the stack mutex.
 *	        The BLE stack uses a MUTEX to prevent internal interface reentrency and
 *	        dead lock, the BLE stack calls this function to acquire the MUTEX
 *	        this function SHALL be implemented.
 * @note    It could be implemented using a mutex or perhaps a semaphore
 */
void SYSTEM_StackMutexPost(void)
{
    bleMutexGive(&BLE_mutex);
}

/**
 * @brief   Lock the Attribute database mutex.
 *		    The BLE stack uses a MUTEX to prevent internal/external multiple access
 *			to the Attribute protocol database, the BLE stack calls this function to
 *  		acquire the MUTEX. This function SHALL be implemented.
 *  Note that the User application does not call this function directly, but
 *  call the application interface ATT_Server_SecureDatabaseAccess().
 *
 *	It could be implemented using a mutex or perhaps a semaphore
 */
void SYSTEM_DatabaseMutexPend(void)
{
    bleMutexTake(&BLE_mutexDb);
}
/**
 * @brief Unlock the stack mutex.
 *		  The BLE stack uses a MUTEX to prevent internal/external multiple access
 *		  to the Attribute protocol database, the BLE stack calls this function to
 * 		  release the MUTEX. This function SHALL be implemented.
 *  	  Note that the User application does not call this function directly, but
 *  	  call the application interface ATT_Server_ReleaseDatabaseAccess().
 * @note  It could be implemented using a mutex or perhaps a semaphore
 */
void SYSTEM_DatabaseMutexPost(void)
{
    bleMutexGive(&BLE_mutexDb);
}
#endif //(BLE_STACK_SKIP_MUTEX_FUNCTIONS == 0)/*    Timer Operation       *//**
 * @brief 	The stack request a timer creation
 *			The BLE stack need one timer in order to calulates timeout or wachdog.
 *  		It uses this function to create the timer, timers are internally
 * 			 multiplexed, so it need only one.
 * @param[in] func : pointer to a function to call when the timer expires
 * @param[in]  param : param to pass in func when the timer expire
 * @param[in]  millisecond : time to wait before the timer expiration
 */
extern void SYSTEM_CreateTimer(void *func, void *param, U32 milliSecond) {

	/* save the parameters */
	BLE_timerCount = milliSecond;
	BLE_timerFunction = (BLE_timerFunc) func;
	BLE_timerCallbackParam = param;

	/* Init Timer0 */
	bleInitTimer(BLE_TIMER_GRANULARITY);

	/*  Set pointer to function which should be called when timer expires */
	bleTimerSetIrqHandler(bleTimerIrqHandler);

	/* Enabling the Ble Timer */
	bleTimerEnable();
}

/**
 * @brief	 The stack request the timer remainting time
 *			 The BLE stack need the remaining time of the timer created by
 * 			 SYSTEM_CreateTimer() in order to do some internal things
 * @retval   Time remaining in millisecond.
 */
extern U32 SYSTEM_TimerGetRemain(void) {
	return (BLE_timerCount);
}

/**
 * @brief	The stack request its timer destruction
 *			The BLE stack no more need timer. it request destruction of the timer
 * 			created with SYSTEM_CreateTimer().
 */
extern void SYSTEM_CancelTimer(void) {

	/* Disable TIMER */
	bleTimerDisable();
	/*Set the timerCount zero*/
	BLE_timerCount = BLE_CLEAR;

}

/*	 Security Operation    */
#if (BLE_SECURITY == BLE_ENABLE)
BLE_memoryTableDescriptor_tp bleMemoryManagerHandle = (BLE_memoryTableDescriptor_tp) NULL;

/** The stack request the system to store an information in persistent memory
 *
 * SYSTEM_SetPersistentInformation()
 *	The BLE stack need to save a specific security information with a specified remote
 * device in the persistent memory.
 *
 * @param addr : the remote address for which security information should be stored,
 *			IMPORTANT NOTE: if BD_ADDR is 00:00:00:00:00:00, all the entry of the
 *			persistent memory shall be updated.
 * @param infoType : the type of the information to store
 * @param infoValue : a valid pointer to the information to save
 * @param infoLen : the length the information to save
 *
 * @return the status of the operation
 *
 * @author Alexandre GIMARD
 */
BleStatus SYSTEM_SetPersistentInformation(BD_ADDR *addr, U8 infoType, U8 *infoValue, U8 InfoLen)
{
    BleStatus status = BLESTATUS_SUCCESS;

    if ((BLE_memoryTableDescriptor_tp) NULL == bleMemoryManagerHandle)
    {
        /* initialize memory manager */
        bleMemoryManagerHandle = BLE_memoryInit();
    }

    /* test whether every group have to be updated */
    if (BLE_isBdAddressValid(addr))
    {
        BLE_memoryDescriptor_tp device;

        /* update or add new TLV to corresponding group */
        device = BLE_getDeviceHandleByAddress(bleMemoryManagerHandle, addr);

        if (!device)
        {
            /* device not found, add it */
            device = BLE_addDevice(bleMemoryManagerHandle, addr);
        }

        if (device)
        {
            TLV_groupHandle_t tlvHandle;

            /* add TLV to device */
            tlvHandle = BLE_getDeviceTlvDatabase(device);

            if (!TLV_addElement(tlvHandle, infoType, InfoLen, infoValue))
            {
                status = BLESTATUS_FAILED;
            }
        }
        else
        {
            status = BLESTATUS_FAILED;
        }
    }
    else
    {
        uint8_t index, size;

        /* update existing TLVs in every group */
        size = BLE_getDeviceDatabaseSize(bleMemoryManagerHandle);

        for (index = 0; index < size; index++)
        {
            BLE_memoryDescriptor_tp device;
            TLV_groupHandle_t tlvHandle;

            device = BLE_getDeviceHandleByIndex(bleMemoryManagerHandle, index);

            /* add TLV to device */
            tlvHandle = BLE_getDeviceTlvDatabase(device);

            if (!TLV_addElement(tlvHandle, infoType, InfoLen, infoValue))
            {
                status = BLESTATUS_FAILED;
                break;
            }
        }
    }

    return (status);
}

/** The stack request the system to retrieve an information from persistent memory
 *
 * SYSTEM_GetPersistentInformation()
 *	The BLE stack need to retrieve a specific security information
 *	from the persistent memory.
 * Important note: The persistent information SHALL have been previously set by
 * SYSTEM_SetPersistentInformation() API, if the information has been never set this
 * function shall answer BLESTATUS_FAILED.
 *
 * @param addr : the remote address for which security information shall be retrieved
 * @param infoType : the type of the information to retrieve
 * @param infoValue : the value of the information retrieved
 * @param infoLen : a the length the information retrieved
 *
 * @return the status of the operation
 *
 * @author Alexandre GIMARD
 */
BleStatus SYSTEM_GetPersistentInformation(BD_ADDR *addr, U8 infoType, U8 **infoValue, U8 *infoLen)
{
    if ((BLE_memoryTableDescriptor_tp) NULL == bleMemoryManagerHandle)
    {
        return (BLESTATUS_FAILED);
    }

    {
        BleStatus status;
        BLE_memoryDescriptor_tp device;

        device = BLE_getDeviceHandleByAddress(bleMemoryManagerHandle, addr);

        if (!device)
        {
            status = BLESTATUS_FAILED;
        }
        else
        {
            TLV_element_t* tlvElement;

            /* get TLV for device */
            tlvElement = TLV_getElement(BLE_getDeviceTlvDatabase(device), infoType);

            if (tlvElement)
            {
                (void) memcpy(*infoValue, tlvElement->dataBuffer, tlvElement->dataLength);
                *infoLen = (uint8_t) tlvElement->dataLength;

                status = BLESTATUS_SUCCESS;
            }
            else
            {
                status = BLESTATUS_FAILED;
            }
        }

        return (status);
    }
}

/** The stack request the system to remove an information from persistent memory
 *
 * SYSTEM_RemovePersistentInformation()
 *	 The BLE stack need to remove a specific persistent information
 *	from the persistent memory. It request the remove a range a infotype
 *   The stack call this function when the Application wants to unbond with a
 *  remote device or when it detect that profile information is no more valid
 *   Also, an application may delete the linkUp information for a given
 *  profile by calling this function.
 *
 *  addr :  A valid pointer to the remote address for which persistent
 *		    information shall be remove
 *  infoTypeFrom : the starting range of the infoType to delete, this
 *          infoTypeFrom is included into the range to delete
 *  infoTypeTo : the ending range of the infoType to delete, this
 *          infoTypeTo is included into the range to delete
 *
 * return the status of the operation
 */
BleStatus SYSTEM_RemovePersistentInformation(BD_ADDR *addr, U8 infoTypeFrom, U8 infoTypeTo)
{
    if ((BLE_memoryTableDescriptor_tp) NULL == bleMemoryManagerHandle)
    {
        return (BLESTATUS_SUCCESS);
    }

    {
        BLE_memoryDescriptor_tp device;

        device = BLE_getDeviceHandleByAddress(bleMemoryManagerHandle, addr);

        if (device)
        {
            uint8_t infoType;

            /* remove TLVs in range */
            for (infoType = infoTypeFrom; infoType <= infoTypeTo; infoType++)
            {
                TLV_removeElement(BLE_getDeviceTlvDatabase(device), infoType);
            }
        }
    }

    return (BLESTATUS_SUCCESS);
}
#endif

/** ************************************************************************* */
