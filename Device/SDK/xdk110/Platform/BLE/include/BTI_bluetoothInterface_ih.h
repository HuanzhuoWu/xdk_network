/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BTI_BLUETOOTHINTERFACE_IH_H_
#define BCDS_BTI_BLUETOOTHINTERFACE_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/** enum values representing the status of the execution */
enum bluetoothIF_retCode_E
{
	BluetoothIF_FAILURE,
	BluetoothIF_SUCCESS,
	BluetoothIF_INVALID_PARAMETER,
	BluetoothIF_BUFFER_FAULT,
};


typedef enum bluetoothIF_retCode_E bluetoothIF_retCode_T;

/** @brief Callback function signature to be called on data reception */
typedef void (*BTI_callBackOnDataRead)(uint8_t *rxBuffer, uint8_t rxDataLength);

/* public function prototype declarations */

/**
 * @brief
 * BTI_init() initializes the BLE stack, creates the OS task and
 * registers the data receive function for AlpwiseDataExchange profile.
 */
extern void BTI_init(BTI_callBackOnDataRead callBackOnDataRead);

/**
 * @brief   To transmit the BLE data into Target device.
 * @param[in]   dataToSend              The payload to be sent
 * @param[in]   dataToSendLen           The length of the payload to be sent
 * @retval  The status of the operation:
 *              - 1 indicates that the operation is successful
 *              - 0 indicates that the BLE stack could not be
 *                initialized
 */
extern bluetoothIF_retCode_T BTI_sendData(uint8_t* dataToSend, uint8_t dataToSendLen);

/**
 * @brief   This API would make the BLE module to enter sleep
 * and suspend the BLE task.
 *
 */
extern void BTI_deviceSleep(void);

/**
 * @brief   This API would resume the BLE task and
 * make the BLE module to wake up.
 *
 */
extern void BTI_deviceWakeUp(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_BTI_BLUETOOTHINTERFACE_IH_H_ */

/** ************************************************************************* */
