/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_BLE_SERIALDRIVER_IH_H_
#define BCDS_BLE_SERIALDRIVER_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* Transmit Buffer size for USART Ringbuffer */
#define BLE_TX_BUFFER_SIZE          	(UINT8_C(60))

/* Receive Buffer size for USART Ringbuffer */
#define BLE_RX_BUFFER_SIZE         		(UINT8_C(60))

/* Hardware Timer Define for BLE AlpwiseSystem */
/** the timer granularity
 *  We no need to much precision, so 1000 ms of granularity is not so bad
 *  greater is the granularity, lower is the number of interruption
 */
#define BLE_TIMER_GRANULARITY 				(UINT16_C(1000))
/** This is the Timer Prescaler Factor value whose suffix can be substituted as 0,1,2,4,8,16,32,64,128,256,512 or 1024.
 * Update the same value for BLE_TIMER_CLOCK_PRESCALAR in the Configuration Header of the BLE_alpwiseSystem_cc.c file to
 * have a proper calculation of time for one second breath mode, when ever this value is altered here */
#define BLE_TIMER_PRESCALAR_VALUE			(timerPrescale1024)
/**  Provides the Timer Prescaler value */
#define BLE_TIMER_CLOCK_PRESCALAR			(UINT16_C(1024))
/**  Provides the millisecond multipler for Top value setting  */
#define BLE_MILLISECOND_CONVERTION			(UINT16_C(1000))
/** Enabling the particular functionality/general use for avoid magic value or constant */
#define BLE_ENABLE							(UINT8_C(1))
/** Disabling the particular functionality/general use for avoid magic value or constant */
#define BLE_DISABLE							(UINT8_C(0))
/** Setting the particular functionality/general use for avoid magic value or constant */
#define BLE_SET								(UINT8_C(1))
/** Clearing the particular functionality/general use for avoid magic value or constant */
#define BLE_CLEAR							(UINT8_C(0))
/**
 *   BLE_return enum have the different return value
 *   supported by Bluetooth Low Energy Module
 */
typedef enum BLE_return_e {
	BLE_STATUS_SUCCESS =  UINT8_C(0),
	BLE_STATUS_FAILED =  UINT8_C(1),
	BLE_STATUS_INVALID_PARMS =  UINT8_C(2)
} BLE_return_t;
/* public global variable declarations */

/* public function prototype declarations */
/**
 *  @brief   This function is called by the BLE stack to Initialize the HCI layer (TRANSPORT layer) and This function is called
 *           internally during the BLEMGMT_Init() process, failure here will issue a failure in BLEMGMT_Init()
 * @retval   BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciDriverInit(void);

/**
 *  @brief      This function is called by the BLE stack to De-Initialize the HCI layer (TRANSPORT layer).
 *              Eventual thread shall be terminated here.
 */
extern void BLE_hciDriverDeinit(void);

/**
 *  @brief     This function is called by the BLE stack to send data through the HCI layer(TRANSPORT layer).
 *  @param[in] transmitBuffer : pointer to send data
 *  @param[in] transmitLength : length of data to send contained in the data pointer
 *  @retval    BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciSendData(uint8_t* transmitBuffer, uint8_t transmitLength);

/** This function used to check any Event/message received from BLE centralized device.
 *  @brief    This function used to check any Event/message received from BLE.if it's received then,
 *            read BLE Event/message from serial ring buffer and then send to the CoreStack
 *  @retval   BLE_return_t - return the success or the error type
 */
extern BLE_return_t BLE_hciReceiveData(void);

/**
 *  @brief This function is de-asserts the wake up pin of EM9301
 *  to allow the BLE device to move to Sleep or Deep-sleep mode.
 */
extern void BLE_hciDeassertWakeupPin(void);

/**
 *  @brief This function is asserts the wake up pin of EM9301
 *  to wake up the BLE device from Sleep or Deep-sleep mode.
 */
extern void BLE_hciAssertWakeupPin(void);

#endif /* BCDS_BLE_SERIALDRIVER_IH_H_ */

/** ************************************************************************* */
