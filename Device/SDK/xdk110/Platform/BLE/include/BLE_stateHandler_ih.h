/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 /**
 * @defgroup BLEStateHandler BLE State Manager
 * @brief BLE State Manager Interface
 * @ingroup BLE
 *
 * @{
 * @brief  Module to set the debug log levels
 *
 *  @details The interface header contain the following features:
 *  						- BLE_coreStackInit()
 *  						- BLE_coreStateMachine()
 *                          - BLE_bleSendData()
 *                          - BLE_readyToGoToSleep()
 * ***************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_BLE_STATEHANDLER__IH_H_
#define BCDS_BLE_STATEHANDLER__IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */
typedef uint8_t BLE_appStateReturn, BLE_status;

#define BLE_commandParamSize	UINT8_C(1)  /**< Size of BLE command parameters */

typedef enum BLE_SleepType_e			/**< To define the possible sleep modes of the BLE */
{
    BLE_ST_NORMAL_SLEEP = UINT8_C(0), /**< Normal Sleep Mode (20 uA)*/
    BLE_ST_DEEP_SLEEP   = UINT8_C(1), /**< Deep Sleep Mode  (9 uA)*/
    BLE_ST_MAX
} BLE_SleepType_t;

/** enum representing the enum values for Bluetooth device states */
typedef enum BLE_configureMode_e
{
	BLE_IDLE, 						/**<  enable idle state*/
	BLE_DISCOVERABLE,				/**<  enable  Discoverable and connectable*/
	BLE_NORMAL_SLEEP,				/**<  enable normal sleep mode */
	BLE_DEEP_SLEEP,                 /**<  enable seep sleep mode*/
}BLE_configureMode_t;

/** enum representing the enum values for Bluetooth device states */
typedef enum BLE_deviceState_e
{
	BLE_DEVICE_IN_IDLE, 			/**< device is in idle state*/
	BLE_DEVICE_IN_DISCOVERABLE,		/**< device is in Discoverable and connectable state*/
	BLE_DEVICE_IN_SLEEP,			/**< device is in sleep state*/
	BLE_DEVICE_IN_CONNECTED,		/**< device is in connected state*/
}BLE_deviceState_t;

/** enum values representing the status of the execution */
typedef enum BLE_returnStatus_e
{
	BLE_FAILURE,
	BLE_SUCCESS,
	BLE_INVALID_PARAMETER,
	BLE_INVALID_DEVICE_NAME_LENGTH,
}BLE_returnStatus_t;

/** enum values representing the status of the execution */
typedef enum BLE_enableNotification_e
{
	BLE_DISABLE_NOTIFICATION,      /**< disable device connect or disconnect notification*/
	BLE_ENABLE_NOTIFICATION,	   /**< enable device connect or disconnect notification*/
}BLE_enableNotification_t;

/**  enum represents the device connection status*/
typedef enum BLE_connectionStatus_e
{
	BLE_CONNECTED_TO_DEVICE,       /**< device is connected */
	BLE_DISCONNECTED_FROM_DEVICE,  /**< device is  disconnect */
}BLE_connectionStatus_t;

/** structure to hold the remote device address */
typedef struct BLE_bdrAddr_s {
   uint8_t   addr[6];
} BLE_bdrAddr_t;

/** structure to hold the device connection details */
typedef struct BLE_connectionDetails_s
{
	BLE_connectionStatus_t connectionStatus;      /**< device connection status(device connected or disconnected to.from the remote device)*/
	BLE_bdrAddr_t remoteAddress; 				  /**<The remote device address*/
}BLE_connectionDetails_t;

/**
 * @brief 	callback function to notify the user with the connection status and the connected remote device address
 *
 * @param[out] connectionDetails  refer structure BLE_connectionDetails_t
 */
typedef void (*BLE_notificationCallback)(BLE_connectionDetails_t connectionDetails);

/** structure to hold the details of enable/disable feature  and the callback function used for notification of device connect/disconnect */
typedef struct BLE_notification_s
{
	BLE_enableNotification_t enableNotification;

	BLE_notificationCallback callback;

}BLE_notification_t, *BLE_notification_tp;
/* public function prototype declarations */

/**
 * @ingroup BLE_StateHandler
 * @brief 	Initialise the whole BLE stack, including transport, radio ...
 * 			It initializes:
 *  			- Some global context and internal states.
 *  			- The transport layer
 *  			- The radio layer
 * 			    - The Core layer (BLE-HCI, L2CAP...)
 * @retval     The status of the operation:
 *  			- BLESTATUS_SUCCESS indicates that the operation is successfull
 * 				- BLESTATUS_FAILED indicates that the BLE stack could not be
 *      		  initialized
 */
extern BLE_status BLE_coreStackInit(void);

/**
 *  @ingroup BLE_StateHandler
 *  @brief   This function is used to run the BLE stack and register a BLE device with the specified role(i.e BROADCASTER,OBSERVER,PERIPHERAL,.),BLE Device Name.
 *           It registers,
 *             - GAP/GATT protocols
 *             - BLE profile (services and characteristic's attribute )
 *             - Establish the connection or advertise to the Remote device
 *             - Update the BLE device connection status i.e Connect/Disconnect
 *           If BLE State process failed due to some reason, then BLE Stack will be Re-initialize automatically.
 * @retval     The status of the operation:
 *  			- BLESTATUS_SUCCESS indicates that the operation is successfull
 * 				- BLESTATUS_FAILED indicates that the BLE stack could not be
 *      		  initialized
 */
extern BLE_appStateReturn BLE_coreStateMachine(void);

/**
 * @ingroup BLE_StateHandler
 * @brief     The stack itself request to be executed once
 *            allow the system to go into low power mode when the SDK have nothing to do
 *            especially the APPREQUEST_NOTIFYTORUN
 */
extern BLE_status BLE_readyToGoToSleep(void);

/**
 * @brief   This API used to wakeup the BLE device from Sleep mode.
 */
void BLE_wakeUpDevice(void);

/**
 * @ingroup BLE_StateHandler
 * @brief   This API used to set the BLE into the desired sleep mode.
 *
 * @param[in]   BLE_SleepType_t - the target sleep mode
 */
void BLE_setDeviceToSleep(BLE_SleepType_t sleepType);

/**
 * BleServiceRegistryCallBack
 *	This callback receives BLE service's from application layer
 *	and registered during the BLE profile Init.
 */
typedef void (*BleServiceRegistryCallBack)(void);

/**
 * @ingroup BLE_StateHandler
 * @brief       This API used to register application call back
 *              with service profile registry.
 *
 * @param[in]   bleCustomService - function pointer
 *
 * @retval  The status of the operation:
 *  			- BLESTATUS_SUCCESS indicates that the operation is successful
 * 				- BLESTATUS_FAILED indicates that the BLE service mapping id not proper
 */
extern BLE_status BLE_customServiceRegistry(BleServiceRegistryCallBack bleCustomService);

/**
 * @brief       This function used to set mode of Bluetooth Low Energy Device
 *
 * @param[in]   modeToSet   Mode to configure , refer BLE_mode_t for available modes
 *
 * @retval    status of the execution
 *
 */
extern BLE_returnStatus_t BLE_setMode(BLE_configureMode_t modeToSet);

/**
 * @brief       This function used to get the mode of Bluetooth Low Energy Device
 *
 * @param[out]   modeConfigured   the mode of the Bluetooth Device.
 *
 * @retval    status of the execution
 *
 */
extern BLE_returnStatus_t BLE_getMode(BLE_deviceState_t *modeConfigured);

/**
 * @brief       This function used to register the callback function device connection and disconnection notification
 * 				and to enable/disable the notification feature
 *
 * @param[in]   configParams   A valid pointer to the Bluetooth device friendly name.
 *
 * @retval      status of the execution
 *
 * @note      the callback function should be minimal size
 */
extern BLE_returnStatus_t BLE_enablenotificationForConnect(BLE_notification_t configParams);

/**
 * @brief       This function used to set Bluetooth Low Energy Device friendly name
 *
 * @param[in]   deviceName   A valid pointer to the Bluetooth device friendly name.
 *
 * @retval      status of the execution
 *
 * @note         API is used before profile initializations and the supported length of the device
 *               name is 20 bytes
 */
extern BLE_returnStatus_t BLE_setDeviceName(uint8_t *deviceName, uint8_t length);

/**
 * @brief   To transmit the BLE data into Target device.
 * @param[in]   dataToSend              The payload to be sent
 * @param[in]   dataToSendLen           The length of the payload to be sent
 * @retval  The status of the operation:
 *              - BLE_SUCCESS indicates that the operation is successful
 *              - BLE_FAILURE indicates that the BLE stack could not be
 *                initialized
 */
extern BLE_returnStatus_t BLE_sendData(uint8_t* dataToSend, uint8_t dataToSendLen);

/* public global variable declarations */

/* inline function definitions */

/** @}*/

#endif /* BCDS_BLE_STATEHANDLER__IH_H_ */
/** ************************************************************************* */
