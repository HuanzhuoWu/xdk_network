##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################
include Settings.mk

# Build chain settings
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif
LINT_EXE = lint-nt.launch

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra -Wstrict-prototypes\
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections -D$(BCDS_DEVICE_ID) -DBLE_DEVICENAME=\"$(BCDS_UNIQUE_NAME)\" -DUNIQUE_MAC=$(BCDS_UNIQUE_MAC)
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -DBCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID)
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

LINTFLAGS = \
           -immediatetrans \
           -usereleased \
           -declundef \
	   		-zero
           
# Source and object files
BCDS_BLE_SOURCE_FILES += \
	$(BCDS_BLE_SOURCE_PATH)/BLE_serialDriver/BLE_serialDriver_cc.c \
	$(BCDS_BLE_SOURCE_PATH)/BTI_bluetoothInterface/BTI_bluetoothInterface_cc.c \
	$(BCDS_BLE_SOURCE_PATH)/BLE_hostInterface/BLE_hostInterface_cc.c \
	$(BCDS_BLE_SOURCE_PATH)/BLE_radioInterface/BLE_radioInterface_cc.c \
	$(BCDS_BLE_SOURCE_PATH)/BLE_stateHandler/BLE_stateHandler_cc.c \
	$(BCDS_BLE_SOURCE_PATH)/BleMemory/BleMemory.c \
	$(BCDS_BLE_SOURCE_PATH)/BLE_system/BLE_system_cc.c

BCDS_BLE_OBJECT_FILES = \
	$(patsubst $(BCDS_BLE_SOURCE_PATH)/%.c, %.o, $(BCDS_BLE_SOURCE_FILES))

BCDS_BLE_LINT_FILES = \
	$(patsubst $(BCDS_BLE_SOURCE_PATH)/%.c, %.lob, $(BCDS_BLE_SOURCE_FILES))

BCDS_BLE_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_BLE_DEBUG_OBJECT_PATH)/,$(BCDS_BLE_OBJECT_FILES))

BCDS_BLE_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_BLE_DEBUG_OBJECT_PATH)/, $(BCDS_BLE_OBJECT_FILES:.o=.d))	
	
BCDS_BLE_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_BLE_RELEASE_OBJECT_PATH)/,$(BCDS_BLE_OBJECT_FILES))

BCDS_BLE_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_BLE_RELEASE_OBJECT_PATH)/, $(BCDS_BLE_OBJECT_FILES:.o=.d))	
	
BCDS_BLE_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_BLE_DEBUG_LINT_PATH)/, $(BCDS_BLE_LINT_FILES))

######################## Build Targets #######################################

.PHONY: debug
debug: $(BCDS_BLE_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_BLE_RELEASE_LIB)

############# Generate BLE Library for BCDS ###################
$(BCDS_BLE_DEBUG_LIB): $(BCDS_BLE_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BLE for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BLE_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_BLE_RELEASE_LIB): $(BCDS_BLE_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build BLE for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_BLE_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"
	
# compile C files
$(BCDS_BLE_DEBUG_OBJECT_PATH)/%.o: $(BCDS_BLE_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
	       -I . $(BCDS_BLE_INCLUDES) $< -o $@

$(BCDS_BLE_RELEASE_OBJECT_PATH)/%.o: $(BCDS_BLE_SOURCE_PATH)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_BLE_INCLUDES) $< -o $@

.PHONY: lint		   
lint: $(BCDS_BLE_DEBUG_LINT_FILES)
	@echo "Lint End"
	
$(BCDS_BLE_DEBUG_LINT_PATH)/%.lob: $(BCDS_BLE_SOURCE_PATH)/%.c
	@echo "===================================="
	@mkdir -p $(@D)
	@echo $@
	@$(LINT_EXE) +d$(DEVICE)=1 $(LINTFLAGS) $(BCDS_LINT_CONFIG_FILE) $(BCDS_EXTERNAL_INCLUDES_LINT) $< -oo >> $@
	@echo "===================================="
	
-include $(BCDS_BLE_DEBUG_DEPENDENCY_FILES) $(BCDS_BLE_RELEASE_DEPENDENCY_FILES)

.PHONY: clean	
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_BLE_DEBUG_PATH) $(BCDS_BLE_RELEASE_PATH)

.PHONY: cleanlint	
cleanlint:
	@echo "Cleaning lint output files"
	@rm -rf $(BCDS_BLE_DEBUG_LINT_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_BLE_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_BLE_DEBUG_PATH)
	@echo $(BCDS_BLE_DEBUG_OBJECT_FILES)
