/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    StepCounter Step Counter
 *  @ingroup     Virtual_sensors_group
 *  @{
 *  @brief Virtual Step Counter Sensor based on BMI160
 *  @details
 *  Step counter is used to calculate the number of steps by detecting the movement of the user. \n
 *
 *  The interface header exports the following features
 *                                              - StepCounter_init()
 *                                              - StepCounter_setMode()
 *                                              - StepCounter_clear()
 *                                              - StepCounter_read()
 *                                              - StepCounter_deInit()
 *
 * @note
 * When counting steps, the update is done after a certain count in the beginning and also when moved from idle position. \n -
 * STEP_COUNTER_NORMAL_MODE has update delay of 7 steps \n -
 * STEP_COUNTER_SENSITIVE_MODE has update delay of 4 steps \n -
 * STEP_COUNTER_ROBUST_MODE has update delay of 11 steps
 *
 * \b Limitations \b of \b stepcounter \b : \n
 * \t This sensor cannot be used while using virtual sensor fingerPrint
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_STEP_COUNTER_H_
#define BCDS_STEP_COUNTER_H_

#include "BCDS_Retcode.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/** Defines the handle for Step Counter sensor */
typedef void* StepCounter_HandlePtr_T;

/** Enum values that represent step counter modes */
enum StepCounter_Mode_E
{
    STEP_COUNTER_NORMAL_MODE, /**< Step counter NORMAL mode */
    STEP_COUNTER_SENSITIVE_MODE, /**< Step counter SENSITIVE mode */
    STEP_COUNTER_ROBUST_MODE, /**< Step counter ROBUST mode */
    STEP_COUNTER_INVALID_MODE /**<This Specifies maximum number of enum elements and it is used only for error handling */
};
typedef enum StepCounter_Mode_E StepCounter_Mode_T;

/* public function prototype declarations */

/**
 * @brief Initializes the Step Counter sensor.This API should return
 * RETCODE_OK in order to communicate with sensor.This function needs to be called before calling any other function of the Step counter sensor api.
 *
 * @param [in] StepCounter_HandlePtr_T the handle to the Step Counter object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T StepCounter_init(StepCounter_HandlePtr_T handle);

/**
 *
 * @brief Set Step Counter Mode \n
 * This sensor allows to configure the mode of the step counter.
 *
 * @param [in] StepCounter_HandlePtr_T the handle to the Step Counter object
 *
 * @param [in] stepCounterMode value of Step counter mode
 *
 *  value    |   mode
 * ----------|---------------------
 *   0       | STEP_COUNTER_NORMAL_MODE        (default setting, recommended for most applications)
 *   1       | STEP_COUNTER_SENSITIVE_MODE     (can be used for light weighted, small persons)
 *   2       | STEP_COUNTER_ROBUST_MODE        (can be used, if many false positive detections are observed)  \n
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T StepCounter_setMode(StepCounter_HandlePtr_T handle, StepCounter_Mode_T stepCounterMode);

/**
 *
 * @brief Clear Step Counter value (Resets the step counts calculated)
 *
 * @param [in] StepCounter_HandlePtr_T the handle to the Step Counter object
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T StepCounter_clear(StepCounter_HandlePtr_T handle);

/**
 *
 * @brief Read step count value (Provides the number of user steps)
 *
 * @param [in] StepCounter_HandlePtr_T the handle to the Step Counter object
 *
 * @param [out] stepCounterValue value of Step count
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T StepCounter_read(StepCounter_HandlePtr_T handle, int16_t * stepCounterValue);

/**
 * @brief API to Deinitialise the step counter module
 *
 * @param [in] StepCounter_HandlePtr_T the handle to the Step Counter object which is to be deinitialised
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T StepCounter_deInit(StepCounter_HandlePtr_T handle);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_STEP_COUNTER_H_ */

/**@} */
