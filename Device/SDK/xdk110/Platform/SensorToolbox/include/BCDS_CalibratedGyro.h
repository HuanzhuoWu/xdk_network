/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    CalibratedGyroscope Calibrated Gyroscope
 *  @ingroup     Virtual_sensors_group
 *
 *  @{
 *  @brief Virtual Calibrated Gyroscope Sensor based on BMI160
 *  @details
 *  Calibrated Gyroscope is a virtual sensor that provides calibrated gyroscope data. \n i.e.,Zero-rate offsets are removed from
 *  the angular data.
 *
 *  The interface header exports the following features
 *  								            - CalibratedGyro_init()
 *  											- CalibratedGyro_readXyzLsbValue()
 *  											- CalibratedGyro_readXyzRpsValue()
 *  											- CalibratedGyro_readXyzDpsValue()
 *  											- CalibratedGyro_getStatus()
 *  											- CalibratedGyro_deInit()
 *
 * \b Gyroscope \b Calibration \b method : \n
 *   Gyroscope calibration is done by leaving the device in flat position a few seconds.
 *
 * Calibrated Gyroscope data is read in three different units.
 *     - LSB units
 *     - rad/sec units
 *     - deg/sec units
 *
 * @note - To know the accuracy of calibration done, use CalibratedGyro_getStatus() API \n -
 * To get proper calibrated gyroscope data, make sure that the calibration accuracy
 * is CALIBRATED_GYRO_HIGH before reading the data.
 *
 * \b Limitations \b of \b calibrated \b gyroscope: \n
 * This sensor cannot be used while using virtual sensor fingerPrint.
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_CALIBRATED_GYRO_H_
#define BCDS_CALIBRATED_GYRO_H_

/* public interface declaration ********************************************** */
/* System Header ************************************************************* */
#include <stdint.h>

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/** custom data types for calibrated Gyroscope data */
typedef int32_t CalibratedGyro_LsbData_T; /**< data type for calibrated Gyroscope lsb data */
typedef float CalibratedGyro_RpsData_T; /**< data type for calibrated Gyroscope rad/sec data */
typedef float CalibratedGyro_DpsData_T; /**< data type for calibrated Gyroscope deg/sec data */

/**
 * Struct that holds calibrated Gyroscope data in lsb units
 */
struct CalibratedGyro_XyzLsbData_S
{
    CalibratedGyro_LsbData_T xAxisData; /**< calibrated Gyroscope x-axis lsb data  */
    CalibratedGyro_LsbData_T yAxisData; /**< calibrated Gyroscope y-axis lsb data  */
    CalibratedGyro_LsbData_T zAxisData; /**< calibrated Gyroscope z-axis lsb data  */
};
typedef struct CalibratedGyro_XyzLsbData_S CalibratedGyro_XyzLsbData_T, *CalibratedGyro_XyzLsbDataPtr_T;

/**
 * Struct that holds calibrated Gyroscope data in rad/sec units
 */
struct CalibratedGyro_XyzRpsData_S
{
    CalibratedGyro_RpsData_T xAxisData; /**< calibrated Gyroscope x-axis rad/sec data  */
    CalibratedGyro_RpsData_T yAxisData; /**< calibrated Gyroscope y-axis rad/sec data  */
    CalibratedGyro_RpsData_T zAxisData; /**< calibrated Gyroscope z-axis rad/sec data  */
};
typedef struct CalibratedGyro_XyzRpsData_S CalibratedGyro_XyzRpsData_T, *CalibratedGyro_XyzRpsDataPtr_T;

/**
 * Struct that holds calibrated Gyroscope data in deg/sec units
 */
struct CalibratedGyro_XyzDpsData_S
{
    CalibratedGyro_DpsData_T xAxisData; /**< calibrated Gyroscope x-axis deg/sec data  */
    CalibratedGyro_DpsData_T yAxisData; /**< calibrated Gyroscope y-axis deg/sec data  */
    CalibratedGyro_DpsData_T zAxisData; /**< calibrated Gyroscope z-axis deg/sec data  */
};
typedef struct CalibratedGyro_XyzDpsData_S CalibratedGyro_XyzDpsData_T, *CalibratedGyro_XyzDpsDataPtr_T;

/**
 * enum that defines calibration status of calibrated gyroscope
 */
enum CalibratedGyro_CalibStatus_E
{
    CALIBRATED_GYRO_UNRELIABLE, /**< unreliable calibration status of calibrated gyroscope */
    CALIBRATED_GYRO_LOW, /**< low calibration status of calibrated gyroscope */
    CALIBRATED_GYRO_MEDIUM, /**< medium calibration status of calibrated gyroscope */
    CALIBRATED_GYRO_HIGH, /**< high calibration status of calibrated gyroscope */
};
typedef enum CalibratedGyro_CalibStatus_E CalibratedGyro_CalibStatus_T, *CalibratedGyro_CalibStatusPtr_T;

/**
 * Defines the handle for calibrated Gyroscope object.
 * This handle is required for the initialisation of calibrated gyroscope
 */
typedef void* CalibratedGyro_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the calibrated Gyroscope sensor. This API needs to be called before calling any other function of
 *  the calibrated Gyroscope.  This API should return SENSOR_SUCCESS in order to communicate with this sensor.
 *
 * @param [in] handle the handle to the calibrated gyroscope object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_init(CalibratedGyro_HandlePtr_T handle);

/**
 * @brief Reads 'LSB' xyz values from calibrated gyroscope.
 *
 * @param[out] calibGyroData output parameter in which the calibrated gyroscope data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_readXyzLsbValue(CalibratedGyro_XyzLsbDataPtr_T calibGyroData);

/**
 * @brief Reads 'rad/sec' xyz values from calibrated gyroscope.
 *
 * @param[out] calibGyroData output parameter in which the calibrated gyroscope data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_readXyzRpsValue(CalibratedGyro_XyzRpsDataPtr_T calibGyroData);

/**
 * @brief Reads 'deg/sec' xyz values from calibrated gyroscope.
 *
 * @param[out] calibGyroData output parameter in which the calibrated gyroscope data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_readXyzDpsValue(CalibratedGyro_XyzDpsDataPtr_T calibGyroData);

/**
 * @brief Reads calibration accuracy status from calibrated gyroscope.
 *
 * @note There are four levels of calibration accuracy
 *              - CALIBRATED_GYRO_UNRELIABLE
 *              - CALIBRATED_GYRO_LOW
 *              - CALIBRATED_GYRO_MEDIUM
 *              - CALIBRATED_GYRO_HIGH
 *
 * @param[out] calibGyroStatus output parameter in which the gyroscope calibration status will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_getStatus(CalibratedGyro_CalibStatusPtr_T calibGyroStatus);

/**
 * @brief Deinitializes the calibrated Gyroscope sensor. This API needs to be called when
 * we require to stop the calibrated Gyroscope data reading
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedGyro_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_CALIBRATED_GYRO_H_ */

/**@} */
