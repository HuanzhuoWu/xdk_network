/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    FingerPrint FingerPrint
 *  @ingroup     Virtual_sensors_group
 *  @{
 *  @brief Virtual FingerPrint Sensor based on BMM150
 *  @details
 *
 *  The interface header exports the following features
 *                                              - FingerPrint_init()
 *                                              - FingerPrint_setValue()
 *                                              - FingerPrint_resetValue()
 *                                              - FingerPrint_checkStoredValue()
 *                                              - FingerPrint_monitoring()
 *                                              - FingerPrint_deInit()
 *
 * \b Limitations \b of \b FingerPrint: \n
 * Other virtual sensors cannot be used while using virtual sensor FingerPrint.
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_FINGER_PRINT_H_
#define BCDS_FINGER_PRINT_H_

#include "BCDS_Retcode.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */
typedef float FingerPrint_Output_DataType_T; /**< Data type used for reading FingerPrint output data */

/** Enum values to represent the different FingerPrint reference values  */
enum FingerPrint_Number_E
{
    FINGERPRINT_REF_VAL_1, /**< Specifies FingerPrint 1 to be recorded */
    FINGERPRINT_REF_VAL_2, /**< Specifies FingerPrint 2 to be recorded */
    FINGERPRINT_REF_VAL_3, /**< Specifies FingerPrint 3 to be recorded */
    FINGERPRINT_REF_MAX, /**< Specifies maximum number of recordable FingerPrint */
};
typedef enum FingerPrint_Number_E FingerPrint_Number_T;

/** Enum values that represent finger print storage status */
enum FingerPrint_StorageState_E
{
    FINGERPRINT_EMPTY, /**< FingerPrint empty */
    FINGERPRINT_RECORDED, /**< FingerPrint recorded */
};
typedef enum FingerPrint_StorageState_E FingerPrint_StorageState_T;

/** Data type for FingerPrint storage status */
typedef uint8_t FingerPrint_StorageStatus_T, *FingerPrint_StorageStatusPtr_T;

/** Data structure for FingerPrint monitoring output data  */
struct FingerPrint_MonitorData_S
{
    FingerPrint_Output_DataType_T fpDistanceMatching; /**< holds FingerPrint distance monitoring output When matchingDistance = 0: measurement is 100% not matching FingerPrint reference value */
    FingerPrint_Output_DataType_T fpAngleMatching; /**< holds FingerPrint angle monitoring output. When matchinAngle < 1: measurement is not matching with FingerPrint reference value */
};
typedef struct FingerPrint_MonitorData_S FingerPrint_Monitor_Data_T, *FingerPrint_Monitor_DataPtr_T;

/** Defines the handle for FingerPrint sensor */
typedef void* FingerPrint_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the FingerPrint sensor.This API should returnRETCODE_OK in order to communicate
 * with sensor.This function needs to be called before calling any other function of the FingerPrint sensor api.
 *
 * @param [in] FingerPrint_HandlePtr_T the handle to the FingerPrint object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_init(FingerPrint_HandlePtr_T handle);

/**
 *
 * @brief Sets the reference value for the selected FingerPrint.
 *
 * @param[in] fingerPrintNum reference value that is set for the finger print sensor
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_setValue(FingerPrint_Number_T fingerPrintNum);

/**
 *
 * @brief Clears the reference value for the selected FingerPrint.
 *
 * @param[in] fingerPrintNum reference value that is used to clear finger print
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_resetValue(FingerPrint_Number_T fingerPrintNum);

/**
 *
 * @brief Reads current FingerPrint reference values from FingerPrint sensor.
 *
 * @param[in] fingerPrintNum input parameter for which finger print stored value has to be checked.
 *
 * @param[out] fingerPrintValueStatus output parameter in which finger print value will be written to, from within function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_checkStoredValue(FingerPrint_Number_T fingerPrintNum, FingerPrint_StorageStatusPtr_T fingerPrintValueStatus);

/**
 *
 * @brief Monitors whether the measured data matches with stored FingerPrint values from FingerPrint sensor.
 *
 * @param[out] fingerPrintResult output parameter in which the finger print monitored value will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_monitoring(FingerPrint_Monitor_DataPtr_T fingerPrintResult);

/**
 * @brief API to Deinitialise the FingerPrint module
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T FingerPrint_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_FINGER_PRINT_H_ */

/**@} */

