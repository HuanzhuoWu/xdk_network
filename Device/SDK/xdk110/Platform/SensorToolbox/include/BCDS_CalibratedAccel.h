/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    CalibratedAccelerometer Calibrated Accelerometer
 *  @ingroup     Virtual_sensors_group
 *
 *  @{
 *  @brief Virtual Calibrated Accelerometer Sensor based on BMI160
 *  @details
 *  Calibrated Accelerometer is a virtual sensor that provides calibrated accelerometer data. \n i.e., Zero-g offsets are removed
 *  from the acceleration data.
 *
 *  The interface header exports the following features
 *  											- CalibratedAccel_init()
 *  											- CalibratedAccel_readXyzLsbValue()
 *  											- CalibratedAccel_readXyzMps2Value()
 *  											- CalibratedAccel_readXyzGValue()
 *  											- CalibratedAccel_getStatus()
 *  											- CalibratedAccel_deInit()
 *
 * \b Accelerometer \b Calibration \b method : \n
 * Accelerometer calibration is done by rotating the device to +/-90 degrees in all axis. \n
 * It takes some time for the accelerometer to calibrate.
 *
 * Calibrated Accelerometer data is read in three different units.
 *     - LSB units
 *     - m/(s^2) units
 *     - g units
 *
 * @note - To know the accuracy of calibration done, use CalibratedAccel_getStatus() API \n -
 * To get proper calibrated accelerometer data, make sure that the calibration accuracy
 * is CALIBRATED_ACCEL_HIGH before reading the data.
 *
 * \b Limitations \b of \b calibrated \b accelerometer: \n
 * This sensor cannot be used while using virtual sensor fingerPrint.
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_CALIBRATED_ACCEL_H_
#define BCDS_CALIBRATED_ACCEL_H_

/* public interface declaration ********************************************** */
/* System Header ************************************************************* */
#include <stdint.h>

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/** custom data types for calibrated Accelerometer data */
typedef int32_t CalibratedAccel_LsbData_T; /**< data type for calibrated accelerometer lsb data */
typedef float CalibratedAccel_Mps2Data_T; /**< data type for calibrated accelerometer m/s2 data */
typedef float CalibratedAccel_GData_T; /**< data type for calibrated accelerometer g data */

/**
 * Struct that holds calibrated accelerometer data in lsb units
 */
struct CalibratedAccel_XyzLsbData_S
{
    CalibratedAccel_LsbData_T xAxisData; /**< calibrated accelerometer x-axis lsb data  */
    CalibratedAccel_LsbData_T yAxisData; /**< calibrated accelerometer y-axis lsb data  */
    CalibratedAccel_LsbData_T zAxisData; /**< calibrated accelerometer z-axis lsb data  */
};
typedef struct CalibratedAccel_XyzLsbData_S  CalibratedAccel_XyzLsbData_T, *CalibratedAccel_XyzLsbDataPtr_T;

/**
 * Struct that holds calibrated accelerometer data in m/s2 units
 */
struct CalibratedAccel_XyzMps2Data_S
{
    CalibratedAccel_Mps2Data_T xAxisData; /**< calibrated accelerometer x-axis m/s2 data  */
    CalibratedAccel_Mps2Data_T yAxisData; /**< calibrated accelerometer y-axis m/s2 data  */
    CalibratedAccel_Mps2Data_T zAxisData; /**< calibrated accelerometer z-axis m/s2 data  */
};
typedef  struct CalibratedAccel_XyzMps2Data_S CalibratedAccel_XyzMps2Data_T, *CalibratedAccel_XyzMps2DataPtr_T;

/**
 * Struct that holds calibrated accelerometer data in g units
 */
 struct CalibratedAccel_XyzGData_S
{
    CalibratedAccel_GData_T xAxisData; /**< calibrated accelerometer x-axis g data  */
    CalibratedAccel_GData_T yAxisData; /**< calibrated accelerometer y-axis g data  */
    CalibratedAccel_GData_T zAxisData; /**< calibrated accelerometer z-axis g data  */
};
typedef  struct CalibratedAccel_XyzGData_S  CalibratedAccel_XyzGData_T, *CalibratedAccel_XyzGDataPtr_T;

/**
 * enum that defines calibration status of calibrated accelerometer
 */
enum CalibratedAccel_Status_E
{
    CALIBRATED_ACCEL_UNRELIABLE, /**< unreliable calibration status of calibrated accelerometer */
    CALIBRATED_ACCEL_LOW, /**< low calibration status of calibrated accelerometer */
    CALIBRATED_ACCEL_MEDIUM, /**< medium calibration status of calibrated accelerometer */
    CALIBRATED_ACCEL_HIGH, /**< high calibration status of calibrated accelerometer */
};
typedef enum CalibratedAccel_Status_E CalibratedAccel_Status_T, *CalibratedAccel_StatusPtr_T;

/**
 * Defines the handle for calibrated accelerometer object.
 * This handle is required for the initialisation of calibrated accelerometer
 */
typedef void* CalibratedAccel_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the calibrated accelerometer sensor. This API needs to be called before calling any other
 * function of the calibrated accelerometer. This API should return SENSOR_SUCCESS in order to communicate with this sensor.
 *
 * @param [in] handle the handle to the calibrated accelerometer object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_init(CalibratedAccel_HandlePtr_T handle);

/**
 * @brief Reads 'LSB' xyz values from calibrated accelerometer.
 *
 * @param[out] calibAccelData output parameter in which the calibrated accelerometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_readXyzLsbValue(CalibratedAccel_XyzLsbDataPtr_T calibAccelData);

/**
 * @brief Reads 'm/(s^2)' xyz values from calibrated accelerometer.
 *
 * @param[out] calibAccelData output parameter in which the calibrated accelerometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_readXyzMps2Value(CalibratedAccel_XyzMps2DataPtr_T calibAccelData);

/**
 * @brief Reads 'g' xyz values from calibrated accelerometer.
 *
 * @param[out] calibAccelData output parameter in which the calibrated accelerometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_readXyzGValue(CalibratedAccel_XyzGDataPtr_T calibAccelData);

/**
 * @brief Reads calibration accuracy status from calibrated accelerometer.
 *
 * @note There are four levels of calibration accuracy
 *              - CALIBRATED_ACCEL_UNRELIABLE
 *              - CALIBRATED_ACCEL_LOW
 *              - CALIBRATED_ACCEL_MEDIUM
 *              - CALIBRATED_ACCEL_HIGH
 *
 * @param[out] calibAccelStatus output parameter in which the accelerometer calibration status will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_getStatus(CalibratedAccel_StatusPtr_T calibAccelStatus);

/**
 * @brief Deinitializes the calibrated accelerometer sensor. This API needs to be called when
 * we require to stop the calibrated Accelerometer data reading
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T CalibratedAccel_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_CALIBRATED_ACCEL_H_ */

/**@} */
