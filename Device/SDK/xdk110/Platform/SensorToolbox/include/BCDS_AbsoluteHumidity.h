/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 /**
 *  @defgroup    AbsoluteHumidity Absolute Humidity
 *  @ingroup     Virtual_sensors_group
 *  @{
 *  @brief Virtual Absolute Humidity Sensor based on BME280
 *  @details
 *  Humidity is a virtual sensor that provides absolute Humidity data. \n
 *
 *  The interface header exports the following features
 *                                              - AbsoluteHumidity_init()
 *                                              - AbsoluteHumidity_readValue()
 *                                              - AbsoluteHumidity_deInit()
 *
 *\b Humidity \b method : \n
 * Humidity is calculated by moving the sensor from one place to another place.
 *
 * Humidity data is read in the following units.
 *      - g/m3 units
 *
 * @note To know the absoluteHumidity, use AbsoluteHumidity_readValue() API
 *
 * \b Limitations \b of \b absolute \b humidity: \n
 * This sensor cannot be used while using virtual sensor fingerPrint.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_ABSOLUTE_HUMIDITY_H_
#define BCDS_ABSOLUTE_HUMIDITY_H_

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/* public type and macro definitions */

/** Defines the handle for absolute humidity sensor */
typedef void* AbsoluteHumidity_HandlePtr_T;

/** Defines the pointer to hold absolute humidity value */
typedef float* AbsoluteHumidity_ValuePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the Humidity sensor.This API should return
 * SENSOR_SUCCESS in order to communicate with sensor.This function needs to be called before calling any other function of the Humidity sensor api.
 *
 * @param [in] AbsoluteHumidity_HandlePtr_T the handle to the humidity sensor object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T AbsoluteHumidity_init(AbsoluteHumidity_HandlePtr_T handle);

/**
 *
 * @brief Reads absolute humidity value from the humidity sensor.
 *
 * @param [in] AbsoluteHumidity_HandlePtr_T the handle to the humidity sensor object that should be initialized
 *
 * @param[out] AbsoluteHumidity_ValuePtr_T absoluteHumidityValue, output parameter in which the absoluteHumidity value will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T AbsoluteHumidity_readValue(AbsoluteHumidity_HandlePtr_T handle,AbsoluteHumidity_ValuePtr_T absoluteValue);

/**
 * @brief Deinitializes the Humidity sensor.This API should return
 * SENSOR_SUCCESS in order to get deinitialised with sensor.This function needs to be called when
 * we require to stop the humidity sensor data reading
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T AbsoluteHumidity_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_ABSOLUTE_HUMIDITY_H_ */

/**@} */
