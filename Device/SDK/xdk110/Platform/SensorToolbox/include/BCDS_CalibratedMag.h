/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    CalibratedMagnetometer Calibrated Magnetometer
 *  @ingroup     Virtual_sensors_group
 *
 *  @{
 *  @brief Virtual Calibrated Magnetometer Sensor based on BMM150
 *  @details
 *  Calibrated Magnetometer is a virtual sensor that provides calibrated magnetometer data. \n i.e., Zero-B offsets are removed
 *  from the magnetic field data.
 *
 * The interface header exports the following features
 *  											- CalibratedMag_init()
 *  											- CalibratedMag_readXyzLsbVal()
 *  											- CalibratedMag_readMicroTeslaVal()
 *  											- CalibratedMag_readXyzGaussVal()
 *  											- CalibratedMag_getStatus()
 *  											- CalibratedMag_deInit()
 *
 * \b Magnetometer \b Calibration \b method : \n
 *   Magnetometer calibration is done by leaving the device in flat position a few seconds, and making a '8' figure motion.
 *   '8' figure motion may be done more than once for a proper calibration. \n
 *   Refer the link on how to calibrate: https://www.youtube.com/watch?v=sP3d00Hr14o
 *
 * Calibrated Magnetometer data is read in three different units.
 *     - LSB units
 *     - micro Tesla units
 *     - gauss units
 *
 * @note - To know the accuracy of calibration done, use CalibratedMag_getStatus() API \n -
 * To get proper calibrated magnetometer data, make sure that the calibration accuracy
 * is CALIBRATED_MAG_STATUS_HIGH before reading the data.
 *
 * \b Limitations \b of \b calibrated \b magnetometer: \n
 * This sensor cannot be used while using virtual sensor fingerPrint.
 *************************************************************************** */
/* header definition ******************************************************** */
#ifndef BCDS_CALIBRATED_MAG_H_
#define BCDS_CALIBRATED_MAG_H_

/* public interface declaration ********************************************** */
/* System Header ************************************************************* */
#include <stdint.h>

/* Interface Header ************************************************************* */
#include "BCDS_Retcode.h"

/** custom data types for calibrated magnetometer data */
typedef int32_t CalibratedMag_LsbData_T; /**< data type for calibrated magnetometer lsb data */
typedef float CalibratedMag_MicroTeslaData_T; /**< data type for calibrated magnetometer micro tesla data */
typedef float CalibratedMag_GaussData_T; /**< data type for calibrated magnetometer gauss data*/

/**
 * Struct that holds calibrated magnetometer data in lsb units
 */
struct CalibratedMag_XyzLsbData_S
{
    CalibratedMag_LsbData_T xAxisData; /**< calibrated magnetometer x-axis lsb data  */
    CalibratedMag_LsbData_T yAxisData; /**< calibrated magnetometer y-axis lsb data  */
    CalibratedMag_LsbData_T zAxisData; /**< calibrated magnetometer z-axis lsb data  */
};
typedef struct CalibratedMag_XyzLsbData_S CalibratedMag_XyzLsbData_T, *CalibratedMag_XyzLsbDataPtr_T;

/**
 * Struct that holds calibrated Magnetometer data in micro tesla units
 */
struct CalibratedMag_XyzMicroTesla_S
{
    CalibratedMag_MicroTeslaData_T xAxisData; /**< calibrated magnetometer x-axis micro tesla data  */
    CalibratedMag_MicroTeslaData_T yAxisData; /**< calibrated magnetometer y-axis micro tesla data  */
    CalibratedMag_MicroTeslaData_T zAxisData; /**< calibrated magnetometer z-axis micro tesla data  */
};
typedef struct CalibratedMag_XyzMicroTesla_S CalibratedMag_XyzMicroTesla_T, *CalibMag_XyzMicroTeslaDataPtr_T;

/**
 * Struct that holds calibrated magnetometer data in gauss units
 */
struct CalibratedMag_XyzGaussData_S
{
    CalibratedMag_GaussData_T xAxisData; /**< calibrated magnetometer x-axis gauss data  */
    CalibratedMag_GaussData_T yAxisData; /**< calibrated magnetometer y-axis gauss data  */
    CalibratedMag_GaussData_T zAxisData; /**< calibrated magnetometer z-axis gauss data  */
};
typedef struct CalibratedMag_XyzGaussData_S  CalibratedMag_XyzGaussData_T, *CalibratedMag_XyzGaussDataPtr_T;

/**
 * enum that defines calibration status of calibrated magnetometer
 */
enum CalibratedMag_CalibStatus_E
{
    CALIBRATED_MAG_STATUS_UNRELIABLE, /**< unreliable calibration status of calibrated magnetometer */
    CALIBRATED_MAG_STATUS_LOW, /**< low calibration status of calibrated magnetometer */
    CALIBRATED_MAG_STATUS_MEDIUM, /**< medium calibration status of calibrated magnetometer */
    CALIBRATED_MAG_STATUS_HIGH, /**< high calibration status of calibrated magnetometer */
};
typedef enum CalibratedMag_CalibStatus_E  CalibratedMag_CalibStatus_T, *CalibratedMag_CalibStatusPtr_T;

/**
 * Defines the handle for calibrated magnetometer object.
 * This handle is required for the initialisation of calibrated accelerometer
 */
typedef void* CalibratedMag_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the calibrated magnetometer sensor. This API needs to be called before calling any other function of
 *  the calibrated magnetometer.  This API should return SENSOR_SUCCESS in order to communicate with this sensor.
 *
 * @param [in] handle the handle to the calibrated magnetometer object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_init(CalibratedMag_HandlePtr_T handle);

/**
 * @brief Reads 'LSB' xyz values from calibrated magnetometer.
 *
 * @param[out] calibMagData output parameter in which the calibrated magnetometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_readXyzLsbVal(CalibratedMag_XyzLsbDataPtr_T calibMagData);

/**
 * @brief Reads 'micro tesla' xyz values from calibrated magnetometer.
 *
 * @param[out] calibMagData output parameter in which the calibrated magnetometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_readMicroTeslaVal(CalibMag_XyzMicroTeslaDataPtr_T calibMagData);

/**
 * @brief Reads 'gauss' xyz values from calibrated magnetometer.
 *
 * @param[out] calibMagData output parameter in which the calibrated magnetometer data will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_readXyzGaussVal(CalibratedMag_XyzGaussDataPtr_T calibMagData);

/**
* @brief Reads calibration accuracy status from calibrated magnetometer.
 *
 * @note There are four levels of calibration accuracy
 *              - CALIBRATED_MAG_STATUS_UNRELIABLE
 *              - CALIBRATED_MAG_STATUS_LOW
 *              - CALIBRATED_MAG_STATUS_MEDIUM
 *              - CALIBRATED_MAG_STATUS_HIGH
 *
 * @param[out] calibMagStatus output parameter in which the magnetometer calibration status will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_getStatus(CalibratedMag_CalibStatusPtr_T calibMagStatus);

/**
 * @brief Deinitializes the calibrated Magnetometer sensor. This API needs to be called when
 * we require to stop the calibrated Magnetometer data reading
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the Sensor Error type present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 *
 */
Retcode_T CalibratedMag_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_CALIBRATED_MAG_H_ */

/**@} */
