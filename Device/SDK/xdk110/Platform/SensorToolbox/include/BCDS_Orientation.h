/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup    Orientation Orientation
 *  @ingroup     Virtual_sensors_group
 *  @{
 *  @brief Orientation Sensor based on BMI160 and BMM150
 *  @details
 *  In geometry, the orientation of an object (such as a line, plane or rigid body) is part of the description of how it is placed in the space it is in. \n
 *  Orientation sensor is applied to detect direction status of the device in two or three dimensions. \n
 *  Certain methods to describe orientations of a rigid body in three dimensions have been developed. \n They are as follows :
 *
 *  \b EULER \b ANGLES \b :
 *
 *  To describe an orientation in 3-dimensional Euclidean space, three parameters are required. \n
 *  They can be given in several ways, Euler angles being one of them, which describes the orientation of a frame of reference (typically, a coordinate system or basis) relative to another. \n
 *  They are represented by \b heading (rotation around the Z-axis), \b pitch (rotation around the X-axis) and \b roll (rotation around the Y-axis). \n
 *  Euler Angles can be represented in *degrees* or in *radians*.
 *
 *  \b QUATERNION \b :
 *
 *  Unit quaternion provides a convenient mathematical notation for representing orientations and rotations of objects in three dimensions. \n
 *  A quaternion represents two things. It has an x, y and z component, which represents the axis about which a rotation will occur. \n
 *  It also has a w component, which represents the amount of rotation which will occur about this axis.
 *
 *  Quaternion is represented by vector q(q[0],q[1],q[2],q[3]) where in, \b q[0] \b = \b cos(θ/2) , \b q[1] \b = \b u \b * \b sin(θ/2) , \b q[2] \b = \b v \b * \b sin(θ/2) , \b q[3] \b = \b w \b * \b sin(θ/2) ( [u,v,w] is the unit vector parallel to rotation axis )
 *
 * \b Orientation \b Calibration \b method : \n
 *  Orientation calibration should be done by leaving the device in flat position a few seconds (for gyroscope calibration) and then making the '8' figure motion (for magnetometer calibration) as in https://www.youtube.com/watch?v=sP3d00Hr14o. \n
 *  '8' figure motion may be done more than once (maybe 5 times) for a proper calibration.
 *
 * \b Limitations \b of \b orientation \b : \n
 * This sensor cannot be used while using virtual sensor fingerPrint.
 *
 *  The interface header exports the following features
 *                                              - Orientation_init()
 *                                              - Orientation_readQuaternionValue()
 *                                              - Orientation_readEulerRadianVal()
 *                                              - Orientation_readEulerDegreeVal()
 *                                              - Orientation_deInit()
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_ORIENTATION_H_
#define BCDS_ORIENTATION_H_

#include "BCDS_Retcode.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

typedef float Orientation_DataType_T; /**< Data type used for reading orientation data */

/** Enum values to represent Orientation sensor initialisation status */
enum Orientation_InitStatus_E
{
    ORIENTATION_SENSOR_UNINITIALISED = 0, /**< Specifies orientation sensor is uninitialised */
    ORIENTATION_SENSOR_INITIALISED, /**< Specifies orientation sensor is initialised */
};
typedef enum Orientation_InitStatus_E Orientation_InitStatus_T;

/** Data structure for Orientation sensor quaternion data */
struct Orientation_QuaternionData_S
{
    Orientation_DataType_T w; /**< q[0] data of vector q  */
    Orientation_DataType_T x; /**< q[1] data of vector q  */
    Orientation_DataType_T y; /**< q[2] data of vector q  */
    Orientation_DataType_T z; /**< q[3] data of vector q  */
};
typedef struct Orientation_QuaternionData_S Orientation_QuaternionData_T, *Orientation_QuaternionDataPtr_T;

/** Data structure for Orientation sensor euler data */
struct Orientation_EulerData_S
{
    Orientation_DataType_T heading; /**< heading data - rotation around z axis */
    Orientation_DataType_T pitch; /**< pitch data - rotation around x axis */
    Orientation_DataType_T roll; /**< roll data - rotation around y axis */
};
typedef struct Orientation_EulerData_S Orientation_EulerData_T, *Orientation_EulerDataPtr_T;

/** Defines the handle for Orientation sensor */
typedef void* Orientation_HandlePtr_T;

/* public function prototype declarations */

/**
 * @brief Initializes the Orientation sensor.This API should return
 * RETCODE_OK in order to communicate with sensor.This function needs to be called before calling any other function of the Orientation sensor api.
 *
 * @param [in] Orientation_HandlePtr_T the handle to the orientation object that should be initialized
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T Orientation_init(Orientation_HandlePtr_T handle);

/**
 *
 * @brief Reads orientation quaternion value from the orientation sensor.
 *
 * @param[out] Orientation_QuaternionDataPtr_T quaternionValue, output parameter in which the quaternion value will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T Orientation_readQuaternionValue(Orientation_QuaternionDataPtr_T quaternionValue);

/**
 *
 * @brief Reads orientation euler value in radians from the orientation sensor. The radian values vary from 0 to 6.28 (2*pi)
 *
 * @param[out] Orientation_EulerDataPtr_T eulerValueInRadian, output parameter in which the euler value in radians will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T Orientation_readEulerRadianVal(Orientation_EulerDataPtr_T eulerValueInRadian);

/**
 *
 * @brief Reads orientation euler value in degrees from the orientation sensor. The degree values vary from 0 to 360.
 *
 * @param[out] Orientation_EulerDataPtr_T eulerValueInDegree, output parameter in which the euler value in degrees will be written to, from within the function
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T Orientation_readEulerDegreeVal(Orientation_EulerDataPtr_T eulerValueInDegree);

/**
 * @brief API to Deinitialise the orientation module. This API needs to be called when we require to stop the orientation data reading.
 *
 * @retval      Retcode_T       The return value consist of (First 1 MSByte represents Package ID, Next  Byte represents Severity and Last 2 LSBytes represents error code)
 *
 * @note  Pass the return value of this function to BCDS_getUserErrorCode() API to know the user type return code present in
 * 'Platform\Sensors\include\BCDS_SensorErrorType.h'
 */
Retcode_T Orientation_deInit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_ORIENTATION_H_ */

/**@} */
