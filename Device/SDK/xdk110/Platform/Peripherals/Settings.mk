#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# The library name is derived from this package name
BCDS_PACKAGE_NAME = Peripherals
BCDS_PACKAGE_ID = 3
BCDS_PACKAGE_HOME = $(CURDIR)

BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_DEVICE_ID ?= EFM32GG395F1024
BCDS_DEVICE_PACKAGE_TYPE ?= STM32L476xx

# Internal Path Settings
BCDS_PERIPHERALS_SOURCE_DIR := source
BCDS_PERIPHERALS_INCLUDE_DIR := include
BCDS_PERIPHERALS_PROTECTED_DIR := source/$(BCDS_TARGET_PLATFORM)/protected
BCDS_PERIPHERALS_OBJECT_DIR := object
BCDS_PERIPHERALS_LINT_DIR := lint

#Path of Platform, Libraries and Tools
BCDS_TOOLS_DIR ?= $(BCDS_PACKAGE_HOME)/../../Tools
BCDS_LIBRARIES_DIR = $(BCDS_PACKAGE_HOME)/../../Libraries
BCDS_PLATFORM_DIR = $(BCDS_PACKAGE_HOME)/../../Platform

#Tool chain definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
BCDS_LINT_CONFIG_FILE_PATH := $(BCDS_TOOLS_DIR)/pclint/config
BCDS_LINT_CONFIG_FILE ?= $(BCDS_TOOLS_DIR)/pclint/config/bcds.lnt
LINT_EXE = $(BCDS_TOOL_DIR)/pclint/exe/lint-nt.launch
BCDS_LINT_PATH ?= $(BCDS_TOOLS_DIR)/PCLint/exe

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)
BCDS_PERIPHERALS_HARDWARE_CONFIG_PATH := $(BCDS_PACKAGE_CONFIG_PATH)/$(BCDS_HW_VERSION)

#External Library Package Dependencies
BCDS_EMLIBS_PATH ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_STM32CUBE_DIR ?= $(BCDS_LIBRARIES_DIR)/STM32Cube/stm32cube
BCDS_FREE_RTOS_PATH ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS

#External Platform Package Dependencies
BCDS_UTILS_PATH ?= $(BCDS_PLATFORM_DIR)/Utils
BCDS_BASICS_PATH ?= $(BCDS_PLATFORM_DIR)/Basics
BCDS_POWER_PATH ?= $(BCDS_PLATFORM_DIR)/Power

BCDS_PERIPHERALS_DEBUG_PATH ?= $(BCDS_PACKAGE_HOME)/debug
BCDS_PERIPHERALS_RELEASE_PATH ?= $(BCDS_PACKAGE_HOME)/release

BCDS_PERIPHERALS_DEBUG_OBJECT_PATH = $(BCDS_PERIPHERALS_DEBUG_PATH)/$(BCDS_PERIPHERALS_OBJECT_DIR)
BCDS_PERIPHERALS_RELEASE_OBJECT_PATH = $(BCDS_PERIPHERALS_RELEASE_PATH)/$(BCDS_PERIPHERALS_OBJECT_DIR)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Lint Path
BCDS_PERIPHERALS_DEBUG_LINT_PATH = $(BCDS_PERIPHERALS_DEBUG_PATH)/$(BCDS_PERIPHERALS_LINT_DIR)
	
# Archive File Settings
BCDS_PERIPHERALS_LIB_BASE_NAME = lib$(BCDS_PACKAGE_NAME)
BCDS_PERIPHERALS_DEBUG_LIB_NAME = $(BCDS_PERIPHERALS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_PERIPHERALS_RELEASE_LIB_NAME = $(BCDS_PERIPHERALS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a

BCDS_PERIPHERALS_DEBUG_LIB   = $(BCDS_PERIPHERALS_DEBUG_PATH)/$(BCDS_PERIPHERALS_DEBUG_LIB_NAME)
BCDS_PERIPHERALS_RELEASE_LIB = $(BCDS_PERIPHERALS_RELEASE_PATH)/$(BCDS_PERIPHERALS_RELEASE_LIB_NAME)
		
# Define the path for the include directories.
BCDS_EXTERNAL_INCLUDES += \
	-I$(BCDS_FREE_RTOS_PATH)/source/include

ifeq ($(BCDS_TARGET_PLATFORM),efm32)
BCDS_EXTERNAL_INCLUDES += \
	-I$(BCDS_EMLIBS_PATH)/CMSIS/Include \
	-I$(BCDS_EMLIBS_PATH)/emlib/inc \
	-I$(BCDS_EMLIBS_PATH)/usb/inc \
	-I$(BCDS_EMLIBS_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
	-I$(BCDS_FREE_RTOS_PATH)/source/portable/ARM_CM3
endif

ifeq ($(BCDS_TARGET_PLATFORM),stm32)
BCDS_EXTERNAL_INCLUDES += \
	-isystem$(BCDS_FREERTOS_PATH)/source/portable/ARM_CM4F \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/$(BCDS_DEVICE_TYPE)_HAL_Driver/Inc/Legacy \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Include \
    -isystem$(BCDS_STM32CUBE_DIR)/Drivers/CMSIS/Device/ST/$(BCDS_DEVICE_TYPE)/Include \
    -isystem$(BCDS_STM32CUBE_DIR)/../BCDS_Customization/config
endif

BCDS_PERIPHERALS_INCLUDES += \
	-I$(BCDS_PERIPHERALS_INCLUDE_DIR) \
	-I$(BCDS_CONFIG_PATH) \
	-I$(BCDS_CONFIG_PATH)/$(BCDS_HW_VERSION) \
	-I$(BCDS_PACKAGE_CONFIG_PATH) \
	-I$(BCDS_PERIPHERALS_PROTECTED_DIR) \
	-I$(BCDS_BASICS_PATH)/include \
	-I$(BCDS_POWER_PATH)/include \
	-I$(BCDS_UTILS_PATH)/include

#The inclusion of the hardware specific config files is required only when the
#compilation is done for project specific configuration
ifdef BCDS_PROJECT_SPECIFIC_CONFIG
BCDS_PERIPHERALS_INCLUDES += \
	-I $(BCDS_PERIPHERALS_HARDWARE_CONFIG_PATH)
endif

# @todo 15 Jul 2015 - mta5cob: Once all the projects migrate to BCDS_assert.h,
# the -DDBG_ASSERT_FILENAME=\"$*.c\" shall be removed.
# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	 -DDBG_ASSERT_FILENAME=\"$*.c\" -DASSERT_FILENAME=\"$*.c\"

#-------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------
BCDS_EXTERNAL_INCLUDES_LINT := $(subst -I,-i,$(BCDS_EXTERNAL_INCLUDES))
BCDS_PERIPHERALS_INCLUDES_LINT := $(subst -I,-i,$(BCDS_PERIPHERALS_INCLUDES))
#-------------------------------------------------------------------------------------------------