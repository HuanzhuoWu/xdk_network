/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>
#include <stdlib.h>

/* additional interface header files */
#include "gpio.h"
#include "em_cmu.h"
#include "em_timer.h"

/* own header files */
#include "led.h"
#include "led_efm32.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
static LED_handleAllocation_t LED_isInitializationRequired = LED_TRUE;
static LED_handleInfo_t LED_handles[LED_MAXIMUM_INSTANCES];
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* This function initialize all required handles and global variables */
LED_errorTypes_t _LED_firstInitialization(void)
{
    uint8_t ledHandleIndex = UINT8_C(0);

    /* One time initializations. */
    if (LED_isInitializationRequired)
    {
        LED_isInitializationRequired = LED_FALSE;

        /*Initialize handles */
        for (ledHandleIndex = 0; ledHandleIndex < LED_MAXIMUM_INSTANCES;
            ledHandleIndex++)
        {
            LED_handles[ledHandleIndex].isAllocated = LED_FALSE;
            LED_handles[ledHandleIndex].magicWord = LED_HANDLE_MAGIC_WORD;
        }
    }
    return (LED_ERROR_OK);
}

/* This function is used to check the handle for correct values, before using it */
LED_errorTypes_t _LED_checkParameters(LED_handleInfo_tp handleInfo)
{
    /* Make sure handle pointer is not NULL */
    if (handleInfo == NULL)
        return LED_ERROR_HANDLE_IS_NULL;

    /* Check handle magic word. */
    if (handleInfo->magicWord != LED_HANDLE_MAGIC_WORD)
        return LED_ERROR_HANDLE_DATA_CORRUPTED;

    /* Check if handle is allocated. */
    if (!handleInfo->isAllocated)
        return LED_ERROR_HANDLE_DEALLOCATED;

    return (LED_ERROR_OK);
}

/* global functions ********************************************************* */

/* API documentation is in the interface header */
LED_handle_tp LED_create(const GPIO_handle_tp pin,
    const GPIO_pinStates_t offState)
{
    uint8_t ledHandleIndex = UINT8_C(0);
    /* Make sure first initialization done. */
    if (_LED_firstInitialization() != LED_ERROR_OK)
        return NULL;

    /* Make sure pin is valid */
    if (pin == NULL)
        return NULL;

    /* Search for available handle */
    while ((LED_handles[ledHandleIndex].isAllocated)
        && (ledHandleIndex < LED_MAXIMUM_INSTANCES))
        ledHandleIndex++;

    /* Make sure we have found one. */
    if (ledHandleIndex >= LED_MAXIMUM_INSTANCES)
        return NULL;

    /* Make sure offState is valid */
    if ((offState != GPIO_STATE_OFF) && (offState != GPIO_STATE_ON))
        return NULL;

    /* Try to initialize GPIO as output with the idle state */
    if (GPIO_init(pin, GPIO_DIRECTION_OUTPUT, offState) != GPIO_ERROR_OK)
    {
        /*  cancel allocation */
        LED_handles[ledHandleIndex].isAllocated = LED_FALSE;
        return NULL;
    }

    /* Init parameters */
    LED_handles[ledHandleIndex].isAllocated = LED_TRUE;
    LED_handles[ledHandleIndex].pin = pin;
    LED_handles[ledHandleIndex].offState = offState;

    return (LED_handle_tp) &LED_handles[ledHandleIndex];
}

/* API documentation is in the interface header */
LED_errorTypes_t LED_delete(LED_handle_tp handle)
{
    LED_handleInfo_tp handleInfo = (LED_handleInfo_tp) handle;

    /* Check handle parameters. */
    LED_errorTypes_t ledReturnStatus = _LED_checkParameters(handleInfo);

    if (ledReturnStatus != LED_ERROR_OK)
    {
        return ledReturnStatus;
    }

    handleInfo->isAllocated = LED_FALSE;

    return (LED_ERROR_OK);
}

/* API documentation is in the interface header */
LED_errorTypes_t LED_setState(LED_handle_tp handle, const LED_operations_t state)
{
    LED_handleInfo_tp handleInfo = (LED_handleInfo_tp) handle;

    /* Check handle parameters. */
    LED_errorTypes_t ledReturnStatus = _LED_checkParameters(handleInfo);
    if (ledReturnStatus != LED_ERROR_OK)
        return ledReturnStatus;

    /* set GPIO state. */
    switch (state)
    {
    case LED_SET_OFF:
        if (GPIO_setPin(handleInfo->pin, handleInfo->offState) != GPIO_ERROR_OK)
            return LED_ERROR_HANDLE_DATA_CORRUPTED;

        break;
    case LED_SET_ON:
        if (GPIO_setPin(handleInfo->pin, GPIO_Invert(handleInfo->offState))
            != GPIO_ERROR_OK)
            return LED_ERROR_HANDLE_DATA_CORRUPTED;

        break;
    case LED_SET_TOGGLE:
        if (GPIO_togglePin(handleInfo->pin) != GPIO_ERROR_OK)
            return LED_ERROR_HANDLE_DATA_CORRUPTED;

        break;
    default:
        return LED_ERROR_INVALID_PARAMETER;
    }

    return (LED_ERROR_OK);
}

