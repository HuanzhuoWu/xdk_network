/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef LED_EFM32_H_
#define LED_EFM32_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */
#include "gpio.h"

/* public type and macro definitions */

#define LED_HANDLE_MAGIC_WORD  		  UINT16_C(0x01ED)      /**< Magic number to validate handle correctness */
#define LED_MAXIMUM_INSTANCES         UINT8_C(16)			/**< Maximum number of LED handle instances */
#define LED_PWM_FREQUENCY			  UINT16_C(40)		    /**< PWM frequency for the LED module */

/* public function prototype declarations */
typedef enum LED_handleAllocation_e
{
    LED_FALSE = 0,
    LED_TRUE = 1
} LED_handleAllocation_t;

/** structure of all data members required for an LED instance */
typedef struct LED_handleInfo_s
{
	uint16_t 				magicWord;
	LED_handleAllocation_t  isAllocated;    /**< Indicates if this handle already used by create function (for static allocation) */
	GPIO_handle_tp			pin;            /**< LED output signal pin location */
	GPIO_pinStates_t		offState;       /**< Specifies whether output is active low or active high */
	uint8_t					brightness;     /**< Stores the last user requested brightness level */
} LED_handleInfo_t, *LED_handleInfo_tp;

#endif /* LED_EFM32_H_ */
