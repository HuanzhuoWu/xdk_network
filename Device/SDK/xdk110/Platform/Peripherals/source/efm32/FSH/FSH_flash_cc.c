/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/*----------------------------------------------------------------------------*/
#include "FSH_flash_ih.h"
#include "BCDS_Basics.h" /* for memcpy */
#include "em_int.h"
#include "em_version.h"

/* Check the version of the emlib to see if the MSC_WriteWord bug is fixed */
#if (_EMLIB_VERSION_MAJOR > 3)
/** With EMLIB >= 4.x.x. it is OK to write multiple words from a buffer to flash */
#define USE_SINGLE_WORD_WRITES  0
#else
/** With EMLIB <= 3.x.x. only single word writes should be used because of
 * a bug in the implementation of MSC_WriteWord */
#define USE_SINGLE_WORD_WRITES  1
#endif


/**
 * @file
 * @brief Realizes internal flash access functions for EFM32 devices
 * @ingroup Flash_Access
 * @{
 */

/* constant definitions ***************************************************** */

/* public function definitions ********************************************** */
/* The function description is available in the interface header */
uint32_t FSH_GetPageSize(void)
{
    return FLASH_PAGE_SIZE;
}


/* The function description is available in the interface header */
FSH_errorCode_t FSH_ErasePage(uint32_t * pageAddress)
{
    uint32_t address = (uint32_t) pageAddress;
    FSH_errorCode_t retval = FSH_ERASEFAIL;
    /* Check if address is aligned to flash page size. */
    if (!(address & (FSH_PAGE_SIZE-1)))
    {
        msc_Return_TypeDef flashReturnCode;
        MSC_Init();
        INT_Disable();
        flashReturnCode = MSC_ErasePage(pageAddress);
        INT_Enable();
        MSC_Deinit();
        if (mscReturnOk == flashReturnCode)
        {
            retval = FSH_SUCCESS;
        }
    }
    return retval;
}

/* The function description is available in the interface header */
FSH_errorCode_t FSH_Erase(uint32_t* startAddress, uint32_t numPages)
{
    uint32_t address = (uint32_t) startAddress;
    FSH_errorCode_t retval = FSH_ERASEFAIL;
    /* Check if address is aligned to flash page size. */
    if (!(address & (FSH_PAGE_SIZE-1)) && numPages)
    {
        MSC_Init(); /* Unlock flash */
        /* Address is correct */
        msc_Return_TypeDef flashReturnCode = mscReturnOk;
        while ((mscReturnOk == flashReturnCode) && (numPages))
        {
            INT_Disable();
            flashReturnCode = MSC_ErasePage((uint32_t*)address);
            INT_Enable();
            address = address + FSH_PAGE_SIZE;
            numPages--;
        }
        MSC_Deinit(); /* Unlock flash */
        if (mscReturnOk == flashReturnCode)
        {
            retval = FSH_SUCCESS;
        }
    }
    return retval;
}

/* The function description is available in the interface header */
FSH_errorCode_t FSH_WriteWord(uint32_t * targetAddress, uint32_t data)
{
    FSH_errorCode_t retval = FSH_WRITEFAIL;
    /*check whether word is erased in flash and address is aligned correctly,
     *else do nothing */
    if ((*targetAddress == 0xFFFFFFFF) && !((uint32_t)targetAddress & (sizeof(uint32_t)-1)))
    {
        MSC_Init();
        INT_Disable();
        if (mscReturnOk == MSC_WriteWord(targetAddress, &data, sizeof(data)))
        {
            retval = FSH_SUCCESS;
        }
        INT_Enable();
        MSC_Deinit();
    }
    else
    {
        retval = FSH_NEEDERASE;
    }
    return retval;
}


/* The function description is available in the interface header */
FSH_errorCode_t FSH_WriteWordStream(uint32_t * targetAddress,
                                    uint32_t * sourceAddress,
                                    uint32_t numWords)
{
    FSH_errorCode_t retval = FSH_WRITEFAIL;
    if (sourceAddress && numWords && !((uint32_t)targetAddress & (sizeof(uint32_t)-1)))
    {
        msc_Return_TypeDef flashReturnCode = mscReturnOk;
        MSC_Init();
        if (USE_SINGLE_WORD_WRITES)
        {
            // EMLIB <= 3.x.x
            while (numWords && (mscReturnOk == flashReturnCode))
            {
                INT_Disable();
                /* MSC_WriteWord expects the number of bytes to write */
                flashReturnCode = MSC_WriteWord(targetAddress,
                                                sourceAddress,
                                                sizeof(uint32_t));
                INT_Enable();
                /* wrote one word to flash */
                numWords -= 1;
                /* but need to increase address by 4. Since pointer is of type uint32_t, below increase the pointer by 4 bytes. */
                targetAddress ++;
                sourceAddress ++;
            }
        }
        else
        {
            // EMLIB >= 4.x.x
            INT_Disable();
            flashReturnCode = MSC_WriteWord(targetAddress,
                                            sourceAddress,
                                            numWords * sizeof(uint32_t));
            INT_Enable();
        }
        MSC_Deinit();
        if ( mscReturnOk == flashReturnCode)
        {
            retval = FSH_SUCCESS;
        }
    }
    return retval;
}

/* The function description is available in the interface header */
FSH_errorCode_t FSH_WriteByte(uint8_t * targetAddress, uint8_t data)
{
    FSH_errorCode_t retval = FSH_WRITEFAIL;
    if (*targetAddress == 0xFF)
    {
        /* OK, Target byte is erased */
        union
        {
            uint8_t uBytes[4];
            uint32_t uInt;
        } byteMap __attribute__ ((aligned(4)));
        /* Align target write address pointer on 4 byte boundaries */
        uint32_t* alignedTargetAddress = (uint32_t*) (((uint32_t) targetAddress) & ~0x3UL);
        /* Get index to write from target address */
        uint32_t byteIndex = ((uint32_t) targetAddress) & 0x3;

        /* Backup current flash value to modifiy it before write */
        byteMap.uInt = *alignedTargetAddress;
        /* Modify byte value */
        byteMap.uBytes[byteIndex] = data;

        MSC_Init();
        INT_Disable();
        msc_Return_TypeDef flashReturnCode = mscReturnOk;
        flashReturnCode = MSC_WriteWord(alignedTargetAddress,
                                        &byteMap.uInt,
                                        sizeof(byteMap.uInt));
        INT_Enable();
        MSC_Deinit();
        if (mscReturnOk == flashReturnCode)
        {
            retval = FSH_SUCCESS;
        }
    }
    else
    {
        /* needs to be erased before write */
        retval = FSH_NEEDERASE;
    }
    return retval;
}


/* The function description is available in the interface header */
FSH_errorCode_t FSH_WriteByteStream( uint8_t* targetAddress,
                                     const uint8_t* sourceAddress,
                                     uint32_t numBytes)
{
    FSH_errorCode_t retval = FSH_WRITEFAIL;
    if (sourceAddress && numBytes)
    {
        /* Return value from MSC_XXXX calls */
        msc_Return_TypeDef mscRet = mscReturnOk;
        /* Pointer to traverse data to write */
        uint8_t* pSource = (uint8_t*)sourceAddress;
        /* stores length of padding needed */
        /* Get padding if address is not aligned.*/
        /* Hint targetAddress & 3 equals targetAddress % 4 */
        uint32_t volatile BytesToPad = (uint32_t)targetAddress & 0x03;
        /* used to create a mini stream to store 4 byte sequence to be flashed */
        uint8_t uBytes[4];

        MSC_Init();
        if (BytesToPad)
        {
           /* pad in front */
           /* align address to 4 byte boundaries */
           targetAddress -= BytesToPad;
           /* Get original word from flash */
           memcpy(uBytes,targetAddress,sizeof(uint32_t));
           /* Get bytes to be changed from input stream */
           for (uint32_t i = BytesToPad; (i < sizeof(uint32_t)) && numBytes; i++)
           {
               uBytes[i] = *pSource;
               pSource++;
               numBytes--;
           }
           INT_Disable();
           mscRet = MSC_WriteWord((uint32_t*)targetAddress,(void*)uBytes, sizeof(uint32_t));
           INT_Enable();
           targetAddress += sizeof(uint32_t);
        }
        /* Address should now be 4 byte aligned, so write all aligned word
         * values if there are any */
        if ((numBytes >= sizeof(uint32_t)) && (mscReturnOk == mscRet))
        {
            if (USE_SINGLE_WORD_WRITES)
            {
                /* By some reason it seems like the MSC_WriteWord function from
                 * EMlib 3.20.7 is not working correctly when it should write
                 * more than 4 bytes at once, so this implmentation uses
                 * single word writes .
                 */
                while ((numBytes >= sizeof(uint32_t)) && (mscReturnOk == mscRet))
                {
                    INT_Disable();
                    mscRet = MSC_WriteWord((uint32_t*)targetAddress,(void*)pSource, sizeof(uint32_t));
                    INT_Enable();
                    numBytes -= sizeof(uint32_t);
                    targetAddress += sizeof(uint32_t);
                    pSource += sizeof(uint32_t);
                }
            }
            else
            {
                // EMLIB >= 4.x.x
                INT_Disable();
                /*Hint:  numByte & ~3 gets the highest multiple of 4 from  number of bytes */
                mscRet = MSC_WriteWord((uint32_t*)targetAddress,(void*)pSource, (numBytes & ~0x03));
                INT_Enable();
                targetAddress += (numBytes & ~0x03);
                pSource += (numBytes & ~0x03);
                numBytes -= (numBytes & ~0x03);
            }
        }
        /* if there a bytes left, pad them, target address should be aligned already */
        if ((numBytes > 0) && (mscReturnOk == mscRet))
        {
            /* pad at back */
            /* Get original word from flash */
            memcpy(uBytes,targetAddress,sizeof(uint32_t));
            for (uint32_t i = 0; i < numBytes; i++)
            {
                uBytes[i] = *pSource;
                pSource++;
            }
            INT_Disable();
            mscRet = MSC_WriteWord((uint32_t*)targetAddress,(void*)uBytes, sizeof(uint32_t));
            INT_Enable();
        }
        MSC_Deinit();
        if (mscReturnOk == mscRet)
        {
            retval = FSH_SUCCESS;
        }
    }
    return retval;
}


/* The function description is available in the interface header */
FSH_errorCode_t FSH_ReadWordStream( uint32_t* readFrom,
                                    uint32_t* writeTo,
                                    uint32_t numWords)
{
    FSH_errorCode_t retval = FSH_READFAIL;
    if (writeTo && numWords && !((uint32_t)readFrom & (sizeof(uint32_t)-1)))
    {
        memcpy(writeTo, readFrom, (numWords * sizeof(uint32_t)));
        retval = FSH_SUCCESS;
    }
    return retval;
}

/* The function description is available in the interface header */
FSH_errorCode_t FSH_ReadByteStream( uint8_t* readFrom,
                                    uint8_t* writeTo,
                                    uint32_t numBytes)
{
    FSH_errorCode_t retval = FSH_READFAIL;
    if (writeTo && numBytes)
    {
        memcpy(writeTo, readFrom, numBytes);
        retval = FSH_SUCCESS;
    }
    return retval;
}

/**@} */ /* End of doxygroup */
