/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stddef.h>

/* additional interface header files */
#include "BCDS_Basics.h"
#include "em_leuart.h"
#include "em_usart.h"
#include "em_int.h"
#include "RB_ringBuffer_ih.h"

/* own header files */
#include "SER_serialDriver_ih.h"
#include "SER_serialDriver_ch.h"


/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
/** Local Array for holding the transmission callback function pointers. */
static SER_txCallback SER_txCallBackTable_ma[SER_PORT_MAX];
/** Local Array for holding the reception callback function pointers. */
static SER_rxCallback SER_rxCallBackTable_ma[SER_PORT_MAX];

#ifdef LEUART_PRESENT
/** Local Array for holding the base pointers for LEUART based on HW availability*/
static LEUART_TypeDef * SER_leuartBasePtrTable_ma[LEUART_COUNT] =
{
    LEUART0,
#if (LEUART_COUNT > 1)
    LEUART1,
#endif
#if (LEUART_COUNT > 2)
    LEUART2
#endif
};
#endif

#ifdef USART_PRESENT
/** Local Array for holding the base pointers for USART based on HW availability*/
USART_TypeDef * SER_usartBasePtrTable_ma[USART_COUNT] =
{
    USART0,
#if (USART_COUNT > 1)
    USART1,
#endif
#if (USART_COUNT > 2)
    USART2,
#endif
#if (USART_COUNT > 3)
    USART3
#endif
};
#endif

#ifdef UART_PRESENT
/** Local Array for holding the base pointers for UART based on HW availability*/
USART_TypeDef * SER_uartBasePtrTable_ma[UART_COUNT] =
{
    UART0,
#if (UART_COUNT > 1)
    UART1,
#endif
#if (UART_COUNT > 2)
    UART2,
#endif
#if (UART_COUNT > 3)
    UART3
#endif
};
#endif

#ifdef LEUART_PRESENT
/** Pointer to the transmit ringbuffer descriptor of the driver which uses LEUART0 */
static RingBuffer_t * SER_LEUART0_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses LEUART0 */
static RingBuffer_t * SER_LEUART0_rxbufDescr_p = NULL;

#if (LEUART_COUNT > 1)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses LEUART1 */
static RingBuffer_t * SER_LEUART1_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses LEUART1 */
static RingBuffer_t * SER_LEUART1_rxbufDescr_p = NULL;
#endif

#if (LEUART_COUNT > 2)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses LEUART2 */
static RingBuffer_t * SER_LEUART2_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses LEUART2 */
static RingBuffer_t * SER_LEUART2_rxbufDescr_p = NULL;
#endif

#endif

#ifdef USART_PRESENT
/** Pointer to the transmit ringbuffer descriptor of the driver which uses USART0 */
static RingBuffer_t * SER_USART0_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses USART0 */
static RingBuffer_t * SER_USART0_rxbufDescr_p = NULL;

#if (USART_COUNT > 1)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses USART1 */
static RingBuffer_t * SER_USART1_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses USART1 */
static RingBuffer_t * SER_USART1_rxbufDescr_p = NULL;
#endif

#if (USART_COUNT > 2)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses USART2 */
static RingBuffer_t * SER_USART2_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses USART2 */
static RingBuffer_t * SER_USART2_rxbufDescr_p = NULL;
#endif

#if (USART_COUNT > 3)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses USART3 */
static RingBuffer_t * SER_USART3_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses USART3 */
static RingBuffer_t * SER_USART3_rxbufDescr_p = NULL;
#endif

#endif

#ifdef UART_PRESENT
/** Pointer to the transmit ringbuffer descriptor of the driver which uses UART0 */
static RingBuffer_t * SER_UART0_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses UART0 */
static RingBuffer_t * SER_UART0_rxbufDescr_p = NULL;

#if (UART_COUNT > 1)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses UART1 */
static RingBuffer_t * SER_UART1_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses UART1 */
static RingBuffer_t * SER_UART1_rxbufDescr_p = NULL;
#endif

#if (UART_COUNT > 2)
/** Pointer to the transmit ringbuffer descriptor of the driver which uses UART2 */
static RingBuffer_t * SER_UART2_txbufDescr_p = NULL;
/** Pointer to the receive ringbuffer descriptor of the driver which uses UART2 */
static RingBuffer_t * SER_UART2_rxbufDescr_p = NULL;
#endif

#endif

/** Look-up table to map the generic parity codes in SER_parity_t to device specific codes */
static const uint32_t SER_parityTable[SER_HWTYPE_MAX][SER_PARITY_MAX] =
{
    { LEUART_CTRL_PARITY_NONE, LEUART_CTRL_PARITY_ODD, LEUART_CTRL_PARITY_EVEN },
    { USART_FRAME_PARITY_NONE, USART_FRAME_PARITY_ODD, USART_FRAME_PARITY_EVEN },
    { USART_FRAME_PARITY_NONE, USART_FRAME_PARITY_ODD, USART_FRAME_PARITY_EVEN },
};

/** Look-up table to map the generic databit-count codes in SER_databits_t to device specific codes */
static const uint32_t SER_dataBitsTable[SER_HWTYPE_MAX][SER_DATABITS_MAX] =
{
    { LEUART_CTRL_DATABITS_EIGHT, LEUART_CTRL_DATABITS_NINE },
    { USART_FRAME_DATABITS_EIGHT, USART_FRAME_DATABITS_NINE },
    { USART_FRAME_DATABITS_EIGHT, USART_FRAME_DATABITS_NINE },
};
/** Look-up table to map the generic stopbit-count codes in SER_stopbits_t to device specific codes */
static const uint32_t SER_stopBitsTable[SER_HWTYPE_MAX][SER_STOPBITS_MAX] =
{
    { LEUART_CTRL_STOPBITS_ONE, LEUART_CTRL_STOPBITS_TWO },
    { USART_FRAME_STOPBITS_ONE, USART_FRAME_STOPBITS_TWO },
    { USART_FRAME_STOPBITS_ONE, USART_FRAME_STOPBITS_TWO },
};
/** Look-up table to map the generic route-location codes in PERIPH_routeLocation_t to device specific codes */
static const uint32_t SER_routeLocationTable[SER_HWTYPE_MAX][SER_ROUTE_LOCATION_MAX] =
{
    { LEUART_ROUTE_LOCATION_LOC0, LEUART_ROUTE_LOCATION_LOC1, LEUART_ROUTE_LOCATION_LOC2, LEUART_ROUTE_LOCATION_LOC3 },
    { USART_ROUTE_LOCATION_LOC0, USART_ROUTE_LOCATION_LOC1, USART_ROUTE_LOCATION_LOC2, USART_ROUTE_LOCATION_LOC3 },
    { USART_ROUTE_LOCATION_LOC0, USART_ROUTE_LOCATION_LOC1, USART_ROUTE_LOCATION_LOC2, USART_ROUTE_LOCATION_LOC3 },
};

/* global variables ********************************************************* */

/*
 * @brief
 * Variable to hold the values of reference frequencies of various serial interfaces
 */
SER_referenceFrequency_t SER_refFreq = {
#ifdef LEUART_PRESENT
    .rfLEUART = UINT32_C(0),
#endif
#ifdef USART_PRESENT
    .rfUSART = UINT32_C(0),
#endif
#ifdef UART_PRESENT
    .rfUART = UINT32_C(0)
#endif
};
/* inline functions ********************************************************* */
/*
 * @brief
 * API to map callback functions into the local function pointer tables
 *
 * @param [in] SER_init_t
 *     Pointer to the serial device initialization descriptor. Contains the callback pointers.
 *
 * @retval   None.
 *
 */
static inline void SER_callbackMapping(const SER_init_t * serialInitData_p)
{
    if (serialInitData_p->txCallback)
    {
        SER_txCallBackTable_ma[serialInitData_p->hwDevicePort] = serialInitData_p->txCallback;
    }
    if (serialInitData_p->rxCallback)
    {
        SER_rxCallBackTable_ma[serialInitData_p->hwDevicePort] = serialInitData_p->rxCallback;
    }
}



/* local functions ********************************************************** */

/*
 * @brief
 * API to initialize a SERIAL driver instance that uses LEUART
 *
 * @param [in] serialdevice_p
 *     Pointer to the serial device descriptor
 * @param [ in ] serialInitData_p
 *     Pointer to the data structure using which the device is to be initialized
 *
 * @retval
 *      SER_RESOURCE : The requested hardware is not present on the chip
 * @retval
 *      SER_SUCCESS  : Success
 *
 */
static inline SER_errorCode_t SER_initLeuart(SER_device_t * serialDev_p, const SER_init_t * serialInitData_p)
{
    LEUART_TypeDef * leuart_p;
    IRQn_Type irqn;
    bool hwFound;

#ifdef LEUART_PRESENT
    leuart_p = SER_leuartBasePtrTable_ma[serialInitData_p->hwDevicePort];

#else
    leuart_p = NULL;
#endif
    hwFound = false;

#ifdef LEUART_PRESENT
    if(leuart_p == LEUART0)
    {
        SER_LEUART0_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_LEUART0_rxbufDescr_p = &serialDev_p->rxBufDescr;

        irqn = LEUART0_IRQn;
        hwFound = true;
    }
#if (LEUART_COUNT > 1)
    else if(leuart_p == LEUART1)
    {
        SER_LEUART1_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_LEUART1_rxbufDescr_p = &serialDev_p->rxBufDescr;

        irqn = LEUART1_IRQn;

        hwFound = true;
    }
#endif
#if (LEUART_COUNT > 2)
    else if(leuart_p == LEUART2)
    {
        SER_LEUART2_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_LEUART2_rxbufDescr_p = &serialDev_p->rxBufDescr;
        ertwert = assign function pointer;
        irqn = LEUART2_IRQn;

        hwFound = true;
    }
#endif

#endif      // #if LEUART_PRESENT
    if (!hwFound)
    {
        return SER_RESOURCE;
    }
    SER_callbackMapping(serialInitData_p);

    LEUART_Init_TypeDef serialInitData =
    {
        (LEUART_Enable_TypeDef) (LEUART_CMD_RXEN | LEUART_CMD_TXEN),
        SER_refFreq.rfLEUART,
        serialInitData_p->baudRate,
        (LEUART_Databits_TypeDef) SER_dataBitsTable[serialInitData_p->hwType][serialInitData_p->dataBits],
        (LEUART_Parity_TypeDef) SER_parityTable[serialInitData_p->hwType][serialInitData_p->parity],
        (LEUART_Stopbits_TypeDef) SER_stopBitsTable[serialInitData_p->hwType][serialInitData_p->stopBits]
    };

    LEUART_Reset(leuart_p);
    LEUART_Init(leuart_p, &serialInitData);

    leuart_p->ROUTE = LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN | SER_routeLocationTable[serialInitData_p->hwType][serialInitData_p->routeLocation];

    NVIC_ClearPendingIRQ(irqn);
    NVIC_EnableIRQ(irqn);

    leuart_p->IEN |= LEUART_IEN_RXDATAV;

    return SER_SUCCESS;
}

/*
 *  @brief
 *      API to initialize a SERIAL driver instance that uses USART
 *
 *  @param [ in ] serialdevice_p
 *      Pointer to the serial device descriptor
 *  @param [ in ] serialInitData_p
 *      Pointer to the data structure using which the device is to be initialized
 *
 *  @retval
 *      SER_RESOURCE : The requested hardware is not present on the chip
 *  @retval
 *      SER_SUCCESS  : Success
 *
 */
static inline SER_errorCode_t SER_initUsart(SER_device_t * serialDev_p, const SER_init_t * serialInitData_p)
{
    USART_TypeDef * usart_p;
    IRQn_Type txIrqn = (IRQn_Type) 0;
    IRQn_Type rxIrqn = (IRQn_Type) 0;
    bool hwFound;
#ifdef USART_PRESENT
    usart_p = SER_usartBasePtrTable_ma[(serialInitData_p->hwDevicePort-LEUART_COUNT)];
#else
    usart_p = NULL;
#endif
    hwFound = false;

#ifdef USART_PRESENT
    if(usart_p == USART0)
    {
        SER_USART0_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_USART0_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = USART0_TX_IRQn;
        rxIrqn = USART0_RX_IRQn;

        hwFound = true;
    }
#if (USART_COUNT > 1)
    else if(usart_p == USART1)
    {
        SER_USART1_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_USART1_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = USART1_TX_IRQn;
        rxIrqn = USART1_RX_IRQn;

        hwFound = true;
    }
#endif
#if (USART_COUNT > 2)
    else if(usart_p == USART2)
    {
        SER_USART2_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_USART2_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = USART2_TX_IRQn;
        rxIrqn = USART2_RX_IRQn;

        hwFound = true;
    }
#endif
#if (USART_COUNT > 3)
    else if(usart_p == USART3)
    {
        SER_USART3_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_USART3_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = USART3_TX_IRQn;
        rxIrqn = USART3_RX_IRQn;

        hwFound = true;
    }
#endif

#endif      // #if USART_PRESENT
    if (!hwFound)
    {
        return SER_RESOURCE;
    }

    SER_callbackMapping(serialInitData_p);

    if (serialInitData_p->protocol == SER_UART_PROTOCOL)
    {
        USART_InitAsync_TypeDef serialInitData =
        {
            (USART_Enable_TypeDef) 0, // Do not auto-enable USART, we need to write some manual config
            SER_refFreq.rfUSART,
            serialInitData_p->baudRate,
            (USART_OVS_TypeDef) USART_CTRL_OVS_X16,
            (USART_Databits_TypeDef) SER_dataBitsTable[serialInitData_p->hwType][serialInitData_p->dataBits],
            (USART_Parity_TypeDef) SER_parityTable[serialInitData_p->hwType][serialInitData_p->parity],
            (USART_Stopbits_TypeDef) SER_stopBitsTable[serialInitData_p->hwType][serialInitData_p->stopBits],
#if defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_TINY_FAMILY)
            .mvdis = false,
            .prsRxEnable = false,
            .prsRxCh = usartPrsRxCh0
#endif
        };

        USART_Reset(usart_p);
        USART_InitAsync(usart_p, &serialInitData);

        usart_p->CTRL |= USART_CTRL_TXBIL_HALFFULL;

        USART_Enable(usart_p, (USART_Enable_TypeDef) (USART_CMD_RXEN | USART_CMD_TXEN));

        usart_p->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN |
            SER_routeLocationTable[serialInitData_p->hwType][serialInitData_p->routeLocation];
    }

    NVIC_ClearPendingIRQ(txIrqn);
    NVIC_ClearPendingIRQ(rxIrqn);

    NVIC_EnableIRQ(txIrqn);
    NVIC_EnableIRQ(rxIrqn);

    usart_p->IEN |= USART_IEN_RXDATAV;

    return SER_SUCCESS;
}

/*
 *  @brief
 *      API to initialize a SERIAL driver instance that uses UART
 *
 *  @param [ in ] serialdevice_p
 *      Pointer to the serial device descriptor
 *  @param [ in ] serialInitData_p
 *      Pointer to the data structure using which the device is to be initialized
 *
 *  @retval
 *      SER_RESOURCE : The requested hardware is not present on the chip
 *  @retval
 *      SER_SUCCESS  : Success
 *
 */
static inline SER_errorCode_t SER_initUart(SER_device_t * serialDev_p, const SER_init_t * serialInitData_p)
{
    USART_TypeDef * uart_p;
    IRQn_Type txIrqn = (IRQn_Type) 0;
    IRQn_Type rxIrqn = (IRQn_Type) 0;
    bool hwFound;
#ifdef UART_PRESENT
    uart_p = SER_uartBasePtrTable_ma[(serialInitData_p->hwDevicePort-LEUART_COUNT-USART_COUNT)];
#else
    uart_p = NULL;
#endif
    hwFound = false;

#ifdef UART_PRESENT
    if(uart_p == UART0)
    {
        SER_UART0_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_UART0_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = UART0_TX_IRQn;
        rxIrqn = UART0_RX_IRQn;

        hwFound = true;
    }
#if (UART_COUNT > 1)
    else if(uart_p == UART1)
    {
        SER_UART1_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_UART1_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = UART1_TX_IRQn;
        rxIrqn = UART1_RX_IRQn;

        hwFound = true;
    }
#endif
#if (UART_COUNT > 2)
    else if(uart_p == UART2)
    {
        SER_UART2_txbufDescr_p = &serialDev_p->txBufDescr;
        SER_UART2_rxbufDescr_p = &serialDev_p->rxBufDescr;

        txIrqn = UART2_TX_IRQn;
        rxIrqn = UART2_RX_IRQn;

        hwFound = true;
    }
#endif
    else
    {
        txIrqn = (IRQn_Type)0;
        rxIrqn = (IRQn_Type)0;

        hwFound = false;
    }
#endif
    if (!hwFound)
    {
		BCDS_UNUSED(serialDev_p);
        return SER_RESOURCE;
    }

    SER_callbackMapping(serialInitData_p);

    USART_InitAsync_TypeDef
    serialInitData =
    {
        (USART_Enable_TypeDef) 0, /* Do not auto-enable USART, we need to write some manual config */
#ifdef UART_REF_FREQUENCY             /* if UART_REF_FREQ is not defined, then the default referece clock will be */
        EFM32_HFXO_FREQ,                      /* selected by the efm32 UART driver.,default clocks are 48Mhz for giant gecko families and 32Mhz for others */
#else
        SER_refFreq.rfUSART,
#endif
        serialInitData_p->baudRate,
        (USART_OVS_TypeDef) USART_CTRL_OVS_X16,
        (USART_Databits_TypeDef) SER_dataBitsTable[serialInitData_p->hwType][serialInitData_p->dataBits],
        (USART_Parity_TypeDef) SER_parityTable[serialInitData_p->hwType][serialInitData_p->parity],
        (USART_Stopbits_TypeDef) SER_stopBitsTable[serialInitData_p->hwType][serialInitData_p->stopBits],
#if defined(_EFM32_GIANT_FAMILY) || defined(_EFM32_TINY_FAMILY)
        false,
        false,
        usartPrsRxCh0
#endif
    };

    USART_Reset(uart_p);
    USART_InitAsync(uart_p, &serialInitData);

    uart_p->CTRL |= USART_CTRL_TXBIL_HALFFULL;

    USART_Enable(uart_p, (USART_Enable_TypeDef) (USART_CMD_RXEN | USART_CMD_TXEN));

    uart_p->ROUTE = USART_ROUTE_TXPEN | USART_ROUTE_RXPEN |
        SER_routeLocationTable[serialInitData_p->hwType][serialInitData_p->routeLocation];
    ;

    NVIC_ClearPendingIRQ(txIrqn);
    NVIC_ClearPendingIRQ(rxIrqn);

    NVIC_EnableIRQ(txIrqn);
    NVIC_EnableIRQ(rxIrqn);

    uart_p->IEN |= USART_IEN_RXDATAV;

    return SER_SUCCESS;
}

/* global functions ********************************************************* */

/*
 * @brief
 * API to enable directly a particular UARTn/USARTn/LEUARTn device
 *
 * @param [in] SER_serialPort_t
 *     Enumeration for the serial ports available in the hardware. Could be any of of USARTn, LEUARTn or UARTn depending on the controller
 *
 * @retval   None.
 *
 */
void SER_serialEnable(SER_serialPort_t hwDevicePort, SER_hwType_t hwType)
{
    switch (hwType)
    {
    case SER_HWTYPE_LEUART:
    {
#ifdef LEUART_PRESENT
        LEUART_TypeDef * leuart_p;
        leuart_p = SER_leuartBasePtrTable_ma[hwDevicePort];

        LEUART_Enable(leuart_p, (LEUART_Enable_TypeDef) (LEUART_CMD_RXEN | LEUART_CMD_TXEN));
#endif

    }
    break;

    case SER_HWTYPE_USART:
    {
#ifdef USART_PRESENT

        USART_TypeDef * usart_p;
        usart_p = SER_usartBasePtrTable_ma[(hwDevicePort - LEUART_COUNT)];

        USART_Enable(usart_p, (USART_Enable_TypeDef) (USART_CMD_RXEN | USART_CMD_TXEN));
#endif

    }
    break;

    case SER_HWTYPE_UART:
    {
#ifdef UART_PRESENT
        USART_TypeDef * uart_p;
        uart_p = SER_uartBasePtrTable_ma[(hwDevicePort - LEUART_COUNT - USART_COUNT)];

        USART_Enable(uart_p, (USART_Enable_TypeDef) (USART_CMD_RXEN | USART_CMD_TXEN));
#endif
    }

    break;
    default:
        break;

    }
}
/*
 * @brief
 * API to disable directly a particular UARTn/USARTn/LEUARTn device
 *
 * @param [in] SER_serialPort_t
 *     Enumeration for the serial ports available in the hardware. Could be any of of USARTn, LEUARTn or UARTn depending on the controller
 *
 * @retval   None.
 *
 */
void SER_serialDisable(SER_serialPort_t hwDevicePort, SER_hwType_t hwType)
{
    switch (hwType)
    {
    case SER_HWTYPE_LEUART:
    {
#ifdef LEUART_PRESENT
        LEUART_TypeDef * leuart_p;
        leuart_p = SER_leuartBasePtrTable_ma[hwDevicePort];

        LEUART_Enable(leuart_p, (LEUART_Enable_TypeDef) (0x0));
#endif
    }
    break;

    case SER_HWTYPE_USART:
    {
#ifdef USART_PRESENT

        USART_TypeDef * usart_p;
        usart_p = SER_usartBasePtrTable_ma[(hwDevicePort - LEUART_COUNT)];

        USART_Enable(usart_p, (USART_Enable_TypeDef) (0x0));
#endif
    }
    break;

    case SER_HWTYPE_UART:
    {
#ifdef UART_PRESENT
        USART_TypeDef * uart_p;
        uart_p = SER_uartBasePtrTable_ma[(hwDevicePort - LEUART_COUNT - USART_COUNT)];

        USART_Enable(uart_p, (USART_Enable_TypeDef) (0x0));
#endif
    }
    break;
    default:
        break;

    }
}

/*
 *  @brief
 *      Initializes an instance of serial device driver
 *
 *  @detail
 *      This API fills in the fields of a SER_device_t structure which will be used later by other SER_xxx
 *      APIs such as SER_write. This API takes care of verifying the requested hardware
 *      (LEUART0, UART2 etc.) is present on the chip, initializes the ring buffers and sets up the
 *      required interrupt vectors
 *  
 *  @param [ out ] serialDev_p
 *      Pointer to a SER_device_t structure that will serve as a serial device instance. The structure that is
 *      initialized by this API need to maintained for future use
 *  @param [ in ] initData_p
 *      Pointer to a SER_init_t structure that holds the necessary data with which we can initialize the serial
 *      device. This data structure is used only during initialization and can be discarded afterwards
 *  
 *  @retval
 *      SER_ARG         : An invalid argument has been passed. Could be an hardware type, parity value, databit count, stopbit count or routing location
 *  @retval
 *      SER_UNREACHABLE : Fatal; should never happen. Please report in case this is found.
 *  @retval
 *      SER_SUCCESS     : Successfully completed initialization
 *  
 */
SER_errorCode_t SER_serialInit(SER_device_t * serialDev_p, const SER_init_t * initData_p)
{
    SER_errorCode_t hwInitStatus;
    SER_errorCode_t retval;

    if (initData_p->hwType >= SER_HWTYPE_MAX)
    {
        return SER_ARG;
    }

    if (initData_p->parity >= SER_PARITY_MAX)
    {
        return SER_ARG;
    }

    if (initData_p->dataBits >= SER_DATABITS_MAX)
    {
        return SER_ARG;
    }

    if (initData_p->stopBits >= SER_STOPBITS_MAX)
    {
        return SER_ARG;
    }

    if (initData_p->routeLocation >= SER_ROUTE_LOCATION_MAX)
    {
        return SER_ARG;
    }

    serialDev_p->hwType = initData_p->hwType;
    serialDev_p->hwDevicePort = initData_p->hwDevicePort;
    serialDev_p->hasHwFlowControl = initData_p->hasHwFlowControl;

    RingBuf_Init(&serialDev_p->rxBufDescr, initData_p->rxBuf_p, initData_p->rxBufSize);
    RingBuf_Init(&serialDev_p->txBufDescr, initData_p->txBuf_p, initData_p->txBufSize);

    switch (serialDev_p->hwType)
    {
    case SER_HWTYPE_LEUART:
        hwInitStatus = SER_initLeuart(serialDev_p, initData_p);
        break;
    case SER_HWTYPE_USART:
        hwInitStatus = SER_initUsart(serialDev_p, initData_p);
        break;
    case SER_HWTYPE_UART:
        hwInitStatus = SER_initUart(serialDev_p, initData_p);
        break;
    default:
        hwInitStatus = SER_ARG;
        break;
    }
    /* Initialize Flow Control */
    if (initData_p->hasHwFlowControl)
    {
    }

    if (hwInitStatus == SER_SUCCESS)
    {
        retval = SER_SUCCESS;
    }
    else
    {
        retval = SER_UNREACHABLE;
    }

    return retval;
}

/*
 *  @brief
 *      Transmits a byte stream
 *  
 *  @param [ in ] serialDev_p
 *      Pointer to the serial device descriptor instance
 *  @param [ in ] attr_p
 *      This pointer is used to provide additional transmission attributes using which any existing setting can be 
 *      overriden. Currently not implemented; pass NULL
 *  @param [ in ] data_p
 *      Pointer to the data to be sent
 *  @param [ in ] byteCount
 *      Number of bytes to send
 *  
 *  @retval
 *      SER_RESOURCE    : Not enough emeory in the internal ring buffer to hold the required number of bytes
 *  @retval
 *      SER_SUCCESS     : Succesfully completed
 *  
 *  @note
 *      Completion of this write API does not mean the data has been delivered to the remote device; this API merely
 *      copies the supplied data to the internal ring buffer. Actual transmission takes place later, handled by the
 *      TX IRQ handlers of the respective peripheral.
 *  
 */
SER_errorCode_t SER_serialWrite(SER_device_t * serialDev_p, void * attr_p, const void * data_p, uint32_t byteCount)
{
    const uint8_t * buffer_p;

    (void) attr_p;

    buffer_p = (const uint8_t *) data_p;

    if (byteCount > (uint32_t) serialDev_p->txBufDescr.Size - (uint32_t) serialDev_p->txBufDescr.Count)
    {
        return SER_RESOURCE;
    }

    while (byteCount-- != 0)
    {
        /* Atomically push a byte into the TX ring buffer */
        INT_Disable();

        /* RIB_ringBufWrite(&serialDev_p->txBufDescr, buffer_p, byteCount); */
        RingBuf_Push(&serialDev_p->txBufDescr, *buffer_p++);

        /* Enable TXBL since we are now in a position to transmit */
        if (serialDev_p->hwType == SER_HWTYPE_LEUART)
        {
#ifdef LEUART_PRESENT
            ((LEUART_TypeDef *) SER_leuartBasePtrTable_ma[serialDev_p->hwDevicePort])->IEN |= LEUART_IEN_TXBL;
#else
            while (1)
                ;
#endif
        }
        else if (serialDev_p->hwType == SER_HWTYPE_USART)
        {
#ifdef USART_PRESENT
            ((USART_TypeDef *) SER_usartBasePtrTable_ma[(serialDev_p->hwDevicePort-LEUART_COUNT)])->IEN |= USART_IEN_TXBL;
#else
            while (1)
                ;
#endif
        }
        else if (serialDev_p->hwType == SER_HWTYPE_UART)
        {
#ifdef UART_PRESENT
            ((USART_TypeDef *) SER_uartBasePtrTable_ma[(serialDev_p->hwDevicePort-LEUART_COUNT-USART_COUNT)])->IEN |= USART_IEN_TXBL;
#else
            while (1)
                ;
#endif
        }
        else
        {
            while (1)
                ;
        }

        INT_Enable();
    }

    return SER_SUCCESS;
}

/*
 *  @brief
 *      Reads a byte stream from the device driver
 *  
 *  @param [ in ] serialDev_p
 *      Pointer to the serial device descriptor
 *  @param [ in ] attr_p
 *      Remaining bytes in the read buffer
 *  @param [ out ] data_p
 *      Pointer to a buffer where the API should store the requested number of bytes
 *  @param [ in ] byteCount
 *      The number of bytes to read
 *  
 *  @retval
 *      SER_RESOURCE    : The requested number of bytes are not present in the driver's buffer
 *  @retval
 *      SER_SUCCESS     : Successfully read the requested number of bytes
 *  
 *  @note
 *      This API just reads the required number of bytes from the driver's internal buffer and writes into the 
 *      user-supplied buffer. No attempt is made to wait until the required number of bytes arrive from the remote
 *      device
 *  
 */
SER_errorCode_t SER_serialRead(SER_device_t * serialDev_p, uint32_t * attr_p, void * data_p, uint32_t byteCount)
{
    uint8_t * buffer_p;

    buffer_p = (uint8_t *) data_p;
    if (byteCount > (uint32_t) serialDev_p->rxBufDescr.Count)
    {
        *attr_p = serialDev_p->rxBufDescr.Count;
        return SER_RESOURCE;
    }

    while (byteCount-- != 0)
    {
        /* Atomically pop a byte from RX buffer */

        INT_Disable();

        RingBuf_Read(&serialDev_p->rxBufDescr, buffer_p++, 1);
        INT_Enable();
    }

    *attr_p = serialDev_p->rxBufDescr.Count;
    return SER_SUCCESS;
}

/*
 *  @brief
 *      Resets the buffer of the serial driver receive buffer
 *
 *  @param [ in ] serialDev_p
 *      Pointer to the serial device descriptor
 *
 *
 *  @note
 *      This API resets the buffer of the receive buffer when the buffer count has non zero value.
 *      This is achieved by using the ring buffer read function.
 *
 */
void SER_serialResetBuffer(SER_device_t * serialDev_p)
{
    uint8_t buffer;

    while ((uint32_t) serialDev_p->rxBufDescr.Count != UINT16_C(0))
    {
        INT_Disable();
        RingBuf_Read(&serialDev_p->rxBufDescr, &buffer, UINT16_C(1));
        INT_Enable();
    }
}

#ifdef LEUART_PRESENT
/**
 *  @brief
 *      LEUART0 irq handler. Will be defined only if the peripheral is present
 */
void LEUART0_IRQHandler(void)
{
    uint8_t byte = UINT8_C(0);

    if(LEUART0->IF & LEUART_IF_RXDATAV)
    {
        if(LEUART0->STATUS & LEUART_STATUS_RXDATAV)
        {
            byte = (uint8_t) LEUART0->RXDATA;

            if(SER_LEUART0_rxbufDescr_p->Count < SER_LEUART0_rxbufDescr_p->Size)
            {
                RingBuf_Push(SER_LEUART0_rxbufDescr_p, byte);
            }

            if(SER_rxCallBackTable_ma[SER_LEUART0] != NULL)
            {
                (*SER_rxCallBackTable_ma[SER_LEUART0])(byte,SER_LEUART0_rxbufDescr_p->Count);
            }
        }
    }
    else if(LEUART0->IF & LEUART_IF_TXBL)
    {
        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_LEUART0_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_LEUART0_txbufDescr_p, byte);

            if(LEUART0->STATUS & LEUART_STATUS_TXBL)
            {
                LEUART_FreezeEnable(LEUART0, true);

                LEUART0->TXDATA = (uint32_t) byte;

                LEUART_FreezeEnable(LEUART0, false);
            }
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_LEUART0_txbufDescr_p->Count == 0)
        {
            LEUART0->IEN &= ~LEUART_IEN_TXBL; /* Disable TXBL if we have no more to send.This will be re-enabled in write API */
        }
    }
}

#if (LEUART_COUNT > 1)
/**
 *  @brief
 *      LEUART1 irq handler. Will be defined only if the peripheral is present
 */
void LEUART1_IRQHandler(void)
{
    uint8_t byte;

    if(LEUART1->IF & LEUART_IF_RXDATAV)
    {
        if(LEUART1->STATUS & LEUART_STATUS_RXDATAV)
        {
            byte = (uint8_t) LEUART1->RXDATA;

            if(SER_LEUART1_rxbufDescr_p->Count < SER_LEUART1_rxbufDescr_p->Size)
            {
                RingBuf_Push(SER_LEUART1_rxbufDescr_p, byte);
            }

            if(SER_rxCallBackTable_ma[SER_LEUART1] != NULL)
            {
                (*SER_rxCallBackTable_ma[SER_LEUART1])(byte,SER_LEUART1_rxbufDescr_p->Count);
            }
        }
    }
    else if(LEUART1->IF & LEUART_IF_TXBL)
    {
        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_LEUART1_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_LEUART1_txbufDescr_p, byte);

            if(LEUART1->STATUS & LEUART_STATUS_TXBL)
            {
                LEUART_FreezeEnable(LEUART1, true);

                LEUART1->TXDATA = (uint32_t) byte;

                LEUART_FreezeEnable(LEUART1, false);
            }
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
         check if it's zero after the transmission */

        if(SER_LEUART1_txbufDescr_p->Count == 0)
        {
            LEUART1->IEN &= ~LEUART_IEN_TXBL; /* Disable TXBL if we have no more to send.
                                               This will be re-enabled in write API */
        }
    }
}
#endif

#if (LEUART_COUNT > 2)
/**
 *  @brief
 *      LEUART2 irq handler. Will be defined only if the peripheral is present
 */
void LEUART2_IRQHandler(void)
{
    uint8_t byte;

    if(LEUART2->IF & LEUART_IF_RXDATAV)
    {
        if(LEUART2->STATUS & LEUART_STATUS_RXDATAV)
        {
            byte = (uint8_t) LEUART2->RXDATA;

            if(SER_LEUART2_rxbufDescr_p->Count < SER_LEUART2_rxbufDescr_p->Size)
            {
                RIB_ringBufQuickPush(SER_LEUART2_rxbufDescr_p, byte);
            }

            if(SER_rxCallBackTable_ma[SER_LEUART2] != NULL)
            {
                (*SER_rxCallBackTable_ma[SER_LEUART2])(byte,SER_LEUART2_rxbufDescr_p->Count);
            }

        }
    }
    else if(LEUART2->IF & LEUART_IF_TXBL)
    {
        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
        pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_LEUART2_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_LEUART2_txbufDescr_p, byte);

            if(LEUART2->STATUS & LEUART_STATUS_TXBL)
            {
                LEUART_FreezeEnable(LEUART2, true);

                LEUART2->TXDATA = (uint32_t) byte;

                LEUART_FreezeEnable(LEUART2, false);
            }
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_LEUART2_txbufDescr_p->Count == 0)
        {
            LEUART2->IEN &= ~LEUART_IEN_TXBL; // Disable TXBL if we have no more to send.
            // This will be re-enabled in write API
        }
    }
}
#endif

#endif      // #if LEUART_PRESENT
#ifdef USART_PRESENT
/**
 *  @brief
 *      USART0 tx irq handler. Will be defined only if the peripheral is present
 */
void USART0_TX_IRQHandler(void)
{
    uint8_t byte = UINT8_C(0);

    if(USART0->IF & USART_IF_TXBL)
    {
        USART0->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_USART0_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_USART0_txbufDescr_p, byte);

            USART0->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_USART0_txbufDescr_p->Count == 0)
        {
            USART0->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write */
            if(SER_txCallBackTable_ma[SER_USART0] != NULL) /* Transmit CallBack function NULL check */
            {
                (*SER_txCallBackTable_ma[SER_USART0])();
            }

        }
    }
}

/* Refer API documentation is in the interface header file SER_serialDriver_ih.h */
uint8_t SER_readDataFromUSART0Buffer(void)
{
    uint8_t byte;
    RingBuf_Pop(SER_USART0_rxbufDescr_p, byte);
    return (byte);
}

/**
 *  @brief
 *      USART0 rx irq handler. Will be defined only if the peripheral is present.
 *
 *   @warning The compiler warning for the uninitialized local variable \auint8_t \abyte; was taken care by making use of this local variable
 *    byte inside the check for the UART0 receive flag check condition
 *  
 */
void USART0_RX_IRQHandler(void)
{
    uint8_t byte;

    if(USART0->IF & USART_IF_RXDATAV)
    {
        USART0->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) USART0->RXDATA;

        if(SER_USART0_rxbufDescr_p->Count < SER_USART0_rxbufDescr_p->Size)
        {
            RingBuf_Push(SER_USART0_rxbufDescr_p, byte);
        }
        if(SER_rxCallBackTable_ma[SER_USART0] != NULL)
        {
            (*SER_rxCallBackTable_ma[SER_USART0])(byte,SER_USART0_rxbufDescr_p->Count);
        }
    }
}

#if (USART_COUNT > 1)
/**
 *  @brief
 *      USART1 tx irq handler. Will be defined only if the peripheral is present
 */
void USART1_TX_IRQHandler(void)
{
    uint8_t byte;

    if(USART1->IF & USART_IF_TXBL)
    {
        USART1->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
            pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_USART1_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_USART1_txbufDescr_p, byte);

            USART1->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_USART1_txbufDescr_p->Count == 0)
        {
            USART1->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send.This will be re-enabled in UART_write */
            if(SER_txCallBackTable_ma[SER_USART1] != NULL) /* Transmit CallBack function NULL check */
            {
                (*SER_txCallBackTable_ma[SER_USART1])();
            }
        }
    }
}

/* Refer API documentation is in the interface header file SER_serialDriver_ih.h */
uint8_t SER_readDataFromUSART1Buffer(void)
{
    uint8_t byte;
    RingBuf_Pop(SER_USART1_rxbufDescr_p, byte);
    return (byte);
}

/**
 *  @brief
 *      USART1 rx irq handler. Will be defined only if the peripheral is present
 */
void USART1_RX_IRQHandler(void)
{
    uint8_t byte=0;

    if(USART1->IF & USART_IF_RXDATAV)
    {
        USART1->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) USART1->RXDATA;

        if(SER_USART1_rxbufDescr_p->Count < SER_USART1_rxbufDescr_p->Size)
        {
            RingBuf_Push(SER_USART1_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_USART1] != NULL)
    {
        (*SER_rxCallBackTable_ma[SER_USART1])(byte,SER_USART1_rxbufDescr_p->Count);
    }
}
#endif

#if (USART_COUNT > 2)
/**
 *  @brief
 *      USART2 tx irq handler. Will be defined only if the peripheral is present
 */
void USART2_TX_IRQHandler(void)
{
    uint8_t byte;

    if(USART2->IF & USART_IF_TXBL)
    {
        USART2->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_USART2_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_USART2_txbufDescr_p, byte);

            USART2->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_USART2_txbufDescr_p->Count == 0)
        {
            USART2->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write */
            if(SER_txCallBackTable_ma[SER_USART2] != NULL) /* Transmit CallBack function NULL check */
            {
                (*SER_txCallBackTable_ma[SER_USART2])();
            }
        }
    }
}

/* Refer API documentation is in the interface header file SER_serialDriver_ih.h */
uint8_t SER_readDataFromUSART2Buffer(void)
{
    uint8_t byte;
    RingBuf_Pop(SER_USART2_rxbufDescr_p, byte);
    return (byte);
}

/**
 *  @brief
 *      USART2 rx irq handler. Will be defined only if the peripheral is present
 */
void USART2_RX_IRQHandler(void)
{
    uint8_t byte;

    if(USART2->IF & USART_IF_RXDATAV)
    {
        USART2->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) USART2->RXDATA;

        if(SER_USART2_rxbufDescr_p->Count < SER_USART2_rxbufDescr_p->Size)
        {
            RingBuf_Push(SER_USART2_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_USART2] != NULL) /* Receive CallBack function NULL check */
    {
        (*SER_rxCallBackTable_ma[SER_USART2])(byte,SER_USART2_rxbufDescr_p->Count);
    }
}
#endif

#if (USART_COUNT > 3)
/**
 *  @brief
 *      USART3 tx irq handler. Will be defined only if the peripheral is present
 */
void USART3_TX_IRQHandler(void)
{
    uint8_t byte;

    if(USART3->IF & USART_IF_TXBL)
    {
        USART3->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_USART3_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_USART3_txbufDescr_p, byte);

            USART3->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
         check if it's zero after the transmission */

        if(SER_USART3_txbufDescr_p->Count == 0)
        {
            USART3->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write */
            if(SER_txCallBackTable_ma[SER_USART3] != NULL) /* Transmit CallBack function NULL check */
            {         
                (*SER_txCallBackTable_ma[SER_USART3])();
            }
        }
    }
}

/**
 *  @brief
 *      USART3 rx irq handler. Will be defined only if the peripheral is present
 */
void USART3_RX_IRQHandler(void)
{
    uint8_t byte;

    if(USART3->IF & USART_IF_RXDATAV)
    {
        USART3->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) USART3->RXDATA;

        if(SER_USART3_rxbufDescr_p->Count < SER_USART3_rxbufDescr_p->Size)
        {
            RIB_ringBufQuickPush(SER_USART3_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_USART3] != NULL)
    {
        (*SER_rxCallBackTable_ma[SER_USART3])(byte,SER_USART3_rxbufDescr_p->Count);
    }
}
#endif

#endif
#ifdef UART_PRESENT
/**
 *  @brief
 *      UART0 tx irq handler. Will be defined only if the peripheral is present
 */
void UART0_TX_IRQHandler(void)
{
    uint8_t byte = UINT8_C(0);;

    if(UART0->IF & USART_IF_TXBL)
    {
        UART0->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
         pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_UART0_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_UART0_txbufDescr_p, byte);

            UART0->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_UART0_txbufDescr_p->Count == 0)
        {
            UART0->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write */
        }
    }
}

/**
 *  @brief
 *      UART0 tx irq handler. Will be defined only if the peripheral is present
 */
void UART0_RX_IRQHandler(void)
{
    uint8_t byte;

    if(UART0->IF & USART_IF_RXDATAV)
    {
        UART0->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) UART0->RXDATA;

        if(SER_UART0_rxbufDescr_p->Count < SER_UART0_rxbufDescr_p->Size)
        {
            RingBuf_Push(SER_UART0_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_UART0] != NULL)
    {
        (*SER_rxCallBackTable_ma[SER_UART0])(byte,SER_UART0_rxbufDescr_p->Count);
    }
}

#if (UART_COUNT > 1)
/**
 *  @brief
 *      UART1 tx irq handler. Will be defined only if the peripheral is present
 */
void UART1_TX_IRQHandler(void)
{
    uint8_t byte;

    if(UART1->IF & USART_IF_TXBL)
    {
        UART1->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
        pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_UART1_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_UART1_txbufDescr_p, byte);

            UART1->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_UART1_txbufDescr_p->Count == 0)
        {
            UART1->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write */
        }
    }
}

/**
 *  @brief
 *      UART1 rx irq handler. Will be defined only if the peripheral is present
 */
void UART1_RX_IRQHandler(void)
{
    uint8_t byte;

    if(UART1->IF & USART_IF_RXDATAV)
    {
        UART1->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) UART1->RXDATA;

        if(SER_UART1_rxbufDescr_p->Count < SER_UART1_rxbufDescr_p->Size)
        {
            RingBuf_Push(SER_UART1_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_UART1] != NULL)
    {
        (*SER_rxCallBackTable_ma[SER_UART1])(byte,SER_UART1_rxbufDescr_p->Count);
    }
}
#endif

#if (UART_COUNT > 2)
/**
 *  @brief
 *      UART2 tx irq handler. Will be defined only if the peripheral is present
 */
void UART2_TX_IRQHandler(void)
{
    uint8_t byte;

    if(UART2->IF & USART_IF_TXBL)
    {
        UART2->IFC = USART_IF_TXBL;

        /* We need to do a double-check here since TXBL interrupt may be triggered due to the
        pending flag in the NVIC, even if we disable it in the IEN register */

        if(SER_UART2_txbufDescr_p->Count > 0)
        {
            RingBuf_Pop(SER_UART2_txbufDescr_p, byte);

            UART2->TXDATA = (uint32_t) byte;
        }

        /* Don't keep this in an else-if. The RingBuf_quickPop changes 'Count' and we need to
        check if it's zero after the transmission */

        if(SER_UART2_txbufDescr_p->Count == 0)
        {
            UART2->IEN &= ~USART_IEN_TXBL; /* Disable TXBL if we have no more to send. This will be re-enabled in UART_write*/
        }
    }
}

/**
 *  @brief
 *      UART2 rx irq handler. Will be defined only if the peripheral is present
 */
void UART2_RX_IRQHandler(void)
{
    uint8_t byte;

    if(UART2->IF & USART_IF_RXDATAV)
    {
        UART2->IFC = USART_IF_RXDATAV;

        byte = (uint8_t) UART2->RXDATA;

        if(SER_UART2_rxbufDescr_p->Count < SER_UART2_rxbufDescr_p->Size)
        {
            RIB_ringBufQuickPush(SER_UART2_rxbufDescr_p, byte);
        }
    }
    if(SER_rxCallBackTable_ma[SER_UART2] != NULL)
    {
        (*SER_rxCallBackTable_ma[SER_UART2])(byte,SER_UART2_rxbufDescr_p->Count);
    }

}
#endif

#endif      // #if UART_PRESENT
/** ************************************************************************* */
