/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_USB_CH_H_
#define BCDS_USB_CH_H_

/* local interface declaration ********************************************** */
#include <stdint.h>
/* local type and macro definitions */

#define USB_EP_DATA_OUT            INT32_C(0x01)        /**< Endpoint for USB data reception.       */
#define USB_EP_DATA_IN             INT32_C(0x81)        /**< Endpoint for USB data transmission.    */
#define USB_EP_NOTIFY              INT32_C(0x82)        /**< The notification endpoint (not used).  */
/** Macro to represent the delay limit(10ms) when USB transfer is not
 * done (since it takes 10ms(appx) to transmit 1kb delay limit is fixed as 10ms)    */
#define USB_DELAY_LIMIT            UINT8_C(10)
#define USB_ONE_MILLI_SECOND_DELAY UINT8_C(1)           /**< Macro to represent the one milli second delay*/

#define USB_ZERO_VALUE             UINT8_C(0)           /**< Macro to represent zero value          */

/* USB uses the below commands to save in a USB Setup request package(USB_Setup_TypeDef) to implement CDC class  */
#define USB_SETUP_VALUE             UINT8_C(0)          /* @see USB_Setup_TypeDef  */
#define USB_SETUP_INDEX             UINT8_C(0)          /* @see USB_Setup_TypeDef  */
#define USB_SETUP_NO_DATA_LENGTH    UINT8_C(0)          /* @see USB_Setup_TypeDef  */
#define USB_SETUP_LENGTH            UINT8_C(7)          /* @see USB_Setup_TypeDef  */

STATIC_CONST_STRING_DESC( USB_manufacturerName_gau, 'B','o','s','c','h',' ','C','o','n','n','e','c','t','e','d', ' ','D','e','v','i','c','e','s',' ','a','n','d',' ','S','o','l','u','t','i','o','n','s',' ','G','m','b','H'); /**< Manufacturer name */
STATIC_CONST_STRING_DESC( USB_productName_gau, BCDS_PRODUCT_NAME); /**< Product name */

/* local function prototype declarations */

/* local module global variable declarations */
/* local inline function definitions */

#endif /* BCDS_USB_CH_H_ */

/** ************************************************************************* */
