/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */
#include <stdint.h>
#include "FreeRTOS.h"
#include "queue.h"
#include "I2C_efm32Driver_ih.h"
#include "task.h"
#include "PTD_portDriver_ih.h"
#include "PTD_portDriver_ph.h"
#include "BCDS_Assert.h"

/* own header files */
#define _INC_SID_MODULE
#include "SID_serialDriverInf_ih.h"
#undef _INC_SID_MODULE

#include "SID_serialDriverInf_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
void* SID_serialQueue_ma[SID_SERIAL_CHANNEL_MAX] = { NULL, NULL };/*in interrupt mode, "SID_serialQueue_ma"array which stores the handle of the functions*/
SID_settings_t SID_setting_gas[SID_SERIAL_CHANNEL_MAX];/*array of structure to store the information regarding data transfer */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/* API documentation is in the interface header */
void SID_init(SID_initSettings_t initSetting)
{
    SID_setting_gas[initSetting.channel].sampleMode = SID_POLL_MODE;
    if (initSetting.noOfqueueItem != 0)
    {
        SID_serialQueue_ma[initSetting.channel] = xQueueCreate(initSetting.noOfqueueItem, initSetting.eachQueueItemSize);
        if(NULL == SID_serialQueue_ma[initSetting.channel])
        {
            assert(0);
        }
        SID_setting_gas[initSetting.channel].sampleMode = SID_INTERRUPT_MODE;
    }
    SID_setting_gas[initSetting.channel].decodeCallbackFunction = initSetting.callbackFunc;

    I2C_initCtrSpecific((I2C_channel_t) initSetting.channel, initSetting.location);
}

/* API documentation is in the interface header */
SID_txStatus_t SID_send(SID_channel_t channel, const void*bufferData)
{
    SID_txStatus_t returnStatus;
    taskENTER_CRITICAL();
    returnStatus = (SID_txStatus_t) (*SID_triggerTransfer_p[SERIAL_channelType[channel]])(channel, bufferData);
    taskEXIT_CRITICAL();
    /*If there is on-going transfer and only if interrupt based method then add to queue. Otherwise it is not necessary to add in queue.*/
    if ((returnStatus == SID_TXFAIL) && (SID_setting_gas[channel].sampleMode == SID_INTERRUPT_MODE))
    {
        if (xQueueSend(SID_serialQueue_ma[channel], (void *) bufferData, (portTickType) (0/portTICK_RATE_MS)) != pdTRUE )
        {
            assert(0);
        }
    }
    return returnStatus;
}

/* API documentation is in the interface header */
SID_txStatus_t SID_sendISR(SID_channel_t channel, const void*bufferData)
{
    SID_txStatus_t returnStatus;
    returnStatus = (SID_txStatus_t) (*SID_triggerTransfer_p[SERIAL_channelType[channel]])(channel, bufferData);
    /*If there is on-going transfer and only if interrupt based method then add to queue. Otherwise it is not necessary to add in queue.*/
    if ((returnStatus == SID_TXFAIL) && (SID_setting_gas[channel].sampleMode == SID_INTERRUPT_MODE))
    {
        xQueueSendFromISR(SID_serialQueue_ma[channel], bufferData, (portTickType) 0);
    }
    return returnStatus;
}
/** ************************************************************************* */
