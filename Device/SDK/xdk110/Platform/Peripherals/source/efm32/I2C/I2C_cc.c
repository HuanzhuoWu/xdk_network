/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "PTD_portDriver_ih.h"

#define _INC_SID_MODULE
#include "SID_serialDriverInf_ih.h"
#undef _INC_SID_MODULE
#include "I2C_efm32Driver_ih.h"
#include "BCDS_Assert.h"

/* own header files */
#include "I2C_ih.h"
#include "BCDS_PowerMgt.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
uint8_t IF_IN_DELAY_FLAG;
/* Bytes which need to be sent*/
uint8_t I2C_i2csendData[SID_SERIAL_CHANNEL_MAX][I2C_SEND_DATA_COPY_SIZE];
/* Bytes which need to be stored temporarily. ( This variable may be used for if we want to pass the data between two transfer )*/
uint8_t I2C_i2ctempData[SID_SERIAL_CHANNEL_MAX][I2C_TEMP_DATA_COPY_SIZE];
/* Bytes which is result of READ command.*/
uint8_t I2C_i2creadData[SID_SERIAL_CHANNEL_MAX][I2C_READ_DATA_COPY_SIZE];

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/* API documentation is in the interface header */
void I2C_init(I2C_init_tp i2c)
{
    assert(i2c != NULL);

    switch (i2c->i2cChannel)
    {
    case I2CCHANNEL_0:
        {
            PTD_pinModeSet(i2c->i2cDataPinConfig.port, i2c->i2cDataPinConfig.pin, i2c->i2cDataPinConfig.modeEnable, i2c->i2cDataPinConfig.doutEnable);
            PTD_pinModeSet(i2c->i2cClockPinConfig.port, i2c->i2cClockPinConfig.pin, i2c->i2cClockPinConfig.modeEnable, i2c->i2cClockPinConfig.doutEnable);
            SID_initSettings_t initSetting0;
            initSetting0.channel = SID_SERIAL_I2C_CHANNEL_0;
            initSetting0.noOfqueueItem = I2C_SERIAL_I2C_CHANNEL_QUEUE_SIZE;
            initSetting0.eachQueueItemSize = sizeof(I2C_interface_s);
            initSetting0.callbackFunc = (SID_callbackFunc) I2C_i2cDecodeCallbackFunction;
            initSetting0.location = i2c->i2cLocation;
            SID_init(initSetting0);
            break;
        }
    case I2CCHANNEL_1:
        {
            PTD_pinModeSet(i2c->i2cDataPinConfig.port, i2c->i2cDataPinConfig.pin, i2c->i2cDataPinConfig.modeEnable, i2c->i2cDataPinConfig.doutEnable);
            PTD_pinModeSet(i2c->i2cClockPinConfig.port, i2c->i2cClockPinConfig.pin, i2c->i2cClockPinConfig.modeEnable, i2c->i2cClockPinConfig.doutEnable);
            SID_initSettings_t initSetting1;
            initSetting1.channel = SID_SERIAL_I2C_CHANNEL_1;
            initSetting1.noOfqueueItem = I2C_SERIAL_I2C_CHANNEL_QUEUE_SIZE;
            initSetting1.eachQueueItemSize = sizeof(I2C_interface_s);
            initSetting1.callbackFunc = (SID_callbackFunc) I2C_i2cDecodeCallbackFunction;
            initSetting1.location = i2c->i2cLocation;
            SID_init(initSetting1);
            break;
        }
    default:
        {
            assert(0);
            break;
        }
    }
}

/* API documentation is in the interface header */
void I2C_delayFunction(uint32_t I2C_msecDelay)
{
    {
        Time_t i2c_time_called = PowerMgt_GetSystemTimeMs();
        while (PowerMgt_GetSystemTimeMs() < (i2c_time_called + (Time_t) I2C_msecDelay))
        {
            IF_IN_DELAY_FLAG = 1;
        }

        IF_IN_DELAY_FLAG = 0;
    }
}

/* API documentation is in the interface header */
void I2C_i2cDecodeCallbackFunction(void *i2cBuffer, SID_i2cTrnsfrInfo_t *infoTransfer)
{
    I2C_interface_s *buffer = (I2C_interface_s *) i2cBuffer;
    infoTransfer->trasferType = buffer->trasferType;
    infoTransfer->i2cChannel = buffer->i2cChannel;
    infoTransfer->i2cAddress = buffer->i2cAddress;
    infoTransfer->noOfSendData = buffer->noOfSendData;
    infoTransfer->noOfTempData = buffer->noOfTempData;
    infoTransfer->readLength = buffer->readLength;
    infoTransfer->callback = buffer->callback;
    infoTransfer->tempCallbackBuffer = buffer->tempCallbackBuffer;

    infoTransfer->sendData = I2C_i2csendData[SERIAL_i2cChannel2SidChannelConv[buffer->i2cChannel]];
    infoTransfer->tempData = I2C_i2ctempData[SERIAL_i2cChannel2SidChannelConv[buffer->i2cChannel]];
    infoTransfer->readData = I2C_i2creadData[SERIAL_i2cChannel2SidChannelConv[buffer->i2cChannel]];
    assert(I2C_SEND_DATA_COPY_SIZE > buffer->noOfSendData);
    assert(I2C_TEMP_DATA_COPY_SIZE > buffer->noOfSendData);
    uint8_t index;
    for (index = 0; index < buffer->noOfSendData; index++)
    {
        I2C_i2csendData[SERIAL_i2cChannel2SidChannelConv[buffer->i2cChannel]][index] = buffer->sendData[index];
    }
    for (index = 0; index < buffer->noOfTempData; index++)
    {
        I2C_i2ctempData[SERIAL_i2cChannel2SidChannelConv[buffer->i2cChannel]][index] = buffer->tempData[index];
    }
}

/* API documentation is in the interface header */
int8_t I2C_busWriteChannel0(unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = I2C_CHANNEL_0;
    i2cData.noOfSendData = wr_len + 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = 0;
    i2cData.sendData[0] = reg_addr;
    i2cData.trasferType = I2C_WRITE;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    assert(I2C_SEND_DATA_SIZE > wr_len);
    uint8_t index;
    for (index = 1; index <= wr_len; index++)
    {
        i2cData.sendData[index] = *data_p;
        data_p++;
    }
    while (SID_send(SID_SERIAL_I2C_CHANNEL_0, (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(I2C_CHANNEL_0) == I2C_TRANSFER_PROGRESS)
        ;
    return (I2C_getStatus());
}

/* API documentation is in the interface header */
int8_t I2C_busWriteChannel1(unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = I2C_CHANNEL_1;
    i2cData.noOfSendData = wr_len + 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = 0;
    i2cData.sendData[0] = reg_addr;
    i2cData.trasferType = I2C_WRITE;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    assert(I2C_SEND_DATA_SIZE > wr_len);
    uint8_t index;
    for (index = 1; index <= wr_len; index++)
    {
        i2cData.sendData[index] = *data_p;
        data_p++;
    }
    while (SID_send(SID_SERIAL_I2C_CHANNEL_1, (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(I2C_CHANNEL_1) == I2C_TRANSFER_PROGRESS)
        ;
    return (I2C_getStatus());
}

/* API documentation is in the interface header */
int8_t I2C_busWrite(I2C_channel_t i2cChannel, unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = i2cChannel;
    i2cData.noOfSendData = wr_len + 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = 0;
    i2cData.sendData[0] = reg_addr;
    i2cData.trasferType = I2C_WRITE;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    assert(I2C_SEND_DATA_SIZE > wr_len);
    uint8_t index;
    for (index = 1; index <= wr_len; index++)
    {
        i2cData.sendData[index] = *data_p;
        data_p++;
    }
    while (SID_send(SERIAL_i2cChannel2SidChannelConv[i2cChannel], (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(i2cChannel) == I2C_TRANSFER_PROGRESS)
        ;
    return (I2C_getStatus());
}

/* API documentation is in the interface header */
int8_t I2C_busReadChannel0(unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = I2C_CHANNEL_0;
    i2cData.noOfSendData = 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = r_len;
    i2cData.sendData[0] = targetAddr;
    i2cData.trasferType = I2C_WRITE_READ;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    while (SID_send(SID_SERIAL_I2C_CHANNEL_0, (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(i2cData.i2cChannel) == I2C_TRANSFER_PROGRESS)
        ; /* Polling method */
    assert(I2C_READ_DATA_COPY_SIZE > r_len);
    unsigned char i;
    for (i = 0; i < r_len; i++)
    {
        *reg_data = I2C_i2creadData[i2cData.i2cChannel][i];
        reg_data++;
    }
    memset(I2C_i2creadData[i2cData.i2cChannel], '\0', I2C_READ_DATA_COPY_SIZE);
    return (I2C_getStatus());
}

/* API documentation is in the interface header */
int8_t I2C_busReadChannel1(unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = I2C_CHANNEL_1;
    i2cData.noOfSendData = 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = r_len;
    i2cData.sendData[0] = targetAddr;
    i2cData.trasferType = I2C_WRITE_READ;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    while (SID_send(SID_SERIAL_I2C_CHANNEL_1, (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(i2cData.i2cChannel) == I2C_TRANSFER_PROGRESS)
        ; /* Polling method */
    assert(I2C_READ_DATA_COPY_SIZE > r_len);
    unsigned char i;
    for (i = 0; i < r_len; i++)
    {
        *reg_data = I2C_i2creadData[i2cData.i2cChannel][i];
        reg_data++;
    }
    memset(I2C_i2creadData[i2cData.i2cChannel], '\0', I2C_READ_DATA_COPY_SIZE);
    return (I2C_getStatus());
}

/* API documentation is in the interface header */
int8_t I2C_busRead(I2C_channel_t i2cChannel, unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len)
{

    I2C_interface_s i2cData;
    i2cData.i2cAddress = dev_addr;
    i2cData.i2cChannel = i2cChannel;
    i2cData.noOfSendData = 1;
    i2cData.noOfTempData = 0;
    i2cData.readLength = r_len;
    i2cData.sendData[0] = targetAddr;
    i2cData.trasferType = I2C_WRITE_READ;
    i2cData.tempData[0] = 0;
    i2cData.callback = NULL;
    i2cData.tempCallbackBuffer = NULL;
    while (SID_send(SERIAL_i2cChannel2SidChannelConv[i2cChannel], (void *) &i2cData) == 0)
        ;
    while (I2C_isTransferCompleted(i2cData.i2cChannel) == I2C_TRANSFER_PROGRESS)
        ; /* Polling method */
    assert(I2C_READ_DATA_COPY_SIZE > r_len);
    unsigned char i;
    for (i = 0; i < r_len; i++)
    {
        *reg_data = I2C_i2creadData[i2cData.i2cChannel][i];
        reg_data++;
    }
    return (I2C_getStatus());
}
