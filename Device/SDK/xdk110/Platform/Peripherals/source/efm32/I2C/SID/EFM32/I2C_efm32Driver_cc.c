/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */
#include <stdint.h>
#include "FreeRTOS.h"
#include "queue.h"

#include "em_i2c.h"
#include "em_cmu.h"
#include "I2C_ph.h"

/**! I2C driver is included in order to access I2C_transferType_t and I2C_channel_t */
/* own header files */
#include "I2C_efm32Driver_ch.h"
#include "I2C_efm32Driver_ih.h"
/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static int8_t I2C_returnStatus = I2C_FAIL; /**< Returning the status of the I2C Write or Read Status either success or failure*/

/* global variables ********************************************************* */
/*Array of structure to store the information regarding adress of device, data that to be transmitted using I2C  */
SID_i2cTrnsfrInfo_t I2C_trnsfrBufferISR_gas[SID_I2C_CHANNEL_MAX];
/*array of structure to store transfer mode and callback function */
extern SID_settings_t SID_setting_gas[SID_SERIAL_CHANNEL_MAX];
/* array of structure to store the transfer status */
I2C_transferStatus_t I2C_readProgressFlag_mae[SID_I2C_CHANNEL_MAX] = { I2C_TRANSFER_DONE, I2C_TRANSFER_DONE };

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

void I2C_initCtrSpecific(I2C_channel_t i2cLine, uint32_t i2cLocation)
{

    I2C_readProgressFlag_mae[i2cLine] = I2C_TRANSFER_DONE;

    if (i2cLine == I2C_CHANNEL_0)
    {
        CMU_ClockEnable(cmuClock_I2C0, true);

        /* Using default settings for I2C 0 */

        I2C_Init_TypeDef i2cInit_s = I2C0_INIT;

        /* Updates I/O Routing Register for I2C0*/
        I2C0->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | i2cLocation;

        /* Initializing the I2C0*/
        I2C_Init(I2C0, &i2cInit_s);

        /*Enable NVIC I2C Interrupts*/
        NVIC_ClearPendingIRQ(I2C0_IRQn);

        /*enable interrupts*/
        NVIC_EnableIRQ(I2C0_IRQn);

        I2C0->CMD = I2C_CMD_ABORT;

        I2C0->IEN = I2C_ENABLEBUSSHOLD_INTT;

    }

#if (I2C_COUNT > 1)
    else if (i2cLine ==I2C_CHANNEL_1)
    {

        CMU_ClockEnable(cmuClock_I2C1, true);

        /* Using default settings for I2C 1 */

        I2C_Init_TypeDef i2cInit_s = I2C1_INIT;

        /* Updates I/O Routing Register for I2C1*/
        I2C1 ->ROUTE = I2C_ROUTE_SDAPEN | I2C_ROUTE_SCLPEN | i2cLocation;

        /* Initializing the I2C1*/
        I2C_Init(I2C1, &i2cInit_s);

        /*Enable NVIC I2C Interrupts*/
        NVIC_ClearPendingIRQ(I2C1_IRQn);

        /* enable interrupts*/
        NVIC_EnableIRQ(I2C1_IRQn);

        I2C1->CMD = I2C_CMD_ABORT;

        I2C1->IEN = I2C_ENABLEBUSSHOLD_INTT;

    }
#endif
}

/* API documentation is in the interface header */
I2C_etxStatus_t I2C_startTransfer(I2C_channel_t i2cLine)
{
    I2C_etxStatus_t retVal = I2C_ETXFAIL;
    if (i2cLine < SID_I2C_CHANNEL_MAX)
    {
        I2C_readProgressFlag_mae[i2cLine] = I2C_TRANSFER_INPROGRESS;
        I2C_TypeDef *i2c;
        if (i2cLine == I2C_CHANNEL_0)
        {
            i2c = I2C0;
        }
#if (I2C_COUNT > 1)
        else if (i2cLine ==I2C_CHANNEL_1 )
        {
            i2c = I2C1;
        }
#endif
        i2c->TXDATA = I2C_trnsfrBufferISR_gas[i2cLine].i2cAddress << I2C_SLV_RW_BIT_SHIFT_POS;
        if (I2C_trnsfrBufferISR_gas[i2cLine].trasferType == I2C_READ)
        {
            i2c->TXDATA |= I2C_RD_SLV_DATA_BIT_VAL;
        }
        i2c->CMD = I2C_CMD_START;
        retVal = I2C_ETXSUCCESS;
    }
    return retVal;
}

void I2C0_IRQHandler(void)
{
    static uint8_t I2C_stateCh0 = 0, I2C_readCountCh0 = 0, I2C_writeCountCh0 = 0;
    I2C_transferStatus_t transerStatus = I2C_TRANSFER_INPROGRESS;
    I2C_TypeDef *i2c = I2C0;
    I2C_channel_t channelIndex = I2C_CHANNEL_0;
    switch (i2c->STATE & 0xFF)
    {
    case I2C_ACK_START:
        if (I2C_stateCh0 == 0)
        {
            i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].i2cAddress << I2C_SLV_RW_BIT_SHIFT_POS;
            I2C_stateCh0 = 1;
        }
        else if (I2C_trnsfrBufferISR_gas[channelIndex].trasferType == I2C_WRITE_READ)
        {
            i2c->TXDATA = (I2C_trnsfrBufferISR_gas[channelIndex].i2cAddress << I2C_SLV_RW_BIT_SHIFT_POS) | 1;
            I2C_stateCh0 = 0;
        }
        transerStatus = I2C_TRANSFER_INPROGRESS;
        break;
    case I2C_ACK_DEVICEADDRESS:
        i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].sendData[0];
        transerStatus = I2C_TRANSFER_INPROGRESS;
        break;
    case I2C_ACK_DATAREAD:
        if ((I2C_trnsfrBufferISR_gas[channelIndex].readLength - 1) > (I2C_readCountCh0))
        {
            I2C_trnsfrBufferISR_gas[channelIndex].readData[I2C_readCountCh0] = i2c->RXDATA;
            i2c->CMD = I2C_CMD_ACK;
            I2C_readCountCh0++;
            transerStatus = I2C_TRANSFER_INPROGRESS;
        }
        else
        {
            I2C_trnsfrBufferISR_gas[channelIndex].readData[I2C_readCountCh0] = i2c->RXDATA;
            i2c->CMD = I2C_CMD_NACK | I2C_CMD_STOP;
            I2C_readCountCh0 = 0;
            I2C_writeCountCh0 = 0;
            I2C_stateCh0 = 0;
            transerStatus = I2C_TRANSFER_DONE;
            I2C_returnStatus = I2C_SUCCESS;
        }
        break;
    case I2C_ACK_DATAWRITE:
        if ((I2C_trnsfrBufferISR_gas[channelIndex].noOfSendData - 1) > I2C_writeCountCh0)
        {
            i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].sendData[I2C_writeCountCh0 + 1];
            I2C_writeCountCh0++;
            transerStatus = I2C_TRANSFER_INPROGRESS;
        }
        else
        {
            if (I2C_trnsfrBufferISR_gas[channelIndex].trasferType == I2C_WRITE_READ)
            {
                i2c->CMD = I2C_CMD_START;
                transerStatus = I2C_TRANSFER_INPROGRESS;
                /* Since Slave address is updated in I2C_startTransfer function, Start Ack will not be provided,
                 * instead it will direct to Device address Ack
                 */
                I2C_stateCh0 = 1;
            }
            else
            {
                i2c->CMD = I2C_CMD_STOP;
                I2C_writeCountCh0 = 0;
                I2C_readCountCh0 = 0;
                I2C_stateCh0 = 0;
                transerStatus = I2C_TRANSFER_DONE;
                I2C_returnStatus = I2C_SUCCESS;
            }
        }
        break;
    default:
        i2c->CMD = I2C_CMD_STOP;
        transerStatus = I2C_TRANSFER_NACK;
        I2C_writeCountCh0 = 0;
        I2C_readCountCh0 = 0;
        I2C_stateCh0 = 0;
        I2C_returnStatus = I2C_FAIL;
        break;
    }
    if (transerStatus != I2C_TRANSFER_INPROGRESS)
    {
        if (I2C_trnsfrBufferISR_gas[channelIndex].callback != NULL)
        {
            I2C_trnsfrBufferISR_gas[channelIndex].callback(&I2C_trnsfrBufferISR_gas[channelIndex]);
        }
        /* Occur the next read data from the I2C Instruction Queue from ISR */
        if (I2C_triggerNextI2CTransfer(channelIndex) == 0)
        {
            I2C_readProgressFlag_mae[channelIndex] = I2C_TRANSFER_DONE;
        }
    }
}

#if (I2C_COUNT > 1)

void I2C1_IRQHandler(void)
{
    static uint8_t I2C_stateCh1 = 0,I2C_readCountCh1 = 0,I2C_writeCountCh1 = 0;
    I2C_transferStatus_t transerStatus=I2C_TRANSFER_INPROGRESS;
    I2C_TypeDef *i2c = I2C1;
    I2C_channel_t channelIndex = I2C_CHANNEL_1;
    switch(i2c->STATE & 0xFF)
    {
        case I2C_ACK_START:
        if(I2C_stateCh1 == 0)
        {
            i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].i2cAddress<<I2C_SLV_RW_BIT_SHIFT_POS;
            I2C_stateCh1 = 1;
        }
        else if(I2C_trnsfrBufferISR_gas[channelIndex].trasferType == I2C_WRITE_READ)
        {
            i2c->TXDATA = (I2C_trnsfrBufferISR_gas[channelIndex].i2cAddress<<I2C_SLV_RW_BIT_SHIFT_POS) | 1;
            I2C_stateCh1 = 0;
        }
        transerStatus = I2C_TRANSFER_INPROGRESS;
        break;
        case I2C_ACK_DEVICEADDRESS:
        i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].sendData[0];
        transerStatus = I2C_TRANSFER_INPROGRESS;
        break;
        case I2C_ACK_DATAREAD:
        if((I2C_trnsfrBufferISR_gas[channelIndex].readLength -1) > (I2C_readCountCh1))
        {
            I2C_trnsfrBufferISR_gas[channelIndex].readData[I2C_readCountCh1] = i2c->RXDATA;
            i2c->CMD = I2C_CMD_ACK;
            I2C_readCountCh1++;
            transerStatus = I2C_TRANSFER_INPROGRESS;
        }
        else
        {
            I2C_trnsfrBufferISR_gas[channelIndex].readData[I2C_readCountCh1] = i2c->RXDATA;
            i2c->CMD = I2C_CMD_NACK | I2C_CMD_STOP;
            I2C_readCountCh1 = 0;
            I2C_writeCountCh1 = 0;
            I2C_stateCh1 = 0;
            transerStatus = I2C_TRANSFER_DONE;
            I2C_returnStatus = I2C_SUCCESS;
        }
        break;
        case I2C_ACK_DATAWRITE:
        if ((I2C_trnsfrBufferISR_gas[channelIndex].noOfSendData - 1) > I2C_writeCountCh1)
        {
            i2c->TXDATA = I2C_trnsfrBufferISR_gas[channelIndex].sendData[I2C_writeCountCh1 + 1];
            I2C_writeCountCh1++;
            transerStatus = I2C_TRANSFER_INPROGRESS;
        }
        else
        {
            if (I2C_trnsfrBufferISR_gas[channelIndex].trasferType == I2C_WRITE_READ)
            {
                i2c->CMD = I2C_CMD_START;
                transerStatus = I2C_TRANSFER_INPROGRESS;
                /* Since Slave address is updated in I2C_startTransfer function, Start Ack will not be provided,
                 * instead it will direct to Device address Ack */
                I2C_stateCh1 = 1;
            }
            else
            {
                i2c->CMD = I2C_CMD_STOP;
                I2C_writeCountCh1 = 0;
                I2C_readCountCh1 = 0;
                I2C_stateCh1 = 0;
                transerStatus = I2C_TRANSFER_DONE;
                I2C_returnStatus = I2C_SUCCESS;
            }
        }
        break;
        default:
        i2c->CMD = I2C_CMD_STOP;
        transerStatus = I2C_TRANSFER_NACK;
        I2C_writeCountCh1 = 0;
        I2C_readCountCh1 = 0;
        I2C_stateCh1 = 0;
        I2C_returnStatus = I2C_FAIL;
        break;
    }
    if(transerStatus != I2C_TRANSFER_INPROGRESS )
    {
        if(I2C_trnsfrBufferISR_gas[channelIndex].callback != NULL)
        {
            I2C_trnsfrBufferISR_gas[channelIndex].callback(&I2C_trnsfrBufferISR_gas[channelIndex]);
        }
        /* Occur the next read data from the I2C Instruction Queue from ISR */
        if(I2C_triggerNextI2CTransfer(channelIndex)==0)
        {
            I2C_readProgressFlag_mae[channelIndex] = I2C_TRANSFER_DONE;
        }
    }
}
#endif

/* API documentation is in the interface header */
void I2C_drvInit(I2C_channel_t i2cLine, uint32_t i2cLocation)
{
    I2C_initCtrSpecific(i2cLine, i2cLocation);
}

/* API documentation is in the interface header */
I2C_etxStatus_t I2C_triggerTransfer(SID_channel_t channel, const void*bufferData)
{
    I2C_channel_t i2cLine = SERIAL_channelToLocalChannelConversion[channel];
    if (I2C_isTransferCompleted(i2cLine))
    {
        if (SID_setting_gas[channel].decodeCallbackFunction != NULL)
        {
            SID_setting_gas[channel].decodeCallbackFunction(bufferData, (void *) &I2C_trnsfrBufferISR_gas[i2cLine]);
        }
        else
        {
            I2C_trnsfrBufferISR_gas[i2cLine] = *((SID_i2cTrnsfrInfo_t*) bufferData);
        }
        I2C_startTransfer(i2cLine);
        return I2C_ETXSUCCESS;
    }
    return I2C_ETXFAIL;
}

/* API documentation is in the interface header */
I2C_etxStatus_t I2C_triggerNextI2CTransfer(I2C_channel_t i2cLine)
{
    I2C_etxStatus_t status;
    SID_channel_t channel = SERIAL_i2cChannel2SidChannelConv[i2cLine];
    /* Check whether interrupt mode is enabled. If not then no element are in queue.*/
    if (SID_setting_gas[channel].sampleMode == SID_INTERRUPT_MODE)
    {
        status = xQueueReceiveFromISR(SID_setting_gas[channel].queueHandle, SID_setting_gas[channel].tempBuffer, NULL);
        if (pdPASS == status)
        {
            if (SID_setting_gas[channel].decodeCallbackFunction != NULL)
            {
                SID_setting_gas[channel].decodeCallbackFunction(SID_setting_gas[channel].tempBuffer, (void *) &I2C_trnsfrBufferISR_gas[i2cLine]);
            }
            else
            {
                SID_i2cTrnsfrInfo_t *newI2cBuffer = (SID_i2cTrnsfrInfo_t *) SID_setting_gas[channel].tempBuffer;
                I2C_trnsfrBufferISR_gas[i2cLine] = *newI2cBuffer;
            }
            I2C_startTransfer(i2cLine);
            return I2C_ETXSUCCESS;
        }
    }
    return I2C_ETXFAIL;
}

/* API documentation is in the interface header */
inline I2C_transferCompleteStatus_t I2C_isTransferCompleted(I2C_channel_t i2cLine)
{
    if (I2C_readProgressFlag_mae[i2cLine] == I2C_TRANSFER_INPROGRESS)
    {
        return (I2C_TRANSFER_PROGRESS);
    }
    else
    {
        return (I2C_TRANSFER_COMPLETED);
    }
}

/* API documentation is in the interface header */
inline int8_t I2C_getStatus(void)
{
    return (I2C_returnStatus);
}

/** ************************************************************************* */
