/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef WDG_WATCHDOG_CH_H_
#define WDG_WATCHDOG_CH_H_
/* According to EM lib version V3.20.2, a new macro has been introduced in WDG module (WDOG_COUNT).
 * Only when this module is defined and its value is greater than 1, the header files of this module is included.
 * Hence, that macro is defined here.
 */
#define WDOG_COUNT 1
#define WDG_ENABLE  0x01
#define WDG_DISABLE 0x00
#define WDG_CLK_RATE_HIGH 32000
#define WDG_CLK_RATE_LOW 1000


#define WDG_INIT_LFXO                                                                       \
  { false,               /* Do not Start watchdog when init done */                                      \
    false,              /* WDOG not counting during debug halt */                                \
    false,              /* WDOG not counting when in EM2 */                                      \
    false,              /* WDOG not counting when in EM3 */                                      \
    false,              /* EM4 can be entered */                                                 \
    false,              /* Do not block disabling LFRCO/LFXO in CMU */                           \
    false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */ \
    wdogClkSelLFXO,     /* Select 32768HZ LFXO as clock source for WDOG oscillator */            \
    wdogPeriod_256k     /* Set longest possible timeout period */                                \
  }

#define WDG_INIT_LFRCO                                                                       \
  { false,               /* Do not Start watchdog when init done */                                      \
    false,              /* WDOG not counting during debug halt */                                \
    false,              /* WDOG not counting when in EM2 */                                      \
    false,              /* WDOG not counting when in EM3 */                                      \
    false,              /* EM4 can be entered */                                                 \
    false,              /* Do not block disabling LFRCO/LFXO in CMU */                           \
    false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */ \
	wdogClkSelLFRCO,    /* Select 32000HZ LFRCO as clock source for WDOG oscillator */           \
    wdogPeriod_256k     /* Set longest possible timeout period */                                \
  }

#define WDG_INIT_ULFRCO                                                                      \
  { false,               /* Do not Start watchdog when init done */                                      \
    false,              /* WDOG not counting during debug halt */                                \
    false,              /* WDOG not counting when in EM2 */                                      \
    false,              /* WDOG not counting when in EM3 */                                      \
    false,              /* EM4 can be entered */                                                 \
    false,              /* Do not block disabling LFRCO/LFXO in CMU */                           \
    false,              /* Do not lock WDOG configuration (if locked, reset needed to unlock) */ \
	wdogClkSelULFRCO,    /* Select 32000HZ LFRCO as clock source for WDOG oscillator */           \
    wdogPeriod_256k     /* Set longest possible timeout period */                                \
  }
/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */



#endif /* WDG_WATCHDOG_CH_H_ */

/** ************************************************************************* */
