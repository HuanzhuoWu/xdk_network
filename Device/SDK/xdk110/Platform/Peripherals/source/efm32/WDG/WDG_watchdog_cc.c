/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */
#include "em_wdog.h"
#include "em_rmu.h"
#include "LFO_LowFrequncyOscSel_ih.h"
#include "WDG_watchdog_ih.h"
/* own header files */
#include "WDG_watchdog_ch.h"

/**
 * @defgroup hal Hardware Abstract Layer (HAL)
 * @{
 * @defgroup Watchdog
 * @ingroup Watchdog
 * @{
 * @brief This module describes the usage of watchdog timer.
 * @details
 * Watchdog timer will trigger the reset whenever any deadlocks occured in the system,
 * configuaration of the watchdog timer based on the timeout periods .
 * Knowing the last reset cause of the system by internal managed reset cause unit.
 * Reset is based on the how did system was reset by externally or internally.These
 * are the resets and its values,Return value from the reset cause function.
 *
 * WDG_RESETCAUSE_PORST - Power on reset(Integer value-1)
 * WDG_RESETCAUSE_BODUNREGRST - Brown out detector, unregulated power(Integer value-2)
 * WDG_RESETCAUSE_BODREGRST - Brown out detector, regulated power(Integer vlue-4)
 * WDG_RESETCAUSE_EXTRST - External reset(integer value-8)
 * WDG_RESETCAUSE_WDOGRST - Watchdog reset(Integer value-16)
 * WDG_RESETCAUSE_LOCKUPRST - Cortex-M3 lockup reset(32)
 * WDG_RESETCAUSE_SYSREQRST - Cortex-M3 system request reset(64)
 * WDG_RESETCAUSE_EM4RST - Set if the system has been in EM4(128)
 * WDG_RESETCAUSE_EM4WURST - Set if the system woke up on a pin from EM4(256)
 * WDG_RESETCAUSE_BODAVDD0 - Analog power domain 0 brown out detector reset(512)
 * WDG_RESETCAUSE_BODAVDD1 - Analog power domain 1 brown out detector reset(1024)
 * WDG_RESETCAUSE_BUBODVDDDREG - Backup BOD on VDDD_REG triggered
 * WDG_RESETCAUSE_BUBODBUVIN - Backup BOD on BU_VIN triggered
 * WDG_RESETCAUSE_BUBODUNREG - Backup BOD on unregulated power triggered
 * WDG_RESETCAUSE_BUBODREG - Backup BOD on regulated powered has triggered
 * WDG_RESETCAUSE_BUMODERST - System has been in Backup mode
 *
 *
 ******************************************************************************/
/* Suggested default config for WDOG init structure. */


/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/*
  *  @brief
  *      API to convert timeout period in seconds to clock periods
  *
  *  @param[ in ] TimeoutInms holds the timeout period in milliseconds
  *  @param [ in ] freqncRate holds the frequency rate(1kz and 32khz).
  *
  *  @retval
  *      Returns the timeout periods in clock periods
  *
  *  @note
  *
  *  This function must be called whenever one need the timeout periods
  *  in number of clock periods. Its taking timeout periods in milliseconds and
  *  convert into number of clock periods based on the frequency rate of 1khz and 32khz.
  *
  *
  *
  */

static WDOG_PeriodSel_TypeDef WDG_secondsconvr(uint32_t freqncRate, uint32_t timeoutInms)
{
    uint32_t clkPeriods[20]={9,17,33,65,129,257,513,1025,2049,4097,8193,16385,32769,65537,131073,262145};
	WDOG_PeriodSel_TypeDef clkInper[20]={ wdogPeriod_9,
			 	 	 	 	 	 	 	  wdogPeriod_17,
			 	 	 	 	 	 	 	  wdogPeriod_33,
			 	 	 	 	 	 	 	  wdogPeriod_65,
			 	 	 	 	 	 	 	  wdogPeriod_129,
			 	 	 	 	 	 	 	  wdogPeriod_257,
	    		                          wdogPeriod_513,
	    		                          wdogPeriod_1k,
	    		                          wdogPeriod_2k,
	    		                          wdogPeriod_4k,
	    		                          wdogPeriod_8k,
	    		                          wdogPeriod_16k,
	    		                          wdogPeriod_32k,
	    		                          wdogPeriod_64k,
	    		                          wdogPeriod_128k,
	    		                          wdogPeriod_256k};
	uint32_t clkperInms = 0;
	int i;
	if(timeoutInms == 0)
	{
	    return wdogPeriod_256k;
	}
	if(freqncRate == WDG_CLK_RATE_HIGH)
	{
		clkperInms = 31.125 * timeoutInms;    /* if frequency is 32khz f=1/t,t=31.125 microseconds. */
	    for(i = 0; i <= 15; i++)
	    {
	    	if(clkperInms <= clkPeriods[i])
	    	{
	    		return clkInper[i];
		    }
	    }
	}
	else
	{
		clkperInms = 1 * timeoutInms;      /* if frequency is 1khz f=1/t,t=1 milliseconds. */
	    for(i=0; i <= 15; i++)
	    {
	    	if(clkperInms <= clkPeriods[i])
		   	{
	    		return clkInper[i];
		    }
	    }
	}
	return wdogPeriod_256k;
}

/* global functions **********************************************************/


/**
 *  @brief
 *      API to initialize the WatchDog timer. Depends upon the timeout periods in milliseconds and frequency rate.
 *
 *  @param [ in ] timeoutInms - it holds the timeout period in milliseconds.
 *  @param [ in ] freqncRate - it holds the frequency rate of 1khz and 32khz
 *
 *  @retval
 *      None
 * *  @note
 * This function will be called at the time of system intialization. watchdog timer init based on the following parameters.
 * Frequency rate of 1khz and 32khz and watchdog timeout period in milliseconds. Its calculating based on the frequency rate.
 * Timeout period values will approximate nearest value in the array({9,17,33,65,129,257,513,1025,2049,4097,8193,16385,32769,65537,131073,262145}).
 *
 *
 */

void WDG_init(uint16_t freqncRate,uint32_t timeoutInms)
{
	LFO_lowFreqOsc_t getCurrentClockSource;
	if(freqncRate == WDG_CLK_RATE_HIGH)
	{
		getCurrentClockSource = LFO_getLowFreqOsc();
		switch (getCurrentClockSource)
		{
		case LFO_LFXO:
		{
			WDOG_Init_TypeDef WDG_initClkLfxo = WDG_INIT_LFXO;
			WDG_initClkLfxo.perSel = WDG_secondsconvr(WDG_CLK_RATE_HIGH,timeoutInms);
			WDOG_Init(&WDG_initClkLfxo);
			WDG_enableWdgTimer();
			break;
		}

		case LFO_LFRCO:
		{
			WDOG_Init_TypeDef WDG_initClkLfrco = WDG_INIT_LFRCO;
			WDG_initClkLfrco.perSel = WDG_secondsconvr(WDG_CLK_RATE_HIGH,timeoutInms);
			WDOG_Init(&WDG_initClkLfrco);
			WDG_enableWdgTimer();
			break;
		}

		case LFO_NONE:
		{
			/* error: No clock source is selected */
			break;
		}

		default:
			break;

		}

	}
	else
	{
		WDOG_Init_TypeDef WDG_initClkLfrco = WDG_INIT_ULFRCO;
		WDG_initClkLfrco.perSel = WDG_secondsconvr(WDG_CLK_RATE_LOW,timeoutInms);
		WDOG_Init(&WDG_initClkLfrco);
		WDG_enableWdgTimer();
	}
}


/**
 *  API to disable the WatchDog timer.
 *@param [ in ] None
 *
 *  @retval
 *      None
 *
 *  @note
 *
 *  This function must called before putting the system to sleep mode.
 *  Watch dog timer register value is made to zero when disabling the
 *  watch dog timer.
 *  Scenario:If sleep time is more than timeout period,then system needs to
 *  disable the watchdog timer.
 *
 */

void WDG_disableWdgTimer(void)
{
	bool disableWdg = WDG_DISABLE;
	WDOG_Enable(disableWdg);
}

/**
 *  @brief
 *      API to enable WatchDog Timer
 *  @param [ in ] None
 *
 *  @return
 *      None
 *
 *  @note
 *
 *  This function must be called whenever the system comes out
 *  of the sleep mode.Following scenarios are the usage of WDG_enableWdgTimer.
 *  Scenrio1:At the time of watchdog timer intialization.
 *  Scenrio2:After system comes out of the sleep mode.
 *
 */

void WDG_enableWdgTimer(void)
{
	bool enableWdg = WDG_ENABLE;
	WDOG_Enable(enableWdg);
}

/**
 *  @brief
 *      API to feed the WatchDog timer i.e, Resets the watchdog timer value to zero
 *
 *  @param [ in ] None
 *
 *  @retval
 *      None
 *
 *  @note
 *
 *  This function must be called periodically, in order to reset the watchdog timer value to "zero", to avoid unwanted system
 *  restart.
 *
 */
 void WDG_feedingWatchdog(void)
 {
	 /*Previous Implementation, Waiting here till WDOG_SYNCBUSY_CTRL busy bit is
	  * not busy, this takes CPU time without executing any other activity */

	 /* while ( WDOG->SYNCBUSY & WDOG_SYNCBUSY_CTRL ); */

	 /* New Implementation: The WDOG_SYNCBUSY_CMD and WDOG_SYNCBUSY_CTRL bits are checked, in case of both are not busy Watchdog
	  * was fed (i.e cleared ),else Watchdog was not fed. */

	 if (!(WDOG->SYNCBUSY & WDOG_SYNCBUSY_CMD) && !( WDOG->SYNCBUSY & WDOG_SYNCBUSY_CTRL))
	 {
		 WDOG_Feed();
	 }
 }


 /**
  *  @brief
  *      API to get last reset cause from the reset management unit
  *
  *  @param [ in ] None
  *
  *  @retval    .WDG_RESETCAUSE_PORST - Power on reset(Integer value-1)
  *  		    .WDG_RESETCAUSE_BODUNREGRST - Brown out detector, unregulated power(Integer value-2)
  *  		    .WDG_RESETCAUSE_BODREGRST - Brown out detector, regulated power(Integer value-4)
  *  		    .WDG_RESETCAUSE_EXTRST - External reset(integer value-8)
  *  		    .WDG_RESETCAUSE_WDOGRST - Watchdog reset(Integer value-16)
  *  		    .WDG_RESETCAUSE_LOCKUPRST - Cortex-M3 lockup reset(32)
  *  		    .WDG_RESETCAUSE_SYSREQRST - Cortex-M3 system request reset(64)
  *  		    .WDG_RESETCAUSE_EM4RST - Set if the system has been in EM4(128)
  *  		    .WDG_RESETCAUSE_EM4WURST - Set if the system woke up on a pin from EM4(256)
  *  		    .WDG_RESETCAUSE_BODAVDD0 - Analog power domain 0 brown out detector reset(512)
  *  		    .WDG_RESETCAUSE_BODAVDD1 - Analog power domain 1 brown out detector reset(1024)
  *  		    .WDG_RESETCAUSE_BUBODVDDDREG - Backup BOD on VDDD_REG triggered
  *  		    .WDG_RESETCAUSE_BUBODBUVIN - Backup BOD on BU_VIN triggered
  *  		    .WDG_RESETCAUSE_BUBODUNREG - Backup BOD on unregulated power triggered
  *  		    .WDG_RESETCAUSE_BUBODREG - Backup BOD on regulated powered has triggered
  *  		    .WDG_RESETCAUSE_BUMODERST - System has been in Backup mode
  *
  *  @note
  *  This function can be called whenever the cause of the last reset of the system is needed. The function will return the integer values based
  *  on the reset management configured values for the all system resets.Reset cause register will be cleared by the RMU_ResetCauseClear
  *  function automatically after call of this function. If passed value is not present in their array means, api will approximate nearest value
  *  and return back the value.
  *
  */

uint16_t WDG_lastResetCause(void)
{
	uint16_t resetCause;
	resetCause = RMU_ResetCauseGet();
	RMU_ResetCauseClear();
	return resetCause;
}

/**@} */
 /**@} */







