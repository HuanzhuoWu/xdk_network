/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include "stddef.h"
#include "stdbool.h"

/* additional interface header files */
#include "em_rtc.h"
#include "em_cmu.h"
#include "BCDS_Assert.h"
#include "RTC_realTimeCounter_ph.h"

/* own header files */
#include "RTC_realTimeCounter_ih.h"
#include "RTC_realTimeCounter_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/** This global varaible which will be incremented by RTC_OVERFLOW_TIME on every RTC tick with */
static uint32_t RTC_timeSeconds;
static RTC_callback overFlowCallbackFunction; /**< variable to hold the callback function of overflow interrupt*/
static RTC_callback comp0CallbackFunction; /**< variable to hold the callback function of comp0 interrupt*/
static RTC_callback comp1CallbackFunction; /**< variable to hold the callback function of comp1 interrupt*/

/* global functions ********************************************************* */

/* The Api description is in the interface header File */
void RTC_initTimer(RTC_configFeature_t startCountAfterInit)
{
    RTC_Init_TypeDef RTC_InitConfig;
    /* use comp0Top to give interrupt on match */
    RTC_InitConfig.comp0Top = RTC_USE_COM0TOP;

    /* In debug RTC will be stopped */
    RTC_InitConfig.debugRun = false;

    /* leave disabled after intialisation */
    RTC_InitConfig.enable = startCountAfterInit;

    /* Select LFXO as clock source for LFACLK */
    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);

    /* divide the clock to increase overflow , it will make system wake less often */
    CMU_ClockDivSet(cmuClock_RTC, RTC_CLOCK_DIVISION_FACTOR);

    /* enable the clock fro RTC */
    CMU_ClockEnable(cmuClock_RTC, true);

    /* Low energy clocking module clock */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Enable LFRCO oscillator */
    CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);

    /* Enable External Interrupt */
    NVIC_EnableIRQ(RTC_IRQn);

    /* Initialize the RTC */
    RTC_Init(&RTC_InitConfig);

    /*  Clearing all interrupts */
    RTC->IFC = (RTC_IFC_OF | RTC_IFC_COMP0 | RTC_IFC_COMP1);

    /* Disabling all interrupts */
    RTC->IEN &= ~(RTC_IEN_COMP0 | RTC_IEN_COMP1 | RTC_IFC_OF);

}

/* The Api description is in the interface header File */
void RTC_configTimer(RTC_configTimerInterrupt_t configParams)
{
    uint32_t convertedTicks;
    if (configParams.interruptConfig & RTC_ENABLE_OVERFLOW_INTERRUPT)
    {
        RTC_setOverflowInterrupt();

        if (NULL != configParams.overFlowCallbackFunction)
        {
            overFlowCallbackFunction = configParams.overFlowCallbackFunction;
        }
    }
    if (configParams.interruptConfig & RTC_ENABLE_COMP_0_INTERRUPT)
    {
        convertedTicks = convertToTicks(RTC_MICRO_SECONDS, configParams.comp0valueinUs);

        assert(convertedTicks < RTC_CNT_TIMER_MAX);

        assert(convertedTicks != 0);

        RTC_CompareSet(RTC_COMP0, convertedTicks);

        if (NULL != configParams.comp0CallbackFunction)
        {
            comp0CallbackFunction = configParams.comp0CallbackFunction;
        }

        RTC_setCompare0Interrupt();
    }
    if (configParams.interruptConfig & RTC_ENABLE_COMP_1_INTERRUPT)
    {
        convertedTicks = convertToTicks(RTC_MICRO_SECONDS, configParams.comp1valueinUs);

        assert(convertedTicks < RTC_CNT_TIMER_MAX);

        assert(convertedTicks != RTC_DEFAULT_VALUE);

        RTC_CompareSet(RTC_COMP1, convertedTicks);

        RTC_setCompare1Interrupt();

        if (NULL != configParams.comp1CallbackFunction)
        {
            comp1CallbackFunction = configParams.comp1CallbackFunction;
        }
    }

}

/* The Api description is in the interface header File */
void RTC_disableAllInterrupts(void)
{
    RTC->IEN &= ~(RTC_ENABLE_OVERFLOW_INTERRUPT | RTC_ENABLE_COMP_0_INTERRUPT | RTC_ENABLE_COMP_1_INTERRUPT);

    /* clear the callback functions */
    overFlowCallbackFunction = NULL;
    comp0CallbackFunction = NULL;
    comp1CallbackFunction = NULL;

}

void RTC_disableInterrupt(RTC_interruptType_t interruptName)
{
    RTC->IEN &= ~(interruptName);

    /* clear the callback function*/

    switch (interruptName)
    {
    case RTC_ENABLE_OVERFLOW_INTERRUPT:
        overFlowCallbackFunction = NULL;
        break;
    case RTC_ENABLE_COMP_0_INTERRUPT:
        comp0CallbackFunction = NULL;
        break;
    case RTC_ENABLE_COMP_1_INTERRUPT:
        comp1CallbackFunction = NULL;
        break;
    default:
        break;
    }

}

/* The Api description is in the interface header File */
uint64_t RTC_getTime(void)
{
    uint64_t retTime;
    uint32_t seconds;
    uint64_t currentCount;

    /*Getting Counter Value*/
    currentCount = RTC_getSystemTimerCountValue();

    seconds = RTC_timeSeconds;

    /* Returning the time in ms after converting CNT to ms and adding with expired time stored in RTC_timeSeconds*/
    retTime = seconds * RTC_CONVERT_TO_MSEC + (currentCount * RTC_OVERFLOW_TIME * RTC_CONVERT_TO_MSEC) / RTC_CNT_TIMER_MAX;

    return retTime;

}

/* The Api description is in the interface header File */
static uint32_t convertToTicks(RTC_unitsOfTime_t timeUnit, uint32_t time)
{

    assert(RTC_CLOCK_DIVISION_FACTOR !=0);

    uint64_t ticks;

    ticks = ((uint64_t)((uint64_t) (time * RTC_CLOCK_SOURCE))) / (RTC_CLOCK_DIVISION_FACTOR);

    switch (timeUnit)
    {
    case RTC_MICRO_SECONDS:
        ticks = (ticks / RTC_CONVERT_TO_USEC);
        break;
    case RTC_MILLI_SECONDS:
        ticks = (ticks / RTC_CONVERT_TO_MSEC);
        break;
    default:
        break;
    }

    return (ticks);

}

/* The Api description is in the interface header File */
void RTC_IRQHandler(void)
{

    /* if(RTC->IF & RTC_IF_OF) */
    if ((RTC->IF) & RTC_ENABLE_OVERFLOW_INTERRUPT)
    {
        /* Clear Cause of interrupt */
        RTC_clearInterrupt(RTC_ENABLE_OVERFLOW_INTERRUPT);

        /*  Increment Seconds by overflow */
        RTC_timeSeconds = RTC_timeSeconds + RTC_OVERFLOW_TIME;

        if (NULL != overFlowCallbackFunction)
        {
            overFlowCallbackFunction();

        }
    }
    if ((RTC->IF) & RTC_ENABLE_COMP_0_INTERRUPT)
    {
        /* clear cause of interrupt */
        RTC_clearInterrupt(RTC_ENABLE_COMP_0_INTERRUPT);

        if (NULL != comp0CallbackFunction)
        {
            comp0CallbackFunction();
        }

    }
    if ((RTC->IF) & RTC_ENABLE_COMP_1_INTERRUPT)
    {
        /* clear cause of interrupt */
        RTC_clearInterrupt(RTC_ENABLE_COMP_1_INTERRUPT);

        if (NULL != comp1CallbackFunction)
        {
            comp1CallbackFunction();
        }

    }
}

/* The Api description is in the interface header File */
void RTC_enable(void)
{
    RTC_Enable(true);
}

/* The Api description is in the interface header File */
void RTC_disable(void)
{
    RTC_Enable(false);
}

/* The Api description is in the interface header File */
void RTC_updateComp0ValueWithCount(uint32_t timeInUs)
{
    uint32_t convertedTicks;

    convertedTicks = convertToTicks(RTC_MICRO_SECONDS, timeInUs);
    convertedTicks = RTC_getSystemTimerCountValue() + convertedTicks;
    RTC_CompareSet(RTC_COMP0, convertedTicks);
}

/* The Api description is in the interface header File */
void RTC_updateComp1ValueWithCount(uint32_t timeInUs)
{
    uint32_t convertedTicks;

    convertedTicks = convertToTicks(RTC_MICRO_SECONDS, timeInUs);
    convertedTicks = RTC_getSystemTimerCountValue() + convertedTicks;
    RTC_CompareSet(RTC_COMP1, convertedTicks);
}

/** ************************************************************************* */
