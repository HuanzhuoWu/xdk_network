/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_RTC_REALTIMECOUNTER_CH_H_
#define BCDS_RTC_REALTIMECOUNTER_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

/** enum to represent the uints of the time*/
typedef enum RTC_unitsOfTime_e
{
    RTC_MICRO_SECONDS,
    RTC_MILLI_SECONDS,
} RTC_unitsOfTime_t;

#define RTC_OVERFLOW_TIME  RTC_CLOCK_DIVISION_FACTOR*512  /**< the overflow time of the RTC*/

#define RTC_COMP0           UINT8_C(0x00)                 /**< the address of compare channel 0 register of RTC*/
#define RTC_COMP1           UINT8_C(0x01)                 /**< the address of compare channel 1 register of RTC*/
#define RTC_CONVERT_TO_USEC UINT32_C(1000000)             /**< number of micro seconds for one second */
#define RTC_CONVERT_TO_MSEC UINT32_C(1000)                /**< number of milli seconds for one second */

#define RTC_CNT_TIMER_MAX   UINT32_C(0xFFFFFF)            /**< the maximum value of timer */
#define RTC_DEFAULT_VALUE   UINT32_C(0)

/** The function like macro implements a configure access provider for the RTC module's interrupt clear register. */
#define RTC_clearInterrupt(interrupt) \
(RTC->IFC = interrupt);

/** The function like macro implements to configure the RTC module's compare channel 0 register . */
#define RTC_setCompare0Interrupt()  (RTC->IEN |= RTC_ENABLE_COMP_0_INTERRUPT);

/** The function like macro implements to configure the RTC module's compare channel 1 register . */
#define RTC_setCompare1Interrupt()  (RTC->IEN |= RTC_ENABLE_COMP_1_INTERRUPT);

/** The function like macro implements to configure the RTC module's interrupt enable register. */
#define RTC_setOverflowInterrupt()  (RTC->IEN |= RTC_ENABLE_OVERFLOW_INTERRUPT);

/* local function prototype declarations */

/**
 * @brief      function to convert tzhe given time into the equivalentz ticks based on the configured clock
 *             and the prescaler value
 *
 * @param[in]  RTC_unitsOfTime_t - the passed "time" unit that need to be converted into equivalent ticks
 *
 * @param[in]  uint32_t - time
 *
 * @retval     uint32_t  the converted ticks
 *
 */
static uint32_t convertToTicks(RTC_unitsOfTime_t timeUnit, uint32_t time);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_RTC_REALTIMECOUNTER_CH_H_ */

/** ************************************************************************* */
