/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_system.h"
#include "em_timer.h"

/* additional interface header files */

/* own header files */
#include "LFO_LowFrequncyOscSel_ih.h"

/* constant definitions ***************************************************** */
/* overflow of 10 second @ 28MHz */
//set for 1 second
#define TIMER1_TOP    27342
#define LFXO_STABILIZATION_PERIOD_SEC 2
/* local variables ********************************************************** */
static uint8_t oneSecondTickCount = 0;
/* global variables ********************************************************* */
LFO_lowFreqOsc_t LFO_lowFreqOsc =LFO_NONE;
/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/** ************************************************************************* */


/**
 *  @brief
 *  CMU_IRQHandler
 *  Interrupt Service Routine CMU Interrupt Line
 *
 *  @param [ in ] None
 *
 *  @return
 *      None
 *
 *  @note
 *	Used at startup only to select LFRCO if LFXO is faulty
 */
void CMU_IRQHandler(void)
{
  /* Read Interrupt flags */
  uint32_t intFlags = CMU_IntGet();

  /* Clear interrupt flags register */
  CMU_IntClear(CMU_IF_LFXORDY);

  /* If LFXORDY interrupt occured select it as clock source for Low frequncy perpherals
   * for the LF clock branch and disable LFRCO (not in use anymore) */
  if (intFlags & CMU_IF_LFXORDY)
  {
	LFO_lowFreqOsc = LFO_LFXO;

  }
}

/**
 *  @brief
 *  TIMER1_IRQHandler
 *  Interrupt Service Routine TIMER1 Interrupt Line
 *
 *  @param [ in ] None
 *
 *  @return
 *      None
 *
 *  @note
 *	Used at startup only to select LFRCO if LFXO is faulty
 */
void TIMER1_IRQHandler(void)
{
	  /* Clear flag for TIMER1 overflow interrupt */
	  TIMER_IntClear(TIMER1, TIMER_IF_OF);

	if(oneSecondTickCount >= LFXO_STABILIZATION_PERIOD_SEC)
	{
	  LFO_lowFreqOsc = LFO_LFRCO;
	}
	oneSecondTickCount++;
}

/**
 *  @brief
 *  LFO_SelectLowFrequncyOscillator
 *  Used at startup only to select LFRCO if LFXO is faulty
 *
 *  @param [ in ] None
 *
 *  @return
 *      None
 *
 *  @note
 *
 */
void LFO_SelectLowFrequncyOscillator(void)
{

	/* Enable clock for TIMER1 module */
	  CMU_ClockEnable(cmuClock_TIMER1, true);


	  /* Select timer parameters */
	  TIMER_Init_TypeDef timerInit =
	  {
	    .enable     = false,
	    .debugRun   = false,
	    .prescale   = timerPrescale1024,
	    .clkSel     = timerClkSelHFPerClk,
	    .fallAction = timerInputActionNone,
	    .riseAction = timerInputActionNone,
	    .mode       = timerModeUp,
	    .dmaClrAct  = false,
	    .quadModeX4 = false,
	    .oneShot    = false,
	    .sync       = false,
	  };
	  /* Set TIMER Top value */
	  TIMER_TopSet(TIMER1, TIMER1_TOP);

	  /* Configure TIMER */
	  TIMER_Init(TIMER1, &timerInit);

	  /* Enable overflow interrupt */
	  TIMER_IntEnable(TIMER1, TIMER_IF_OF);


	  /* Enable TIMER1 interrupt vector in NVIC */
	  NVIC_EnableIRQ(TIMER1_IRQn);

	  /* Enable interrupts for LFXORDY  */
	  CMU_IntEnable(CMU_IF_LFXORDY);

	  /* Enable CMU interrupt vector in NVIC */
	  NVIC_EnableIRQ(CMU_IRQn);

      /* Enable LFXO without waiting for LFXORDY */
      CMU_OscillatorEnable(cmuOsc_LFXO, true, false);
      TIMER_Enable(TIMER1, true);

      //wait for LFA to be selected either LFXO or LFRCO
      while(LFO_lowFreqOsc == LFO_NONE); //we can also go to sleep mode EM1


		/* Disable interrupts for LFXORDY  */
	  CMU_IntDisable(CMU_IF_LFXORDY);
	  //disable the timer
	  TIMER_Enable(TIMER1, false);
		/* Disable CMU interrupt vector in NVIC */
	  NVIC_DisableIRQ(CMU_IRQn);


	  /* Disable overflow interrupt */
	  TIMER_IntDisable(TIMER1, TIMER_IF_OF);

		 /* Disable TIMER1 interrupt vector in NVIC */
	  NVIC_DisableIRQ(TIMER1_IRQn);

      CMU_ClockEnable(cmuClock_TIMER1, false);

      if(LFO_lowFreqOsc == LFO_LFRCO)
      {
    	  CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);
    	  CMU_OscillatorEnable(cmuOsc_LFXO, false, false);
    	  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);
      }
      else if(LFO_lowFreqOsc == LFO_LFXO)
      {
    	    CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFXO);
    	    CMU_OscillatorEnable(cmuOsc_LFRCO, false, false);
      }

}

/**
 *  @brief
 *  LFO_getLowFreqOsc
 *  Provides which LF Oscillator is being use LFXO or LFRCO
 *
 *  @param [ in ] None
 *
 *  @return
 *      None
 *
 *  @note
 *
 */
LFO_lowFreqOsc_t LFO_getLowFreqOsc(void)
{
	return LFO_lowFreqOsc;
}
