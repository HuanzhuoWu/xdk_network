/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

/* additional interface header files */

/* own header files */
#include "ADC_ih.h"

/* constant definitions ***************************************************** */

#define ADC_12_BIT_SAMPLING_MAX_VAL     (UINT32_C(4096))
#define ADC_8_BIT_SAMPLING_MAX_VAL      (UINT32_C(255))
#define ADC_6_BIT_SAMPLING_MAX_VAL      (UINT32_C(127))
#define ADC_1_25_REFERENCE_MAX_VOL      (UINT32_C(1250))
#define ADC_2_5_REFERENCE_MAX_VOL       (UINT32_C(2500))

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/* The description is in the interface header */
void ADC_init(ADC_node_tp adcNode)
{
	/* Enables clock for ADC instance */
	 CMU_ClockEnable(adcNode->cmuClk, true);

    /* Initialize common settings for both single conversion and scan mode */
	adcNode->init.timebase = ADC_TimebaseCalc(0);

    /* Sets ADC clock to 7 MHz, use default HFPERCLK */
	adcNode->init.prescale = ADC_PrescaleCalc(adcNode->adcFreq, 0);

	/* Calls for EFM32 API. */
	ADC_Init(adcNode->registerBaseAddress, &adcNode->init);
}

/* The description is in the interface header */
void ADC_pollSingleData(ADC_singleAcq_tp adcSingleAcq)
{
	ADC_TypeDef * adcInstance = adcSingleAcq->registerBaseAddress;

	/* Sets ADC channel to be used. */
	adcSingleAcq->initSingle.input = adcSingleAcq->adcChannel;

	/* Configures single aquisition mode. */
	ADC_InitSingle(adcInstance, &adcSingleAcq->initSingle);

	/* Starts ADC conversion. */
	ADC_Start(adcInstance, adcStartSingle);

    /* Waits while conversion is active */
    while (adcInstance->STATUS & ADC_STATUS_SINGLEACT)
        ;

    /** Aquires ADC result */
    adcSingleAcq->data = ADC_DataSingleGet(adcInstance);
}

/* The description is in the interface header */
uint32_t ADC_scaleAdcValue(ADC_singleAcq_tp adcSingleAcq)
{
	uint32_t scaledData = UINT32_C(0);
	uint32_t maxVoltageValue = UINT32_C(0);
	uint32_t maxValue = UINT32_C(0);

	switch(adcSingleAcq->initSingle.resolution)
	{
		case adcRes12Bit :  /* 12 bit sampling. */
		{
			maxValue = ADC_12_BIT_SAMPLING_MAX_VAL;
			break;
		}

		case adcRes8Bit :  	/* 8 bit sampling. */
		{
			maxValue = ADC_8_BIT_SAMPLING_MAX_VAL;
			break;
		}

		case adcRes6Bit : 	/* 6 bit sampling. */
		{
			maxValue = ADC_6_BIT_SAMPLING_MAX_VAL;
			break;
		}

		default:
			break;
	}

	switch(adcSingleAcq->initSingle.reference)
	{
		case adcRef1V25 :  	/* Internal 1.25V reference. */
		{
			maxVoltageValue = ADC_1_25_REFERENCE_MAX_VOL;
			break;
		}

		case adcRef2V5 :  	/* Internal 2.5V reference. */
		{
			maxVoltageValue = ADC_2_5_REFERENCE_MAX_VOL;
			break;
		}

		default:
			break;
	}

	/* Scale the result according to the precision and maximum value. */
	scaledData = (unsigned long) (adcSingleAcq->data * maxVoltageValue) / maxValue;

	return scaledData;
}

/** ************************************************************************* */
