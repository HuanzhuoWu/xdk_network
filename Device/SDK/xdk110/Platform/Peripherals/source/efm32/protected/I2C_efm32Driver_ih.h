/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef I2C_EFM32DRIVER_IH_H_
#define I2C_EFM32DRIVER_IH_H_

/* public interface declaration ********************************************* */

#include "SID_serialDriverInf_ih.h"

/**
 * @defgroup I2C I2C
 * @ingroup SID
 * @{
 * @brief This module discribes the usage of I2C.
 * @details
<b> Introduction </b>

Module is responsible:
 - Provides API for READ, WRITE and WRITE_READ.
 - Each I2C communication has its own queue. It makes the possible to trigger two communication at the same time.
 
<b>Architecture</b>

Higher Layer Module ( Application or Device Driver ) can start I2C communication using I2C_drv.
If a callback is provided while stating a transfer, then callback will be called once transfer is completed.

<img alt="architecture" src="../pics/I2CArchitecture.png">


<b> Highlights </b>

 - Module is fully working on I2C interrupt based method.
 - A callback will be triggered once any transfer is completed. And callback is given as pointer while initiating the transfer.
 - I2C command can be queued and driver will process it one after other without any delay.

<b> Points to be considered </b>

 If there is dependency between two I2C then it need to be handle in the higher layer. For example
 - As shown in below diagram, If reading value of XYZ BMA2x2_readXYZ() is depends on setting of BMA2x2_setABCD(), Then BMA2x2_readXYZ() shall be called only after or in the callback of BMA2x2_setABCD().

 <img alt="dependency" src="../pics/I2CDependency.png">

 - Similarly, If delay need to be provided between two I2C command, then each command shouldn't be consequently put in queue. Rather shall be triggered only after 1st command is completed and after providing necessary delay 2nd command need to be triggered.

*/

/* public type and macro definitions */
#define I2C_FAIL     INT8_C(-1) /**< data transmission failed      */
#define I2C_SUCCESS  INT8_C(0)  /**< data transmitted successfully */

/*type by which status is referenced*/
typedef enum I2C_eTxStatus_e
{
	I2C_ETXFAIL, 	/*data transmission failed */
	I2C_ETXSUCCESS, /* data transmitted successfully*/

} I2C_etxStatus_t;

/*type by which transfer status is referenced*/
typedef enum I2C_transferCompleteStatus_e
{
	I2C_TRANSFER_PROGRESS = 0 , /*data transmission failed */
	I2C_TRANSFER_COMPLETED     = 1 , /* data transmitted successfully*/

} I2C_transferCompleteStatus_t;

/** Return codes for single master mode transfer function. */
typedef enum I2C_transferStatus_e
{
	/* In progress code (>0) */
	I2C_TRANSFER_INPROGRESS = 1, /**< Transfer in progress. */

	/* Complete code (=0) */
	I2C_TRANSFER_DONE = 0, /**< Transfer completed successfully. */

	/* Transfer error codes (<0) */
	I2C_TRANSFER_NACK = -1, /**< NACK received during transfer. */
	I2C_TRANSFER_BUSERR = -2, /**< Bus error during transfer (misplaced START/STOP). */
	I2C_TRANSFER_ARBLOST = -3, /**< Arbitration lost during transfer. */
	I2C_TRANSFER_USAGEFAULT = -4, /**< Usage fault. */
	I2C_TRANSFER_SWFAULT = -5 /**< SW fault. */
} I2C_transferStatus_t;

#define I2C_RD_SLV_DATA_BIT_VAL        UINT32_C(0x01)
#define I2C_SLV_RW_BIT_SHIFT_POS   	   UINT32_C(0x01)

/* public function prototype declarations */

/**
 * @brief       The API initialize the I2C bus based on the channel and location given as input parameter
 *
 * @param [in]  i2cLine
 * 						I2C bus channel number
 *
 * @param [in]  i2cLocation
 * 						I2C channel location
 */
void I2C_initCtrSpecific(I2C_channel_t i2cLine,uint32_t i2cLocation);

/**
 * @brief       The API will issue start command to I2C bus and I2C transfer will  start
 *
 * @param [in]  i2cLine
 * 				I2C bus channel number
 *
 * @retval 		I2C_ETXFAIL  Valid START command was either given to available channel I2C0 or I2C1
 * 				I2C_ETXFAIL  I2C channel selection is wrong
 *
 */
I2C_etxStatus_t I2C_startTransfer(I2C_channel_t i2cLine);

/**
 * @brief       The I2C_drvInit API Initialization of I2C Driver
 *
 * @param [in]  i2cLine
 * 					I2C bus channel number
 *
 * @param [in]  i2cLocation
 * 					I2C channel location
 *
 */
void I2C_drvInit(I2C_channel_t i2cLine,uint32_t i2cLocation);

/**
 * @brief       The API transfers the bufferData on to the i2c channel
 *
 * @param [in]  i2cLine
 * 						I2C bus channel number
 *
 * @param [in]  Data that to be transfered on the i2c bus.
 *
 * @retval 	    I2C_ETXSUCCESS  I2C transfer success.
 * 				I2C_ETXFAIL 	I2C transfer  fail.
 *
 */
I2C_etxStatus_t I2C_triggerNextI2CTransfer(I2C_channel_t i2cLine);

/**
 * @brief       The API transfers the bufferData on to the i2c channel
 *
 * @param [in]  channel
 * 						I2C bus channel number
 *
 * @param [in]  bufferData
 * 						Pointer to the Data that to be transfered on the i2c bus
 *
 * @retval 		I2C_ETXSUCCESS   I2C transfer success.
 * 				I2C_ETXFAIL		 I2C transfer  fail.
 *
 */
I2C_etxStatus_t I2C_triggerTransfer(SID_channel_t channel,const void*bufferData);

/* public global variable declarations */

/* inline function definitions */

/**
 *  @brief      The API gets the status of the i2c channel
 *
 *  @retval 	I2C_ETXSUCCESS   I2C transfer success.
 * 				I2C_ETXFAIL		 I2C transfer  fail.
 *
 */
int8_t I2C_getStatus(void);

/*
 * @brief       The API gives the status of the I2C bus transfer complete status
 *
 * @param [in]  i2cLine
 * 						I2C bus channel number
 *
 * @retval 		I2C_TRANSFER_PROGRESS    I2C transfer Completed
 * 				I2C_TRANSFER_COMPLETED   I2C transfer Not Completed
 *
 */
I2C_transferCompleteStatus_t I2C_isTransferCompleted(I2C_channel_t i2cLine);

/**@}*/
#endif /* I2C_EFM32DRIVER_IH_H_ */

/****************************************************************************/
