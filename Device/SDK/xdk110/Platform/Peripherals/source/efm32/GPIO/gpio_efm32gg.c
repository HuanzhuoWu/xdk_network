/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/* module includes ********************************************************** */

/* system header files */
#include <stdlib.h>

/* additional interface header files */
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_int.h"
/* own header files */
#include "gpio.h"
#include "gpio_efm32gg.h"

/* constant definitions ***************************************************** */

/* type definitions ***************************************************** */

/* local variables ********************************************************** */

static _Bool GPIO_isInitializationRequired = true;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* This function is used to check the handle for correct values, before using it */
GPIO_errorTypes_t _GPIO_checkParameters(GPIO_handleInfo_tp handleInfo)
{
    /* Make sure handle pointer is not NULL */
    if (handleInfo == NULL )
        return GPIO_ERROR_HANDLE_IS_NULL;

    /* Check handle magic word. */
    if (handleInfo->magicWord != GPIO_HANDLE_MAGIC_WORD)
        return GPIO_ERROR_HANDLE_DATA_CORRUPTED;

    /* Make sure that port is valid */
    if (!GPIO_PORT_VALID(handleInfo->port))
        return GPIO_ERROR_INVALID_PORT;

    /* Make sure that pin is valid */
    if (!GPIO_PIN_VALID(handleInfo->bitIndex))
        return GPIO_ERROR_INVALID_PIN;

    /* Check if handle initialized. Must be last one, since we are using it in the init function and we don't want to skip tests when not initialized yet. */
    if (!handleInfo->initDone)
        return GPIO_ERROR_HANDLE_NOT_INITIALIZED;

    return GPIO_ERROR_OK;
}

/* global functions ********************************************************* */

/* API documentation is in the interface header */
GPIO_errorTypes_t GPIO_init(GPIO_handle_tp handle, GPIO_pinDirections_t direction, GPIO_pinStates_t defaultState)
{
    GPIO_handleInfo_tp handleInfo = (GPIO_handleInfo_tp) handle;

    /* Check handle parameters. Allow not initialized error only here. */
    GPIO_errorTypes_t error = _GPIO_checkParameters(handleInfo);
    if ((error != GPIO_ERROR_OK) && (error != GPIO_ERROR_HANDLE_NOT_INITIALIZED))
        return error;

    /* Make sure that requested direction is allowed. */
    if ((handleInfo->Direction != direction) && handleInfo->lockDirection)
        return GPIO_ERROR_WRONG_DIRECTION;

    /* Make sure that the direction is in range */
    if ((int) direction >= GPIO_DIRECTION_OUT_OF_RANGE)
        return GPIO_ERROR_WRONG_DIRECTION;

    /* Run all one time initializes. */
    if (GPIO_isInitializationRequired)
    {
        GPIO_isInitializationRequired = false;

        /* Enable GPIO in CMU */
        CMU_ClockEnable(cmuClock_GPIO, true);
    }

    // Store requested direction.
    handleInfo->Direction = direction;

    // Convert pin direction to hardware specific type.
    GPIO_Mode_TypeDef mode;
    if (direction == GPIO_DIRECTION_INPUT)
        /* @TODO push button fix, need to check */
        mode = gpioModeInputPullFilter;
    else
        mode = gpioModePushPull;

    /* Set pin configuration*/
    /* @TODO push button fix, need to check */
    GPIO_PinModeSet(handleInfo->port, handleInfo->bitIndex, mode, 1);

    // Update init done flag.
    handleInfo->initDone = true;

    /* Set default state */
    GPIO_setPin(handle, defaultState);

    return GPIO_ERROR_OK;
}

/* API documentation is in the interface header */
GPIO_errorTypes_t GPIO_deInit(GPIO_handle_tp handle)
{
    GPIO_handleInfo_tp handleInfo = (GPIO_handleInfo_tp) handle;

    /* Check handle parameters. */
    GPIO_errorTypes_t error = _GPIO_checkParameters(handleInfo);
    if (error != GPIO_ERROR_OK)
        return error;

    /* Disable pin */
    GPIO_PinModeSet(handleInfo->port, handleInfo->bitIndex, gpioModeDisabled, 0);

    return GPIO_ERROR_OK;
}

/* API documentation is in the interface header */
GPIO_pinStates_t GPIO_getPin(GPIO_handle_tp handle)
{
    GPIO_handleInfo_tp handleInfo = (GPIO_handleInfo_tp) handle;

    /* Check handle parameters. */
    GPIO_errorTypes_t error = _GPIO_checkParameters(handleInfo);
    if (error != GPIO_ERROR_OK)
        return GPIO_STATE_OUT_OF_RANGE;

    if (GPIO_PinInGet(handleInfo->port, handleInfo->bitIndex))
        return GPIO_STATE_ON;

    return GPIO_STATE_OFF;
}

/* API documentation is in the interface header */
GPIO_errorTypes_t GPIO_setPin(GPIO_handle_tp handle, GPIO_pinStates_t state)
{
    GPIO_handleInfo_tp handleInfo = (GPIO_handleInfo_tp) handle;

    /* Check handle parameters. */
    GPIO_errorTypes_t error = _GPIO_checkParameters(handleInfo);
    if (error != GPIO_ERROR_OK)
        return error;

    /* Make sure that direction is output. */
    if (handleInfo->Direction != GPIO_DIRECTION_OUTPUT)
        return GPIO_ERROR_WRONG_DIRECTION;

    switch (state)
    {
    case GPIO_STATE_OFF:
        GPIO_PinOutClear(handleInfo->port, handleInfo->bitIndex);
        break;
    case GPIO_STATE_ON:
        GPIO_PinOutSet(handleInfo->port, handleInfo->bitIndex);
        break;
    default:
        return GPIO_ERROR_OUT_OF_RANGE;
    }

    return GPIO_ERROR_OK;
}

/* API documentation is in the interface header */
GPIO_errorTypes_t GPIO_togglePin(GPIO_handle_tp handle)
{
    GPIO_handleInfo_tp handleInfo = (GPIO_handleInfo_tp) handle;

    /* Check handle parameters. */
    GPIO_errorTypes_t error = _GPIO_checkParameters(handleInfo);
    if (error != GPIO_ERROR_OK)
        return error;

    /* Make sure that direction is output. */
    if (handleInfo->Direction != GPIO_DIRECTION_OUTPUT)
        return GPIO_ERROR_WRONG_DIRECTION;

    GPIO_PinOutToggle(handleInfo->port, handleInfo->bitIndex);

    return GPIO_ERROR_OK;
}

/* API documentation is in the interface header */
GPIO_pinStates_t GPIO_Invert(GPIO_pinStates_t state)
{
    return ((state == GPIO_STATE_OFF) ? GPIO_STATE_ON : GPIO_STATE_OFF);
}
