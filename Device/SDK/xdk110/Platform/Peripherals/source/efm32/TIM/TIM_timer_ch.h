/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_TIM_TIMER_CH_H_
#define BCDS_TIM_TIMER_CH_H_

/* local interface declaration ********************************************** */
#include "TIM_timer_ph.h"

/* local type and macro definitions */

#define POWER_OF_TWO(X)						((X) * (X))							/**<  Equation for Square Functionality */

#define CLK_PRESCALER_MULTIPLIED_FACTOR		(UINT32_C(32))						/**<  LETIMER Prescaler Multiplication Factor */
#define LETIMER_CLOCK_DIVISION_FACTOR  		(UINT32_C(32))						/**<  LETIMER Clock Division Factor */
#define LETIMER_OVERFLOW_TIME  				(LETIMER_CLOCK_DIVISION_FACTOR*2)	/**<  LETIMER Overflow Time Reference*/
#define LETIMER_CNT_MAX 					(0xFFFF)							/**<  LETIMER Maximum Count Value Reference */
#define PERCENTAGE_VALUE_MAX 				(100)								/**<  This is the Reference for Maximum Percentage Value*/

#define TIM_TIMER_TYPE 						(TIMER3)							/**<  Timer Type */
#define TIM_TIMER_IRQ 						(TIMER3_IRQn)						/**<  Timer IRQ Type */
#define TIM_TIMER_CLOCK 					(cmuClock_TIMER3)					/**<  Timer Clock Type */

#define TIM_TIMER_ROUTE_PIN_DISABLE 		(0x0000)							/**<  Timer Route Pin Disable */

/**<  This is the Default Initial Duty Cycle represented as Percentage for Each Channel of the TIM_TIMER_TYPE Defined.
 * This should be approximately same as the ratio of the TIM_TIMER_DEFAULT_DUTYCYCLE_CC value with respect to the
 * TIM_TIMER_FREQ_TOP_SET_VALUE value as defined in the TIM_timerConfig.h file. This must not exceed
 * PERCENTAGE_VALUE_MAX */
#define TIM_TIMER_DEF_DUTYCYC_PERC_CC		{50, 50, 50}

#define TIM_TIMER_RESET						{TIM_RESET, TIM_RESET, TIM_RESET}	/**<  This Configures the Timer Parameters to Zero (0-Reset) */
#define TIM_TIMER_SET						{TIM_SET, TIM_SET, TIM_SET}			/**<  This Configures the Timer Parameters to One (1-Set) */

#define TIM_TIMER_INPUT_CLOCK_FREQ			(UINT32_C(28000000))				/**<  Provides the HFRCO Frequency */
#define TIM_TIMER_CLOCK_PRESCALAR			(UINT32_C(16))						/**<  Provides the Timer Prescaler value */

/**<  Provides the Count value For One Second of a Triangular PWM Breath Waveform Output. In this computation, the
 * purpose of Square Functionality utilization is due to scenario of skipping TIM_frequency times the
 * TIM_frequency ticks. The Division factor of 2 handles the ON and OFF time for breath pulse within the Expected
 * time Bound. This does not consider the overhead of the IRQ code lines */
#define TIM_PWM_COUNT_FOR_SECOND			(((TIM_TIMER_INPUT_CLOCK_FREQ \
												/ TIM_TIMER_CLOCK_PRESCALAR ) \
												/ (POWER_OF_TWO(TIM_frequency))) \
												/ UINT32_C(2))
#ifndef NULL
#define	NULL								(0)									/**<  This is defined in stdio.h header by Default */
#endif

/* local function prototype declarations */

/****************************************************************************
 * @brief
 *    The setupLetimer0 API Enables the Clock for the LETIMER0,
 *    configures the Channels, routes the Hardware Location,
 *    sets the required Interrupt type and the Default
 *    initial Parameters
 *
 ****************************************************************************/
static void setupLetimer0(void);

/****************************************************************************
 * @brief
 *    The setupTimer3 API Enables the Clock for the TIMER3,
 *    configures the Channels, routes the Hardware Location,
 *    sets the required Interrupt type and the Default
 *    initial Parameters
 *
 ****************************************************************************/
static void setupTimer3(void);

/****************************************************************************
 * @brief
 *    The enableLetimer0 API will enable the Interrupt Flag corresponding to
 *    LETIMER0. Also enables the interrupt vector in NVIC
 *
 ****************************************************************************/
static void enableLetimer0(void);

/****************************************************************************
 * @brief
 *    The enableTimer3 API will enable the Interrupt Flag corresponding to
 *    TIMER3. Also enables the interrupt vector in NVIC
 *
 ****************************************************************************/
static void enableTimer3(void);

/****************************************************************************
 * @brief
 *    The disableLetimer0 API will disable the Interrupt Flag corresponding
 *    to LETIMER0. Also disbles the interrupt vector in NVIC
 *
 ****************************************************************************/
static void disableLetimer0(void);

/****************************************************************************
 * @brief
 *    The disableTimer3 API will disable the Interrupt Flag corresponding
 *    to TIMER3. Also disbles the interrupt vector in NVIC apart from
 *    reseting the TIMER routing
 *
 ****************************************************************************/
static void disableTimer3(void);

/****************************************************************************
 * @brief
 *    The pwmAlterDutyCycleTimer3 API will alter the Duty Cycle of the
 *    TIMER3
 *
 * @param [in] timerPortPin
 *    Enum defining the required Channel's Duty Cycle to be altered
 *
 * @param [in] dutyCycleInPercentage
 *    Value of the Duty Cycle of the PWM signal in Percentage.
 *    This should be within the range of 0 - 100.
 *
 ****************************************************************************/
static void pwmAlterDutyCycleTimer3(TIM_port_t timerPortPin, uint8_t dutyCycleInPercentage);

/****************************************************************************
 * @brief
 *    The pwmAlterFrequencyTimer3 API will alter the Frequency of the
 *    TIMER3
 *
 * @param [in] frequency
 *    Value of the frequency component which would reflect as
 *    ( ( HFRCO frequency ) / ( TIM_TIMER_PRESCALAR_VALUE * frequency ) )
 *    where TIM_TIMER_PRESCALAR_VALUE is defined in the
 *    TIM_timerConfig.h file.
 *
 * @detail
 *    The frequency is altered by varying the Top value of the Timer.
 *    This means that all the Channels would run on same Frequency.
 *    Usage Example : If the user intended to have a Buzzer and LED's
 *    connected in different channels of the same Timer, they need to
 *    configure the Timer based on Buzzer that is used which could
 *    have an optimal operational Frequency. Its volume and tone
 *    can be controlled based on Duty cycle and Frequency .
 *    The LED's would be needed to be set at 100% duty cycle.
 *
 ****************************************************************************/
static void pwmAlterFrequencyTimer3(uint32_t frequency);

/****************************************************************************
 * @brief
 *    The pwmBreathMode API will initiate the Timer Channel that the
 *    User intends to Breath mode with Configurations as set.
 *
 * @param [in] timerPortPin
 *    Enum defining the required Timer Channel's Mode to be set for Breath
 *
 * @param [in] breathConfig
 *    Pointer to a Structure for different Configurations of the Breath Mode.
 *    This would consider the Enable / Disable of the Feature,
 *    Breath Time and Count apart from the Callback Function Handle.
 *
 ****************************************************************************/
static void pwmBreathMode(TIM_port_t timerPortPin, TIM_breathConfig_t *breathConfig);

/****************************************************************************
 * @brief
 *    The irqHandlerTimer3 API will handle the service request of
 *    TIMER3_IRQHandler function.
 *
 * @detail
 * 	  The Duty Cycle variation is controlled by Different compare
 * 	  Values inside the IRQ for each channel. Breath Mode Functionality
 * 	  Support is provided with a Callback Feature. Therefore recursive
 * 	  Call or a Prolonged Delay should be avoided.
 *
 ****************************************************************************/
static void irqHandlerTimer3(TIM_port_t timerPortPin);

/****************************************************************************
 * @brief
 *    The LETIMER0_IRQHandler API will handle the interrupt request of
 *    LETIMER0. LETIMER0 runs based on RTC as a Counter with a maximum
 *    duration of 2 seconds
 *
 ****************************************************************************/
void LETIMER0_IRQHandler(void);

/****************************************************************************
 * @brief
 *    The TIMER3_IRQHandler API will handle the interrupt request of
 *    TIMER3. TIMER3 runs PWM Functionality with variable Duty Cycle
 *    for individual channels of the TIMER3 apart from producing a
 *    breath Mode in the Channel as per the Configuration the user intends.
 *
 * @detail
 * 	  The Duty Cycle variation is controlled by Different compare
 * 	  Values inside the IRQ for each channel. Breath Mode Functionality
 * 	  Support is provided with a Callback Feature. This makes use of the
 * 	  irqHandlerTimer3 API internally for various channels after clearing
 * 	  the Interrupt overflow flags for the Timer and Channels.
 *
 ****************************************************************************/
void TIMER3_IRQHandler(void);

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_TIM_TIMER_CH_H_ */

/** ************************************************************************* */
