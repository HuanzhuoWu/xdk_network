/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include "em_cmu.h"
#include "em_timer.h"
#include "em_letimer.h"
#include "em_gpio.h"

/* additional interface header files */
#include "BCDS_Assert.h"

/* own header files */
#include "TIM_timer_ih.h"
#include "TIM_timer_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/** LETIMER initialization structure. */
static LETIMER_Init_TypeDef TIM_letimerInit;

/** TIMER3 initialization structure. */
static TIMER_Init_TypeDef TIM_timerInit;

/** TIMER3 compare/capture initialization structure. */
static TIMER_InitCC_TypeDef TIM_timerChannelInit;

/** TIMER3 PWM Frequency */
static uint32_t TIM_frequency = TIM_TIMER_FREQ_TOP_SET_VALUE;

/** TIMER3 Channel PWM Duty Cycles.
 * This cannot exceed the TIM_frequency value */
static volatile uint32_t TIM_dutycycleCC[TIM_CC_MAX] = TIM_TIMER_DEF_DUTYCYCLE_CC;

/** TIMER3 Channel PWM Duty Cycles in Percentage.
 * This cannot exceed the PERCENTAGE_VALUE_MAX value */
static uint8_t TIM_dutycycleInPercentageCC[TIM_CC_MAX] = TIM_TIMER_DEF_DUTYCYC_PERC_CC;

/** TIMER3 Channel PWM Breath Mode Configuration.
 * This is disabled (RESET by default) initially */
static uint8_t TIM_breathModeCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Breath Mode Step Value.
 * This is disabled (RESET by default) initially */
static uint32_t TIM_breathStepValueCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Compare Value Register.
 * This is disabled (RESET by default) initially */
static volatile uint32_t TIM_compareValueCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Top Reach Value Reference Register.
 * This is disabled (RESET by default) initially */
static volatile uint8_t TIM_topValueFlagCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Breath Mode Count Reference Register.
 * This is disabled (RESET by default) initially */
static volatile uint8_t TIM_breathCountCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Breath Mode End Reference Flag.
 * This is disabled (RESET by default) initially */
static volatile uint8_t TIM_breathEndFlagCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Breath Mode Count Computation Factor Value.
 * This is disabled (RESET by default) initially */
static volatile uint32_t TIM_breathTimeCountFactorCC[TIM_CC_MAX] = TIM_TIMER_RESET;

/** TIMER3 Channel PWM Breath Mode Call Back Function Declaration */
static volatile TIM_breathCallBack TIM_breathCallBackCC[TIM_CC_MAX];

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
static void setupLetimer0(void)
{
    /* Enable CMU clock for LETIMER0 */
    CMU_ClockEnable(cmuClock_LETIMER0, true);

    TIM_letimerInit.enable = false; /* Dont Start counting */
    TIM_letimerInit.debugRun = false; /* Counter shall not keep running during debug halt */
    TIM_letimerInit.rtcComp0Enable = false; /* Don't start counting on RTC COMP0 match */
    TIM_letimerInit.rtcComp1Enable = false; /* Don't start counting on RTC COMP1 match */
    TIM_letimerInit.comp0Top = true; /* Load COMP0 register into CNT when counter underflows. COMP0 is used as TOP */
    TIM_letimerInit.bufTop = false; /* Don't load COMP1 into COMP0 when REP0 reaches 0 */
    TIM_letimerInit.out0Pol = 0; /* Idle value for output 0 */
    TIM_letimerInit.out1Pol = 0; /* Idle value for output 1 */
    TIM_letimerInit.ufoa0 = letimerUFOANone; /* PWM output on output 0 */
    TIM_letimerInit.ufoa1 = letimerUFOANone; /* Pulse output on output 1 */
    TIM_letimerInit.repMode = letimerRepeatFree; /* Count until stopped */

    /* divide the clock to increase overflow , it will make system wake less often */
    CMU_ClockDivSet(cmuClock_LETIMER0, CLK_PRESCALER_MULTIPLIED_FACTOR);

    /* Disable all LETIMER0 interrupts */
    LETIMER0->IEN = 0;

    /* Initialize the timer without starting */
    LETIMER_Init(LETIMER0, &TIM_letimerInit);
}

static void setupTimer3(void)
{
    /* Enable  CMU clock for TIMER3 */
    CMU_ClockEnable(TIM_TIMER_CLOCK, true);

    /* Select CC channel parameters */
    TIM_timerChannelInit.eventCtrl = timerEventEveryEdge; /* PRS output pulse, interrupt flag and DMA request set on every capture */
    TIM_timerChannelInit.edge = timerEdgeBoth; /* Input capture edge selected to detect */
    TIM_timerChannelInit.prsSel = timerPRSSELCh0; /* Peripheral reflex system trigger selection set on PRS channel 0 */
    TIM_timerChannelInit.cufoa = timerOutputActionNone; /* Counter underflow output action disabled */
    TIM_timerChannelInit.cofoa = timerOutputActionNone; /* Counter overflow output action disabled */
    TIM_timerChannelInit.cmoa = timerOutputActionToggle; /* Counter match output action set to Toggle on event */
    TIM_timerChannelInit.mode = timerCCModePWM; /* Compare/capture channel mode set for Pulse-Width modulation */
    TIM_timerChannelInit.filter = false; /* digital filter Disabled */
    TIM_timerChannelInit.prsInput = false; /* Select TIMERnCCx */
    TIM_timerChannelInit.coist = false; /* Compare output initial state disabled */
    TIM_timerChannelInit.outInvert = true; /* Invert output from compare/capture channel enabled */

    /* Configure CC channel 0 */
    TIMER_InitCC(TIM_TIMER_TYPE, TIM_CC0, &TIM_timerChannelInit);

    /* Configure CC channel 1 */
    TIMER_InitCC(TIM_TIMER_TYPE, TIM_CC1, &TIM_timerChannelInit);

    /* Configure CC channel 2 */
    TIMER_InitCC(TIM_TIMER_TYPE, TIM_CC2, &TIM_timerChannelInit);

    /* Route CC0 to location 0 (PE14),
     * Route CC1 to location 0 (PE15),
     * Route CC2 to location 0 (PA15)
     * and enable pin */
    TIM_TIMER_TYPE->ROUTE |= (TIMER_ROUTE_CC0PEN | TIMER_ROUTE_CC1PEN | TIMER_ROUTE_CC2PEN | TIMER_ROUTE_LOCATION_LOC0);

    /* Set Top Value */
    TIMER_TopSet(TIM_TIMER_TYPE, TIM_TIMER_FREQ_TOP_SET_VALUE);

    /* Set compare value starting at 0 for CC0 - it will be utilised in the interrupt handler */
    TIMER_CompareBufSet(TIM_TIMER_TYPE, TIM_CC0, 0);

    /* Set compare value starting at 0 for CC1 - it will be utilised in the interrupt handler */
    TIMER_CompareBufSet(TIM_TIMER_TYPE, TIM_CC1, 0);

    /* Set compare value starting at 0 for CC2 - it will be utilised in the interrupt handler */
    TIMER_CompareBufSet(TIM_TIMER_TYPE, TIM_CC2, 0);

    /* Select timer parameters */
    TIM_timerInit.enable = false; /* Do not Start counting when init completed */
    TIM_timerInit.debugRun = true; /* Counter shall keep running during debug halt */
    TIM_timerInit.prescale = TIM_TIMER_PRESCALAR_VALUE; /* Prescaling factor, for HFPER clock set as 2*/
    TIM_timerInit.clkSel = timerClkSelHFPerClk; /* Clock selection - Prescaled HFPER*/
    TIM_timerInit.fallAction = timerInputActionNone; /* Action on falling input edge disabled */
    TIM_timerInit.riseAction = timerInputActionNone; /* Action on rising input edge disabled */
    TIM_timerInit.mode = timerModeUp; /* Counting mode - Up-counting*/
    TIM_timerInit.dmaClrAct = false; /* DMA request clear on active disabled */
    TIM_timerInit.quadModeX4 = false; /* Select X2 quadrature decode mode */
    TIM_timerInit.oneShot = false; /* Determines as only counting up once */
    TIM_timerInit.sync = false; /* Timer start/stop/reload by other timers disabled */
}

static void enableLetimer0(void)
{
    /* Enable all LETIMER0 interrupts */
    LETIMER0->IEN = 1;

    /* Enable the LETIMER interrupt in NVIC */
    NVIC_EnableIRQ(LETIMER0_IRQn);
}

static void enableTimer3(void)
{
    /* Start counting when init completed */
    TIM_timerInit.enable = true;

    /* Enable overflow interrupt */
    TIMER_IntEnable(TIM_TIMER_TYPE, TIMER_IF_OF);

    /* Enable TIMER3 interrupt vector in NVIC */
    NVIC_EnableIRQ(TIM_TIMER_IRQ);

    /* Configure timer */
    TIMER_Init(TIM_TIMER_TYPE, &TIM_timerInit);
}

static void disableLetimer0(void)
{
    /* Disable all LETIMER0 interrupts */
    LETIMER0->IEN = 0;

    /* Disable the LETIMER interrupt in NVIC */
    NVIC_DisableIRQ(LETIMER0_IRQn);
}

static void disableTimer3(void)
{
    /* Do not Start counting when init completed */
    TIM_timerInit.enable = false;

    /* Diable TIMER3 routing and pin */
    TIM_TIMER_TYPE->ROUTE = TIM_TIMER_ROUTE_PIN_DISABLE;

    /* Disable overflow interrupt */
    TIMER_IntDisable(TIM_TIMER_TYPE, TIMER_IF_OF);

    /* Disable TIMER1 interrupt vector in NVIC */
    NVIC_DisableIRQ(TIM_TIMER_IRQ);

    /* Configure timer */
    TIMER_Init(TIM_TIMER_TYPE, &TIM_timerInit);
}

static void pwmAlterDutyCycleTimer3(TIM_port_t timerPortPin, uint8_t dutyCycleInPercentage)
{

    assert(timerPortPin < TIM_CC_MAX);

    assert(dutyCycleInPercentage <= PERCENTAGE_VALUE_MAX);

    if (TIM_dutycycleInPercentageCC[timerPortPin] != dutyCycleInPercentage)
    {
        /* Saving the value onto a Global variable for Reference */
        TIM_dutycycleInPercentageCC[timerPortPin] = dutyCycleInPercentage;

        /* PWM Duty cycle for the corresponding Channel calculated and Altered
         * with respect to the current Maximum PWM Frequency value (TIM_frequency) */
        TIM_dutycycleCC[timerPortPin] = ((TIM_frequency * dutyCycleInPercentage) / UINT8_C(100));

        /** TIMER3 Channel PWM Breath Mode End Reference Flag
         * is Reset to Disable Breath Mode in the IRQ */
        TIM_breathEndFlagCC[timerPortPin] = TIM_RESET;
    }
}

static void pwmAlterFrequencyTimer3(uint32_t frequency)
{
    if (TIM_frequency != frequency)
    {
        /* Altering the Duty Cycle of Channel 0 Based on the
         * Current Frequency Update and older Duly cycle Value */
        TIM_dutycycleCC[TIM_CC0] = ((frequency * TIM_dutycycleInPercentageCC[TIM_CC0]) / UINT8_C(100));

        /* Altering the Duty Cycle of Channel 1 Based on the
         * Current Frequency Update and older Duly cycle Value */
        TIM_dutycycleCC[TIM_CC1] = ((frequency * TIM_dutycycleInPercentageCC[TIM_CC1]) / UINT8_C(100));

        /* Altering the Duty Cycle of Channel 2 Based on the
         * Current Frequency Update and older Duly cycle Value */
        TIM_dutycycleCC[TIM_CC2] = ((frequency * TIM_dutycycleInPercentageCC[TIM_CC2]) / UINT8_C(100));

        /* Set Top Value according to the newly updated value */
        TIMER_TopSet(TIM_TIMER_TYPE, frequency);

        /* Saving the value onto a Global variable for Reference */
        TIM_frequency = frequency;
    }
}

static void pwmBreathMode(TIM_port_t timerPortPin, TIM_breathConfig_t *breathConfig)
{
    assert(breathConfig != NULL);

    assert(timerPortPin < TIM_CC_MAX);

    assert(breathConfig->timerMode < TIM_MODE_MAX);

    assert(breathConfig->breathTimeInSeconds != 0);

    if ((TIM_PWM_COUNT_FOR_SECOND * breathConfig->breathTimeInSeconds) >= UINT32_MAX)
    {
        assert(0);
    }

    assert(breathConfig->breathCount != 0);

    assert(breathConfig->callbackFunction != NULL);

    /** Saving the PWM Breath Mode Count Reference Register
     * for the Corresponding Channel as per the User Configuration */
    TIM_breathCountCC[timerPortPin] = breathConfig->breathCount;

    /** Resetting the Compare Buffer Value to 0 to reset the PWM
     * start value irrespective of the Previous Configurations */
    TIMER_CompareBufSet(TIM_TIMER_TYPE, timerPortPin, 0);

    /** TIMER3 Channel PWM Breath Mode End Reference Flag
     * is Set to Enable Breath Mode in the IRQ */
    TIM_breathEndFlagCC[timerPortPin] = TIM_SET;

    /** TIMER3 Channel PWM Breath Mode Count Computation
     * Factor Value is computed based on the Count for a
     * second multiplied by the requested Time in Seconds */
    TIM_breathTimeCountFactorCC[timerPortPin] = ( TIM_PWM_COUNT_FOR_SECOND * breathConfig->breathTimeInSeconds);

    /** TIMER3 Channel PWM Breath Mode is Enabled or
     * Disabled based on the User Configuration */
    TIM_breathModeCC[timerPortPin] = breathConfig->timerMode;

    /** TIMER3 Channel PWM Breath Mode Call Back
     * Function is assigned with the User Defined
     * Function Address */
    TIM_breathCallBackCC[timerPortPin] = (TIM_breathCallBack) breathConfig->callbackFunction;
}

static void irqHandlerTimer3(TIM_port_t timerPortPin)
{
    /* Validates If Breath Mode is Enabled */
    if (TIM_breathModeCC[timerPortPin] == TIM_SET)
    {
        /* Reads the Capture Value of the Timer Channel and stores in Compare Register */
        TIM_compareValueCC[timerPortPin] = TIMER_CaptureGet(TIM_TIMER_TYPE, timerPortPin);

        /* Enters this Loop by Default (Based on initial Configuration) or for
         * incrementing Output Breath Wave or if the Output is reached the
         * Top / Maximum Value(TIM_frequency) */
        if (TIM_topValueFlagCC[timerPortPin] == 0)
        {
            /* Enters this Loop only if Top / Maximum Top Set Value is reached */
            if (TIM_compareValueCC[timerPortPin] >= TIM_frequency)
            {
                /* Enabling the corresponding Channel's Top Reach Value for Further Reference */
                TIM_topValueFlagCC[timerPortPin] = 1;

                /* Reconfigures the PWM Breath Mode Step Value to default
                 * which is made use in the Decrementing Output */
                TIM_breathStepValueCC[timerPortPin] = TIM_breathTimeCountFactorCC[timerPortPin];
            }
        }

        /* Enters this Loop by for Decrementing Output Breath wave or if the
         * Breath Output Line has reached the Bottom / Minimum Value(0) */
        if (TIM_topValueFlagCC[timerPortPin] == 1)
        {
            /* Enters this Loop only if Bottom / Minimum Value is reached */
            if (TIM_compareValueCC[timerPortPin] <= 0)
            {
                /* Disables the corresponding Channel's Top Reach Value for Further Reference */
                TIM_topValueFlagCC[timerPortPin] = 0;

                /* Reconfigures the PWM Breath Mode Step Value to default
                 * which is made use in the Incrementing Output as long as
                 * the Breath Count has reached the Expected Maximum Value */
                TIM_breathStepValueCC[timerPortPin] = TIM_breathTimeCountFactorCC[timerPortPin];

                /* Decrementing the Breath Count Value */
                TIM_breathCountCC[timerPortPin]--;

                /* Enters this Loop only if the Breath Count has Reached 0.
                 * This is a Down Counter Logic Implementation */
                if (TIM_breathCountCC[timerPortPin] == 0)
                {
                    /** TIMER3 Channel PWM Breath Mode is Disabled */
                    TIM_breathModeCC[timerPortPin] = TIM_RESET;

                    /** TIMER3 Channel PWM Breath Mode End Reference Flag
                     * is Set to avoid the False condition of Breath Mode
                     * in the same function for keeping the Channel output
                     * line unaffected until a Reconfiguration is Triggered */
                    TIM_breathEndFlagCC[timerPortPin] = TIM_SET;

                    /* Call to the Corresponding Channel's Callback Function */
                    TIM_breathCallBackCC[timerPortPin]();
                }
            }
        }

        /* Enters this Loop for Decrementing Output Breath wave */
        if (TIM_topValueFlagCC[timerPortPin] == 1)
        {
            /* Enters this Loop only if the Breath Count has reached to 0 */
            if (TIM_breathStepValueCC[timerPortPin] == 0)
            {
                /* Decrementing the Compare Buffer Value by one lesser than the Previous Value */
                TIMER_CompareBufSet(TIM_TIMER_TYPE, timerPortPin, --TIM_compareValueCC[timerPortPin]);

                /* Reconfigures the PWM Breath Mode Step Value to default Value */
                TIM_breathStepValueCC[timerPortPin] = TIM_breathTimeCountFactorCC[timerPortPin];
            }
            else
            {
                /* Decrementing the PWM Breath Mode Step Value by 1 */
                TIM_breathStepValueCC[timerPortPin]--;
            }
        }

        /* Enters this Loop for Incrementing Output Breath wave */
        else
        {
            /* Enters this Loop only if the Breath Count has reached to 0 */
            if (TIM_breathStepValueCC[timerPortPin] == 0)
            {
                /* Incrementing the Compare Buffer Value by one higher than the Previous Value */
                TIMER_CompareBufSet(TIM_TIMER_TYPE, timerPortPin, ++TIM_compareValueCC[timerPortPin]);

                /* Reconfigures the PWM Breath Mode Step Value to default Value */
                TIM_breathStepValueCC[timerPortPin] = TIM_breathTimeCountFactorCC[timerPortPin];
            }
            else
            {
                /* Decrementing the PWM Breath Mode Step Value by 1 */
                TIM_breathStepValueCC[timerPortPin]--;
            }
        }
    }

    /* Enters this Loop If Breath Mode is Disabled */
    else
    {
        /* Enters this Loop If Breath Mode is Completed or Free Running PWM mode is Set */
        if (TIM_breathEndFlagCC[timerPortPin] == TIM_RESET)
        {
            /* Set duty cycle for Channel according to current status value */
            TIMER_CompareBufSet(TIM_TIMER_TYPE, timerPortPin, TIM_dutycycleCC[timerPortPin]);
        }
    }
}

void LETIMER0_IRQHandler(void)
{
    if (LETIMER_IntGet(LETIMER0) & LETIMER_IF_UF)
    {
        /* Clear Cause of interrupt */
        LETIMER_IntClear(LETIMER0, LETIMER_IFC_UF);
    }
    else if (LETIMER_IntGet(LETIMER0) & LETIMER_IF_COMP0)
    {
        /* clear cause of interrupt */
        LETIMER_IntClear(LETIMER0, LETIMER_IFC_COMP0);
    }
}

void TIMER3_IRQHandler(void)
{

    /* Clear flag for TIMER3 overflow interrupt */
    TIMER_IntClear(TIM_TIMER_TYPE, TIMER_IF_OF | TIMER_IF_CC0 | TIMER_IF_CC1 | TIMER_IF_CC2);

    /* Calling the Custom Timer3 Interrupt service request for Channel 0 */
    irqHandlerTimer3(TIM_CC0);

    /* Calling the Custom Timer3 Interrupt service request for Channel 1 */
    irqHandlerTimer3(TIM_CC1);

    /* Calling the Custom Timer3 Interrupt service request for Channel 2 */
    irqHandlerTimer3(TIM_CC2);
}

/* global functions ********************************************************* */
extern void TIM_init(TIM_name_t timerName)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        setupTimer3();
        break;

    case TIM_LETIMER0:
        setupLetimer0();
        break;

    default:
        assert(0);
        ;
        break;
    }
}

extern void TIM_enable(TIM_name_t timerName)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        enableTimer3();
        break;

    case TIM_LETIMER0:
        enableLetimer0();
        break;

    default:
        assert(0);
        ;
        break;
    }
}

extern void TIM_disable(TIM_name_t timerName)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        disableTimer3();
        break;

    case TIM_LETIMER0:
        disableLetimer0();
        break;

    default:
        assert(0);
        break;
    }
}

extern void TIM_pwmAlterDutyCycle(TIM_name_t timerName, TIM_port_t timerPortPin, uint8_t dutyCycleInPercentage)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        pwmAlterDutyCycleTimer3(timerPortPin, dutyCycleInPercentage);
        break;

    default:
        assert(0);
        break;
    }
}

extern void TIM_pwmAlterFrequency(TIM_name_t timerName, uint32_t frequency)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        pwmAlterFrequencyTimer3(frequency);
        break;

    default:
        assert(0)
        ;
        break;
    }
}

extern void TIM_pwmBreathMode(TIM_name_t timerName, TIM_port_t timerPortPin, TIM_breathConfig_t *breathConfig)
{
    switch (timerName)
    {
    case TIM_TIMER3:
        pwmBreathMode(timerPortPin, breathConfig);
        break;

    default:
        assert(0)
        ;
        break;
    }
}

/** ************************************************************************* */
