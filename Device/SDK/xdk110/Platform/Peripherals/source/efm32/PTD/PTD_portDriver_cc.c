/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* standard header files */

/* additional interface header files */
#include "em_cmu.h"

#include <stddef.h>
#include "FreeRTOS.h"
#include "portable.h"
#include "BCDS_Assert.h"

/* Own Header Files   */
#include "PTD_portDriver_ih.h"
#include "PTD_portDriver_ch.h"
#include "PTD_portDriver_ph.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */
static PTD_pinConfig_t *PTD_evenPinConfig_mps;
static PTD_pinConfig_t *PTD_oddPinConfig_mps;
static uint8_t maxEvenIntr_mdu = UINT8_C(0);
static uint8_t maxOddIntr_mdu = UINT8_C(0);

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief
 *   This function is to check whether the even pin is already configured or not.
 *   If the already configured pin is again called with a different callback, the old callback will be overwritten.
 *
 * @param [in]   : pin          The pin number in the port to be configured
 * @param [in]   : callBack     callback function have to be mapped to the pin which will be get called
 *                              when interrupt occurs
 * @param [in]   : even_index   The index of the array holding the pins and callback
 * @param [in]   : ptr_index    The pointer to store the value where the same pins is associated with a different callback
 * @param [out]  : PTD_retVal   The status of the pins and callback
 *
 * @retval           : PTD_CONFIGURED            Both the pins and callback are already configured.
 *                     PTD_DIFFERENT_CALLBACK    The pin is already configured, but again called with a different callback.
 *                     PTD_NOT_CONFIGURED        The pins are not configured.
 *                     PTD_INVALID_PARAMETER     If a ptr_index pointer is NULL
 */
static inline int32_t PTD_checkEvenPinConfigured(uint32_t pin, PTD_intrCallback callBack, uint8_t even_index, uint8_t *ptr_index)
{
    uint8_t even_ptr = UINT8_C(0);
    int32_t PTD_retVal = PTD_NOT_CONFIGURED;
    if(ptr_index != NULL)
    {
    for (even_ptr = UINT8_C(0); even_ptr < even_index; even_ptr++)
    {
        if ((PTD_evenPinConfig_mps[even_ptr].pin == pin) && (PTD_evenPinConfig_mps[even_ptr].pinCallBackFunction == callBack))
        {
            PTD_retVal = PTD_CONFIGURED;
            break;
        }
        else if ((PTD_evenPinConfig_mps[even_ptr].pin == pin) && (PTD_evenPinConfig_mps[even_ptr].pinCallBackFunction != callBack))
        {
            *ptr_index = even_ptr;
            PTD_retVal = PTD_DIFFERENT_CALLBACK;
            break;
        }
        else
        {
            PTD_retVal = PTD_NOT_CONFIGURED;
        }
    }
    }
    else
    {
        PTD_retVal = PTD_INVALID_PARAMETER;
    }
    return (PTD_retVal);
}

/**
 * @brief
 *   This function is to check whether the odd pin is already configured or not.
 *   If the already configured pin is again called with a different callback, the old callback will be overwritten.
 *
 * @param [in]   : pin          The pin number in the port to be configured
 * @param [in]   : callBack     callback function have to be mapped to the pin which will be get called
 *                              when interrupt occurs
 * @param [in]   : odd_index    The index of the array holding the pins and callback
 * @param [in]   : ptr_index    The pointer to store the value where the same pins is associated with a different callback
 * @param [out]  : PTD_retVal   The status of the pins and callbac
 *
 * @retval           : PTD_CONFIGURED            Both the pins and callback are already configured.
 *                     PTD_DIFFERENT_CALLBACK    The pin is already configured, but again called with a different callback.
 *                     PTD_NOT_CONFIGURED        The pins are not configured.
 *                     PTD_INVALID_PARAMETER     If a ptr_index pointer is NULL
 */

static inline int32_t PTD_checkOddPinConfigured(uint32_t pin, PTD_intrCallback callBack, uint8_t odd_index, uint8_t *ptr_index)
{
    uint8_t odd_ptr = UINT8_C(0);
    int32_t PTD_retVal = PTD_NOT_CONFIGURED;
    if(ptr_index != NULL)
    {
    for (odd_ptr = UINT8_C(0); odd_ptr < odd_index; odd_ptr++)
    {
        if ((PTD_oddPinConfig_mps[odd_ptr].pin == pin) && (PTD_oddPinConfig_mps[odd_ptr].pinCallBackFunction == callBack))
        {
            PTD_retVal = PTD_CONFIGURED;
            break;
        }
        else if ((PTD_evenPinConfig_mps[odd_ptr].pin == pin) && (PTD_evenPinConfig_mps[odd_ptr].pinCallBackFunction != callBack))
        {
            *ptr_index = odd_ptr;
            PTD_retVal = PTD_DIFFERENT_CALLBACK;
            break;
        }
        else
        {
        PTD_retVal = PTD_NOT_CONFIGURED;
        }
    }
    }
    else
    {
        PTD_retVal = PTD_INVALID_PARAMETER;
    }
    return (PTD_retVal);
}
/* global functions ********************************************************* */
/* API documentation is in the interface header PTD_portDriver_ih.h*/
void PTD_portInit(void)
{
    /* Enable the clock to make the module registers accessible */
    CMU_ClockEnable(cmuClock_GPIO, true);
}

/**
 * @brief       : Configuring the interrupt by enabling and disabling IRQs.
 *
 * @param [in/out]   : void
 * @retval           : void
 */
void PTD_portInteruptConfig(void)
{
    /* Before going to INTERRUPT INITIALIZATION Clear Pending IRQ */
#if (PTD_GPIO_ODD_INTERRUPT == PTD_CONFIG_ENABLED)
    NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
    NVIC_EnableIRQ(GPIO_ODD_IRQn);
#endif
#if (PTD_GPIO_EVEN_INTERRUPT == PTD_CONFIG_ENABLED)
    NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);
#endif
}

/* API documentation is in the interface header PTD_portDriver_ih.h*/
void PTD_setInterruptPinCount(uint8_t even_pins, uint8_t odd_pins)
{
    static bool evenPinMemoryDone = false;
    static bool oddPinMemoryDone = false;

    if ((even_pins == 0) && (odd_pins == 0))
    {
        assert(0);
    }
    else
    {
        if (evenPinMemoryDone != true)
        {
            if (even_pins != 0)
            {
                /* Allocate memory to the structure that having pin and call back mapping */
                PTD_evenPinConfig_mps = (PTD_pinConfig_t *) pvPortMalloc(
                    sizeof(PTD_pinConfig_t) * even_pins);
                /* Set the number of max interrupts that can be registered for even and odd pins*/
                maxEvenIntr_mdu = even_pins;
                evenPinMemoryDone = true; /* Allocate memory once */
            }
        }
        else
        {
            assert(0);
        }

        if (oddPinMemoryDone != true)
        {
            if (odd_pins != 0)
            {
                /* Allocate memory to the structure that having pin and call back mapping */
                PTD_oddPinConfig_mps = (PTD_pinConfig_t *) pvPortMalloc(
                    sizeof(PTD_pinConfig_t) * odd_pins);
                /* Set the number of max interrupts that can be registered for even and odd pins*/
                maxOddIntr_mdu = odd_pins;
                oddPinMemoryDone = true; /* Allocate memory once */
            }
        }
        else
        {
            assert(0);
        }
    }
}

/* API documentation is in the interface header PTD_portDriver_ih.h*/
void PTD_intConfig(GPIO_Port_TypeDef port, uint32_t pin, bool risingEdge,
    bool fallingEdge, bool enable, PTD_intrCallback callBack)
{
    static uint8_t odd_index = UINT8_C(0);
    static uint8_t even_index = UINT8_C(0);
    uint8_t ptr_index = UINT8_C(0);
    int32_t retVal = PTD_NOT_CONFIGURED;

    if((UINT8_C(1) == enable) && (NULL == callBack))
    {
        assert(0);
    }
    else
    {
        /* Check for Even or Odd pin */
        if ((pin % 2) == UINT32_C(0))
        {
            /* Even interrupt Pin*/
            if (even_index < maxEvenIntr_mdu)
            {
                if (PTD_evenPinConfig_mps != NULL )
                {
                    retVal = PTD_checkEvenPinConfigured(pin, callBack, even_index, &ptr_index);
                    if (retVal == PTD_DIFFERENT_CALLBACK)
                    {
                        PTD_evenPinConfig_mps[ptr_index].pinCallBackFunction =
                            callBack;
                    }
                    else if (retVal == PTD_NOT_CONFIGURED)
                    {
                        PTD_evenPinConfig_mps[even_index].pin = pin;
                        PTD_evenPinConfig_mps[even_index].pinCallBackFunction =
                            callBack;
                        even_index++;
                    }
                    else
                    {
                        ; /* Already pins and callback configured */
                    }
                }
                else
                {
                    assert(0);
                }
            }
            else
            {
                assert(0);
            }
        }
        else
        {
            /* Odd interrupt Pin*/
            if (odd_index < maxOddIntr_mdu)
            {
                if (PTD_oddPinConfig_mps != NULL )
                {
                    retVal = PTD_checkOddPinConfigured(pin, callBack, odd_index, &ptr_index);
                    if (retVal == PTD_DIFFERENT_CALLBACK)
                    {
                        PTD_oddPinConfig_mps[ptr_index].pinCallBackFunction =
                            callBack;
                    }
                    else if (retVal == PTD_NOT_CONFIGURED)
                    {
                        PTD_oddPinConfig_mps[odd_index].pin = pin;
                        PTD_oddPinConfig_mps[odd_index].pinCallBackFunction =
                            callBack;
                        odd_index++;
                    }
                    else
                    {
                        ; /* Already pins and callback configured */
                    }
                }
                else
                {
                    assert(0);
                }
            }
            else
            {
                assert(0);
            }
        }
    }

    GPIO_IntConfig((GPIO_Port_TypeDef) port, pin, risingEdge, fallingEdge,
        enable);
}

/* API documentation is in the interface header PTD_portDriver_ih.h*/
int32_t PTD_interruptSwitchCallback(uint32_t pin, PTD_intrCallback callBack)
{
    int32_t statusCode = PTD_FAILURE;
    uint32_t index;
    uint32_t pinConfigExist = PTD_CLEAR;

    if (callBack == NULL )
    {
        return (PTD_INVALID_PARAMETER);
    }

    /* Check for Even or Odd pin */
    if ((pin % PTD_EVENNUMBER_IDENTIFIER) == UINT32_C(0))
    {
        for (index = UINT32_C(0); index < maxEvenIntr_mdu; index++)
        {
            if (PTD_evenPinConfig_mps[index].pin == pin)
            {
                pinConfigExist = PTD_SET;
                break;
            }
        }
        if ((pinConfigExist == PTD_SET) && (PTD_evenPinConfig_mps != NULL ))
        {
            /* clear the pending interrupt and disable it at GPIO as we are updating handle */
            PTD_intClear(pin);
            PTD_intDisable(pin);

            PTD_evenPinConfig_mps[index].pin = pin;
            PTD_evenPinConfig_mps[index].pinCallBackFunction = callBack;
            pinConfigExist = PTD_CLEAR;

            /* enabling interrupt back at GPIO */
            PTD_intEnable(pin);
            statusCode = PTD_SUCCESS;
        }
        else
        {
            statusCode = PTD_FAILURE;
        }
    }
    else
    {
        for (index = UINT32_C(0); index < maxOddIntr_mdu; index++)
        {
            if (PTD_oddPinConfig_mps[index].pin == pin)
            {
                pinConfigExist = PTD_SET;
                break;
            }
        }
        if ((pinConfigExist == PTD_SET) && (PTD_oddPinConfig_mps != NULL ))
        {
            /* clear the pending interrupt and disable it at GPIO as we are updating handle */
            PTD_intClear(pin);
            PTD_intDisable(pin);

            PTD_oddPinConfig_mps[index].pin = pin;
            PTD_oddPinConfig_mps[index].pinCallBackFunction = callBack;
            pinConfigExist = PTD_CLEAR;

            /* enabling interrupt back at GPIO */
            PTD_intEnable(pin);
            statusCode = PTD_SUCCESS;
        }
        else
        {
            statusCode = PTD_FAILURE;
        }
    }

    return (statusCode);
}
/***************************************************************************//**
 * @brief
 *   This is the Interrupt Service routine for ODD GPIO pins.
 *
 * @details
 *   This will check pins registered for interrupt. If the interrupt flag is
 *   set for the pin, then the call back will be executed.
 *
 * @note
 *    Only one handler for all odd GPIO pins.
 *
 * @param[in] void
 *
 * @return void
 ******************************************************************************/
void GPIO_ODD_IRQHandler(void)
{
    uint8_t index;
    for (index = UINT8_C(0); index < maxOddIntr_mdu; index++)
    {
        if (GPIO->IF & 1 << PTD_oddPinConfig_mps[index].pin)
        {
            PTD_oddPinConfig_mps[index].pinCallBackFunction();
            GPIO_IntClear(1 << PTD_oddPinConfig_mps[index].pin);
            break;
        } /* Else - do nothing */
    }
}

/***************************************************************************//**
 * @brief
 *   This is the Interrupt Service routine for EVEN GPIO pins.
 *
 * @details
 *   This will check pins registered for interrupt. If the interrupt flag is
 *   set for the pin, then the call back will be executed.
 *
 * @note
 *    Only one handler for all even GPIO pins.
 *
 * @param[in] void
 *
 * @return void
 ******************************************************************************/
void GPIO_EVEN_IRQHandler(void)
{
    uint8_t index;
    for (index = UINT8_C(0); index < maxEvenIntr_mdu; index++)
    {
        if (GPIO->IF & 1 << PTD_evenPinConfig_mps[index].pin)
        {
            PTD_evenPinConfig_mps[index].pinCallBackFunction();
            GPIO_IntClear(1 << PTD_evenPinConfig_mps[index].pin);
            break;
        } /* Else - do nothing */
    }
}

/******************************************************************************/
