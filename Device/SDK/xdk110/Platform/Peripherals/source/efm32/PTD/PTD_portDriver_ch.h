/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BCDS_PORTDRIVER_CH_H_
#define BCDS_PORTDRIVER_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define PTD_SET                 	UINT8_C(1)   /**< macro used to set the variable to one */
#define PTD_CLEAR               	UINT8_C(0)   /**< macro used to set the variable to zero */
#define PTD_SUCCESS             	INT32_C(0)   /**< macro used to indicate success status from the routine */
#define PTD_FAILURE             	INT32_C(-1)  /**< macro used to indicate failure status from the routine */
#define PTD_INVALID_PARAMETER   	INT32_C(-2)  /**< macro used to indicate invalid parameter to the routine */
#define PTD_EVENNUMBER_IDENTIFIER  	UINT32_C(2)  /**< macro used in identifying even numbers */
#define PTD_DIFFERENT_CALLBACK      INT32_C(3)    /**< macro used  for indicating pins already configured but with a different callback*/
#define PTD_NOT_CONFIGURED          INT32_C(-3)   /**< macro used for indicating pins not configured*/
#define PTD_CONFIGURED              INT32_C(4)    /**< macro used for indicating pins configured with same callback*/
/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_PORTDRIVER_CH_H_ */
