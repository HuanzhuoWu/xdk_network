/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

/* additional interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "PTD_portDriver_ih.h"
#include "gpio.h"
#include "gpio_efm32gg.h"
#include "portmacro.h"
#include "projdefs.h"
#include "BCDS_Assert.h"

/* own header files */
#include "button.h"
#include "button_efm32.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/** Indicates if first time initialization should be done */
static _Bool BUTTON_isInitializationRequired = true;

/** Static allocation of all instances */
static BUTTON_handleInfo_t BUTTON_handles[BUTTON_MAXIMUM_INSTANCES];

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
/**
 * @brief Button deferred callback function, Application's callback will be called in deffered context.
 *
 * @param [in]   interruptDataPointer, pointer to button handle
 *
 * @param [in]   interruptData, dummy data and not used.
 *
 */
static void _BUTTON_deferredInterruptHandler(void *interruptDataPointer, uint32_t interruptData)
{
    BUTTON_handleInfo_t *handle = (BUTTON_handleInfo_t *) interruptDataPointer;
    (void) interruptData;

    /* call application's callback */
    if (handle->callback != NULL )
    {
        handle->callback(handle, handle->callbackUserParameter);
    }
}

/**
 * @brief Button deferred callback function, Application's callback will be called in deffered context.
 *
 * @param [in]   interruptDataPointer, pointer to button handle
 *
 * @param [in]   interruptData, dummy data and not used.
 *
 */
void _BUTTON_InterruptHandler(void)
{
    /*The ISR scan all handles, For each handle that is enabled, it compares the pin last state with the current state. 
     And then deffers the call from which the application calback gets invoked
     @Note: Debounce feature is not provided */
    for (uint8_t i = 0; i < BUTTON_MAXIMUM_INSTANCES; i++)
    {
        BUTTON_handleInfo_t *handle = &BUTTON_handles[i];

        /* take care only for enabled handles */
        if (handle->isEnabled)
        {
            /* Get pin state. */
            GPIO_pinStates_t state = GPIO_getPin(handle->pin);

            /* check if pin state has changed. */
            if (state != handle->lastPinState)
            {
                /* Store current pin state. */
                handle->lastPinState = state;

                /* Store current pin state. */
                (state == handle->pressedState) ? (handle->wasPressed = true) : (handle->wasReleased = true);

                /* pending to deferred procedure call to lower the context and to call application's deferred callback */
                portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
                if (xTimerPendFunctionCallFromISR(_BUTTON_deferredInterruptHandler, handle, UINT8_C(0), &xHigherPriorityTaskWoken) == pdPASS )
                {
                    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
                }
                else
                {
                    assert(0);
                }
            }
        }
    }
}

/* This function initialize all required handles and global variables */
/**
 * @brief Button deferred callback function, Application's callback will be called in deffered context.
 *
 * @param [in]   interruptDataPointer, pointer to button handle
 *
 * @param [in]   interruptData, dummy data and not used.
 *
 */
BUTTON_errorTypes_t _BUTTON_firstInitialization(void)
{
    /* One time initializations. */
    if (BUTTON_isInitializationRequired)
    {
        BUTTON_isInitializationRequired = false;

        /*Initialize handles */
        for (uint8_t i = 0; i < BUTTON_MAXIMUM_INSTANCES; i++)
        {
            BUTTON_handles[i].isAllocated = false;
            BUTTON_handles[i].isEnabled = false;
            BUTTON_handles[i].magicWord = BUTTON_HANDLE_MAGIC_WORD;
        }
    }
    return BUTTON_ERROR_OK;
}

/* This function is used to check the handle for correct values, before using it */
/**
 * @brief Button deferred callback function, Application's callback will be called in deffered context.
 *
 * @param [in]   interruptDataPointer, pointer to button handle
 *
 * @param [in]   interruptData, dummy data and not used.
 *
 */
BUTTON_errorTypes_t _BUTTON_checkParameters(BUTTON_handleInfo_tp handleInfo)
{
    /* Make sure handle pointer is not NULL */
    if (handleInfo == NULL )
        return BUTTON_ERROR_HANDLE_IS_NULL;

    /* Check handle magic word. */
    if (handleInfo->magicWord != BUTTON_HANDLE_MAGIC_WORD)
        return BUTTON_ERROR_HANDLE_DATA_CORRUPTED;

    /* Check if handle is allocated. */
    if (!handleInfo->isAllocated)
        return BUTTON_ERROR_HANDLE_DEALLOCATED;

    return BUTTON_ERROR_OK;
}

/* global functions ********************************************************* */

/* API documentation is in the interface header */
BUTTON_handle_tp BUTTON_create(const GPIO_handle_tp pin, const GPIO_pinStates_t pressedState)
{
    /* Make sure first initialization done. */
    if (_BUTTON_firstInitialization() != BUTTON_ERROR_OK)
        return NULL ;

    /* Make sure pin is valid */
    if (pin == NULL )
        return NULL ;

    /* Make sure pressed State is valid */
    if ((pressedState != GPIO_STATE_OFF) && (pressedState != GPIO_STATE_ON))
        return NULL ;

    /* Search for available handle */
    uint8_t i = 0;
    while ((BUTTON_handles[i].isAllocated) && (i < BUTTON_MAXIMUM_INSTANCES))
        i++;

    /* Make sure we have found one. */
    if (i >= BUTTON_MAXIMUM_INSTANCES)
        return NULL ;

    /* Initialize GPIO as input */
    if (GPIO_init(pin, GPIO_DIRECTION_INPUT, GPIO_STATE_OFF) != GPIO_ERROR_OK)
        return NULL ;

    /* Init parameters */
    BUTTON_handles[i].isAllocated = true;
    BUTTON_handles[i].isEnabled = false;
    BUTTON_handles[i].pin = pin;
    BUTTON_handles[i].pressedState = pressedState;
    BUTTON_handles[i].callback = NULL;
    BUTTON_handles[i].wasPressed = false;
    BUTTON_handles[i].wasReleased = false;

    PTD_intConfig(((GPIO_handleInfo_tp) (BUTTON_handles[i].pin))->port, ((GPIO_handleInfo_tp) (BUTTON_handles[i].pin))->bitIndex,
        BUTTON_RAISING_EDGE, BUTTON_FALLING_EDGE, BUTTON_INTERRUPT_ENABLE, _BUTTON_InterruptHandler);

    return (BUTTON_handle_tp) &BUTTON_handles[i];
}

/* API documentation is in the interface header */
BUTTON_errorTypes_t BUTTON_delete(BUTTON_handle_tp handle)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;

    /* Check handle parameters. */
    BUTTON_errorTypes_t error = _BUTTON_checkParameters(handleInfo);
    if (error != BUTTON_ERROR_OK)
        return error;

    handleInfo->isAllocated = false;
    handleInfo->isEnabled = false;

    return BUTTON_ERROR_OK;
}

/* API documentation is in the interface header */
BUTTON_errorTypes_t BUTTON_enable(BUTTON_handle_tp handle)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;

    /* Check handle parameters. */
    BUTTON_errorTypes_t error = _BUTTON_checkParameters(handleInfo);
    if (error != BUTTON_ERROR_OK)
        return error;

    /* Nothing to do if already enabled. */
    if (handleInfo->isEnabled)
        return BUTTON_ERROR_OK;

    /* Reset values and enable BUTTON. */
    handleInfo->wasPressed = 0;
    handleInfo->wasReleased = 0;
    handleInfo->lastPinState = GPIO_getPin(handleInfo->pin);
    handleInfo->isEnabled = true;

    return BUTTON_ERROR_OK;
}

/* API documentation is in the interface header */
BUTTON_errorTypes_t BUTTON_disable(BUTTON_handle_tp handle)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;

    /* Check handle parameters. */
    BUTTON_errorTypes_t error = _BUTTON_checkParameters(handleInfo);
    if (error != BUTTON_ERROR_OK)
        return error;

    /* disable BUTTON. */
    handleInfo->isEnabled = false;

    return BUTTON_ERROR_OK;
}

/* API documentation is in the interface header */
_Bool BUTTON_isPressed(BUTTON_handle_tp handle)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;

    /* Check handle parameters. */
    if (_BUTTON_checkParameters(handleInfo) != BUTTON_ERROR_OK)
        return false;

    /* Check pressed flag. */
    if (handleInfo->wasPressed)
    {
        /* Reset flag. */
        handleInfo->wasPressed = false;
        return true;
    }

    return false;
}

/* API documentation is in the interface header */
_Bool BUTTON_isReleased(BUTTON_handle_tp handle)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;

    /* Check handle parameters. */
    if (_BUTTON_checkParameters(handleInfo) != BUTTON_ERROR_OK)
        return false;

    /* Check released flag. */
    if (handleInfo->wasReleased)
    {
        /* Reset flag. */
        handleInfo->wasReleased = false;
        return true;
    }

    return false;
}

/* API documentation is in the interface header */
BUTTON_errorTypes_t BUTTON_setCallback(BUTTON_handle_tp handle, const BUTTON_callback_t callbackFunction, const uint32_t userParameter)
{
    BUTTON_handleInfo_tp handleInfo = (BUTTON_handleInfo_tp) handle;
    /* check function parameters */
    if (callbackFunction == NULL )
        return BUTTON_ERROR_INVALID_PARAMETER;

    /* Check handle parameters. */
    BUTTON_errorTypes_t error = _BUTTON_checkParameters(handleInfo);
    if (error != BUTTON_ERROR_OK)
        return error;

    /* Remember handle last state before temporarily disabling it. */
    _Bool lastIsEnabled = handleInfo->isEnabled;

    /* set values. */
    handleInfo->isEnabled = false;
    handleInfo->callbackUserParameter = userParameter;
    handleInfo->callback = callbackFunction;

    if (lastIsEnabled)
        BUTTON_enable(handle);

    return BUTTON_ERROR_OK;
}
