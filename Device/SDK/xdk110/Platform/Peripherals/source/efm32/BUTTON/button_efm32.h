/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BUTTON_EFM32_H_
#define BUTTON_EFM32_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */

/* public type and macro definitions */

#define BUTTON_HANDLE_MAGIC_WORD          UINT16_C(0x0b21)       /**< Magic number to validate handle corrrectness */
#define BUTTON_MAXIMUM_INSTANCES          UINT8_C(16)            /**< Maximum number of LED handle instances */
#define BUTTON_DEFAULT_DEBOUNCE_MS        BUTTON_DEBOUNCE_25_MS  /**< Default de-bounce time for the button handle */
#define BUTTON_TIMER_INTERVAL_MS          UINT8_C(1)			 /**< Defines the time interval that the timer will be initialized to */
#define BUTTON_RAISING_EDGE                1                     /**< raising edge interrupt is selected for button interface */
#define BUTTON_FALLING_EDGE                1                     /**< falling edge interrupt is selected for button interface */
#define BUTTON_INTERRUPT_ENABLE            1                     /**< Interrupt is selected for button interface */

/** structure of all data members required for a timer instance */
typedef struct BUTTON_handleInfo_s
{
    uint16_t magicWord; /**< Magic word to be used for sanity testings */
    _Bool isAllocated; /**< Indicates if this handle already used by create function (for static allocation) */
    _Bool isEnabled; /**< Stores if the button is currently enabled */
    GPIO_handle_tp pin; /**< Button input signal pin location */
    GPIO_pinStates_t lastPinState; /**< Stores the last known pin state (so we can detect changes) */
    GPIO_pinStates_t pressedState; /**< Specifies whether button is normally closed or normally open */
    _Bool wasReleased; /**< Indicates the released state of the button */
    _Bool wasPressed; /**< Indicates the pressed state of the button */
    BUTTON_callback_t callback; /**< Stores the custom user callback function pointer */
    uint32_t callbackUserParameter; /**< Stores the custom user callback parameter */
} BUTTON_handleInfo_t, *BUTTON_handleInfo_tp;

#endif /* BUTTON_EFM32_H_ */
