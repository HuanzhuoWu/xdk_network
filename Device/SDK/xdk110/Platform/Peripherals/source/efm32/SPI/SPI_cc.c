/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include "BCDS_Basics.h"
#include "BCDS_Assert.h"

/* additional interface header files */
#include "em_cmu.h"
#include "em_int.h"
#ifdef ENABLE_DMA
#include "em_dma.h"
#endif
#include "SER_serialDriver_ih.h"

/* own header files */
#include "SPI_ih.h"
#include "SPI_ch.h"

/* constant definitions ***************************************************** */

/* #define LOG_SKIP_RECV */

/* local variables ********************************************************** */
#ifdef ENABLE_DMA

#define DMA_CHANNEL_RX_0   UINT8_C(0) /* prioritize RX over TX to solve missing RXs */
#define DMA_CHANNEL_TX_0   UINT8_C(2)
#define DMA_CHANNEL_RX_1   UINT8_C(1) /* prioritize RX over TX to solve missing RXs */
#define DMA_CHANNEL_TX_1   UINT8_C(3)
#define DMA_CHANNELS     UINT8_C(4)

#define MIN_DMA_SIZE 1

#if 1 < MIN_DMA_SIZE
#define SPI_DMA_ONLY 0
#else
#define SPI_DMA_ONLY 1
#endif

/* DMA Callback structure */
DMA_CB_TypeDef SpiDmaCallback = { .cbFunc = NULL };

/* Transfer Flags */
volatile bool dmaActive[DMA_CHANNELS];

/* Structure to configure transmission */
DMA_CfgDescr_TypeDef TxDescrCfgNone;
DMA_CfgDescr_TypeDef TxDescrCfgInc;

#endif
/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

static USART_TypeDef *getSpiSettings(SPI_serialPort_t port)
{
    if (SPI_USART0 == port)
    {
        return USART0;
    }
    else if (SPI_USART1 == port)
    {
        return USART1;
    }
    else if (SPI_USART2 == port)
    {
        return USART2;
    }
    assert(0);
    return null;
}

#ifdef LOG_SKIP_RECV
static SPI_portNumber_t getSpi(SPI_serialPort_t port)
{
    switch (port)
    {
    case SPI_USART0:
        return SPI0;
    case SPI_USART1:
        return SPI1;
    case SPI_USART2:
        return SPI2;
    default:
        break;
    }
    assert(0);
    return SPI0;
}
#endif

#ifdef ENABLE_DMA

/**
 * @brief  Call-back called when transfer is complete
 */
static void DataTransferCallback(unsigned int channel, bool primary, void *user)
{
    BCDS_UNUSED(primary);
    BCDS_UNUSED(user);

    if (DMA_CHANNELS > channel)
    {
        /* Clear flag to indicate complete transfer */
        dmaActive[channel] = false;
    }
}
/**
 * @brief Configure DMA in basic mode for both TX and RX to/from USART
 */
static void SetupDma(SPI_portNumber_t spi)
{
    /* Initialization structs */
    DMA_CfgChannel_TypeDef RxChannelCfg;
    DMA_CfgDescr_TypeDef RxDescrCfg;
    DMA_CfgChannel_TypeDef TxChannelCfg;

    if (NULL == SpiDmaCallback.cbFunc)
    {
        /* Initializing the DMA */
        DMA_Init_TypeDef DmaInit;
        DmaInit.hprot = 0;
        DmaInit.controlBlock = dmaControlBlock;
        DMA_Init(&DmaInit);

        /* Setup call-back function */
        SpiDmaCallback.cbFunc = DataTransferCallback;
        SpiDmaCallback.userPtr = NULL;
    }

    /* Setting up channel */
    RxChannelCfg.highPri = false;
    RxChannelCfg.enableInt = true;
    RxChannelCfg.cb = &SpiDmaCallback;

    /* Setting up channel descriptor */
    RxDescrCfg.dstInc = dmaDataInc1;
    RxDescrCfg.srcInc = dmaDataIncNone;
    RxDescrCfg.size = dmaDataSize1;
    RxDescrCfg.arbRate = dmaArbitrate1;
    RxDescrCfg.hprot = 0;

    /*** Setting up TX DMA ***/

    /* Setting up channel */
    TxChannelCfg.highPri = false;
    TxChannelCfg.enableInt = true;
    TxChannelCfg.cb = &SpiDmaCallback;

    TxDescrCfgInc.dstInc = dmaDataIncNone;
    TxDescrCfgInc.srcInc = dmaDataInc1;
    TxDescrCfgInc.size = dmaDataSize1;
    TxDescrCfgInc.arbRate = dmaArbitrate1;
    TxDescrCfgInc.hprot = 0;

    TxDescrCfgNone = TxDescrCfgInc;
    TxDescrCfgNone.srcInc = dmaDataIncNone;

    switch (spi)
    {
    case SPI0:
        /*** Setting up RX DMA ***/
        /* Setting up channel */
        RxChannelCfg.select = DMAREQ_USART0_RXDATAV;
        DMA_CfgChannel(DMA_CHANNEL_RX_0, &RxChannelCfg);

        /* Setting up channel descriptor */
        DMA_CfgDescr(DMA_CHANNEL_RX_0, true, &RxDescrCfg);

        /*** Setting up TX DMA ***/

        /* Setting up channel */
        TxChannelCfg.select = DMAREQ_USART0_TXBL;
        DMA_CfgChannel(DMA_CHANNEL_TX_0, &TxChannelCfg);

        /* Setting up channel descriptor */
        DMA_CfgDescr(DMA_CHANNEL_TX_0, true, &TxDescrCfgInc);
        dmaActive[DMA_CHANNEL_TX_0] = false;
        dmaActive[DMA_CHANNEL_RX_0] = false;
        break;
    case SPI1:
        /*** Setting up RX DMA ***/
        /* Setting up channel */
        RxChannelCfg.select = DMAREQ_USART1_RXDATAV;
        DMA_CfgChannel(DMA_CHANNEL_RX_1, &RxChannelCfg);

        /* Setting up channel descriptor */
        DMA_CfgDescr(DMA_CHANNEL_RX_1, true, &RxDescrCfg);

        /*** Setting up TX DMA ***/

        /* Setting up channel */
        TxChannelCfg.select = DMAREQ_USART1_TXBL;
        DMA_CfgChannel(DMA_CHANNEL_TX_1, &TxChannelCfg);

        /* Setting up channel descriptor */
        DMA_CfgDescr(DMA_CHANNEL_TX_1, true, &TxDescrCfgInc);
        dmaActive[DMA_CHANNEL_TX_1] = false;
        dmaActive[DMA_CHANNEL_RX_1] = false;
        break;
    case SPI2:
        break;
    }
}

/**
 * @brief  Returns if an SPI transfer is active
 */
static bool IsDmaActive(SPI_portNumber_t spi)
{
    bool temp = false;
    ;

    switch (spi)
    {
    case SPI0:
        temp = dmaActive[DMA_CHANNEL_RX_0];
        temp = temp | dmaActive[DMA_CHANNEL_TX_0];
        break;
    case SPI1:
        temp = dmaActive[DMA_CHANNEL_RX_1];
        temp = temp | dmaActive[DMA_CHANNEL_TX_1];
        break;
    case SPI2:
        break;
    }
    return temp;
}

/**
 * @brief  SPI DMA Transfer
 * NULL can be input as txBuffer if tx data to transmit dummy data
 * If only sending data, set rxBuffer as NULL to skip DMA activation on RX
 */
static bool SpiDmaTransfer(SPI_portNumber_t spi, uint8_t *txBuffer, uint8_t *rxBuffer, uint32_t bytes)
{
    if ((txBuffer == NULL) && (rxBuffer == NULL))
    {
        return false;
    }
#if !SPI_DMA_ONLY
    switch (spi)
    {
    case SPI0:
        NVIC_DisableIRQ(USART0_TX_IRQn);
        NVIC_DisableIRQ(USART0_RX_IRQn);
        break;
    case SPI1:
        NVIC_DisableIRQ(USART1_TX_IRQn);
        NVIC_DisableIRQ(USART1_RX_IRQn);
        break;
    case SPI2:
        break;
    }
#endif

    if (rxBuffer != NULL)
    {
        switch (spi)
        {
        case SPI0:
            /* Setting flag to indicate that RX is in progress
             * will be cleared by call-back function */
            dmaActive[DMA_CHANNEL_RX_0] = true;
            /* Clear RX/TX register */
            USART0->CMD = USART_CMD_CLEARTX | USART_CMD_CLEARRX;

            /* Activate RX channel */
            DMA_ActivateBasic(DMA_CHANNEL_RX_0,
            true,
            false,
                    rxBuffer,
                    (void *) &(USART0->RXDATA),
                    (unsigned int) (bytes - 1));
            break;
        case SPI1:
            /* Setting flag to indicate that RX is in progress
             * will be cleared by call-back function */
            dmaActive[DMA_CHANNEL_RX_1] = true;
            /* Clear RX/TX register */
            USART1->CMD = USART_CMD_CLEARTX | USART_CMD_CLEARRX;

            /* Activate RX channel */
            DMA_ActivateBasic(DMA_CHANNEL_RX_1,
            true,
            false,
                    rxBuffer,
                    (void *) &(USART1->RXDATA),
                    (unsigned int) (bytes - 1));
            break;
        case SPI2:
            break;
        }
    }
    /* Only activate RX DMA if a receive buffer is specified */
    if (txBuffer != NULL)
    {
        switch (spi)
        {
        case SPI0:
            /* Setting flag to indicate that TX is in progress
             * will be cleared by call-back function */
            dmaActive[DMA_CHANNEL_TX_0] = true;

            /* Clear RX/TX registers */
            USART0->CMD = USART_CMD_CLEARTX | USART_CMD_CLEARRX;

            /* Activate TX channel */
            DMA_ActivateBasic(DMA_CHANNEL_TX_0,
            true,
            false,
                    (void *) &(USART0->TXDATA),
                    txBuffer,
                    (unsigned int) (bytes - 1));
            break;
        case SPI1:
            /* Setting flag to indicate that TX is in progress
             * will be cleared by call-back function */
            dmaActive[DMA_CHANNEL_TX_1] = true;

            /* Clear RX/TX registers */
            USART1->CMD = USART_CMD_CLEARTX | USART_CMD_CLEARRX;

            /* Activate TX channel */
            DMA_ActivateBasic(DMA_CHANNEL_TX_1,
            true,
            false,
                    (void *) &(USART1->TXDATA),
                    txBuffer,
                    (unsigned int) (bytes - 1));
            break;
        case SPI2:
            break;
        }
    }

    while (IsDmaActive(spi))
        ;

#if !SPI_DMA_ONLY
    switch (spi)
    {
    case SPI0:
        USART0->CMD = USART_CMD_CLEARRX /* | USART_CMD_CLEARTX*/ ;
        USART0->IFC = USART_IF_RXDATAV | USART_IFC_TXC;
        NVIC_ClearPendingIRQ(USART0_TX_IRQn);
        NVIC_ClearPendingIRQ(USART0_RX_IRQn);
        NVIC_EnableIRQ(USART0_TX_IRQn);
        NVIC_EnableIRQ(USART0_RX_IRQn);
        break;
    case SPI1:
        USART1->CMD = USART_CMD_CLEARRX /*| USART_CMD_CLEARTX*/;
        USART1->IFC = USART_IF_RXDATAV | USART_IFC_TXC;
        NVIC_ClearPendingIRQ(USART1_TX_IRQn);
        NVIC_ClearPendingIRQ(USART1_RX_IRQn);
        NVIC_EnableIRQ(USART1_TX_IRQn);
        NVIC_EnableIRQ(USART1_RX_IRQn);
        break;
    case SPI2:
        break;
    }
#endif
    return true;
}
#endif
/* global functions ********************************************************* */

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_init(SPI_device_t *spiHandle, const SPI_initParams_t *initParams)
{
    SER_errorCode_t serReturn = SER_SUCCESS;
    SER_init_t initData;
    USART_TypeDef *spiSettings;
    /* Check the validity of input parameters */
    if ((!spiHandle) || (!initParams))
    {
        return (SPI_INVALID_PARAMETER);
    }

    /* Enable the clock */
    uint32_t perClockFreq = CMU_ClockFreqGet(cmuClock_HFPER);

    /* Fill the parameters based on the port number*/
    switch (initParams->portNumber)
    {
    case SPI0:
        CMU_ClockEnable(cmuClock_USART0, true);
        spiSettings = USART0;
        initData.hwDevicePort = SER_USART0;
        initData.txCallback = NULL;
        initData.rxCallback = NULL;
#ifdef ENABLE_DMA
        SetupDma(SPI0);
#endif
        break;
    case SPI1:
        CMU_ClockEnable(cmuClock_USART1, true);
        spiSettings = USART1;
        initData.hwDevicePort = SER_USART1;
        initData.txCallback = NULL;
        initData.rxCallback = NULL;
#ifdef ENABLE_DMA
        SetupDma(SPI1);
#endif
        break;
    case SPI2:
        CMU_ClockEnable(cmuClock_USART2, true);
        spiSettings = USART2;
        initData.hwDevicePort = SER_USART2;
        initData.txCallback = NULL;
        initData.rxCallback = NULL;
        break;
    default:
        return (SPI_INVALID_PARAMETER);
        break;
    }

    /* Fill the USART related parameters */
    spiSettings->CMD = USART_CMD_MASTEREN | USART_CMD_TXEN | USART_CMD_RXEN;
    spiSettings->CLKDIV = ((perClockFreq / initParams->baudrate) - 2UL) * 128UL;
    spiSettings->CTRL = USART_CTRL_SYNC;
    spiSettings->CMD = USART_CMD_CLEARRX | USART_CMD_CLEARTX;
    spiSettings->IEN = 0;
    spiSettings->CTRL |= USART_CTRL_MSBF | USART_CTRL_TXBIL_HALFFULL;
    switch (initParams->clockMode)
    {
    case SPI_CLOCK_MODE_CPOL0_CPHA0:
        spiSettings->CTRL &= (~USART_CTRL_CLKPOL);
        spiSettings->CTRL &= (~USART_CTRL_CLKPHA);
        break;
    case SPI_CLOCK_MODE_CPOL0_CPHA1:
        spiSettings->CTRL &= (~USART_CTRL_CLKPOL);
        spiSettings->CTRL |= (USART_CTRL_CLKPHA);
        break;
    case SPI_CLOCK_MODE_CPOL1_CPHA0:
        spiSettings->CTRL |= (USART_CTRL_CLKPOL);
        spiSettings->CTRL &= (~USART_CTRL_CLKPHA);
        break;
    case SPI_CLOCK_MODE_CPOL1_CPHA1:
        spiSettings->CTRL |= (USART_CTRL_CLKPOL);
        spiSettings->CTRL |= (USART_CTRL_CLKPHA);
        break;
    default:
        return (SPI_INVALID_PARAMETER);
        break;
    }
    spiSettings->IFC = _USART_IFC_MASK;
    spiSettings->ROUTE |= USART_ROUTE_TXPEN | USART_ROUTE_RXPEN | USART_ROUTE_CLKPEN | (initParams->routeLocation << 8);
    initData.hwType = SER_HWTYPE_USART;
    initData.protocol = SER_SPI_PROTOCOL;
    initData.txBuf_p = initParams->txBuf_p;
    initData.txBufSize = initParams->txBufSize; /* Capacity in bytes of the transmit buffer */
    initData.rxBuf_p = initParams->rxBuf_p;
    initData.rxBufSize = initParams->rxBufSize;/* Size of the receive circular buffer in bytes */
    initData.parity = SER_PARITY_NONE;
    initData.dataBits = SER_DATABITS_8;
    initData.stopBits = SER_STOPBITS_ONE;
    initData.routeLocation = SER_ROUTE_LOCATION1;
    serReturn = SER_serialInit((SER_device_t*) spiHandle, &initData);

    if (serReturn != SER_SUCCESS)
    {
        return (SPI_SERIAL_INIT_FAILED);
    }

#if SPI_DMA_ONLY
    switch (initParams->portNumber)
    {
    case SPI0:
        NVIC_DisableIRQ(USART0_TX_IRQn);
        NVIC_DisableIRQ(USART0_RX_IRQn);
        break;
    case SPI1:
        NVIC_DisableIRQ(USART1_TX_IRQn);
        NVIC_DisableIRQ(USART1_RX_IRQn);
        break;
    case SPI2:
        break;
    }
#endif

    return (SPI_SUCCESS);
}

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_write(SPI_device_t *spiHandle, const uint8_t *transmitBuffer, uint32_t transmitBufferSize, SPI_writeFlag_t writeOnly)
{
    /* Check the validity of input parameters */
    if ((!spiHandle) || (!transmitBuffer) || (!transmitBufferSize))
    {
        return (SPI_INVALID_PARAMETER);
    }

#ifdef ENABLE_DMA
    if (SPI_USART0 == (spiHandle->hwDevicePort) || SPI_USART1 == (spiHandle->hwDevicePort))
    {
        if (MIN_DMA_SIZE <= transmitBufferSize)
        {
            unsigned int channel = DMA_CHANNEL_TX_0;
            SPI_portNumber_t spi = SPI0;
            if (SPI_USART1 == (spiHandle->hwDevicePort))
            {
                channel = DMA_CHANNEL_TX_1;
                spi = SPI1;
            }
            DMA_CfgDescr(channel, true, &TxDescrCfgInc);
            bool ReturnValue = SpiDmaTransfer(spi, (uint8_t *) transmitBuffer, NULL, transmitBufferSize);
            if (0 < spiHandle->rxBufDescr.Count)
            {
#ifdef LOG_SKIP_RECV
                printf("DMA SPI%d_write %lu, skip after %hu bytes\n", getSpi(spiHandle->hwDevicePort), transmitBufferSize, spiHandle->rxBufDescr.Count);
#endif
                SER_serialResetBuffer((SER_device_t*) spiHandle);
            }
            if (false == ReturnValue)
            {
                return (SPI_FAILURE);
            }
            return (SPI_SUCCESS);
        }
    }
#endif
    uint32_t byteCount = 0;
    USART_TypeDef *spiSettings = getSpiSettings(spiHandle->hwDevicePort);
    SER_errorCode_t serReturn = SER_SUCCESS;

    if (NULL == spiSettings)
        return (SPI_INVALID_PARAMETER);

    if (writeOnly == SPI_WRITE_ONLY)
    {
        spiSettings->CMD = USART_CMD_RXDIS;
        if (0 < spiHandle->rxBufDescr.Count)
        {
#ifdef LOG_SKIP_RECV
            printf("SPI%d_write %lu, skip before %hu bytes\n", getSpi(spiHandle->hwDevicePort), transmitBufferSize, spiHandle->rxBufDescr.Count);
#endif
            SER_serialResetBuffer((SER_device_t*) spiHandle);
        }
    }

    for (byteCount = 0; byteCount < transmitBufferSize; byteCount++)
    {
        serReturn = SER_serialWrite((SER_device_t*) spiHandle, NULL, &transmitBuffer[byteCount], 1);
        if (serReturn != SER_SUCCESS)
        {
            return (SPI_SERIAL_WRITE_FAILED);
        }

        while (0 == (spiSettings->STATUS & USART_STATUS_TXC))
            ;
    } /* End of For loop - byteCount < transmitBufferSize */
    if (writeOnly == SPI_WRITE_ONLY)
    {
        if (0 < spiHandle->rxBufDescr.Count)
        {
#ifdef LOG_SKIP_RECV
            printf("SPI%d_write %lu, skip after %hu bytes\n", getSpi(spiHandle->hwDevicePort), transmitBufferSize, spiHandle->rxBufDescr.Count);
#endif
            SER_serialResetBuffer((SER_device_t*) spiHandle);
        }
        spiSettings->CMD = USART_CMD_RXEN;
#if 0
        /* Read the unwanted data */
        uint8_t rcvbuff[1];
        uint32_t remainingBytes = 0; /* Currently not used*/
        for (byteCount = 0; byteCount < transmitBufferSize; byteCount++)
        {
            serReturn = SER_serialRead((SER_device_t*) spiHandle, &remainingBytes, rcvbuff, 1);
            if (serReturn != SER_SUCCESS)
            {
                return (SPI_SERIAL_READ_FAILED);
            }
        }
#endif
    }

    return (SPI_SUCCESS);
}

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_read(SPI_device_t *spiHandle, uint8_t *receiveBuffer, uint32_t receiveBufferSize)
{
    /* Check the validity of input parameters */
    if ((!spiHandle) || (!receiveBuffer) || (!receiveBufferSize))
    {
        return (SPI_INVALID_PARAMETER);
    }

#ifdef ENABLE_DMA
    if (SPI_USART0 == (spiHandle->hwDevicePort) || SPI_USART1 == (spiHandle->hwDevicePort))
    {
        if (MIN_DMA_SIZE <= receiveBufferSize)
        {
            unsigned int channel = DMA_CHANNEL_TX_0;
            SPI_portNumber_t spi = SPI0;
            uint8_t dummybytes = 0xFF;
            if (SPI_USART1 == (spiHandle->hwDevicePort))
            {
                channel = DMA_CHANNEL_TX_1;
                spi = SPI1;
            }
            DMA_CfgDescr(channel, true, &TxDescrCfgNone);
            /* just clear buffer, to see, if the content is "really received" */
            memset(receiveBuffer, 0, receiveBufferSize);
            bool ReturnValue = SpiDmaTransfer(spi, &dummybytes, receiveBuffer, receiveBufferSize);
            if (0 < spiHandle->rxBufDescr.Count)
            {
#ifdef LOG_SKIP_RECV
                printf("DMA SPI%d_read %lu, skip after %hu bytes\n", getSpi(spiHandle->hwDevicePort), receiveBufferSize, spiHandle->rxBufDescr.Count);
#endif
                SER_serialResetBuffer((SER_device_t*) spiHandle);
            }
            if (false == ReturnValue)
            {
                return (SPI_FAILURE);
            }
            return (SPI_SUCCESS);
        }
    }
#endif
    /* Dummy write to receive the data from other end */
    uint8_t transmitBuffer = 0xFF;
    uint32_t index = 0;
    uint32_t remainingBytes = 0; /* Currently not used*/
    USART_TypeDef *spiSettings = getSpiSettings(spiHandle->hwDevicePort);
    SER_errorCode_t serReturn = SER_SUCCESS;

    SER_serialResetBuffer((SER_device_t*) spiHandle);

    if (0 < spiHandle->rxBufDescr.Count)
    {
#ifdef LOG_SKIP_RECV
        printf("SPI%d_read %lu, skip before %hu bytes\n", getSpi(spiHandle->hwDevicePort), receiveBufferSize, spiHandle->rxBufDescr.Count);
#endif
        SER_serialResetBuffer((SER_device_t*) spiHandle);
    }

    for (index = 0; index < receiveBufferSize; index++)
    {
        serReturn = SER_serialWrite((SER_device_t*) spiHandle, NULL, &transmitBuffer, 1);
        if (serReturn != SER_SUCCESS)
        {
            return (SPI_SERIAL_WRITE_FAILED);
        }
        while (0 == (spiSettings->STATUS & USART_STATUS_TXC))
            ;
    }
    serReturn = SER_serialRead((SER_device_t*) spiHandle, &remainingBytes, receiveBuffer, receiveBufferSize);
    if (0 < spiHandle->rxBufDescr.Count)
    {
#ifdef LOG_SKIP_RECV
        printf("SPI%d_read %lu, skip after %hu bytes\n", getSpi(spiHandle->hwDevicePort), receiveBufferSize, spiHandle->rxBufDescr.Count);
#endif
        SER_serialResetBuffer((SER_device_t*) spiHandle);
    }
    if (serReturn != SER_SUCCESS)
    {
        return (SPI_SERIAL_READ_FAILED);
    }

    return (SPI_SUCCESS);
}

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_writeRead(SPI_device_t *spiHandle, uint8_t *transmitBuffer, uint32_t transmitBufferSize,
        uint8_t *receiveBuffer, uint32_t receiveBufferSize)
{
    SPI_return_t retVal = SPI_SUCCESS;

    /* Check the validity of input parameters */
    if ((spiHandle != NULL) && (transmitBuffer != NULL) && (receiveBuffer != NULL) &&
            (transmitBufferSize != 0) && (receiveBufferSize != 0))
    {
        retVal = SPI_write(spiHandle, transmitBuffer, transmitBufferSize, SPI_WRITE_ONLY);
        if (retVal == SPI_SUCCESS)
        {
            retVal = SPI_read(spiHandle, receiveBuffer, receiveBufferSize);
        }
    }
    else
    {
        retVal = SPI_INVALID_PARAMETER;
    }
    return (retVal);
}

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_deinit(SPI_device_t *spiHandle)
{
    SPI_return_t retVal = SPI_SUCCESS;
    if (spiHandle != NULL)
    {
        spiHandle = NULL;
    }
    return (retVal);
}

/* Refer API documentation is in the interface header file SPI_ih.h */
SPI_return_t SPI_writeReadInOut(SPI_device_t *spiHandle, const uint8_t *transmitData, uint8_t *receiveData)
{
    USART_TypeDef *spiSettings;
    SER_errorCode_t serReturn = SER_SUCCESS;

    /* Check the validity of input parameters */
    if ((!spiHandle) || (!transmitData))
    {
        return (SPI_INVALID_PARAMETER);
    }
    spiSettings = getSpiSettings(spiHandle->hwDevicePort);
    if (NULL == spiSettings)
        return (SPI_INVALID_PARAMETER);

    serReturn = SER_serialWrite((SER_device_t*) spiHandle, NULL, &transmitData[0], 1);
    if (serReturn != SER_SUCCESS)
    {
        return (SPI_SERIAL_WRITE_FAILED);
    }
    while (0 == (spiSettings->STATUS & USART_STATUS_TXC))
        ;
    while (0 == (spiSettings->STATUS & USART_STATUS_RXDATAV))
        ;

    if (spiHandle->hwDevicePort == SPI_USART0)
    {
        *receiveData = SER_readDataFromUSART0Buffer();
    }
    else if (spiHandle->hwDevicePort == SPI_USART1)
    {
        *receiveData = SER_readDataFromUSART1Buffer();
    }
    else if (spiHandle->hwDevicePort == SPI_USART2)
    {
        *receiveData = SER_readDataFromUSART2Buffer();
    }
    return (SPI_SUCCESS);
}

/**************************************************************************** */
