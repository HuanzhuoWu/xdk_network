/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup LED_Driver LED
 * @brief LED Driver
 * @ingroup Peripheral
 *
 * @{
 * @brief  This module provides interface for LED module.
 * 
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef LED_H_
#define LED_H_

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */
#include "gpio.h"

/* public type and macro definitions */

/** enum values to represent error type options */
typedef enum LED_errorTypes_e
{
    LED_ERROR_OK, /**< No error - everything is OK */
    LED_ERROR_INVALID_PARAMETER, /**< One of the input parameters is invalid */
    LED_ERROR_HANDLE_IS_NULL, /**< The handle parameter is an invalid NULL value  */
    LED_ERROR_HANDLE_DATA_CORRUPTED,/**< The handle parameter is an invalid or corrupted handle object */
    LED_ERROR_HANDLE_DEALLOCATED, /**< The handle parameter is not a valid because it was deleted  */
    LED_ERROR_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} LED_errorTypes_t, *LED_errorTypes_tp;

/** enum values to represent LED state operations */
typedef enum LED_operations_e
{
    LED_SET_OFF, /**< Turn off the led */
    LED_SET_ON, /**< Turn on the led */
    LED_SET_TOGGLE, /**< Change current led state */
    LED_SET_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} LED_operations_t, *LED_operations_tp;

/** Defines the handle to the LED object this api operates on.
 * The handle needs to be obtained using the ::LED_create function.
 */
typedef void* LED_handle_tp;

/* public function prototype declarations */

/**
 * @brief Creates a LED object handle according to the provided GPIO handle info. 
 * This function needs to be called to create a LED object handle. The created LED handle can be
 * used to manifulate the LED state using the LED controlling functions.
 *
 * @param [in] pin the handle info to the GPIO pin that is connected to the LED.
 * @param [in] offState the GPIO pin state that turns the LED off.
 *
 * @retval LED_handle_tp the created LED object handle.\nFunction will return NULL if an error occurs.
 */
LED_handle_tp LED_create(const GPIO_handle_tp pin, const GPIO_pinStates_t offState);

/**
 * @brief Free and release a LED handle. 
 * This function can be called to release a LED handle.
 *
 * @param [in] handle the LED handle (acquired by ::LED_create) that needs to be released.
 *
 * @retval     LED_ERROR_OK                    LED handle was deleted successfully
 * @retval     LED_ERROR_HANDLE_IS_NULL        LED handle deletion failed because a NULL handle was given as an argument
 * @retval     LED_ERROR_HANDLE_DATA_CORRUPTED LED handle deletion failed because a corrupted handle was given as an argument
 * @retval     LED_ERROR_HANDLE_DEALLOCATED    LED handle deletion failed because the has already been deleted
 */
LED_errorTypes_t LED_delete(LED_handle_tp handle);

/**
 * @brief Change a LED state.
 * This function provide control over the LED state. 
 *
 * @param [in] handle the handle to the LED object returned by ::LED_create
 * @param [in] state the new LED state
 *
 * @retval     LED_ERROR_OK                     Led state was changed successfully
 * @retval     LED_ERROR_HANDLE_IS_NULL         Failed to change led state due to invalid handle object argument
 * @retval     LED_ERROR_HANDLE_DATA_CORRUPTED  Failed to change led state due to corrupted handle object argument
 * @retval     LED_ERROR_HANDLE_DEALLOCATED     Failed to change led state due to usage of a deallocated handle object
 */
LED_errorTypes_t LED_setState(LED_handle_tp handle, const LED_operations_t state);

#endif /* LED_H_ */
