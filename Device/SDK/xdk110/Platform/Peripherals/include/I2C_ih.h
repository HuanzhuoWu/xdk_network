/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup I2C_Support I2C
 * @brief I2C Driver
 * @ingroup Peripheral
 *
 * @{
 * @brief  The interface header exports the following features:
 *  												- I2C_init()
 *  												- I2C_delayFunction()
 *  												- I2C_i2cDecodeCallbackFunction()
 *  												- I2C_busWriteChannel0()
 *  												- I2C_busWriteChannel1()
 *  												- I2C_busWrite()
 *  												- I2C_busReadChannel0()
 *  												- I2C_busReadChannel1()
 *  												- I2C_busRead()
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef I2C_IH_H_
#define I2C_IH_H_

/* public interface declaration ********************************************* */
#include "PTD_portDriver_ih.h"
#undef _INC_SID_MODULE
#include "SID_serialDriverInf_ih.h"
#define _INC_SID_MODULE

#define I2C_SEND_DATA_COPY_SIZE 30 /**< Size of the local buffer containing send message */
#define I2C_TEMP_DATA_COPY_SIZE 30 /**<Size of the local buffer containing temp message */
#define I2C_READ_DATA_COPY_SIZE 30 /**<Size of the local buffer containing read message */
#define I2C_SEND_DATA_SIZE 3 /**< Size of the data writing on the I2C line */
#define I2C_TEMP_DATA_SIZE 2 /**< Size of the stored temporarily data writing on the I2C line */

/* public type and macro definitions */
typedef struct I2C_interface_s
{
    SID_i2cTrnsfrType_t trasferType; /**< Instruction to indicate whether I2C of type READ,WRITE or READ/WRITE */
    I2C_channel_t i2cChannel; /**< I2C channel I2C0 or I2C1*/
    uint8_t i2cAddress; /**< I2C address*/
    uint8_t sendData[I2C_SEND_DATA_SIZE]; /**< Bytes which need to be sent*/
    uint8_t noOfSendData; /**< Number of bytes need to be sent*/
    uint8_t tempData[I2C_TEMP_DATA_SIZE]; /**< Bytes which need to be stored temporarily. ( This variable may be used for if we want to pass the data between two transfer )*/
    uint8_t noOfTempData; /**< Number of bytes which need to be stored temporarily.*/
    uint8_t readLength; /**< Number of bytes which need to be read in a I2C command.*/
    void (*callback)(SID_i2cTrnsfrInfo_t* transferInfo); /**< callback which needs to be called once all the transcation is completed.*/
    void (*tempCallbackBuffer)(SID_i2cTrnsfrInfo_t* transferInfo); /**< A buffer to store any callback. This will be useful, if we are doing multiple transcation.*/
} I2C_interface_s;

/**
 *  Enum to represent the I2C channels
 */
typedef enum I2C_channelSelect_e
{
    I2CCHANNEL_0,
    I2CCHANNEL_1,
} I2C_channelSelect_t;

/**
 * structure to hold I2C related details
 */
typedef struct I2C_init_s
{
    I2C_channelSelect_t i2cChannel; /* I2C channel I2C0 or I2C1*/
    PTD_configInit_t i2cDataPinConfig;/* I2C data port pin details*/
    PTD_configInit_t i2cClockPinConfig;/* I2C clock port pin details*/
    uint32_t i2cLocation;/*I2C location*/
} I2C_init_t, *I2C_init_tp;

/** struct represent the i2c configurations (channel and the corresponding i2c address) */
struct I2C_BusInfo_S
{
    I2C_channelSelect_t I2C_Channel;
    uint8_t I2C_Address;
};
typedef struct I2C_BusInfo_S I2C_BusInfo_T;


typedef uint64_t Time_t;
/* public function prototype declarations */

/* public global variable declarations */
#define I2C_SERIAL_I2C_CHANNEL_QUEUE_SIZE 0 /**<I2C_SERIAL_I2C_CHANNEL_QUEUE_SIZE = 0==> Polling method*/

/* inline function definitions */

/**
 *@brief        : This Function initializes the I2C based on the structure input
 *
 *@param [in]   : Struct to hold the I2C related details like channel,location,port pins
 */
void I2C_init(I2C_init_tp i2c);

/**
 * @brief Delay function
 *
 * @param [in] I2C_msecDelay    : Delay in ms
 *
 */
void I2C_delayFunction(uint32_t I2C_msecDelay);

/**
 * @brief Decode callback function: It is translation of I2C_interface_s into SID_i2cTrnsfrInfo_s
 *
 * @param [in]     : Pointer to the I2C configuration buffer
 *
 * @param [in]     : Structure to store device information and data that uses i2c bus
 *
 */
void I2C_i2cDecodeCallbackFunction(void *i2cBuffer,SID_i2cTrnsfrInfo_t *infoTransfer);

/**
 * @brief 		: The I2C write API which is mapped to the sensors connected on the channel0
 *
 * @param [in]  : Device address of the Sensor
 *
 * @param [in]  : Register address to be written
 *
 * @param [in]  : Pointer to the data buffer
 *
 * @param [in]  : Length of the data
 *
 * @retval 		: 0  Success
 *             	: -1 Failure
 *
 */
int8_t I2C_busWriteChannel0(unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len);

/*
 * @brief 				: The I2C write API which is mapped to the sensors connected on the channel1
 *
 * @param [in] 			: Device address of the Sensor
 *
 * @param [in] 			: Register address to be written
 *
 * @param [in] 			: Pointer to the data buffer
 *
 * @param [in] 			: Length of the data
 *
 * @retval 				: 0  Success
 *             	        : -1 Failure
 *
 */
int8_t I2C_busWriteChannel1(unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len);

/**
 * @brief 				: The I2C write API which is mapped to the sensors.
 *
 * @param [in] channel  : Channel which is connected to the Sensor
 *
 * @param [in] dev_addr : Device address of the Sensor
 *
 * @param [in] reg_addr : Register address to be written
 *
 * @param [in] data_p   : Pointer to the data buffer
 *
 * @param [in] wr_len   : Length of the data
 *
 * @retval 				: 0  Success
 *             	        : -1 Failure
 *
 */
int8_t I2C_busWrite(I2C_channel_t i2cChannel,unsigned char dev_addr, unsigned char reg_addr, unsigned char *data_p, unsigned char wr_len);

/**
 * @brief				  : The I2C read API which is mapped to the sensors connected to Channel0
 *
 * @param [in] dev_addr   : Device address of the Sensor
 *
 * @param [in] targetAddr : Register address to be written
 *
 * @param [in] reg_data   : Pointer to the data buffer
 *
 * @param [in] r_len      : Length of the data
 *
 * @retval 				  : 0  Success
 *             	          : -1 Failure
 *
 */
int8_t I2C_busReadChannel0(unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len);

/*
 * @brief				  : The I2C write API which is mapped to the sensors connected to Channel1
 *
 * @param [in] dev_addr   : Device address of the Sensor
 * @param [in] targetAddr : Register address to be written
 * @param [in] reg_data   : Pointer to the data buffer
 * @param [in] r_len   	  : Length of the data
 *
 * @retval 				  : 0  Success
 *             	          : -1 Failure
 *
 */
int8_t I2C_busReadChannel1(unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len);

/**
 * @brief				  : The I2C write API which is mapped to the sensors
 *
 * @param [in] channel    : Channel which is connected to the Sensor
 *
 * @param [in] dev_addr   : Device address of the Sensor
 *
 * @param [in] targetAddr : Register address to be written
 *
 * @param [in] reg_data   : Pointer to the data buffer
 *
 * @param [in] r_len      : Length of the data
 *
 * @retval 				  :  0  Success
 *             	          : -1  Failure
 *
 */
int8_t I2C_busRead(I2C_channel_t i2cChannel, unsigned char dev_addr, unsigned char targetAddr, unsigned char *reg_data, unsigned char r_len);

#endif /* I2C_IH_H_ */

/**@}*/

/** ************************************************************************* */
