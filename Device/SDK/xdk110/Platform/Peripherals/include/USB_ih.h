/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 *  @defgroup   USB USB
 *  @ingroup    Peripheral
 *
 *  @{
 *  @brief USB Driver Interface
 *  @details
 *
 *  The interface header exports the following features:
 * 													 - USB_init()
 *  												 - USB_transmitByte()
 *  												 - USB_receiveByte()
 *
 * ****************************************************************************/
/* header definition ******************************************************** */
#ifndef BCDS_USB_IH_H_
#define BCDS_USB_IH_H_

/* public interface declaration ********************************************* */
#include <stdint.h>
#include <stdio.h>
#include "em_usb.h"
/* public type and macro definitions */

/** Return status for the USB transfer  */
typedef enum USB_returnCode_e
{
    USB_SUCCESS = INT8_C(0),
    USB_FAILURE = INT8_C(-1),
} USB_returnCode_t;

/** Define the prototype to which USB receive callback functions must confirm.
 *
 * Functions implementing USB receive callbacks do not need to return, therefore their return type
 * is void none.
 *
 * Example task function prototype which confirms to the type declaration:
 * @code void EXL_exampleTxCallbackFunction(buffer,length); @endcode
 */
typedef void (*USB_rxCallback)(uint8_t *, uint16_t);

/** The serial port LINE CODING data structure, used to carry information
 *  about serial port baudrate, parity etc. between host and device.       
 */
typedef struct USB_lineCoding_s
{
    uint32_t Baudrate; /**< Baudrate                            */
    uint8_t charFormat; /**< Stop bits, 0=1 1=1.5 2=2            */
    uint8_t parityType; /**< 0=None 1=Odd 2=Even 3=Mark 4=Space  */
    uint8_t dataBits; /**< 5, 6, 7, 8 or 16                    */
    uint8_t dummy; /**< To ensure size is a multiple of 4 bytes.*/
    USB_rxCallback usbRxCallback; /**< received callback**/

}__attribute__ ((packed)) USB_lineCoding_t, *USB_lineCoding_tp;

/* public function prototype declarations */

/**
 * @brief       Initializes the USB device
 * @param[in]   lineCoding :  The usb port LINE CODING data structure, used to carry information
 *                                    about serial port baudrate, parity etc. between host and device.
 */
void USB_init(USB_lineCoding_tp lineCoding);

/**
 * @brief       Transmit single byte to USB.(only for printf purpose)
 * @param[in]   byte : The byte to be sent
 * @retVal      USB_FAILURE : API call failed
 * @retVal      USB_SUCCESS : API call succeeded
 *
 * @warning : This API was made to transmit the data send through printf API. This API is blocking
 *          TODO interrupt based USB transfers must be implemented.
 */
USB_returnCode_t USB_transmitByte(uint8_t byte);

/**
 * @brief       Get single byte from USB
 * @param[in]   byte : Pointer to which the received byte must be written
 * @retVal      USB_FAILURE : API call failed
 * @retVal      USB_SUCCESS : API call succeeded
 *
 * @warning : This API was made to scan the data received through scanf API. This API is blocking
 *          TODO interrupt based USB transfers must be implemented
 */
USB_returnCode_t USB_receiveByte(uint8_t* byte);

/**
 * @brief       Transmit Data to USB.(only for printf purpose)
 * @param[in]   data : buffer of data that to be transmitted
 * @param[in]   len  : length of data that to be transmitted
 * @retVal      USB_FAILURE : API call failed
 * @retVal      USB_SUCCESS : API call succeeded
 *
 * @warning : This API was made to transmit the data send through printf API.
 *          This API is blocking and other API(TBD) must be used for interrupt based USB transfers
 */
USB_returnCode_t USB_transmitData(uint8_t *data, uint32_t len);

/**
 * @brief       Called each time the USB device state is changed.
 *              Starts CDC operation when device has been configured by USB host
 * @param[in]   oldState : previous USB device state enumerator. Unused parameter.
 * @param[in]   newState : present USB device state enumerator
 * @return      void
 */
void USB_stateChange(USBD_State_TypeDef oldState, USBD_State_TypeDef newState);

/**
 * @brief       Called each time the USB host sends a SETUP command.
 *              Implements CDC class specific commands.
 * @param[in]   setup  : USB Setup request package
 * @return      USB_STATUS_REQ_UNHANDLED :  Setup request not handled.
 * @return      USB_STATUS_OK            : No errors detected
 */
int USB_setupCommand(const USB_Setup_TypeDef *setup);

/**
 * @brief       This API used to map the call back that needed to be called whenever a USB interrupt occurs
 *
 * @param[in]   usbcallback - function pointer of the callback to  be mapped
 *
 * @retVal      USB_FAILURE : API call failed
 * @retVal      USB_SUCCESS : API call succeeded
 *
 * @warning     This API will over-write any previous callback if configured.
 */
USB_returnCode_t USB_callBackMapping(USB_rxCallback usbcallback);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_USB_IH_H_ */

/**@}*/

/** ************************************************************************* */
