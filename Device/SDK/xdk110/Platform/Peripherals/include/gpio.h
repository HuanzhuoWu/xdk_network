/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup GPIO_Driver GPIO
 * @brief GPIO Interface
 * @ingroup Peripheral
 *
 * @{
 * @brief  This module provides interface for GPIO module.
 * 
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef GPIO_XDK_H_
#define GPIO_XDK_H_

/* additional interface header files */
#include "em_gpio.h"

/* public interface declaration ********************************************** */

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */

/* public type and macro definitions */
#define GPIO_INTERRUPT_DISABLE_FLAG     UINT8_C(0x00)       /**< interrupt disable flag used while configuring sensor interrupt */

/**
 * @brief This data type represents a function pointer that is used to register realtime interrupt callback
 */
typedef void (*Gpio_intrCallback)(void);

/** enum values to represent error type options */
typedef enum GPIO_errorTypes_e
{
    GPIO_ERROR_OK, /**< No error - everything is OK */
    GPIO_ERROR_HANDLE_IS_NULL, /**< The handle parameter is an invalid NULL value */
    GPIO_ERROR_HANDLE_NOT_INITIALIZED, /**< The handle parameter is an initialized yet */
    GPIO_ERROR_HANDLE_DATA_CORRUPTED, /**< The handle parameter is an invalid or corrupted handle object */
    GPIO_ERROR_WRONG_DIRECTION, /**< The request pin direction is not supported by the hardware */
    GPIO_ERROR_INVALID_PORT, /**< The request port value is invalid */
    GPIO_ERROR_INVALID_PIN, /**< The request pin number is invalid */
    GPIO_ERROR_RESOURCE_NOT_AVAILABLE, /**< Required resource is not available for this request */
    GPIO_ERROR_EVENT_NOT_SUPPORTED, /**< Event/Interrupt is not supported for this handle object */
    GPIO_ERROR_TIMEOUT, /**< Timeout while waiting for pin state */
    GPIO_ERROR_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} GPIO_errorTypes_t, *GPIO_errorTypes_tp;

/** enum values to represent pin state options */
typedef enum GPIO_pinStates_e
{
    GPIO_STATE_OFF, /**< Pin is at it's low state (zero state) */
    GPIO_STATE_ON, /**< Pin is at it's high state (one state) */
    GPIO_STATE_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} GPIO_pinStates_t, *GPIO_pinStates_tp;

/** enum values to represent pin direction options */
typedef enum GPIO_pinDirections_e
{
    GPIO_DIRECTION_INPUT, /**< Specifies pin direction as input */
    GPIO_DIRECTION_OUTPUT, /**< Specifies pin direction as output */
    GPIO_DIRECTION_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} GPIO_pinDirections_t, *GPIO_pinDirections_tp;

/** structure to hold the GPIO Port and the corresponding pin*/
struct Gpio_S
{
    GPIO_Port_TypeDef port; /* GPIO port to access */
    uint32_t pin; /* pin number in the port */
};
typedef struct Gpio_S Gpio_T;

/** structure to hold the interrupt configurations*/
struct Gpio_PinInterrupt_S
{
    Gpio_T gpio;
    uint8_t risingEdge;
    uint8_t fallingEdge;
    uint8_t enable;
    Gpio_intrCallback callBack;
};
typedef struct Gpio_PinInterrupt_S Gpio_PinInterrupt_T;

/** Defines the handle to the GPIO object this api operates on.
 * The handle needs to be initialized using the ::GPIO_init function.
 */
typedef void* GPIO_handle_tp;

/**
 * @brief Initializes the GPIO referenced by the passed handle. This function needs to be called before
 * calling any other function of the GPIO api with this handle.
 *
 * @param [in] GPIO_handle_tp the handle to the GPIO object that should be initialized
 * @param [in] GPIO_pinDirections_t the required pin direction
 * @param [in] GPIO_pinStates_t the required initial pin statue
 *
 * @retval     GPIO_ERROR_OK                      The handle was successfully initialized
 * @retval     GPIO_ERROR_HANDLE_IS_NULL          The handle parameter is an invalid NULL value 
 * @retval     GPIO_ERROR_HANDLE_DATA_CORRUPTED   The handle parameter is an invalid or corrupted handle object 
 * @retval     GPIO_ERROR_WRONG_DIRECTION         The request pin direction is not supported by the hardware 
 * @retval     GPIO_ERROR_INVALID_PORT            The request port value is invalid 
 * @retval     GPIO_ERROR_INVALID_PIN             The request pin number is invalid 
 * @retval     GPIO_ERROR_ALREADY_USED            Pin is already used 
 */
GPIO_errorTypes_t GPIO_init(GPIO_handle_tp handle, GPIO_pinDirections_t direction, GPIO_pinStates_t defaultState);

/**
 * @brief Deinitialize GPIO object handle.
 *
 * @param [in] handle the handle to the GPIO object to be deinitialized.
 *
 * @retval     GPIO_ERROR_OK                      The handle was successfully deinitialized
 * @retval     GPIO_ERROR_HANDLE_IS_NULL          The handle parameter is an invalid NULL value 
 * @retval     GPIO_ERROR_HANDLE_DATA_CORRUPTED   The handle parameter is an invalid or corrupted handle object 
 */
GPIO_errorTypes_t GPIO_deInit(GPIO_handle_tp handle);

/**
 * @brief Read the current GPIO object handle state.
 *
 * @param [in] handle the handle to the GPIO object
 *
 * @retval GPIO_pinStates_t The current input or output pin state
 */
GPIO_pinStates_t GPIO_getPin(GPIO_handle_tp handle);

/**
 * @brief Set the GPIO object handle state.
 *
 * @param [in] handle the handle to the GPIO object
 * @param [in] state the desired new pin state
 *
 * @retval     GPIO_ERROR_OK                      Pin state was successfully changed
 * @retval     GPIO_ERROR_HANDLE_IS_NULL          The handle parameter is an invalid NULL value 
 * @retval     GPIO_ERROR_HANDLE_DATA_CORRUPTED   The handle parameter is an invalid or corrupted handle object 
 */
GPIO_errorTypes_t GPIO_setPin(GPIO_handle_tp handle, GPIO_pinStates_t state);

/**
 * @brief Toggle a GPIO state.
 *
 * @param [in] handle the handle to the GPIO object
 *
 * @retval     GPIO_ERROR_OK                      Pin state was successfully toggled
 * @retval     GPIO_ERROR_HANDLE_IS_NULL          The handle parameter is an invalid NULL value 
 * @retval     GPIO_ERROR_HANDLE_DATA_CORRUPTED   The handle parameter is an invalid or corrupted handle object 
 */
GPIO_errorTypes_t GPIO_togglePin(GPIO_handle_tp handle);

/**
 * @brief Invert pin state variable
 *
 * @param [in] state the state to invert
 *
 * @retval     GPIO_pinStates_t inverted state of the input parameter
 */
GPIO_pinStates_t GPIO_Invert(GPIO_pinStates_t state);

#endif /* GPIO_XDK_H_ */
