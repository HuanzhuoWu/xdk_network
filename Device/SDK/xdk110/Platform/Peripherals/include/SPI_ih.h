/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup SPI SPI
 * @ingroup Peripheral
 *
 * @{
 * @brief  SPI driver have the functionalities to have serial communication
 *  APIs to have the SPI communication
 *
 *   - SPI_init()      - To initialize the SPI module with user specific characteristics
 *   - SPI_write()     - To send the data to the slave device
 *   - SPI_read()      - To receive the data from the slave device
 *   - SPI_writeRead() - To Write some data and read some data with the same API
 *   - SPI_deinit()    - To terminate the spi module
 *
 */

/* header definition ******************************************************** */
#ifndef BCDS_SPI_IH_H_
#define BCDS_SPI_IH_H_

/* public interface declaration ********************************************* */

#include <stdint.h>
#include "RB_ringBuffer_ih.h"

/* public type and macro definitions */

/**
 *  @brief Error code returned by APIs of SPI driver
 */
typedef enum SPI_return_e
{
	SPI_FAILURE,
    SPI_SUCCESS,
    SPI_INVALID_PARAMETER,
    SPI_SERIAL_INIT_FAILED,
    SPI_SERIAL_READ_FAILED,
    SPI_SERIAL_WRITE_FAILED,
	SPI_ERROR_END
} SPI_return_t;

/**
 *  @brief Select SPI hardware number type
 */
typedef enum SPI_portNumber_e
{
    SPI0, /**< SPI port 0 */
    SPI1, /**< SPI port 1 */
    SPI2, /**< SPI port 2 */
} SPI_portNumber_t;

/**
 *  @brief  SPI clock modes available for data capture and transfer
 */
typedef enum SPI_clockMode_e
{
    SPI_CLOCK_MODE_CPOL0_CPHA0, /**< data are captured on the clock's
									rising edge (low to high transition)*/
    SPI_CLOCK_MODE_CPOL0_CPHA1, /**< data are captured on the clock's falling edge
									and data is propagated on a rising edge*/
    SPI_CLOCK_MODE_CPOL1_CPHA0, /**< data are captured on clock's falling edge
									and data is propagated on a rising edge*/
    SPI_CLOCK_MODE_CPOL1_CPHA1, /**< data are captured on clock's rising edge
									and data is propagated on a falling edge*/
} SPI_clockMode_t;

/**
 *  @brief Enumeration for the serial ports available in the hardware.
 *  Could be a list of USARTn, LEUARTn or UARTn depending on the controller
 *  This is a common structure used for USART, LEUART or UART.
 *  So this structure contains all the serial ports. For SPI module,
 *  The  SPI_USART0, SPI_USART1 and SPI_USART2 only have to be used based
 *  on the number of USART ports.
 */
typedef enum SPI_serialPort_e
{
#ifdef LEUART_PRESENT
	SPI_LEUART0,
#if (LEUART_COUNT > 1)
	SPI_LEUART1,
#endif
#if (LEUART_COUNT > 2)
	SPI_LEUART2,
#endif
#endif

#ifdef USART_PRESENT
	SPI_USART0,
#if (USART_COUNT > 1)
	SPI_USART1,
#endif
#if (USART_COUNT > 2)
	SPI_USART2,
#endif
#if (USART_COUNT > 3)
	SPI_USART3,
#endif
#endif

#ifdef UART_PRESENT
	SPI_UART0,
#if (UART_COUNT > 1)
	SPI_UART1,
#endif
#if (UART_COUNT > 2)
    SPI_UART2,
#endif
#if (UART_COUNT > 3)
    SPI_UART3,
#endif
#endif

    SPI_PORT_MAX
} SPI_serialPort_t;

/**
 *  @brief
 *      Type of SERIAL hardware. Can be an LEUART, USART or UART
 */
typedef enum SPI_hwType_e
{
	SPI_HWTYPE_LEUART,
	SPI_HWTYPE_USART,
	SPI_HWTYPE_UART,
	SPI_HWTYPE_MAX
} SPI_hwType_t;

/**
 *  @brief
 *      Route location for EFM32 peripherals
 */
typedef enum SPI_routeLocation_e
{
	SPI_ROUTE_LOCATION0,
	SPI_ROUTE_LOCATION1,
	SPI_ROUTE_LOCATION2,
	SPI_ROUTE_LOCATION3,
	SPI_ROUTE_LOCATION_MAX
} SPI_routeLocation_t;

/**
 *  @brief
 *      The write API has to be indicated with a flag about
 *      whether the write is to do only write operation or
 *      it also have to read the reply from other end.
 */
typedef enum SPI_writeFlag_e
{
	SPI_WRITE_READ,
	SPI_WRITE_ONLY
} SPI_writeFlag_t;

/** Define the prototype to which serial transmission callback functions must conform.
 *
 * Functions implementing serial transmission callbacks do not need to return, therefore their return type
 * is void none.
 *
 * Example task function prototype which conforms to the type declaration:
 * @code void EXL_exampleTxCallbackFunction(void); @endcode
 */
typedef void (*SPI_txCallback)(void);

/** Define the prototype to which serial reception callback functions must conform.
 *
 * Functions implementing serial reception callbacks do not need to return, therefore their return type
 * is void none. Two input parameters of received byte and buffer unread count are passed with each call.
 *
 * Example task function prototype which conforms to the type declaration:
 * @code void EXL_exampleRxCallbackFunction(uint8_t byte, uint16_t count); @endcode
 */
typedef void (*SPI_rxCallback)(uint8_t, uint16_t);

/**
 *  @brief
 *      Describes an instance of a serial device handle. This descriptor is used by all SPI_xxx APIs
 *
 *  @note
 *      All the fields in this structure will be initialized by the SPI_init
 *      API using the values provided in the SPI_initParams_t structure and
 *      do not have to be set manually. Moreover, this structure should be maintained in a
 *      static location and should be initialized only with the SPI_init API.
 *
 */
typedef struct SPI_device_s
{
    SPI_hwType_t hwType; 		/* Type of UART hardware. That is SPI_HWTYPE_SPI */

    SPI_serialPort_t hwDevicePort;/* The Energy Micro peripheral to be used. That is USART */

    RingBuffer_t txBufDescr; 	/* Descriptor for the transmit ring buffer */
    RingBuffer_t rxBufDescr; 	/* Descriptor for the receive ring buffer */

    /* Callback function which is called when we
     * don't want wait in SPI_write to complete.
     * @warning This call back is a place holder for future implementation.
     * This is not used in current implementation. Pass it as NULL. */
    SPI_txCallback txCallback;

    /* Callback function which is called when we
     * don't want wait in SPI_write to complete.
     * @warning This call back is a place holder for future implementation.
     * This is not used in current implementation. Pass it as NULL. */
    SPI_rxCallback rxCallback;

    uint8_t hasHwFlowControl; /* Specifies whether flow control has to be used. @TODO currently not implemented*/
} SPI_device_t;

/**
 *  @brief
 *      Describes an instance of a SPI driver initialize parameters.
 *  @note
 *      All the fields in this structure will be initialized by the SPI_init API using the values provided in the
 *      SPI_initParams_t structure and do not have to be set manually. Moreover, this structure should be maintained in a
 *      static location and should be initialized only with the SPI_init API
 */
typedef struct SPI_initParams_s
{
	SPI_portNumber_t    portNumber;
	SPI_clockMode_t     clockMode;
	SPI_routeLocation_t routeLocation;  /* One of the locations in the chip's pin set to which the
									       peripheral signals can be sent */
	uint8_t             *txBuf_p;       /* Pointer to the buffer that is to be used as the serial device's
	                                       circular buffer that holds bytes that are to be transmitted. 
						                   This buffer must be maintained by the application or the kernel 
						                   and must exist until the end of the program */
    uint8_t             *rxBuf_p;       /* Pointer to the circular buffer that holds the bytes received 
						                   by the driver. This buffer must be maintained by the kernel 
						                   or the application and must exist until the end of the program */
    uint32_t            txBufSize;      /* Capacity in bytes of the transmit buffer */
    uint32_t            rxBufSize;      /* Size of the receive circular buffer in bytes */
	uint32_t            baudrate;       /* Baud rate to communicate */
} SPI_initParams_t;
 
/* public function prototype declarations */
/****************************************************************************/
/**
 *  @brief       Initialize SPI
 *               Initializes SPI interface. User must be fill the SPI_initParams_t structure.
 *               The function automatically detects the frequency of the current HF
 *               peripheral clock and hence, the HF peripheral clock must be properly set up.
 *               Based on the Port number, the internal call back function and
 *               other parameters will be registered. The SPI_device_t will given
 *		         as output parameter handle. After the initialization, the SPI_device_t handle
 *		         have to be passed to other APIs.
 *
 * @param [in]   initParams pointer to SPI_initParams_t structure pointing to
 *               configuration parameters for SPI initialization
 * @param [out]  spiHandle Pointer to a SPI_spiHandle_t structure that will
				 serve as a serial device instance. The structure that is initialized
				 by this API need to maintained for future use.
 * @retval       SPI_return_t - return the status of SPI operation
 *
 ****************************************************************************/
SPI_return_t SPI_init(SPI_device_t *spiHandle, const SPI_initParams_t *initParams);

/**
 * @brief        Send the data using SPI.
 * 				 This API will send the data to the device and return back.
 * 				 It will not return before sending the data.
 * @param [in]   spiHandle handle to corresponding SPI
 * @param [in]   transmitBuffer pointer to the data to send
 * @param [in]   transmitBufferSize - size of the data to send
 * @param [in]   writeOnly Write only flag
 * @retval       SPI_return_t - return the status of SPI operation
 * @note         The SPI_device_t handle will be given as output parameter from SPI_init.
 * 				 The same handle have to be used in this API without any change.
 * @note 		 Write only flag is set to 1 to indicate that the Write does
 * 				 not need the value that is received from other end after the write.
 * 				 Write only flag is set to 0 to indicate that the Write needs
 * 				 the value that is received from other end after the write.
 * @warning 	 This is a blocking API. This API will return after the buffer sent to
 * 				 the slave device. Or this API will return the corresponding error value.
 */
SPI_return_t SPI_write(SPI_device_t *spiHandle, const uint8_t *transmitBuffer, uint32_t transmitBufferSize,SPI_writeFlag_t writeOnly);

/**
 * @brief        Read the data using SPI
 * 				 This API writes dummy data to the other end to receive the actual data.
 * @param [in]   spiHandle pointer to SPI_spiHandle_t structure pointing to
 *               the handle of the required SPI.
 * @param [in]   receiveBuffer pointer to the data to receive
 * @param [in]   receiveBufferSize size of the data to receive
 * @retval       SPI_return_t - return the status of SPI operation
 * @note         The SPI_device_t handle will be given as output parameter from SPI_init.
 * 				 The same handle have to be used in this API without any change.
 * @note         This API just reads the required number of bytes from the driver's
 * 				 internal buffer and writes into the user-supplied buffer. No attempt
 * 				 is made to wait until the required number of bytes arrive from the remote device.
 * @warning 	 This is a blocking API. This API will return after the data read from
 * 				 the slave device. Or this API will return the corresponding error value.*
 */
SPI_return_t SPI_read(SPI_device_t *spiHandle, uint8_t *receiveBuffer, uint32_t receiveBufferSize);

/**
 * @brief        API to read both write and read
 * @param [in]   spiHandle pointer to SPI_spiHandle_t structure pointing to
 *               the handle of the required SPI.
 * @param [in]   transmitBuffer pointer to the data to send
 * @param [in]   transmitBufferSize size of the data to send
 * @param [in]   receiveBuffer pointer to the data to receive
 * @param [in]   receiveBufferSize size of the data to receive
 * @retval       SPI_return_t return the status of SPI operation
 * @note		 This API is used to Write some data to SPI and read some data from SPI.
 * @note         The SPI_device_t handle will be given as output parameter from SPI_init.
 * 				 The same handle have to be used in this API without any change.
 */
SPI_return_t SPI_writeRead(SPI_device_t *spiHandle, uint8_t *transmitBuffer, uint32_t transmitBufferSize,
													uint8_t *receiveBuffer, uint32_t receiveBufferSize);

/**
 * @brief        deInitializes SPI driver
 * @param [in]   spiHandle - pointer to SPI_spiHandle_t structure pointing to
 *               the handle of the required SPI
 * @retval       SPI_return_t - return the status of SPI operation
 * @note         The SPI_device_t handle will be given as output parameter from SPI_init.
 * 				 The same handle have to be used in this API without any change.
 */
SPI_return_t SPI_deinit(SPI_device_t *spiHandle);

/**
 * @brief        Send the data using SPI.
 *               This API will send the data to the device and return back.
 *               It will not return before sending the data.
 * @param [in]   spiHandle pointer to SPI_spiHandle_t structure pointing to
 *               the handle of the required SPI.
 * @param [in]   transmitData - byte to the data to send
 * @param [out]   receiveData - byte to the data to receive
 * @retval       SPI_return_t - return the status of SPI operation
 * @note         The SPI_device_t handle will be given as output parameter from SPI_init.
 *               The same handle have to be used in this API without any change.
 */
SPI_return_t SPI_writeReadInOut(SPI_device_t *spiHandle, const uint8_t *transmitData, uint8_t *receiveData);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_SPI_IH_H_ */

/**@}*/

/** ************************************************************************* */
