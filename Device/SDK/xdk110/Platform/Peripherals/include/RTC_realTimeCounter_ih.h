/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_RTC_REALTIMECOUNTER_IH_H_
#define BCDS_RTC_REALTIMECOUNTER_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/** The function like macro implements a read access provider for the RTC module's compare channel 0 register. */
#define RTC_getSystemTimerCompare0Value() \
    (RTC->COMP0)

/** The function like macro implements a read access provider for the RTC module's compare channel 1 register. */
#define RTC_getSystemTimerCompare1Value() \
    (RTC->COMP1)

/** The function like macro implements a read access provider for the RTC module's count register. */
#define RTC_getSystemTimerCountValue() \
    (RTC->CNT)

/** enum that represents the interrupts of the RTC */
typedef enum RTC_interruptType_e
{
    RTC_ENABLE_OVERFLOW_INTERRUPT = (0x1UL << 0), /**< Start counting when init completed. */
    RTC_ENABLE_COMP_0_INTERRUPT = (0x1UL << 1),   /**< Start counting when init completed. */
    RTC_ENABLE_COMP_1_INTERRUPT = (0x1UL << 2),   /**< Start counting when init completed. */
} RTC_interruptType_t;

/** enum that represents enable and disable a feature of the RTC */
typedef enum RTC_configFeature_e
{
    RTC_DISABLE,
    RTC_ENABLE,
} RTC_configFeature_t;

/** RTC interrupt callback function prototype */
typedef void (*RTC_callback)(void);

/** structure that holds the configuration parameters of the RTC interrupt */
typedef struct RTC_configTimerInterrupt_e
{
    uint32_t comp0valueinUs;                    /**< value of the comp0  register in micro seconds */
    uint32_t comp1valueinUs;                    /**< value of the comp1  register in micro seconds */
    uint32_t interruptConfig;                   /**< type of the interrupt that to be enabled */
    RTC_callback overFlowCallbackFunction;      /**< callback function of overflow interrupt */
    RTC_callback comp0CallbackFunction;         /**< callback function of overflow interrupt */
    RTC_callback comp1CallbackFunction;         /**< callback function of overflow interrupt */
} RTC_configTimerInterrupt_t;

/* public function prototype declarations */

/**
 * @brief     function to initialize the RTC
 *
 * @param[in]  RTC_configFeature_t - start counting after initialize
 */
void RTC_initTimer(RTC_configFeature_t startCountAfterInit);

/**
 * @brief      configure the logging levels
 *
 * @param[in]  RTC_configTimerInterrupt_t - interrupt configuration parameters for more details
 *              refer the structure(RTC_configTimerInterrupt_t) documentation
 */
void RTC_configTimer(RTC_configTimerInterrupt_t configParams);

/**
 * @brief      function to disable all the interrupt
 */
void RTC_disableAllInterrupt(void);

/**
 * @brief      function to get the time in milliseconds
 *
 * @retval     uint64_t : time in milliseconds
 *
 */
uint64_t RTC_getTime(void);

/**
 * @brief      function to get the time in milliseconds
 */
void RTC_IRQHandler(void);

/**
 * @brief        function to disable  the interrupt of the RTC
 *
 * @param[in]    RTC_interruptType_t  interrupt that to be disabled
 */

void RTC_disableInterrupt(RTC_interruptType_t interruptName);

/**
 * @brief      function to enable the RTC
 */
void RTC_enable(void);

/**
 * @brief      function to disable RTC
 */
void RTC_disable(void);

/**
 * @brief         function to update the compare channel0 with the current counter value
 *
 * @param[in]     uint32_t : time in micro seconds
 */
void RTC_updateComp0ValueWithCount(uint32_t timeInUs);

/**
 * @brief         function to update the compare channel 1  with the current counter value
 *
 * @param[in]     uint32_t : time in micro seconds
 */
void RTC_updateComp1ValueWithCount(uint32_t timeInUs);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_RTC_REALTIMECOUNTER_IH_H_ */

/** ************************************************************************* */
