/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef BCDS_GPIO_H_
#define BCDS_GPIO_H_

/* public interface declaration */
#include "BCDS_Compiler.h"
#include "BCDS_Peripherals.h"

/* public type and macro definitions */

/**
 * @brief   Enumeration to represent the GPIO ports.
 */
enum Gpio_Port_E
{
    GPIO_PORT_A                     = 0x01UL,
    GPIO_PORT_B                     = 0x02UL,
    GPIO_PORT_C                     = 0x04UL,
    GPIO_PORT_D                     = 0x08UL,
    GPIO_PORT_E                     = 0x10UL,
    GPIO_PORT_F                     = 0x20UL,
    GPIO_PORT_G                     = 0x40UL,
    GPIO_PORT_H                     = 0x80UL,
    GPIO_PORT_I                     = 0x100UL,
    GPIO_PORT_OUT_OF_RANGE          = 0x200UL
};

/**
 * @brief   Enumeration to represent the GPIO pins.
 */
enum Gpio_Pin_E
{
    GPIO_PIN_NO_0                   = 0x400UL,
    GPIO_PIN_NO_1                   = 0x800UL,
    GPIO_PIN_NO_2                   = 0x1000UL,
    GPIO_PIN_NO_3                   = 0x2000UL,
    GPIO_PIN_NO_4                   = 0x4000UL,
    GPIO_PIN_NO_5                   = 0x8000UL,
    GPIO_PIN_NO_6                   = 0x10000UL,
    GPIO_PIN_NO_7                   = 0x20000UL,
    GPIO_PIN_NO_8                   = 0x40000UL,
    GPIO_PIN_NO_9                   = 0x80000UL,
    GPIO_PIN_NO_10                  = 0x100000UL,
    GPIO_PIN_NO_11                  = 0x200000UL,
    GPIO_PIN_NO_12                  = 0x400000UL,
    GPIO_PIN_NO_13                  = 0x800000UL,
    GPIO_PIN_NO_14                  = 0x1000000UL,
    GPIO_PIN_NO_15                  = 0x2000000UL,
    GPIO_PIN_NO_16                  = 0x4000000UL,
    GPIO_PIN_NO_17                  = 0x8000000UL,
    GPIO_PIN_NO_18                  = 0x10000000UL,
    GPIO_PIN_NO_OUT_OF_RANGE        = 0x20000000UL
};

/**
 * @brief   This data type represents a function pointer which would be executed
 *          when a corresponding interrupt is hit.
 *
 * @param [in]  arg : The argument containing relevant information.
 */
typedef void (* const Gpio_InterruptCB_T)(void * const arg);

/**
 * @brief   Structure to represent the GPIO Interrupt reference block.
 */
struct Gpio_PortPin_S {
    uint32_t portPin;
    Gpio_InterruptCB_T callback;
};

/**
 * @brief   Typedef to represent the GPIO Interrupt reference block.
 */
typedef struct Gpio_PortPin_S Gpio_PortPin_T;

/**
 * @brief   Enumeration to represent the GPIO return codes.
 */
enum Gpio_ResultCode_E
{
    GPIO_FAILURE,
    GPIO_SUCCESS,
    GPIO_RESOURCE_BUSY,
    GPIO_INVALID_REQUEST,
    GPIO_INVALID_IRQ
};

/**
 * @brief   Typedef to represent the GPIO return code.
 */
typedef enum Gpio_ResultCode_E Gpio_ResultCode_T;

/**
 * @brief   Typedef to represent the GPIO port.
 */
typedef enum Gpio_Port_E Gpio_Port_T;

/**
 * @brief   Enumeration to represent the GPIO controls.
 */
enum Gpio_Control_E
{
    /**< GPIO Directions */
    GPIO_CONTROL_DIRECTION_INPUT            = 0x01UL,
    GPIO_CONTROL_DIRECTION_OUTPUT           = 0x02UL,

    /**< GPIO Types */
    GPIO_CONTROL_TYPE_DISABLED              = 0x04UL,
    GPIO_CONTROL_TYPE_ANALOG                = 0x08UL,
    GPIO_CONTROL_TYPE_DIGITAL               = 0x10UL,

    /**< GPIO Modes */
    GPIO_CONTROL_MODE_PULL_UP               = 0x20UL,
    GPIO_CONTROL_MODE_PULL_DOWN             = 0x40UL,
    GPIO_CONTROL_MODE_PUSH_PULL             = 0x80UL,
    GPIO_CONTROL_MODE_WIRED_AND             = 0x100UL,
    GPIO_CONTROL_MODE_WIRED_OR              = 0x200UL,

    /**< GPIO Filters */
    GPIO_CONTROL_FILTER_OFF                 = 0x400UL,
    GPIO_CONTROL_FILTER_ON                  = 0x800UL,

    /**< GPIO Interrupts */
    GPIO_CONTROL_INT_RISING_EDGE            = 0x1000UL,
    GPIO_CONTROL_INT_FALLING_EDGE           = 0x2000UL,
    GPIO_CONTROL_INT_LOW_LEVEL              = 0x4000UL,
    GPIO_CONTROL_INT_HIGH_LEVEL             = 0x8000UL,

    /**< GPIO Drive Strengths */
    GPIO_CONTROL_DRV_STRG_VERY_LOW          = 0x10000UL,
    GPIO_CONTROL_DRV_STRG_LOW               = 0x20000UL,
    GPIO_CONTROL_DRV_STRG_MEDIUM            = 0x40000UL,
    GPIO_CONTROL_DRV_STRG_HIGH              = 0x80000UL
};

/**
 * @brief   Typedef to represent the GPIO control.
 */
typedef enum Gpio_Control_E Gpio_Control_T;

/**
 * @brief   This data type represents a function pointer which would be executed
 *          when any event (error/status/...) is to be notified.
 *
 * @param [in]  event : The argument containing relevant event information.
 */
typedef void (*Gpio_EventCB_T) (uint32_t event);

/**
 * @brief   Enumeration to represent the GPIO state.
 */
enum Gpio_PinState_E
{
    GPIO_PIN_STATE_LOW,
    GPIO_PIN_STATE_HIGH,
    GPIO_PIN_STATE_UNDEFINED
};

/**
 * @brief   Typedef to represent the GPIO state.
 */
typedef enum Gpio_PinState_E Gpio_PinState_T;

/**
 * @brief   GPIO callback registration macro.
 *          This will register a callback to the corresponding port and pin.
 *
 * @note    Linker file should define a section for gpio_irq.
 *
 * Example linker definition:
 *
 @code

  gpio_irq :
  {
    Gpio_IrqLookUpTableStartAddress = .;
    KEEP(*(gpio_irq))
    Gpio_IrqLookUpTableEndAddress = .;
  } >FLASH

 @endcode
 *
 * @note : The definition must be in global or file scope.
 *         Intended use case is to put definitions in the beginning of C files.
 *         This macro is a compilation time definition which cannot be
 *         overridden at run-time
 *
 * @param [in]  port        : A Gpio_Port_E member.
 * @param [in]  pin         : A Gpio_Pin_E member.
 * @param [in]  callback    : A callback function pointer of type Gpio_InterruptCB_T.
 */
#define GPIO_REGISTER_IRQ_CALLBACK(port, pin, callback) \
        COMPILER_SECTION(gpio_irq) static volatile const Gpio_PortPin_T Gpio_##port##pin = {(port|pin), callback}

/**
 * @brief   Structure to represent the GPIO Driver functions.
 */
struct Gpio_Driver_S
{
    /**
     * @brief   Pointer to get driver version.
     *
     * @retval  structure denoting the underlying API and Driver version.
     */
    Peripherals_DriverVersion_T (*getVersion) (void);

    /**
     * @brief   Pointer to get driver capabilities.
     *
     * @retval  This would return a OR-ed combination of Gpio_Control_E members.
     *          For example : A return value equivalent to the below line
     *          (GPIO_CONTROL_INT_LOW_LEVEL|GPIO_CONTROL_INT_HIGH_LEVEL)
     *          value would mean that the current GPIO driver would
     *          support level triggered interrupt.
     */
    uint32_t (*getCapabilities) (void);

    /**
     * @brief   Pointer to initialize GPIO Interface.
     *          This would enable the clock configurations.
     *
     * @param [in]  eventCB     : Callback to be called in case of any event notification.
     *
     * @retval  This would return the status of GPIO initialization.
     */
    Gpio_ResultCode_T (*initialize) (Gpio_EventCB_T eventCB);

    /**
     * @brief   Pointer to uninitialize GPIO Interface.
     *
     * @retval  This would return the status of GPIO uninitialization.
     */
    Gpio_ResultCode_T (*uninitialize) (void);

    /**
     * @brief   Pointer to control GPIO Interface power.
     *
     * @param [in]  state       : state of Power that the GPIO peripheral is to be set.
     *
     * @retval  This would return the status of GPIO Interface power control.
     */
    Gpio_ResultCode_T (*powerControl) (Peripherals_PowerState_T state);

    /**
     * @brief   Pointer to write value to GPIO Interface pin.
     *
     * @param [in]  portPin     : An OR-ed combination of a valid Gpio_Port_E and Gpio_Pin_E
     *                            to which data is to be written to.
     *
     * @param [in]  writeValue  : value to be written to the GPIO port pin.
     *
     * @retval  This would return the status of GPIO pin write operation.
     */
    Gpio_ResultCode_T (*write) (uint32_t portPin, Gpio_PinState_T writeValue);

    /**
     * @brief   Pointer to read value from the GPIO Interface pin.
     *
     * @param [in]  portPin     : An OR-ed combination of a valid Gpio_Port_E and Gpio_Pin_E
     *                            from which data is to be read from.
     *
     * @param [in]  readValue   : value read from the GPIO port pin.
     *
     * @retval  This would return the status of GPIO pin read operation.
     */
    Gpio_ResultCode_T (*read) (uint32_t portPin, Gpio_PinState_T* readValue);

    /**
     * @brief   Pointer to toggle a GPIO Interface pin.
     *
     * @param [in]  portPin     : An OR-ed combination of a valid Gpio_Port_E and Gpio_Pin_E
     *                            which is to be toggled.
     *
     * @retval  This would return the status of GPIO pin toggle operation.
     */
    Gpio_ResultCode_T (*toggle) (uint32_t portPin);

    /**
     * @brief   Pointer to control a GPIO Interface.
     *
     * @param [in]  portPin     : An OR-ed combination of a valid Gpio_Port_E and Gpio_Pin_E
     *                            which is to be controlled.
     *
     * @param [in]  config      : An OR-ed combination of a valid Gpio_Control_E combinations.
     *
     * @note : single as well as multiple control can be performed in single call.
     *
     *         For example :
     *
     *         Gpio_Driver.Control \
     *         (GPIO_PORT_B|GPIO_PIN_2, \
     *         GPIO_CONTROL_DIRECTION_INPUT|GPIO_CONTROL_TYPE_DIGITAL|GPIO_CONTROL_MODE_PULLUP);
     *
     *         will configure portB pin2 as input, digital pull-up.
     *         If later filter is to be enabled, the user simply needs to provide the below call.
     *
     *         Gpio_Driver.Control \
     *         (GPIO_PORT_B|GPIO_PIN_2, \
     *         GPIO_CONTROL_FILTER_ON);
     *
     * @retval  This would return the status of GPIO pin toggle operation.
     */
    Gpio_ResultCode_T (*control) (uint32_t portPin, uint32_t config);
};

/**
 * @brief   Typedef to represent the GPIO Driver functions.
 */
typedef struct Gpio_Driver_S const Gpio_Driver_T;

/**
 * @brief   GPIO driver object through which all related member
 * functions can be accessed.
 */
extern Gpio_Driver_T Gpio_Driver;

/* public function prototype declarations */

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_GPIO_H_ */

/** ************************************************************************* */
