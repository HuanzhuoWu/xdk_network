/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup BUTTON BUTTON
 * @brief Button access module
 * @ingroup Peripheral
 *
 * @{
 * @brief  This module provides interface for Button module.
 * 
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BUTTON_H_
#define BUTTON_H_

/* public interface declaration ********************************************** */
#include "gpio.h"

/* System Header ************************************************************* */

/* Interface Header ************************************************************* */

/* public type and macro definitions */

/** enum values to represent error type options */
typedef enum BUTTON_errorTypes_e
{
    BUTTON_ERROR_OK, /**< No error - everything is OK */
    BUTTON_ERROR_INVALID_PARAMETER, /**< One of the input parameters is invalid */
    BUTTON_ERROR_HANDLE_IS_NULL, /**< The handle parameter is an invalid NULL value  */
    BUTTON_ERROR_HANDLE_DATA_CORRUPTED, /**< The handle parameter is an invalid or corrupted handle object */
    BUTTON_ERROR_HANDLE_DEALLOCATED, /**< The handle parameter is not a valid because it was deleted  */
    BUTTON_ERROR_NOT_ENABLED, /**< The button must be enabled before calling to this function */
    BUTTON_ERROR_OUT_OF_RANGE /**< This Specifies maximum number of enum elements and it is used only for error handling */
} BUTTON_errorTypes_t, *BUTTON_errorTypes_tp;

/** Defines the handle to the button object this api operates on.
 * The handle needs to be obtained using the ::BUTTON_create function
 */
typedef void* BUTTON_handle_tp;

/** 
 * @brief Button callback function pointer type.
 *
 * @param handle the handle from which the callback initiated for
 * @param userParameter user define parameter as given in the ::BUTTON_setCallback function
 */
typedef void (*BUTTON_callback_t)(BUTTON_handle_tp handle, uint32_t userParameter);

/**
 * @brief Creates a button object handle according to the provided GPIO pin handle. 
 * This function needs to be called to create a button object handle. The created object handle can be
 * used to manipulate the button state and callbacks using this API.
 *
 * @param [in] pin the handle to the GPIO pin to be used for button object generation.
 * @param [in] pressedState specifies whether button is normally closed or normally open
 *
 * @retval BUTTON_handle_tp the created button object handle.\nFunction will return NULL if an error occurs.
 */
BUTTON_handle_tp BUTTON_create(const GPIO_handle_tp pin, const GPIO_pinStates_t pressedState);

/**
 * @brief Free and release a button handle.
 * This function can be called to release a button handle.
 *
 * @param [in] handle the button handle (acquired by ::BUTTON_create) that needs to be released.
 *
 * @retval     BUTTON_ERROR_OK                    Button handle was deleted successfully
 * @retval     BUTTON_ERROR_HANDLE_IS_NULL        Button handle deletion failed because a NULL handle was given as an argument
 * @retval     BUTTON_ERROR_HANDLE_DATA_CORRUPTED Button handle deletion failed because a corrupted handle was given as an argument
 * @retval     BUTTON_ERROR_HANDLE_DEALLOCATED    Button handle deletion failed because the has already been deleted
 */
BUTTON_errorTypes_t BUTTON_delete(BUTTON_handle_tp handle);

/**
 * @brief Enable the button object
 *
 * @param [in] handle the handle to the button object returned by ::BUTTON_create
 *
 * @retval     BUTTON_ERROR_OK                     Button object was enabled successfully
 * @retval     BUTTON_ERROR_HANDLE_IS_NULL         Failed to enable button object due to invalid handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DATA_CORRUPTED  Failed to enable button object due to corrupted handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DEALLOCATED     Failed to enable button object due to usage of a deallocated handle object
 */
BUTTON_errorTypes_t BUTTON_enable(BUTTON_handle_tp handle);

/**
 * @brief Disable the button object
 *
 * @param [in] handle the handle to the button object returned by ::BUTTON_create
 *
 * @retval     BUTTON_ERROR_OK                     Button object was disabled successfully
 * @retval     BUTTON_ERROR_HANDLE_IS_NULL         Failed to disable button object due to invalid handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DATA_CORRUPTED  Failed to disable button object due to corrupted handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DEALLOCATED     Failed to disable button object due to usage of a deallocated handle object
 */
BUTTON_errorTypes_t BUTTON_disable(BUTTON_handle_tp handle);

/**
 * @brief Check if the button has been pressed.  Button status will be cleared after it is read using this API.
 *
 * @param [in] handle the handle to the button object returned by ::BUTTON_create
 *
 * @retval _Bool The function will return true if the button is pressed.
 */
_Bool BUTTON_isPressed(BUTTON_handle_tp handle);

/**
 * @brief Check if the button has been released.  Button status will be cleared after it is read using this API.
 *
 * @param [in] handle the handle to the button object returned by ::BUTTON_create
 *
 * @retval _Bool The function will return true if the button is released.
 */
_Bool BUTTON_isReleased(BUTTON_handle_tp handle);

/**
 * @brief Register a deferred callback function for a specific GPIO event
 *
 * @param [in] handle the handle to the button object returned by ::BUTTON_create
 * @param [in] callbackFunction pointer to a user define function
 * @param [in] userParameter a user define value to be passed into the callback as parameter 
 *
 * @retval     BUTTON_ERROR_OK                     Button object was disabled successfully
 * @retval     BUTTON_ERROR_HANDLE_IS_NULL         Failed to disable button object due to invalid handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DATA_CORRUPTED  Failed to disable button object due to corrupted handle object argument
 * @retval     BUTTON_ERROR_HANDLE_DEALLOCATED     Failed to disable button object due to usage of a deallocated handle object
 */
BUTTON_errorTypes_t BUTTON_setCallback(BUTTON_handle_tp handle, const BUTTON_callback_t callbackFunction, const uint32_t userParameter);

#endif /* BUTTON_H_ */
