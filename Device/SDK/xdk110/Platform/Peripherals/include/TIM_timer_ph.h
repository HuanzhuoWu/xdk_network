/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_TIM_TIMER_PH_H_
#define BCDS_TIM_TIMER_PH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

/** This is the PWM Maximum Frequency Top Set Value. Must not Exceed 65536 since it is a 16 bit register
 * Here, the Time for Each Tick is calculated as (( 2 ^ ( Prescaler ) ) / ( clock frequency ) ).
 * The Clock used is HFRCO whose Frequency is configured at the time of CMU Initialization */
#define TIM_TIMER_FREQ_TOP_SET_VALUE			(700)


/** This is the Timer Prescaler Factor value whose suffix can be substituted as 0,1,2,4,8,16,32,64,128,256,512 or 1024.
 * Update the same value for TIM_TIMER_CLOCK_PRESCALAR in the Configuration Header of the TIM_timer_cc.c file to
 * have a proper calculation of time for one second breath mode, when ever this value is altered here */
#define TIM_TIMER_PRESCALAR_VALUE				(timerPrescale16)


/** This is the Default Initial Duty Cycle for Each Channel of the TIM_TIMER_TYPE Defined. The Timer PWM
 * mode is configured for inverted output by Default */
#define TIM_TIMER_DEF_DUTYCYCLE_CC 				{0, 0, 0}


/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */

#endif /* BCDS_TIM_TIMER_PH_H_ */
