/*
 *  Copyright (C) Robert Bosch. All Rights Reserved. Confidential.
 *
 *  Distribution only to people who need to know this information in
 *  order to do their job.(Need-to-know principle).
 *  Distribution to persons outside the company, only if these persons
 *  signed a non-disclosure agreement.
 *  Electronic transmission, e.g. via electronic mail, must be made in
 *  encrypted form.
 *
 *****************************************************************************/
/**
 * @file
 * @defgroup Flash_Access Internal Flash
 * @brief Flash Access Driver for internal flash
 * @ingroup Peripheral
 *
 * @{
 * @brief  This module provides interface to access the internal flash.
 *
 * ****************************************************************************/
#ifndef BCDS_FSH_FLASH_IH_H_
#define BCDS_FSH_FLASH_IH_H_

/* Need to include the driver specific header file which should be available
 * with that name for each MCU specific driver. This must be done to get
 * the correct flash page size as a  constant expression else it would not
 * be possible to create e.g. static buffers of the correct size.
 * The defines checked here are usually coming from the makefile variable
 * BCDS_DEVICE_TYPE.
 */
#ifdef BCDS_TARGET_EFM32
#include "em_msc.h"
#define MCU_FLASH_PAGE_SIZE     (FLASH_PAGE_SIZE)
#else
#define MCU_FLASH_PAGE_SIZE     (0)
#error FSH: You have to define the correct device type!
#endif

/**
 * \brief Defines the size of a single flash page in bytes
 * \details This value is set by the particular FSH realization and depends on
 * the flash part that is used by the MCU.
 *
 * \note For EFM32 MCUs the flash page size is defined in the particular
 * device include file of the EMLib and taken from there. The size differs
 * depending on the MCU used e.g. FLASH_PAGE_SIZE is 4KB for EFM32GG and
 * 2KB for EFM32LG.
 */
#define FSH_PAGE_SIZE   MCU_FLASH_PAGE_SIZE

/** Error codes returned by FSH module functions.*/
typedef enum FSH_errorCode_e
{
    FSH_SUCCESS = 0,         /**< Operation successfully executed */
    FSH_WRITEFAIL = 1, /**< Error occurred during write operation */
    FSH_ERASEFAIL = 2, /**< Error occurred during erase operation */
    FSH_NEEDERASE = 3,  /**< Flash needs to be erased before write operation */
    FSH_READFAIL = 4,  /**< Error occurred during read operation */
} FSH_errorCode_t;


/**
 * @brief Gets the page size of the flash device
 * @details Use the function to get the page size of the flash device.
 * This function may be used if a function pointer is needed that is
 * able to return the page size.
 * @return The page size in bytes of the flash device.
 */
uint32_t FSH_GetPageSize(void);

/**
 * \brief Erases a single page of internal flash
 * @details Use the function to erase a single page of internal flash. Erasing
 * a page is necessary before new values can be written to the flash page.
 *
 * Usage:
 * @code
 * #include "FSH_Flash_ih.h"
 * const uint32 MyFlashPageStartAddress = 0x0FE000000
 * if (FSH_SUCCESS == FSH_ErasePage((uint32_t*)MyFlashPageStartAddress))
 * {
 *      // Start writing to flash
 * }
 * @endcode
 * @param pageAddress is a pointer on the start address of the flash page to be
 * erased. Please notice that the address needs to be aligned to flash page
 * size of the particular flash device used. E.g. if the flash page size is
 * 4KB the address needs to be aligned to 4KB boundaries else the call to this
 * function will fail.
 * @retval FSH_SUCCESS upon if erasing of the flash page was successful.
 * @retval FSH_ERASEFAIL if any error has occurred. Possible reasons may
 * be:
 *  - The flash page is locked
 *  - The address is not aligned to flash page size boundaries
 *  - The passed address is not valid flash address
 *  - The command to erase the page has timed out
 *  @note As this function only erases a single page it is more efficient to
 *  use FSH_Erase() if an application has to erase multiple pages of flash
 *  memory.
 *  @see FSH_Erase
 */
FSH_errorCode_t FSH_ErasePage(uint32_t * pageAddress);

/**
 * \brief Erases multiple pages of internal flash
 * @details Use the function to erase multiple pages of internal flash.
 *
 * Usage:
 * @code
 * #include "FSH_Flash_ih.h"
 * const uint32_t MyFlashPageStartAddress = 0x000AF000
 * const uint32_t MyNumPages = 3
 * // Erase 3 pages starting at MyFlashPageStartAddress
 * if (FSH_SUCCESS == FSH_Erase((uint32_t*)MyFlashPageStartAddress,MyNumPages)))
 * {
 *      // Start writing to flash
 * }
 * @endcode
 *
 * @param startAddress is a pointer on the start address of the first flash page
 * to be erased. Please notice that the address needs to be aligned to the flash
 * page size of the particular flash device used. E.g. if the flash page size is
 * 4KB then the address needs to be aligned at 4KB boundaries else the call to
 * this function will fail.
 * @param numPages is the number of pages that should be erased starting from
 * the given start address.
 * @retval FSH_SUCCESS upon if erasing of the flash page was successful.
 * @retval FSH_ERASEFAIL if any error has occurred. Possible reasons may
 * be:
 *  - The flash page is locked
 *  - The startAddress is not aligned to flash page size boundaries
 *  - The passed startAddress is not a valid flash address
 *  - A command to erase a page has timed out
 *  @see FSH_ErasePage
 */
FSH_errorCode_t FSH_Erase(uint32_t* startAddress, uint32_t numPages);

/**
 *  @brief Writes a single 32-bit word at the specified target address.
 *  @details Use the function to write a 32-bit value at the specified address
 *  in flash memory.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint32_T MyValueToWrite = 0xFEEDFACE
 *  if (FSH_SUCCESS == FSH_WriteWord((uint32_t*)MyFlashPageStartAddress,
 *                                   &MyValueToWrite))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *  @param [ in ] targetAddress is the destination address at which the 32-bit
 *  word has to be written. The function checks whether the destination address
 *  is erased before it writes the value. If the destination address is not
 *  erased then it will write nothing and return a failure.
 *
 *  @param [ in ] data is the 32-bit data to write.
 *
 *  @retval FSH_SUCCESS if data was successfully written
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 *  @retval FSH_NEEDERASE if flash address to write on is not blank.
 *                               An erase is required.
 */
FSH_errorCode_t FSH_WriteWord(uint32_t * targetAddress, uint32_t data);

/**
 *  @brief Writes multiple 32-bit words beginning at the specified target address.
 *  @details Use the function to write multiple 32-bit word values into flash
 *  beginning at the specified target address.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint32_t MyValuesToWrite[2] = {0xFEEDFACE, 0xCAFEBABE}
 *  // Write two word values to flash
 *  if (FSH_SUCCESS == FSH_WriteWordStream((uint32_t*)MyFlashPageStartAddress,
 *                                         MyValuesToWrite, 2))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *
 *  @param [ in ] targetAddress is the target address at which writing of the
 *  32-bit values should start. The function DOES NOT check whether the
 *  destination address is erased before it writes the values. However the
 *  targetAddress needs to be aligned at 32-bit boundaries.
 *
 *  @param [ in ] sourceAddress is a pointer on the data to be written.
 *  @param [ in ] numWords is the number of 32-bit values to write into flash.
 *
 *  @retval FSH_SUCCESS if data was successfully written
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 *  @note Please note that the flash region to write must be erased before
 *  this function is executed.
 */
FSH_errorCode_t FSH_WriteWordStream(uint32_t * targetAddress,
                                    uint32_t * sourceAddress,
                                    uint32_t numWords);

/**
 *  @brief Writes a single byte at the specified target address.
 *  @details Use the function to write a single byte value at the specified
 *  address in flash memory.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint8_t MyValueToWrite = 0xAB;
 *  // Write Byte value to flash
 *  if (FSH_SUCCESS == FSH_WriteByte((uint32_t*)(MyFlashPageStartAddress+1),
 *                                               MyValueToWrite))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *
 *  @param [ in ] targetAddress is the destination address at which the byte
 *  has to be written. The function checks whether the destination address
 *  is erased before it writes the value. If the destination address is not
 *  erased then it will write nothing and return a failure. It is not necessary
 *  that the address is aligned at 4 byte boundaries.
 *
 *  @param [ in ] data is 8-bit data to write.
 *
 *  @retval FSH_SUCCESS if data was successfully written
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 *  @retval FSH_NEEDERASE if flash address to write on is not blank.
 *                               An erase is required.
 */
FSH_errorCode_t FSH_WriteByte(uint8_t * targetAddress, uint8_t data);

/**
 *  @brief Writes multiple byte values beginning at the specified target address.
 *  @details Use the function to write byte values into flash beginning at the
 *  specified target address.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint8_t MyValuesToWrite[4] = { 0xFE, 0xED, 0xFA, 0xCE };
 *  // Write 4 Byte values to flash
 *  if (FSH_SUCCESS == FSH_WriteByteStream((uint32_t*)(MyFlashPageStartAddress),
 *                                          MyValuesToWrite,4))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *
 *  @param [ in ] targetAddress is the target address at which writing the
 *  byte values should start. The function DOES NOT check whether the
 *  destination address is erased before it writes the values.
 *  @param [ in ] sourceAddress is a pointer on the data to be written.
 *  @param [ in ] numBytes is the number of bytes to write into flash.
 *
 *  @retval FSH_SUCCESS if data was successfully written
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 *  @note Please note that the flash region to write must be erased before
 *  this function is executed.
 */
FSH_errorCode_t FSH_WriteByteStream( uint8_t* targetAddress,
                                     const uint8_t* sourceAddress,
                                     uint32_t numBytes);

/**
 *  @brief Reads multiple 32-bit words beginning at the specified target address.
 *  @details Use the function to read multiple 32-bit word values from flash
 *  beginning at the specified target address.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint32_t MyReadBuffer[3];
 *  // Read three values from flash
 *  if (FSH_SUCCESS == FSH_ReadWordStream((uint32_t*)(MyFlashPageStartAddress),
 *                                               MyReadBuffer,3))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *
 *  @param [ in ] readFrom is the target address from which reading the
 *  32-bit values should start. The address needs to be aligned at 32-bit
 *  boundaries else the function will fail.
 *
 *  @param [ in ] writeTo is a pointer to which the read data will be copied,
 *  @param [ in ] numWords is the number of 32-bit values to read from flash.
 *
 *  @retval FSH_SUCCESS if data was successfully read.
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 */
FSH_errorCode_t FSH_ReadWordStream( uint32_t* readFrom,
                                    uint32_t* writeTo,
                                    uint32_t numWords);
/**
 *  @brief Reads multiple byte values beginning at the specified target address.
 *  @details Use the function to read multiple byte values from flash
 *  beginning at the specified address.
 *
 *  Usage:
 *  @code
 *  #include "FSH_Flash_ih.h"
 *  const uint32_t MyFlashPageStartAddress = 0x000AF000
 *  const uint8_t MyReadBuffer[20];
 *  // Read 20 byte values from flash
 *  if (FSH_SUCCESS == FSH_ReadByteStream((uint32_t*)(MyFlashPageStartAddress),
 *                                         MyReadBuffer,20))
 *  {
 *       // Do something else
 *  }
 *  @endcode
 *
 *  @param [ in ] readFrom is the target address from which reading the
 *  byte values should start.
 *
 *  @param [ in ] writeTo is a pointer to which the read data will be copied,
 *  @param [ in ] numBytes is the number of byte values to read from flash.
 *
 *  @retval FSH_SUCCESS if data was successfully read.
 *  @retval FSH_WRITEFAIL if writing fails. Possible reasons to fail are:
 *      - The flash is locked.
 *      - The provided targetAddress is not a valid flash address.
 *      - The targetAddress is not aligned to 32 bit boundaries.
 *      - The operation timed out.
 */
FSH_errorCode_t FSH_ReadByteStream( uint8_t* readFrom,
                                    uint8_t* writeTo,
                                    uint32_t numBytes);

#endif /* BCDS_FSH_FLASH_IH_H_ */
/**@}*/ /* End of doxygroup */
