/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
 /**
 * @defgroup UC_TIMER UC TIMER
 * @brief Timer Driver
 * @ingroup Peripheral
 *
 * @{
 * @brief  The interface header exports the following features:
 * 														 - TIM_init()
 *  													 - TIM_enable()
 *  													 - TIM_disable()
 *  													 - TIM_pwmAlterDutyCycle()
 *  													 - TIM_pwmAlterFrequency()
 *
 *  This File has interface support details for
 *  TIMER3 in PWM Functionality and the LETIMER0 runs
 *  based on RTC as a Counter with a maximum duration
 *  of 2 seconds.
 *
 *  If the User intends to add any more timer support or change
 *  the configuration, the TIM_timer_cc.c should be altered.
 *
 *  Timer Location routing for PWM is Internally Provided.
 *  GPIO configuration should be provided and initialized
 *  before utilizing the TIM_init API.
 *
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef BCDS_TIM_TIMER_IH_H_
#define BCDS_TIM_TIMER_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */

/* public function prototype declarations */

/**
 * @brief Enum for different communication channel lines from a single Timer.
 */
typedef enum TIM_port_e
{
    TIM_CC0, /**< Corresponds to the Timer Channel 0 */
    TIM_CC1, /**< Corresponds to the Timer Channel 1 */
    TIM_CC2, /**< Corresponds to the Timer Channel 2 */
    TIM_CC_MAX /**< Defines the Total number of Timer Channels */
} TIM_port_t;

/**
 * @brief Enum for different Timers of the Controller.
 */
typedef enum TIM_name_e
{
    TIM_TIMER3, /**< Corresponds to the TIMER3 */
    TIM_LETIMER0, /**< Corresponds to the LETIMER0 */
    TIM_XTIMER_MAX /**< Defines the Total number of Timers */
} TIM_name_t;

/**
 * @brief Enum for Reset and Set of various applicable Modes and Features.
 */
typedef enum TIM_breath_mode_e
{
    TIM_RESET, /**< Corresponds to Disabling the Mode */
    TIM_SET, /**< Corresponds to Enabling the Mode */
    TIM_MODE_MAX /**< Defines the Boundary of the applicable Modes */
} TIM_mode_t;

/**
 * @brief Callback Function Type Definition for Breath Mode.
 */
typedef void (*TIM_breathCallBack)(void);

/**
 * @brief Structure for different Configurations of the Breath Mode.
 */
typedef struct TIM_breath_config_s
{
    TIM_mode_t timerMode; /**< Corresponds to Enable / Disable of Breath Mode */
    uint32_t breathTimeInSeconds; /**< Corresponds to breath Time in Seconds */
    uint32_t breathCount; /**< Corresponds to Count of Breath to be continued by the IRQ*/

    /** Corresponds to the Callback Function to be requested once the Breath Mode Expires.
     * recursive Call or a Prolonged Delay should be avoided. Ideally making use of Timer
     * Configuration Functions of this same File is recommended */
    TIM_breathCallBack callbackFunction;

} TIM_breathConfig_t;

/****************************************************************************
 * @brief
 *    The TIM_init API will initialize the Timer that the user intends.
 *    Also Enables the Clock for the Timer, configures the Channels,
 *    routes the Hardware Location, sets the required Interrupt type
 *    and the Default initial Parameters
 *
 * @param [in] timerName
 *    Enum defining the required Timer to be initialized
 *
 ****************************************************************************/
extern void TIM_init(TIM_name_t timerName);

/****************************************************************************
 * @brief
 *    The TIM_enable API will enable the Interrupt Flag corresponding to
 *    TIMER that the user intends. Also enables the interrupt vector
 *    in NVIC
 *
 * @param [in] timerName
 *    Enum defining the required Timer to be enabled
 *
 ****************************************************************************/
extern void TIM_enable(TIM_name_t timerName);

/****************************************************************************
 * @brief
 *    The TIM_disable API will disable the Interrupt Flag corresponding to
 *    TIMER that the user intends. Also disables the interrupt vector
 *    in NVIC apart from reseting the TIMER routing
 *
 * @param [in] timerName
 *    Enum defining the required Timer to be disabled
 *
 ****************************************************************************/
extern void TIM_disable(TIM_name_t timerName);

/****************************************************************************
 * @brief
 *    The TIM_pwmAlterDutyCycle API will alter the Duty Cycle of the
 *    TIMER that the user intends
 *
 * @param [in] timerName
 *    Enum defining the required Timer's Duty Cycle to be altered
 *
 * @param [in] timerPortPin
 *    Enum defining the required Timer Channel's Duty Cycle to be altered
 *
 * @param [in] dutyCycleInPercentage
 *    Value of the Duty Cycle of the PWM signal in Percentage.
 *    This should be within the range of 0 - 100.
 *
 ****************************************************************************/
extern void TIM_pwmAlterDutyCycle(TIM_name_t timerName, TIM_port_t timerPortPin, uint8_t dutyCycleInPercentage);

/****************************************************************************
 * @brief
 *    The TIM_pwmAlterFrequency API will alter the Frequency of the
 *    TIMER that the user intends
 *
 * @param [in] timerName
 *    Enum defining the required Timer's Frequency to be altered
 *
 * @param [in] frequency
 *    Value of the Top register of the Timer that controls the Frequency
 *    of the Corresponding Timer which is derived using the
 *    Following Formulae.
 *    		( ( Timer's Input frequency Band Value as Configured) /
 *    		( TIM_TIMER_PRESCALAR_VALUE * frequency ) )
 *    where TIM_TIMER_PRESCALAR_VALUE is defined in the
 *    TIM_timerConfig_ph.h file
 *
 ****************************************************************************/
extern void TIM_pwmAlterFrequency(TIM_name_t timerName, uint32_t frequency);

/****************************************************************************
 * @brief
 *    The TIM_pwmBreathMode API will initiate the Timer's Channel that the
 *    User intends to Breath mode with Configurations as provided.
 *
 * @param [in] timerName
 *    Enum defining the required Timer's Mode to be set for Breath
 *
 * @param [in] timerPortPin
 *    Enum defining the required Timer Channel's Mode to be set for Breath
 *
 * @param [in] breathConfig
 *    Pointer to a Structure for different Configurations of the Breath Mode.
 *    This would consider the Enable / Disable of the Feature,
 *    Breath Time and Count apart from the Callback Function Handle.
 *
 ****************************************************************************/
extern void TIM_pwmBreathMode(TIM_name_t timerName, TIM_port_t timerPortPin, TIM_breathConfig_t *breathConfig);

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_TIM_TIMER_IH_H_ */

/**@}*/

/** ************************************************************************* */
