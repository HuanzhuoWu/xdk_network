/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup SER_serialDriver UART
 * @brief UART Driver
 * @ingroup Peripheral
 *
 * @{
 * @brief  serial interface driver module is a wrapper of UART driver module that is used to transfer/exchange the data
 * 
 * ****************************************************************************/

 /* header definition ******************************************************** */
#ifndef BCDS_SER_SERIALDRIVER_IH_H_
#define BCDS_SER_SERIALDRIVER_IH_H_

/* public interface declaration ********************************************* */

#include "BCDS_Basics.h"
#include "RB_ringBuffer_ih.h"

/* public type and macro definitions */


/** Define the prototype to which serial transmission callback functions must conform.
 *
 * Functions implementing serial transmission callbacks do not need to return, therefore their return type
 * is void none.
 *
 * Example task function prototype which conforms to the type declaration:
 * @code void EXL_exampleTxCallbackFunction(void); @endcode
 */
typedef void (*SER_txCallback)(void);
/** Define the prototype to which serial reception callback functions must conform.
 *
 * Functions implementing serial reception callbacks do not need to return, therefore their return type
 * is void none. Two input parameters of received byte and buffer unread count are passed with each call.
 *
 * Example task function prototype which conforms to the type declaration:
 * @code void EXL_exampleRxCallbackFunction(uint8_t byte, uint16_t count); @endcode
 */
typedef void (*SER_rxCallback)(uint8_t, uint16_t);

/**
 *  @brief
 *      polarity for the pin whether active high or active low
 */
typedef enum
{
    /* Signal is low when asserted */
    SER_activeLow = 0,
    /* Signal is high when asserted */
    SER_activeHigh = 1

} SER_FC_Polarity;
/**
 *  @brief
 *      Route location for EFM32 peripherals
 */
typedef enum SER_routeLocation_e
{
    SER_ROUTE_LOCATION0,
    SER_ROUTE_LOCATION1,
    SER_ROUTE_LOCATION2,
    SER_ROUTE_LOCATION3,
    SER_ROUTE_LOCATION_MAX
} SER_routeLocation_t;

/**
 *  @brief
 *      Type of SERIAL hardware. Can be an LEUART, USART or UART
 */
typedef enum SER_hwType_e
{
    SER_HWTYPE_LEUART,
    SER_HWTYPE_USART,
    SER_HWTYPE_UART,
    SER_HWTYPE_MAX
} SER_hwType_t;

/**
 *  @brief
 *      parity. Can be NONE, ODD or EVEN
 */
typedef enum SER_parity_e
{
    SER_PARITY_NONE,
    SER_PARITY_ODD,
    SER_PARITY_EVEN,
    SER_PARITY_MAX
} SER_parity_t;

/**
 *  @brief
 *      Number of databits in a serial frame. Only 8 and 9 are supported
 */
typedef enum SER_dataBits_e
{
    SER_DATABITS_8,
    SER_DATABITS_9,
    SER_DATABITS_MAX
} SER_dataBits_t;

/**
 *  @brief
 *      Number of stopbits to put after transmitting the data bits. Can be 1 or 2
 */
typedef enum SER_stopBits_e
{
    SER_STOPBITS_ONE,
    SER_STOPBITS_TWO,
    SER_STOPBITS_MAX
} SER_stopBits_t;

/**
 *  @brief
 *      Channel number of the corresponding Hw type.
 */
typedef enum SER_channelNumber_e
{
    SER_CHANNEL_ZERO,
    SER_CHANNEL_ONE,
    SER_CHANNEL_TWO,
    SER_CHANNEL_THREE,
    SER_CHANNEL_MAX
} SER_channelNumber_t;

/**
 *  @brief
 *      The protocol to be used for serail communication
 *
 */
typedef enum SER_serialTransferProtocol_e
{
    SER_SPI_PROTOCOL,
    SER_UART_PROTOCOL
} SER_serialTransferProtocol_t;

/** Enumeration for the serial ports available in the hardware. Could be a list of of USARTn, LEUARTn or UARTn depending on the controller*/
typedef enum SER_serialPort_e
{
#ifdef LEUART_PRESENT
    SER_LEUART0,
#if (LEUART_COUNT > 1)
    SER_LEUART1,
#endif
#if (LEUART_COUNT > 2)
    SER_LEUART2,
#endif
#endif

#ifdef USART_PRESENT
    SER_USART0,
#if (USART_COUNT > 1)
    SER_USART1,
#endif
#if (USART_COUNT > 2)
    SER_USART2,
#endif
#if (USART_COUNT > 3)
    SER_USART3,
#endif
#endif

#ifdef UART_PRESENT
    SER_UART0,
#if (UART_COUNT > 1)
    SER_UART1,
#endif
#if (UART_COUNT > 2)
    SER_UART2,
#endif
#if (UART_COUNT > 3)
    SER_UART3,
#endif
#endif

    SER_PORT_MAX
} SER_serialPort_t;

/**
 *  @brief
 *      Error code returned by APIs of serial driver
 *
 */
typedef enum SER_errorCode_e
{
    SER_SUCCESS,
    SER_ARG,
    SER_RESOURCE,
    SER_LOCK,
    SER_UNREACHABLE,
    SER_ERROR,
    SER_PERMISSION
} SER_errorCode_t;


/**
 *  @brief
 *      The reference frequency for the USART for determining the baudrate
 */
typedef struct SER_referenceFrequency_s
{
#ifdef LEUART_PRESENT
    uint32_t rfLEUART;
#endif
#ifdef USART_PRESENT
    uint32_t rfUSART;
#endif
#ifdef UART_PRESENT
    uint32_t rfUART;
#endif
} SER_referenceFrequency_t;

/**
 *  @brief
 *      The data to initialize an instance of a serial device with
 *
 *  @note
 *      This structure is only used by SER_init API and can be released after the call
 *
 */
typedef struct SER_init_s
{
    SER_hwType_t hwType; /* Hardware type. Can be one of SER_hwType_t */
    SER_serialPort_t hwDevicePort; /* The Energy Micro peripheral to be used. Could be any of USARTn, LEUARTn or UARTn depending on the hwType in question */
    SER_serialTransferProtocol_t protocol; /* The protocol to be used in serial communication */
    uint32_t baudRate; /* Baud rate */
    uint8_t hasHwFlowControl; /* Specifies whether flow control should be used. @TODO currently not implemented */

    uint8_t * txBuf_p; /* Pointer to the buffer that is to be used as the serial device's circular buffer that holds bytes that are to be transmitted. This buffer must be maintained by the application or the kernel and must exist until the end of the program */
    uint32_t txBufSize; /* Capacity in bytes of the transmit buffer */
    uint8_t * rxBuf_p; /* Pointer to the circular buffer that holds the bytes received by the driver. This buffer must be maintained by the kernel or the application and must exist until the end of the program */
    uint32_t rxBufSize; /* Size of the receive circular buffer in bytes */

    SER_parity_t parity; /* parity. Can be any one in SER_parity_t */
    SER_dataBits_t dataBits; /* Number of data bits. Can be any one in SER_dataBits_t */
    SER_stopBits_t stopBits; /* Stop bit count. Can be any value in SER_stopBits_t */

    SER_routeLocation_t routeLocation; /* One of the locations in the chip's pin set to which the peripheral's signals can be sent. Use any value in SER_routeLocation_t */

    /* Callback function which is called when
     * the entire message has been sent.
     * If a callback is not used, this field MUST
     * be set to NULL. @TODO currently not implemented*/
    SER_txCallback txCallback;

    /* Callback function which is called for every byte received.
     * This MUST be defined */
    SER_rxCallback rxCallback;

    /* PTD driver can replace the below mentioned configurations for CTS and RTS @TODO currently not implemented*/
    uint8_t rtsPort;
    uint8_t rtsPin;
    SER_FC_Polarity rtspolarity;
    uint8_t ctsPort;
    uint8_t ctsPin;
    SER_FC_Polarity ctspolarity;

} SER_init_t;

/**
 *  @brief
 *      Describes an instance of a serial device. This descriptor is used by all SER_xxx APIs
 *
 *  @note
 *      All the fields in this structure will be initialized by the SER_init API using the values provided in the
 *      SER_init_t structure and do not have to be set manually. Moreover, this structure should be maintained in a
 *      static location and should be initialized only with the SER_init API
 *
 */
typedef struct SER_device_s
{
    SER_hwType_t hwType; /* Type of UART hardware. Could be any value in SER_hwType_t */

    SER_serialPort_t hwDevicePort; /* The Energy Micro peripheral to be used. Could be one of USARTn, LEUARTn or UARTn depending on the value of hwType */

    RingBuffer_t txBufDescr; /* Descriptor for the transmit ring buffer */
    RingBuffer_t rxBufDescr; /* Descriptor for the receive ring buffer */

    /* Callback function which is called when
     * the entire message has been sent.
     * If a callback is not used, this field MUST
     * be set to NULL. @TODO currently not implemented*/
    SER_txCallback txCallback;

    /* Callback function which is called for every byte received.
     * This MUST be defined */
    SER_rxCallback rxCallback;

    uint8_t hasHwFlowControl; /* Specifies whether flow control has to be used. @TODO currently not implemented*/
} SER_device_t;

/* public function prototype declarations */
/**
 *  @brief
 *      Initializes an instance of serial device driver
 *
 *  @detail
 *      This API fills in the fields of a SER_device_t structure which will be used later by other SER_xxx
 *      APIs such as SER_write. This API takes care of verifying the requested hardware
 *      (LEUART0, UART2 etc.) is present on the chip, initializes the ring buffers and sets up the
 *      required interrupt vectors
 *
 *  @param [ out ] dev
 *      Pointer to a SER_device_t structure that will serve as a serial device instance. The structure that is
 *      initialized by this API need to maintained for future use
 *  @param [ in ] initdata
 *      Pointer to a SER_init_t structure that holds the necessary data with which we can initialize the serial
 *      device. This data structure is used only during initialization and can be discarded afterwards
 *
 *  @retval
 *      SER_ARG         : An invalid argument has been passed. Could be an hardware type, parity value, databit count, stopbit count or routing location
 *  @retval
 *      SER_UNREACHABLE : Fatal; should never happen. Please report in case this is found.
 *  @retval
 *      SER_SUCCESS     : Successfully completed initialization
 *
 */
SER_errorCode_t SER_serialInit(SER_device_t * dev, const SER_init_t * initdata);
/**
 *  @brief
 *      Transmits a byte stream
 *
 *  @param [ in ] dev
 *      Pointer to the serial device descriptor instance
 *  @param [ in ] attr
 *      This pointer is used to provide additional transmission attributes using which any existing setting can be
 *      overriden. Currently not implemented; pass NULL
 *  @param [ in ] data
 *      Pointer to the data to be sent
 *  @param [ in ] len
 *      Number of bytes to send
 *
 *  @retval
 *      SER_RESOURCE    : Not enough emeory in the internal ring buffer to hold the required number of bytes
 *  @retval
 *      SER_SUCCESS     : Succesfully completed
 *
 *  @note
 *      Completion of this write API does not mean the data has been delivered to the remote device; this API merely
 *      copies the supplied data to the internal ring buffer. Actual transmission takes place later, handled by the
 *      TX IRQ handlers of the respective peripheral.
 *
 */
SER_errorCode_t SER_serialWrite(SER_device_t * dev, void * attr, const void * data, uint32_t len);

/**
 *  @brief
 *      Reads a byte stream from the device driver
 *
 *  @param [ in ] dev
 *      Pointer to the serial device descriptor
 *  @param [ in ] attr
 *      Remaining bytes in the read buffer
 *  @param [ out ] data
 *      Pointer to a buffer where the API should store the requested number of bytes
 *  @param [ in ] len
 *      The number of bytes to read
 *
 *  @retval
 *      SER_RESOURCE    : The requested number of bytes are not present in the driver's buffer
 *  @retval
 *      SER_SUCCESS     : Successfully read the requested number of bytes
 *
 *  @note
 *      This API just reads the required number of bytes from the driver's internal buffer and writes into the
 *      user-supplied buffer. No attempt is made to wait until the required number of bytes arrive from the remote
 *      device
 *
 */
SER_errorCode_t SER_serialRead(SER_device_t * dev, uint32_t * attr, void * data, uint32_t len);

/*
 *  @brief
 *      Resets the buffer of the serial driver receive buffer
 *
 *  @param [ in ] serialDev_p
 *      Pointer to the serial device descriptor
 *
 *
 *  @note
 *      This API resets the buffer of the receive buffer when the buffer count has non zero value.
 *      This is achieved by using the ring buffer read function.
 *
 */
void SER_serialResetBuffer(SER_device_t * serialDev_p);

/**
 * @brief
 * API to enable directly a particular UARTn/USARTn/LEUARTn device
 *
 * @param [in] hwDevicePort
 *     Enumeration for the serial ports available in the hardware.
 *
 * @param [in] hwType hardware type that needs to be enabled.
 *
 * @retval   None.
 *
 */
void SER_serialEnable(SER_serialPort_t hwDevicePort, SER_hwType_t hwType);
/**
 * @brief
 * API to disable directly a particular UARTn/USARTn/LEUARTn device
 *
 * @param [in] hwDevicePort
 *     Enumeration for the serial ports available in the hardware.
 *
 * @param [in] hwType hardware type that needs to be disabled.
 *
 * @retval   None.
 *
 */
void SER_serialDisable(SER_serialPort_t hwDevicePort, SER_hwType_t hwType);
/* public global variable declarations */
extern SER_referenceFrequency_t SER_refFreq;

/* inline function definitions */

/** @brief API to set reference frequency for LEUART assumed when configuring baudrate setup. To be called before using serial init.
 *
 * @note
 * LEUART reference clock assumed when configuring baudrate setup. Set
 * it to 0 if currently configured reference clock shall be used.
 *
 *
 * @param[in] LEUART reference clock
 * @retval
 *      SER_RESOURCE    : The requested operation was not successful since hardware was not found
 * @retval
 *      SER_SUCCESS     : successfully set reference frequency
 */
static inline SER_errorCode_t SER_setRefFreqLEUART(uint32_t refFreq)
{
    SER_errorCode_t retVal = SER_RESOURCE;
#ifdef LEUART_PRESENT
    SER_refFreq.rfLEUART = refFreq;
    retVal = SER_SUCCESS;
#else
    refFreq = refFreq;
#endif
    return retVal;
}
/** @brief API to set reference frequency for LEUART assumed when configuring baudrate setup. To be called before using serial init.
 *
 * @note
 * LEUART reference clock assumed when configuring baudrate setup. Set
 * it to 0 if currently configured reference clock shall be used.
 *
 *
 * @param[in] LEUART reference clockto be set
 *
 * @retval
 *      SER_RESOURCE    : The requested operation was not successful since hardware was not found
 * @retval
 *      SER_SUCCESS     : successfully set reference frequency
 */
static inline SER_errorCode_t SER_setRefFreqUSART(uint32_t refFreq)
{
    SER_errorCode_t retVal = SER_RESOURCE;
#ifdef USART_PRESENT
    SER_refFreq.rfUSART = refFreq;
    retVal = SER_SUCCESS;
#else
    refFreq = refFreq;
#endif
    return retVal;
}
/** @brief API to set reference frequency for UART assumed when configuring baudrate setup. To be called before using serial init.
 *
 * @note
 * LEUART reference clock assumed when configuring baudrate setup. Set
 * it to 0 if currently configured reference clock shall be used.
 *
 *
 * @param[in] refFreq UART reference clock to be set
 * @retval
 *      SER_RESOURCE    : The requested operation was not successful since hardware was not found
 * @retval
 *      SER_SUCCESS     : successfully set reference frequency
 */
static inline SER_errorCode_t SER_setRefFreqUART(uint32_t refFreq)
{
    SER_errorCode_t retVal = SER_RESOURCE;

    #ifdef UART_PRESENT
    SER_refFreq.rfUART = refFreq;
    retVal = SER_SUCCESS;
    #else
    BCDS_UNUSED(refFreq);
    #endif

    return retVal;
}

/** @brief API to used to read data from ringbuffer.
 *
 *
 * @retval  Reads the data from the ringbuffer & returns that value
 */
uint8_t SER_readDataFromUSART0Buffer(void);

/** @brief API to used to read data from ringbuffer.
 *
 *
 * @retval  Reads the data from the ringbuffer & returns that value
 */
uint8_t SER_readDataFromUSART1Buffer(void);

/** @brief API to used to read data from ringbuffer.
 *
 *
 * @retval  Reads the data from the ringbuffer & returns that value
 */
uint8_t SER_readDataFromUSART2Buffer(void);

/**@}*/
#endif /* BCDS_SER_SERIALDRIVER_IH_H_ */

/**@}*/

/** ************************************************************************* */
