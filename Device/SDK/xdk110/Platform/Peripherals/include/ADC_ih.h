/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @defgroup ADC_Driver ADC
 * @brief Interface for the Analog-to-Digital Converter
 *
 * @{
 * @brief  This module provides interface for accessing the ADC.
 * 
 * ****************************************************************************/

/* header definition ******************************************************** */
#ifndef ADC_DRIVER_IH_H_
#define ADC_DRIVER_IH_H_

/* additional interface header files */
#include "em_cmu.h"
#include "em_adc.h"

/* public interface declaration ********************************************* */

/* public type and macro definitions */

typedef struct ADC_node_s
{
	ADC_TypeDef * registerBaseAddress;
	CMU_Clock_TypeDef cmuClk;
	uint32_t adcFreq;
	ADC_Init_TypeDef init;
} ADC_node_t, *ADC_node_tp;

typedef struct ADC_singleAcq_s
{
	ADC_TypeDef * registerBaseAddress;
	ADC_InitSingle_TypeDef initSingle;
	ADC_SingleInput_TypeDef adcChannel;
	uint32_t data;
} ADC_singleAcq_t, *ADC_singleAcq_tp;

#define ADC_SINGLE_ACQ_DEFAULT		\
{									\
	ADC0,							\
	ADC_INITSINGLE_DEFAULT,			\
	adcSingleInpVrefDiv2,			\
	0UL								\
}

typedef struct ADC_scanAcq_s
{
	ADC_TypeDef * registerBaseAddress;
	ADC_InitScan_TypeDef initScan;
} ADC_scanAcq_t, *ADC_scanAcq_tp;

/* public function prototype declarations */

/**
 * @brief ADC initialization.
 *
 * @param[in] adcNode ADC node details
 *
 */
extern void ADC_init(ADC_node_tp adcNode);

/**
 * @brief Configures single ADC mode and aquires data in polling mode.
 *
 * @param[in] adcSingleAcq ADC single Acquisition details
 *
 */
extern void ADC_pollSingleData(ADC_singleAcq_tp adcSingleAcq);

/**
 * @brief Scales the ADC values.
 *
 * @param[in] adcSingleAcq ADC single Acquisition details
 *
 * @retVal uint32_t value after scaling
 */
extern uint32_t ADC_scaleAdcValue(ADC_singleAcq_tp adcSingleAcq);

/* public global variable declarations */

/* inline function definitions */

#endif /* ADC_DRIVER_IH_H_ */

/** ************************************************************************* */
/**@}*/