/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup  SID Serial Interface Driver (I2C / SPI)
 * @ingroup  hal
 * @{
 * @brief Serial Interface Driver describes the usage of SPI/I2C interface
 \tableofcontents
 \section intro_sec serial interface driver
 serial interface driver module is a wrapper of i2c driver module that is used to transfer/exchange the data using i2c bus
 */
/* header definition ******************************************************** */
#ifndef SID_SERIALDRIVERINF_IH_H_
#define SID_SERIALDRIVERINF_IH_H_
#include <stdint.h>
/* public interface declaration ********************************************* */
/* public type and macro definitions */

typedef void* SID_queueHandle_tp;
/**Type by which serial data transfer mode is referenced*/
typedef enum SID_sampleMode_e
{
	SID_POLL_MODE, /* channel: 0*/
	SID_INTERRUPT_MODE, /* channel: 1*/
	SID_MODE_MAX, /* channel: End of Channel*/
} SID_sampleMode_t;

/**Type by which data transmission status is referenced*/
typedef enum SID_txStatus_e
{
	SID_TXFAIL, /*data transmission failed */
	SID_TXSUCCESS, /* data transmitted successfully*/

} SID_txStatus_t;

/**
 * Type by which serial data transfer mode is referenced.
 *
 * structure is intialized in SID_init API and processed by APIs of I2C module based on the sample mode
 *
 * */
typedef struct SID_settings_s
{
	SID_queueHandle_tp queueHandle;
	void* tempBuffer;
	SID_sampleMode_t sampleMode;
	void (*decodeCallbackFunction)(const void* buffer, void *updateValue);
} SID_settings_t;

/*Macro used to represent the I2C channel number*/
#define SID_SERIAL_MACRO_I2C_ALL    SID_SERIAL_MACRO_I2C(I2C_CHANNEL_0 ),\
                                SID_SERIAL_MACRO_I2C(I2C_CHANNEL_1 )
/*macro used to represent the SPI channel number*/
#define SID_SERIAL_MACRO_SPI_ALL    SID_SERIAL_MACRO_SPI(SPI_CHANNEL_0 ),\
                                SID_SERIAL_MACRO_SPI(SPI_CHANNEL_1 )

/* Please don't change below code. Some of things are automatically generated using above defined macro. */

/**
 * The type by which the channel type(SPI/I2C) are referenced
 * */
typedef enum SID_channelType_e
{
	SID_I2C_CHANNEL_TYPE,
	SID_SPI_CHANNEL_TYPE,
	SID_SERIAL_CHANNELTYPE_MAX,
} SID_channelType_t;


/*Macro for concatenating I2C/SPI channel with the "SID_SERIAL_"*/
#define SID_SERIAL_MACRO_I2C(M) SID_SERIAL_##M
#define SID_SERIAL_MACRO_SPI(M) SID_SERIAL_##M

typedef enum SID_channel_e
{
	SID_SERIAL_MACRO_I2C_ALL, SID_SERIAL_MACRO_SPI_ALL, SID_SERIAL_CHANNEL_MAX,
} SID_channel_t;
#undef SID_SERIAL_MACRO_I2C
#undef SID_SERIAL_MACRO_SPI
/*Macro for I2C channel*/
#define SID_SERIAL_MACRO_I2C(M) M
typedef enum I2C_channel_e
{
	SID_SERIAL_MACRO_I2C_ALL, SID_I2C_CHANNEL_MAX
} I2C_channel_t;
extern I2C_channel_t I2C_channel_gde;
#undef SID_SERIAL_MACRO_I2C
/*Macro for SPI channel*/
#define SID_SERIAL_MACRO_SPI(M) M
typedef enum SPI_channel_e
{
	SID_SERIAL_MACRO_SPI_ALL, SID_SPI_CHANNEL_MAX
} SPI_channel_t;
#undef SID_SERIAL_MACRO_SPI

#ifdef _INC_SID_MODULE
/*Macro for I2C channel*/
#define SID_SERIAL_MACRO_I2C(M) SID_I2C_CHANNEL_TYPE
/*Macro for SPI channel*/
#define SID_SERIAL_MACRO_SPI(M) SID_SPI_CHANNEL_TYPE
const SID_channelType_t SERIAL_channelType[SID_SERIAL_CHANNEL_MAX]=
{
	SID_SERIAL_MACRO_I2C_ALL,
	SID_SERIAL_MACRO_SPI_ALL
};
#undef SID_SERIAL_MACRO_I2C
#undef SID_SERIAL_MACRO_SPI
/*Macro for I2C channel*/
#define SID_SERIAL_MACRO_I2C(M) (uint8_t) M
/*Macro for SPI channel*/
#define SID_SERIAL_MACRO_SPI(M) (uint8_t) M
const uint8_t SERIAL_channelToLocalChannelConversion[SID_SERIAL_CHANNEL_MAX]=
{
	SID_SERIAL_MACRO_I2C_ALL,
	SID_SERIAL_MACRO_SPI_ALL
};
#undef SID_SERIAL_MACRO_I2C
#undef SID_SERIAL_MACRO_SPI

#define SID_SERIAL_MACRO_I2C(M) SID_SERIAL_##M
const SID_channel_t SERIAL_i2cChannel2SidChannelConv[SID_I2C_CHANNEL_MAX]=
{
	SID_SERIAL_MACRO_I2C_ALL
};
#undef SID_SERIAL_MACRO_I2C

extern uint8_t I2C_triggerTransfer(SID_channel_t channel,const void*bufferData);
extern uint8_t SPI_triggerTransfer(SID_channel_t channel,const void*bufferData);
/**
 * Array of function pointer which points to i2c trigger(function defined in I2C_cc.c file that transfers the data using i2c communication),SPI_triggerTransfer(function that transfers the data using SPI communication)
 *
 *Pointed api's are called in SID_send/ISR function to transfer data using I2C or SPI communication on preffered channel
 *
 *  */
uint8_t (*SID_triggerTransfer_p[SID_SERIAL_CHANNELTYPE_MAX])(SID_channel_t channel,const void*bufferData)=
{
	&I2C_triggerTransfer,
	// &SPI_triggerTransfer
};

#else
extern const SID_channelType_t SERIAL_channelType[SID_SERIAL_CHANNEL_MAX];
extern uint8_t (*SID_triggerTransfer_p[SID_SERIAL_CHANNELTYPE_MAX])(SID_channel_t channel, const void*bufferData);
extern const uint8_t SERIAL_channelToLocalChannelConversion[SID_SERIAL_CHANNEL_MAX];
extern const SID_channel_t SERIAL_i2cChannel2SidChannelConv[SID_I2C_CHANNEL_MAX];

#endif
/**
 * Mode of operation(R or W or RW) using I2C communictaion are referenced by SID_i2cTrnsfrType_t enum
 * */
typedef enum SID_i2cTrnsfrType_e
{
	I2C_READ, /**< I2C READ */
	I2C_WRITE, /**< I2C WRITE  */
	I2C_WRITE_READ, /**< I2C WRITE READ */
	I2C_TRANSFER_TYPE_MAX, /**< I2C Transfer Max Type */
} SID_i2cTrnsfrType_t;

/* I2C instruction */
typedef struct SID_i2cTrnsfrInfo_s SID_i2cTrnsfrInfo_t;

/**
 * Structure to store device information and data that uses i2c bus
 *
 * After I2C communication intialized,the device address where the data to be write/read ,the data,length of the data..etc are to be configured to transfer the data,
 * this can be achieved by passing the arguments to SID_send/SID_sendISR API which calls I2C_triggerTransfer in which this structure variable "SID_i2cTrnsfrInfo_t"(global) is  configured and used to transfer the data / read the data(I2C_efm32Driver module)
 *
 * */
struct SID_i2cTrnsfrInfo_s
{
	SID_i2cTrnsfrType_t trasferType; /**< Type of instruction ( READ, WRITE or READ/WRITE)  */
	I2C_channel_t i2cChannel; /**< Channel of I2C communication  */
	uint8_t i2cAddress; /**< Address of I2C comm.  */
	uint8_t noOfSendData; /**< Length of data to be written.(in bytes)  */
	uint8_t noOfTempData; /**< Length of data to be written.(in bytes)  */
	uint8_t readLength; /**< Length of data to be read.(in bytes)  */
	void (*callback)(SID_i2cTrnsfrInfo_t* transferInfo); /**< callback function to indicate trasfer/communication is complete  */
	void (*tempCallbackBuffer)(SID_i2cTrnsfrInfo_t* transferInfo); /**< callback function to indicate trasfer/communication is complete  */
	uint8_t *sendData; /**< Data pointer which hold the value to be written.  */
	uint8_t *tempData; /**< Data pointer which hold the value to be written.  */
	uint8_t *readData; /**< Data pointer which hold the value to be written.  */
}__attribute__((packed));
typedef void (*SID_callbackFunc)(const void*, void *);
/**
 * Type by which pheripherals can be referenced to intialize the i2c communication
 *
 * For using i2c or spi communication,basic initialization of the communication is done by intiating this structure variable and passing this variable
 * as argument to the function "SID_init"
 *
 * */
typedef struct SID_initSettings_s
{
	SID_channel_t channel;/*channel type*/
	unsigned char noOfqueueItem;
	unsigned char eachQueueItemSize;
	SID_callbackFunc callbackFunc;
	uint32_t location;
} SID_initSettings_t;

/* public function prototype declarations */

/**
 * @brief       The SID_init API initializes I2C
 *
 * @param [in]  The Pointer to the function that implements the task.
 *
 */
void SID_init(SID_initSettings_t initSetting);

/**
 * @brief 	   The SID_send API Send data over SERIAL INTERFACE driver.
 *
 * @param [in] The channel on which data to be transmitted.
 *
 * @param [in] The data that should be transimitted.
 *
 * @retval 	   SID_TXSUCCESS  data transmitted succesfully.
 * 			   SID_TXFAIL     data not transmitted.
 *
 */
SID_txStatus_t SID_send(SID_channel_t channel, const void*bufferData);

/**
 * @brief 		The SID_sendISR API Send data over SERIAL INTERFACE driver from the ISR routine..
 *
 * @param [in]  The channel on which data to be transmitted.
 *
 * @param [in]  The data that should be transimitted.
 *
 * @retval 		SID_TXSUCCESS  data transmitted succesfully
 * 				SID_TXFAIL     data not transmitted.
 *
 */
SID_txStatus_t SID_sendISR(SID_channel_t channel, const void*bufferData);

/* public global variable declarations */

/* inline function definitions */

/**@}*/
#endif /* SID_SERIALDRIVERINF_IH_H_ */

/** ************************************************************************* */
