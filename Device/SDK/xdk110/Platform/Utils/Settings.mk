#This file has the path information of the other libraries, tools and platform.
#This file needs to be included in the makefile for creating static library.

# Package Information
# Package name is used for generating the library. lib<PackageName>_<BCDS_DEVICE_TYPE>.a
BCDS_PACKAGE_NAME = Utils
BCDS_PACKAGE_HOME = $(CURDIR)
BCDS_PACKAGE_ID = 2

BCDS_DEVICE_TYPE ?= EFM32GG
BCDS_DEVICE_ID ?= EFM32GG390F1024
BCDS_DEVICE_PACKAGE_TYPE ?= STM32L476xx

# Internal Path Settings
BCDS_UTILS_SOURCE_DIR := source
BCDS_UTILS_INCLUDE_DIR := include
BCDS_UTILS_PROTECTED_DIR := source/protected
BCDS_UTILS_OBJECT_DIR := object
BCDS_UTILS_LINT_DIR := lint

#Path of Platform, Libraries and Tools
BCDS_TOOLS_DIR ?= $(BCDS_PACKAGE_HOME)/../../Tools
BCDS_LIBRARIES_DIR = $(BCDS_PACKAGE_HOME)/../../Libraries
BCDS_PLATFORM_DIR = $(BCDS_PACKAGE_HOME)/../../Platform

#Tool chain definition
BCDS_TOOL_CHAIN_PATH ?= $(BCDS_TOOLS_DIR)/gcc-arm-none-eabi/bin
BCDS_LINT_CONFIG_FILE_PATH := $(BCDS_TOOLS_DIR)/pclint/config
BCDS_LINT_CONFIG_FILE ?= $(BCDS_TOOLS_DIR)/pclint/config/bcds.lnt
LINT_EXE = $(BCDS_TOOL_DIR)/pclint/exe/lint-nt.launch
BCDS_LINT_PATH ?= $(BCDS_TOOLS_DIR)/PCLint/exe

#External Library Package Dependencies
BCDS_EMLIBS_PATH ?= $(BCDS_LIBRARIES_DIR)/EMlib/EMLib
BCDS_FREE_RTOS_PATH ?= $(BCDS_LIBRARIES_DIR)/FreeRTOS/FreeRTOS

#External Platform Package Dependencies
BCDS_MISCDRIVERS_PATH = $(BCDS_PLATFORM_DIR)/MiscDrivers
BCDS_PHERIPHERAL_PATH = $(BCDS_PLATFORM_DIR)/Peripherals
BCDS_BASICS_PATH ?= $(BCDS_PLATFORM_DIR)/Basics

#Path info for taking the configuration files
BCDS_CONFIG_PATH ?= $(BCDS_PLATFORM_DIR)/RefConfig
BCDS_PACKAGE_CONFIG_PATH = $(BCDS_CONFIG_PATH)/$(BCDS_PACKAGE_NAME)

BCDS_UTILS_DEBUG_PATH ?= $(BCDS_PACKAGE_HOME)/debug
BCDS_UTILS_RELEASE_PATH ?= $(BCDS_PACKAGE_HOME)/release

BCDS_UTILS_DEBUG_OBJECT_PATH = $(BCDS_UTILS_DEBUG_PATH)/$(BCDS_UTILS_OBJECT_DIR)
BCDS_UTILS_RELEASE_OBJECT_PATH = $(BCDS_UTILS_RELEASE_PATH)/$(BCDS_UTILS_OBJECT_DIR)

#This flag is used to generate dependency files 
DEPEDENCY_FLAGS = -MMD -MP -MF $(@:.o=.d)

#Lint Path
BCDS_UTILS_DEBUG_LINT_PATH = $(BCDS_UTILS_DEBUG_PATH)/$(BCDS_UTILS_LINT_DIR)
	
# Archive File Settings
BCDS_UTILS_LIB_BASE_NAME = lib$(BCDS_PACKAGE_NAME)
BCDS_UTILS_DEBUG_LIB_NAME = $(BCDS_UTILS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM)_debug.a
BCDS_UTILS_RELEASE_LIB_NAME = $(BCDS_UTILS_LIB_BASE_NAME)_$(BCDS_TARGET_PLATFORM).a

BCDS_UTILS_DEBUG_LIB   = $(BCDS_UTILS_DEBUG_PATH)/$(BCDS_UTILS_DEBUG_LIB_NAME)
BCDS_UTILS_RELEASE_LIB = $(BCDS_UTILS_RELEASE_PATH)/$(BCDS_UTILS_RELEASE_LIB_NAME)

# Define the path for the include directories.
BCDS_EXTERNAL_INCLUDES = \
	-I $(BCDS_EMLIBS_PATH)/CMSIS/Include \
	-I $(BCDS_EMLIBS_PATH)/emlib/inc \
	-I $(BCDS_EMLIBS_PATH)/Device/SiliconLabs/$(BCDS_DEVICE_TYPE)/Include \
	-I $(BCDS_CONFIG_PATH) \
	-I $(BCDS_CONFIG_PATH)/MiscDrivers \
	-I $(BCDS_CONFIG_PATH)/$(BCDS_HW_VERSION) \
	-I $(BCDS_FREE_RTOS_PATH)/source/include \
	-I $(BCDS_FREE_RTOS_PATH)/source/portable/ARM_CM3 \
	-I $(BCDS_PHERIPHERAL_PATH)/include \
	-I $(BCDS_EMLIBS_PATH)/usb/inc \
	-I $(BCDS_BASICS_PATH)/include

BCDS_UTILS_INCLUDES = \
	-I $(BCDS_UTILS_INCLUDE_DIR) \
	-I $(BCDS_PACKAGE_CONFIG_PATH) \
    -I $(BCDS_MISCDRIVERS_PATH)/include

# @todo 2016-06-24, mta5cob: Once all the projects migrate to BCDS_assert.h,
# the -DDBG_ASSERT_FILENAME=\"$*.c\" shall be removed.
# This variable should fully specify the debug build configuration 
BCDS_DEBUG_FEATURES_CONFIG = \
	 -DDBG_ASSERT_FILENAME=\"$*.c\" -DASSERT_FILENAME=\"$*.c\"