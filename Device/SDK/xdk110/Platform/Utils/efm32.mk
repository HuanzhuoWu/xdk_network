##################################################################
#      Makefile for generating the static library(.a files)      #
##################################################################

include Settings.mk

# Build chain settings
ifneq ("$(wildcard $(BCDS_TOOL_CHAIN_PATH))","")
CC = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-gcc
AR = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-ar
SIZE = $(BCDS_TOOL_CHAIN_PATH)/arm-none-eabi-size
else
CC = arm-none-eabi-gcc
AR = arm-none-eabi-ar
SIZE = arm-none-eabi-size
endif
LINT =  lint-nt.launch

# Compiler settings
#The following CFLAG settings will be taken when this module is not compiled from a main project.
#If this make is invoked from the main project, then the CFLAG settings will be taken over from that main project.
ifndef BCDS_CFLAGS_COMMON
BCDS_CFLAGS_COMMON = -std=c99 -Wall -Wextra -Wstrict-prototypes\
-mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections -D$(BCDS_DEVICE_ID) -D$(BCDS_TARGET_PLATFORM) -DBCDS_TARGET_EFM32
endif

ifndef BCDS_CFLAGS_DEBUG_COMMON
BCDS_CFLAGS_DEBUG_COMMON = -O0 -g $(BCDS_DEBUG_FEATURES_CONFIG)
endif

ifndef BCDS_CFLAGS_RELEASE_COMMON
BCDS_CFLAGS_RELEASE_COMMON = -Os -DNDEBUG
endif

#If there is any additional CFLAG that is required to compile this module, it can be added under the following variable.
CFLAGS_COMMON  = $(BCDS_CFLAGS_COMMON) $(DEPEDENCY_FLAGS) -D BCDS_PACKAGE_ID=$(BCDS_PACKAGE_ID) -DBCDS_TARGET_EFM32
CFLAGS_DEBUG = $(CFLAGS_COMMON) $(BCDS_CFLAGS_DEBUG_COMMON)
CFLAGS_RELEASE = $(CFLAGS_COMMON) $(BCDS_CFLAGS_RELEASE_COMMON)

ARFLAGS = -cr

LINTFLAGS = \
           -immediatetrans \
           -usereleased \
           -declundef \
	   		-zero

# Source and object files
BCDS_UTILS_SOURCE_FILES = \
	$(BCDS_UTILS_SOURCE_DIR)/DBG_assert/DBG_assert_cc.c \
	$(BCDS_UTILS_SOURCE_DIR)/DBG_assert/DBG_faultHandler_cc.c \
	$(BCDS_UTILS_SOURCE_DIR)/LOG/LOG_module_cc.c \
	$(BCDS_UTILS_SOURCE_DIR)/Ringbuffer/RB_ringBuffer.c \
	$(BCDS_UTILS_SOURCE_DIR)/RSM_retargetStdioMessage/retargetio.c \
	$(BCDS_UTILS_SOURCE_DIR)/TLV_dataHandler/TLV_dataHandler_cc.c \
    $(BCDS_UTILS_SOURCE_DIR)/LB2C_lb2cProtocol/LB2C_lb2cProtocol_cc.c 
	
BCDS_UTILS_OBJECT_FILES = \
	$(patsubst $(BCDS_UTILS_SOURCE_DIR)/%.c, %.o, $(BCDS_UTILS_SOURCE_FILES))

BCDS_UTILS_LINT_FILES = \
	$(patsubst $(BCDS_UTILS_SOURCE_DIR)/%.c, %.lob, $(BCDS_UTILS_SOURCE_FILES))

BCDS_UTILS_DEBUG_OBJECT_FILES = \
	$(addprefix $(BCDS_UTILS_DEBUG_OBJECT_PATH)/,$(BCDS_UTILS_OBJECT_FILES))

BCDS_UTILS_DEBUG_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_UTILS_DEBUG_OBJECT_PATH)/, $(BCDS_UTILS_OBJECT_FILES:.o=.d))
	
BCDS_UTILS_RELEASE_OBJECT_FILES = \
	$(addprefix $(BCDS_UTILS_RELEASE_OBJECT_PATH)/,$(BCDS_UTILS_OBJECT_FILES))

BCDS_UTILS_RELEASE_DEPENDENCY_FILES = \
	$(addprefix $(BCDS_UTILS_RELEASE_OBJECT_PATH)/, $(BCDS_UTILS_OBJECT_FILES:.o=.d))
	
BCDS_UTILS_DEBUG_LINT_FILES = \
	$(addprefix $(BCDS_UTILS_DEBUG_LINT_PATH)/, $(BCDS_UTILS_LINT_FILES))

######################## Build Targets #######################################
.PHONY: debug
debug: $(BCDS_UTILS_DEBUG_LIB)

.PHONY: release	
release: $(BCDS_UTILS_RELEASE_LIB)

############# Generate UTILS Library for BCDS ###################
$(BCDS_UTILS_DEBUG_LIB): $(BCDS_UTILS_DEBUG_OBJECT_FILES)  
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build UTILS for EFM32 (debug)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_UTILS_DEBUG_OBJECT_FILES)
	@echo "Library created: $@"
	@echo "========================================"

$(BCDS_UTILS_RELEASE_LIB): $(BCDS_UTILS_RELEASE_OBJECT_FILES)
	@echo
	@echo "========================================"
	@mkdir -p $(@D)
	@echo "Build UTILS for EFM32 (release)"
	@$(AR) $(ARFLAGS) $@ $(BCDS_UTILS_RELEASE_OBJECT_FILES) 
	@echo "Library created: $@"
	@echo "========================================"
	
# compile C files
$(BCDS_UTILS_DEBUG_OBJECT_PATH)/%.o: $(BCDS_UTILS_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_DEBUG) \
	       -I . $(BCDS_UTILS_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) $< -o $@


$(BCDS_UTILS_RELEASE_OBJECT_PATH)/%.o: $(BCDS_UTILS_SOURCE_DIR)/%.c
	@echo "Build $<"
	@mkdir -p $(@D)
	@$(CC) -c $(CFLAGS_RELEASE) \
           -I . $(BCDS_UTILS_INCLUDES) $(BCDS_EXTERNAL_INCLUDES) $< -o $@

.PHONY: lint		   
lint: $(BCDS_UTILS_DEBUG_LINT_FILES)
	@echo "Lint End"


$(BCDS_UTILS_DEBUG_LINT_PATH)/%.lob: $(BCDS_UTILS_SOURCE_DIR)/%.c
	@echo "===================================="
	@mkdir -p $(@D)
	@echo $@
	@$(LINT) +d$(DEVICE)=1 $(LINTFLAGS) $(BCDS_LINT_CONFIG_FILE) $< -oo >> $@
	@echo "===================================="

-include $(BCDS_UTILS_DEBUG_DEPENDENCY_FILES) $(BCDS_UTILS_RELEASE_DEPENDENCY_FILES)

.PHONY: clean	
clean:
	@echo "Cleaning project"
	@rm -rf $(BCDS_UTILS_DEBUG_PATH) $(BCDS_UTILS_RELEASE_PATH)

.PHONY: diagnosis	
diagnosis:
	@echo "Target Platform: "$(BCDS_TARGET_PLATFORM)
	@echo "Objects"
	@echo $(BCDS_UTILS_OBJECT_FILES)
	@echo ""
	@echo "Debug Objects"
	@echo $(BCDS_UTILS_DEBUG_PATH)
	@echo $(BCDS_UTILS_DEBUG_OBJECT_FILES)
