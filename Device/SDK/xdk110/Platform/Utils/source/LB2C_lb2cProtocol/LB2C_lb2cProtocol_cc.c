/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes */

/* system header files */
#include <stdbool.h>
#include <string.h>

/* additional interface header files */
#include "DBG_assert_ih.h"

/* own header files */
#include "LB2C_lb2cProtocol_ih.h"
#include "LB2C_lb2cProtocol_ch.h"

/**
 * @addtogroup LB2C
 *
 * @{
 */

/* plausibility checks */
DBG_STATIC_ASSERT((sizeof(((LB2C_bufferHandle_tp)NULL)->bufferSize) == 2UL),
    "The buffer size member shall be able to hold more than a value of 256.");

DBG_STATIC_ASSERT((LB2C_RESULT_SUCCESS == 0) && (LB2C_RESULT_FAILURE == 1),
    "The implementation relies on the fixed values for LB2C_RESULT_SUCCESS and LB2C_RESULT_FAILURE.");

/* constant definitions */

#define LB2C_INTERNAL_BUFFER_SIZE 200

/* local variables */

/* global variables */

/* inline functions */

/* local functions */

static bool isCommandTypeValid(LB2C_frameCommandType_t type)
{
    /* If the type is 0, the command is invalid */
    if(0U == type)
    {
        return (false);
    }

    bool resultCode;

    /*
     *  the list of valid commands and the evaluated command are put into strings and strstr()
     *  searches for the evaluated command in the list. If found, result is non-NULL.
     */
    uint8_t* commandList = (uint8_t*) "\x43" "\x52" "\x57" "\x79" "\x7A" "\x7B" "\x7C" "\x9A" "\x9E" "\xA3" "\xE6";
    uint8_t command[2];

    command[0] = (uint8_t) type;
    command[1] = '\0';

    if (strstr((const char*) commandList, (const char*) command))
    {
        resultCode = true;
    }
    else
    {
        resultCode = false;
    }

    return (resultCode);
}

static LB2C_resultCode_t evaluateChecksum(LB2C_controlBlock_tp controlBlock)
{
    LB2C_event_t eventType;
    LB2C_receiveEvent_t message;

    if (LB2C_processorGetCalculatedCS(controlBlock) == LB2C_processorGetStoredCS(controlBlock))
    {
        message.errorCode = LB2C_NO_ERROR;
        eventType = LB2C_EVENT_MESSAGE_RX_READY;
    }
    else
    {
        message.errorCode = LB2C_ERROR_INVALID_CHECKSUM;
        eventType = LB2C_EVENT_MESSAGE_RX_ERROR;
    }

    message.messageType = LB2C_processorGetFrameType(controlBlock);
    message.buffer.bufferPointer = LB2C_processorGetBufferPointer(controlBlock);
    message.buffer.bufferSize = LB2C_processorGetBufferPosition(controlBlock) > LB2C_processorGetBufferSize(controlBlock)
                                ? LB2C_processorGetBufferSize(controlBlock) : LB2C_processorGetBufferPosition(controlBlock);

    return (controlBlock->notifyCallback(&message, eventType));
}

static void frameProcessor(LB2C_controlBlock_tp controlBlock)
{
    uint8_t inputBuffer[LB2C_INTERNAL_BUFFER_SIZE];
    uint8_t iterator;
    uint8_t bytesRead;

    for (;;)
    {
        if (0U < (bytesRead = controlBlock->processor.reader(inputBuffer, sizeof(inputBuffer))))
        {
            for (iterator = 0U; iterator < bytesRead; iterator++)
            {
                switch (LB2C_processorGetState(controlBlock))
                {
                case LB2C_FRMPRC_SD:
                    {
                    if (LB2C_FRAME_START_DELIMITER == inputBuffer[iterator])
                    {
                        LB2C_processorSetState(controlBlock, LB2C_FRMPRC_LE);
                    }
                }
                    break;
                case LB2C_FRMPRC_LE:
                    {
                    LB2C_processorSetPayloadLength(controlBlock, inputBuffer[iterator]);
                    LB2C_processorSetChecksum(controlBlock, inputBuffer[iterator]);

                    LB2C_processorSetState(controlBlock, LB2C_FRMPRC_CMD);
                }
                    break;
                case LB2C_FRMPRC_CMD:
                    {
                    if (isCommandTypeValid((LB2C_frameCommandType_t) inputBuffer[iterator]))
                    {
                        LB2C_processorSetFrameType(controlBlock, inputBuffer[iterator]);
                        LB2C_processorAddToChecksum(controlBlock, (uint8_t)LB2C_processorGetFrameType(controlBlock));

                        LB2C_processorSetState(controlBlock, LB2C_FRMPRC_CHECKSUM);
                    }
                    else
                    {
                        /* previous data position stored into LE field may be a valid SD field */
                        if (LB2C_FRAME_START_DELIMITER == LB2C_processorGetPayloadLength(controlBlock))
                        {
                            LB2C_processorSetChecksum(controlBlock, inputBuffer[iterator]);
                            LB2C_processorSetPayloadLength(controlBlock, inputBuffer[iterator]);
                        }
                        else
                        {
                            LB2C_processorSetState(controlBlock, LB2C_FRMPRC_SD);
                        }
                    }
                }
                    break;
                case LB2C_FRMPRC_CHECKSUM:
                    {
                    LB2C_processorSetStoredCS(controlBlock, inputBuffer[iterator]);

                    if (LB2C_processorGetPayloadLength(controlBlock))
                    {
                        LB2C_processorSetBufferPosition(controlBlock, 0U);
                        LB2C_processorSetState(controlBlock, LB2C_FRMPRC_MSG);

                        if (LB2C_processorGetBufferSize(controlBlock) < LB2C_processorGetPayloadLength(controlBlock))
                        {
                            if (LB2C_RESULT_SUCCESS == freeMemory(&LB2C_processorGetBuffer(controlBlock), controlBlock->notifyCallback))
                            {
                                /*
                                 * the return value is discarded as in both cases the function must progress as it would
                                 * be very difficult to resolve the error case via recovering by the user application. So
                                 * instead of recovering the error is propagated and it is only reported to the user when
                                 * the currently parsed frame fails with checksum error (LB2C_EVENT_MESSAGE_RX_ERROR event)
                                 */
                                (void) requestMemory(controlBlock, &LB2C_processorGetBuffer(controlBlock), LB2C_MEM_RECEIVE_BUFFER);
                            }
                            else
                            {
                                controlBlock->lastError = LB2C_ERROR_MEMORY_RELEASE_FAILED;
                            }
                        }
                    }
                    else
                    {
                        (void) evaluateChecksum(controlBlock);
                        LB2C_processorSetState(controlBlock, LB2C_FRMPRC_SD);
                    }
                }
                    break;
                case LB2C_FRMPRC_MSG:
                    {
                    if (LB2C_processorGetBufferPosition(controlBlock) < LB2C_processorGetPayloadLength(controlBlock))
                    {
                        /*
                         * the conditional statement tests whether the buffer is smaller than the payload size
                         * which could be a valid use case when the conditional memory request fails inside
                         * the LB2C_FRMPRC_CHECKSUM frame processor state - the error case is reported by last
                         * error set to LB2C_ERROR_MEMORY_RELEASE_FAILED and by failing the checksum calculation
                         * and reporting to the user LB2C_EVENT_MESSAGE_RX_ERROR via the event notification
                         * callback
                         */
                        if (LB2C_processorGetBufferPointer(controlBlock) && (LB2C_processorGetBufferPosition(controlBlock) < LB2C_processorGetBufferSize(controlBlock)))
                        {
                            LB2C_processorGetBufferPointer(controlBlock)[LB2C_processorGetBufferPosition(controlBlock)] = inputBuffer[iterator];
                            LB2C_processorAddToChecksum(controlBlock, LB2C_processorGetBufferPointer(controlBlock)[LB2C_processorGetBufferPosition(controlBlock)]);
                        }
                        LB2C_processorIncBufferPosition(controlBlock);
                    }

                    if (LB2C_processorGetBufferPosition(controlBlock) == LB2C_processorGetPayloadLength(controlBlock))
                    {
                        (void) evaluateChecksum(controlBlock);
                        LB2C_processorSetState(controlBlock, LB2C_FRMPRC_SD);
                    }
                }
                    break;
                default:
                    DBG_FAIL("Unknown states are not allowed!")
                    break;
                }
            }
        }
        else
        {
            break; /* from for loop */
        }
    }
}

static LB2C_resultCode_t requestMemory(LB2C_controlBlock_tp controlBlock, LB2C_bufferHandle_tp targetBuffer, LB2C_memoryRequestType_t type)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_FAILURE;
    LB2C_memoryRequest_t requestData;

    requestData.buffer.bufferPointer = (uint8_t*) NULL;
    requestData.buffer.bufferSize = targetBuffer->bufferSize;
    requestData.requestType = type;

    if (LB2C_RESULT_SUCCESS == controlBlock->notifyCallback(&requestData, LB2C_EVENT_MEMORY_REQUEST))
    {
        if ((requestData.buffer.bufferPointer)
            && (requestData.buffer.bufferSize >= targetBuffer->bufferSize))
        {
            *targetBuffer = requestData.buffer;

            resultCode = LB2C_RESULT_SUCCESS;
        }
        else
        {
            targetBuffer->bufferPointer = (uint8_t*) NULL;
            targetBuffer->bufferSize = 0U;

            controlBlock->lastError = LB2C_ERROR_INVALID_BUFFER_OR_SIZE_TOO_SMALL;
        }
    }
    else
    {
        controlBlock->lastError = LB2C_ERROR_MEMORY_REQUEST_FAILED;
    }

    return (resultCode);
}

static LB2C_resultCode_t freeMemory(LB2C_bufferHandle_tp memoryBuffer, LB2C_eventDispatcher_tp notifyCallback)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_SUCCESS;

    if (notifyCallback && memoryBuffer && memoryBuffer->bufferPointer)
    {
        LB2C_memoryRequest_t memoryRequest;

        memoryRequest.buffer = *memoryBuffer;
        memoryRequest.requestType = LB2C_MEM_FREE;

        if (LB2C_RESULT_SUCCESS == notifyCallback(&memoryRequest, LB2C_EVENT_MEMORY_REQUEST))
        {
            memoryBuffer->bufferPointer = (uint8_t*) NULL;
            memoryBuffer->bufferSize = 0U;
        }
        else
        {
            resultCode = LB2C_RESULT_FAILURE;
        }
    }

    return (resultCode);
}

/* global functions */

extern LB2C_handle_tp LB2C_init(LB2C_eventDispatcher_tp callback)
{
    LB2C_handle_tp lb2cHandle = (LB2C_handle_tp) NULL;

    if (callback)
    {
        LB2C_memoryRequest_t requestData;

        requestData.buffer.bufferPointer = (uint8_t*) NULL;
        requestData.buffer.bufferSize = sizeof(LB2C_controlBlock_t);
        requestData.requestType = LB2C_MEM_INTERNAL;

        if (LB2C_RESULT_SUCCESS == callback(&requestData, LB2C_EVENT_MEMORY_REQUEST))
        {
            if ((requestData.buffer.bufferPointer)
                && (requestData.buffer.bufferSize == sizeof(LB2C_controlBlock_t)))
            {
                LB2C_controlBlock_t handle;

                memset(&handle, 0, sizeof(LB2C_controlBlock_t));
                handle.notifyCallback = callback;
                handle.lastError = LB2C_NO_ERROR;
                memcpy(requestData.buffer.bufferPointer, &handle, sizeof(LB2C_controlBlock_t));

                lb2cHandle = (LB2C_handle_tp) requestData.buffer.bufferPointer;
            }
        }
    }

    return (lb2cHandle);
}

extern LB2C_handle_tp LB2C_initEx(LB2C_eventDispatcher_tp callback, LB2C_bufferHandle_tp receiveBuffer, LB2C_bufferHandle_tp transmitBuffer)
{
    LB2C_handle_tp handle;

    handle = LB2C_init(callback);

    if (handle)
    {
        do
        {
            if (LB2C_RESULT_FAILURE == LB2C_registerBuffer(handle, receiveBuffer, LB2C_RECEIVE_BUFFER))
            {
                (void) LB2C_deinit(&handle);
                break;
            }

            if (LB2C_RESULT_FAILURE == LB2C_registerBuffer(handle, transmitBuffer, LB2C_TRANSMIT_BUFFER))
            {
                (void) LB2C_deinit(&handle);
                break;
            }
        } while (false);
    }

    return (handle);
}

extern LB2C_resultCode_t LB2C_deinit(LB2C_handle_tp* handlePointer)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_SUCCESS;

    if (handlePointer)
    {
        if (*handlePointer)
        {
            LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) (*handlePointer);
            LB2C_bufferHandle_t internalBuffer;

            /*
             * result code is correctly set only if results code values are properly selected
             * which is guaranteed by a static assertion plausibility test
             */
            resultCode = (LB2C_resultCode_t) (resultCode | freeMemory(&controlBlock->builder.transmitBuffer, controlBlock->notifyCallback));
            resultCode = (LB2C_resultCode_t) (resultCode | freeMemory(&controlBlock->processor.receiveBuffer, controlBlock->notifyCallback));

            /*
             * free own control block - no references are allowed to the control block variable after the
             * execution of the notifyCallback function
             */

            internalBuffer.bufferPointer = (uint8_t*) *handlePointer;

            /*
             * the LB2C_init() fails if internal buffer size is not equal to the control block size,
             * thus it is correct to assume the size of the buffer
             */
            internalBuffer.bufferSize = sizeof(LB2C_controlBlock_t);

            resultCode = (LB2C_resultCode_t) (resultCode | freeMemory(&internalBuffer, controlBlock->notifyCallback));

            /* destroy the instance handle */
            *handlePointer = NULL;
        }
    }
    else
    {
        resultCode = LB2C_RESULT_FAILURE;
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_receiveProcess(LB2C_handle_tp handle)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_FAILURE;

    if (handle)
    {
        LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) (handle);

        if (controlBlock->processor.reader)
        {
            DBG_ASSERT(controlBlock->notifyCallback,
                "The event dispatcher callback must be valid.")

            if (!controlBlock->processor.receiveBuffer.bufferPointer)
            {
                controlBlock->processor.receiveBuffer.bufferSize = LB2C_TYPICAL_READ_BUFFER_SIZE;

                if (LB2C_RESULT_SUCCESS == requestMemory(controlBlock, &controlBlock->processor.receiveBuffer, LB2C_MEM_RECEIVE_BUFFER))
                {
                    frameProcessor(controlBlock);

                    resultCode = LB2C_RESULT_SUCCESS;
                }
                else
                {
                    ; /* last error is set by requestMemory() interface */
                }
            }
            else
            {
                frameProcessor(controlBlock);

                resultCode = LB2C_RESULT_SUCCESS;
            }

        }
        else
        {
            controlBlock->lastError = LB2C_ERROR_INVALID_READER_CALLBACK;
        }
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_transmitProcess(LB2C_handle_tp handle)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_FAILURE;

    if (handle)
    {
        LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) (handle);

        if (controlBlock->builder.writer)
        {
            /** @todo LB2C headers to be added */
            controlBlock->builder.writer(controlBlock->builder.transmitBuffer.bufferPointer, controlBlock->builder.length);
            controlBlock->state = LB2C_STATE_IDLE;
            resultCode = LB2C_RESULT_SUCCESS;
            /* try to send the message via the registered serial interface */
        }
        else
        {
            /* report built message frame via callback interface */
        }
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_transmitFrame(LB2C_handle_tp handle, LB2C_frameCommandType_t type, LB2C_bufferHandle_tp payload)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_FAILURE;
    uint8_t iterator;

    if (handle)
    {
        LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) (handle);

        if (LB2C_STATE_IDLE == controlBlock->state)
        {
            if (isCommandTypeValid(type))
            {
                if ((payload) && (!payload->bufferPointer))
                {
                    controlBlock->lastError = LB2C_ERROR_INVALID_ARGUMENT;
                }
                else
                {
                    if (controlBlock->builder.transmitBuffer.bufferPointer)
                    {
                        /* buffered operation mode */

                        DBG_ASSERT(controlBlock->notifyCallback,
                            "The event dispatcher callback must be valid.")

                        /* invalidate source buffer which is only used in non-buffered mode */
                        controlBlock->builder.sourceBuffer.bufferPointer = NULL;
                        controlBlock->builder.sourceBuffer.bufferSize = 0U;

                        if (controlBlock->builder.transmitBuffer.bufferSize < (payload->bufferSize + LB2C_FRAME_HEADER_SIZE))
                        {
                            if (LB2C_RESULT_SUCCESS == freeMemory(&controlBlock->builder.transmitBuffer, controlBlock->notifyCallback))
                            {
                                controlBlock->builder.transmitBuffer.bufferSize = payload->bufferSize;

                                if (LB2C_RESULT_SUCCESS == requestMemory(controlBlock, &controlBlock->builder.transmitBuffer, LB2C_MEM_TRANSMIT_BUFFER))
                                {
                                    (void) memcpy(controlBlock->builder.transmitBuffer.bufferPointer, payload->bufferPointer, payload->bufferSize);

                                    controlBlock->state = LB2C_STATE_TX_IN_PROGRESS;
                                    resultCode = LB2C_transmitProcess(handle);
                                }
                                else
                                {
                                    controlBlock->lastError = LB2C_ERROR_MEMORY_REQUEST_FAILED;
                                }
                            }
                            else
                            {
                                controlBlock->lastError = LB2C_ERROR_MEMORY_RELEASE_FAILED;
                            }
                        }
                        else
                        {
                            uint8_t * dataPointer = controlBlock->builder.transmitBuffer.bufferPointer;
                            *dataPointer = LB2C_FRAME_START_DELIMITER;
                            dataPointer++;
                            *dataPointer = payload->bufferSize;
                            dataPointer++;
                            *dataPointer = type;
                            dataPointer++;
                            *dataPointer = (payload->bufferSize + type);
                            for(iterator = 0; iterator < payload->bufferSize ; iterator++)
                            {
                                *dataPointer += (payload->bufferPointer[iterator]);
                            }
                            dataPointer++;
                            controlBlock->builder.length = payload->bufferSize + 4;
                            memcpy(dataPointer, payload->bufferPointer,payload->bufferSize );

                            controlBlock->state = LB2C_STATE_TX_IN_PROGRESS;
                            resultCode = LB2C_transmitProcess(handle);
                        }
                    }
                    else
                    {
                        /* non buffered operation mode */

                        /* set transmit buffer size to zero as it is only used in buffered mode */
                        controlBlock->builder.transmitBuffer.bufferSize = 0U;
                        controlBlock->builder.sourceBuffer = *payload;

                        controlBlock->state = LB2C_STATE_TX_IN_PROGRESS;

                        resultCode = LB2C_transmitProcess(handle);
                    }
                }
            }
            else
            {
                controlBlock->lastError = LB2C_ERROR_INVALID_ARGUMENT;
            }
        }
        else
        {
            resultCode = LB2C_RESULT_BUSY;
        }
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_registerBuffer(LB2C_handle_tp handle, LB2C_bufferHandle_tp buffer, LB2C_bufferType_t type)
{
    LB2C_resultCode_t resultCode = LB2C_RESULT_FAILURE;

    if (handle && ((LB2C_TRANSMIT_BUFFER == type) || (LB2C_RECEIVE_BUFFER == type)))
    {
        LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) handle;
        LB2C_bufferHandle_tp targetBuffer;
        LB2C_resultCode_t freeMemoryResultCode = LB2C_RESULT_SUCCESS;

        DBG_ASSERT(controlBlock->notifyCallback,
            "The event dispatcher callback must be valid.")

        if (LB2C_RECEIVE_BUFFER == type)
        {
            targetBuffer = &controlBlock->processor.receiveBuffer;
        }
        else
        {
            targetBuffer = &controlBlock->builder.transmitBuffer;
        }

        if (targetBuffer->bufferPointer)
        {
            freeMemoryResultCode = freeMemory(targetBuffer, controlBlock->notifyCallback);
        }

        if (LB2C_RESULT_SUCCESS == freeMemoryResultCode)
        {
            if (buffer)
            {
                if ((buffer->bufferPointer)
                    && (buffer->bufferSize >= LB2C_FRAME_HEADER_SIZE))
                {
                    *targetBuffer = *buffer;

                    resultCode = LB2C_RESULT_SUCCESS;
                }
                else
                {
                    controlBlock->lastError = LB2C_ERROR_INVALID_BUFFER_OR_SIZE_TOO_SMALL;
                }
            }
            else
            {
                resultCode = requestMemory(controlBlock, targetBuffer, (LB2C_memoryRequestType_t) type);
            }
        }
        else
        {
            controlBlock->lastError = LB2C_ERROR_MEMORY_RELEASE_FAILED;
        }
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_registerReadCallback(LB2C_handle_tp handle, LB2C_readCallback_tp callback)
{
    LB2C_resultCode_t resultCode;

    if (handle)
    {
        ((LB2C_controlBlock_tp) handle)->processor.reader = callback;
        resultCode = LB2C_RESULT_SUCCESS;
    }
    else
    {
        resultCode = LB2C_RESULT_FAILURE;
    }

    return (resultCode);
}

extern LB2C_resultCode_t LB2C_registerWriteCallback(LB2C_handle_tp handle, LB2C_writeCallback_tp callback)
{
    LB2C_resultCode_t resultCode;

    if (handle)
    {
        if (LB2C_STATE_IDLE == ((LB2C_controlBlock_tp) handle)->state)
        {
            ((LB2C_controlBlock_tp) handle)->builder.writer = callback;

            resultCode = LB2C_RESULT_SUCCESS;
        }
        else
        {
            resultCode = LB2C_RESULT_BUSY;
        }
    }
    else
    {
        resultCode = LB2C_RESULT_FAILURE;
    }

    return (resultCode);
}

extern LB2C_errorCode_t LB2C_getLastError(LB2C_handle_tp handle)
{
    LB2C_errorCode_t result;

    if (handle)
    {
        LB2C_controlBlock_tp controlBlock = (LB2C_controlBlock_tp) handle;

        result = controlBlock->lastError;
        controlBlock->lastError = LB2C_NO_ERROR;
    }
    else
    {
        result = LB2C_ERROR_INVALID_HANDLE;
    }

    return (result);
}

extern uint32_t LB2C_getProtocolVersion(void)
{
    return (LB2C_PROTOCOL_VERSION);
}

/** @} *//* LB2C group */
