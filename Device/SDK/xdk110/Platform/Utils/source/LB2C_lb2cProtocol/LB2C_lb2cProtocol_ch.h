/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition */
#ifndef BCDS_LB2C_LB2CPROTOCOL_CH_H_
#define BCDS_LB2C_LB2CPROTOCOL_CH_H_

/* local interface declaration */

/* local type and macro definitions */

#define LB2C_FRAME_HEADER_SIZE 4U
#define LB2C_TYPICAL_READ_BUFFER_SIZE 32U
#define LB2C_FRAME_START_DELIMITER 0x55U

#define LB2C_PROTOCOL_VERSION 1UL

#define LB2C_processorGetState(controlBlock) (controlBlock)->processor.state
#define LB2C_processorSetState(controlBlock, newState) (controlBlock)->processor.state = (newState)

#define LB2C_processorGetPayloadLength(controlBlock) (controlBlock)->processor.length
#define LB2C_processorSetPayloadLength(controlBlock, lengthValue) (controlBlock)->processor.length = (lengthValue)

#define LB2C_processorGetFrameType(controlBlock) (controlBlock)->processor.type
#define LB2C_processorSetFrameType(controlBlock, typeValue) (controlBlock)->processor.type = (LB2C_frameCommandType_t)(typeValue)

#define LB2C_processorGetBuffer(controlBlock) (controlBlock)->processor.receiveBuffer
#define LB2C_processorGetBufferPointer(controlBlock) (controlBlock)->processor.receiveBuffer.bufferPointer
#define LB2C_processorGetBufferSize(controlBlock) (controlBlock)->processor.receiveBuffer.bufferSize
#define LB2C_processorGetBufferPosition(controlBlock) (controlBlock)->processor.iterator
#define LB2C_processorSetBufferPosition(controlBlock, positionValue) (controlBlock)->processor.iterator = (positionValue)
#define LB2C_processorIncBufferPosition(controlBlock) (controlBlock)->processor.iterator ++

#define LB2C_processorSetChecksum(controlBlock, value) (controlBlock)->processor.calculatedChecksum = (value)
#define LB2C_processorAddToChecksum(controlBlock, value) (controlBlock)->processor.calculatedChecksum += (value)
#define LB2C_processorGetCalculatedCS(controlBlock) (controlBlock)->processor.calculatedChecksum

#define LB2C_processorGetStoredCS(controlBlock) (controlBlock)->processor.checksum
#define LB2C_processorSetStoredCS(controlBlock, value) (controlBlock)->processor.checksum = (value)

#define LB2C_builderGetPayloadLength(controlBlock) (controlBlock)->builder.length
#define LB2C_builderSetPayloadLength(controlBlock, lengthValue) (controlBlock)->builder.length = (lengthValue)

/** Type definition for the LB2C frame processor state machine states. */
typedef enum LB2C_frameProcStates_e
{
    LB2C_FRMPRC_SD = 0,
    LB2C_FRMPRC_LE,
    LB2C_FRMPRC_CMD,
    LB2C_FRMPRC_MSG,
    LB2C_FRMPRC_CHECKSUM
} LB2C_frameProcStates_t, *LB2C_frameProcStates_tp;

typedef enum LB2C_stateMachineStates_e
{
    LB2C_STATE_IDLE = 0,
    LB2C_STATE_TX_IN_PROGRESS,
    LB2C_STATE_TX_WAIT_CONFIRMATION
} LB2C_stateMachineStates_t, *LB2C_stateMachineStates_tp;

typedef struct LB2C_frameProcessor_s
{
    LB2C_readCallback_tp reader;
    LB2C_bufferHandle_t receiveBuffer;
    uint8_t length;
    uint8_t iterator;
    uint8_t calculatedChecksum;
    uint8_t checksum;
    LB2C_frameProcStates_t state;
    LB2C_frameCommandType_t type;
} LB2C_frameProcessor_t, *LB2C_frameProcessor_tp;

typedef struct LB2C_frameBuilder_s
{
    LB2C_writeCallback_tp writer;
    LB2C_bufferHandle_t transmitBuffer;
    LB2C_bufferHandle_t sourceBuffer;
    uint8_t length;
    uint8_t iterator;
    uint8_t checksum;
    LB2C_frameCommandType_t type;
} LB2C_frameBuilder_t, *LB2C_frameBuilder_tp;

/** Type definition for the LB2C internal control block. */
typedef struct LB2C_controlBlock_s
{
    LB2C_frameProcessor_t processor;
    LB2C_frameBuilder_t builder;
    LB2C_eventDispatcher_tp notifyCallback;
    LB2C_stateMachineStates_t state;
    LB2C_errorCode_t lastError;
} LB2C_controlBlock_t, *LB2C_controlBlock_tp;

/* local function prototype declarations */

static void frameProcessor(LB2C_controlBlock_tp controlBlock);
static LB2C_resultCode_t requestMemory(LB2C_controlBlock_tp controlBlock, LB2C_bufferHandle_tp targetBuffer, LB2C_memoryRequestType_t type);
static LB2C_resultCode_t freeMemory(LB2C_bufferHandle_tp memoryBuffer, LB2C_eventDispatcher_tp notifyCallback);

/* local module global variable declarations */

/* local in-line function definitions */

#endif /* BCDS_LB2C_LB2CPROTOCOL_CH_H_ */

/** @} *//* LB2C group */
