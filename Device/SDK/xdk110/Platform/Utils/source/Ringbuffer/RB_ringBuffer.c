/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdint.h>

/**RingBuffer compatible for efm32 micro controller and includes the efm32 library files **/
#if (BCDS_TARGET_PLATFORM == efm32)
#include "em_int.h"
#endif

/** RingBuffer compatible for stm32 micro controller **/
#if (BCDS_TARGET_PLATFORM == stm32)
#define INT_Disable()
#define INT_Enable()
#endif

#include "DBG_assert_ih.h"

#include "RB_ringBuffer_ih.h"

/**
 *  @brief
 *      Initializes a ringbuffer data structure for future use
 *  
 *  @param [ in ] ringbuf
 *      Pointer to the ringbuffer descriptor
 *  
 *  @param [ in ] memoryBufferAddress
 *      Pointer to the circular buffer. This buffer must be created and maintained by the 
 *      caller. The reason the buffer is separate from the descriptor is to allow for 
 *      buffers of different sizes while using the same descriptor to manage any buffer
 *  
 *  @param [ in ] size
 *      Size of the circular buffer. Must be known to the caller and will get stored 
 *      inside the descriptor for future use
 *  
 */
void RingBuf_Init(RingBuffer_t * ringBufferHandler, uint8_t * memoryBufferAddress, uint16_t memoryBufferSize)
{
    INT_Disable();

    ringBufferHandler->Base = memoryBufferAddress;
    ringBufferHandler->Rptr = &memoryBufferAddress[0];
    ringBufferHandler->Wptr = &memoryBufferAddress[0];
    ringBufferHandler->Count = UINT16_C(0);
    ringBufferHandler->Size = memoryBufferSize;

    INT_Enable();
}

/**
 *  @brief
 *      Copies specified number of bytes into the circular buffer
 *  
 *  @param [ in ] ringbuf
 *      Pointer to the ringbuffer descriptor
 *  
 *  @param [ in ] src_p
 *      Pointer to the user data that is to be copied into the circular buffer
 *  
 *  @param [ in ] len
 *      Number of bytes to copy
 *  
 *  @return
 *      RB_RESOURCE : If there is not enough space in the buffer to accommodate another
 *                     'len' bytes
 *      RB_SUCCESS  : Successfully copied the supplied byte stream
 *  
 */
RB_status_t RingBuf_Write(RingBuffer_t * ringbuf, uint8_t * src_p, uint16_t len)
{
    if (len > (ringbuf->Size) - (ringbuf->Count))
    {
        DBG_FAIL("If software integration is proper this should never happen.")

        return RB_RESOURCE;
    }

    if (len == UINT16_C(1))
    {
        *ringbuf->Wptr++ = *src_p;

        if (ringbuf->Wptr == ringbuf->Base + ringbuf->Size)
        {
            ringbuf->Wptr = ringbuf->Base;
        }

        ringbuf->Count++;
        return (RB_SUCCESS);
    }

    else
    {

        while (len-- != UINT16_C(0))
        {
            INT_Disable();

            *ringbuf->Wptr++ = *src_p++;

            if (ringbuf->Wptr == ringbuf->Base + ringbuf->Size)
            {
                ringbuf->Wptr = ringbuf->Base;
            }

            ringbuf->Count++;
            INT_Enable();
        }

    }

    return (RB_SUCCESS);
}

/**
 *  @brief
 *      Removes the specifed number of bytes from the circular buffer and stores 
 *      them in the user supplied buffer
 *  
 *  @param [ in ] ringbuf
 *      Pointer to the ringbuffer descriptor
 *  
 *  @param [ out ] dest_p
 *      Pointer to the user supplied buffer into which the data has to be copied. 
 *      Must be large enough to hold the specifie number of bytes
 *  
 *  @param [ in ] len
 *      The number of bytes to copy
 *  
 *  @return
 *      RB_RESOURCE : The specifed number of bytes do not exist in the circular buffer
 *      RB_SUCCESS  : Successful copy
 *  
 */
RB_status_t RingBuf_Read(RingBuffer_t * ringbuf, uint8_t * dest_p, uint16_t len)
{
    if (len > ringbuf->Count)
    {
        DBG_ASSERT(len <= ringbuf->Count, "Bytes to copy is more than the unread Bytes");
        return RB_RESOURCE;
    }

    if (len == UINT16_C(1))
    {

        *dest_p = *ringbuf->Rptr++;

        if (ringbuf->Rptr == ringbuf->Base + ringbuf->Size)
        {
            ringbuf->Rptr = ringbuf->Base;
        }
        ringbuf->Count--;
        return (RB_SUCCESS);

    }

    else
    {

        while (len-- != UINT16_C(0))
        {
            INT_Disable();
            *dest_p++ = *ringbuf->Rptr++;

            if (ringbuf->Rptr == ringbuf->Base + ringbuf->Size)
            {
                ringbuf->Rptr = ringbuf->Base;
            }
            ringbuf->Count--;
            INT_Enable();
        }

    }

    return (RB_SUCCESS);
}

