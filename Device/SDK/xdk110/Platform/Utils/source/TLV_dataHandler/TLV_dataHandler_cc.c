/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* additional interface header files */
#include "DBG_assert_ih.h"

/* own header files */
#include "TLV_dataHandler_ih.h"
#include "TLV_dataHandler_ch.h"

/* static assertion tests *************************************************** */

DBG_STATIC_ASSERT((sizeof(TLV_element_t) % sizeof(uint32_t)) == 0,
    "TLV_element_t structure must be aligned to the machine word size for optimal memory utilization.")

DBG_STATIC_ASSERT(sizeof(TLV_group_t) % sizeof(uint32_t) == 0,
    "TLV_group_t structure must be aligned to the machine word size for optimal memory utilization.")

DBG_STATIC_ASSERT(sizeof(TLV_group_t) % sizeof(TLV_element_t) == 0,
    "TLV_group_t must be divisible by TLV_element_t to make it sure that the alignment rules for the"
    "TLV element table entries are always fulfilled and no unaligned access is attempted.")

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

static inline uint32_t isElementValid(TLV_element_t* element)
{
    return (element->dataBuffer && element->dataLength);
}

/* local functions ********************************************************** */

static uint16_t garbageCollectGroup(TLV_groupHandle_t const handle)
{
    TLV_element_t* element;
    uint16_t elementTableIndex;

    /* first step is to remove all invalid elements from the table */

    /* the element table size should be up to date when garbageCollectGroup()
     * is called as getAvailableMemorySize() calls to getLastElement()
     * within TLV_addElement()
     */

    /* search for invalid elements in table */
    for (elementTableIndex = 0; elementTableIndex < handle->elements; elementTableIndex++)
    {
        element = GET_ELEMENT(handle, elementTableIndex);

        if (!isElementValid(element))
        {
            TLV_element_t* nextValidElement;
            uint16_t nextFreeSlot;

            /* found an invalid element, now search for next valid element */
            nextValidElement = (TLV_element_t*) NULL;

            for (nextFreeSlot = elementTableIndex + 1; nextFreeSlot < handle->elements; nextFreeSlot++)
            {
                if (isElementValid(GET_ELEMENT(handle, elementTableIndex)))
                {
                    nextValidElement = GET_ELEMENT(handle, elementTableIndex);
                    break;
                }
            }

            if (nextValidElement)
            {
                /* move next valid element to first empty slot in table */
                memcpy(element, nextValidElement, sizeof(TLV_element_t));

                /* invalidate old location of element */
                memset(nextValidElement, 0, sizeof(TLV_element_t));
            }
            else
            {
                /* no more valid elements found in table
                 * it is not necessary to update the elements count here
                 * as getLastElement() in getAvailableMemorySize() will
                 * update it
                 */
                break;
            }
        }
    }

    /* second step is to move all data of valid elements to the end of the buffer
     * filling up gaps
     */
    {
        uint8_t* writePointer;

        writePointer = (uint8_t*) ((uint32_t) &handle->buffer[handle->limit]);

        for (elementTableIndex = 0; elementTableIndex < handle->elements; elementTableIndex++)
        {
            element = GET_ELEMENT(handle, elementTableIndex);

            if (isElementValid(element))
            {
                writePointer = (uint8_t*) ((uint32_t) writePointer - (element->dataLength));

                if (writePointer != element->dataBuffer)
                {
                    /* use memmove() to move the data as it allows overlapping source and target addresses */
                    memmove(writePointer, element->dataBuffer, element->dataLength);
                    element->dataBuffer = writePointer;
                }
            }
        }
    }

    return (getAvailableMemorySize(handle));
}

static TLV_element_t* getLastElement(TLV_groupHandle_t const handle)
{
    TLV_element_t* result = (TLV_element_t*) NULL;
    uint16_t elementIndex = handle->elements;

    while (elementIndex--)
    {
        TLV_element_t* element = GET_ELEMENT(handle, elementIndex);
        if (isElementValid(element))
        {
            result = element;
            break;
        }
        else
        {
            /* the last element had been invalidated, decrease elements table size */
            handle->elements--;
        }
    }

    return (result);
}

static uint16_t getAvailableMemorySize(TLV_groupHandle_t const handle)
{
    TLV_element_t* element = getLastElement(handle);
    uint16_t availableMemory;

    if (element != (TLV_element_t*) NULL)
    {
        availableMemory = (uint32_t) element->dataBuffer - ((uint32_t) element + sizeof(TLV_element_t));
    }
    else
    {
        availableMemory = handle->limit;
    }

    return (availableMemory);
}

/* global functions ********************************************************* */

extern TLV_groupHandle_t TLV_addGroup(void* const buffer, const uint16_t size)
{
    /* perform plausibility tests on the input parameters */
    if ((NULL == buffer) ||
        ((uint32_t) buffer % sizeof(TLV_group_t) != 0) ||
        (size <= (sizeof(TLV_group_t))))
    {
        /* return invalid handle to report generic failure */
        return ((TLV_groupHandle_t) NULL);
    }

    {
        TLV_group_t handle;

        /* set up group descriptor */
        handle.buffer = &(((uint8_t*) buffer)[sizeof(TLV_group_t)]);
        handle.limit = size - sizeof(TLV_group_t);
        handle.elements = UINT16_C(0);

        /* reset buffer */
        (void) memset(buffer, 0, size);

        /* copy group descriptor to buffer */
        (void) memcpy(buffer, &handle, sizeof(TLV_group_t));
    }

    /* return newly created group handle */
    return ((TLV_groupHandle_t) buffer);
}

extern void TLV_removeGroup(TLV_groupHandle_t handle)
{
    /* the implementation design does not require resources to be freed up */
    (void) handle;
}

extern TLV_element_t* TLV_addElement(TLV_groupHandle_t const handle, const uint16_t type, const uint16_t length, const void* const value)
{
    /* perform plausibility tests on the input parameters */
    if (((TLV_groupHandle_t) NULL == handle) ||
        (NULL == value))
    {
        /* return invalid handle to report generic failure */
        return ((TLV_element_t*) NULL);
    }

    TLV_element_t* element = TLV_getElement(handle, type);

    /* test whether element already exists in the database */
    if (element)
    {
        /* test whether new data length fits into already reserved space */
        if (element->dataLength >= length)
        {
            /* update element in place */
            element->dataLength = length;
            memcpy(element->dataBuffer, (uint8_t*) value, length);
        }
        else
        {
            /* invalidate old element */
            TLV_removeElement(handle, type);

            /* reset the element variable to NULL to indicate that a new element have to be added */
            element = (TLV_element_t*) NULL;
        }
    }

    /* test whether a new element have to be added */
    if (!element)
    {
        uint16_t availableMemory, requiredMemory;

        /* test available memory space */
        requiredMemory = length + sizeof(TLV_element_t);
        availableMemory = getAvailableMemorySize(handle);

        if (availableMemory < requiredMemory)
        {
            /* perform garbage collection on the database */
            availableMemory = garbageCollectGroup(handle);
        }

        /* test available memory space after garbage collection */
        if (availableMemory < requiredMemory)
        {
            ; /* insufficient memory error - return NULL */
        }
        else
        {
            /* add new element and related data to database */
            element = getLastElement(handle);

            if (element)
            {
                /* element database is not empty */
                TLV_element_t* lastElement;

                lastElement = element;
                element = &element[1];

                element->dataBuffer = (uint8_t*) ((uint32_t) &lastElement->dataBuffer[0] - length);
            }
            else
            {
                /* element database is empty */
                element = (TLV_element_t*) handle->buffer;
                element->dataBuffer = (uint8_t*) ((uint32_t) &handle->buffer[handle->limit] - length);
            }

            element->dataType = type;
            element->dataLength = length;
            memcpy(element->dataBuffer, value, length);

            /* increment number of elements in table */
            handle->elements++;
        }
    }

    return (element);
}

extern TLV_element_t* TLV_getElement(TLV_groupHandle_t const handle, const uint16_t type)
{
    /* perform plausibility tests on the input parameters */
    if ((TLV_groupHandle_t) NULL == handle)
    {
        /* return invalid handle to report generic failure */
        return ((TLV_element_t*) NULL);
    }

    {
        uint16_t elementIndex;
        TLV_element_t* result = (TLV_element_t*) NULL;

        /* search for matching TLV elements in the buffer */
        for (elementIndex = 0; elementIndex < handle->elements; elementIndex++)
        {
            TLV_element_t* element = GET_ELEMENT(handle, elementIndex);

            if (element->dataType == type)
            {
                result = element;
                break;
            }
        }

        return (result);
    }
}

extern void TLV_removeElement(TLV_groupHandle_t const handle, const uint16_t type)
{
    TLV_element_t* element;

    /* check whether requested element exists
     * TLV_getElement() performs plausibility check on the passed handle
     */
    element = TLV_getElement(handle, type);

    if (element)
    {
        memset(element, 0, sizeof(TLV_element_t));
    }
}

/** ************************************************************************* */
