/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* module includes ********************************************************** */

/* system header files */

#include <stdint.h>
#include <stdio.h>

/* additional interface header files */
#include "em_int.h"
#include "DBG_assert_ih.h"
/* own header files */
#include "LOG_module_ih.h"
#include "LOG_module_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */
static LOG_level_t LOG_info_ma[LOG_MAXMODULE] =
    { LOG_ERROR };/**< array used to hold the logging levels with respective to
 the module name as index values based on the configuration
 initialize with "LOG_ERROR" */

/* global functions ********************************************************* */

/* API documentation is in public interface header file 'LOG_module_ih.h' */
LOG_retStatus_t LOG_init(LOG_config_t * logLevelConfigParam,
    uint8_t noOfModules)
{
    LOG_retStatus_t retVal = LOG_INVALID_ARGS;

    if (noOfModules <= LOG_MAXMODULE)
    {
        for (uint8_t index = LOG_INITVALUE; index < noOfModules; index++)
        {
            /*here Module name used as index of the array , each element in the array "LOG_info_ma" will be one of the enum values
             * provided by LOG_level_t type , so we can directly know the configured level of the module by using the index(module name)*/
            if (logLevelConfigParam[index].moduleName < LOG_MAXMODULE && logLevelConfigParam[index].logLevel < LOG_MAX_LEVEL)
            {
                LOG_info_ma[logLevelConfigParam[index].moduleName] =
                    logLevelConfigParam[index].logLevel;
            }
            else
            {
                printf("Please check arguments module name: %d  Level: %d", logLevelConfigParam[index].moduleName, logLevelConfigParam[index].logLevel);
            }
        }
        retVal = LOG_SUCCESS;
    }
    return retVal;
}

/* API documentation is in public interface header file 'LOG_module_ih.h' */
uint8_t LOG_printf(LOG_module_t moduleName, LOG_level_t level, const void * format, ...)
{
    DBG_ASSERT(moduleName < LOG_MAXMODULE, "Module name is not valid");
    DBG_ASSERT(level < LOG_MAX_LEVEL, "Log level passed is not valid");

    uint8_t retval = LOG_FAILURE;
    uint8_t currentLevel = LOG_info_ma[moduleName];

    if ((currentLevel >= level) && (currentLevel != LOG_DISABLE))
    {
        /*here , needs to seperate the actual message arguments from the arguments used for identification of module name and
         * type of message*/
        __VALIST args;/* __VALIST - Type to hold information about variable arguments*/
        __builtin_va_start(args, format);/*starts copying arguments from "format"*/
        /*pass the seperated arguments to vprintf*/
        retval = vprintf((const char *) format, args);/*as VALIST is not supported by printf , vprintf is used  */
        fflush(stdout);/*releases the current contents of the output buffer */
        __builtin_va_end(args);/*End using variable argument list*/
        return retval;
    }
    return retval;
}

/* API documentation is in public interface header file 'LOG_module_ih.h' */
LOG_retStatus_t LOG_disableModuleLog(LOG_module_t moduleName)
{
    LOG_retStatus_t retVal = LOG_INVALID_ARGS;
    if (moduleName < LOG_MAXMODULE)
    {
        LOG_info_ma[moduleName] = LOG_DISABLE;
        retVal = LOG_SUCCESS;
    }
    return retVal;
}

/* API documentation is in public interface header file 'LOG_module_ih.h' */
LOG_retStatus_t LOG_changeModuleLogLevel(LOG_module_t moduleName, LOG_level_t level)
{
    LOG_retStatus_t retVal = LOG_INVALID_ARGS;
    if (moduleName < LOG_MAXMODULE && level < LOG_MAX_LEVEL)
    {
        LOG_info_ma[moduleName] = level;
        retVal = LOG_SUCCESS;

    }
    return retVal;
}

/* API documentation is in public interface header file 'LOG_module_ih.h' */
void LOG_deinit(void)
{
    for (uint8_t index = LOG_INITVALUE; index < LOG_MAXMODULE; index++)
    {
        LOG_info_ma[index] = LOG_DISABLE;
    }
}
