/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef NDEBUG /* valid only for debug builds */

/* module includes ********************************************************** */

/* system header files */
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#if defined(DGB_ASSERT_USE_STD_EXIT)
#include <stdlib.h>
#endif

/* additional interface header files */
#include "em_device.h"
#ifndef DISABLE_DBG_ASSERT_FLASH_USERPAGE
#include "FSH_flash_ih.h"
#endif

/* own header files */
#include "DBG_assert_ih.h"

/* local prototypes ********************************************************* */

/* constant definitions ***************************************************** */
#ifndef DISABLE_DBG_ASSERT_FLASH_USERPAGE
/** Variable which can store a failed assertion's line number. */
__attribute__ ((section(".usrpg"))) static volatile const uint32_t DBG_lastAssertPosition_mdu = { UINT32_C(0xFFFFFFFF) };
__attribute__ ((section(".usrpg"))) static volatile const uint32_t DBG_lastAssertFile_mdu = { UINT32_C(0xFFFFFFFF) };
#endif
/* local variables ********************************************************** */

/* global variables ********************************************************* */

#ifdef DISABLE_DBG_ASSERT_FLASH_USERPAGE
static DBG_assertCallback DBG_assertCallbackFunc = NULL; /*< Variable to store the callback function pointer */
#endif

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

#ifdef DISABLE_DBG_ASSERT_FLASH_USERPAGE
/**
* @param assertCallback Function pointer which will be called when program enters assert
*
* @retval void none - the function never returns
*/
void DBG_assertIndicationMapping(DBG_assertCallback assertCallback)
{
	DBG_assertCallbackFunc = assertCallback;
}
#endif

/** Program assertion helper function. Linking order could be important for
 *  the module. Put the DBG_assert_cc.o object file after all other object files
 *  in the linking order to implicitly contains the assertion data
 *
 *  Note that the file[] argument only contains the file name without path.
 *
 * @param line Line number in unknown file where the assertion error occurred.
 * @param file File name as a NULL delimited C string.
 *
 * @retval void none - the function never returns
 */
void DBG_assertError(const uint32_t line, const uint8_t const file[])
{
#ifndef DISABLE_DBG_ASSERT_FLASH_USERPAGE
    /** Usage of the FSH_erase() interface is forbidden as it would erase all
     *  configuration settings found in the USERPAGE which would be outrageous
     *  in the assertion library!
     *
     *  According to EFM32G reference manual chapter 7.3.6 (Erase and Write Operations),
     *  write operations can be performed multiple times as far as bits which needs to
     *  be changed are in 1 state before the programming operation.
     */
    if ((DBG_lastAssertPosition_mdu == UINT32_C(0xFFFFFFFF)) && (DBG_lastAssertFile_mdu == UINT32_C(0xFFFFFFFF)))
    {
        /* save first three characters of the file prefix */
        uint32_t fileName ;

        ((uint8_t*) &fileName)[0] = file[0];
        ((uint8_t*) &fileName)[1] = file[1];
        ((uint8_t*) &fileName)[2] = file[2];
        ((uint8_t*) &fileName)[3] = '\0'; /* NUL string delimiter */

        FSH_WriteWord((uint32_t*) &DBG_lastAssertFile_mdu, fileName);

        /* save line number */
        FSH_WriteWord((uint32_t*) &DBG_lastAssertPosition_mdu, line);
    }
#else
        if (NULL != DBG_assertCallbackFunc)
        {
            /* Calling the callback function mapped */
        	DBG_assertCallbackFunc(line , file);
        }

#endif
    /* disable all interrupt sources */
    __disable_irq();

#if defined(DGB_ASSERT_USE_STD_EXIT)
    /* exit application */
    exit(EXIT_FAILURE);
#else
    for (;;)
    {
        ; /* block application */
    }
#endif
}

/** The function should serve the watchdog and it blocks program execution
 *  if it finds an assertion log from a previous execution session.
 *
 * @pre DBG_blockIfAsserted() must be called from task level in the
 *      beginning of the application code before initialization finishes.
 *
 * @todo Add watchdog handling.
 *
 * @param void none
 *
 * @retval void none
 */
void DBG_blockIfAsserted(void)
{
#ifndef DISABLE_DBG_ASSERT_FLASH_USERPAGE
    /* test whether there is a saved assertion log */
    if (DBG_lastAssertPosition_mdu != UINT32_C(0xFFFFFFFF))
#endif
    {
        /* disable all interrupt sources */
        __disable_irq();

        for (;;)
        {
            ; /* intended endless loop */
        }
    }
}

#else /* valid only for release builds */

#endif /* ifndef ndebug */

/** ************************************************************************* */
