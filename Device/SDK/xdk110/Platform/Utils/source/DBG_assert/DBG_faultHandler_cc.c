/***************************************************************************//**
 * @section License
 * <b>(C) Copyright 2014 Silicon Laboratories, Inc. http://www.silabs.com</b>
 ******************************************************************************
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software.@n
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.@n
 * 3. This notice may not be removed or altered from any source distribution.
 *
 * DISCLAIMER OF WARRANTY/LIMITATION OF REMEDIES: Silicon Laboratories, Inc.
 * has no obligation to support this Software. Silicon Laboratories, Inc. is
 * providing the Software "AS IS", with no express or implied warranties of any
 * kind, including, but not limited to, any implied warranties of
 * merchantability or fitness for any particular purpose or warranties against
 * infringement of any proprietary rights of a third party.
 *
 * Silicon Laboratories, Inc. will not be liable for any consequential,
 * incidental, or special damages, or any other relief, or for any claim by
 * any third party, arising from your use of this Software.
 *
 *****************************************************************************/
/***************************************************************************//**
 * Modified by Bosch Connected Devices and Solutions GmbH.
 *****************************************************************************/
/******************************************************************************/
/**
 *  @file        DBG_faultHandler_cc.c
 *
 *  The fault handler module can aid software developers to analyze exceptions
 *  thrown by the MCU.
 *
 *  The module defines a dummy ISR for each unused interrupt source. This way
 *  whenever an interrupt source is enabled without defining a proper ISR for
 *  it will block the application in the dummy ISR.
 *
 *  In case of exceptions, the hard fault exception handler will be called
 *  which will save the context into C variables which can be checked in the
 *  debugger after the exception event as the event handler blocks the
 *  application.
 *
 *  @note The SCB structure from CMSIS can be monitored in the debugger to get
 *        additional information about the hard fault failure.
 *        For example, SCB->CFSR can tell what kind of exception occurred.
 *
 *  @note The module is only active in debug builds!
 *
 *  Release builds must be built with the NDEBUG symbol defined in the
 *  makefile.
 *
 *  @note The library is specifically designed for ARM based MCUs implementing
 *  the ARM Embedded ABI.
 * 
 * ****************************************************************************/

#ifndef NDEBUG /* valid only for debug builds */

/* module includes ********************************************************** */

/* standard header files */
#include <stdint.h>

/* additional interface header files */
#include "DBG_compilerConfig.h"

/* own header files */

/* vertor function prototype declarations */
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void DMA_IRQHandler(void);
void GPIO_EVEN_IRQHandler(void);
void TIMER0_IRQHandler(void);
void USART0_RX_IRQHandler(void);
void USART0_TX_IRQHandler(void);
void ACMP0_IRQHandler(void);
void ADC0_IRQHandler(void);
void DAC0_IRQHandler(void);
void I2C0_IRQHandler(void);
void GPIO_ODD_IRQHandler(void);
void TIMER1_IRQHandler(void);
void TIMER2_IRQHandler(void);
void USART1_RX_IRQHandler(void);
void USART1_TX_IRQHandler(void);
void USART2_RX_IRQHandler(void);
void USART2_TX_IRQHandler(void);
void UART0_RX_IRQHandler(void);
void UART0_TX_IRQHandler(void);
void LEUART0_IRQHandler(void);
void LEUART1_IRQHandler(void);
void LETIMER0_IRQHandler(void);
void PCNT0_IRQHandler(void);
void PCNT1_IRQHandler(void);
void PCNT2_IRQHandler(void);
void RTC_IRQHandler(void);
void CMU_IRQHandler(void);
void VCMP_IRQHandler(void);
void LCD_IRQHandler(void);
void MSC_IRQHandler(void);
void AES_IRQHandler(void);
void NMI_Handler(void);
void Reserved9_Handler(void);
void Reserved7_Handler(void);
void Reserved10_Handler(void);
void Reserved8_Handler(void);
void Reserved13_Handler(void);
void prvGetRegistersFromStack(uint32_t *pulFaultStackAddress);
#else /* valid only for release builds */
void _IRQHandlerinterrupt(void);
#endif

#ifndef NDEBUG /* valid only for debug builds */

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/** Debug function which is entered whenever an unrecoverable system fault occurs.
 *
 * @param pulFaultStackAddress Pointer to the saved fault stack.
 */

void prvGetRegistersFromStack(uint32_t *pulFaultStackAddress)
{
    /* variables must be volatile to prevent the optimizer removing them */
    volatile uint32_t DBG_UNUSED_VARARG(r0);
    volatile uint32_t DBG_UNUSED_VARARG(r1);
    volatile uint32_t DBG_UNUSED_VARARG(r2);
    volatile uint32_t DBG_UNUSED_VARARG(r3);
    volatile uint32_t DBG_UNUSED_VARARG(r12);
    volatile uint32_t DBG_UNUSED_VARARG(lr); /* link register */
    volatile uint32_t DBG_UNUSED_VARARG(pc); /* program counter */
    volatile uint32_t DBG_UNUSED_VARARG(psr); /* program status register */

    r0 = pulFaultStackAddress[0];
    r1 = pulFaultStackAddress[1];
    r2 = pulFaultStackAddress[2];
    r3 = pulFaultStackAddress[3];

    r12 = pulFaultStackAddress[4];
    lr = pulFaultStackAddress[5];
    pc = pulFaultStackAddress[6];
    psr = pulFaultStackAddress[7];

    /* when the following line is hit, the variables contain the register values */
    for (;;)
        ;
}

/** Hard Fault exception handler */
__attribute__((naked, noreturn)) void HardFault_Handler(void)
{
    __asm volatile
    (
        " tst lr, #4                                                \n"
        " ite eq                                                    \n"
        " mrseq r0, msp                                             \n"
        " mrsne r0, psp                                             \n"
        " ldr r1, [r0, #24]                                         \n"
        " ldr r2, handler2_address_const                            \n"
        " bx r2                                                     \n"
        " handler2_address_const: .word prvGetRegistersFromStack    \n"
    );
    for (;;)
        ;
}

/** Memory Manager Fault exception handler */
__attribute__((weak, noreturn)) void MemManage_Handler(void)
{
    for (;;)
        ;
}

/** BUS Fault exception handler */
__attribute__((weak, noreturn)) void BusFault_Handler(void)
{
    for (;;)
        ;
}

/** Usage Fault exception handler */
__attribute__((weak, noreturn)) void UsageFault_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void DMA_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void GPIO_EVEN_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void TIMER0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART0_RX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART0_TX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void ACMP0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void ADC0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void DAC0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void I2C0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void GPIO_ODD_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void TIMER1_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void TIMER2_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART1_RX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART1_TX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART2_RX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void USART2_TX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void UART0_RX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void UART0_TX_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void LEUART0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void LEUART1_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void LETIMER0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void PCNT0_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void PCNT1_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void PCNT2_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void RTC_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void CMU_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void VCMP_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void LCD_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void MSC_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void AES_IRQHandler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void NMI_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void Reserved9_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void Reserved7_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void Reserved10_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void Reserved8_Handler(void)
{
    for (;;)
        ;
}

__attribute__((weak, noreturn)) void Reserved13_Handler(void)
{
    for (;;)
        ;
}

#else /* valid only for release builds */

/** @brief The ISR blocks the application if entered.
 *
 * The watch-dog supposed to reset the software if the function is entered.
 *
 * @note The ISR does not return and it is provided only for convenience.
 */
__attribute__((noreturn)) void _IRQHandlerinterrupt(void)
{
    for(;;);
}

#endif /* ifndef ndebug */

/** ************************************************************************* */
