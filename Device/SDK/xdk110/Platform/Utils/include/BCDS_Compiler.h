/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************* */
#ifndef BCDS_COMPILER_H
#define BCDS_COMPILER_H

/* public interface declaration ******************************************** */

/* public type and macro definitions */

/**
 * @brief	Macro to inform the compiler that a variable is intentionally
 * not in use.
 *
 * @param [in] variableName : The unused variable.
 */
#define COMPILER_UNUSED_VARIABLE(variableName) (void) variableName

/**
 * @brief	Macro to inform the compiler that or an argument is intentionally
 * not in use.
 *
 * GCC: in GCC the compiler option -Wunused (-Wall) generates warnings if a
 * function argument is not in use. Using the COMPILER_UNUSED_ARG() macro
 * results in the following changes in compiler behavior:
 * - attached to a variable, the macro means that the variable is meant to
 *   be possibly unused. GCC will not produce a warning for this variable.
 *
 * @example void FOO_foo(uint8_t param1, uint8_t param2){ COMPILER_UNUSED_ARG(param2); }
 *
 * @param [in] argumentName : The unused argument.
 */
#ifdef __GNUC__
#  define COMPILER_UNUSED_ARG(argumentName) argumentName __attribute__((__unused__))
#else
#  define COMPILER_UNUSED_ARG(argumentName) argumentName
#endif

/**
 * @brief	Macro to inform the compiler that a function is intentionally not in use.
 *
 * GCC: in GCC the compiler option -Wunused (-Wall) generates warnings if a
 * function is not in use. Using the COMPILER_UNUSED_FUNCTION() macro results in
 * the following changes in compiler behavior:
 * - attached to a function, the macro means that the function is meant to be
 *   possibly unused. GCC will not produce a warning for this function.
 *
 * @example void COMPILER_UNUSED_FUNCTION(FOO_foo(uint8_t param1, uint8_t param2)){ ; }
 *
 * @param [in] function : The unused function with its arguments list.
 */
#ifdef __GNUC__
#  define COMPILER_UNUSED_FUNCTION(function) __attribute__((__unused__)) function
#else
#  define COMPILER_UNUSED_FUNCTION(function) function
#endif

/**
 * @brief	Macro to inform the compiler that this section is a part of the provided
 * linker section.
 *
 * @example void COMPILER_SECTION(section_name)
 *
 * @param [in] sectionName : The section name that has been provided in the Linker file.
 */
#ifdef __GNUC__
#  define COMPILER_SECTION(sectionName) __attribute__((section(#sectionName)))
#else
#  define COMPILER_SECTION(sectionName)
#endif

/**
 * @brief	Macro to force the compiler to always inline an inline function.
 *
 * GCC: in GCC, the inline keyword is not always regarded by the compiler.
 * Based on the situation the compiler can decide to emit a real function
 * instead of the function body only.
 *
 * @example extern inline void FOO_foo(void){ ; }
 *
 * Inlining makes C level debugging hard as the inlined code is not aligned
 * with the source file. To eliminate this issue, in debug builds the macro
 * is disabled.
 */
#if defined(__GNUC__) && defined(NDEBUG)
#  define COMPILER_ALWAYS_INLINE __attribute__((always_inline))
#else
#  define COMPILER_ALWAYS_INLINE
#endif

/* public function prototype declarations */

/* public global variable declarations */

/* inline function definitions */

#endif /* BCDS_COMPILER_H */

/** ************************************************************************ */
