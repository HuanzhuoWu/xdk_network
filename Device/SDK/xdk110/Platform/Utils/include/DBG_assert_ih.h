/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/

/* header definition ******************************************************** */
#ifndef BCDS_DBG_ASSERT_IH_H_
#define BCDS_DBG_ASSERT_IH_H_

/* Make NDEBUG the default option */
#ifndef NDEBUG
#ifndef DEBUG
#define NDEBUG
#endif
#endif

#ifndef NDEBUG /* valid only for debug builds */
#include <stdint.h>
/* test if all required header files are included by the module which uses the program assertion module */
#if !defined( _STDINT_H )
#error "The C99 integer type declarations are required for the assertion module. Please include stdint.h in your C file."
#endif

/* test whether the makefile is set up properly to pass the file name to the header file */
#if !defined( DBG_ASSERT_FILENAME )
#error "The makefile must add the '-D DBG_ASSERT_FILENAME=\"$*.c\"' option to the CFLAGS make variable in debug mode."
#endif

/* public interface declaration ********************************************* */

/* public type and macro definitions */

#ifdef DISABLE_DBG_ASSERT_FLASH_USERPAGE
/** Typedef for function which is called when asserted
 *
 * @param[in] line line number at which assertion was called
 *
 * @param[in] file file name in which assertion was called
 *
 * Example function prototype which confirms to the type declaration:
 * @code void EAC_exampleAssertCallbackFunction(uint32_t line, const uint8_t* file); @endcode
 */
typedef void (*DBG_assertCallback)(uint32_t line, const uint8_t* file);

/** Maps the function which will be called when asserted
*
* @param[in] assertCallback Function pointer which will be called when program enters assert
*
* @retval void none - the function never returns
* @code  DBG_assertIndicationMapping(&EAC_exampleAssertCallbackFunction); @endcode
*/
void DBG_assertIndicationMapping(DBG_assertCallback assertCallback);
#endif

/**
 *  @note The standard exit() function increases code and ram size requirement
 *        by nearly 1 kilobyte in total! Therefore the assertion macros will
 *        block the application, instead of exiting it using the exit() API.
 *        To enable the standard exit() in the assertion library, define the
 *        DGB_ASSERT_USE_STD_EXIT symbol in the DBG_assert_ch.h configuration
 *        header file.
 */
/* #define DGB_ASSERT_USE_STD_EXIT */

/** Program assertion macro.
 *
 * The program assertion macro implements an assertion, which can be used to
 * verify assumptions made by the program and store a diagnostic message in
 * the USAERPAGE area if this assumption is false and stop program execution.
 *
 * Program assertion is behaving differently in debug and release builds.
 *
 * @note Release builds must be built with the NDEBUG symbol defined.
 *
 * @param expression A logical statement which can be true or false.
 * @param message A string about the test case - note that it is not compiled into the code.
 */
#define DBG_ASSERT(expression, message) \
	if (!(expression)) { DBG_assertError((uint32_t)__LINE__,(uint8_t*)DBG_ASSERT_FILENAME); }

/** Program assertion macro which always triggers.
 *
 * The DBG_FAIL() assertion macro implements an assertion, which always triggers.
 *
 * It can be used for example in default cases of switch::case structures.
 *
 * Program assertion is behaving differently in debug and release builds.
 *
 * @note Release builds must be built with the NDEBUG symbol defined.
 *
 * @param message A string about the test case - note that it is not compiled into the code.
 * @sa DBG_ASSERT()
 */
#define DBG_FAIL(message) \
	DBG_assertError((uint32_t)__LINE__,(uint8_t*)DBG_ASSERT_FILENAME);

/** Macro to block program execution if an assertion log have been found from
 * a previous execution session.
 *
 * @note Release builds must be built with the NDEBUG symbol defined.
 *
 * @param none
 */
#define DBG_BLOCK_IF_ASSERTED() \
	DBG_blockIfAsserted();

/** Helper macro - direct use is forbidden! */
#define DBG_ASSERT_CONCAT_(a, b) a##b

/** Helper macro - direct use is forbidden! */
#define DBG_ASSERT_CONCAT(a, b) DBG_ASSERT_CONCAT_(a, b)

/** Static (compilation time) assertion macro.
 *
 * The C99 standard does not support static assertions, nor the sizeof() keyword
 * in preprocessor macros. To be able to perform compilation time variable size
 * related checks, this macro is provided as a workaround. Note that the macro
 * has some limitations.
 *
 * @warning This macro can't be used twice on the same __LINE__ number.
 *          The user must ensure that even in header files the __LINE__
 *          number is always unique. Protect header files against multiple
 *          time inclusions and if possible put static assertions into C
 *          files instead of headers.
 *
 * @note Release builds must be built with the NDEBUG symbol defined.
 *
 * @example To check if the int integer type is 4 bytes long, the following
 *          static assertion can be used:
 *
 *          DBG_STATIC_ASSERT(sizeof(int) == 4, "The int type shall be 32 bits long.");
 *
 * @param expression A logical statement which can be true or false.
 * @param message A string about the test case - note that it is not compiled into the code.
 */
#define DBG_STATIC_ASSERT(expression, message) \
	enum { DBG_ASSERT_CONCAT(DBG_ASSERT_LINE_, __LINE__) = 1/(!!(expression)) };

/** Macro to define a Trace buffer.
 *
 * @note This macro should be put into C files.
 *
 * The trace buffer is defined in global scope. Use the DBG_TRACE_BUFFER_DECL() macro
 * to declare the defined trace buffer so it could be used in other modules.
 *
 * @param vartype Any valid variable type like uint8_t, int32_t, custom types, etc.
 * @param varname Name of the trace buffer. Reference the given buffer using the
 *                provided name. The name must be shorter than 12 characters.
 * @param buffsize Size of the trace buffer.
 *
 * @sa DBG_TRACE_BUFFER_DECL()
 */
#define DBG_TRACE_BUFFER_DEF(vartype, varname, buffsize) \
	vartype DBG_traceBuffer##varname[buffsize]; \
	uint16_t DBG_traceIndexer##varname;

/** Macro to declare a previously defined trace buffer.
 *
 * @note This macro usually goes into header files.
 *
 * Use the DBG_TRACE_BUFFER_DEF() with the same parameter list in a C file to
 * actually define the trace buffer.
 *
 * @param vartype Any valid variable type like uint8_t, int32_t, custom types, etc.
 * @param varname Name of the trace buffer. Reference the given buffer using the
 *                provided name. The name must be shorter than 12 characters.
 * @param buffsize Size of the trace buffer.
 *
 * @sa DBG_TRACE_BUFFER_DEF()
 */
#define DBG_TRACE_BUFFER_DECL(vartype, varname, buffsize) \
	extern vartype DBG_traceBuffer##varname[buffsize]; \
	extern uint16_t DBG_traceIndexer##varname; \
	enum{ DBG_TRACE_##varname##_LIMIT = buffsize };

/** Macro to push a value to the end of the trace buffer as if it were a stack.
 *
 * The trace buffer must be defined before it could be used.
 *
 * If the trace buffer is completely filled up with data, calling this macro
 * will do nothing. So plan the buffersize wisely.
 *
 * @param varname Name of the trace buffer.
 * @param element The variable or literal value which should be pushed into the trace buffer.
 *
 * @sa DBG_TRACE_BUFFER_DEF()
 */
#define DBG_TRACE_PUSH_BACK(varname, element) \
	if(DBG_traceIndexer##varname < DBG_TRACE_##varname##_LIMIT){ DBG_traceBuffer##varname[DBG_traceIndexer##varname++] = element; }

/** Macro to define an access counter.
 *
 * The macro must be placed in the beginning of a C block as it defines a static
 * variable.
 *
 * @param countername Name of the counter to be created. The name must be shorter than 16 characters.
 */
#define DBG_ACCESS_COUNTER_DEF(countername) \
	static uint8_t DBG_accCnt_##countername;

/** Macro to increment a previously defined access counter.
 *
 * The counter must be defined in the same scope otherwise an error will be
 * thrown by the compiler.
 *
 * @note The counter is not protected against overflows.
 *
 * @param countername Name of the counter to be incremented.
 */
#define DBG_ACCESS_COUNTER_HIT(countername) \
	DBG_accCnt_##countername++;

/** Macro to fetch the value of a previously defined access counter.
 *
 * The counter must be defined in the same scope otherwise an error will be
 * thrown by the compiler.
 *
 * @param countername Name of the counter to be read.
 */
#define DBG_ACCESS_COUNTER_GETVAL(countername) \
	(DBG_accCnt_##countername)

/* public function prototype declarations */
extern void DBG_assertError(const uint32_t line, const uint8_t const file[]);
extern void DBG_blockIfAsserted(void);

/* public global variable declarations */

/* inline function definitions */

#else /* valid only for release builds */

#define DBG_ASSERT(expression, message)
#define DBG_FAIL(message)
#define DBG_BLOCK_IF_ASSERTED()
#define DBG_STATIC_ASSERT(expression, message)

#define DBG_TRACE_BUFFER_DEF(vartype, varname, buffsize)
#define DBG_TRACE_BUFFER_DECL(vartype, varname, buffsize)
#define DBG_TRACE_PUSH_BACK(varname, element)

#define DBG_ACCESS_COUNTER_DEF(countername)
#define DBG_ACCESS_COUNTER_HIT(countername)
#define DBG_ACCESS_COUNTER_GETVAL(countername)

/* public function prototype declarations */

/* public global variable declarations */

/* inline function definitions */

#endif /* ifndef NDEBUG */

#endif /* BCDS_DBG_ASSERT_IH_H_ */

/**@}*/

/** ************************************************************************* */
