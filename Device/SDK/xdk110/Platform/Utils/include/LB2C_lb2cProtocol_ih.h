/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup LB2C LB2C Protocol
 *
 * @brief Lean B2C Protocol Specification
 *
 * The LB2C protocol is a binary framing protocol to transmit payload data
 * over a serial transport layer. The protocol is mainly designed to be used
 * together with stateless Client - Server architectures.
 *
 * System dependencies like transport layer and memory allocation services
 * are broken up via optional call-back functions.
 *
 * The protocol supports the following messaging schemes:
 *
 * GET operation:
 * @startuml{lb2c_get.png}
 * Client -> Server : READ (data request #1)
 * Client <- Server : WRITE (send data #1)
 * Client -> Server : READ (data request #2)
 * Client X- Server : no response
 * Client -> Server : READ (data request #2)
 * Client <- Server : WRITE (send data #2)
 * @enduml
 *
 * POST operation:
 * @startuml{lb2c_post.png}
 * Client -> Server : WRITE (send data)
 * @enduml
 *
 * NOTIFY operation:
 * @startuml{lb2c_notify.png}
 * Client <- Server : WRITE (send data on change)
 * Client <- Server : WRITE (send data on change)
 * @enduml
 *
 * POST with ACKNOWLEDGMENT operation:
 * @startuml{lb2c_post_ack.png}
 * Client <- Server : WRITE (send data)
 * Client -X Server : no confirmation
 * Client <- Server : WRITE (send same data as before)
 * Client -> Server : CONFIRMATION (acknowledge write)
 * @enduml
 *
 * INDICATION operation:
 * @startuml{lb2c_indicate.png}
 * Client <- Server : WRITE (send data on change)
 * Client -> Server : CONFIRMATION (acknowledge write)
 * Client <- Server : WRITE (send data on change)
 * Client -X Server : no confirmation
 * Client <- Server : WRITE (send same data as before)
 * @enduml
 *
 * The API set supports different integration schemes and memory models which
 * could be selected at run-time. Integration examples are provided with the
 * component in its example folder.
 *
 * The protocol uses commands (see LB2C_frameCommandType_t) to indicate the type
 * or category of a message frame.
 *
 * @todo 2014-10-22, klt2bp: Document frame format and type of commands.
 *
 * The message frame parser implements the following state machine to iterate
 * through the frame format:
 *
 * @startuml{parserStateMachine.png}
 * state "Evaluate Check Sum" as ECS
 * ECS: Plausibility Test Check Sum
 * SD: Start Delimiter
 * LE: Length of payload
 * CMD: Command Type
 * MSG: Payload data
 * CS: Check Sum
 * [*] -right-> SD
 * SD -right-> LE: Found SD
 * LE -right-> CMD: Stored LE
 * CMD -right-> CS: CMD valid
 * CS -right-> MSG: Stored CS
 * MSG -right-> ECS: Read MSG
 * CS -> ECS: Stored CS and LE = 0
 * CMD -> LE: CMD invalid, but stored LE = SD
 * CMD -> SD: CMD invalid, and stored LE != SD
 * ECS -> [*]: CS invalid
 * ECS -> [*]: CS valid
 * @enduml
 *
 * @{
 */

/* header definition */
#ifndef BCDS_LB2C_LB2CPROTOCOL_IH_H_
#define BCDS_LB2C_LB2CPROTOCOL_IH_H_

/* public interface declaration */

/* interface dependencies */
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

    /* interface dependency checks */

    /* public type and macro definitions */

    typedef enum LB2C_event_e
    {
        LB2C_EVENT_MESSAGE_RX_ERROR,
        LB2C_EVENT_MESSAGE_RX_READY,
        LB2C_EVENT_MESSAGE_TX_READY,
        LB2C_EVENT_MESSAGE_TX_CONFIRMED,
        LB2C_EVENT_MEMORY_REQUEST
    }LB2C_event_t;

    typedef enum LB2C_memoryRequestType_e
    {
        LB2C_MEM_RECEIVE_BUFFER,
        LB2C_MEM_TRANSMIT_BUFFER,
        LB2C_MEM_INTERNAL,
        LB2C_MEM_FREE
    }LB2C_memoryRequestType_t;

    typedef enum LB2C_bufferType_e
    {
        LB2C_RECEIVE_BUFFER = LB2C_MEM_RECEIVE_BUFFER,
        LB2C_TRANSMIT_BUFFER = LB2C_MEM_TRANSMIT_BUFFER,
    }LB2C_bufferType_t;

    /** Type definition for the LB2CAP API return values or result codes. */
    typedef enum LB2C_resultCode_e
    {
        LB2C_RESULT_SUCCESS = 0,
        LB2C_RESULT_FAILURE = 1,
        LB2C_RESULT_PENDING,
        LB2C_RESULT_BUSY
    }LB2C_resultCode_t;

    /** Type definition for the LB2CAP command types. */
    typedef enum LB2C_frameCommandType_e
    {
        LB2C_CMD_READ = 'R', /* 52h */
        LB2C_CMD_READ_LONG = 'R' + 'L', /* 9Eh */
        LB2C_CMD_WRITE = 'W', /* 57h */
        LB2C_CMD_WRITE_WITH_CONFIRMATION = 'W' + 'C', /* 9Ah */
        LB2C_CMD_WRITE_LONG = 'W' + 'L', /* A3h */
        LB2C_CMD_WRITE_LONG_WITH_CONFIRMATION = 'W' + 'L' + 'C', /* E6h */
        LB2C_CMD_CONFIRMATION = 'C', /* 43h */
        LB2C_CMD_INFO_CHANNEL0 = 'I' + '0', /* 79h */
        LB2C_CMD_INFO_CHANNEL1 = 'I' + '1', /* 7Ah */
        LB2C_CMD_INFO_CHANNEL2 = 'I' + '2', /* 7Bh */
        LB2C_CMD_INFO_CHANNEL3 = 'I' + '3', /* 7Ch */
    }LB2C_frameCommandType_t, *LB2C_frameCommandType_tp;

    typedef enum LB2C_errorCode_e
    {
        LB2C_NO_ERROR,
        LB2C_ERROR_INVALID_HANDLE,
        LB2C_ERROR_INVALID_ARGUMENT,
        LB2C_ERROR_INVALID_READER_CALLBACK,
        LB2C_ERROR_INVALID_BUFFER_OR_SIZE_TOO_SMALL,
        LB2C_ERROR_INVALID_CHECKSUM,
        LB2C_ERROR_MEMORY_REQUEST_FAILED,
        LB2C_ERROR_MEMORY_RELEASE_FAILED
    }LB2C_errorCode_t;

    typedef void* LB2C_handle_tp;

    typedef struct LB2C_bufferHandle_s
    {
        uint8_t* bufferPointer;
        uint16_t bufferSize;
    }LB2C_bufferHandle_t, *LB2C_bufferHandle_tp;

    typedef struct LB2C_memoryRequest_s
    {
        LB2C_bufferHandle_t buffer;
        LB2C_memoryRequestType_t requestType;
    }LB2C_memoryRequest_t, *LB2C_memoryRequest_tp;

    typedef struct LB2C_receiveEvent_s
    {
        LB2C_bufferHandle_t buffer;
        LB2C_frameCommandType_t messageType;
        LB2C_errorCode_t errorCode;
    }LB2C_receiveEvent_t, *LB2C_receiveEvent_tp;

    typedef uint8_t (*LB2C_readCallback_tp)(void* buffer, uint8_t bytesToRead);
    typedef uint8_t (*LB2C_writeCallback_tp)(void* buffer, uint8_t bytesToWrite);
    typedef LB2C_resultCode_t (*LB2C_eventDispatcher_tp)(void* data, LB2C_event_t eventType);

    /* public function prototype declarations */

    /**
     * @brief Initialize an LB2C instance.
     *
     * @param callback Optional event dispatcher callback interface to receive
     *        asynchronous event notifications from the protocol layer. @sa LB2C_eventDispatcher_tp.
     *
     * @return A valid instance handle or (LB2C_handle_tp)NULL in case of failure.
     */
    extern LB2C_handle_tp LB2C_init(LB2C_eventDispatcher_tp callback);
    extern LB2C_handle_tp LB2C_initEx(LB2C_eventDispatcher_tp callback, LB2C_bufferHandle_tp receiveBuffer, LB2C_bufferHandle_tp transmitBuffer);
    extern LB2C_resultCode_t LB2C_deinit(LB2C_handle_tp* handle);

    /**
     * The LB2C_receiveProcess() API implements an event driven architecture to read
     * an input data stream from a user registered serial interface and asynchronously
     * inform the user application about protocol errors or received message frames.
     *
     * Calling the API from interrupt context is not allowed.
     *
     * Process flow:
     * @startuml{lb2c_receiveProcess.png}
     * :<b>EVENT_UART_DATA_AVALIABLE</b>;
     * -> received event notification from system;
     * :LB2C_receiveProcess( <b>handle</b> )|
     * -> read data from serial interface;
     * note right
     *   LB2C_registerReadCallback() must
     *   have been called to register the
     *   serial read interface hook for the
     *   given instance of the LB2C service.
     * end note
     * :serialReadCallback( <b>data to read</b> )<
     * -> process read data;
     * note left
     *   Request data from serial input buffer
     *   and process whatever number of bytes
     *   were received back from the input
     *   buffer.
     * end note
     * :evetNotificationCallback( <b>data , event_type</b> );
     * note right
     * If a complete message or an error had been
     * found notify user application via the event
     * notification callback.
     * end note
     * @enduml
     *
     * @param[in] handle Valid LB2C instance handle.
     *
     * @retval LB2C_RESULT_SUCCESS No internal errors occurred.
     * @retval LB2C_RESULT_FAILURE An internal error occurred. Call
     *         LB2C_getLastError() to get more information on the error.
     */
    extern LB2C_resultCode_t LB2C_receiveProcess(LB2C_handle_tp handle);
    extern LB2C_resultCode_t LB2C_transmitProcess(LB2C_handle_tp handle);
    extern LB2C_resultCode_t LB2C_transmitFrame(LB2C_handle_tp handle, LB2C_frameCommandType_t type, LB2C_bufferHandle_tp payload);
    extern LB2C_resultCode_t LB2C_registerBuffer(LB2C_handle_tp handle, LB2C_bufferHandle_tp buffer, LB2C_bufferType_t type);
    extern LB2C_resultCode_t LB2C_registerReadCallback(LB2C_handle_tp handle, LB2C_readCallback_tp callback);
    extern LB2C_resultCode_t LB2C_registerWriteCallback(LB2C_handle_tp handle, LB2C_writeCallback_tp callback);
    extern LB2C_errorCode_t LB2C_getLastError(LB2C_handle_tp handle);
    extern uint32_t LB2C_getProtocolVersion(void);

    /* public global variable declarations */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* BCDS_LB2C_LB2CPROTOCOL_IH_H_ */

/** @} *//* LB2C group */
