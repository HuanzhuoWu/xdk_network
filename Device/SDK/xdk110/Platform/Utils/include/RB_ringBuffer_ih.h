/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BCDS_RB_RINGBUFFER_IH_H
#define BCDS_RB_RINGBUFFER_IH_H

/**
 *  @brief
 *      Describes a circular buffer
 *  
 */

typedef struct RingBuffer_t
{
    uint8_t * Base; /** Pointer to the base of the user-supplied buffer */
    uint8_t * Wptr; /** Write pointer. NOT to be changed by hand */
    uint8_t * Rptr; /** Read pointer. NOT to be changed by hand */
    uint16_t Count; /** Number of unread bytes currently in the buffer. May be read */
    uint16_t Size; /** Maximum number of bytes in the user-supplied buffer. Must be set during init */
} RingBuffer_t;

typedef enum RB_status_t
{
    RB_SUCCESS = 0,
    RB_ARG = 1,
    RB_RESOURCE = 2,
    RB_LOCK = 3,
    RB_UNREACHABLE = 4,
    RB_FLASH_WRITEFAIL = 5,
    RB_FLASH_ERASEFAIL = 6,
    RB_FLASH_ERASEREQ = 7,
    RB_PERMISSION = 8
} RB_status_t;

#define RingBuf_Push(ringBufferHandler, byte)   RingBuf_Write((ringBufferHandler),((uint8_t *) &(byte)),UINT16_C(1))

#define RingBuf_Pop(ringBufferHandler, byte)   RingBuf_Read((ringBufferHandler),((uint8_t *) &(byte)),UINT16_C(1))

extern void RingBuf_Init(RingBuffer_t * ringBufferHandler, uint8_t * buffer_p, uint16_t size);
extern RB_status_t RingBuf_Write(RingBuffer_t * ringBufferHandler, uint8_t * src_p, uint16_t len);
extern RB_status_t RingBuf_Read(RingBuffer_t * ringBufferHandler, uint8_t * dest_p, uint16_t len);

#endif

/**@}*/
