/**
* Licensee agrees that the example code provided to Licensee has been developed and released by Bosch solely as an example to be used as a potential reference for Licensee�s application development. 
* Fitness and suitability of the example code for any use within Licensee�s applications need to be verified by Licensee on its own authority by taking appropriate state of the art actions and measures (e.g. by means of quality assurance measures).
* Licensee shall be responsible for conducting the development of its applications as well as integration of parts of the example code into such applications, taking into account the state of the art of technology and any statutory regulations and provisions applicable for such applications. Compliance with the functional system requirements and testing there of (including validation of information/data security aspects and functional safety) and release shall be solely incumbent upon Licensee. 
* For the avoidance of doubt, Licensee shall be responsible and fully liable for the applications and any distribution of such applications into the market.
* 
* 
* Redistribution and use in source and binary forms, with or without 
* modification, are permitted provided that the following conditions are 
* met:
* 
*     (1) Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer. 
* 
*     (2) Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in
*     the documentation and/or other materials provided with the
*     distribution.  
*     
*     (3)The name of the author may not be used to
*     endorse or promote products derived from this software without
*     specific prior written permission.
* 
*  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
*  IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
*  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
*  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
*  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
*  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
*  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
*  HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
*  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
*  IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
*  POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @defgroup log LOG
 * @brief Logging Interface
 * @ingroup Utility
 *
 * @{
 * @brief  Module to set the debug log levels
 *
 * 	API to set the debug log levels
 *
 * 	 By default Log module allows to print messages related to error type, if application needs warning/info/all messages , needs to configure using
 * 	 LOG_init API, configuration of messages categorized into the following levels
 *
 * 	 LOG_ERROR   :  Setting this option would enable log messages of type ERROR alone to be displayed through USB
 * 	 LOG_WARNING :  Setting this option would enable log messages of types WARNING and INFO to be displayed through USB
 * 	 LOG_INFO    :  Setting this option would enable log messages of types ERROR, WARNING and INFO to be displayed through USB
 * 	 LOG_DEBUG   :  Setting this option would enable log messages of types ERROR, WARNING, INFO and DEBUG to be displayed through USB
 * 	 LOG_DISABLE :  Setting this option would disable log messages of all types
 *
 */
/* header definition ******************************************************** */
#ifndef BCDS_LOG_MODULE_IH_H_
#define BCDS_LOG_MODULE_IH_H_
/* public interface declaration ********************************************* */

/* public type and macro definitions */

/** enum representing module names
 *add modules to this enum list*/
typedef enum LOG_module_e
{
    LOG_WIFI,           /**< Wifi module */
    LOG_UART,           /**< UART module */
    LOG_USB,            /**< USB module */
    LOG_BTLE,           /**< Bluetooth module */
    LOG_ACCELEROMETER,  /**< Accelerometer sensor advanced API module */
    LOG_LIGHTSENSOR,    /**< light sensor advanced API module */
    LOG_GYROSCOPE,      /**< Gyroscope sensor advanced API module */
    LOG_MAGNETOMETER,   /**< Magnetometer sensor advanced API module */
    LOG_MAXMODULE,      /**< Maximum Modules*/
} LOG_module_t;

/**return values are represent by this enum*/
typedef enum LOG_retStatus_e
{
    LOG_FAILURE,
    LOG_SUCCESS,
    LOG_INVALID_ARGS,
} LOG_retStatus_t;

/** enum representing logging levels*/
typedef enum LOG_level_e
{
    LOG_ERROR,/**<enable debug error messages*/
    LOG_WARNING,/**<enable debug warning messages*/
    LOG_INFO,/**<enable debug info messages*/
    LOG_DEBUG, /**<enable debug error and warning messages*/
    LOG_DISABLE, /**<Disable debug messages*/
    LOG_MAX_LEVEL,
} LOG_level_t;

/** structure to hold module name and corresponding level values*/
typedef struct LOG_config_s
{
    LOG_module_t moduleName;/*Module name*/
    LOG_level_t logLevel;/*Logging level*/
} LOG_config_t;

/* public global variable declarations */

/* public function prototype declarations */

/**
 * @brief Configure the logging levels
 *
 * @param [in]  logLevelConfigParam          Pointer to the structure to configure log levels
 * @param [in]  noOfModules                  Number of modules to be configured
 * @retval LOG_FAILURE Returned when the API call failed
 * @retval LOG_SUCCESS Returned when the API call succeeded
 *
 */
LOG_retStatus_t LOG_init(LOG_config_t * logLevelConfigParam,
    uint8_t noOfModules);

/**
 * @brief vprintf is used to print the data according to configured log levels
 *
 * @param [in]  moduleName          Module name from which the message to be printed
 * @param [in]  level               Log level of the message categorized to
 * @param [in]  format              Data that need to be printed
 * @retval      uint8_t             Total number of characters written is returned
 *
 * @warning    The API is based on the vprintf functionality from newlib. Please note that the newlib is not thread safe by default.
 *             Therefore API is protected by disabling interrupts during usage of vprintf and then re-enabled.
 *             RTOS critical section has not been used here.
 */
uint8_t LOG_printf(LOG_module_t moduleName, LOG_level_t level,
    const void * format, ...);

/**
 * @brief To disable printing the particular module debug log messages
 *
 * @param [in]  moduleName         Module name to which logging feature needs to be disabled
 * @retval LOG_FAILURE Returned when the API call failed
 * @retval LOG_SUCCESS Returned when the API call succeeded
 *
 */
LOG_retStatus_t LOG_disableModuleLog(LOG_module_t moduleName);

/**
 * @brief To change the logging level of a module
 *
 * @param [in]  moduleName         Module name to which the log level has to be changed
 * @param [in]  level              Log level that needs to be changed from previous configured
 * @retval LOG_FAILURE Returned when the API call failed
 * @retval LOG_SUCCESS Returned when the API call succeeded
 *
 */
LOG_retStatus_t LOG_changeModuleLogLevel(LOG_module_t moduleName,
    LOG_level_t level);

/**
 * @brief      To disable the printing of all modules debug logging messages
 *
 */
void LOG_deinit(void);

#endif/* BCDS_LOG_MODULE_IH_H_*/

/**@}*/
