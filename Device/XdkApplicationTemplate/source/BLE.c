#include "BCDS_Basics.h"
#include "XdkSensorHandle.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BLE_serialDriver_ih.h" //BLE_serialDriver.h
#include "BLE_stateHandler_ih.h" //BLE_stateHandler.h
#include "BleAlpwDataExchange_Server.h"
#include "XdkApplicationTemplate.h"
#include <stdio.h>

#include "BLE.h"

xTaskHandle blehandle;

void bleDataTransmit()
{
	dataStruct data = send_readEnvironmentalSensor();
	BleStatus bleSendReturn;

	bleSendReturn = BLE_sendData((uint32_t *) &data, (sizeof(dataStruct))/2);

//	vTaskDelay(MILLISECONDS(500));

	bleSendReturn = BLE_sendData((uint32_t *) &data + 5, (sizeof(dataStruct))/2);

	if(bleSendReturn == BLESTATUS_FAILED)
	{
		printf("SENDEN FEHLGESCHLAGEN\n");
		// Sending of Data did not work
	}
}

static void bleAlpwDataExchangeService(BleAlpwDataExchangeEvent event, BleStatus status, void *parms)
{
	BleStatus bleSendReturn;

	// check the Host side receive event
	if ((BLEALPWDATAEXCHANGE_EVENT_RXDATA == event) && (BLESTATUS_SUCCESS == status) && (parms))
	{
		BleAlpwDataExchangeServerRxData *rxData = (BleAlpwDataExchangeServerRxData *) parms;

		// initiate and copy the payload to a local array and print
		//uint8_t bleReceiveBuffer[rxData->rxDataLen];

		//memcpy(bleReceiveBuffer, rxData->rxData, rxData->rxDataLen);
		//printf("Received data: %I %i \n", bleReceiveBuffer, rxData->rxDataLen);
		uint32_t* data = rxData->rxData;
		printf("Set timer intervall to %i\n", *data);


		changeCurrentTimerVal(*data);
		//uint32_t timerVal = (uint32_t) bleReceiveBuffer;
		//changeCurrentTimerVal(timerVal);
//		bleSendReturn = BLE_sendData((int*) "Daten erhalten!", sizeof("Daten erhalten!"));
	}
}

static void bleAppServiceRegister(void)
{
	BleStatus serviceRegistryStatus;

	// ALPWISE data Exchange Service Register
	serviceRegistryStatus = BLEALPWDATAEXCHANGE_SERVER_Register(bleAlpwDataExchangeService);
	if (serviceRegistryStatus == BLESTATUS_FAILED)
	{
		// BLE Service registry failure
	}
}

static void notificationCallback(BLE_connectionDetails_t connectionDetails)
{
	switch (connectionDetails.connectionStatus)
	{
		case BLE_CONNECTED_TO_DEVICE:
			printf("Device connected: %02x:%02x:%02x:%02x:%02x:%02x \r\n",
					connectionDetails.remoteAddress.addr[0],
					connectionDetails.remoteAddress.addr[1],
					connectionDetails.remoteAddress.addr[2],
					connectionDetails.remoteAddress.addr[3],
					connectionDetails.remoteAddress.addr[4],
					connectionDetails.remoteAddress.addr[5]);

			uint32_t * uniqueIDAddress = 0xFE081F0;
			BLE_sendData(uniqueIDAddress, sizeof(uint32_t));

			
			initEnvironmentalSensor();
			createSensorDataTimer();

			break;
		case BLE_DISCONNECTED_FROM_DEVICE:
			printf("Device disconnected!\r\n Going back to Selection of connection-type\n");

			deleteSensorDataTimer();
			ButtonInit();
			vTaskDelete(blehandle);

		default:
			// Invalid status of BLE Device
			break;
	}
}

static void bleAppHandler(void* handle)
{
	(void) handle;

	/*enable connection notifications*/
		BLE_notification_t configParams;
		BLE_returnStatus_t bleRetStatus;

		// callback function to react accordingly to the connection events
		configParams.callback = notificationCallback;

		// configuration to enable connection event notifications
		configParams.enableNotification = BLE_ENABLE_NOTIFICATION;

		// set configuration parameters to BLE stack
		bleRetStatus = BLE_enablenotificationForConnect(configParams);
		if (bleRetStatus == BLE_INVALID_PARAMETER){
			// invalid configuration
		}

	/*register the callback for the custom ALAPWISE Data Exchange Service*/
		BLE_status bleStatus;

		// Registering the BLE Services
		bleStatus = BLE_customServiceRegistry(bleAppServiceRegister);
		if (bleStatus == BLESTATUS_FAILED){
			// BLE service registry failed
		}

	/*set the name for the BLE device*/
		BLE_returnStatus_t bleRetNameStatus;

		// Setting the BT device name
		uint8_t devname[] = "BLE XDK Sensor";

		bleRetNameStatus = BLE_setDeviceName(devname, sizeof(devname));
		if (bleRetNameStatus == BLE_INVALID_PARAMETER){
			//invalid device name
		}

	/*initialize the BLE Stack*/
		BleStatus appInitReturn = BLESTATUS_FAILED;

		// Initialize the whole BLE stack
		appInitReturn = BLE_coreStackInit();
		if (appInitReturn == BLESTATUS_FAILED){
		// BLE Boot up process Failed
		}
//		else if (appInitReturn == BLE_STATUS_SUCCESS)
//		{
//			initEnvironmentalSensor();
//			createSensorDataTimer();
//		}

	/*run the BLE State Machine*/
		// return variable for stack receive status from base band
		BLE_return_t bleTrsprtRetVal;

		// return variable for BLE state handler
		BLE_appStateReturn bleStateHandleRetVal;
		for (;;)
		{
			// Notify that BLE data is received from base band
			bleTrsprtRetVal = BLE_hciReceiveData();

			// Run BLE state machine with specified configuration
			bleStateHandleRetVal = BLE_coreStateMachine();

			// Send data to the client if notification/indication enabled
//			bleDataTransmit();
		}

	//delete BLE-task when not needed anymore
	vTaskDelete(NULL);
}

//create BLE-task
void create_BLETask(void)
{
    int taskStackDepth = 1024;
    int taskPriority = 1;

    if(pdPASS != xTaskCreate
    		(bleAppHandler,				//function for taskcreation of BLE-task
    		(const char * const) "BLE",	//name for BLE-task
			taskStackDepth,
			NULL,						//parameters for BLE-task function
			taskPriority,
			blehandle							//pointer to a task handle (for later reference??)
    		))
    {
    	//SW timer was not started, due to insufficient heap memory
    }
}
