#ifndef XDK110_BLE_H_
#define XDK110_BLE_H_

#define SECONDS(x) ((portTickType) (x * 1000) / portTICK_RATE_MS)
#define MILLISECONDS(x) ((portTickType) (x) / portTICK_RATE_MS)

#define BLE_ROLE_SCANNER 				0
#define BLE_ROLE_ADVERTISER 			1
#define BLE_CONNECTION_SUPPORT 			1
#define BLE_RANDOM_ADDRESSING_SUPPORT 	1

void bleDataTransmit(void);

void create_BLETask(void);

#endif
