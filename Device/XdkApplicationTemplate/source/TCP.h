#ifndef XDK110_TCP_H_
#define XDK110_TCP_H_

typedef enum returnTypes_e
{
    STATUS_NOT_OK,
    STATUS_OK,
    SOCKET_ERROR,
    SEND_ERROR
} returnTypes_t;


#define WLAN_CONNECT_WPA_SSID                "Wolke"         /**< Macros to define WPA/WPA2 network settings */
#define WLAN_CONNECT_WPA_PASS                "65617616595501194770"      /**< Macros to define WPA/WPA2 network settings */
#define BUFFER_SIZE        UINT8_C(4)
/** IP addressed of server side socket.Should be in long format, E.g: 0xc0a82177 == 192.168.33.119 */
#define SERVER_IP         UINT32_C(0xC0A802B0)
#define SERVER_PORT        UINT16_C(65530)           /**< Port number on which server will listen */


#define MILLISECONDS(x) ((portTickType) (x) / portTICK_RATE_MS)

void create_TCPTask(void);
void tcpDataTransmit(void);

#endif
