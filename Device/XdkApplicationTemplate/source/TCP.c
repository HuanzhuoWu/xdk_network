
#include <stdio.h>
#include "simplelink.h"
#include "BCDS_Basics.h"
#include "BCDS_Assert.h"
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_WlanConnect.h"
#include "BCDS_NetworkConfig.h"
#include <Serval_Types.h>
#include <Serval_Basics.h>
#include <Serval_Ip.h>

#include <XdkSensorHandle.h>

#include "TCP.h"
#include "XDKApplicationTemplate.h"

xTaskHandle tcphandle;
//xTaskHandle sendHandle;
xTaskHandle recvHandle;
_i16 SockID;

uint32_t delay;


static void wifiConnectGetIP(void)
{
    NetworkConfig_IpSettings_T myIpSettings;
    memset(&myIpSettings, (uint32_t) 0, sizeof(myIpSettings));
    char ipAddress[PAL_IP_ADDRESS_SIZE] = { 0 };
    Ip_Address_T* IpaddressHex = Ip_getMyIpAddr();
    WlanConnect_SSID_T connectSSID;
    WlanConnect_PassPhrase_T connectPassPhrase;
    Retcode_T ReturnValue = (Retcode_T)RETCODE_FAILURE;
    int32_t Result = INT32_C(-1);

    if (RETCODE_OK != WlanConnect_Init())
    {
        printf("Error occurred initializing WLAN \r\n ");
        return;
    }

    printf("Connecting to %s \r\n ", WLAN_CONNECT_WPA_SSID);

    connectSSID = (WlanConnect_SSID_T) WLAN_CONNECT_WPA_SSID;
    connectPassPhrase = (WlanConnect_PassPhrase_T) WLAN_CONNECT_WPA_PASS;
    ReturnValue = NetworkConfig_SetIpDhcp(NULL);
    if (ReturnValue)
    {
        printf("Error in setting IP to DHCP\n\r");
        return;
    }

    if (RETCODE_OK == WlanConnect_WPA(connectSSID, connectPassPhrase, NULL))
    {
        ReturnValue = NetworkConfig_GetIpSettings(&myIpSettings);
        if (RETCODE_OK == ReturnValue)
        {
            *IpaddressHex = Basics_htonl(myIpSettings.ipV4);
            Result = Ip_convertAddrToString(IpaddressHex, ipAddress);
            if (Result < 0)
            {
                printf("Couldn't convert the IP address to string format \r\n ");
                return;
            }
            printf("Connected to WPA network successfully. \r\n ");
            printf("Ip address of the device: %s \r\n ", ipAddress);
        }
        else
        {
            printf("Error in getting IP settings\n\r");
            return;
        }
    }
    else
    {
        printf("Error occurred connecting %s \r\n ", WLAN_CONNECT_WPA_SSID);
        return;
    }

}

static returnTypes_t initTCP(void){
	_i16 Status;

	SlSockAddrIn_t  Addr;
	_i16 AddrSize = sizeof(SlSockAddrIn_t);

	Addr.sin_family = SL_AF_INET;
	Addr.sin_port = sl_Htons(SERVER_PORT);
	Addr.sin_addr.s_addr = sl_Htonl(SERVER_IP);

	SockID = sl_Socket(SL_AF_INET,SL_SOCK_STREAM, 0);
	if (SockID < 0){
		printf ("sl_Socket failed\n");
		return (SOCKET_ERROR);
	}
	else if (SockID == 0){
		printf ("sl_Socket failed, Null ID\n");
		return (SOCKET_ERROR);
	}
	else{
		printf ("sl_Socket successfully\n");

		while(sl_Connect(SockID, (SlSockAddr_t *)&Addr, AddrSize)){
			printf ("sl_Connect failed\n");
		}

		uint32_t * uniqueIDAddress = 0xFE081F0;
		sl_Send(SockID, uniqueIDAddress, sizeof(uint32_t) , 0 );

		printf ("sl_Connect successfully\n");
		return (STATUS_OK);
	}
}

void tcpDataTransmit(){
	printf("Start sending");
	printf("%i\n", sizeof(char));
	dataStruct data = send_readEnvironmentalSensor();

	_i16 test = sl_Send(SockID, &data, sizeof(dataStruct) , 0 );
	printf("%i\n", test);

	if(test == -1)
	{
		printf("TCP Connection disconnected! Going back to Selection of connection-type\n");
		deleteSensorDataTimer();
		vTaskDelete(recvHandle);
		ButtonInit();
		vTaskDelete(tcphandle);

	}
}

static void recvLoop(void){
	_i16 Status;
	printf("Start recvLoop");
	uint32_t recBuf;
	while(1){
		Status = sl_Recv(SockID, &recBuf, sizeof(uint32_t) , 0);
		if (Status == 4){
			printf("%u, %i\n", recBuf, Status);
			delay = recBuf;
			changeCurrentTimerVal(delay);
		}
	}
}

static void initConnection(void){
	returnTypes_t Status;
	delay = 1000;
	wifiConnectGetIP();
	Status = initTCP();

	if(Status == STATUS_OK){
//		xTaskCreate(sendLoop,(const char* const)"Send", configMINIMAL_STACK_SIZE,NULL,1,&sendHandle);

		initEnvironmentalSensor();
		createSensorDataTimer();

		xTaskCreate(recvLoop,(const char* const)"recv", configMINIMAL_STACK_SIZE,NULL,1,&recvHandle);
	}
}

void create_TCPTask(void)
{
	xTaskCreate(initConnection,(const char* const)"TCP - Task", configMINIMAL_STACK_SIZE,NULL,1,&tcphandle);
}
