//#include <BCDS_Basics.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <timers.h>
#include <led.h>
#include <button.h>
#include <XdkBoardHandle.h>
#include <XdkSensorHandle.h>

#include "XdkApplicationTemplate.h"

#include "TCP.h"
#include "BLE.h"

static BUTTON_handle_tp Button1Handle = (BUTTON_handle_tp) NULL;
static BUTTON_handle_tp Button2Handle = (BUTTON_handle_tp) NULL;

Retcode_T SensorInitialised = RETCODE_FAILURE;

xTimerHandle datahandle;

uint32_t currentTimerVal = 1000;

connectiontype connection = TCP;

void changeCurrentTimerVal(uint32_t timerVal)
{
	xTimerChangePeriod(datahandle, MILLISECONDS(timerVal), MILLISECONDS(10));
	currentTimerVal = timerVal;
}

void sendData(void)
{
	if(connection == BLE)
	{
		bleDataTransmit();
	}
	else if(connection == TCP)
	{
		tcpDataTransmit();
	}
	else
	{
		printf("No Method for sending the Data is specified!\n");
	}
}

void initEnvironmentalSensor (void)
{
	Retcode_T returnOverSamplingValue_Env = Environmental_setOverSamplingPressure(xdkEnvironmental_BME280_Handle,ENVIRONMENTAL_BME280_OVERSAMP_2X);
	Retcode_T returnFilterValue_Env = Environmental_setFilterCoefficient(xdkEnvironmental_BME280_Handle, ENVIRONMENTAL_BME280_FILTER_COEFF_2);
	Retcode_T returnValue_Env = (Environmental_init(xdkEnvironmental_BME280_Handle) || returnFilterValue_Env || returnOverSamplingValue_Env);

	Retcode_T returnBandwidthValue_Gyr = Gyroscope_setBandwidth(xdkGyroscope_BMG160_Handle, GYROSCOPE_BMG160_BANDWIDTH_116HZ);
	Retcode_T retunRangeValue_Gyr = Gyroscope_setRange(xdkGyroscope_BMG160_Handle, GYROSCOPE_BMG160_RANGE_500s);
	Retcode_T returnValue_Gyr = (Gyroscope_init(xdkGyroscope_BMG160_Handle) || retunRangeValue_Gyr || returnBandwidthValue_Gyr);

	Retcode_T returnBandwidthValue_Acc = Accelerometer_setBandwidth(xdkAccelerometers_BMA280_Handle, ACCELEROMETER_BMA280_BANDWIDTH_125HZ);
	Retcode_T returnRangeValue_Acc = Accelerometer_setRange(xdkAccelerometers_BMA280_Handle, ACCELEROMETER_BMA280_RANGE_2G);
	Retcode_T returnValue_Acc = (Accelerometer_init(xdkAccelerometers_BMA280_Handle) || returnRangeValue_Acc || returnBandwidthValue_Acc);

	if((returnValue_Env || returnValue_Gyr || returnValue_Acc) == RETCODE_FAILURE)
	{
		SensorInitialised = RETCODE_SUCCESS;
	}
}

dataStruct send_readEnvironmentalSensor(void)
{
	Environmental_Data_T bme280 = { INT32_C(0), UINT32_C(0), UINT32_C(0) };
	Gyroscope_XyzData_T bmg160 = {INT32_C(0), INT32_C(0), INT32_C(0)};
	Accelerometer_XyzData_T bma280 = {INT32_C(0), INT32_C(0), INT32_C(0)};

	if(SensorInitialised == RETCODE_FAILURE)
	{
		printf("Sensoren sind nicht initialisiert!!!");
	}
	else
	{
		Environmental_readData(xdkEnvironmental_BME280_Handle, &bme280);
		//printf("BME280 Environmental Conversion Data :\n\rp =%ld Pa\n\rt =%ld mDeg\n\rh =%ld %%rh\n\r", (long int) bme280.pressure, (long int) bme280.temperature, (long int) bme280.humidity);

//		memset(&bmg160, 0, sizeof(Gyroscope_XyzData_T));
		Gyroscope_readXyzDegreeValue(xdkGyroscope_BMG160_Handle, &bmg160);
		//printf("BMG160 Gyro Data :\n\rx =%ld mDeg\n\ry =%ld mDeg\n\rz =%ld mDeg\r", (long int) bmg160.xAxisData, (long int) bmg160.yAxisData, (long int) bmg160.zAxisData);

		memset(&bma280, 0, sizeof(Accelerometer_XyzData_T));
		Accelerometer_readXyzGValue(xdkAccelerometers_BMA280_Handle,&bma280);
		//printf("BMA280 Acceleration Data - M/S2\t: %f m/s2\t %f m/s2\t %f m/s2\n\r", (float) bma280.xAxisData, (float) bma280.yAxisData, (float)bma280.zAxisData);
	}

	dataStruct data = {currentTimerVal, bme280, bmg160, bma280};
	return data;
}

void deleteSensorDataTimer(void)
{
	xTimerDelete(datahandle, MILLISECONDS(10));
}

void createSensorDataTimer(void)
{
	datahandle = xTimerCreate(
			(const char * const) "Send Sensor Data", // used only for debugging purposes
			MILLISECONDS((int) currentTimerVal), // timer period
			pdTRUE, //Autoreload pdTRUE or pdFALSE - should the timer start again
			//after it expired?
			NULL, // optional identifier
			sendData // static callback function
			);

	if(NULL == datahandle)
	{
		assert(pdFAIL);
		return;
	}

	BaseType_t timerResult = xTimerStart(datahandle, MILLISECONDS(10));
	if(pdTRUE != timerResult)
	{
		assert(pdFAIL);
	}
}

static void Buttoncallback(void *handle, uint32_t userParameter)
{
    switch (userParameter)
    {
    /*  Button 1 press/release */
    case CALLBACK_PARAMETER_PB1:
        if (BUTTON_isPressed(handle))
        {
        	printf("PB1 pressed\n\r");
        	connection = TCP;
        	create_TCPTask();
        	ButtonDeinit();
        }
        break;

        /* Button 2 press/release */
    case CALLBACK_PARAMETER_PB2:
        if (BUTTON_isPressed(handle))
        {
        	printf("PB2 pressed\n\r");
        	connection = BLE;
        	create_BLETask();
        	ButtonDeinit();
        }
        break;
    default:
        printf("Button not available \n\r");
        break;
    }
}

static return_t setButtonCallback(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_setCallback(Button1Handle, Buttoncallback, CALLBACK_PARAMETER_PB1);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_setCallback(Button2Handle, Buttoncallback, CALLBACK_PARAMETER_PB2);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

static return_t enableButton(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;
    buttonReturn = BUTTON_enable(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_enable(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

static return_t createButton(void)
{
    return_t returnValue = FAILURE;

    Button1Handle = BUTTON_create(gpioButton1_Handle, GPIO_STATE_OFF);
    if (Button1Handle != NULL)
    {
        Button2Handle = BUTTON_create(gpioButton2_Handle, GPIO_STATE_OFF);
    }
    if (Button2Handle != NULL)
    {
        returnValue = SUCCESS;
    }
    return (returnValue);
}

return_t ButtonInit(void)
{
   	return_t returnValue = FAILURE;

   	returnValue = createButton();

   	if (returnValue == SUCCESS)
   	{
   		returnValue = enableButton();
   	}
   	else
   	{
   		printf("Error in creating button\n\r");

   	}
   	if (returnValue == SUCCESS)
   	{
   		returnValue = setButtonCallback();
   	}
   	else
   	{
   		printf("Error in enabling button\n\r");
   	}
   	return (returnValue);
}

static return_t ButtonDeinit(void)
{
    return_t returnValue = FAILURE;
    BUTTON_errorTypes_t buttonReturn = BUTTON_ERROR_INVALID_PARAMETER;

    buttonReturn = BUTTON_delete(Button1Handle);
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        buttonReturn = BUTTON_delete(Button2Handle);
    }
    if (buttonReturn == BUTTON_ERROR_OK)
    {
        returnValue = SUCCESS;
    }
    return returnValue;
}

void appInitSystem(xTimerHandle xTimer)
{
    return_t returnValue = FAILURE;
    BCDS_UNUSED(xTimer);
    returnValue = ButtonInit();
    if (returnValue != SUCCESS)
    {
        printf("Error in button initialization\n\r");
    }
}
