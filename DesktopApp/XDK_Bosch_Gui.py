import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtSvg import QSvgRenderer, QSvgWidget
import random
import SVGPrinterClass
import time
from TCP import *
from BLE import *
from functools import *

class Xdk_Desktop_Gui(QWidget):

	def __init__(self, deviceHandler):
		super().__init__()
		self.myDeviceHandler = deviceHandler
		self.initUi()

	def initUi(self):
		self.grid = QGridLayout()
		self.setLayout(self.grid)

		self.label_explanation = QLabel("Choose a Sensor and a sample rate and press \"connect\"!")
		self.grid.addWidget(self.label_explanation,1,1)													#Positionierung

		self.radio_temperature = QRadioButton("temperature")
		self.radio_humidity = QRadioButton("humidity")
		self.radio_pressure = QRadioButton("air pressure")
		self.radio_gyroscopeX = QRadioButton("gyroscope x")
		self.radio_gyroscopeY = QRadioButton("gyroscope y")
		self.radio_gyroscopeZ = QRadioButton("gyroscope z")
		self.radio_accelerationX = QRadioButton("acceleration x")
		self.radio_accelerationY = QRadioButton("acceleration y")
		self.radio_accelerationZ = QRadioButton("acceleration z")
		self.radio_group_sensors = QGroupBox("Please choose a sensor:")
		radio_group_layout_sensors = QVBoxLayout()
		radio_group_layout_sensors.addWidget(self.radio_temperature)
		radio_group_layout_sensors.addWidget(self.radio_humidity)
		radio_group_layout_sensors.addWidget(self.radio_pressure)
		radio_group_layout_sensors.addWidget(self.radio_gyroscopeX)
		radio_group_layout_sensors.addWidget(self.radio_gyroscopeY)
		radio_group_layout_sensors.addWidget(self.radio_gyroscopeZ)
		radio_group_layout_sensors.addWidget(self.radio_accelerationX)
		radio_group_layout_sensors.addWidget(self.radio_accelerationY)
		radio_group_layout_sensors.addWidget(self.radio_accelerationZ)
		self.radio_group_sensors.setLayout(radio_group_layout_sensors)
		self.grid.addWidget(self.radio_group_sensors,3,2)												#Positionierung



		self.btn_connect_and_measure = QPushButton("connect to selected device and start measuring")
		self.btn_connect_and_measure.clicked.connect(self.btn_connect_and_measure_pressed)
		self.grid.addWidget(self.btn_connect_and_measure,4,1,1,3)														#Positionierung

		self.slider_rate = QSlider(Qt.Horizontal)
		self.slider_rate.setMaximum(100)
		self.slider_rate.setMinimum(2)
		self.slider_rate.setValue(20)
		self.slider_rate.setTickPosition(QSlider.TicksBelow)
		self.slider_rate.setTickInterval(19.6)
		self.grid.addWidget(self.slider_rate,2,2)														#Positionierung
		self.slider_rate.valueChanged.connect(self.slider_rate_changed)



		self.slider_rate_label = QLabel("sample rate: "+str(self.slider_rate.value()/10)+" Hz")
		self.grid.addWidget(self.slider_rate_label,1,2)														#Positionierung


		self.scroll_area_layout = QVBoxLayout()
		self.scroll_area_group = QGroupBox()
		self.scroll_area_group.setLayout(self.scroll_area_layout)

		self.scroll_area = QScrollArea()
		self.scroll_area.setWidget(self.scroll_area_group)
		self.scroll_area.setWidgetResizable(True)
		self.grid.addWidget(self.scroll_area,3,1,1,1)													#Positionierung
		self.scroll_area_group.resize(self.scroll_area.height(),self.scroll_area.width())

		self.scroll_area_label = QLabel("Choose a device from the list below:")
		self.grid.addWidget(self.scroll_area_label,2,1)													#Positionierung



		self.svg_image = QSvgWidget('empty_diagramme.svg',self)
		self.svg_image.setFixedSize(700,400)
		self.grid.addWidget(self.svg_image,5,1,3,3)

		self.device_update_thread = UpdateDeviceThread(self.myDeviceHandler)
		self.device_update_thread.devicelist_updated.connect(self.update_devicelist)
		self.device_update_thread.start()

		self.data_update_thread = UpdateDataThread()
		self.data_update_thread.data_updated.connect(self.update_data)

		self.printing_thread = 0

		self.scroll_area_device_list = []

		self.data = [0,0,0,0,0,0,0,0,0,0,0]
		self.toggledDevicesStringList = []

		self.setFixedSize(self.sizeHint())
		self.setWindowTitle('Hauptseminar - Bosch XDK')
		self.show()



	def slider_rate_changed(self):
		self.slider_rate_label.setText("sample rate: "+str(self.slider_rate.value()/10)+" Hz")

	def slider_xaxis_changed(self):
		self.slider_xaxis_label.setText("number of steps on time axis: "+str(self.slider_xaxis.value()))

	def button_clicked(self, btn):
		print(btn.text())
		if btn.text() not in self.toggledDevicesStringList:
			self.toggledDevicesStringList.append(btn.text())
		else:
			self.toggledDevicesStringList.remove(btn.text())
		print(self.toggledDevicesStringList)



	def btn_connect_and_measure_pressed(self):
		if self.data_update_thread != 0 and self.printing_thread != 0:
			self.data_update_thread.terminationRequested = True
			self.data_update_thread.wait(500)

			self.printing_thread.terminationRequested = True
			self.printing_thread.wait(500)
			self.data_update_thread = UpdateDataThread()
			self.data_update_thread.data_updated.connect(self.update_data)

		devices = []
		for i in self.toggledDevicesStringList:
			devices.append(self.myDeviceHandler.getDeviceByIpAddress(i))
		for i in devices:
			print(1/(self.slider_rate.value()/10)*1000)
			i.setUpdateIntervall(int(1/(self.slider_rate.value()/10)*1000))
			self.data_update_thread.new_devices(i)



		if self.radio_temperature.isChecked():
			unit = "mdeg"
			self.data_update_thread.change_sensor("temperature")

		elif self.radio_pressure.isChecked():
			unit = "mbar"
			self.data_update_thread.change_sensor("pressure")


		elif self.radio_humidity.isChecked():
			unit = "%"
			self.data_update_thread.change_sensor("humidity")


		elif self.radio_gyroscopeX.isChecked():
			unit = "degree"
			self.data_update_thread.change_sensor("gyroX")


		elif self.radio_gyroscopeY.isChecked():
			unit = "degree"
			self.data_update_thread.change_sensor("gyroY")

		elif self.radio_gyroscopeZ.isChecked():
			unit = "degree"
			self.data_update_thread.change_sensor("gyroZ")


		elif self.radio_accelerationX.isChecked():
			unit = "milli g"
			self.data_update_thread.change_sensor("accelerationX")

		elif self.radio_accelerationY.isChecked():
			unit = "milli g"
			self.data_update_thread.change_sensor("accelerationY")

		elif self.radio_accelerationZ.isChecked():
			unit = "milli g"
			self.data_update_thread.change_sensor("accelerationZ")

		else:
			unit = "testunit"																				#welche Einheit
			self.data_update_thread.change_sensor("")



		self.printing_thread = Printing_Thread((1/self.slider_rate.value())*10000,10,unit,[],True) # testliste und True fuer TestCase setzen
		self.printing_thread.printing_done.connect(self.update_gui)

		self.data_update_thread.start()
		self.printing_thread.start()


	def update_gui(self):
		self.svg_image.load("grafik.svg")

	def update_devicelist(self,device_list):
		identifierList = []
		for i in self.scroll_area_device_list:
			identifierList.append(i.text())
		for device in device_list:
			if device.returnAddressInformation()[0] not in identifierList:
				device_button = QPushButton(device.returnAddressInformation()[0])
				device_button.setFlat(True)
				device_button.setCheckable(True)
				if device.returnType() == "tcp":
					device_button.setStyleSheet('QPushButton {color: red}')
				elif device.returnType() == "ble":
					device_button.setStyleSheet('QPushButton {color: blue}')
				device_button.toggled.connect(partial(self.button_clicked,device_button))
				self.scroll_area_device_list.append(device_button)
				self.scroll_area_layout.addWidget(device_button)
				self.scroll_area_group.resize(self.sizeHint())

			else:
				pass


	def update_data(self,updated_data):

			self.data = updated_data.copy()
			self.printing_thread.dataHandover(self.data)


	def closeEvent(self, event):											# alle Threads beenden
		try:
			self.printing_thread.terminate()
			self.data_update_thread.terminate()
			self.device_update_thread.terminate()
			QCoreApplication.quit()
		except:
			self.data_update_thread.terminate()
			self.device_update_thread.terminate()
			QCoreApplication.quit()
		finally:
			myLogger.closeFile()



class Printing_Thread(QThread):
	printing_done = pyqtSignal(object)


	def __init__(self,timest,nots,yu,dat,testcase):
		QThread.__init__(self)
		self.printer = SVGPrinterClass.SVGPrinterClass()
		self.timestep = timest
		self.number_of_timesteps = nots
		self.yunit = yu
		self.data = dat
		self.testcase = testcase
		self.terminationRequested = False


	def run(self):
		while not self.terminationRequested:
			if self.data ==[]:
				time.sleep(0.01)
			else:
				self.printer.dataHandover(self.timestep,self.number_of_timesteps,self.yunit,self.data)
				self.printer.printSVG()
				self.printing_done.emit("Success")
				time.sleep(0.09)

	def dataHandover(self,dat):
		self.data = dat




class UpdateDeviceThread(QThread):
	devicelist_updated = pyqtSignal(object)

	def __init__(self, deviceHandler):
		QThread.__init__(self)
		self.deviceHandler = deviceHandler
		self.deviceList = []

	def run(self):
		while True:
			devices = self.deviceHandler.getDevices()
			if devices != self.deviceList:
				self.deviceList = []
				self.deviceList = list(devices)
				self.devicelist_updated.emit(self.deviceList)

			time.sleep(1)


class UpdateDataThread(QThread):
	data_updated = pyqtSignal(object)
	BLEConnectionThreadList = []

	def __init__(self):
		QThread.__init__(self)
		self.devices = []
		self.selection_string = ""
		self.data = []
		self.terminationRequested = False

	def run(self):
		for device in self.devices:
			if (device.returnType() == "ble") and (device.ConnectionThread == None):
				UpdateDataThread.BLEConnectionThreadList.append(BLEConnectionThread())
				UpdateDataThread.BLEConnectionThreadList[-1].daemon = True
				UpdateDataThread.BLEConnectionThreadList[-1].setDeviceAddress(device.returnAddressInformation()[0])
				UpdateDataThread.BLEConnectionThreadList[-1].start()
		for i in UpdateDataThread.BLEConnectionThreadList:
			while i.connected != True:
					pass




		while not self.terminationRequested:											#falls Geraet ausgewaehlt
			data = []
			for i in self.devices:
				data.append(i.getDataByType(self.selection_string))
			if data != self.data:
				self.data = data.copy()
				self.data_updated.emit(self.data)
			time.sleep(0.08)

	def new_devices(self,devices):
		self.devices.append(devices)

	def change_sensor(self, sensor_string):
		self.selection_string = sensor_string





if __name__ == '__main__':
	myLogger = SimpleLog.Logging(Device.valueTypeList)
	Device.Logger = myLogger
	BLEDevice.Logger = myLogger

	myDeviceHandler = DeviceHandler()
	Device.deviceHandler = myDeviceHandler
	TCPServer = ThreadedServer(TCP_IP,TCP_PORT)
	TCPServer.daemon = True
	TCPServer.start()										# Wifi Scan Thread


	BLEDevice.deviceHandler = myDeviceHandler
	bleServer = BLEScannerThread()
	bleServer.daemon = True
	bleServer.start()										# BLE Scan Thread

	app = QApplication(sys.argv)
	xdk_gui = Xdk_Desktop_Gui(myDeviceHandler)
	sys.exit(app.exec_())
