import socket
import threading
import struct
import collections
import time
import SimpleLog

TCP_IP = ''
TCP_PORT = 65530
defaultUpdateIntervall = 1000
defaultQuantityOfDataItmes = 11


class Device():
    deviceHandler = None
    recvBufferSize = 40
    threadSafety = threading.Lock()
    valueTypeList = ["temperature", "pressure", "humidity", "gyroX", "gyroY", "gyroZ", "accelerationX", "accelerationY", "accelerationZ"]
    Logger = None

    def __init__(self, TCPClient, address):
        self.uidBufferSize = 4

        self.TCPClient = TCPClient
        self.address = address
        self.uid = struct.unpack("<I", self.TCPClient.recv(self.uidBufferSize, socket.MSG_WAITALL))[0]

        self.data = collections.deque(defaultQuantityOfDataItmes * [[0,0,0,0,0,0,0,0,0]], defaultQuantityOfDataItmes)
        self.currentUpdateIntervall = defaultUpdateIntervall



        self.setUpdateIntervall(self.currentUpdateIntervall)
        Device.deviceHandler.addDevice(self)

        print("New Device: %s, %i" % (self.address, self.uid))

    def getTCPClient(self):
        return self.TCPClient

    def returnAddressInformation(self):
        return self.address

    def receiveData(self):
        reveiveData = self.TCPClient.recv(Device.recvBufferSize, socket.MSG_WAITALL)
        if reveiveData:
            reveiveData = struct.unpack("<IiIIiiiiii", reveiveData)
            if reveiveData[0] == self.currentUpdateIntervall:
                Device.Logger.setData(self.uid, reveiveData[1:10])
                self.data.append(reveiveData[1:10])
            else:
                self.currentUpdateIntervall = reveiveData[0]
                self.data = collections.deque(defaultQuantityOfDataItmes * [[0,0,0,0,0,0,0,0,0]], defaultQuantityOfDataItmes)
        else:
            raise Exception('Client disconnected')

    def getData(self):
        #Device.threadSafety.acquire()
        return list(self.data)
        #Device.threadSafety.release()

    def getDataByType(self, Type):
        returnList = []

        for element in self.data:
            returnList.append(element[Device.valueTypeList.index(Type)])
        return returnList



    def setUpdateIntervall(self, milliseconds):
        sendData = struct.pack("<I", milliseconds)
        self.TCPClient.send(sendData)
        print("update intervall:" +str(milliseconds))

    def getUpdateIntervall(self):
        return self.currentUpdateIntervall

    def returnType(self):
        return "tcp"


    def delete(self):
        self.TCPClient.close()
        Device.deviceHandler.deleteDevice(self)


    def __del__(self):
        self.TCPClient.close()
        try:
            Device.deviceHandler.deleteDevice(self)
        except:
            pass


class DeviceHandler():
    def __init__(self):
        self.deviceList = []

    def addDevice(self, deviceObject):
        self.deviceList.append(deviceObject)

    def getDevices(self):
        return self.deviceList

    def deleteDevice(self, deviceObject):
        self.deviceList.remove(deviceObject)

    def getDeviceByIpAddress(self, addressInformation):
        for device in self.deviceList:
            if device.returnAddressInformation()[0] == addressInformation:
                return device
        return Null

class ThreadedServer(threading.Thread):
    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.sock.bind((self.host, self.port))

    def run(self):
        self.listen()

    def listen(self):
        print("listen with TCP in Port %i" % self.port)
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            currentDevice = Device(client, address)
            threading.Thread(target = self.listenToClient,args = (currentDevice, )).start()

    def listenToClient(self, device):
        while True:
            try:
                device.receiveData()
            except:
                print("disconnected with %s, %i " % (device.address[0], device.uid))
                device.delete()
                del device
                return False

if __name__ == "__main__":
    myDeviceHandler = DeviceHandler()
    myLogger = SimpleLog.Logging(Device.valueTypeList)

    Device.deviceHandler = myDeviceHandler
    Device.Logger = myLogger

    TCPServer = ThreadedServer(TCP_IP,TCP_PORT)
    TCPServer.daemon = True
    TCPServer.start()
    #TCPServer.join()

    imput = input(">>")
    myLogger.closeFile()
