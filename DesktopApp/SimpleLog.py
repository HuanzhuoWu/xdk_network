from time import gmtime, strftime
from datetime import datetime
import random
import csv
import threading

class Logging():
    writeDataLock = threading.Lock()
    def __init__(self, sensorTypes, filepath = False):
        self.filepath = filepath
        self.sensorTypes = sensorTypes

        if self.filepath == False:
            self.filepath = strftime("%Y%m%H%M%S-", gmtime()) + str(random.randint(1000, 9999)) + ".csv"

        print("Logfile was created: ", self.filepath)

        self.fileObject = open(self.filepath, "w")
        self.writer = csv.writer(self.fileObject, dialect = "excel")
        self.tableHead = ["time" , "deviceId"] + sensorTypes
        self.writer.writerow(self.tableHead)


    def setData(self, deviceId, data):
        Logging.writeDataLock.acquire()
        now = datetime.now()
        self.writer.writerow([now.strftime("%Y-%m-%d %H:%M:%S.%f"), deviceId] + list(data))
        self.fileObject.flush()
        Logging.writeDataLock.release()


    def closeFile(self):
        self.fileObject.close()
