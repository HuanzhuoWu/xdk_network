from bluepy import btle
import threading
import struct
import collections
import TCP

class BLEDelegate(btle.DefaultDelegate):
    def __init__(self, BLEDevice):
        btle.DefaultDelegate.__init__(self)
        self.BLEDevice = BLEDevice
        print("Delegate initialised! " + self.BLEDevice.addr)

    def handleNotification(self, cHandle, bytedata):
        if cHandle == self.BLEDevice.tx_Characteristic.valHandle:
            if int.from_bytes(bytedata, byteorder = 'little', signed = True).bit_length() != 32:
                self.BLEDevice.processData(bytedata)
            else:
                self.BLEDevice.changeUID(bytedata)

class BLEDevice(btle.Peripheral):
    Lock = threading.Lock()

    # defined uuid's for btle
    alpwiseServiceUUID = btle.UUID("00005301-0000-0041-4c50-574953450000")
    rxCharacteristicUUID = btle.UUID("00005302-0000-0041-4c50-574953450000")
    txCharacteristicUUID = btle.UUID("00005303-0000-0041-4c50-574953450000")

    deviceHandler = None
    Logger = None
    defaultquantityOfDataItems = 11
    defaultUpdateIntervall = 1000
    valueTypeList = ["temperature", "pressure", "humidity", "gyroX", "gyroY", "gyroZ", "accelerationX", "accelerationY", "accelerationZ"]

    def __init__(self, address):
        btle.Peripheral.__init__(self)

        self.step = 0
        self.addr = address
        self.uid = address
        self.data = collections.deque(BLEDevice.defaultquantityOfDataItems * [[0,0,0,0,0,0,0,0,0]], BLEDevice.defaultquantityOfDataItems)
        self.currentUpdateIntervall = BLEDevice.defaultUpdateIntervall
        self.alpwise_Service = None
        self.rx_Characteristic = None
        self.tx_Characteristic = None
        self.data1 = (0, 0, 0, 0, 0)
        self.data2 = (0, 0, 0, 0, 0)
        self.ConnectionThread = None

        BLEDevice.deviceHandler.addDevice(self)
        print("New Device: %s" % self.addr)

    def connect(self):
        try:
            print("Trying to connnect to device " + self.addr)
            super().connect(self.addr, btle.ADDR_TYPE_PUBLIC)
            self.alpwise_Service = self.getServiceByUUID(BLEDevice.alpwiseServiceUUID)
            self.rx_Characteristic = self.alpwise_Service.getCharacteristics(BLEDevice.rxCharacteristicUUID)[0]
            self.tx_Characteristic = self.alpwise_Service.getCharacteristics(BLEDevice.txCharacteristicUUID)[0]
            self.change_tx_Characteristic("Notification")
            self.changeUpdateIntervall()
            return True

        except btle.BTLEException:
            print("Couldn't establish a connection to Bosch XDK sensor with address: " + str(self.addr) + "\n")
            return False

    def setConnectionThread(self, thread):
        self.ConnectionThread = thread

    def changeUID(self, byteuid):
        self.uid = struct.unpack("I", byteuid)

    def change_tx_Characteristic(self, mode):
        if str(mode) == "Notification":
            byte = b'\x01\x00'
        elif str(mode) == "Indication":
            byte = b'\x02\x00'
        else:
            byte = b'\x00\x00'

        try:
            self.writeCharacteristic(self.tx_Characteristic.valHandle + 1, byte, withResponse=True)
        except(btle.BTLEException, AttributeError):
            print("Changing TX Characteristic Mode for sensor " + self.addr + " failed!\n")

    def processData(self, bytedata):
        datatest = struct.unpack("IiIIi", bytedata)
        if (datatest[0] == self.currentUpdateIntervall) or (datatest[0] == tuple(self.data1)[0]):
            self.data1 = datatest

        else:
            self.data2 = struct.unpack("iiiii", bytedata)
            data = self.data1 + self.data2
            self.data.append(data[1:10])
            BLEDevice.Logger.setData(self.uid, data[1:10])

    def waitForNotifications(self, milliseconds):
        BLEDevice.Lock.acquire()
        btle.Peripheral.waitForNotifications(self, milliseconds)
        BLEDevice.Lock.release()

    def send(self, data):
        BLEDevice.Lock.acquire()
        self.rx_Characteristic.write(data)
        BLEDevice.Lock.release()

    def read(self):
        return self.tx_Characteristic.read()

    def setUpdateIntervall(self, milliseconds):
        self.currentUpdateIntervall = milliseconds
        try:
            if self.ConnectionThread.connected:
                self.changeUpdateIntervall()
        except AttributeError:
            pass

    def changeUpdateIntervall(self):
        timerval_struct = struct.pack("<I", self.currentUpdateIntervall)
        self.send(timerval_struct)

    def getUpdateIntervall(self):
        return self.currentUpdateIntervall

    def returnAddressInformation(self):
        return [self.addr]

    def returnType(self):
        return "ble"

    def getData(self):
        return list(self.data)

    def getDataByType(self, Type):
        returnList = []
        for element in self.data:
            returnList.append(element[BLEDevice.valueTypeList.index(Type)])
        return returnList


    def delete(self):
        self.disconnect()
        self.ConnectionThread.connected = False
        BLEDevice.deviceHandler.deleteDevice(self)

    def __del__(self):
        self.disconnect()
        self.ConnectionThread.connected = False
        try:
            BLEDevice.deviceHandler.deleteDevice(self)
        except:
            pass


class ScanDelegate(btle.DefaultDelegate):
    def __init__(self):
        btle.DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            inList = False
            try:
                for device in BLEDevice.deviceHandler.getDevices():
                    if device.returnAddressInformation()[0] == dev.addr:
                        inList = True

            except AttributeError:
                print("No Devicehandler-Object")

            if (not inList) and ("fc:d6:bd" in str(dev.addr)):
                device = BLEDevice(dev.addr)

class BLEScannerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.devices = []

    def run(self):
        self.scan()

    def scan(self):
        print("Start ScannerThread")
        scanner = btle.Scanner().withDelegate(ScanDelegate())
        while True:
            try:
                self.devices = scanner.scan()
            except btle.BTLEException:
                continue

class BLEConnectionThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.deviceAddress = None
        self.connected = False

    def run(self):
        self.device = BLEDevice.deviceHandler.getDeviceByIpAddress(self.deviceAddress)
        self.device.setDelegate(BLEDelegate(self.device))
        self.device.setConnectionThread(self)
        if not self.device.connect():
            BLEDevice.deviceHandler.deleteDevice(self.device)
            self.device.delete()
            del device
        else:
            self.connected = True

        while self.connected == True:
            try:
                if self.device.waitForNotifications(1/1000):
                    pass
            except btle.BTLEException:
                continue
    def setDeviceAddress(self, address):
        self.deviceAddress = address

    def disconnectFromDevice(self):
        if self.connected == True:
            self.connected = False
            try:
                self.device.disconnect()
            except ValueError:
                pass

if __name__ == "__main__":
    myDeviceHandler = TCP.DeviceHandler()
    BLEDevice.deviceHandler = myDeviceHandler
    myScanner = BLEScannerThread()
    myScanner.start()

    input("Verzögerung")
    for device in BLEDevice.deviceHandler.getDevices():
        if device.returnAddressInformation()[0] == "fc:d6:bd:10:17:76":
            BLEDeviceConnection = BLEConnectionThread()
            BLEDeviceConnection.setDeviceAddress(device.returnAddressInformation()[0])
            BLEDeviceConnection.start()

    input("Verzögerung")
    while True:
        for device in myDeviceHandler.getDevices():
            if device.returnAddressInformation()[0] == "fc:d6:bd:10:17:76":
                print(device.read())
