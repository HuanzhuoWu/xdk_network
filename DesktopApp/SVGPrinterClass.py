import svgwrite

class SVGPrinterClass():
    timestep = 0                    #Aktualisierungsintervall der Grafik in Millisekunden
    number_of_timesteps = 0         #Anzahl an dargestellten Werten (evtl noch veraendern)
    y_unit = ""                     #Bezeichnung an der Y-Achse
    data = []                       #Liste mit Werten
    #muessen vom Konstruktor uebergeben werden

    x_unit = ""                     #Bezeichnung an der X-Achse
    xlabeling = []                  #Liste fuer die Beschriftung der X-Achse
    ylabeling = []                  #Liste fuer die Beschriftung der Y-Achse
    max_value_bound = 0             #obere Grenze fuer die Werte
    min_value_bound = 0             #untere Grenze fuer die Werte
    valuestep = 0                   #Wertschritt
    polyline_data = []              #Liste fuer die Berechnung der Bildpunkte der Graphen
    dim = 0                         #Dimension der DatenListe
    max_bound = 0
    min_bound = 0





    color = ["red","green", "blue", "yellow", "gray" ,"black", "orange", "brown", "pink"]

    def __init__(self):
        self.max_bound = 0
        self.min_bound = 0
        self.labelingIterator = 0
        self.scaleFactor = 0.3

    def dataHandover(self,timest,nots,yu,dat):             
        self.timestep = timest
        self.number_of_timesteps = nots
        self.y_unit =  yu
        self.scaleFactor = 0.3
        self.data = dat
        self.ylabeling = []
        self.xlabeling = []
        self.polyline_data = []
    
    def __calculateLabeling(self,x_or_y):
        if x_or_y == "y":           # y-Achse
            if self.labelingIterator == 0:
                print(self.data)
                self.min_bound = min(sum(self.data,[]))-abs(min(sum(self.data,[])))*self.scaleFactor
                self.max_bound = max(sum(self.data,[]))+abs(max(sum(self.data,[])))*self.scaleFactor
                self.labelingIterator = 1
            else:
                x = sum(self.data,[])
                x.append(self.min_bound)
                self.min_bound = min(x)
               
                x.append(self.max_bound)
                self.max_bound = max(x)

            self.valuestep = (self.max_bound-self.min_bound)/11
            
            if self.valuestep ==0:
                self.valuestep = self.max_bound/11
            
            for i in range(0,11):
                self.ylabeling.append(round(self.min_bound+i*self.valuestep,1))


        elif x_or_y == "x":         # x-Achse
            for i in range(self.number_of_timesteps+1,0,-1):
                if i == 1:
                    self.xlabeling.append("t[k]")
                else:
                    self.xlabeling.append("t[k-"+str(i-1)+"]")

        else:                       # Fehler
            print("Error")

    def __calculateData(self):
        for sublist in self.data:
            liste = []
            iterator = len(sublist)-1
            for i in sublist[::-1]:
                try:   
                    tupel = (200+iterator*500/self.number_of_timesteps , 100+(self.max_bound-i)/(self.max_bound-self.min_bound)*500)
                    iterator = iterator-1
                except ZeroDivisionError:
                    tupel = (200+iterator*500/self.number_of_timesteps, 600)
                    iterator = iterator-1
                liste.append(tupel)
            self.polyline_data.append(liste)


    def printSVG(self):
        dwg = svgwrite.Drawing('grafik.svg', profile='full')                                #Anlegen einer SVG Datei
        my_polyline = dwg.add(dwg.g(stroke_width=2, fill='none'))
        dwg.add(dwg.line((200, 600), (750, 600), stroke=svgwrite.rgb(10, 10, 16, '%')))     #das ist die x-Achse
        dwg.add(dwg.line((200, 600), (200, 100), stroke=svgwrite.rgb(10,10,16, '%')))       #das ist die y-Achse

        dwg.add(dwg.line((750, 600), (740, 607), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((750, 600), (740, 593), stroke=svgwrite.rgb(10,10,16, '%')))       #das ist der Pfeil an der x-Achse

        dwg.add(dwg.line((200, 100), (193, 110), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 100), (207, 110), stroke=svgwrite.rgb(10,10,16, '%')))       #das ist der Pfeil an der y-Achse

        dwg.add(dwg.line((200, 600), (190, 600), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 550), (190, 550), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 500), (190, 500), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 450), (190, 450), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 400), (190, 400), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 350), (190, 350), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 300), (190, 300), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 250), (190, 250), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 200), (190, 200), stroke=svgwrite.rgb(10, 10, 16, '%')))
        dwg.add(dwg.line((200, 150), (190, 150), stroke=svgwrite.rgb(10, 10, 16, '%')))     #Achseneinteilung y-Achse im Moment fest auf 10 verschiedene Werte

        for i in range(0,self.number_of_timesteps+1):
            dwg.add(dwg.line((200+i*500/self.number_of_timesteps, 600), (200+i*500/self.number_of_timesteps, 610), stroke=svgwrite.rgb(10, 10, 16, '%')))   #Achseneinteilung x-Achse variabel

        self.__calculateLabeling("y")
        for i in range(0,10):
            dwg.add(dwg.text(round(self.ylabeling[i],3), insert=(160, 605-i*50), fill='black'))         # Beschriftung der Achseneinteilung der y-Achse
        
        self.__calculateLabeling("x")
        for i in range(0,self.number_of_timesteps+1):
            dwg.add(dwg.text(self.xlabeling[i], insert=(187+i*500/self.number_of_timesteps, 630), fill='black'))            # Beschriftung der Achseneinteilung der x-Achse


        self.y_unit = self.y_unit.split()
        for i in range(len(self.y_unit)):
            dwg.add(dwg.text(self.y_unit[i], insert=(95, 110+i*12), fill='black'))               # Beschriftung y-Achse

        self.x_unit = ["time","in", str(round(self.timestep/1000,4))+" seconds"]
        for i in range(len(self.x_unit)):
            dwg.add(dwg.text(self.x_unit[i], insert=(740, 630+i*13), fill='black'))             # Beschriftung x-Achse


        
        self.__calculateData()
        for i in self.polyline_data:
            my_polyline.add(dwg.polyline(i,stroke=SVGPrinterClass.color[self.polyline_data.index(i)],stroke_linejoin='round'))                      # den Grafen zeichnen fuer mehrdimensionale Daten
        dwg.viewbox(0,50,850,650)                                                               # wegen cairosvg
        dwg.save()                                                                              #SVG Datei speichern und fertig
        











